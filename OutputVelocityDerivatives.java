
public class OutputVelocityDerivatives extends Output {

	public OutputVelocityDerivatives(double outputTimeInterval, GridCartesian grid, Equations equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints spatial velocity derivatives";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double dyu = 0, dxv = 0, dyv = 0, dxu = 0;
		double dyuNorm = 0, dxvNorm = 0, dyvNorm = 0, dxuNorm = 0;
    	int i, j, k;
    	
    	for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			if (equations instanceof EquationsAcousticSimple){
				dyu = (quantities[i][j+1][k][EquationsAcousticSimple.INDEX_VX]
					  -quantities[i][j-1][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2;
				dxv = (quantities[i+1][j][k][EquationsAcousticSimple.INDEX_VY]
				      -quantities[i-1][j][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2;
				dxu = (quantities[i+1][j][k][EquationsAcousticSimple.INDEX_VX]
					  -quantities[i-1][j][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).xSpacing()/2;
				dyv = (quantities[i][j+1][k][EquationsAcousticSimple.INDEX_VY]
				      -quantities[i][j-1][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).ySpacing()/2;
			} else if (equations instanceof EquationsHydroIdeal){
					dyu = (quantities[i][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
						  -quantities[i][j-1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j-1][k][EquationsHydroIdeal.INDEX_RHO]
								  )/((GridCartesian) grid).ySpacing()/2;
					dxv = (quantities[i+1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
					      -quantities[i-1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i-1][j][k][EquationsHydroIdeal.INDEX_RHO]
					    		  )/((GridCartesian) grid).xSpacing()/2;
					dxu = (quantities[i+1][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
						  -quantities[i-1][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i-1][j][k][EquationsHydroIdeal.INDEX_RHO]
								  )/((GridCartesian) grid).xSpacing()/2;
					dyv = (quantities[i][j+1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
					      -quantities[i][j-1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j-1][k][EquationsHydroIdeal.INDEX_RHO]
					    		  )/((GridCartesian) grid).ySpacing()/2;
			} else {
				System.err.println("ERROR: No implementation of velocity-derivative output for these equations!");
			}
				
			dxuNorm += Math.abs(dxu);
			dyuNorm += Math.abs(dyu);
			dxvNorm += Math.abs(dxv);
			dyvNorm += Math.abs(dyv);
			
			
			//println(g.getX() + " " + g.getY() + " " + dxu + " " + dxv + " " + dyu + " " + dyv); 
		}
    	dxuNorm *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing();
		dyuNorm *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing();
		dxvNorm *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing();
		dyvNorm *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing();
		
		
    	println(time + " " + " " + dxuNorm + " " + dxvNorm + " " + dyuNorm + " " + dyvNorm);
    	//println(""); println("");
    	
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}

}
