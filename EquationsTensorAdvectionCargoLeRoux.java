import matrices.SquareMatrix;


public class EquationsTensorAdvectionCargoLeRoux extends Equations {

	protected Setup hydroSetup;
	
	public EquationsTensorAdvectionCargoLeRoux(GridCartesian2D grid, Setup hydroSetup) {
		super(grid, 0);
		this.hydroSetup = hydroSetup;
	}

	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		double[] vel = ((EquationsHydroIdeal) hydroSetup.equations()).getVelocity(hydroSetup.getConservedQuantities()[i][j][k]); 
		if (direction == GridCartesian.X_DIR){
			// k = 0
			return new double[] {
					vel[0] * quantities[0],   // pi00
					vel[0] * quantities[1],   // pi01
					vel[1] * quantities[0],   // pi10
					vel[1] * quantities[1] }; // pi11
		} else if (direction == GridCartesian.Y_DIR){
			// k = 1
			return new double[] {
					vel[0] * quantities[2],   // pi00
					vel[0] * quantities[3],   // pi01
					vel[1] * quantities[2],   // pi10
					vel[1] * quantities[3] }; // pi11
		} else {
			System.err.println("Cargo Le Roux not implemented for 2-d grids");
			return null;
		}
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		// TODO continue here
		return null;
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getNumberOfConservedQuantities() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void transformFixedParameters(double[] normalVector) {
		// TODO Auto-generated method stub
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// TODO Auto-generated method stub
	}


}
