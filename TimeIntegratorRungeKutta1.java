
public class TimeIntegratorRungeKutta1 extends TimeIntegratorRungeKutta {

	protected double[] RKOnestepCoefficient = new double[] {0.0, 1.0};

	public TimeIntegratorRungeKutta1(GridCartesian grid, double CFL) {
		super(grid, CFL, 1);
	}

	@Override
	protected double RKCoefficient(int step, int n) { return 0.0; }

	@Override
	protected double RKOnestepCoefficient(int step) { return RKOnestepCoefficient[step]; }


}
