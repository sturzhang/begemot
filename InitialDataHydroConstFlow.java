
public class InitialDataHydroConstFlow extends InitialDataHydro {

	private double rho;
	private double pressure;
	private double[] velocity;
	
	public InitialDataHydroConstFlow(Grid grid, EquationsHydroIdeal equations, double rho, double pressure, double[] velocity) {
		super(grid, equations);
		this.rho = rho;
		this.pressure = pressure;
		this.velocity = velocity;
	}

    public String getDetailedDescription(){
    	return "Ideal gas of density = " + rho + "and pressure = " + pressure + 
    			" flowing in a stationary manner at a velocity v = (" + velocity[0] + ", "
    			+ velocity[1] + ", " + velocity[2] + ")";
    }
    
	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double rhoV2;
		double[] res = new double[q];
		
		rhoV2 = rho*(velocity[0]*velocity[0]+velocity[1]*velocity[1]+velocity[2]*velocity[2]);
		
		res[EquationsHydroIdeal.INDEX_RHO] = rho;
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    				
		res[EquationsHydroIdeal.INDEX_XMOM] = rho*velocity[0];
		res[EquationsHydroIdeal.INDEX_YMOM] = rho*velocity[1];
		res[EquationsHydroIdeal.INDEX_ZMOM] = rho*velocity[2];
		
		for (int s = 5; s < q; s++){
			// passive scalars
			//if (grid.getX(i) > grid.xMidpoint()){
				res[s] = Math.pow(-1.0, Math.max(0, Math.round((g.getY() - grid.ymin())/(grid.ymax() - grid.ymin())*10)));
			//}
		}
		
		return res;	
	}

}
