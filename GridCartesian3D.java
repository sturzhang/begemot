
public class GridCartesian3D extends GridCartesian {

	public GridCartesian3D(double xmin, double xmax, int nx, 
			double ymin, double ymax, int ny, 
			double zmin, double zmax, int nz, int ghostcells) {
		super();
		
		this.xmin = xmin;
    	this.xmax = xmax;
    	
    	this.ymin = ymin;
    	this.ymax = ymax;
    	
    	this.zmin = zmin;
    	this.zmax = zmax;
    	
    	ghostcellsX = ghostcells;
    	ghostcellsY = ghostcells;
    	ghostcellsZ = ghostcells;
    	
    	this.nx = nx;
    	this.ny = ny;
    	this.nz = nz;
 	}

	@Override
	public String getDetailedDescription() {
		return "(3-d grid)";
	}

	@Override
	public int dimensions() {
		return 3;
	}

	

}
