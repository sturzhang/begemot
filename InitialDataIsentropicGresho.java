
public class InitialDataIsentropicGresho extends InitialDataIsentropicVortex {

	protected InitialDataHydroGresho myGreshoHydro;
	
	protected double rhocenter, mach;
	
	
	public InitialDataIsentropicGresho(Grid grid, EquationsIsentropicEuler equations, double mach, double[] offsetSpeed) {
		super(grid, equations, offsetSpeed);
		
		myGreshoHydro = new InitialDataHydroGresho(grid, 
				new EquationsHydroIdeal(grid, 0, equations.gamma()), mach, 1.0, 1.0);
		
		this.mach = mach;
		this.rhocenter = Math.pow(( 
				1.0/(mach*mach) - 
				(myGreshoHydro.pressure(0.2) - myGreshoHydro.pressure(0))*(equations.gamma() - 1)
				)/equations.gamma()/equations.K(), 
				1.0/(equations.gamma()-1));
		
		
	}

	@Override
	protected double speed(double r) {
		return myGreshoHydro.speed(r);
	}

	@Override
	protected double rho(double r) {
		return Math.pow(   ( myGreshoHydro.pressure(r) - myGreshoHydro.pressure(0) )*
				(equations.gamma() - 1)/equations.K() / equations.gamma() + 
				Math.pow(rhocenter, equations.gamma() - 1), 1.0/(equations.gamma()-1));
	}

	@Override
	public String getDetailedDescription() {
		return "Gresho-like vortex for isentropic Euler";
	}

}
