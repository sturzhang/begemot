
public class FluxSolverVertexAcousticBichar extends FluxSolverVertex {
	
	public FluxSolverVertexAcousticBichar(GridCartesian2D grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	protected VertexFluxAndWaveSpeed vertexflux(double[] NE, double[] NW, double[] SW, double[] SE) {
		int quantities = equations.getNumberOfConservedQuantities();
		double[][] flux = new double[2][quantities];
		double vertexP, vertexU, vertexV;
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		int indV = EquationsAcousticSimple.INDEX_VY;
		int indU = EquationsAcousticSimple.INDEX_VX;
		int indP = EquationsAcousticSimple.INDEX_P;
		
		/*for (int q = 0; q < quantities; q++){
			flux[Grid.Z_DIR][q] = 0.0;
		}*/
		flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_VZ] = 0.0;
		flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_VY] = 0.0;
		
		
		
		double meanP = 0.25*(	NE[indP]  + NW[indP] + SW[indP]  + SE[indP]);
		double meanU = 0.25*(	NE[indU] + NW[indU] + SW[indU] + SE[indU]);
		double meanV = 0.25*(	NE[indV] + NW[indV] + SW[indV] + SE[indV]);

		double divUV = 0.25 * ( SW[indU] + NW[indU] - NE[indU] - SE[indU]
				- NW[indV] + SW[indV] - NE[indV] + SE[indV]
				);
		double gradxP = 0.25 * (SW[indP] + NW[indP] - NE[indP] - SE[indP]);
		double gradyP = 0.25 * (SW[indP] + SE[indP] - NW[indP] - NE[indP]);
		
		if (((EquationsAcousticSimple) equations).symmetric()){
			vertexP = meanP + divUV; 
			vertexU = meanU + gradxP;
			vertexV = meanV + gradyP;
		
			flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_P]  = c*vertexU;
			flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_VX] = c*vertexP;
			flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_VY]  = 0.0;
			
			flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_P]  = c*vertexV;
			flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_VX] = 0.0;
			flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_VY] = c*vertexP;  //*/
		} else {
			vertexP = meanP + c*divUV; 
			vertexU = meanU + gradxP/c;
			vertexV = meanV + gradyP/c;
			
			
			flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_P]  = c*c*vertexU;
			flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_VX] = vertexP;
			flux[GridCartesian.X_DIR][EquationsAcousticSimple.INDEX_VY]  = 0.0;
			
			flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_P]  = c*c*vertexV;
			flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_VX] = 0.0;
			flux[GridCartesian.Y_DIR][EquationsAcousticSimple.INDEX_VY] = vertexP; // */ 
		}
			
		
		double[] waveSpeeds = new double[] {
				((EquationsAcousticSimple) equations).soundspeed(),  ((EquationsAcousticSimple) equations).soundspeed()};
		
		return new VertexFluxAndWaveSpeed(flux, waveSpeeds);
	}

	@Override
	public String getDetailedDescription() {
		return "Vertex-flux solver based on bicharacteristics discretization for linear acoustics.";
	}





	

}
