
public class SourceIsentropicEulerLikeInShallowWater extends SourceInCellOnly {

	protected double bumpHeight = 0.96;
	protected double bumpWidth = 0.1;
	
	
	public SourceIsentropicEulerLikeInShallowWater(Grid grid,
			EquationsIsentropicEuler equations, boolean averaged) {
		super(grid, equations, averaged);
	}

	public SourceIsentropicEulerLikeInShallowWater(Grid grid, EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell g,
			double time) {
		double[] res = new double[quantities.length];
		res[EquationsIsentropicEuler.INDEX_XMOM] = -quantities[EquationsIsentropicEuler.INDEX_RHO]*((EquationsIsentropicEuler) equations).K()*2
				*bottomTopographyDerivative(g.getX());
		return res;
	}

	public double bottomTopography(double x){
		x -= grid.xMidpoint();
		return bumpHeight*Math.exp(-x*x/bumpWidth/bumpWidth);
	}
	
	
	protected double bottomTopographyDerivative(double x){
		x -= grid.xMidpoint();
		// b(x) = height*Math.exp(-x*x/width/width);
		return bumpHeight*Math.exp(-x*x/bumpWidth/bumpWidth)*(-2.0*x/bumpWidth/bumpWidth);
	}
	
	@Override
	public String getDetailedDescription() {
		return "Source term for shallow water";
	}

}
