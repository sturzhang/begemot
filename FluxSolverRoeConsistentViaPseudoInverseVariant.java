import matrices.SquareMatrix;


public class FluxSolverRoeConsistentViaPseudoInverseVariant extends FluxSolverRoeConsistentViaPseudoInverse {

	public FluxSolverRoeConsistentViaPseudoInverseVariant(GridCartesian grid,
			Equations equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		
		
		int quantities = equations.getNumberOfConservedQuantities();
		
		double[] diff    = new double[quantities];
		double[] perp    = new double[quantities];
		double[] perpNew = new double[quantities];
		double[] central = new double[quantities];
		double[] upwindingTermX     = new double[quantities];
		double[] upwindingTermY     = new double[quantities];
		double[] upwindingTermMixed = new double[quantities];
		double[] fluxRes;
		
		boolean detailedOutput = false;
		
		SquareMatrix upwindingMatrixX = new SquareMatrix(quantities);
		SquareMatrix upwindingMatrixY = new SquareMatrix(quantities);
		UpwindingMatrixAndWaveSpeed tmp;
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, ijk, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, ip1jk, direction);
		
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, ijp1k, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, ijm1k, direction);
		
		double[] fluxRightTop = equations.fluxFunction(i, j, k, ip1jp1k, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, direction);
		
		double verticalAverageFluxLeft, verticalAverageFluxRight;
		
		for (int q = 0; q < quantities; q++){ 
			verticalAverageFluxLeft = 0.25*(fluxLeftTop[q] + 2.0*fluxLeft[q] + fluxLeftBottom[q]);
			verticalAverageFluxRight = 0.25*(fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q]);
			diff[q] = verticalAverageFluxRight - verticalAverageFluxLeft; 
			//diff[q] = fluxRight[q] - fluxLeft[q];
			
			perp[q] = 0.25*(fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q]);
			
			perpNew[q] = 0.25*(fluxRightTop[q] - fluxRightBottom[q] - fluxLeftTop[q] + fluxLeftBottom[q]);
			
			central[q] = 0.5*(verticalAverageFluxLeft + verticalAverageFluxRight);
			//central[q] = 0.5*(fluxRight[q] + fluxLeft[q]);
		}
		
		
		
		
		tmp = getAveragedSignMatrix(g.i(), g.j(), g.k(), ijk, ip1jk, GridCartesian.X_DIR);
		upwindingMatrixX = tmp.matrix();
		upwindingTermX = upwindingMatrixX.mult(0.5).mult(diff);
	    
		tmp = getAveragedSignMatrix(g.i(), g.j(), g.k(), ijk, ip1jk, GridCartesian.Y_DIR);
		upwindingMatrixY = tmp.matrix();
		upwindingTermY = upwindingMatrixY.mult(0.5).mult(perp);
	    
		upwindingTermMixed = upwindingMatrixY.mult(upwindingMatrixX).add(
					upwindingMatrixX.mult(upwindingMatrixY)
				).mult(0.125).mult(perpNew);
		
    	fluxRes = new double[quantities];
    	
    	
    	for (int q = 0; q < quantities; q++){
    		fluxRes[q] = central[q] - upwindingTermX[q] - upwindingTermY[q] - upwindingTermMixed[q];
    		if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("fluxes are NaN");
    	    	detailedOutput = true;
    	    }
    	}

    		
    	if (detailedOutput){
        	for (int q = 0; q < quantities; q++){
        	    System.err.println("left: " + ijk[q] + " right: " + ip1jk[q]
        	    					+ " res: " + fluxRes[q] + " central: " + central[q] 
        	    					+ " upwinding: " + upwindingTermX[q]);
        	    
        	}
    	}
    	
 
    	return new FluxAndWaveSpeed(fluxRes, tmp.waveSpeed());
 	}
	
	@Override
	public String getDetailedDescription() {
		return "Roe solver extended to multiple dimensions with stationarity consistent diffusion: variant with a mixed (commutator-like) additional term";
	}

}
