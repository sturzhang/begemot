import matrices.SquareMatrix;


public class FluxSolverTurkel extends FluxSolverRoeHydro {

	protected double machCutoff;
	
	public FluxSolverTurkel(GridCartesian grid, EquationsHydroIdeal equations,
			int meanType, double machCutoff) {
		super(grid, equations, meanType);
		this.machCutoff = machCutoff;
	}

	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		double[][] mat = new double[5][5];
		HydroState state = new HydroState(quantities, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		double machParameter = 1.0; // equations.mach(); does not work fully yet
		
		double rho = 1.0;
		double v2 = state.v2;
		double c = state.soundSpeed;
	    double c2 = c*c;
	    double vX = state.vX; 
	    double vY = state.vY; 
	    double vZ = state.vZ;
	    
	    double machParameter2 = machParameter*machParameter;
	    double machLocal = Math.sqrt(v2) / c;
	    double limitedMach, limitedMach2;

	    
	    if (machLocal > 1.0){
	    	limitedMach = 1.0;
	    } else {
	    	if (machLocal > machCutoff){
	    		limitedMach = machLocal; 
	    	} else {
	    		limitedMach = machCutoff;
	    	}
	    }
	    limitedMach2 = limitedMach*limitedMach;
	    
		double waveSpeed = c/machParameter + Math.sqrt(v2);
		
		if (direction == GridCartesian.X_DIR){
			double m1n = (vX >= 0? 1.0 : -1.0); 
			double v = vX;
			double tau = Math.sqrt(4 *c2 *limitedMach2+Math.pow(-1+limitedMach2, 2)* machParameter2 * v*v);
			
			mat[0][0] = m1n * vX;
			mat[0][1] = machParameter*rho*v*(limitedMach2+1)/tau;
			mat[0][2] = 0.0;
			mat[0][3] = 0.0;
			mat[0][4] = (2*c2 + machParameter*v*( machParameter* (limitedMach2-1)* v - m1n* tau))/ (c2* machParameter* tau);
							
			mat[1][0] = 0.0;
			mat[1][1] = (2* c2 *limitedMach2 - (-1+limitedMach2)* machParameter2* v*v)/(machParameter* tau);
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = (1+limitedMach2)*v / (machParameter*rho*tau);
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = m1n*vX;
			mat[2][3] = 0.0;
			mat[2][4] = 0.0;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = m1n*vX;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = (c2* (1+limitedMach2)* machParameter* v)/tau;
			mat[4][2] = 0.0;
			mat[4][3] = 0.0;
			mat[4][4] = (2*c2+(-1+limitedMach2)* machParameter2* v*v)/(machParameter*tau);
		}
		if (direction == GridCartesian.Y_DIR){
			double m1n = (vY >= 0? 1.0 : -1.0); 
			double v = vY;
			double tau = Math.sqrt(4 *c2 *limitedMach2+Math.pow(-1+limitedMach2, 2)* machParameter2 * v*v);
			
			mat[0][0] = m1n * vX;
			mat[0][1] = 0.0;
			mat[0][2] = machParameter*rho*v*(limitedMach2+1)/tau;
			mat[0][3] = 0.0;
			mat[0][4] = (2*c2 + machParameter*v*( machParameter* (limitedMach2-1)* v - m1n* tau))/ (c2* machParameter* tau);

			mat[1][0] = 0.0;
			mat[1][1] = m1n*vX;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = 0.0;
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = (2* c2 *limitedMach2 - (-1+limitedMach2)* machParameter2* v*v)/(machParameter* tau);
			mat[2][3] = 0.0;
			mat[2][4] = (1+limitedMach2)*v / (machParameter*rho*tau);
			
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = m1n*vX;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = 0.0;
			mat[4][2] = (c2* (1+limitedMach2)* machParameter* v)/tau;
			mat[4][3] = 0.0;
			mat[4][4] = (2*c2+(-1+limitedMach2)* machParameter2* v*v)/(machParameter*tau);

		}
		
		double dUdVarr[][] = new double[5][5];
		double dVdUarr[][] = new double[5][5];
		
		dUdVarr[0][0] = 1.0;
		dUdVarr[1][0] = vX;
		dUdVarr[1][1] = rho;
		dUdVarr[2][0] = vY;
		dUdVarr[2][2] = rho;
		dUdVarr[3][0] = vZ;
		dUdVarr[3][3] = rho;
		dUdVarr[4][0] = machParameter*machParameter * v2/2;
		dUdVarr[4][1] = machParameter*machParameter * rho*vX;
		dUdVarr[4][2] = machParameter*machParameter * rho*vY;
		dUdVarr[4][3] = machParameter*machParameter * rho*vZ;
		dUdVarr[4][4] = 1.0/(((EquationsHydroIdeal) equations).gamma() - 1);
		
		dVdUarr[0][0] = 1.0;
		dVdUarr[1][0] = -vX/rho;
		dVdUarr[1][1] = 1.0/rho;
		dVdUarr[2][0] = -vY/rho;
		dVdUarr[2][2] = 1.0/rho;
		dVdUarr[3][0] = -vZ/rho;
		dVdUarr[3][3] = 1.0/rho;
		dVdUarr[4][0] = machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*v2/2;
		dVdUarr[4][1] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vX;
		dVdUarr[4][2] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vY;
		dVdUarr[4][3] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vZ;
		dVdUarr[4][4] = (((EquationsHydroIdeal) equations).gamma() - 1);

		SquareMatrix dUdV = new SquareMatrix(dUdVarr);
		SquareMatrix dVdU = new SquareMatrix(dVdUarr);

		// TODO replace here by calling ((EquationsHydroIdeal) equations).primToCons();
		
		return new UpwindingMatrixAndWaveSpeed(
				dUdV.mult(new SquareMatrix(mat)).mult(dVdU).toSquareMatrix(),
				waveSpeed);
	}

	
	
	
	
	
}
