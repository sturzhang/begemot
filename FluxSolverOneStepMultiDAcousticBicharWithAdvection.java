
public class FluxSolverOneStepMultiDAcousticBicharWithAdvection extends
		FluxSolverOneStepMultiDAcousticBichar {

	public FluxSolverOneStepMultiDAcousticBicharWithAdvection(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}
	
	@Override
	protected double[] multidflux(double[] LT, double[] L, double[] LB, double[] RT, double[] R, double[] RB){
		// TODO this is mostly a copy from superclass, but for the sake of a self-contained implementation tolerated
		
		int quantities = L.length;
		double[] fluxRes = new double[quantities];
		double diffX[] = new double[quantities];
		double mixeddiffX[] = new double[quantities];
		double sumX[] = new double[quantities];
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		
		double U = ((EquationsAcousticSimple) equations).vx();
		double V = ((EquationsAcousticSimple) equations).vy();
		
		boolean includeAcousticOperator = true;
		
		int perpMatrixWithoutAdvectiveContributions = 0;
		int perpChoiceForAdvectionJustAveraged      = 1;		
		int perpChoice = perpChoiceForAdvectionJustAveraged;

		
		for (int q = 0; q < quantities; q++){ 
			diffX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] - (0.25*LT[q] + 0.5*L[q] + 0.25*LB[q]);
			sumX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] + 0.25*LT[q] + 0.5*L[q] + 0.25*LB[q];
			mixeddiffX[q] = (LT[q] + RT[q] - (LB[q] + RB[q]));
		}
		
		// fluxes of the central scheme:
		
		if (includeAcousticOperator){
			if (((EquationsAcousticSimple) equations).symmetric()){
				fluxRes[EquationsAcousticSimple.INDEX_VX] = 0.5*c*sumX[EquationsAcousticSimple.INDEX_P];
				fluxRes[EquationsAcousticSimple.INDEX_VY] = 0.0;
				fluxRes[EquationsAcousticSimple.INDEX_VZ] = 0.0;
				fluxRes[EquationsAcousticSimple.INDEX_P ] = 0.5*c*sumX[EquationsAcousticSimple.INDEX_VX];
			} else {
				fluxRes[EquationsAcousticSimple.INDEX_VX] = 0.5*sumX[EquationsAcousticSimple.INDEX_P];
				fluxRes[EquationsAcousticSimple.INDEX_VY] = 0.0;
				fluxRes[EquationsAcousticSimple.INDEX_VZ] = 0.0;
				fluxRes[EquationsAcousticSimple.INDEX_P ] = 0.5*c*c*sumX[EquationsAcousticSimple.INDEX_VX];
			}
		}
		for (int q = 0; q < quantities; q++){
			fluxRes[q] += 0.5*U*sumX[q];
		}
			
		
		// diffusion of a dimsplit scheme:
		
		if (includeAcousticOperator){
			fluxRes[EquationsAcousticSimple.INDEX_VX] -= 0.5*(Math.abs(U+c) + Math.abs(U-c))/2*diffX[EquationsAcousticSimple.INDEX_VX];
		}
		fluxRes[EquationsAcousticSimple.INDEX_VX] -=  0.5*(Math.abs(U+c) - Math.abs(U-c))/2/c*diffX[EquationsAcousticSimple.INDEX_P];
		
		fluxRes[EquationsAcousticSimple.INDEX_VY] -= 0.5*Math.abs(U)*diffX[EquationsAcousticSimple.INDEX_VY];
		
		if (includeAcousticOperator){
			fluxRes[EquationsAcousticSimple.INDEX_P ] -= 0.5*(Math.abs(U+c) + Math.abs(U-c))/2*diffX[EquationsAcousticSimple.INDEX_P];
		}
		fluxRes[EquationsAcousticSimple.INDEX_P ] -=  0.5*(Math.abs(U+c) - Math.abs(U-c))/2*c*diffX[EquationsAcousticSimple.INDEX_VX];
		
		// multi-dimensional contributions:
		if (((EquationsAcousticSimple) equations).symmetric()){
			// TODO this is not too hard!
			System.out.println("Error: FluxSolverBichar not implemented for symmetric acoustic system!");
		} else {
			double sgnU = (U == 0 ? 0 : U/Math.abs(U));
			double sgnV = (V == 0 ? 0 : V/Math.abs(V));
			
			double acousticEntry = 0, advectiveEntry = 0;			
			
			if (perpChoice == perpMatrixWithoutAdvectiveContributions){ 	
				acousticEntry = c;
				advectiveEntry = 0;
			} else if (perpChoice == perpChoiceForAdvectionJustAveraged){
				acousticEntry = 0.5*(Math.abs(V+c) + Math.abs(V-c)) - Math.abs(V); // subsonic: c-Math.abs(V)
				advectiveEntry = (Math.abs(V+c) - Math.abs(V-c)) / 2 / c; // subsonic: V/c
			}
		
			fluxRes[EquationsAcousticSimple.INDEX_VX] -= 0.125*(acousticEntry)*mixeddiffX[EquationsAcousticSimple.INDEX_VY] 
					+ 0.125*(advectiveEntry)*mixeddiffX[EquationsAcousticSimple.INDEX_P]; // unwesentlich
					//+ 0.125*U*(sgnV*U+sgnU*V)/(U+V)*mixeddiffX[EquationsAcousticSimple.INDEX_VX]; unstable
				//			;
			
			
			
			/*
			fluxRes[EquationsAcoustic.INDEX_VX] -= 0.125*sgnU*V*mixeddiffX[EquationsAcoustic.INDEX_VX] 
					 ;//+ 0.125*V/c*mixeddiffX[EquationsAcoustic.INDEX_P]; // unknown term
			if (includeAcousticOperator){
				fluxRes[EquationsAcoustic.INDEX_VX] -= 0.125*c*mixeddiffX[EquationsAcoustic.INDEX_VY];
			}
			fluxRes[EquationsAcoustic.INDEX_VY] -= 
					-0.125*sgnV*mixeddiffX[EquationsAcoustic.INDEX_VX] +
					0.125*sgnU*V*mixeddiffX[EquationsAcoustic.INDEX_VY]
					; // + 0.125*sgnU*mixeddiffX[EquationsAcoustic.INDEX_P];
			fluxRes[EquationsAcoustic.INDEX_P] -= 0.125*sgnU*V*mixeddiffX[EquationsAcoustic.INDEX_P]
					//+ 0.125*c*c*U*mixeddiffX[EquationsAcoustic.INDEX_VY]
					;//+ 0.125*c*V*mixeddiffX[EquationsAcoustic.INDEX_VX]; //*/
		}	
		
		return fluxRes;
	}

	
	
	@Override
	public String getDetailedDescription() {
		return "Roe' multi-d solver implemented in a way to avoid the vertex fluxes; augmented with vorticity-advective diffusion of the advective part";
	}
}
