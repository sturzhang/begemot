
public abstract class InitialDataAcoustic extends InitialData {

	protected EquationsAcoustic equations; 
	
	public InitialDataAcoustic(Grid grid, EquationsAcoustic equations) {
		super(grid);
		this.equations = equations;
	}

}
