
public class InitialDataHydroTriplePoint extends InitialDataHydro {

	public InitialDataHydroTriplePoint(Grid grid, EquationsHydro equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		double x, y;
		
		x = g.getX() - 1.0;
		y = g.getY() - grid.yMidpoint();

		res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;

		if ( x <= 0 ){
			res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(1.0, 0.0);	
		} else {
			if (y > 0 ){
				res[EquationsHydroIdeal.INDEX_RHO] = 0.1;
				res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(0.1, 0.0);
			} else {
				res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(0.1, 0.0);
			}
		}
		
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0; //*/

		return res;
		
	}

	@Override
	public String getDetailedDescription() {
		return "Triple point initialization: no velocity and values 0.1 and 1 of pressure and density";
	}

}
