import matrices.*;

public class EquationsMaxwell extends Equations {
	
	public static final int INDEX_EX = 0;
	public static final int INDEX_EY = 1; 
	public static final int INDEX_EZ = 2;
	public static final int INDEX_BX = 3;
	public static final int INDEX_BY = 4; 
	public static final int INDEX_BZ = 5;
	
	protected double lightspeed = 1.0;

	
	public EquationsMaxwell(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}

	public SquareMatrix getJacobian(int direction){
		double[][] J = new double[6][6];
		double c2 = lightspeed*lightspeed;
		
		if (direction == GridCartesian.X_DIR){ 
			J[1][5] =  c2;  J[2][4] = -c2;
			J[4][2] = -1.0; J[5][1] =  1.0;
		}
		if (direction == GridCartesian.Y_DIR){ 
			J[0][5] = -c2;  J[2][3] =  c2;
			J[3][2] =  1.0; J[5][0] = -1.0;
		}
		if (direction == GridCartesian.Z_DIR){ 
			J[0][4] =  c2;  J[1][3] = -c2;
			J[3][1] = -1.0; J[4][0] =  1.0;
		}
		
		return new SquareMatrix(J);
	}
	
	public double lightspeed(){ return lightspeed; }

	
	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		return getJacobian(direction).mult(quantities);
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		SquareMatrix[] mat = new SquareMatrix[3];
		double arrmat[][][] = new double[2][6][6];
		double[] eval = new double[6];
		
		double c = lightspeed;
		double cInv2 = 1.0/c/2;
		
		if (direction == GridCartesian.X_DIR){ 
			arrmat[0][0][1] = 1.0;
			arrmat[0][1][2] = -c;
			arrmat[0][1][4] = c;
			arrmat[0][2][3] = c;
			arrmat[0][2][5] = -c;
			arrmat[0][3][0] = 1.0;
			arrmat[0][4][3] = 1.0;
			arrmat[0][4][5] = 1.0;
			arrmat[0][5][2] = 1.0;
			arrmat[0][5][4] = 1.0;
			
			arrmat[1][0][3] = 1.0;
			arrmat[1][1][0] = 1.0;
			arrmat[1][2][1] = -cInv2;
			arrmat[1][2][5] = 0.5;
			arrmat[1][3][2] = cInv2;
			arrmat[1][3][4] = 0.5;
			arrmat[1][4][1] = cInv2;
			arrmat[1][4][5] = 0.5;
			arrmat[1][5][2] = -cInv2;
			arrmat[1][5][4] = 0.5;
		}
		if (direction == GridCartesian.Y_DIR){ 
			arrmat[0][0][2] = c;
			arrmat[0][0][4] = -c;
			arrmat[0][1][1] = 1.0;
			arrmat[0][2][3] = -c;
			arrmat[0][2][5] =  c;
			arrmat[0][3][3] = 1.0;
			arrmat[0][3][5] = 1.0;
			arrmat[0][4][0] = 1.0;
			arrmat[0][5][2] = 1.0;
			arrmat[0][5][4] = 1.0;

			arrmat[1][0][4] = 1.0;
			arrmat[1][1][1] = 1.0;
			arrmat[1][2][1] = cInv2;
			arrmat[1][2][5] = 0.5;
			arrmat[1][3][2] = -cInv2;
			arrmat[1][3][4] = 0.5;
			arrmat[1][4][0] = -cInv2;
			arrmat[1][4][5] = 0.5;
			arrmat[1][5][2] = cInv2;
			arrmat[1][5][3] = 0.5;
		}
		if (direction == GridCartesian.Z_DIR){ 
			arrmat[0][0][2] = -c;
			arrmat[0][0][4] = c;
			arrmat[0][1][3] =  c;
			arrmat[0][1][5] = -c;
			arrmat[0][2][1] = 1.0;
			arrmat[0][3][3] = 1.0;
			arrmat[0][3][5] = 1.0;
			arrmat[0][4][2] = 1.0;
			arrmat[0][4][4] = 1.0;
			arrmat[0][5][0] = 1.0;
			
			arrmat[1][0][5] = 1.0;
			arrmat[1][1][2] = 1.0;
			arrmat[1][2][0] = -cInv2;
			arrmat[1][2][4] = 0.5;
			arrmat[1][3][1] = cInv2;
			arrmat[1][3][3] = 0.5;
			arrmat[1][4][0] = cInv2;
			arrmat[1][4][4] = 0.5;
			arrmat[1][5][1] = -cInv2;
			arrmat[1][5][3] = 0.5;
		}
		
		eval[0] = 0;
		eval[1] = 0;
		eval[2] = -c;
		eval[3] = -c;
		eval[4] =  c;
		eval[5] =  c;
		
		
		mat[0] = new SquareMatrix(arrmat[0]);
		mat[1] = new DiagonalMatrix(eval);
		mat[2] = new SquareMatrix(arrmat[1]);

		//Matrix test  = mat[0].mult(mat[2]); // .sub(SquareMatrix.id(6))
		//Matrix test2 = mat[0].mult(mat[1]).mult(mat[2]).sub(getJacobian(direction));
		
		//System.err.println("-------------" + direction);
		//System.err.println("inverse:" + test);
		//System.err.println("jacobian:" + test2);
		
		return mat;	
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getNumberOfConservedQuantities() { return 6; }

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		for (int q = 0; q < getNumberOfConservedQuantities(); q++){
			res[q] = quantities[q];
		}
		
		double[] transformedE = transformVector(quantities[INDEX_EX], quantities[INDEX_EY], quantities[INDEX_EZ], normalVector);
		double[] transformedB = transformVector(quantities[INDEX_BX], quantities[INDEX_BY], quantities[INDEX_BZ], normalVector);
		
		res[INDEX_EX] = transformedE[0];
		res[INDEX_EY] = transformedE[1];
		res[INDEX_EZ] = transformedE[2];
		
		res[INDEX_BX] = transformedB[0];
		res[INDEX_BY] = transformedB[1];
		res[INDEX_BZ] = transformedB[2];
		
		return res;
	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		for (int q = 0; q < getNumberOfConservedQuantities(); q++){
			res[q] = quantities[q];
		}
		
		double[] transformedE = transformVectorBack(quantities[INDEX_EX], quantities[INDEX_EY], quantities[INDEX_EZ], normalVector);
		double[] transformedB = transformVectorBack(quantities[INDEX_BX], quantities[INDEX_BY], quantities[INDEX_BZ], normalVector);
		
		res[INDEX_EX] = transformedE[0];
		res[INDEX_EY] = transformedE[1];
		res[INDEX_EZ] = transformedE[2];
		res[INDEX_BX] = transformedB[0];
		res[INDEX_BY] = transformedB[1];
		res[INDEX_BZ] = transformedB[2];
		
		return res;
	}

	@Override
	public void transformFixedParameters(double[] normalVector) {
		// nothing
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// nothing 
	}

	@Override
	public String getDetailedDescription() {
		return "Maxwell equations";
	}

}
