
public class InitialDataHydroGreshoIsentropic extends InitialDataHydroVortex {

	protected InitialDataHydroGresho myGresho;
	protected double entropy, mach, rho0gammaMinus1;
	protected double gamma;
	
	public InitialDataHydroGreshoIsentropic(Grid grid, EquationsHydroIdeal equations, double mach, double entropy, double[] offsetSpeed, boolean addPressurePerturbation) {
		super(grid, equations, offsetSpeed, addPressurePerturbation);
		myGresho = new InitialDataHydroGresho(grid, equations, 1.0, 1.0, 1.0, offsetSpeed, addPressurePerturbation);
		this.mach = mach;
		this.entropy = entropy;
		
		// entropy is P/rho^gamma
		
		gamma = ((EquationsHydroIdeal) equations).gamma();
		rho0gammaMinus1 = (1.0/mach/mach - (gamma-1) * (myGresho.pressure(0.2) - myGresho.pressure(0))) / gamma/entropy;
	}

	@Override
	protected double speed(double r) {
		return myGresho.speed(r);
	}

	@Override
	protected double pressure(double r) {
		return entropy*Math.pow(rho(r), gamma);
	}

	@Override
	protected double rho(double r) {
		return Math.pow(
				(gamma - 1)/gamma/entropy * ( myGresho.pressure(r) - myGresho.pressure(0) ) + rho0gammaMinus1, 
				1.0/(gamma - 1) );
	}

	@Override
	public String getDetailedDescription() {
		return "Gresho-like velocity field with a constant entropy";
	}

}
