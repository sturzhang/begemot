
public class InitialDataHydroRayleighTaylor extends InitialDataHydro {

	protected double rhoBottom, rhoCeiling;
	protected double heightInitialPerturbation;
	protected double pressureCeiling;
	protected double gy;
	
	public InitialDataHydroRayleighTaylor(Grid grid, EquationsHydro equations, 
			double rhoBottom, double rhoCeiling, double pressureCeiling, double heightInitialPerturbation, 
			double gy) {
		super(grid, equations);
		this.rhoBottom = rhoBottom;
		this.rhoCeiling = rhoCeiling;
		this.pressureCeiling = pressureCeiling;
		this.gy = gy;
		this.heightInitialPerturbation = heightInitialPerturbation;
	}

    public String getDetailedDescription(){
    	return "Rayleigh-Taylor onset data with density = " + rhoCeiling + " over " + rhoBottom + 
    			" with an initial perturbation of height " + heightInitialPerturbation + 
    			" and a hydrostatic pressure with its vaue at the top = " + pressureCeiling;
    }

	
	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		double interfacePosition;
		double pressure, pressureconst;
		
		interfacePosition = grid.yMidpoint() + 
				heightInitialPerturbation * Math.cos((g.getX() - grid.xMidpoint()) / grid.xMidpoint() * Math.PI);
				
				
		pressureconst = pressureCeiling - rhoBottom * gy * interfacePosition + rhoCeiling * gy * (grid.ymax() - interfacePosition) + 10.0;
		
		if (g.getY() > interfacePosition){
			res[EquationsHydroIdeal.INDEX_RHO] = rhoCeiling;
			pressure = rhoBottom * gy * interfacePosition + rhoCeiling * gy * (g.getY() - interfacePosition) + pressureconst;
		} else {
			res[EquationsHydroIdeal.INDEX_RHO] = rhoBottom;
			pressure = rhoBottom * gy * g.getY() + pressureconst;  
		}
    				
		
    				
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
				((EquationsHydroIdeal) equations).getEnergy(
    								pressure, 0.0);
 
		res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
		
		for (int s = 5; s < qq; s++){
			if (g.getY() > interfacePosition){
				res[s] = -1.0;
			} else {
				res[s] = 1.0;
			}
		}
		
		
		return res;
	}

}
