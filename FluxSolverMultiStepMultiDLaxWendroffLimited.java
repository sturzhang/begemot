import matrices.SquareMatrix;


public class FluxSolverMultiStepMultiDLaxWendroffLimited extends
		FluxSolverMultiStepMultiDCartesianSecondOrder {

	public FluxSolverMultiStepMultiDLaxWendroffLimited(
			GridCartesian grid, Equations equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	protected double limiter(double rightSlope, double leftSlope, double centralSlope, double speed){
		//return (speed > 0 ? rightSlope : leftSlope);
		
		//return 0;

		/*if ((rightSlope > 0) && (leftSlope > 0) && (centralSlope > 0)){
			return (speed > 0 ? rightSlope : leftSlope); // inverse upwind
		} else if ((rightSlope < 0) && (leftSlope < 0) && (centralSlope < 0)){
			return (speed > 0 ? rightSlope : leftSlope); // inverse upwind
		} else {
			return 0;
		}//*/

		
		/*if ((rightSlope > 0) && (leftSlope > 0) && (centralSlope > 0)){
			return Math.min(Math.min(rightSlope, leftSlope), centralSlope);
		} else if ((rightSlope < 0) && (leftSlope < 0) && (centralSlope < 0)){
			return Math.max(Math.max(rightSlope, leftSlope), centralSlope);
		} else {
			return 0;
		}//*/
		
		if ((rightSlope > 0) && (leftSlope > 0) && (centralSlope > 0)){
			return Math.min((speed > 0 ? rightSlope : leftSlope), centralSlope);
		} else if ((rightSlope < 0) && (leftSlope < 0) && (centralSlope < 0)){
			return Math.max((speed > 0 ? rightSlope : leftSlope), centralSlope);
		} else {
			return 0;
		}//*/
	}
	
	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LLTT, double[] LLT, double[] LL, double[] LLB,
			double[] LLBB, double[] LTT, double[] LT, double[] L, double[] LB,
			double[] LBB, double[] RTT, double[] RT, double[] R, double[] RB,
			double[] RBB, double[] RRTT, double[] RRT, double[] RR,
			double[] RRB, double[] RRBB) {

		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		int quantities = equations.getNumberOfConservedQuantities();
		
		double[] diff    = new double[quantities];
		double[] centralFlux = new double[quantities];
		double[] central = new double[quantities];
		double[] slope    = new double[quantities];
		double[] slopeRight = new double[quantities];
		double[] slopeAverage = new double[quantities];
		double[] slopeDiff = new double[quantities];
		
		double[] flux1d = new double[quantities];
		double[] flux2d = new double[quantities];
		
		SquareMatrix[] mat = equations.diagonalization(i, j, k, mean(L, R), direction);
		SquareMatrix Jx = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		SquareMatrix absJx = mat[0].mult(mat[1].absElementwise()).mult(mat[2]).toSquareMatrix();
		
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, L, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, R, direction);
		for (int q = 0; q < quantities; q++){ 
			diff[q] = R[q] - L[q]; 
			centralFlux[q] = 0.5*(fluxLeft[q] + fluxRight[q]);
			central[q] = 0.5*(L[q] + R[q]);
		}
		
		/*double[] charLL = mat[2].mult(LL);
		double[] charL  = mat[2].mult(L );
		double[] charR  = mat[2].mult(R );
		double[] charRR = mat[2].mult(RR);
		
		for (int q = 0; q < quantities; q++){
			slope[q]      = limiter(charR[q] - charL[q], charL[q] - charLL[q], (charR[q] - charLL[q])/2, mat[1].value(q, q));
			slopeRight[q] = limiter(charRR[q] - charR[q], charR[q] - charL[q], (charRR[q] - charL[q])/2, mat[1].value(q, q));
		}
		slope      = mat[0].mult(slope);
		slopeRight = mat[0].mult(slopeRight);*/
		
		
		for (int q = 0; q < quantities; q++){
			slope[q]      = limiter(R[q] - L[q], L[q] - LL[q], (R[q] - LL[q])/2, mat[1].value(q, q));
			slopeRight[q] = limiter(RR[q] - R[q], R[q] - L[q], (RR[q] - L[q])/2, mat[1].value(q, q));
		}
		
		for (int q = 0; q < quantities; q++){
			slopeAverage[q] = 0.5*(slope[q] + slopeRight[q]);
			slopeDiff[q] = slopeRight[q] - slope[q];
		}
		
		
		double waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		double[] term1 = absJx.mult(0.5).mult(diff);
		double[] term2 = absJx.mult(0.5).mult(slopeAverage);
		double[] term3 = Jx.mult(0.25).mult(slopeDiff);
		for (int q = 0; q < quantities; q++){
			flux1d[q] = centralFlux[q] - term1[q] + term2[q] - term3[q];
		}
		
		term1 = absJx.mult(Jx).mult(0.25).mult(slopeDiff);
		term2 = Jx.mult(Jx).mult(0.5).mult(slopeAverage);
		
		for (int q = 0; q < quantities; q++){
    		flux2d[q] = term1[q] - term2[q];
    		
    		if ((Double.isNaN(flux2d[q])) || (Double.isNaN(flux1d[q]))){
    	    	System.err.println("fluxes are NaN");
    	    }
    	}
		
		return new FluxMultiStepAndWaveSpeed(flux1d, flux2d, waveSpeed);

		
	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep) {
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (dir == GridCartesian.X_DIR){ // we don't know which one to divide by dx, and which by dy
				return dt/((GridCartesian) grid).xSpacing();
			} else if (dir == GridCartesian.Y_DIR){
				return dt/((GridCartesian) grid).xSpacing();
			} else {
				System.err.println("ERROR: asked for non-x, non-y direction");
				return 0.0;
			}
		}
	}

	@Override
	public int steps() { return 2; }


	@Override
	public String getDetailedDescription() {
		return "Lax-Wendroff-type method with slope limiting";
	}

	
}
