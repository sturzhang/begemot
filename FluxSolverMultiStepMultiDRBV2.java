import matrices.SquareMatrix;


public class FluxSolverMultiStepMultiDRBV2 extends FluxSolverMultiStepMultiDCartesianFirstOrder {

	protected double[][][][][][] average;
	protected boolean[][][] averagesSet;
	protected SourceRBV2 source;
	
	public FluxSolverMultiStepMultiDRBV2(GridCartesian grid, Equations equations, SourceRBV2 source) {
		super(grid, equations);
		this.source = source;
		
		average = new double[2][][][][][];
		for (int i = 0; i < average.length; i++){ average[i] = grid.generateEmptyFluxArray(equations.getNumberOfConservedQuantities()); }
		averagesSet = source.getInitializedAveragesSet(average[0].length, average[0][0].length, average[0][0][0].length, false);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LT, double[] L, double[] LB,
			double[] RT, double[] R, double[] RB, int timeIntegratorStep) {
		
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		int otherDirection = GridCartesian.Y_DIR;
		
		int indexOfThisEdge = -1;
		GridEdgeMapped[] allEdges = grid.getAllEdges(i, j, k);
		for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
			if (gridEdge == allEdges[dir]){ indexOfThisEdge = dir; break; }
		}
		if (indexOfThisEdge == -1){
			System.err.println("ERROR: Edge not found in list!");
		}
		
		int quantities = equations.getNumberOfConservedQuantities();
		double[] flux1 = new double[quantities];
		double[] flux2 = new double[quantities];
		
		
		double[] diff    = new double[quantities];
		double[] perp    = new double[quantities];
		double[] central = new double[quantities];
		double[] upwindingTermX, upwindingTermY;
		
		
		
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, L, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, R, direction);
		
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, LT, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, LB, direction);
		
		double[] fluxRightTop = equations.fluxFunction(i, j, k, RT, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, RB, direction);
		
		double verticalAverageFluxLeft, verticalAverageFluxRight;

		
		SquareMatrix[] mat = equations.diagonalization(i, j, k, mean(L, R), direction);
		
		double waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				//System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		
				
		if (timeIntegratorStep % 2 == 1){
			for (int q = 0; q < quantities; q++){ 
				verticalAverageFluxLeft = 0.25*(fluxLeftTop[q] + 2.0*fluxLeft[q] + fluxLeftBottom[q]);
				verticalAverageFluxRight = 0.25*(fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q]);
				central[q] = 0.5*(verticalAverageFluxLeft + verticalAverageFluxRight);
			}
					
			flux1 = central; // and sources
					// flux2 = 0
		} else {
			double[] averageCurrent = new double[quantities];
		
			for (int q = 0; q < quantities; q++){ 
				verticalAverageFluxLeft = 0.25*(fluxLeftTop[q] + 2.0*fluxLeft[q] + fluxLeftBottom[q]);
				verticalAverageFluxRight = 0.25*(fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q]);
				diff[q] = verticalAverageFluxRight - verticalAverageFluxLeft; 
				perp[q] = 0.25*(fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q]);
				
				averageCurrent[q] = 0.5*(0.25*(LT[q] + 2.0*L[q] + LB[q]) + 0.25*(LT[q] + 2.0*L[q] + LB[q]) );
				
				if (timeIntegratorStep == 2){
					if (averagesSet[i][j][k]){
						average[0][i][j][k][indexOfThisEdge][q] = average[1][i][j][k][indexOfThisEdge][q];
						averagesSet[i][j][k] = true;
					} else { // is this the correct choice?
						average[0][i][j][k][indexOfThisEdge][q] = averageCurrent[q];
					}
					average[1][i][j][k][indexOfThisEdge][q] = averageCurrent[q];
				}
			}
			
			
			
			SquareMatrix Jx = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
			
			mat = equations.diagonalization(i, j, k, mean(L, R), otherDirection);
			SquareMatrix Jy = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
			
			upwindingTermX = Jx.mult(0.5).mult(diff);
			upwindingTermY = Jy.mult(0.5).mult(perp);
		
			double[] previousTimes = new double[quantities];
			double[] averagePrevious = average[1][i][j][k][indexOfThisEdge];
			double[] averageOld      = average[0][i][j][k][indexOfThisEdge];
			for (int q = 0; q < quantities; q++){
				previousTimes[q] = 1.5*averageCurrent[q] - 2.0*averagePrevious[q] + 0.5*averageOld[q];
			}
			
			
			
			double[] upwindingTermPreviousTimes = Jx.mult(0.5).mult(previousTimes);
			
			for (int q = 0; q < quantities; q++){
				flux1[q] = - upwindingTermX[q] - upwindingTermY[q];
				flux2[q] = - upwindingTermPreviousTimes[q];
				if ((Double.isNaN(flux2[q])) || (Double.isNaN(flux1[q]))){
					//System.err.println("fluxes are NaN");
				}
			}
		}
			
		return new FluxMultiStepAndWaveSpeed(flux1, flux2, waveSpeed);
		
	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt,
			int fluxSolverStep) {
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (dir == GridCartesian.X_DIR){
				return ((GridCartesian) grid).xSpacing() / dt;
			} else if (dir == GridCartesian.Y_DIR){
				return ((GridCartesian) grid).xSpacing() / dt;
			} else {
				System.err.println("ERROR: asked for non-x, non-y direction");
				return 0.0;
			}
		}
	}

	@Override
	public int steps() { return 2; }

	@Override
	public String getDetailedDescription() {
		return "RBV2 scheme of Lerat et al as implemented in Seligman Laughlin 2017";
	}

}
