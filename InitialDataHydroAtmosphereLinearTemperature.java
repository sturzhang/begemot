
public class InitialDataHydroAtmosphereLinearTemperature extends InitialDataHydroAtmosphere {

	protected double temperatureGradient;
	
	public InitialDataHydroAtmosphereLinearTemperature(GridCartesian grid,
			EquationsHydroIdeal equations, double gy, double xVelocity, boolean randomPerturbation, double temperatureGradient) {
		super(grid, equations, gy, xVelocity, randomPerturbation);
		this.temperatureGradient = temperatureGradient;
	}
	
	public InitialDataHydroAtmosphereLinearTemperature(GridCartesian grid,
			EquationsHydroIdealWith1dDeriv equations, double gy, double xVelocity, boolean randomPerturbation, double temperatureGradient) {
		super(grid, equations, gy, xVelocity, randomPerturbation);
		this.temperatureGradient = temperatureGradient;
	}
	
	
	@Override
	public String getDetailedDescription() {
		return "Linear-temperature atmosphere from Miczek's thesis.";
	}

	@Override
	public double getRho(double y) {
		return getPressure(y) / getTemperature(y);
	}

	@Override
	public double getPressure(double y) {
		double p = Math.pow(getTemperature(y), gy/temperatureGradient);
		
		return p;
	}

	public double getTemperature(double y){
		return 1.0 + temperatureGradient * y;
	}
	
	public double temperatureGradient(){ return temperatureGradient; }

	@Override
	public double getRhoDeriv(double y) {
		return (getTemperature(y)*getPressureDeriv(y) - getPressure(y)*getTemperatureDeriv(y)) 
				/ getTemperature(y) / getTemperature(y);
	}

	public double getTemperatureDeriv(double y){
		return temperatureGradient;
	}
	
	@Override
	public double getPressureDeriv(double y) {
		return gy/temperatureGradient * Math.pow(getTemperature(y), gy/temperatureGradient-1) * getTemperatureDeriv(y);
	}

	
}
