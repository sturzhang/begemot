import matrices.*;

public class FluxSolverMiczek extends FluxSolverRoeHydro {

	protected double machCutoff;
	
	public FluxSolverMiczek(Grid grid, EquationsHydroIdeal equations,
			int meanType, double machCutoff) {
		super(grid, equations, meanType);
		this.machCutoff = machCutoff;
	}
		
	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		double[][] mat = new double[5][5];
		HydroState state = new HydroState(quantities, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		double machParameter = 1.0; // equations.mach(); does not work fully yet
		
		double rho = 1.0;
		double v2 = state.v2;
		double c = state.soundSpeed;
	    double c2 = c*c;
	    double vX = state.vX; 
	    double vY = state.vY; 
	    double vZ = state.vZ;
	    
	    
	    double machLocal = Math.sqrt(v2) / c;
	    double limitedMach;

	    
	    if (machLocal > 1.0){
	    	limitedMach = 1.0;
	    } else {
	    	if (machLocal > machCutoff){
	    		limitedMach = machLocal; 
	    	} else {
	    		limitedMach = machCutoff;
	    	}
	    }
	    
		double delta = 1.0/limitedMach - 1;  //cutoffMach  
		
		double waveSpeed = c/machParameter + Math.sqrt(v2);
		
		
		if (direction == GridCartesian.X_DIR){
			double m1n = (vX >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vX*vX*machParameter*machParameter);
			
			mat[0][0] = m1n * vX;
			//mat[0][1] = -1.0;
			mat[0][1] = (rho * (-c2 * delta + c * vX * machParameter + delta*vX*vX*machParameter*machParameter)) / (c * tau) ;
			mat[0][2] = 0.0;
			mat[0][3] = 0.0;
			mat[0][4] = -m1n*vX/c2 + 1.0/tau/machParameter;
							
			mat[1][0] = 0.0;
			mat[1][1] = c2/tau/machParameter;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = (c*c2*delta + machParameter*tau*tau*vX) / 
					 (c2*machParameter*machParameter*rho*tau + c * delta*machParameter*machParameter*machParameter*rho*tau*vX);
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = m1n*vX;
			mat[2][3] = 0.0;
			mat[2][4] = 0.0;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = m1n*vX;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = (c*rho * (-c2*delta + c*vX*machParameter + delta*vX*vX*machParameter*machParameter)) / tau;
			mat[4][2] = 0.0;
			mat[4][3] = 0.0;
			mat[4][4] = c2 / machParameter / tau;
		}
		if (direction == GridCartesian.Y_DIR){
			double m1n = (vY >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vY*vY*machParameter*machParameter);
			
			mat[0][0] = m1n * vY;
			mat[0][1] = 0.0;
			//mat[0][2] = -1.0; 
			mat[0][2] = (rho * (-c2 * delta + machParameter*c*vY + delta*vY*vY*machParameter*machParameter)) / (c * tau);
			mat[0][3] = 0.0;
			mat[0][4] = -m1n*vY/c2+1.0/machParameter/tau;
			
			mat[1][0] = 0.0;
			mat[1][1] = m1n*vY;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = 0.0;
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = c2/tau/machParameter;
			mat[2][3] = 0.0;
			mat[2][4] = (c*c2*delta + machParameter*tau*tau*vY) / 
					(c2 * machParameter*machParameter * rho * tau + c * delta * machParameter*machParameter*machParameter * rho * tau * vY);
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = m1n*vY;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = 0.0;
			mat[4][2] = (c*rho * (-c2*delta + machParameter*c*vY + delta*vY*vY*machParameter*machParameter)) / tau;
			mat[4][3] = 0.0;
			mat[4][4] = c2 / machParameter / tau;
		}
		
		double dUdVarr[][] = new double[5][5];
		double dVdUarr[][] = new double[5][5];
		
		dUdVarr[0][0] = 1.0;
		dUdVarr[1][0] = vX;
		dUdVarr[1][1] = rho;
		dUdVarr[2][0] = vY;
		dUdVarr[2][2] = rho;
		dUdVarr[3][0] = vZ;
		dUdVarr[3][3] = rho;
		dUdVarr[4][0] = machParameter*machParameter * v2/2;
		dUdVarr[4][1] = machParameter*machParameter * rho*vX;
		dUdVarr[4][2] = machParameter*machParameter * rho*vY;
		dUdVarr[4][3] = machParameter*machParameter * rho*vZ;
		dUdVarr[4][4] = 1.0/(((EquationsHydroIdeal) equations).gamma() - 1);
		
		dVdUarr[0][0] = 1.0;
		dVdUarr[1][0] = -vX/rho;
		dVdUarr[1][1] = 1.0/rho;
		dVdUarr[2][0] = -vY/rho;
		dVdUarr[2][2] = 1.0/rho;
		dVdUarr[3][0] = -vZ/rho;
		dVdUarr[3][3] = 1.0/rho;
		dVdUarr[4][0] = machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*v2/2;
		dVdUarr[4][1] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vX;
		dVdUarr[4][2] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vY;
		dVdUarr[4][3] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vZ;
		dVdUarr[4][4] = (((EquationsHydroIdeal) equations).gamma() - 1);

		SquareMatrix dUdV = new SquareMatrix(dUdVarr);
		SquareMatrix dVdU = new SquareMatrix(dVdUarr);

		// TODO replace here by calling ((EquationsHydroIdeal) equations).primToCons();
		
		return new UpwindingMatrixAndWaveSpeed(
				dUdV.mult(new SquareMatrix(mat)).mult(dVdU).toSquareMatrix(),
				waveSpeed);
	}

		
}

	

