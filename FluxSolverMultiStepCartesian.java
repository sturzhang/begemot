
public abstract class FluxSolverMultiStepCartesian extends FluxSolverFromConservativeOnly {

	public FluxSolverMultiStepCartesian(GridCartesian grid, Equations equations, StencilTurner stencilTurner) {
		super(grid, equations, stencilTurner);
	}

	public abstract double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep);
	
	@Override
	public double[][][][][] sum( double[][][][][][] interfaceFluxes, double dt, double[][][][][] summedInterfaceFluxes, double[][][][] localWaveSpeeds, double [][][][][] additionalInterfaceQuantities){
		double factor = 0.0;
		GridEdgeMapped[] allEdges;
		double[] normal;
    	int direction = 0; // to make compiler happy
		int i,j,k;
    	
		if (interfaceFluxes.length == 1){
			summedInterfaceFluxes = interfaceFluxes[0];
		} else { 

			for (int fluxSolverStep = 0; fluxSolverStep < interfaceFluxes.length; fluxSolverStep++){
				for (GridCell g : grid){
					i = g.i(); j = g.j(); k = g.k();
					allEdges = grid.getAllEdges(i, j, k);
							
					for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
						normal = allEdges[dir].normalVector();
						if (normal[0] != 0){
							direction = GridCartesian.X_DIR;
						} else if (normal[1] != 0){
							direction = GridCartesian.Y_DIR;
						} else if (normal[2] != 0){
							direction = GridCartesian.Z_DIR;
						}
						
						
						factor = prefactor(direction, localWaveSpeeds[i][j][k][GridCartesian.X_DIR],
									                localWaveSpeeds[i][j][k][GridCartesian.Y_DIR], dt, fluxSolverStep);
						
						for (int q = 0; q < interfaceFluxes[0][i][j][k][dir].length; q++){
							if (fluxSolverStep == 0){
								summedInterfaceFluxes[i][j][k][dir][q] = 0.0;
							}
							
							summedInterfaceFluxes[i][j][k][dir][q] += factor * interfaceFluxes[fluxSolverStep][i][j][k][dir][q];
						}
						
						
					}
				}
			}
	

		}
		
		return summedInterfaceFluxes;
	}

	public double[] mean(double[] left, double[] right){
		double[] res = new double[left.length];
		
		for (int i = 0; i < res.length; i++){
			res[i] = 0.5 * (left[i] + right[i]);
		}
		return res;
	}
	
}
