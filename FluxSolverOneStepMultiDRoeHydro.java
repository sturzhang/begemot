import matrices.SquareMatrix;


public class FluxSolverOneStepMultiDRoeHydro extends FluxSolverOneStepMultiDRoe {

	protected int meanType;
	
	public FluxSolverOneStepMultiDRoeHydro(GridCartesian grid, EquationsHydroIdeal equations, int meanType) {
		super(grid, equations);
		this.meanType = meanType;
	}
	
	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] left, double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		UpwindingMatrixAndWaveSpeed res = usualSolver.getAveragedUpwindingMatrix(i, j, k, left, right, direction); 
		
		/* unnecessary double absMeanU = Math.abs(left[EquationsHydroIdeal.INDEX_XMOM]/left[EquationsHydroIdeal.INDEX_RHO] + 
				right[EquationsHydroIdeal.INDEX_XMOM]/right[EquationsHydroIdeal.INDEX_RHO])/2;
		for (int e = 0; e < left.length; e++){
			res.matrix().setElement(e, e, res.matrix().value(e, e) + absMeanU);
		}*/
		return res;
	}

	protected SquareMatrix getOrthogonalContribution(double[] left,
			double[] right, int direction) {
		int otherdirection;
		// multiple inheritance -- workaround
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		SquareMatrix res;
		
		/*SquareMatrix JxSign = usualSolver.getUpwindingMatrix(left, right, direction, true);
		SquareMatrix Jy;
		// get jacobian for x and y directions!
		// jy gets \not direction
		
		if (direction == Grid.X_DIR){ otherdirection = Grid.Y_DIR; } else { otherdirection = Grid.X_DIR; }
		Jy = usualSolver.getJacobian(left, right, otherdirection);
		
		res = JxSign.mult(Jy).toSquareMatrix();
		if (direction == Grid.X_DIR){ 
			res.setElement(2, 4, 0.0); 
			res.setElement(1, 0, 0.0); 
			res.setElement(1, 4, 0.0); 
		} else { 
			res.setElement(1, 4, 0.0); 
			res.setElement(2, 0, 0.0); 
			res.setElement(2, 4, 0.0); 
		}
		return res;
		//*/
		
		
		
		double[][] mat = new double[5][5];
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				mat[i][j] = 0.0;
			}
		}
		
		HydroState le = new HydroState(left, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		
		double cMean, vMean, vMeanOther, vMeanOtherAbs;
				
		//cMean = usualSolver.roeMean(le.rho, ri.rho, le.soundSpeed, ri.soundSpeed);
		//vMean = usualSolver.roeMean(le.rho, ri.rho, le.vX, ri.vX);
		//vMeanOther = usualSolver.roeMean(le.rho, ri.rho, le.vY, ri.vY);
		
		
		HydroState mean = new HydroState(usualSolver.mean(left, right), EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		cMean = mean.soundSpeed;
		vMean = mean.vX;
		vMeanOther = mean.vY;
		
		
		vMeanOtherAbs = Math.abs(vMeanOther);
		if (direction == GridCartesian.X_DIR){
			
			mat[1][2] = cMean;
			mat[1][0] = -cMean*vMeanOther;
			
			// new, inspired by analysis in primitve variables:
			//mat[1][2] = cMean - vMeanOtherAbs;
			//mat[1][0] = (vMeanOtherAbs-cMean)*vMeanOther;
			//mat[4][0] = (vMeanOtherAbs-cMean)*vMean*vMeanOther; 
			//mat[4][2] = -(vMeanOtherAbs-cMean)*vMean;
			
			//additional:
			/*mat[1][1] = vMean*sign(vMeanOther);
			mat[4][1] = cMean*cMean*sign(vMeanOther); //*/
			// new:
			/*mat[4][2] = vMean*cMean;
			mat[2][4] = vMean/cMean;//*/
			
			// new changes looking at vorticity
			//mat[2][1] = -Math.abs(vMean);
			//mat[2][1] = -Math.sqrt(vMean*vMean + vMeanOther*vMeanOther);
			
		//	mat[2][4] = 1.0;
		}

		
		// remove this if (direction == GridCartesian.Y_DIR){
		//	vMean = usualSolver.roeMean(le.rho, ri.rho, le.vY, ri.vY);
		//	vMeanOther = usualSolver.roeMean(le.rho, ri.rho, le.vX, ri.vX);
		//
		//	mat[2][1] = cMean;
		//	
		//	//additional:
		//	/*mat[2][2] = vMean*sign(vMeanOther);
		//	mat[4][2] = cMean*cMean*sign(vMeanOther);//*/
		//	// new:
		//	/*mat[4][1] = vMean*cMean;
		//	mat[1][4] = vMean/cMean;//*/
		//	
		//	
		//	//	mat[1][4] = 1.0;
		//}
		
		
		return new SquareMatrix(mat); //*/
		
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		return usualSolver.averageFlux(i, j, k, left, right, direction);		
	}

	public String getDetailedDescription() {
		return "Multi-d solver based on an extension of the Roe solver for ideal gas.";
	}



	@Override
	protected SquareMatrix getUpwindOrthogonalContribution(double[] left,
			double[] right, int direction) {
		int otherdirection;
		// multiple inheritance -- workaround
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		SquareMatrix res;
		
		
		double[][] mat = new double[5][5];
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				mat[i][j] = 0.0;
			}
		}
		
		HydroState le = new HydroState(left, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		
		double cMean, vMean, vMeanOther;
				
		cMean = usualSolver.roeMean(le.rho, ri.rho, le.soundSpeed, ri.soundSpeed);
		
		
		if (direction == GridCartesian.X_DIR){
			vMean = usualSolver.roeMean(le.rho, ri.rho, le.vX, ri.vX);
			vMeanOther = usualSolver.roeMean(le.rho, ri.rho, le.vY, ri.vY);
			
			mat[4][2] = cMean*cMean;
			
			
			// anticommutator solution 
			/*mat[1][1] = cMean*sign(vMeanOther);
			mat[1][2] = vMean + cMean*sign(vMean);
			mat[1][4] = vMean*sign(vMeanOther)/cMean;
			
			mat[2][1] = vMean;
			mat[2][4] = Math.abs(vMean)/cMean;
			
			mat[4][1] = cMean*vMean*sign(vMeanOther);
			mat[4][2] = cMean*cMean + cMean*Math.abs(vMean);
			mat[4][4] = cMean*sign(vMeanOther); //*/
		}
		if (direction == GridCartesian.Y_DIR){
			vMean = usualSolver.roeMean(le.rho, ri.rho, le.vY, ri.vY);
			vMeanOther = usualSolver.roeMean(le.rho, ri.rho, le.vX, ri.vX);

			mat[4][1] = cMean*cMean;
						
			// anticommutator solution
			/*mat[2][2] = cMean*sign(vMeanOther);
			mat[2][1] = vMean + cMean*sign(vMean);
			mat[2][4] = vMean*sign(vMeanOther)/cMean;
			
			mat[1][2] = vMean;
			mat[1][4] = Math.abs(vMean)/cMean;
			
			mat[4][2] = cMean*vMean*sign(vMeanOther);
			mat[4][1] = cMean*cMean + cMean*Math.abs(vMean);
			mat[4][4] = cMean*sign(vMeanOther); //*/
		}
		
		
		return new SquareMatrix(mat); 
	}
	


}
