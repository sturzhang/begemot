
public class OutputErrorNorm extends Output {

	protected int exponent;
	protected InitialData initial;
	protected double[] errorNorm;	 // filled after computation has been finished
	
	public OutputErrorNorm(double outputTimeInterval, GridCartesian grid,
			Equations equations, InitialData initial, int exponent) {
		super(outputTimeInterval, grid, equations);
		this.initial = initial;
		this.exponent = exponent;
	}

	@Override
	public String getDetailedDescription() {
		return "prints of the L" + exponent + " error (w.r.t. the initial data) norms of the conserved quantities the maximum one" + 
				" over the grid x-spacing.";
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}
	
	public double[] errorNorm(){
		return errorNorm;
	}
	
	protected double[] getExactSolution(int i, int j, int k, int numberOfConservedQuantities, double time){
		return initial.getInitialValue(new GridCell(i, j, k, grid), numberOfConservedQuantities);
	}
	
	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		computeError(time, quantities);
		String res = time + " " + ((GridCartesian) grid).xSpacing();
		for (int q = 0; q < errorNorm.length; q++) {
			res += " " + errorNorm[q];
		}
		println(res);
	}
	
	protected void computeError(double time, double[][][][] quantities) {
		double[] error;
		double[] init;
		int numberOfConservedQuantities = quantities[0][0][0].length;
		
		
		error = new double[quantities[0][0][0].length];
		
		if (grid.dimensions() == 1){
			for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
				init = getExactSolution(i, 0, 0, numberOfConservedQuantities, time);
				for (int q = 0; q < quantities[i][0][0].length; q++){
					error[q] += Math.pow(Math.abs(quantities[i][0][0][q] - init[q]), exponent);
				}
			}
		}
		if (grid.dimensions() == 2){
			for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
				for (int j = ((GridCartesian) grid).indexMinY(); j <= ((GridCartesian) grid).indexMaxY(); j++){
					init = getExactSolution(i, j, 0, numberOfConservedQuantities, time);

					//System.err.println(i + " " + j + " " + quantities[i][j][0][0] + " " + init[0] + " " + (quantities[i][j][0][1] - init[1]));
					for (int q = 0; q < quantities[i][j][0].length; q++){
						
						error[q] += Math.pow(Math.abs(quantities[i][j][0][q] - init[q]), exponent);
						/*error[q] += Math.pow(Math.abs(quantities[i][j][0][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j][0][EquationsHydroIdeal.INDEX_RHO]
								- init[EquationsHydroIdeal.INDEX_XMOM]/init[EquationsHydroIdeal.INDEX_RHO]), exponent); //*/
					}
				}
			}
		}
		if (grid.dimensions() == 3){ 
			for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
				for (int j = ((GridCartesian) grid).indexMinY(); j <= ((GridCartesian) grid).indexMaxY(); j++){
					for (int k = ((GridCartesian) grid).indexMinZ(); k < ((GridCartesian) grid).indexMaxZ(); k++){
						
						init = getExactSolution(i, j, k, numberOfConservedQuantities, time);
						for (int q = 0; q < quantities[i][j][k].length; q++){
							error[q] += Math.pow(Math.abs(quantities[i][j][k][q] - init[q]), exponent);
						}
					}
				}
			}			
		}

		
		
		for (int q = 0; q < error.length; q++) {
			if (grid.dimensions() == 1){ error[q] *= ((GridCartesian) grid).xSpacing(); }
			if (grid.dimensions() == 2){ error[q] *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing(); }
			if (grid.dimensions() == 3){ error[q] *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing()*((GridCartesian) grid).zSpacing(); }
			
			error[q] = Math.pow(error[q], 1.0/exponent);
		}
		
		
				
		this.errorNorm = error;
	}

	

}
