import matrices.SquareMatrix;


public class FluxSolverRoeEntropyFixHartenHyman extends FluxSolverRoe {

	public FluxSolverRoeEntropyFixHartenHyman(Grid grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected SquareMatrix getAbsoluteValueOfEigenvalues(SquareMatrix eigenvalues,
			double[] left, double[] right, int ii, int jj, int kk, int direction){
		double delta;
		SquareMatrix leftEval  = equations.diagonalization(ii, jj, kk, left,  direction)[1];
		SquareMatrix rightEval = equations.diagonalization(ii, jj, kk, right, direction)[1];
		double absEval;
		SquareMatrix res = new SquareMatrix(eigenvalues.rows());
		
		// for my version:
		double epsilon, n = 4.0;
		
		for (int k = 0; k < eigenvalues.rows(); k++){
			absEval = Math.abs(eigenvalues.value(k,k));
			delta = Math.max(Math.max(0, eigenvalues.value(k, k) - leftEval.value(k, k)),
						rightEval.value(k,k) - eigenvalues.value(k, k));
			if (absEval < delta){
				
				// HH1:
				res.setElement(k, k, delta);
				
				// HH2
				// not correctly implemented
				//res.setElement(k, k, 0.5*(absEval*absEval / delta + delta));
				
				// modified version by myself
				/*epsilon = 2.0*delta;
				res.setElement(k, k, delta + epsilon/n*Math.pow(absEval / epsilon, n)); //*/
			} else {
				res.setElement(k, k, absEval);
			}
		}
		return res;
	}
	
}
