
public class OutputPressureGradientsAndDivergenceForLowMach extends Output {

	public OutputPressureGradientsAndDivergenceForLowMach(
			double outputTimeInterval, GridCartesian2D grid, EquationsHydroIdeal equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints pressure gradient and vertex-based divergence";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		int i,j,k;
		double gradxp = 0, gradyp = 0, divergence = 0;
		double uxx = 0, uyy = 0, vxx = 0, vyy = 0;
		EquationsHydroIdeal myEquations = (EquationsHydroIdeal) equations;
		
		int counter = 0;
		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			gradxp += Math.abs((myEquations.getPressureFromConservative(quantities[i+1][j][k]) 
					- myEquations.getPressureFromConservative(quantities[i-1][j][k]) )/2);
			gradyp += Math.abs((myEquations.getPressureFromConservative(quantities[i][j+1][k]) 
					- myEquations.getPressureFromConservative(quantities[i][j-1][k]) )/2);
			
			divergence += Math.abs((quantities[i+1][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
						- quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]
						+ quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
						- quantities[i][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
						+
						quantities[i][j+1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
						- quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]
						+ quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
						- quantities[i+1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
						));

			uxx += Math.abs((quantities[i+1][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
					 - 2.0*quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] 
							 + quantities[i-1][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i-1][j][k][EquationsHydroIdeal.INDEX_RHO]));
			uyy += Math.abs((quantities[i][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
					 - 2.0*quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] 
							 + quantities[i][j-1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j-1][k][EquationsHydroIdeal.INDEX_RHO]));
			vxx += Math.abs((quantities[i+1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
					 - 2.0*quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] 
							 + quantities[i-1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i-1][j][k][EquationsHydroIdeal.INDEX_RHO]));
			vyy += Math.abs((quantities[i][j+1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
					 - 2.0*quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] 
							 + quantities[i][j-1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j-1][k][EquationsHydroIdeal.INDEX_RHO]));
			
			counter++;
		}

		//        1               2                       3                           4
		println(time + " " + (gradxp/counter) + " " + (gradyp/counter) + " " + (divergence/counter) + " "
				+ (uxx/counter) + " " + (uyy/counter) + " " + (vxx/counter) + " " + (vyy/counter) + " "); 
		//            5                      6                     7                      8
		
		
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
	}

}
