
public class ReconstructionParabolic1DModified extends ReconstructionParabolic1D {

	public ReconstructionParabolic1DModified(boolean lockInterfaceValues) {
		super(lockInterfaceValues);
		// TODO actually this reconstruction procedure does not use any interface values, so move higher up in inheritance hierarchy
	}
	
	@Override
	protected void setParameters(GridCell g, double[][][] interfaceValuesOrderedByDirection, double[] quantities){
		double dx, dy;
		int i = g.i(), j = g.j(), k = g.k();
		double u2, u1, v2, v1;
		
		dx = ((GridCartesian) grid).xSpacing();
		dy = ((GridCartesian) grid).ySpacing();
		// TODO works only for acoustic equations and only in 2-d
		// TODO bit of cheating with the neighbours' quantities
				
		
		
		int q = EquationsAcousticSimple.INDEX_VX;
		double uij     = grid.conservedQuantities[i][j][k][q];
		double uijm1   = grid.conservedQuantities[i][j-1][k][q];
		double uijp1   = grid.conservedQuantities[i][j+1][k][q];
		double uip1j   = grid.conservedQuantities[i+1][j][k][q];
		double uip1jm1 = grid.conservedQuantities[i+1][j-1][k][q];
		double uip1jp1 = grid.conservedQuantities[i+1][j+1][k][q];
		double uim1j   = grid.conservedQuantities[i-1][j][k][q];
		double uim1jm1 = grid.conservedQuantities[i-1][j-1][k][q];
		double uim1jp1 = grid.conservedQuantities[i-1][j+1][k][q];
		
		u2 = 0.25 * (uim1jp1 + 2.0*uim1j + uim1jm1 - 2.0*(uijp1 + 2.0*uij + uijm1) + uip1jp1 + 2.0*uip1j + uip1jm1);
		u1 = 0.125*(uip1jp1 + 2.0*uip1j + uip1jm1 - (uim1jp1 + 2.0*uim1j + uim1jm1)); 
				
		param0[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = quantities[q] - u2/24;
		param1[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = u1 / dx;
		param2[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = u2 / 2 / dx / dx;

		// not needed:
		param0[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = quantities[q];
		param1[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = 0.0;
		param2[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = 0.0;

		
		
		q = EquationsAcousticSimple.INDEX_VY;
		double vij     = grid.conservedQuantities[i][j][k][q];
		double vijm1   = grid.conservedQuantities[i][j-1][k][q];
		double vijp1   = grid.conservedQuantities[i][j+1][k][q];
		double vip1j   = grid.conservedQuantities[i+1][j][k][q];
		double vip1jm1 = grid.conservedQuantities[i+1][j-1][k][q];
		double vip1jp1 = grid.conservedQuantities[i+1][j+1][k][q];
		double vim1j   = grid.conservedQuantities[i-1][j][k][q];
		double vim1jm1 = grid.conservedQuantities[i-1][j-1][k][q];
		double vim1jp1 = grid.conservedQuantities[i-1][j+1][k][q];
		
		v2 = 0.25*(vip1jp1 + 2.0*vijp1 + vim1jp1 - 2.0*(vip1j + 2.0*vij + vim1j) + vip1jm1 + 2.0*vijm1 + vim1jm1);
		v1 = 0.125*(vip1jp1 + 2.0*vijp1 + vim1jp1 - (vip1jm1 + 2.0*vijm1 + vim1jm1)); 
				
		param0[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = quantities[q] - v2/24;
		param1[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = v1 / dy;
		param2[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = v2 / 2 / dy / dy;

		// not needed:
		param0[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = quantities[q];
		param1[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = 0.0;
		param2[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = 0.0;
		
		
		
		q = EquationsAcousticSimple.INDEX_P;
		param0[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = quantities[q];
		param1[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = 0.0;
		param2[g.i()][g.j()][g.k()][GridCartesian.X_DIR][q] = 0.0;

		param0[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = quantities[q];
		param1[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = 0.0;
		param2[g.i()][g.j()][g.k()][GridCartesian.Y_DIR][q] = 0.0;

		
	}
}
