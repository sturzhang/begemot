import java.io.*;
import java.util.*;
import java.nio.file.*;

public class FileInteraction {

	// modified 20170924 12:25 
	
    public static boolean VERBOSE = true;

    public static void writefile(String pathtofile, String content){
	try {
	    if (VERBOSE) { System.out.println("Writing file " + pathtofile); }
	    BufferedWriter out = new BufferedWriter(new FileWriter(pathtofile));
	    out.write(content);
	    out.close();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
    
    public static void append(String pathtofile, String content){
    	try {
    	    if (VERBOSE) { System.out.println("Writing file " + pathtofile); }
    	    BufferedWriter out = new BufferedWriter(new FileWriter(pathtofile, true));
    	    out.append(content);
    	    out.close();
    	} catch (IOException e) {
    	    e.printStackTrace();
    	}
        }
    
    public static String readfile(String pathtofile){
	boolean firsttime = true;
	String res = "";
	try {
	    BufferedReader in = new BufferedReader(new FileReader(pathtofile));
	    String line = null;
	    while ((line = in.readLine()) != null) {
		res += (firsttime? "" : "\n<br>") + line;
		firsttime = false;
	    }
	    in.close();
	} catch (IOException e) {
	    e.printStackTrace();
	} 
	return res;
    }

    public static void copyreplacefile(String pathfrom, String pathto){
	try {
	    Files.copy(new File(pathfrom).toPath(), new File(pathto).toPath(), StandardCopyOption.REPLACE_EXISTING);
	    if (VERBOSE) { System.out.println("Copying file to " + pathto); }
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public static void createdir(String path){
	File tmpdir;
	tmpdir = new File(path);
	tmpdir.mkdir();
	if (VERBOSE) { System.out.println("Creating directory " + path); }
    }

    public static void deleteandcreatedir(String path){
	File tmpdir;
	tmpdir = new File(path);
	
    	tmpdir.delete();
	if (VERBOSE) { System.out.println("Deleting directory " + path); }
	tmpdir.mkdir();
	if (VERBOSE) { System.out.println("Creating directory " + path); }
    }

    public static boolean exists(String path){
	if (VERBOSE) { System.out.println("Checking link to " + path); }
	return new File(path).exists();
    }
}