
public class InitialDataAdvectionStationaryGaussian extends
		InitialDataAdvectionStationary {

	protected double amplitude, width, offset;
	
	public InitialDataAdvectionStationaryGaussian(Grid grid, EquationsAdvectionConstant equations,
			double amplitude, double width, double offset) {
		super(grid, equations);
		this.amplitude = amplitude;  this.width = width;  this.offset = offset;
	}

	@Override
	protected double initialDataPerpendicularToVelocity(double y) {
		return amplitude * Math.exp(-y*y/width/width) + offset;
	}

	@Override
	public String getDetailedDescription() {
		return "Stationary solution of advection because the gaussian (amplitude = " + 
				amplitude + ", width = " + width + ", offset = " + offset + 		
				") depends only on the coordinate perpendicular to the velocity";
	}

}
