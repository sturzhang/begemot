
public class InitialDataIsentropicSodShock extends InitialDataIsentropic {

	public InitialDataIsentropicSodShock(GridCartesian grid,
			EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x = g.getX() - grid.xMidpoint();
    	double y = g.getY() - grid.yMidpoint();
    	
		
    	double v = 0.0;
    	
    	if (Math.abs(x) < Math.sqrt(0.062)){
    	//if (Math.abs(x) < 0.25){
    	//if (x*x + y*y < 0.04){
    	//if ( (x > -0.25) && (x < 0.25) && (y > -0.25) && (y < 0.25) ){
    		res[EquationsIsentropicEuler.INDEX_RHO] = 1.0;
    	} else {
    		res[EquationsIsentropicEuler.INDEX_RHO] = 0.1; //0.125;
    	} 
    				
    	res[EquationsIsentropicEuler.INDEX_XMOM] = v*res[EquationsIsentropicEuler.INDEX_RHO];
    	res[EquationsIsentropicEuler.INDEX_YMOM] = 0.0;
    	res[EquationsIsentropicEuler.INDEX_ZMOM] = 0.0;
    			
    		
    	return res;

	}

	@Override
	public String getDetailedDescription() {
		return "Shock tube test for the isentropic Euler equations";
	}

}
