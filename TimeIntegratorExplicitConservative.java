public class TimeIntegratorExplicitConservative extends TimeIntegratorExplicit {

	public TimeIntegratorExplicitConservative(Grid grid, double CFL) {
		super(grid, CFL);
	}

	public int steps() {
		return 1;
	}
	

	@Override
	public double[][][][] perform(int step, double dt, 
			double[][][][][] interfaceFluxes, double[][][][] sources, double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {
		
		
		int i, j, k;
		double cellSize;
		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			cellSize = g.size();

			if ((excludeFromTimeIntegration == null) || (!excludeFromTimeIntegration[i][j][k])){
				
				for (int dir = 0; dir < interfaceFluxes[i][j][k].length; dir++){
					for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
						//g.addToConservedQuantity(- dt / cellSize * interfaceFluxes[i][j][k][dir][q] , q);
						conservedQuantities[i][j][k][q] -= dt / cellSize * interfaceFluxes[i][j][k][dir][q];
					}
				}
				
				if (sources != null){
					for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
						//g.addToConservedQuantity(dt * sources[i][j][k][q], q);
						conservedQuantities[i][j][k][q] += dt * sources[i][j][k][q];
					}
				}
			}
		}
		
		
		return conservedQuantities;
	}

}
