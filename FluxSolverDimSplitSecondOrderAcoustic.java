
public class FluxSolverDimSplitSecondOrderAcoustic extends FluxSolverDimSplitSecondOrder {
	
	protected double s1, s2, s3;

	public FluxSolverDimSplitSecondOrderAcoustic(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
		
		//s1 = 0.1;
		//s2 = -0.2;
		//s3 = -(3.0*s1 + 2.0*s2);   // second order
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] leftleft,
			double[] left, double[] right, double[] rightright) {
		
		int velDirection = EquationsAcousticSimple.indexVelocity(GridCartesian.X_DIR);
		double[] flux = new double[left.length];
		
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		
		/*s1 = 0;
		s3 = Math.min(0.5, Math.sqrt(left[EquationsAcoustic.INDEX_VX]*left[EquationsAcoustic.INDEX_VX] + 
				left[EquationsAcoustic.INDEX_VY]*left[EquationsAcoustic.INDEX_VY] +
				left[EquationsAcoustic.INDEX_VZ]*left[EquationsAcoustic.INDEX_VZ] )/
				c); 
		s2 = -s3;  //*/
		s1 = 0; s2 = 0; s3 = 0;
		
		
		if (((EquationsAcousticSimple) equations).symmetric()){
			System.err.println("ERROR: Second order dim split solver not implemented for symmetrized acoustic equations!");
			return null;
		} else {
			flux[velDirection] = 0.5*(
					left[EquationsAcousticSimple.INDEX_P] + right[EquationsAcousticSimple.INDEX_P]
					-2.0*c*(s1*leftleft[velDirection] +
							s2*left[velDirection] +
						    s3*right[velDirection] - 
						    (s1+s2+s3)*rightright[velDirection]
						   )
					);
			
			/*flux[EquationsAcoustic.INDEX_P] = 0.5*(
					c*c*(left[velDirection] + right[velDirection])
					-2.0*c*(s1*leftleft[EquationsAcoustic.INDEX_P] +
							s2*left[EquationsAcoustic.INDEX_P] + 
							s3*right[EquationsAcoustic.INDEX_P] -
							(s1+s2+s3)*rightright[EquationsAcoustic.INDEX_P]
						)
					);*/
			flux[EquationsAcousticSimple.INDEX_P] = 0.5*(
					c*c*(left[velDirection] + right[velDirection])
					-c*(right[EquationsAcousticSimple.INDEX_P] -
							left[EquationsAcousticSimple.INDEX_P])); 
							
			return new FluxAndWaveSpeed(flux, c);
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Second order scheme as an approximate version of an exact Godunov 1-d solver for acoustics based on low Mach reconstruction" + 
				" constants: s1 = " + s1 + ", s2 = " + s2;
	}

}
