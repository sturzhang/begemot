
public class EquationsScalarGuckenheimer extends EquationsScalar {

	public EquationsScalarGuckenheimer(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}

	@Override
	public double flux(int i, int j, int k, double quantity, int direction) {
		if (direction == GridCartesian.X_DIR){
			return quantity*quantity/2;
		} else if (direction == GridCartesian.Y_DIR){
			return quantity*quantity*quantity/3;
		} else {return 0.0;}
		
	}

	@Override
	public String getDetailedDescription() {
		return "Guckenheimer's nonlinear scalar conservation law.";
	}

	@Override
	public double jacobian(int i, int j, int k, double quantity, int direction) {
		if (direction == GridCartesian.X_DIR){
			return quantity;
		} else if (direction == GridCartesian.Y_DIR){
			return quantity*quantity;
		} else {return 0.0;}
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		return jacobian(i, j, k, quantities[0], direction);
	}
	
	@Override
	public void transformFixedParameters(double[] normalVector) {
		// nothing to do
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// nothing to do
	}


}
