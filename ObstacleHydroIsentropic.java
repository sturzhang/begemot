
public abstract class ObstacleHydroIsentropic extends Obstacle {

	public ObstacleHydroIsentropic(GridCartesian grid, EquationsIsentropicEuler equations) {
		super(grid, equations);
	}
	
	@Override
    public double[][][][][] insertRigidWalls(double[][][][][] fluxes, double[][][][] conservedQuantities){
    	int i, j, k, momentumIndex, direction;
    	int ni, nj, nk;
    	double pressure;
    	
    	for (CellInterface interf : obstacleBoundary){
    		i = interf.fluxi();
			j = interf.fluxj();
			k = interf.fluxk();
			
			// TODO unsafe as interf is working with EquationsHydroIdeal
			momentumIndex = interf.momentumIndex();
			direction = interf.fluxDirection();
			
			ni = interf.neighbouri();
			nj = interf.neighbourj();
			nk = interf.neighbourk();
			
			pressure = ((EquationsIsentropicEuler) equations).getPressure(conservedQuantities[ni][nj][nk][EquationsIsentropicEuler.INDEX_RHO]);
			
			fluxes[i][j][k][direction][EquationsIsentropicEuler.INDEX_RHO] = 0;
			
			fluxes[i][j][k][direction][EquationsIsentropicEuler.INDEX_XMOM] = 0;
			fluxes[i][j][k][direction][EquationsIsentropicEuler.INDEX_YMOM] = 0;
			fluxes[i][j][k][direction][EquationsIsentropicEuler.INDEX_ZMOM] = 0;
			
			fluxes[i][j][k][direction][momentumIndex] = pressure;
			
			for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
				fluxes[i][j][k][direction][s] = 0;
			}
    	}
    	return fluxes;
    }
    
	@Override
	public void set(double[][][][] conservedQuantities) {
		int xOffset, yOffset, zOffset;
		int normalMomDirection = 0;
		int i, j, k;
		
		for (i = 0; i < conservedQuantities.length; i++){
			for (j = 0; j < conservedQuantities[i].length; j++){
				for (k = 0; k < conservedQuantities[i][j].length; k++){
					if (excludeFromTimeEvolution[i][j][k]){
						conservedQuantities[i][j][k][EquationsIsentropicEuler.INDEX_XMOM] = 0.0;
						conservedQuantities[i][j][k][EquationsIsentropicEuler.INDEX_YMOM] = 0.0;
						conservedQuantities[i][j][k][EquationsIsentropicEuler.INDEX_ZMOM] = 0.0;
						
						for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
							conservedQuantities[i][j][k][s] = 0;
						}
					}
				}
			}
		}
		
		
		//System.err.println(obstacleBoundary.size());
		for (CellInterface interf : obstacleBoundary){
			xOffset = 0; yOffset = 0; zOffset = 0;
			if (interf.interf() == ReconstructionCartesian.EAST ){ xOffset =  1; normalMomDirection = EquationsIsentropicEuler.INDEX_XMOM; }
			if (interf.interf() == ReconstructionCartesian.WEST ){ xOffset = -1; normalMomDirection = EquationsIsentropicEuler.INDEX_XMOM; }
			if (interf.interf() == ReconstructionCartesian.NORTH){ yOffset =  1; normalMomDirection = EquationsIsentropicEuler.INDEX_YMOM; }
			if (interf.interf() == ReconstructionCartesian.SOUTH){ yOffset = -1; normalMomDirection = EquationsIsentropicEuler.INDEX_YMOM; }
			if (interf.interf() == ReconstructionCartesian.FRONT){ zOffset =  1; normalMomDirection = EquationsIsentropicEuler.INDEX_ZMOM; }
			if (interf.interf() == ReconstructionCartesian.BACK ){ zOffset = -1; normalMomDirection = EquationsIsentropicEuler.INDEX_ZMOM; }
			
			//System.err.println(interf.i()+ " " + xOffset+ " " + interf.j()+ " " + yOffset+ " " + interf.k() +" " +zOffset);
			i = interf.i();
			j = interf.j();
			k = interf.k();
						
			conservedQuantities[i][j][k][normalMomDirection] = 0.0;
				   //-conservedQuantities[i+xOffset][j+yOffset][k+zOffset][normalMomDirection]; 
				    
				/*/ conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsIsentropicEuler.INDEX_RHO] *
					conservedQuantities[i][j][k][EquationsIsentropicEuler.INDEX_RHO];*/
			
			conservedQuantities[i][j][k][EquationsIsentropicEuler.INDEX_RHO] =
					   conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsIsentropicEuler.INDEX_RHO]; 
				
			
			
		}
		
	}


}
