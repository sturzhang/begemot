

import java.util.*;

public class TestSeries {

	protected LinkedList<Test> tests;
	
	public TestSeries() {
		tests = new LinkedList<Test>();
	}
	
	public void add(Test test){
		tests.add(test);
	}
	
	private void println(String text){
		System.out.println(text);
		System.err.println(text);
	}
	
	private void printLine(){
		println("####################################################################################");
	}
	
	public void run(){
		boolean passed = true;
		int counterPassed = 0;
		LinkedList<Integer> notPassed = new LinkedList<Integer>(); 
		
		for (Test t : tests ){
			println("");
			println("");
			printLine();
			println("");
			
			System.err.println("Running TEST " + tests.indexOf(t) + "; so far passed: " + counterPassed);
			System.out.println("        TEST " + tests.indexOf(t) );
			
			println("");
			println("");
			
			
			
			if (!t.passed()) {
				notPassed.add(tests.indexOf(t));
				println("");
				println("<<<<<<<<<<<<<<<<<<<<<<<<<<<< NOT PASSED >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				println("");
									
				println(t.description() );
				passed = false;
				//break;
			} else {
				println(t.description() );
				counterPassed++;
			}
			printLine();
			
		}
		
		println(""); println(""); println(""); println("");
			
		if (passed) { 
			println("All tests passed!"); 
		} else {
			println("Passed: " + counterPassed + "/" + tests.size());
			println("Not passed:");
			for (int i : notPassed){
				println("-> " + i);
			}
		}
	}
}
