
public class InitialDataHydroKelvinHelmholtz extends InitialDataHydro {

	protected double vMagnitude;
	protected double rho;
	protected double pressure;
	protected double heightInitialPerturbation;
	
	public InitialDataHydroKelvinHelmholtz(Grid grid, EquationsHydroIdeal equations, 
			double vMagnitude, double rho, double pressure, double heightInitialPerturbation) {
		super(grid, equations);
		
		this.vMagnitude = vMagnitude;
		this.rho = rho;
		this.pressure = pressure;
		this.heightInitialPerturbation = heightInitialPerturbation;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		double interfacePosition;
		double argument = (g.getX() - grid.xMidpoint()) / grid.xMidpoint() * Math.PI * 2.0;
		double deltarho = 0.05;
		double vOffset = 0.02;
		
		
		interfacePosition = grid.yMidpoint() + 
				heightInitialPerturbation * Math.cos(argument);
		
		
		
		
		if (g.getY() > interfacePosition){
			res[EquationsHydroIdeal.INDEX_RHO] = rho + deltarho;
			
			res[EquationsHydroIdeal.INDEX_XMOM] =  (vMagnitude + vOffset) * res[EquationsHydroIdeal.INDEX_RHO];
			res[EquationsHydroIdeal.INDEX_YMOM] =  (vMagnitude + vOffset) * res[EquationsHydroIdeal.INDEX_RHO] * 
					(- heightInitialPerturbation*2.0*Math.PI/grid.xMidpoint()*Math.sin(argument)*Math.exp(-(g.getY() - interfacePosition)));
		} else {
			res[EquationsHydroIdeal.INDEX_RHO] = rho - deltarho;
			
			res[EquationsHydroIdeal.INDEX_XMOM] = (-vMagnitude + vOffset) * res[EquationsHydroIdeal.INDEX_RHO];
			res[EquationsHydroIdeal.INDEX_YMOM] = (-vMagnitude + vOffset) * res[EquationsHydroIdeal.INDEX_RHO] * 
					(- heightInitialPerturbation*2.0*Math.PI/grid.xMidpoint()*Math.sin(argument)*Math.exp(-(interfacePosition - g.getY())));
		}
    				
		double rhoV2 = Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) / res[EquationsHydroIdeal.INDEX_RHO];
					
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
				((EquationsHydroIdeal) equations).getEnergy(
    								pressure, rhoV2);
 
		//res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
		
		for (int s = 5; s < qq; s++){
			// passive scalars
			if (g.getY() > interfacePosition){
				res[s] =  1.0 * res[EquationsHydroIdeal.INDEX_RHO];
			} else {
				res[s] = -1.0 * res[EquationsHydroIdeal.INDEX_RHO]; 
			}			
		}
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Kelvin Helmholtz instability setup with constant density = " + rho + " and pressure = " + pressure + 
				" and an initial perturbation amplitude of the interface of " + heightInitialPerturbation;
	}

}
