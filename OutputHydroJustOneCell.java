
public class OutputHydroJustOneCell extends Output {

	private int i, j, k;
	private boolean firsttime = true;
	private double initialval;
	private double initialdux;
	
	public OutputHydroJustOneCell(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations, 
			int i, int j, int k, boolean doPrint) {
		super(outputTimeInterval, grid, equations);
		this.i = i;
		this.j = j;
		this.k = k;
	}

	@Override
	public String getDetailedDescription() {
		return "output of quantities in given cell";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double rho = quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		double u = quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/rho;
		double v = quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]/rho;
		
		
		double rhoV2 = (Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM], 2) + 
				Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM], 2) +
				Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM], 2)) / 
				quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		double pressure = ((EquationsHydroIdeal) equations).getPressure(quantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY], rhoV2);
		
		
		double dux = (quantities[i+1][j][k][EquationsHydroIdeal.INDEX_XMOM] - 
				quantities[i-1][j][k][EquationsHydroIdeal.INDEX_XMOM])/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]/2;
		
		if (firsttime){
			firsttime = false;
			initialval = pressure;
			initialdux = dux;
		}
		
		GridCell g = new GridCell(i,j,k, grid);
		double x = g.getX();
		double y = g.getX();
		
		
		//println(time + " " + Math.abs(pressure-initialval) + " " + Math.sqrt(rhoV2/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]));
		//println(time + " " + pressure + " " + (rhoV2/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]));
		//println(time + " " + Math.abs(pressure-initialval) + " " + Math.abs(dux)); 
		println(time + " " + rho + " " + u + " " + v + " " + pressure + " " + x + " " + y);
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
