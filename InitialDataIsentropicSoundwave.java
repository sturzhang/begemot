
public class InitialDataIsentropicSoundwave extends InitialDataIsentropic {

	private double amplitudeRho, backgroundRho, wavelengthX, wavelengthY, amplitudeV;
	
	public InitialDataIsentropicSoundwave(Grid grid, EquationsIsentropicEuler equations,
			double backgroundRho, double amplitudeRho, double amplitudeV, double wavelengthX, double wavelengthY) {
		super(grid, equations);
		this.backgroundRho = backgroundRho;
		this.amplitudeRho = amplitudeRho;
		this.amplitudeV = amplitudeV;
		this.wavelengthX = wavelengthX;
		this.wavelengthY = wavelengthY;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x = g.getX() - grid.xMidpoint();
    	double y = g.getY() - grid.yMidpoint();
    	
    	double val = Math.cos(2.0*Math.PI*(x/wavelengthX + y/wavelengthY));
    	res[EquationsIsentropicEuler.INDEX_RHO] = backgroundRho + amplitudeRho * val;
    	res[EquationsIsentropicEuler.INDEX_XMOM] = amplitudeV*val*res[EquationsIsentropicEuler.INDEX_RHO];
    	res[EquationsIsentropicEuler.INDEX_YMOM] = 0.0;
    	res[EquationsIsentropicEuler.INDEX_ZMOM] = 0.0;
    			
    		
    	return res;

	}

	@Override
	public String getDetailedDescription() {
		return "Soundwave on background density " + backgroundRho + " with amplitude in density: " + amplitudeRho + " and in velocity: " + 
				amplitudeV + " with lambdax = " + wavelengthX + " and lambday = " + wavelengthY;
	}

}
