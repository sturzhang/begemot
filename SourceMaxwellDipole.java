
public class SourceMaxwellDipole extends SourceMaxwell {

	protected double omega, distance;
	protected double width = 0.02; // width of gaussian approximation to a point charge
	protected double prefactor;
	
	public SourceMaxwellDipole(Grid grid, EquationsMaxwell equations, double omega, double distance) {
		super(grid, equations);
		this.omega = omega;
		this.distance = distance;
		
		prefactor = distance*omega/2/Math.pow(Math.PI*width, 1.5);
	}

	@Override
	protected double[] current(GridCell g, double time) {
		double x = g.getX() - grid.xMidpoint();
		double y = g.getY() - grid.yMidpoint();
		double z = g.getZ() - grid.zMidpoint();
		
		double shift = distance/2*Math.cos(omega*time);
				
		double J = -prefactor*Math.sin(omega*time)*(gaussian(x, y - shift, z) + gaussian(x, y + shift, z));
		return new double[]{0, J, 0};
	}

	protected double gaussian(double x, double y, double z){
		return Math.exp(- (x*y + y*y + z*z) / width/width);
	}
	
	@Override
	public String getDetailedDescription() {
		return "Gaussian unit charges moving with frequency = " + omega + ", distance = " + distance;
	}

}
