
public class TimeIntegratorForSemiLagrangianAcoustic extends
		TimeIntegratorForSemiLagrangian {

	public TimeIntegratorForSemiLagrangianAcoustic(Grid grid, double CFL, FluxSolverSemiLagrangianAcoustic fluxSolver) {
		super(grid, CFL, fluxSolver);
	}

	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {
		
		if (!fluxSolverKnowsMe){
			fluxSolver.setTimeIntegrator(this);
			fluxSolverKnowsMe = true;
		}
		
		if (step <= 2){
			savedQuantities[step] = myIntegrator.perform(step, dt, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
		} else {
			System.err.println("ERROR: time integrator should have only two steps!");
		}
					
		
		return savedQuantities[step];
		
	}

}
