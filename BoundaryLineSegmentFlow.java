
public class BoundaryLineSegmentFlow extends Boundary {

	private double alpha, vinfty; 
	
	public BoundaryLineSegmentFlow(GridCartesian grid, double alpha, double vinfty) {
		super(grid);
		// TODO Auto-generated constructor stub
	}

	public double alpha(){ return alpha; }
	public double vinfty(){ return vinfty; }
	
	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k;
		double x, y, vx, vy;
		double Im1, Im2, Imx, Imy, r1, r2, theta1, theta2;
		
		i = g.i(); j = g.j(); k = g.k();
		
		x = grid.getX(i,j,0) - grid.xMidpoint();
		y = grid.getY(i,j,0) - grid.yMidpoint();
		
		r1 = Math.sqrt((x-2)*(x-2) + y*y);
		r2 = Math.sqrt((x+2)*(x+2) + y*y);
		theta1 = Math.atan2(y, x-2);
		theta2 = Math.atan2(y, x+2);
		
		Im1 = Math.sin((theta1+theta2)/2) / 2 / Math.sqrt(r1*r2);
		Im2 = Math.cos((theta1+theta2)/2) / 2 / Math.sqrt(r1*r2);
		
		Imx = Im1 * (x * (r2/r1 + r1/r2) + 2 * (r1/r2 - r2/r1)) - Im2 * y * (1.0/r1/r1 + 1.0/r2/r2);
		Imy = Im1 * y * (r2/r1 + r1/r2) + Im2 * (x*(1.0/r1/r1 + 1.0/r2/r2) + 2*(1.0/r2/r2 - 1.0/r1/r1));
		
		vx = vinfty*(Math.cos(alpha) + Math.sin(alpha)*Imx);
		vy = vinfty*                   Math.sin(alpha)*Imy;
		
		/*for (int k = 0; k < conservedQuantities[i][j].length; k++){
									
			if ((i >= grid.indexMinX()) && (i <= grid.indexMaxX()) && 
				(j >= grid.indexMinY()) && (j <= grid.indexMaxY()) && 
				(k >= grid.indexMinZ()) && (k <= grid.indexMaxZ())){
			} else {*/
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 1.0;					
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
		
		
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = vx*conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] = vy*conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = 1.0;
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
