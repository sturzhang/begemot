
public abstract class TimeIntegrator {


	protected Grid grid;

	public TimeIntegrator(Grid grid) {
		this.grid = grid;
	}

	public abstract String getDetailedDescription();
    
    public String description(){ 
    	return "# Time integrator used: " + getDetailedDescription(); 
    }
}
