import java.util.*;

public class TestSeriesAdvection extends TestSeries {

	// NOTE: one should pay attention to the iteration having an output after time has reached finaltime
	
	public TestSeriesAdvection() {
    	
		double finaltime = 0.05;
		
		EquationsAdvectionConstant equations;
		
    	LinkedList<Reconstruction> reconstructions = new LinkedList<Reconstruction>();
    	reconstructions.add(new ReconstructionPiecewiseConstant());
    	reconstructions.add(new ReconstructionWENO(1));  // WENO3
    	reconstructions.add(new ReconstructionWENO(2));  // WENO5
    	
    	LinkedList<InitialData> initialData;
    	LinkedList<Boundary> boundaries;
		LinkedList<FluxSolver> fluxSolvers;
    	LinkedList<TimeIntegratorFiniteVolume> timeIntegrators;		
		
    	for (Reconstruction reconstruction: reconstructions){
    		LinkedList<GridCartesian> grids = new LinkedList<GridCartesian>();
    		grids.add(new Grid1D(0.0, 1.0, 51, 
    				reconstruction.numberOfGhostcells()));
    		grids.add(new Grid1D(-1.0, 1.0, 101, 
    				reconstruction.numberOfGhostcells()));
        	grids.add(new GridCartesian2D(-1.5, 1.0, 51, 0.3, 1.3, 51, 
    				reconstruction.numberOfGhostcells()));
        	grids.add(new GridCartesian2D(-1.5, 1.0, 51, 0.3, 1.3, 51, 
    				reconstruction.numberOfGhostcells()));

    		for (GridCartesian grid : grids){
    			equations     = new EquationsAdvectionConstant(grid, 0, new double[] {1.2, -0.3, 0.0});
    	    	
    			
    	    	initialData = new LinkedList<InitialData>();
    	    	initialData.add(new InitialDataAdvectionGaussian(grid, equations, 1.0, 0.1, 0.5));
    	    	initialData.add(new InitialDataAdvectionJump(grid, equations));
    	    	
    	    	boundaries = new LinkedList<Boundary>();
    	    	boundaries.add(new BoundaryPeriodic(grid));
    	    	//boundaries.add(new BoundaryZeroGradient(grid));
    	    	//boundaries.add(new BoundaryFixedToInitial(grid, initialData));

    	    	fluxSolvers = new LinkedList<FluxSolver>();
    	    	fluxSolvers.add(new FluxSolverRoe(grid, equations));
    	    	fluxSolvers.add(new FluxSolverOneStepMultiDAdvectionConstant(grid, equations));
    	    	fluxSolvers.add(new FluxSolverMultiStepMultiDExactRiemannAdvectionConstant(grid, equations));
    	    	
    	    	timeIntegrators = new LinkedList<TimeIntegratorFiniteVolume>();
    	    	timeIntegrators.add(new TimeIntegratorExplicitConservative(grid, 0.2));
    	    	timeIntegrators.add(new TimeIntegratorRungeKutta2(grid, 0.3));
    	    	timeIntegrators.add(new TimeIntegratorRungeKutta3(grid, 0.3));
    	    	
    	    	for (InitialData initialDatum : initialData){
    	    		for (Boundary boundary : boundaries){
    	    			for (FluxSolver fluxSolver : fluxSolvers){
    	    				for (TimeIntegratorFiniteVolume timeIntegrator : timeIntegrators){
    	    					add(new Test(
		    						reconstruction, grid, equations,
		    						new SourceZero(grid), new ObstacleNone(grid), 
		    						initialDatum, boundary, fluxSolver, timeIntegrator,
		    						new OutputErrorNormAdvectionConstant(finaltime, grid, equations, initialDatum, boundary, 1),
		    						finaltime)
    	    					);  
    	    				}
    	    			}
    	    		}
    	    	}
    	    	
    		}
    	}
	}

}
