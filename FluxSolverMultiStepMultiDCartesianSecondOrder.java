
public abstract class FluxSolverMultiStepMultiDCartesianSecondOrder extends
		FluxSolverMultiStepMultiDCartesian {


	public FluxSolverMultiStepMultiDCartesianSecondOrder(GridCartesian grid,
			Equations equations) {
		super(grid, equations, new StencilTurnerMultiDSecondOrder(grid));
	}

	
	@Override
	protected FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge,
			double[][] quantities, int timeIntegratorStep) {
		return fluxmultid(gridEdge, 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LLTT], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LLT], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LL], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LLB], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LLBB], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LTT], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LT], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_L], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LB], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_LBB],
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RTT], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RT], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_R], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RB], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RBB], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RRTT], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RRT], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RR], 
				quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RRB], quantities[StencilTurnerMultiDSecondOrder.NEIGHBOUR_RRBB]);
	}
	
	protected abstract FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] LLTT, double[] LLT, double[] LL, double[] LLB, double[] LLBB, 
			double[] LTT, double[] LT, double[] L, double[] LB, double[] LBB,
			double[] RTT, double[] RT, double[] R, double[] RB, double[] RBB, 
			double[] RRTT, double[] RRT, double[] RR, double[] RRB, double[] RRBB);
	
}
