
public class OutputShallowWater extends OutputAllQuantities {

	protected SourceIsentropicEulerLikeInShallowWater source;
	
	public OutputShallowWater(double outputTimeInterval, GridCartesian grid,
			Equations equations, SourceIsentropicEulerLikeInShallowWater source) {
		super(outputTimeInterval, grid, equations);
		this.source = source;
	}

	public OutputShallowWater(double outputTimeInterval, double zproportion,
			GridCartesian3D grid, Equations equations, SourceIsentropicEulerLikeInShallowWater source) {
		super(outputTimeInterval, zproportion, grid, equations);
		this.source = source;
	}

	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		double vx, vy, vz, height, bottom;
		
		vx = quantities[EquationsIsentropicEuler.INDEX_XMOM]/quantities[EquationsIsentropicEuler.INDEX_RHO];
		vy = quantities[EquationsIsentropicEuler.INDEX_YMOM]/quantities[EquationsIsentropicEuler.INDEX_RHO];
		vz = quantities[EquationsIsentropicEuler.INDEX_ZMOM]/quantities[EquationsIsentropicEuler.INDEX_RHO];
		
		height = quantities[EquationsIsentropicEuler.INDEX_RHO];
		bottom = source.bottomTopography(grid.getX(i, j, k));

		
		print((height + bottom) + " ");
		print(vx + " " + vy + " " + vz + " ");
		print(bottom + " ");
		
	}

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getDetailedDescription() {
		return "prints height+bottom=waterlevel, vx, vy, vz, bottomlevel";
	}

}
