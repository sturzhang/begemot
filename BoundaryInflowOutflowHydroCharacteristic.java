
public class BoundaryInflowOutflowHydroCharacteristic extends Boundary {

	protected double[] vel;
		
	protected EquationsHydroIdeal equations;
	protected HydroState boundaryState;
	
	public BoundaryInflowOutflowHydroCharacteristic(GridCartesian grid, EquationsHydroIdeal equations, 
			double rho, double pressure, double[] vel) {
		super(grid);
		this.vel = vel;
		this.equations = equations;
		
		boundaryState = new HydroState(rho, vel[0], pressure, equations);
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int nextI, nextJ, nextK, i, j, k;
		boolean left, right;
		double rhoV2;
		HydroState adjacent;
		double Iplus, Iminus, I0;  // characteristic variables
		int leftRightSign;
		
		i = g.i(); j = g.j(); k = g.k();
		
		nextI = i; nextJ = j; nextK = k;
		left = false;
		right = false;
			
		if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); left = true;}
		if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
			
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); } //right = true; }
		if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }

		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
		}

		if (left || right){
			adjacent = new HydroState(conservedQuantities[nextI][nextJ][nextK], EquationsHydroIdeal.getIndexDirection(GridCartesian.X_DIR), equations);
			if (left){ leftRightSign = 1; } else {leftRightSign = -1; }
			
			if ((boundaryState.soundSpeed + boundaryState.v)*leftRightSign > 0){
				Iplus = 2.0*boundaryState.soundSpeed/(equations.gamma() - 1) + boundaryState.v;
			} else {
				Iplus = 2.0*adjacent.soundSpeed/(equations.gamma() - 1) + adjacent.v;
			}
			
			if (boundaryState.v*leftRightSign > 0){
				I0 = boundaryState.pressure*Math.pow(boundaryState.rho, -equations.gamma());
			} else {
				I0 = adjacent.pressure*Math.pow(adjacent.rho, -equations.gamma());
			}
			
			if ((-boundaryState.soundSpeed + boundaryState.v)*leftRightSign > 0){
				Iminus = 2.0*boundaryState.soundSpeed/(equations.gamma() - 1) - boundaryState.v;
			} else {
				Iminus = 2.0*adjacent.soundSpeed/(equations.gamma() - 1) - adjacent.v;
			}
			
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 
					Math.pow(
							(Iplus+Iminus)*(Iplus+Iminus)*(equations.gamma()-1)*(equations.gamma()-1)
							/16/equations.gamma()/I0, 1.0/(equations.gamma()-1));
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = 
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]*(Iplus + Iminus)/2;
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] = 
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]*vel[1];
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] = 
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]*vel[2];
			
			rhoV2 = (conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]*
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] + 
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]*
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM])/
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
			
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = 
					equations.getEnergy(
							I0*Math.pow(conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO], equations.gamma()), rhoV2);
			
			for (int qq = 0; qq < 5; qq++){
				if (Double.isNaN(conservedQuantities[i][j][k][qq])){
					System.err.println("value of " + qq + "(" + i + "," + j + ")" + " set to NaN by boundary! " + I0 + " " + Iplus + " " + Iminus + "|" + adjacent.soundSpeed + " " + adjacent.v
							 + " " + conservedQuantities[nextI][nextJ][nextK][0] + "=" + adjacent.rho
							 + " " + conservedQuantities[nextI][nextJ][nextK][1] + "=" + adjacent.v*adjacent.rho 
							 + " " + conservedQuantities[nextI][nextJ][nextK][2] 
							 + " " + conservedQuantities[nextI][nextJ][nextK][4] + " " + adjacent.pressure);
				}
			}
			
			
			if (left){
				if (time > 5){
					//if (grid.getY(j) < 0.1*grid.yMidpoint()){
						for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
							// passive scalars
							conservedQuantities[i][j][k][s] = Math.pow(-1.0, Math.max(0, Math.round((grid.getY(i,j,k) - grid.ymin())/(grid.ymax() - grid.ymin())*10)));
						}
					//}
				}
			}
		} 
		 
	}


}
