import matrices.SquareMatrix;


public class EquationsHydroIdealWith1dDeriv extends EquationsHydro {

	public static final int INDEX_RHO_DERIV = 5;
	public static final int INDEX_XMOM_DERIV = 6;
	public static final int INDEX_YMOM_DERIV = 7;
	public static final int INDEX_ZMOM_DERIV = 8;
	public static final int INDEX_ENERGY_DERIV = 9;
	
	// TODO can't we make them inherit EquationsHydro
	
	int directionDerivatives;
	EquationsHydroIdeal myEquations;
	
	public EquationsHydroIdealWith1dDeriv(Grid grid, int numberOfPassiveScalars, double gamma, int directionDerivatives) {
		super(grid, numberOfPassiveScalars);
		myEquations = new EquationsHydroIdeal(grid, numberOfPassiveScalars, gamma);
		this.directionDerivatives = directionDerivatives;
	}

	public EquationsHydroIdeal getHydroEquations(){
		return myEquations;
	}
	
	public String getDetailedDescription(){
		return "Ideal Hydrodynamics with the EOS of a perfect gas with gamma = " + myEquations.gamma() + 
				" with the spatial derivatives in direction " + directionDerivatives + " as additional variables.";
	}

	public int getNumberOfConservedQuantities() {
		return 5+5;
	}
	
	public double gamma(){ return myEquations.gamma(); }
		
	public double getEnergy(double pressure, double rhoV2) {
		return myEquations.getEnergy(pressure, rhoV2);
	}
	
	public double getEnthalpy(double energy, double rho, double pressure){
		return myEquations.getEnthalpy(energy, rho, pressure);
	}

	public double getPressure(double energy, double rhoV2){
		return myEquations.getPressure(energy, rhoV2);
	}
	
	public double getSoundSpeedFromPressure(double pressure, double rho){
		return myEquations.getSoundSpeedFromPressure(pressure, rho);
	}
	
	public double getRhoV2(double[] quantities){
		return myEquations.getRhoV2(quantities);						
	}
	
	public double getSoundSpeedFromEnthalpy(double enthalpy, double v2){
		return myEquations.getSoundSpeedFromEnthalpy(enthalpy, v2);
	}

	/* old public double[][] JacobianWOutDeriv(double[] state, int direction){
		return myEquations.Jacobian(state, direction);
	}*/

	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		return myEquations.fluxFunction(i, j, k, quantities, direction);
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		return myEquations.diagonalization(i, j, k, quantities, direction);
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		return myEquations.advectionVelocityForPassiveScalar(i, j, k, quantities, direction);
	}

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		// TODO Auto-generated method stub
		return null;
	}
		
	
}
