
public class OutputMaxDevFromHorizDensAverageAndMaxDevMachNumber extends Output {

	protected InitialData initial;
	
	public OutputMaxDevFromHorizDensAverageAndMaxDevMachNumber(
			double outputTimeInterval, GridCartesian grid, Equations equations, InitialData initial) {
		super(outputTimeInterval, grid, equations);
		this.initial = initial;
	}

	@Override
	public String getDetailedDescription() {
		return "prints maximum value of local mach number over time";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double deviationDensity = 0.0;
		double deviationHorVelocity = 0.0;
		
		double horizontalDensityAverage[] = new double[quantities[0].length];
		double horizontalVelocityAverage[] = new double[quantities[0].length];
		double rhoV2;
		double maxmachnumber = 0.0;
		double mach, initialmach, vx;

		for (int j = ((GridCartesian) grid).indexMinY(); j <= ((GridCartesian) grid).indexMaxY(); j++){
			horizontalDensityAverage[j] = 0.0;
			horizontalVelocityAverage[j] = 0.0;
			for (int i = 0; i < quantities.length; i++){
				horizontalDensityAverage[j] += quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				horizontalVelocityAverage[j] += quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_XMOM]/
						quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
			}
			horizontalDensityAverage[j] /= quantities.length;
			horizontalVelocityAverage[j] /= quantities.length;
		}

		for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
			for (int j = ((GridCartesian) grid).indexMinY(); j <= ((GridCartesian) grid).indexMaxY(); j++){
				for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){

					deviationDensity = Math.max(deviationDensity, 
							Math.abs(quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] - horizontalDensityAverage[j]) /
							horizontalDensityAverage[j]);

					vx = quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/
							quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
					
					deviationHorVelocity = Math.max(deviationHorVelocity, 
							Math.abs((vx - horizontalVelocityAverage[j]) / horizontalVelocityAverage[j]));
					
					rhoV2 = (Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM], 2) + 
							Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM], 2) +
							Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM], 2)) / 
							quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];

					initialmach = ((EquationsHydroIdeal) equations).getMachFromConservative(quantities[i][j][k]);
					
					mach = ((EquationsHydroIdeal) equations).getMachFromConservative(
							initial.getInitialValue(new GridCell(i, j, k, grid), quantities[i][j][k].length));
					
					maxmachnumber = Math.max(maxmachnumber, Math.abs(mach - initialmach)); 
							//Math.sqrt(rhoV2 / quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]) / soundspeed);
				}
			}
		}

		println(time + " " + deviationDensity + " " + maxmachnumber + " " + deviationHorVelocity);
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
