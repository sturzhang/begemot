public abstract class EquationsHydro extends Equations {

	public EquationsHydro(Grid grid, int numberOfPassiveScalars){
		super(grid, numberOfPassiveScalars);
	}
	
	@Override
	public void transformFixedParameters(double[] normalVector) {
		// nothing to do
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// nothing to do
	}
}
