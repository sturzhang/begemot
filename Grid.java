import java.util.LinkedList;


public abstract class Grid implements Iterable<GridCell> {
	
	public boolean[][][]       excludeFromTimeEvolution;
    public double [][][][]     conservedQuantities;
    public double [][][][][]   interfaceValues;
    public double [][][][][]   pointValues; // point values on edges
    public double [][][][][]   summedInterfaceFluxes;
    public double [][][][][][] interfaceFluxes;
    public double [][][][][] additionalInterfaceQuantities;
    public double [][][][]     sources;
    public double [][][][]     localWaveSpeeds;
	
    protected LinkedList<GridCell> allGhostCells;
    protected LinkedList<GridEdge> allBoundaryEdges;
    
    //protected GridEdgeMapped[][][][] allEdges;
    protected GridEdgeMappedList allEdges;
    
    protected Obstacle myObstacle;
	
    public abstract void initialize(Obstacle obstacle);
    
	public Grid(){

	}
    
	// TODO this method should become abstract
    public boolean isInsideCell(GridCell g, double x, double y, double z){
    	return false;
    }
    
	public abstract String description(); 
	public abstract boolean[][][] generateEmptyBooleanArray(); 
    public abstract double[][][][] generateEmptyArray(int numberOfQuantities); 
    public abstract double[][][][][][] generateEmptyFluxArray(int numberOfQuantities, int numberOfFluxSolverSteps); 
    public abstract double[][][][][] generateEmptyFluxArray(int numberOfQuantities);
    public abstract double[][][][] generateEmptyWaveSpeedArray();
    public abstract double[][][][][] generateEmptyArrayForInterface(int numberOfQuantities);
    
    
    public abstract double cellSize(GridCell cell);
    public abstract double edgeSize(GridEdge edge);
    
    public abstract LinkedList<GridCell> getNeighbours(GridCell cell);
    public abstract LinkedList<GridEdge> getEdges(GridCell cell);
    
    public abstract double[] normalVector(GridEdge e);
    
    public abstract double getX(int i, int j, int k);
    public abstract double getY(int i, int j, int k);
    public abstract double getZ(int i, int j, int k);
    
    public double xMidpoint(){ return (xmin() + xmax()) / 2; }
    public double yMidpoint(){ return (ymin() + ymax()) / 2; }
    public double zMidpoint(){ return (zmin() + zmax()) / 2; }

    public abstract double xmax();
    public abstract double xmin();
    public abstract double ymax();
    public abstract double ymin();
    public abstract double zmax();
    public abstract double zmin();
    
    public abstract int dimensions();
    
    public abstract double minSpacing();
        
    public abstract boolean isValidCell(GridCell g);
    
    public abstract void setAllGhostcells();
    
    public void setAllEdges(){
    	allEdges = new GridEdgeMappedList(interfaceValues.length, interfaceValues[0].length, interfaceValues[0][0].length, interfaceValues[0][0][0].length);
    	boolean[][][][] counterpartSet = new boolean[interfaceValues.length][interfaceValues[0].length][interfaceValues[0][0].length][interfaceValues[0][0][0].length];
    	
    	allEdges.initialize(this);
    	
    	
    	// TODO the index is so far a property of GridEdgeMapped only; change this: every edge has an index
    	
    	for (int i = 0; i < counterpartSet.length; i++){
    		for (int j = 0; j < counterpartSet[i].length; j++){
    			for (int k = 0; k < counterpartSet[i][j].length; k++){
    				for (int l = 0; l < counterpartSet[i][j][k].length; l++){
    					counterpartSet[i][j][k][l] = false;
    			}}}}
    	
    	allBoundaryEdges = new LinkedList<GridEdge>();
    	
    	GridEdgeMapped edge = null, otherEdge = null; // make compiler happy
    	GridCell nb;
    	for (GridCell g : this){
    		for (int edgeIndex = 0; edgeIndex < allEdges.get(g.i(), g.j(), g.k()).length; edgeIndex++){
    			if (!counterpartSet[g.i()][g.j()][g.k()][edgeIndex]){
	    			edge = allEdges.get(g.i(), g.j(), g.k(), edgeIndex);
	    			nb = edge.getOtherGridCell(g);
	    			
	    			for (int otherEdgeIndex = 0; otherEdgeIndex < allEdges.get(nb.i(), nb.j(), nb.k()).length; otherEdgeIndex++){
	    				otherEdge = allEdges.get(nb.i(), nb.j(), nb.k(), otherEdgeIndex);
	    				if (otherEdge != null){
	    					// edges at the boundary don't have a counterpart!
		    				if (edge.equals(otherEdge)){
		    					edge.setCounterPart(otherEdge);
		    					//edge.setIndex(edgeIndex); // TODO the index should have been set automatically
		    					
		    	    			otherEdge.setCounterPart(edge);
		    	    			//otherEdge.setIndex(otherEdgeIndex); // TODO the index should have been set automatically
		    	    			
		    	    			counterpartSet[g.i()][g.j()][g.k()][edgeIndex] = true;
		    	    			counterpartSet[nb.i()][nb.j()][nb.k()][otherEdgeIndex] = true;
		    	    			break;
		    				}
	    				} else {
	    					allBoundaryEdges.add(edge);
	    				}
	    			}
    			}    			
    		}
    	}
    	
    	setAdditionalEdgeRelationship();
    	
    } 
    
    protected void setAdditionalEdgeRelationship(){ }
    
    public GridEdgeMapped[] getAllEdges(int i, int j, int k){ return allEdges.get(i, j, k); }
    public GridEdgeMappedList getAllEdges(){ return allEdges; }
    
    public LinkedList<GridCell> getAllGhostcells(){    return allGhostCells;    }
    public LinkedList<GridEdge> getAllBoundaryEdges(){ return allBoundaryEdges; }
    
    
}
