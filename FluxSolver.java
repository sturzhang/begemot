
public abstract class FluxSolver {

	private double curMaxSignalSpeed;
	protected Grid grid;
	protected Equations equations;
	protected boolean[][][] excludeFromTimeEvolution;
	
	
	public FluxSolver(Grid grid, Equations equations){
		this.grid = grid;
		this.equations = equations;
	}
	
	protected double sign(double a){
		if (a == 0){ 
			return 0.0; 
		} else {
			if (a > 0){ 
				return 1.0; 
			} else {
				return -1.0;
			}
		}
	}

	public void setCellsExcludedFromTimeEvolution(boolean[][][] excludeFromTimeEvolution){
		this.excludeFromTimeEvolution = excludeFromTimeEvolution;
	}
	
	// by default -- we could also set this as abstract and let everyone write its own zero :)
	public int getNumberOfAdditionalInterfaceQuantities(){ return 0; }

	/*public abstract void setWaveSpeeds(double[][][][][] interfaceValues, 
			double[][][][][][] interfaceFluxes, double[][][][] localWaveSpeeds, double[][][][][] additionalInterfaceQuantities); //*/    	

	public abstract double[][][][][][] getAndSetWaveSpeeds(double[][][][][] interfaceValues, 
						double[][][][][][] interfaceFluxes, double[][][][] localWaveSpeeds, int timeIntegratorStep, double[][][][][] additionalInterfaceQuantities);    	

	public abstract double[][][][][] sum( double[][][][][][] interfaceFluxes, double dt, double[][][][][] summedInterfaceFluxes, double[][][][] localWaveSpeeds, double [][][][][] additionalInterfaceQuantities);

	
	public abstract int steps();
	
	public abstract String getDetailedDescription();
    
    public String description(){ 
    	return "# Flux solver used: " + steps() + "-step solver:" + getDetailedDescription(); 
    }

    public void resetCurMaxSignalSpeed(){ curMaxSignalSpeed = 0.0; }
	protected void chooseCurMaxSignalSpeed(double value) { curMaxSignalSpeed = Math.max(Math.abs(value),curMaxSignalSpeed); }
    public double curMaxSignalSpeed(){ return curMaxSignalSpeed; }

	public double[] combine(double[] flux, double[] fluxPassiveScalar){
		String error;
		double[] res = new double[equations.getTotalNumberOfConservedQuantities()];
		if (flux.length != equations.getNumberOfConservedQuantities()){ 
			error = "ERROR: Wrong implementation of fluxes! The flux solver should only return a vector " + 
					"with as many entries as there are true conserved quantities whereas the passive scalars are " + 
					"treated separately!";
			System.out.println(error);
			System.err.println(error);
		} else {
			if (fluxPassiveScalar.length != equations.getNumberOfPassiveScalars()){
				error = "ERROR: Wrong implementation of fluxes for the passive scalars -- mismatch in dimension!";
				System.out.println(error);
				System.err.println(error);
			} else {
				
				for (int i = 0; i < flux.length; i++){
					res[i] = flux[i];
				}
				for (int i = 0; i < fluxPassiveScalar.length; i++){
					res[i+flux.length] = fluxPassiveScalar[i];
				}
				
			}
		}
		return res;
	}
	
	
	
}
