import matrices.*;


// should not be used any more

public class FluxSolverRoeAcoustic { //extends FluxSolverRoe {

	/*public FluxSolverRoeAcoustic(Grid grid, EquationsAcoustic equations) {
		super(grid, equations);
	}*/

	//protected double[] fluxPassiveScalarAcoustic(double[] left, double[] right,	int direction) {
		/*
		 * in order to avoid copying this is a workaround for missing multiple inheritance
		 * fluxPassiveScalar stays protected and is an abstract method to be implemented
		 * if needed however this front-end can be used to access the same code
		 */
		
		//return fluxPassiveScalar(0,0,0,left, right, direction);
	//}
	
	/*@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right,	int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double v = 0;
		
		if (direction == Grid.X_DIR){ v = ((EquationsAcoustic) equations).vx; }
		if (direction == Grid.Y_DIR){ v = ((EquationsAcoustic) equations).vy; }
		if (direction == Grid.Z_DIR){ v = ((EquationsAcoustic) equations).vz; }
		
		for (int ii = 0; ii < res.length; ii++){
			s = sc[ii];
			res[i] = 0.5*v*(left[s] + right[s]) - 0.5 * Math.abs(v) * (right[s] - left[s]);
		}
		return res;
	}
	
	@Override
	public SquareMatrix getUpwindingMatrix(double[] left, double[] right, int direction) {
		int pos = 0; // to make compiler happy
		double vDir = 0.0;
		double c = ((EquationsAcoustic) equations).soundspeed();
		
		if (direction == Grid.X_DIR){ pos = 0; vDir = ((EquationsAcoustic) equations).vx(); }
		if (direction == Grid.Y_DIR){ pos = 1; vDir = ((EquationsAcoustic) equations).vy(); }
		if (direction == Grid.Z_DIR){ pos = 2; vDir = ((EquationsAcoustic) equations).vz(); }
		
		double[][] mat = new double[4][4];
		
		for (int i = 0; i <= 3; i++){
			for (int j = 0; j <= 3; j++){
				mat[i][j] = 0.0;
			}
		}
		
		mat[0][0] = Math.abs(vDir);
		mat[1][1] = Math.abs(vDir);
		mat[2][2] = Math.abs(vDir);
		mat[3][3] = ((EquationsAcoustic) equations).soundspeed();
		mat[pos][pos] = c;
		
		
		mat[pos][3  ] = vDir/c;
		mat[3  ][pos] = vDir*c;
		
		
		curMaxSignalSpeed = Math.max(((EquationsAcoustic) equations).soundspeed(), curMaxSignalSpeed);
		
		return new SquareMatrix(mat);
	}

	@Override
	public double[] averageFlux(double[] left, double[] right, int direction) {
		double[] leftF, rightF;
		SquareMatrix J = ((EquationsAcoustic) equations).getJacobian(direction);
		leftF = J.mult(left);
		rightF = J.mult(right);
		
		
		double[] res = new double[left.length];
    	for (int q = 0; q < left.length; q++){
    		res[q] = 0.5 * (leftF[q] + rightF[q]);
    	}
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Roe solver for the linear acoustic system";
	} */

}
