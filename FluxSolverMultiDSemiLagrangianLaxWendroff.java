
public class FluxSolverMultiDSemiLagrangianLaxWendroff extends
		FluxSolverMultiStepMultiDCartesianFirstOrder {

	public FluxSolverMultiDSemiLagrangianLaxWendroff(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			int timeIntegratorStep) {
		int quantities = ijk.length;
		// largely copy from FluxSolverOneStepMultiDSemiLagrangianSimplified
		
		double maxEVal;
		EquationsHydroIdeal eq = ((EquationsHydroIdeal) equations);
		
		double uStar, relaxationSpeed, PiStar;
		
		double dx = ((GridCartesian) grid).xSpacing();
		
		
		double[] flux1 = new double[quantities];
		double[] flux2 = new double[quantities];
		
		
		double uip1j   = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double uij     = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double uip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double uijp1   = ijp1k[EquationsHydroIdeal.INDEX_XMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double uip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double uijm1   = ijm1k[EquationsHydroIdeal.INDEX_XMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
		
		double vip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double vijp1   = ijp1k[EquationsHydroIdeal.INDEX_YMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double vij     = ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double vip1j   = ip1jk[EquationsHydroIdeal.INDEX_YMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double vip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double vijm1   = ijm1k[EquationsHydroIdeal.INDEX_YMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
				
		double pip1j   = eq.getPressureFromConservative(ip1jk);
		double pij     = eq.getPressureFromConservative(ijk);
		double pip1jp1 = eq.getPressureFromConservative(ip1jp1k);
		double pijp1   = eq.getPressureFromConservative(ijp1k);
		double pip1jm1 = eq.getPressureFromConservative(ip1jm1k);
		double pijm1   = eq.getPressureFromConservative(ijm1k);
		
		double cR = eq.getSoundSpeedFromConservative(ip1jk);
		double cL = eq.getSoundSpeedFromConservative(ijk);
		
		relaxationSpeed = Math.max(cR*ip1jk[EquationsHydroIdeal.INDEX_RHO], 
				cL*ijk[EquationsHydroIdeal.INDEX_RHO]);
		
		double diffP = 0.25*(pip1jp1 - pijp1 + 2.0*(pip1j - pij) + pip1jm1 - pijm1);
		double diffU = 0.25*(uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1);
		
		double diffV = 0.25*(vijp1 - vijm1 + vip1jp1 - vip1jm1);
		
		double sumP  = 0.25*(pip1jp1 + pijp1 + 2.0*(pip1j + pij) + pip1jm1 + pijm1);
		double sumU  = 0.25*(uip1jp1 + uijp1 + 2.0*(uip1j + uij) + uip1jm1 + uijm1);
		
		double uStarAverage = 0.5*sumU;
		double PiStarAverage = 0.5*sumP;
		
		double uStar2 = - 0.5*diffP/dx;
		double PiStar2 = - relaxationSpeed*relaxationSpeed/2*(diffU + diffV)/dx;
		
		uStar  = uStarAverage - 0.5/relaxationSpeed*diffP;
		PiStar = PiStarAverage - relaxationSpeed/2*(diffU + diffV);	
		
		flux1[EquationsHydroIdeal.INDEX_RHO]    = 0; 
		flux1[EquationsHydroIdeal.INDEX_XMOM]   = PiStar;
		flux1[EquationsHydroIdeal.INDEX_YMOM]   = 0;
		flux1[EquationsHydroIdeal.INDEX_ZMOM]   = 0;
		flux1[EquationsHydroIdeal.INDEX_ENERGY] = PiStar*uStar;//*/
		
		/*flux1[EquationsHydroIdeal.INDEX_RHO]    = 0; 
		flux1[EquationsHydroIdeal.INDEX_XMOM]   = PiStar1;
		flux1[EquationsHydroIdeal.INDEX_YMOM]   = 0;
		flux1[EquationsHydroIdeal.INDEX_ZMOM]   = 0;
		flux1[EquationsHydroIdeal.INDEX_ENERGY] = PiStar1*uStar1;
		flux2[EquationsHydroIdeal.INDEX_RHO]    = 0; 
		flux2[EquationsHydroIdeal.INDEX_XMOM]   = PiStar2;
		flux2[EquationsHydroIdeal.INDEX_YMOM]   = 0;
		flux2[EquationsHydroIdeal.INDEX_ZMOM]   = 0;
		flux2[EquationsHydroIdeal.INDEX_ENERGY] = PiStar1*uStar2 + PiStar2*uStar1;//*/
		
		
		maxEVal = relaxationSpeed/Math.min(ip1jk[EquationsHydroIdeal.INDEX_RHO], ijk[EquationsHydroIdeal.INDEX_RHO]) + Math.abs(uStar);
		
		
		// ---------- pressureless euler_:
		
		double average, diffX, diffY;
		//uStar = (uip1j + uij)/2;
		
				
		for (int q = 0; q < quantities; q++){
			//average = (ijk[q]*uij + ip1jk[q]*uip1j)/2;
			//average = uStar * (ijk[q] + ip1jk[q])/2;
			average = uStar * (ijp1k[q] + ip1jp1k[q] + 2.0*(ijk[q] + ip1jk[q]) + ijm1k[q] + ip1jm1k[q])/8;
			
			//diffX = ip1jk[q]*uip1j*uip1j - ijk[q]*uij*uij;
			diffX = (ip1jk[q] - ijk[q])*uStar*uStar;
			
			diffY = (ip1jp1k[q]*uip1jp1*vip1jp1 + ijp1k[q]*uijp1*vijp1 - ip1jm1k[q]*uip1jm1*vip1jm1 - ijm1k[q]*uijm1*vijm1)/4;
			//diffY = uStar * (ip1jp1k[q] + ijp1k[q] - ip1jm1k[q] - ijm1k[q])/4;
			//diffY = 0;
			
			flux1[q] += average;
			
			flux2[q] += - 0.5*(diffX + diffY)/dx;
			
		}
		
		
		
		return new FluxMultiStepAndWaveSpeed(flux1, flux2, maxEVal);
	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep) {
		if (fluxSolverStep == 0){ return 1.0; } else { return dt; }
	}

	@Override
	public int steps() { return 2; }

	@Override
	public String getDetailedDescription() {
		return "pressureless euler solved with lw and remaining acoustic operator solved in a low mach compliant fashion";
	}

}
