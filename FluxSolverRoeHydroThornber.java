import matrices.SquareMatrix;


public class FluxSolverRoeHydroThornber extends FluxSolverRoeHydro {

	

	public FluxSolverRoeHydroThornber(Grid grid, EquationsHydroIdeal equations, int meanType) {
		super(grid, equations, meanType);
	}

	// overriding method, no safe implementation possible as this method is in super.super
	protected SquareMatrix[] getDiagonalization(int i, int j, int k, double[] quantities, int direction){
		double thornberModificationFactor = Math.min(1.0, 
				10.0*((EquationsHydroIdeal) equations).getMachFromConservative(quantities));
		
		return ((EquationsHydroIdeal) equations).diagonalizationWithModification(
				i, j, k, quantities, direction, thornberModificationFactor);
	}
	
}
