
public class InitialDataHydroVortexYee extends InitialDataHydroVortex {

	protected double Gamma;
	protected double pOffset;
	
	public InitialDataHydroVortexYee(Grid grid, EquationsHydroIdeal equations, double[] offsetSpeed, double Gamma, double mach) {
		super(grid, equations, offsetSpeed, false);
		this.Gamma = Gamma;
		computePOffset(mach);
	}

	public InitialDataHydroVortexYee(Grid grid, EquationsHydro equations, double Gamma, double mach) {
		super(grid, equations);
		this.Gamma = Gamma;
		computePOffset(mach);
	}

	protected void computePOffset(double mach){
		pOffset = Math.max(0,  1.0 / mach / mach - 1.0);
	}
	
	@Override
	protected double speed(double r) {
		return Gamma / 2 / Math.PI * r * Math.exp((1.0 - r*r)/2);
	}

	@Override
	protected double pressure(double r) {
		return rho(r) * temperature(r) + pOffset;
	}

	@Override
	protected double rho(double r) {
		return Math.pow(temperature(r), 1.0 / (((EquationsHydroIdeal) equations).gamma() - 1));
	}

	protected double temperature(double r){
		double gamma = ((EquationsHydroIdeal) equations).gamma();
		return 1.0 - (gamma - 1) * Gamma * Gamma / 8 / gamma / Math.PI / Math.PI * Math.exp(1.0 - r*r);
	}
	
	@Override
	public String getDetailedDescription() {
		return "Yee et al. 1999 compactly supported stationary vortex as implemented in Lerat et al. 2007";
	}

}
