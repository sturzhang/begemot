import org.ejml.data.DMatrixRMaj;
import org.ejml.data.DMatrixSparseCSC;
import org.ejml.interfaces.linsol.LinearSolverSparse;
import org.ejml.sparse.FillReducing;
import org.ejml.sparse.csc.factory.LinearSolverFactory_DSCC;


public abstract class TimeIntegratorFiniteElement extends TimeIntegrator {

	// solving A \dot q + B q = 0
	
	protected LinearSolverSparse<DMatrixSparseCSC,DMatrixRMaj> solver;
	protected DMatrixSparseCSC leftMatrix, rightMatrix;
	
	public TimeIntegratorFiniteElement(Grid grid) {
		super(grid);
		solver = LinearSolverFactory_DSCC.qr(FillReducing.NONE);
	}

	public abstract DMatrixRMaj perform(double dt, DMatrixRMaj solutionNow, int multipleEstimateSparsity);
	
	// if needed this can be called each time step
	public void setLeftMatrix(DMatrixSparseCSC matrix){
		leftMatrix = matrix;
	}

	public void setRightMatrix(DMatrixSparseCSC matrix){
		rightMatrix = matrix; 
	}
}
