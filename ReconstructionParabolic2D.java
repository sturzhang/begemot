
public class ReconstructionParabolic2D extends ReconstructionParabolic {

	protected double[][][][] paramx1, paramx2, param0, paramy1, paramy2;
	
	
	public ReconstructionParabolic2D(boolean lockInterfaceValues) {
		super(lockInterfaceValues);
	}

	@Override
	protected void initializeParameters(int nx, int ny, int nz, int ndir, int nq) {
		param0  = new double[nx][ny][nz][nq];
		paramx1 = new double[nx][ny][nz][nq];
		paramx2 = new double[nx][ny][nz][nq];
		paramy1 = new double[nx][ny][nz][nq];
		paramy2 = new double[nx][ny][nz][nq];
	}

	@Override
	protected double[] eval(GridCell g, int direction, int nq, double pos) {
		double[] res = new double[nq];
		for (int q = 0; q < res.length; q++){
			res[q] = param0[g.i()][g.j()][g.k()][q];
			if (direction == GridCartesian.X_DIR){
				res[q] += paramx1[g.i()][g.j()][g.k()][q]*pos + 
						  paramx2[g.i()][g.j()][g.k()][q]*pos*pos;
			} else if (direction == GridCartesian.Y_DIR){
				res[q] += paramy1[g.i()][g.j()][g.k()][q]*pos + 
						  paramy2[g.i()][g.j()][g.k()][q]*pos*pos;
			} else {
				System.err.println("ERROR: 'Bi'-Parabolic reconstruction not implemented for grid of more than two dimensions");				
			}
		}
		return res;
	}

	@Override
	protected void setParameters(GridCell g,
			double[][][] interfaceValuesOrderedByDirection, double[] quantities) {
		double dx, dy;
		
		dx = ((GridCartesian) grid).xSpacing();
		dy = ((GridCartesian) grid).ySpacing();
		
		double L, R, T, B;
		
		for (int q = 0; q < quantities.length; q++){

			L = interfaceValuesOrderedByDirection[GridCartesian.X_DIR][NEGATIVE][q];
			R = interfaceValuesOrderedByDirection[GridCartesian.X_DIR][POSITIVE][q];
			if (grid.dimensions() > 1){
				B = interfaceValuesOrderedByDirection[GridCartesian.Y_DIR][NEGATIVE][q];
				T = interfaceValuesOrderedByDirection[GridCartesian.Y_DIR][POSITIVE][q];
			} else {
				T = (-L + 6.0*quantities[q] - R)/4;
				B = T;
				dy = 1.0;
			}
			
			param0[g.i()][g.j()][g.k()][q] = 0.5*(-B - L + 6.0*quantities[q] - R - T);
			paramx1[g.i()][g.j()][g.k()][q] = (R - L) / dx;
			paramx2[g.i()][g.j()][g.k()][q] = 2.0* (B + 2.0*(L - 3.0*quantities[q] + R) + T) / dx/dx;
					
			paramy1[g.i()][g.j()][g.k()][q] = (T - B) / dy;
			paramy2[g.i()][g.j()][g.k()][q] = 2.0 * (L + 2.0*(B - 3.0*quantities[q] + T) + R) / dy/dy;
		}
	}
	
	@Override
	public String getDetailedDescription() {
		return "Continuous parabolic recontruction in 2-d: conservative and agrees with interface values in four points";
	}

}
