
public abstract class InitialDataIsentropic extends InitialData {

	
	protected EquationsIsentropicEuler equations;
	
	public InitialDataIsentropic(Grid grid, EquationsIsentropicEuler equations) {
		super(grid);
		this.equations = equations;
	}
	

}
