
public class InitialDataHydroShocktubeToroEntropyTest extends
		InitialDataHydroShocktube {

	public InitialDataHydroShocktubeToroEntropyTest(Grid grid,
			EquationsHydroIdeal equations) {
		// toro 1 (?)
		super(grid, equations);
	}

	// gamma = 5/3 
	
	@Override
	public double leftDensity() {  return 1.0; }
	@Override
	public double leftVx() {       return 0.75; }
	@Override
	public double leftPressure() { return 1.0; }

	@Override
	public double rightDensity() {  return 0.125; }
	@Override
	public double rightVx() {       return 0.0; }
	@Override
	public double rightPressure() { return 0.1; }

}
