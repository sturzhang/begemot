
public class ObstacleNone extends Obstacle { //old: ObstacleHydro {

	public ObstacleNone(Grid grid) {
		// dirty hack
		// old: super(grid, new EquationsHydroIdeal(0, 2.0));
		super(grid);
	}

	@Override
	public String getDetailedDescription() {
		return "No obstacle";
	}

	@Override
	protected void setObstacleBoundaries() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {
		// nothing
	}

	@Override
	public void set(double[][][][] conservedQuantities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double[][][][][] insertRigidWalls(double[][][][][] interfaceFluxes,
			double[][][][] conservedQuantities) {
			return interfaceFluxes;
	}

}
