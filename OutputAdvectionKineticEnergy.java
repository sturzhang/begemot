
public class OutputAdvectionKineticEnergy extends Output {

	public OutputAdvectionKineticEnergy(double outputTimeInterval, GridCartesian grid, EquationsAdvection equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints sum of (quantity^2/2) over all cells and its mean over time";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double kinen = 0;
		
		for (int i = 0; i < quantities.length; i++){
			for (int j = 0; j < quantities[i].length; j++){
				for (int k = 0; k < quantities[i][j].length; k++){
					kinen += 0.5 * Math.pow(quantities[i][j][k][0], 2);
				}
			}
		}
		
		println(time + " " + kinen);

	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
