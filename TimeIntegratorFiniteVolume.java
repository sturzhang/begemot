public abstract class TimeIntegratorFiniteVolume extends TimeIntegrator {
	
    public abstract int steps();
    
    
    public TimeIntegratorFiniteVolume(Grid grid){
    	super(grid);
    }

    
    /*public double[][][][] perform(int step, double dt, 
    		double[][][][][][] interfaceFluxes, double[][][][] sources, double[][][][] conservedQuantities){
    	
    	for (int fluxSolverStep = 0; fluxSolverStep < interfaceFluxes.length; fluxSolverStep++){
    		// TODO we need to pay attention to prefactors (easy to insert via a rescaling of dt)
    		conservedQuantities = perform(step, dt, interfaceFluxes[fluxSolverStep], sources, conservedQuantities);
    	}
    	return conservedQuantities;
    }*/
    
    public abstract double[][][][] perform(int step, double dt, 
    		double[][][][][] interfaceFluxes, double[][][][] sources, double[][][][] conservedQuantities,
    		boolean[][][] excludeFromTimeIntegration);
    
    public abstract double getDtSuggestion(double maxSignalSpeed, Grid grid);
    
    
    

}