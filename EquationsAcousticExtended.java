import matrices.DiagonalMatrix;
import matrices.SquareMatrix;


public class EquationsAcousticExtended extends EquationsAcoustic {

	public static final int INDEX_RHO = 4;
	
	public EquationsAcousticExtended(Grid grid, int numberOfPassiveScalars, double soundspeed) {
		super(grid, numberOfPassiveScalars, soundspeed);
	}

	@Override
	public SquareMatrix getJacobian(int direction){
		int pos = 0; // to make compiler happy
		if (direction == GridCartesian.X_DIR){ pos = INDEX_VX; }
		if (direction == GridCartesian.Y_DIR){ pos = INDEX_VY; }
		if (direction == GridCartesian.Z_DIR){ pos = INDEX_VZ; }
		
		double[][] J = new double[getNumberOfConservedQuantities()][getNumberOfConservedQuantities()];
		
		J[INDEX_RHO][pos] = 1.0;
		J[pos][INDEX_P]   = 1.0;
		J[INDEX_P][pos]   = soundspeed*soundspeed;
				
		return new SquareMatrix(J);
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		SquareMatrix[] mat = new SquareMatrix[3];
		double arrmat[][][] = new double[2][getNumberOfConservedQuantities()][getNumberOfConservedQuantities()];
		double[] eval = new double[getNumberOfConservedQuantities()];
		
		int pos = 0; // to make compiler happy
		double c = soundspeed;
		
		if (direction == GridCartesian.X_DIR){ pos = INDEX_VX; }
		if (direction == GridCartesian.Y_DIR){ pos = INDEX_VY; }
		if (direction == GridCartesian.Z_DIR){ pos = INDEX_VZ; }
		
		for (int ii = 0; ii <= 3; ii++){
			for (int jj = 0; jj <= 3; jj++){
				arrmat[0][ii][jj] = 0.0;
				arrmat[1][ii][jj] = 0.0;
			}
			eval[ii] = 0.0;
		}
		
		// matrix of (right) eigenvectors
		// diagonal components:
		arrmat[0][0][0] = 1.0;
		arrmat[0][1][1] = 1.0;
		arrmat[0][2][2] = 1.0;
		arrmat[0][3][3] = 1.0;
		arrmat[0][4][4] = 1.0;
		arrmat[0][pos][pos] = -c;
		arrmat[0][INDEX_P][INDEX_P] = c*c;
		
		// offdiagonal components:
		arrmat[0][pos][INDEX_P]     = c;
		arrmat[0][INDEX_P][pos]    =  c*c;
		arrmat[0][INDEX_RHO][pos]     = 1.0;
		arrmat[0][INDEX_RHO][INDEX_P] = 1.0;
		
		// matrix of left eigenvectors
		// diagonal components:
		arrmat[1][0][0] = 1.0;
		arrmat[1][1][1] = 1.0;
		arrmat[1][2][2] = 1.0;
		arrmat[1][3][3] = 1.0;
		arrmat[1][4][4] = 1.0;
		arrmat[1][pos][pos] = -1.0/c/2;
		arrmat[1][INDEX_P][INDEX_P] = 1.0/2/c/c;
		
		// off-diagonal
		arrmat[1][pos][INDEX_P] =  0.5/c/c;
		arrmat[1][INDEX_P][pos] =  0.5/c;
		arrmat[1][INDEX_RHO][INDEX_P] = -1.0/c/c;
		
		
		eval[pos]       = -c;
		eval[INDEX_P]   =  c;
		
		mat[0] = new SquareMatrix(arrmat[0]);
		mat[1] = new DiagonalMatrix(eval);
		mat[2] = new SquareMatrix(arrmat[1]);

		return mat;
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		return 0;
	}

	@Override
	public int getNumberOfConservedQuantities() {
		return 5;
	}

	@Override
	public void transformFixedParameters(double[] normalVector) {
		// nothing to do
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// nothing to do
	}

	@Override
	public String getDetailedDescription() {
		return "Extended acoustic system (derived as the linearization of hydrodynamics in primitive variables rho-v-p) with a constant soundspeed of " + 
				soundspeed;
	}

}
