
public abstract class FluxSolverMultiStepMultiDCartesian extends FluxSolverMultiStepCartesian {

	public FluxSolverMultiStepMultiDCartesian(GridCartesian grid, Equations equations, StencilTurner stencilTurner) {
		super(grid, equations, stencilTurner);
	}
	
	
}
