import java.util.Random;


public class Begemot {
    
	
	
    public static void main(String[] args){
    	
    	double mach      = 0.0001;
    	double finaltime = 0.6; // 0.05;

    	
    	//performConvergenceTests(mach, finaltime, 21, 401, 8);
    	
    	setupFiniteVolume(mach, finaltime, 50, 0.000, 0.5);  	

    	//setupFiniteElement(mach);
    	
    	/*for (mach = 0.001; mach < 1.1; mach *= 2.0){
    		setup(mach, finaltime, 501, 0.000);
    	}*/
    	
    	/*for (double cflNumber = 0.05; cflNumber < 1.5; cflNumber +=0.05){
    		System.out.print(cflNumber + " ");
    		setup(mach, finaltime, 501, 0.000, cflNumber); 
    	}*/
    	
    	
    	/*for (double xVelocity = 0.0; xVelocity < 1.5; xVelocity +=0.1){
    		System.out.print(xVelocity + " ");
    		setup(mach, finaltime, 501, xVelocity, 0.5); 
		}//*/
    	
    	/*for (double xVelocity = 0.001; xVelocity <= 1.0; xVelocity += (xVelocity < 0.05 ? 0.005 : 0.05)){
    		System.out.println(xVelocity);
    		setup(mach, finaltime, 1, xVelocity);  	
    		
    		System.out.println(" ");
    		System.out.println(" ");
    	} //*/
    	
    	
    	 
    	//(new TestSeriesAdvection()).run();
    	//(new TestSeriesAcoustics()).run();
    }

    private static void performConvergenceTests(double mach, double finaltime, int gridsizestart, int gridsizeend, 
    		int numberOfPoints){
    	int gridsize;
    	
    	for (int i = 0; i < numberOfPoints; i++){
    		gridsize = (int) Math.round(gridsizestart * Math.pow(1.0 * gridsizeend / gridsizestart, 1.0*i/(numberOfPoints-1)));
    		gridsize = ((gridsize/2)*2 == gridsize) ? gridsize+1 : gridsize;
    		
    		setupFiniteVolume(mach, finaltime, gridsize, 0.0, 0.85);
    	}
    
    }
    
    
    public static void setupFiniteElement(double mach){
    	IterationFiniteElement iteration = new IterationFiniteElement();
    	
    	//Grid1D grid = new Grid1D(0.0, 200.0, 200, 0); 
    	GridCartesian2D grid = new GridCartesian2D(0.0, 1.0, 40, 
    			0.0, 1.0, 40,      0);   //*/ 
    	

    	iteration.setGrid(grid);
    	iteration.setOutputPath("dat/testiii.dat");
    	
    	// temporary equations here
    	//EquationsAdvectionConstant equations = new EquationsAdvectionConstant(grid, 0, new double[] {1.0, 0.5, 0});
    	EquationsAcousticSimple equations = new EquationsAcousticSimple(grid, 0, 1.0, false, new double[] {0.0, 0.0, 0});
    	iteration.equations = equations;
    	
    	//setups.initializeArrays(grid); // TODO initialization has to happen somehow!
    	grid.conservedQuantities      = grid.generateEmptyArray( equations.getTotalNumberOfConservedQuantities() );

    	
    	//iteration.initialData = new InitialDataAdvectionGaussian(grid, equations, 1.0, 0.2, 0.0);
    	//iteration.initialData = new InitialDataAdvectionSineWave(grid, equations, 1.0, 100.0/16, 100.0/8);
    	iteration.initialData = new InitialDataAcousticGaussianGresho(grid, equations, 0.0, 0.2, 0.0, 1.0/mach/mach);
    	
    	iteration.timeIntegrator = new TimeIntegratorFiniteElementForwardEuler(grid);
    	//iteration.timeIntegrator = new TimeIntegratorFiniteElementBackwardEuler(grid);
    	
    	iteration.run(3000.0);
    }

    
            
    public static void setupFiniteVolume(double mach, double finaltime, int gridsize, double offsetVelocity, double cflNumber){
    	IterationFiniteVolume iteration = new IterationFiniteVolume();
    	//iteration.onlyFinalOutput = true;
    	//iteration.printDescriptions = false;

    	
    	Setup hydroSetup = new Setup();
    	
    	
    	Reconstruction reconstruction    = new ReconstructionPiecewiseConstant();
    	//Reconstruction reconstruction    = new ReconstructionPiecewiseConstant(2);
    	//ReconstructionParabolic reconstruction    = new ReconstructionParabolic1D(true);
    	//ReconstructionParabolic reconstruction    = new ReconstructionParabolic2D(true);
    	//ReconstructionParabolic reconstruction    = new ReconstructionParabolic1DModified(true);
    	//Reconstruction reconstruction     = new ReconstructionWENO(1);  // WENO3
    	//Reconstruction reconstruction     = new ReconstructionWENO(2);  // WENO5
    	hydroSetup.reconstruction = reconstruction;
    	
    	
    	
    	Grid1D grid = new Grid1D(-1.0, 5.0, 4000,
				 hydroSetup.reconstruction.numberOfGhostcells()); //*/
    	/*Grid1D grid = new Grid1D(-1.0, 2.0, 200,
    			hydroSetup.reconstruction.numberOfGhostcells()); //*/
    	/*GridCartesian2D grid = new GridCartesian2D(-1.0, 2.0, 3000,  0.0, 1.0, 3,  
				 hydroSetup.reconstruction.numberOfGhostcells());   //*/ 
    	/*GridCartesian2D grid = new GridCartesian2D(0.0, 1.0, gridsize, 0.0, 1.0, gridsize,
				 hydroSetup.reconstruction.numberOfGhostcells());   //*/
    	/*GridTriangularPseudoCartesianFromPoints grid = new GridTriangularPseudoCartesianFromPoints(0.0, 1.0, 101, 0.0, 1.0, 101,
				 hydroSetup.reconstruction.numberOfGhostcells());   //*/
    	//GridFittedCircle grid = new GridFittedCircle(1.0, 4.0, 50, 100, hydroSetup.reconstruction.numberOfGhostcells());
    	/*GridCartesian2D grid = new GridCartesian2D(0.0, 1.0, 1,   0.0, 1.0, 101,
				 hydroSetup.reconstruction.numberOfGhostcells());   //*/
    	/*GridCartesian2D grid = new GridCartesian2D(0.0, 0.06, 3, 0.0, 0.06, 3,
				 hydroSetup.reconstruction.numberOfGhostcells());   //*/
    	/*GridCartesian3D grid = new GridCartesian3D(0.0, 1.0, 41,  0.0, 1.0, 41,  0.0, 1.0, 41, 
				 hydroSetup.reconstruction.numberOfGhostcells());   //*/
    	/*GridCartesian2D grid = new GridCartesian2D(0.0, 1.0, gridsize, 0.0, 1.0, gridsize,
    			 hydroSetup.reconstruction.numberOfGhostcells());   //*/
    	
    	
    	
    	
    	iteration.setGrid(grid);
    	hydroSetup.reconstruction.setGrid(iteration.grid);
    	
    	
    	//EquationsAdvectionConstant equations     = new EquationsAdvectionConstant(grid, 0, new double[] {0.4, 0.1, 0.0});
    	//EquationsAdvectionGresho equations         = new EquationsAdvectionGresho(grid, 0, 0.2);
    	/*EquationsAcousticSimple equations              = new EquationsAcousticSimple(grid, 0, 1.0 / mach, false, 
    			new double[] {0.0, 0.0, 0.0}); //*/
    	//EquationsAcousticExtended equations      = new EquationsAcousticExtended(grid, 0, 1.0/mach);
    	EquationsHydroIdeal equations            = new EquationsHydroIdeal(grid, 0, 1.4);
    	//EquationsHydroIdeal equations            = new EquationsHydroIdeal(grid, 0, 2.0);
    	//incomplete EquationsHydroIdealWith1dDeriv equations = new EquationsHydroIdealWith1dDeriv(grid, 0, 1.4, Grid.Y_DIR);
    	//EquationsScalar equations                = new EquationsScalarGuckenheimer(grid, 0);
    	//EquationsScalar equations                = new EquationsScalarBurgers(grid, 0);
    	//EquationsIsentropicEuler equations       = new EquationsIsentropicEuler(grid, 0, 2.0, 0.5);
    	//EquationsMaxwell equations               = new EquationsMaxwell(grid, 0);
    	//EquationsPressurelessEuler equations     = new EquationsPressurelessEuler(grid, 0);
    	/*EquationsLinearizedEuler equations       = new EquationsLinearizedEuler(grid, 0, 1.4, 
    			new InitialDataHydroGresho(grid, new EquationsHydroIdeal(grid, 0, 1.4), mach, 1.0, 1.0, new double[] {0.0, 0.0, 0.0})); //*/
    	/*EquationsLinearizedEuler equations       = new EquationsLinearizedEuler(grid, 0, 1.4, 
    			new InitialDataSinusoidal(grid, 0.25, 100000.0, 
    					new double[] {0.0, 0.0, 0.1, 0.0, 0.0},
    					new double[] {1.0, 0.0, 0.0, 0.0, 1.0}, 0.0)); //*/
    	hydroSetup.equations = equations;
    	

    	//SourceRBV2 source                 = new SourceRBV2(grid, equations);
    	// ----- maxwell source ----------------------------------------------------------------------------------
    	//Source source                     = new SourceMaxwellDipole(grid, equations, 2.0*Math.PI, 0.1);
    	// ----- acoustic source ----------------------------------------------------------------------------------
    	//SourceAcousticConstantGravity source             = new SourceAcousticConstantGravity(grid, equations, new double[]{-1.0, 0, 0}, true); 
    	//SourceAcousticExtendedConstantGravity source     = new SourceAcousticExtendedConstantGravity(grid, equations, new double[]{-1.0, 0, 0}, true);
    	// ----- hydrodynamics source -----------------------------------------------------------------------------
    	/*SourceConstantGravity tmpsource   = new SourceConstantGravity(iteration.grid, equations.getHydroEquations(), new double[]{0,-1.0, 0});
    	SourceGravityWithDerivatives source = new SourceGravityWithDerivatives(iteration.grid, equations, tmpsource); // */
    	//SourceConstantGravity source      = new SourceConstantGravity(grid, equations, new double[]{-1.0, 0, 0}, false); 
    	//SourceConstantGravity source      = new SourceConstantGravity(grid, equations, new double[]{0, -0.03, 0}, false); // 0.001 too little -0.05 quite good 0.1 still too much
    	//SourceSphericalGravity source     = new SourceSphericalGravity(iteration.grid, equations, 5000.0);
    	//Source source                     = new SourceGradientTestForCheckerboard(iteration.grid, equations, -1.0, +0.5, 1.0);
    	//SourceIsentropicEulerLikeInShallowWater source = new SourceIsentropicEulerLikeInShallowWater(grid, equations);
    	Source source                     = new SourceZero(iteration.grid); 
    	hydroSetup.source = source;
  
    	
     	
    	hydroSetup.obstacle           = new ObstacleNone(grid); 
    	//hydroSetup.obstacle           = new ObstacleHydroSphericalSource(grid, equations, 0.1,   2.0, 2.0,   2.0);
    	//hydroSetup.obstacle           = new ObstacleAcousticCylinder(grid, equations, 0.2);
    	//hydroSetup.obstacle           = new ObstacleHydroIsentropicCylinder(grid, equations, 0.2);
    	//hydroSetup.obstacle           = new ObstacleHydroCylinder(grid, equations, 0.2);
    	//hydroSetup.obstacle           = new ObstacleSquare(grid, equations, 0.4);
    	//doesn't work hydroSetup.obstacle           = new ObstacleLineSegment(grid, 2.0);
    	//hydroSetup.obstacle           = new ObstacleStep(grid, equations, 1.5, 1.0);
    	

    	//hydroSetup.initialData       = new InitialDataZero(grid, 1e-10);
    	//hydroSetup.initialData       = new InitialDataCheckerboard(grid, 1e-6);
    	//hydroSetup.initialData       = new InitialDataFourierMode(grid, 0.086, 0.172, new double[] {1.0, 1.0, 1.0, 0.0, 1.0});
    	// ----- scalar initial data -----------------------------------------------------------------------
    	//hydroSetup.initialData       = new InitialDataScalarGuckenheimer(grid, equations);    	
    	//hydroSetup.initialData       = new InitialDataScalarGaussian(grid, equations);
    	//hydroSetup.initialData       = new InitialDataScalarContinuousRiemannProblem(grid, equations);
    	// ----- advection iniial data -----------------------------------------------------------------------
    	//hydroSetup.initialData       = new InitialDataAdvectionGaussian(grid, equations, 1.0, 0.1, 0.0);
    	//hydroSetup.initialData       = new InitialDataAdvectionJump(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAdvectionSphericalJump(grid, equations);
    	//hydroSetup.initialData        = new InitialDataGreshoVorticity(grid, equations);
    	//hydroSetup.initialData        = new InitialDataAdvectionStationaryGaussian(grid, equations, 1.0, 0.3, 0.5);
    	// ----- acoustic initial data ----------------------------------------------------------------------------------
    	/*hydroSetup.initialData       = new InitialDataAcousticConst(grid, equations, 1.0,
    			new double[] {1.0, 0.0, 0.0});//*/
    	//hydroSetup.initialData       = new InitialDataAcousticDivFree(grid, equations, 1.0, 5.0);
    	//hydroSetup.initialData       = new InitialDataAcousticSphericalShock(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticQuadraticVortex(grid, equations, 0.4, 1.0, 1.0/mach/mach);
    	//hydroSetup.initialData       = new InitialDataAcousticGaussianGresho(grid, equations, 0.0, 0.2, 0.0, 1.0/mach/mach);
    	//hydroSetup.initialData       = new InitialDataAcousticDeltaPeak(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticFourRegionsRiemannProblem(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticMultiDRiemannProblem(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticSixRegionsRiemannProblem(grid, equations, ...);
    	//hydroSetup.initialData       = new InitialDataFourRegionsDivergence(grid, equations, 7.0, 0.0, 0.0, 0.0);
    	//hydroSetup.initialData       = new InitialDataAcousticPertubationEvolution(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticAtmosphere(grid, equations, source.g()[0], true);
    	//hydroSetup.initialData       = new InitialDataAcousticExtendedAtmosphere(grid, equations, source.gy(), 1.0, 1.4, 1.0);
    	//hydroSetup.initialData       = new InitialDataAcousticExtendedRiemannProblem(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticExtendedDelta(grid, equations);
    	//hydroSetup.initialData       = new InitialDataAcousticExtendedGaussian(grid, equations, 0.15);
    	// ----- hydrodynamic initial data ------------------------------------------------------------------------------
    	//hydroSetup.initialData       = new InitialDataHydroVortexChalons(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroForwardFacingStepSlipLine(grid, equations);
    	/*hydroSetup.initialData       = new InitialDataSinusoidal(grid, 0.25, 100000.0, 
    					new double[] {0.0, 0.0, 0.1, 0.0, 0.0},
    					new double[] {1.0, 0.0, 0.0, 0.0, 1.0}, 0.0);//*/
    	//hydroSetup.initialData       = new InitialDataHydroLineSegmentFlow(grid, equations, Math.PI / 6, 1.0, boundary);
    	//hydroSetup.initialData       = new InitialDataHydroMultiDRiemannProblem(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroTriplePoint(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroSodShock(grid, equations);
    	hydroSetup.initialData       = new InitialDataHydroRiemannProblemsFVIllustration(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroLaxShocktube(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroStrongRarefaction(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroShocktubeLevequeEntropyTest(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroShocktubeToroEntropyTest(grid, equations);
    	//hydroSetup.initialData       = new InitialDataDelaunay(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroShocktubeIsentropic(grid, equations, 1.0, 0.0, 1.0, 0.1, 0.0);
    	//hydroSetup.initialData       = new InitialDataHydroObliqueShock(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroSphericalShock(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroCheckerboard(grid, equations, 1.0, 0.1,    0.3, 0.0,   10.0, 0.0); 
    	//hydroSetup.initialData       = new InitialDataHydroStationaryForSphericalGravity(grid, equations, source.prefactor());
    	//hydroSetup.initialData       = new InitialDataHydroGresho(grid, equations, mach, 1.0, 1.0, new double[] {0.0, 0.0, 0.0}, false);
    	//hydroSetup.initialData       = new InitialDataHydroTaylorGreen(grid, equations, mach);
    	//hydroSetup.initialData       = new InitialDataHydroStream(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroGreshoIsentropic(grid, equations, mach, 1.0, new double[] {0,0,0});
    	//hydroSetup.initialData       = new InitialDataHydroVortexInteraction(grid, equations, 1.0, mach, 0.4);
    	//hydroSetup.initialData       = new InitialDataHydroQuadraticVortex(grid, equations, mach, 1.0);
    	//hydroSetup.initialData       = new InitialDataHydroExpoVortex(grid, equations, mach, 1.0);
    	//hydroSetup.initialData       = new InitialDataHydroVortexYee(grid, equations, 5.0, mach);
    	//hydroSetup.initialData       = new InitialDataHydroEllipticVortex(grid, equations, 0.75, mach);
    	//hydroSetup.initialData       = new InitialDataHydroFlyingBlop(grid, equations, 0.2); 
    	//hydroSetup.initialData       = new InitialDataHydroNoFlow(grid, equations, 1.0, 1.0/mach/mach);
    	//hydroSetup.initialData       = new InitialdataHydroAtmosphereConstDensity(grid, equations, 1.0, 2.0, source.gy(), true);
    	//hydroSetup.initialData       = new InitialDataHydroAtmosphereIsothermal(grid, equations, 1.0, 1.0, source.gx(), 0.0);
    	//hydroSetup.initialData       = new InitialDataHydroAtmospherePolytropic(grid, equations, 1.0, 1.0, source.gy(), 0.0, 1.8);
    	//hydroSetup.initialData       = new InitialDataHydroAtmosphereTwoTemperatures(grid, equations, source.gy(), offsetVelocity);
    	/*double Tgradient = 4.0;
    	hydroSetup.initialData       = new InitialDataHydroAtmosphereLinearTemperature(grid, equations, tmpsource.gy(), true, Tgradient); //*/
    	//hydroSetup.initialData       = new InitialDataHydroRayleighTaylor(grid, equations, 1.0, 1.5, 1.0, 0.05, source.gy());
    	//hydroSetup.initialData       = new InitialDataHydroShearFlow(grid, equations);
    	//hydroSetup.initialData       = new InitialDataHydroKelvinHelmholtz(grid, equations, 0.1, 1.0, 5.0, 0.1); 
    	//hydroSetup.initialData       = new InitialDataHydroKelvinHelmholtzSimple(grid, equations, 0.1, 5.0, 0.001, 0.99, 1.01);
    	//hydroSetup.initialData       = new InitialDataHydroKelvinHelmholtzWithGravity(grid, equations, 0.1, 1.0, 0.001, 0.01, 0.1, source.gy());
    	/*hydroSetup.initialData       = new InitialDataHydroConstFlow(grid, equations, 1.0, 1.0, 
    		new double[]{0.01, 0.0, 0.0}); //*/
    	//hydroSetup.initialData       = new InitialDataHydroSoundwave(grid, equations, mach, 0.5);
    	/*hydroSetup.initialData       = new InitialDataHydroTornado(grid, equations, 
    			new InitialDataHydroGresho(grid, equations, mach, 1.0, 1.0), 
    			source.gz(), 0.3);// */
    	// ----- pressureless hydrodynamic initial data ----------------------------------------------------------------------------
    	//hydroSetup.initialData       = new InitialDataPressurelessRiemannProblem(grid, equations);
    	// ----- isentropic hydrodynamic initial data ------------------------------------------------------------------------------
    	//hydroSetup.initialData       = new InitialDataLakeAtRest(grid, equations, source, 1.0);
    	//hydroSetup.initialData       = new InitialDataIsentropicSodShock(grid, equations);
    	//hydroSetup.initialData       = new InitialDataIsentropicIsoenergeticShocktube(grid, equations);
    	//hydroSetup.initialData       = new InitialDataIsentropicSimpleShock(grid, equations, 0.2, 0.378, -0.466); // 0.0, 0.378, -0.486
    	//hydroSetup.initialData       = new InitialDataIsentropicSoundwave(grid, equations, 1.0, 0.01, 0.04, 0.2, 0.3);
    	//hydroSetup.initialData       = new InitialDataIsentropicGresho(grid, equations, mach, new double[] {0.0, 0, 0});
    	/*hydroSetup.initialData       = new InitialDataConstFlowIsentropic(grid, equations, 1.0, 
				new double[]{0.1, 0.0, 0.0}); //*/
    	//hydroSetup.initialData       = new InitialDataIsentropicGaussianPertubation(grid, equations, 0.4, 1.0, 0.05);
    	//hydroSetup.initialData       = new InitialDataIsentropicTaylorGreen(grid, equations);
    	//hydroSetup.initialData       = new InitialDataIsentropicKelvinHelmholtz(grid, equations, 0.1, 1.0, 0.1);
    	//hydroSetup.initialData       = new InitialDataIsentropicDelaunay(grid, equations);
    	// ----- maxwell initial data ----------------------------------------------------------------------------------------------
    	//hydroSetup.initialData       = new InitialDataMaxwellZero(grid, equations);
    	//hydroSetup.initialData       = new InitialDataMaxwellGaussian(grid, equations);
    	//hydroSetup.initialData       = new InitialDataMaxwellWave(grid, equations);
    	
    	
       	Boundary boundary               = new BoundaryPeriodic(grid);
    	//Boundary boundary               = new BoundaryZeroGradient(iteration.grid);
    	//Boundary boundary               = new BoundaryFixedToInitial(grid, hydroSetup.initialData);
    	//Boundary boundary               = new BoundaryZeroGradientReflectiveBottom(grid, equations);
    	/*Boundary boundary               = new BoundaryInflowHorizontalZeroGradientVertical(iteration.grid, 
    			new double[] {1.0, 0.1, 0.0, 0.0, equations.getEnergy(1.0, 0.01)}); //*/
    	/*Boundary boundary               = new BoundaryInflowHorizontalZeroGradientVertical(iteration.grid, 
    			new double[] {1.0, 0.1, 0.0, 0.0}); //*/
    	/*Boundary boundary               = new BoundaryInflowHorizontalZeroGradientVertical(iteration.grid, 
    			new double[] {1.0, 0.0, 0.0, 1.0}); //*/
    	/*Boundary boundary               = new BoundaryInflowOutflowHydroCharacteristic(grid, equations, 1.0, 1.0,
    			new double[] {0.1, 0, 0}); //*/
    	/*Boundary boundary               = new BoundaryInflowOutflowHydro(grid, equations, 1.0, 1.0,
    			new double[] {0.1, 0, 0}); //*/
    	/*Boundary boundary               = new BoundaryZeroGradientWithBottomAndCeiling(iteration.grid, 
    			new double[]{1.0, 0.0, 0.0, 0.0, 1.0 / (equations.gamma-1)}, 
    			new double[]{1.5, 0.0, 0.0, 0.0, (1.0) / (equations.gamma-1)}); //*/
    	//Boundary boundary               = new BoundaryDensityGradientAdvection(grid, equations, hydroSetup.initialData, 0.0);
    	//Boundary boundary               = new BoundaryBodyFitted(grid, hydroSetup.initialData, equations);
    	//Boundary boundary               = new BoundaryFixedToInitialHorizontalZeroGradientVertical(grid, hydroSetup.initialData);
    	//Boundary boundary               = new BoundaryFixedToInitialVerticalPeriodicHorizontal(grid, hydroSetup.initialData);
    	//Boundary boundary               = new BoundaryFixedToInitialVerticalZeroGradientHorizontal(grid, hydroSetup.initialData);
    	//Boundary boundary               = new BoundaryFixedToInitialButZeroGradientOnTheLeft(grid, hydroSetup.initialData);
    	/*Boundary boundary               = new BoundaryZeroGradientWithBottomAndCeiling(iteration.grid, 
    			new double[]{Math.pow(1+Tgradient*grid.getY(0), -1+source.gy()/Tgradient),                          0.0, 0.0, 0.0, 
				     Math.pow(1+Tgradient*grid.getY(0), source.gy()/Tgradient) / (equations.gamma-1)}, 
    			new double[]{Math.pow(1+Tgradient*grid.getY(grid.lastGhostCellYIndex()), -1+source.gy()/Tgradient), 0.0, 0.0, 0.0, 
    						Math.pow(1+Tgradient*grid.getY(grid.lastGhostCellYIndex()), source.gy()/Tgradient) / (equations.gamma-1)}); //*/
    	/*Boundary boundary               = new BoundaryZeroGradientWithBottomAndCeiling(iteration.grid, 
    			new double[]{1.0, 0.0, 0.0, 0.0, (2.0 - grid.getY(0)) / (equations.gamma-1)}, 
    			new double[]{1.0, 0.0, 0.0, 0.0, (2.0 - grid.getY(grid.lastGhostCellYIndex())) / (equations.gamma-1)}); //*/
    	/*Boundary boundary               = new BoundaryZeroGradientWithBottomAndCeiling(iteration.grid, 
    			new double[]{0.0, 0.0, 0.0, 1.0+grid.ymin-grid.getY(0)}, 
    			new double[]{0.0, 0.0, 0.0, 1.0+grid.ymin-grid.getY(grid.lastGhostCellYIndex())}); //*/
    	//Boundary boundary               = new BoundaryReflective((GridCartesian) iteration.grid, equations);
    	//Boundary boundary               = new BoundaryReflectiveVerticalPeriodicHorizontal(grid, equations);
    	//BoundaryLineSegmentFlow boundary  = new BoundaryLineSegmentFlow(iteration.grid, Math.PI / 6, 1.0); 
    	hydroSetup.boundary = boundary;

    	
    	
    	// ----- general solvers
    	//FluxSolver fluxSolver        = new FluxSolverCentral(grid, equations);
    	FluxSolver fluxSolver        = new FluxSolverRoe(iteration.grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRusanov(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverHLL(iteration.grid, equations, false);
    	//FluxSolver fluxSolver        = new FluxSolverRoeEntropyFixHartenHyman(iteration.grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRoeHLLEfix(iteration.grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepLinearReconstruction(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRoeConsistentViaPseudoInverse(grid, equations); 
    	//FluxSolver fluxSolver        = new FluxSolverRusanovConsistent(grid, equations); // unstable?
    	//FluxSolver fluxSolver        = new FluxSolverRoeConsistentViaPseudoInverseVariant(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDLaxWendroff(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDLaxWendroffLimited(grid, equations); // works in 1D only
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDLaxWendroffLimited2(grid, equations); // works in 1D only
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDBeamWarming(grid, equations); // works for linear systems only at the moment
    	// ----- scalar solver ----------------------------------------------------------------------------------------
    	//FluxSolver fluxSolver         = new FluxSolverExactScalar(grid, equations);
    	// ----- advection solver ----------------------------------------------------------------------------------------
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDExactRiemannAdvectionConstant(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDAdvectionConstant(grid, equations);
    	// don't use FluxSolver fluxSolver        = new FluxSolverRoeAdvection(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverActiveCartesianAdvection(grid, equations, reconstruction, FluxSolverActive.SIMPSON);
    	// ----- acoustic solver ----------------------------------------------------------------------------------------
    	//FluxSolver fluxSolver        = new FluxSolverAcousticEqualEigenvalues(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverLungRoe(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDExactAcousticWithAdvection(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDAcousticBicharWithAdvectionGodunov(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDAcousticBicharWithAdvection(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRoeAcousticExtendedGravity(grid, equations, source.gy());
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDCartesianSecondOrderAcoustic(grid, equations);
    	// don't use FluxSolver fluxSolver        = new FluxSolverRoeAcoustic(iteration.grid, equations);
        //FluxSolver fluxSolver                   = new FluxSolverRoeAcousticSmoothed(grid, equations, 1.15);
    	//FluxSolver fluxSolver        = new FluxSolverMultiStepMultiDExactRiemannAcoustic(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRoePreconditionedAcoustic(grid, equations, "0201");
       	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDAcousticBichar(grid, equations);
       	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDConsistentAcoustic(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDConsistentVariantAcoustic(grid, equations); 
       	//FluxSolver fluxSolver        = new FluxSolverActiveCartesianAcoustic(grid, equations, reconstruction);
    	//retest FluxSolver fluxSolver        = new FluxSolverVertexAcousticBichar(grid, equations);
    	//FluxSolver fluxSolver               = new FluxSolverDimSplitSecondOrderAcoustic(grid, equations);
    	//FluxSolver fluxSolver               = new FluxSolverMultiStepCartesianSecondOrderLinearized1DAverage(grid, equations);
    	//FluxSolver fluxSolver               = new FluxSolverMultiStepCartesianSecondOrderLinearized2DAverage(grid, equations);
    	//FluxSolverSemiLagrangianAcoustic fluxSolver = new FluxSolverSemiLagrangianAcoustic(grid, equations, true);
    	//FluxSolver fluxSolver         = new FluxSolverAcousticCentralSplitUp(grid, equations, true);
    	// ----- hydrodynamic solver ------------------------------------------------------------------------------------
    	//FluxSolver fluxSolver        = new FluxSolverRusanovMultiD(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRusanovHydroGravity(grid, equations, source.gy());
    	//FluxSolver fluxSolver        = new FluxSolverRoeHydroGravity(grid, equations, source.gy());
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDRoeHydro(grid, equations, FluxSolverRoeHydro.MEANSIMPLE);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDRoeHydroFineTunedDivergence(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDRoeHydroVertexFocused(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDDiagonalDiffusion(grid, equations, FluxSolverRoeHydro.MEANSIMPLE);
    	//FluxSolver fluxSolver        = new FluxSolverRoeConsistentViaPseudoInverseHydro(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDConsistent(grid, equations); // isentropic and acoustic
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDSplitConsistentDiffusion(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverOneStepMultiDHLLConsistent(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverSplitConsistentGalilei(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverDimSplitApproxRiemannLinearInterpolation(grid, equations);
    	/*FluxSolver fluxSolver        = new FluxSolverGravityWDerivatives(iteration.grid, equations, 
    			new FluxSolverRoeHydro(iteration.grid, equations.getHydroEquations(), FluxSolverRoeHydro.MEANROE)); // */
    	//FluxSolver fluxSolver        = new FluxSolverRoeHydro(iteration.grid, equations, FluxSolverRoeHydro.MEANROE);
    	//FluxSolver fluxSolver        = new FluxSolverRoe1DHydrostatic(grid, equations, source);
    	//FluxSolver fluxSolver        = new FluxSolverRoeHydroThornber(iteration.grid, equations, FluxSolverRoeHydro.MEANSIMPLE);
    	//FluxSolver fluxSolver        = new FluxSolverRoeIsentropicModified(iteration.grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverRoePreconditioned(iteration.grid, equations, mach, FluxSolverRoeHydro.MEANSIMPLE);
    	//FluxSolver fluxSolver        = new FluxSolverRoe0201(iteration.grid, equations, FluxSolverRoeHydro.MEANSIMPLE);
    	//FluxSolver fluxSolver        = new FluxSolverMiczek(iteration.grid, equations, FluxSolverRoeHydro.MEANSIMPLE, 1e-14);
    	//FluxSolver fluxSolver        = new FluxSolverTurkel(iteration.grid, equations, FluxSolverRoeHydro.MEANSIMPLE, 1e-5);
    	//FluxSolver fluxSolver        = new FluxSolverAusmLike(iteration.grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverEqualEigenvalues(grid, equations);
    	//FluxSolver fluxSolver        = new FluxSolverEulerEqualEigenvaluesMultiD(grid, equations);
    	//FluxSolverSemiLagrangianHydro fluxSolver        = new FluxSolverSemiLagrangianHydro(grid, equations);
    	//FluxSolverSemiLagrangianHydro fluxSolver        = new FluxSolverSemiLagrangianHydroConsistent(grid, equations);
    	//FluxSolverSemiLagrangianHydroSimplifiedConsistent fluxSolver = new FluxSolverSemiLagrangianHydroSimplifiedConsistent(grid, equations);
    	//FluxSolverMultiStepMultiDRBV2 fluxSolver        = new FluxSolverMultiStepMultiDRBV2(grid, equations, source);
    	//FluxSolver fluxSolver          = new FluxSolverOneStepMultiDSemiLagrangianSimplified(grid, equations);
    	//FluxSolver fluxSolver          = new FluxSolverMultiDSemiLagrangianLaxWendroff(grid, equations);
    	//FluxSolver fluxSolver          = new FluxSolverPressurelessEulerRelaxation(grid, equations);
    	//FluxSolver fluxSolver          = new FluxSolverOneStepMultiDRelaxationSolver(grid, equations);
    	//FluxSolver fluxSolver          = new FluxSolverOneStepMultiDEulerRelaxation(grid, equations);
    	//FluxSolver fluxSolver          = new FluxSolverLagrangeProjectionTypeCartesianEuler(grid, equations);   // yee
    	//FluxSolver fluxSolver          = new FluxSolverRoeConsistentEulerWithCompressiveTerms(grid, equations);
    	hydroSetup.fluxSolver = fluxSolver;
    	
    	iteration.timeIntegrator    = new TimeIntegratorExplicitConservative(iteration.grid, 0.45);
    	//iteration.timeIntegrator    = new TimeIntegratorRungeKutta3(iteration.grid, 0.4);
    	//iteration.timeIntegrator    = new TimeIntegratorSemiImplicitEuler(grid, 0.9, equations);
    	//iteration.timeIntegrator    = new TimeIntegratorSemiImplicitIsentropicEuler(grid, 0.7, equations);
    	//iteration.timeIntegrator    = new TimeIntegratorSemiImplicitAcoustic(grid, 0.4, equations);
    	//iteration.timeIntegrator    = new TimeIntegratorExplicitCubicSpline(grid, 0.9, equations);
    	/*if (cflNumber <= 1.0){
		iteration.timeIntegrator    = new TimeIntegratorExplicitConservative(iteration.grid, cflNumber);
		} else {
		iteration.timeIntegrator    = new TimeIntegratorRungeKutta3(iteration.grid, cflNumber);
		}//*/
    	//iteration.timeIntegrator    = new TimeIntegratorForSemiLagrangianAcoustic(grid, 0.95, fluxSolver);
    	//iteration.timeIntegrator    = new TimeIntegratorForSemiLagrangianHydro(grid, 0.45, fluxSolver);
    	//iteration.timeIntegrator    = new TimeIntegratorForSemiLagrangianHydroSimplifiedConsistent(grid, 0.45, fluxSolver);
    	//iteration.timeIntegrator    = new TimeIntegratorJameson(grid, 0.75);
    	//unused iteration.timeIntegrator    = new TimeIntegratorForSemiLagrangianSimplifiedConsistent(grid, 0.45, fluxSolver);
    	
    	
    	
    	//hydroSetup.output            = new OutputConservative(0.01, 0.5, grid, equations);
    	//hydroSetup.output            = new OutputConservative(0.1, grid, equations);
    	//hydroSetup.output            = new OutputSampled(5.0, grid, equations);
    	//hydroSetup.output            = new OutputDeviationFromInitial(0.1, grid, equations, hydroSetup.initialData);
    	//hydroSetup.output            = new OutputErrorNorm(0.01, grid, equations, hydroSetup.initialData, 1);
    	//hydroSetup.output            = new OutputFluxDivergence(0.00001, grid, equations);
    	//hydroSetup.output            = new OutputVelocityDerivatives(0.01, grid, equations);
    	//hydroSetup.output            = new OutputConservativeJustOneCell(0.01, grid, equations, 38, 26, 0); 
    	// ----- advection output ----------------------------------------------------------------------------------------
    	//hydroSetup.output            = new OutputAdvectionKineticEnergy(0.01, iteration.grid, equations);    	
    	// ----- acoustic output ----------------------------------------------------------------------------------------
    	//hydroSetup.output            = new OutputAcoustic(0.01, grid, equations);
    	//hydroSetup.output            = new OutputAcousticJustOneCell(0.001, grid, equations, 51, 51, 0, hydroSetup.initialData);
    	//hydroSetup.output            = new OutputAcousticPressure(2e-6, grid, equations, mach);
    	//hydroSetup.output            = new OutputAcousticKineticEnergy(2e-5, grid, equations);
    	//hydroSetup.output            = new OutputMaximumSkewedVelocityDerivatives(2e-5, grid, equations);
    	//hydroSetup.output            = new OutputVorticityAcoustic(0.001, grid, equations, hydroSetup.initialData);
    	// ----- hydrodynamic output    ------------------------------------------------------------------------------------
    	hydroSetup.output            = new OutputHydro(0.01, grid, equations);
    	//hydroSetup.output            = new OutputPressureGradientsAndDivergenceForLowMach(1e-4, grid, equations);
    	//hydroSetup.output            = new OutputVorticityAndDivergenceHydro(0.01, grid, equations);
    	//hydroSetup.output            = new OutputHydro(0.1, iteration.grid, equations.getHydroEquations());
    	//hydroSetup.output            = new OutputHydro(finaltime, grid, equations);
    	//hydroSetup.output            = new OutputIsentropic(0.001, grid, equations);
    	//hydroSetup.output            = new OutputShallowWater(0.1, grid, equations, source);
    	//hydroSetup.output            = new OutputPressureless(0.1, grid, equations);
    	//hydroSetup.output            = new OutputKineticEnergy(0.499, iteration.grid, equations, mach);
    	//hydroSetup.output            = new OutputTemperature(0.02, iteration.grid, equations, 0.85);
    	//hydroSetup.output            = new OutputCheckerboard(0.01, iteration.grid, equations);
    	//hydroSetup.output            = new OutputDeviationMaxNeighbour(0.00005, iteration.grid, equations, hydroSetup.initialData);
    	//hydroSetup.output            = new OutputMaxDensity(0.01, iteration.grid, equations);
    	//hydroSetup.output            = new OutputMaxMachNumber(0.05, iteration.grid, equations);
    	//hydroSetup.output            = new OutputMaxDevFromHorizDensAverageAndMaxDevMachNumber(0.05, iteration.grid, equations, hydroSetup.initialData);
    	//hydroSetup.output            = new OutputHydroJustOneCell(0.00002, grid, equations, 38, 26, 0, true); 
    	//hydroSetup.output            = new OutputNone(iteration.grid);
    	
    	
    	hydroSetup.output().setPrinter(new PrinterStdOut());
    	//hydroSetup.output().setPrinter(new PrinterFile("dat/testKH.dat"));
    	//hydroSetup.output().setPrinter(new PrinterVTK());
    	
    	
    	/*Setup passiveScalarSetup = new Setup();
    	passiveScalarSetup.reconstruction      = new ReconstructionPiecewiseConstant();
    	passiveScalarSetup.reconstruction.setGrid(iteration.grid);
    	EquationsAdvectionWithHydroIdeal equationsScalar = new EquationsAdvectionWithHydroIdeal(iteration.grid, 0, hydroSetup, equations); 
    	passiveScalarSetup.equations           = equationsScalar;
    	passiveScalarSetup.source              = new SourceZero(iteration.grid); ;
    	passiveScalarSetup.obstacle            = new ObstacleNone(grid); 
    	passiveScalarSetup.initialData         = new InitialDataAdvectionJump(grid, equationsScalar);
    	passiveScalarSetup.boundary            = new BoundaryZeroGradient(iteration.grid);
    	//passiveScalarSetup.pointValueEvolution = new PointValueEvolutionNone(iteration.grid);
    	passiveScalarSetup.fluxSolver          = new FluxSolverRoe(iteration.grid, equationsScalar);
    	passiveScalarSetup.output              = new OutputConservative(hydroSetup.output().outputTimeInterval(), grid, equationsScalar);
    	//*/
    	 
    	
    	iteration.setups.add(hydroSetup);
    	//iteration.setups.add(passiveScalarSetup);
    	
    	iteration.run(finaltime);
    }
    

    
    
}
