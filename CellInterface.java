
public class CellInterface {

	protected int i, j, k;
	protected int interf;
	
	protected int fluxi, fluxj, fluxk;
	protected int direction, momentumIndex;
	
	protected int neighbouri, neighbourj, neighbourk;
	
	public CellInterface(int i, int j, int k, int interf) {
		this.i = i;
		this.j = j;
		this.k = k;
		this.interf = interf;
		
		// i, j, k is always inside the obstacle
		// the "neighbour" is the neighbouring cell just outside the obstacle, in the direction of the interf
		neighbouri = i;
		neighbourj = j;
		neighbourk = k;
		
		
		if (interf == ReconstructionCartesian.WEST){
			neighbouri--;
			fluxi = i; direction = GridCartesian.X_DIR; momentumIndex = EquationsHydroIdeal.INDEX_XMOM;
		}
		if (interf == ReconstructionCartesian.EAST){
			neighbouri++;
			fluxi = i+1; direction = GridCartesian.X_DIR; momentumIndex = EquationsHydroIdeal.INDEX_XMOM;
		}
		
		if (interf == ReconstructionCartesian.SOUTH){
			neighbourj--;
			fluxj = j; direction = GridCartesian.Y_DIR; momentumIndex = EquationsHydroIdeal.INDEX_YMOM;
		}
		if (interf == ReconstructionCartesian.NORTH){
			neighbourj++;
			fluxj = j+1; direction = GridCartesian.Y_DIR; momentumIndex = EquationsHydroIdeal.INDEX_YMOM;
		}
		
		if (interf == ReconstructionCartesian.BACK){
			neighbourk--;
			fluxk = k; direction = GridCartesian.Z_DIR; momentumIndex = EquationsHydroIdeal.INDEX_ZMOM;
		}
		if (interf == ReconstructionCartesian.FRONT){
			neighbourk++;
			fluxk = k+1; direction = GridCartesian.Z_DIR; momentumIndex = EquationsHydroIdeal.INDEX_ZMOM;
		}
		
		
	}
	
	public int i(){ return i; }
	public int j(){ return j; }
	public int k(){ return k; }
	public int interf(){ return interf; }
	
	public int fluxi(){ return fluxi; }
	public int fluxj(){ return fluxj; }
	public int fluxk(){ return fluxk; }
	public int fluxDirection(){ return direction; }
	public int momentumIndex(){ return momentumIndex; }

	public int neighbouri(){ return neighbouri; }
	public int neighbourj(){ return neighbourj; }
	public int neighbourk(){ return neighbourk; }
	
	
	
}
