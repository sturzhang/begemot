public class FluxSolverRoeHydro extends FluxSolverRoe {

	public static int MEANROE    = 1;
	public static int MEANSIMPLE = 2;
	protected int meanType;
	
	public FluxSolverRoeHydro(Grid grid, EquationsHydroIdeal equations, int meanType) {
		super(grid, equations);
		this.meanType = meanType;
	}
	
    public String getDetailedDescription(){
    	return "Roe solver for ideal gas";
    }

    	// TODO implement passive scalalrs as instance of a spatially-non-constant advection equation once for all
/*	@Override
	protected double[] fluxPassiveScalar(double[] left, double[] right,	int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double mLeft, mRight, absMeanV;
		int indexDirection = ((EquationsHydroIdeal) equations).getIndexDirection(direction);
		
		mLeft = left[indexDirection];
		mRight = right[indexDirection];
		
		absMeanV = Math.abs(0.5 * mLeft / left[EquationsHydroIdeal.INDEX_RHO] + 
							0.5 * mRight / right[EquationsHydroIdeal.INDEX_RHO]);
		
		
		for (int i = 0; i < res.length; i++){
			s = sc[i];
			res[i] = 0.5*(left[s]*mLeft + right[s]*mRight) - 0.5 * absMeanV * (right[s] - left[s]);
		}
		return res;
	} // remove once things work -> moved to superclass */
    
	
	/*public double[] averageFlux(double[] left, double[] right, int direction) {
		int indexDirection = getIndexDirection(direction);
		double[] fluxLeft, fluxRight, res;
	    fluxLeft = ((EquationsHydroIdeal) equations).fluxFunction1DFlow(indexDirection, left);
    	fluxRight = ((EquationsHydroIdeal) equations).fluxFunction1DFlow(indexDirection, right);
    	res = new double[left.length];
    	for (int q = 0; q < equations.getNumberOfConservedQuantities(); q++){
    		res[q] = 0.5 * (fluxLeft[q] + fluxRight[q]);
    	}
    	return res;
	} // remove once things work */

/* old comment?	public double[] flux(double[] left, double[] right, int direction) {
		int quantities = left.length;
		double diff[] = new double[quantities];
		double[] upwindingTerm = new double[5];
		double[] fluxLeft, fluxRight, fluxRes;
		SquareMatrix upwindingMatrix = new SquareMatrix(5);
		
		for (int q = 0; q < quantities; q++){ diff[q] = right[q] - left[q]; }
		
		upwindingMatrix = getUpwindingMatrix(left, right, direction);
		upwindingTerm = upwindingMatrix.mult(0.5).mult(diff);
	    
		int indexDirection = getIndexDirection(direction);
			    	    
	    fluxLeft = equations.fluxFunction1DFlow(indexDirection, left);
    	fluxRight = equations.fluxFunction1DFlow(indexDirection, right);	    	    	
	    
    	fluxRes = new double[5];
    	for (int q = 0; q <= 4; q++){
    	    fluxRes[q] = 0.5 * (fluxLeft[q] + fluxRight[q]) - upwindingTerm[q];
    	    if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("?");
    	    }
    	}
		
    	return fluxRes;
	}*/

	/* not needed public SquareMatrix getUpwindingMatrix(double[] left, double[] right, int direction, boolean returnSign){
		int indexDirection = getIndexDirection(direction);
		
		HydroState le = new HydroState(left, indexDirection, (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, indexDirection, (EquationsHydroIdeal) equations);
		
		// new position
		curMaxSignalSpeed = Math.max(le.soundSpeed + Math.sqrt(le.v2), curMaxSignalSpeed);
		curMaxSignalSpeed = Math.max(ri.soundSpeed + Math.sqrt(ri.v2), curMaxSignalSpeed);
		
		double vXMean =0, vYMean =0, vZMean =0, enthalpyMean =0, rhoMean =0; // to make compiler happy
		
		if (meanType == MEANROE){
			vXMean = roeMean(le.rho, ri.rho, le.vX, ri.vX);
			vYMean = roeMean(le.rho, ri.rho, le.vY, ri.vY);
			vZMean = roeMean(le.rho, ri.rho, le.vZ, ri.vZ);
			enthalpyMean = roeMean(le.rho, ri.rho, le.enthalpy, ri.enthalpy);
		}
		if (meanType == MEANSIMPLE){
			vXMean = 0.5*(le.vX + ri.vX);
			vYMean = 0.5*(le.vY + ri.vY);
			vZMean = 0.5*(le.vZ + ri.vZ);
			enthalpyMean = 0.5*(le.enthalpy + ri.enthalpy);
		}
			
		if (Double.isNaN(enthalpyMean)){
			System.err.println("?");
		}
		
		return absJacobian(direction, vXMean, vYMean, vZMean, enthalpyMean, returnSign);
	} */

	
	
	/* public SquareMatrix getJacobian(double[] left, double[] right, int direction){
		double[][] JacArr = new double[5][5];
		double vDir = 0.0; // to make compiler happy
		int indexDirection = getIndexDirection(direction);
		HydroState le = new HydroState(left, indexDirection, (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, indexDirection, (EquationsHydroIdeal) equations);
		
		double vXMean, vYMean, vZMean, rhoMean, cMean;
				
		vXMean = roeMean(le.rho, ri.rho, le.vX, ri.vX);
		vYMean = roeMean(le.rho, ri.rho, le.vY, ri.vY);
		vZMean = roeMean(le.rho, ri.rho, le.vZ, ri.vZ);
		rhoMean = roeMean(le.rho, ri.rho, le.rho, ri.rho);
		cMean = roeMean(le.rho, ri.rho, le.soundSpeed, ri.soundSpeed);
		
		double machParameter = 1.0;
		double v2 = Math.sqrt(vXMean*vXMean + vYMean*vYMean + vZMean*vZMean);
		
		if (direction == Grid.X_DIR){ vDir = vXMean; indexDirection = 1; }
		if (direction == Grid.Y_DIR){ vDir = vYMean; indexDirection = 2; }
		if (direction == Grid.Z_DIR){ vDir = vZMean; indexDirection = 3; }
				
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				JacArr[i][j] = 0.0;
			}	
		}
		
		JacArr[0][1] = rhoMean;
		
		JacArr[0][0] = vDir;
		JacArr[1][1] = vDir;
		JacArr[2][2] = vDir;
		JacArr[3][3] = vDir;
		JacArr[4][4] = vDir;
		
		JacArr[indexDirection][4] = 1.0 / rhoMean;
		
		JacArr[4][indexDirection] = rhoMean*cMean*cMean;
		
		SquareMatrix Jac = new SquareMatrix(JacArr);
		return primToCons(vXMean, vYMean, vZMean, rhoMean, machParameter, v2, Jac);
	} */

	/*public double[][] getJacobian(double[] state, int direction){
		double[][] JacArr = new double[5][5];
		double vDir = 0.0; // to make compiler happy
		int indexDirection = getIndexDirection(direction);
		HydroState val = new HydroState(state, indexDirection, (EquationsHydroIdeal) equations);
		
		double vXMean, vYMean, vZMean, rhoMean, cMean;
				
		vXMean = val.vX;
		vYMean = val.vY;
		vZMean = val.vZ;
		rhoMean = val.rho;
		cMean = val.soundSpeed;
		
		double machParameter = 1.0;
		double v2 = Math.sqrt(vXMean*vXMean + vYMean*vYMean + vZMean*vZMean);
		
		if (direction == Grid.X_DIR){ vDir = vXMean; indexDirection = 1; }
		if (direction == Grid.Y_DIR){ vDir = vYMean; indexDirection = 2; }
		if (direction == Grid.Z_DIR){ vDir = vZMean; indexDirection = 3; }
				
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				JacArr[i][j] = 0.0;
			}	
		}
		
		JacArr[0][1] = rhoMean;
		
		JacArr[0][0] = vDir;
		JacArr[1][1] = vDir;
		JacArr[2][2] = vDir;
		JacArr[3][3] = vDir;
		JacArr[4][4] = vDir;
		
		JacArr[indexDirection][4] = 1.0 / rhoMean;
		
		JacArr[4][indexDirection] = rhoMean*cMean*cMean;
		
		SquareMatrix Jac = new SquareMatrix(JacArr);
		return primToCons(vXMean, vYMean, vZMean, rhoMean, machParameter, v2, Jac).toArray();
	} */

	

	
/* old -> moved to equations 	protected SquareMatrix absJacobian(int direction, double vX, double vY, double vZ, double enthalpy, boolean returnSign){
		double[] unitVector = new double[]{0.0,
				(direction == Grid.X_DIR) ? 1.0 : 0.0,
				(direction == Grid.Y_DIR) ? 1.0 : 0.0,
				(direction == Grid.Z_DIR) ? 1.0 : 0.0, 0.0};	
	
		
		SquareMatrix rightEigenvectors = new SquareMatrix(5);
		SquareMatrix leftEigen1Forms = new SquareMatrix(5);
		SquareMatrix eigenValues = new SquareMatrix(5);
		double[] eigenValuesArr;
		double vDirection = 0.0; // init to make the compiler happy 
		double v2 = vX*vX + vY*vY + vZ*vZ;
		double[][] inverse = new double[5][5];
		double soundSpeed = ((EquationsHydroIdeal) equations).getSoundSpeedFromEnthalpy(enthalpy, v2);

		
	    if (direction == Grid.X_DIR){ 
	    	rightEigenvectors.set(((EquationsHydroIdeal) equations).getJacobianEigenvectorsFor1DFlow(1, vX, vY, vZ, soundSpeed, enthalpy)); 
	    	vDirection = vX;
	    }	
	    if (direction == Grid.Y_DIR){ 
	    	rightEigenvectors.set(((EquationsHydroIdeal) equations).getJacobianEigenvectorsFor1DFlow(2, vX, vY, vZ, soundSpeed, enthalpy)); 
	    	vDirection = vY;
		}	
	    if (direction == Grid.Z_DIR){ 
	    	rightEigenvectors.set(((EquationsHydroIdeal) equations).getJacobianEigenvectorsFor1DFlow(3, vX, vY, vZ, soundSpeed, enthalpy)); 
	    	vDirection = vZ;
		}	
	    
	    // old position
	    // curMaxSignalSpeed = Math.max(soundSpeed + Math.sqrt(v2), curMaxSignalSpeed);
		
	    eigenValuesArr = ((EquationsHydroIdeal) equations).getJacobianEigenvaluesFor1DFlow(vDirection, soundSpeed);
	    if (returnSign){
	    	for (int k = 0; k < eigenValuesArr.length; k++){
	    		eigenValuesArr[k] = (eigenValuesArr[k] >= 0) ? 1.0 : -1.0;
	    	}
	    	eigenValues = new DiagonalMatrix(eigenValuesArr);
	    } else {
	    	eigenValues = new DiagonalMatrix(eigenValuesArr);
	    	eigenValues.absMe();
    	}
	    	    
	    inverse[4][0] = (enthalpy - v2) * (((EquationsHydroIdeal) equations).gamma() - 1) / soundSpeed / soundSpeed;
	    inverse[4][1] =	vX * (((EquationsHydroIdeal) equations).gamma() - 1) / soundSpeed / soundSpeed; 
	    inverse[4][2] = vY * (((EquationsHydroIdeal) equations).gamma() - 1) / soundSpeed / soundSpeed; 
	    inverse[4][3] = vZ * (((EquationsHydroIdeal) equations).gamma() - 1) / soundSpeed / soundSpeed; 
	    inverse[4][4] = - (((EquationsHydroIdeal) equations).gamma() - 1) / soundSpeed / soundSpeed;
	    	    
	    inverse[2] = new double[]{- vY, 0.0, 1.0, 0.0, 0.0};
	    inverse[3] = new double[]{- vZ, 0.0, 0.0, 1.0, 0.0};
	    
	    inverse[0] = new double[]{1.0 + vDirection / soundSpeed, 0.0, 0.0, 0.0, 0.0};
	    for (int i = 0; i <= 4; i++){
	    	inverse[0][i] = 0.5 * (inverse[0][i]  - unitVector[i]/soundSpeed - inverse[4][i]);
	    }
	    
	    inverse[1] = new double[]{1.0 - vDirection / soundSpeed, 0.0, 0.0, 0.0, 0.0};
	    for (int i = 0; i <= 4; i++){
	    	inverse[1][i] = 0.5 * (inverse[1][i] + unitVector[i]/soundSpeed - inverse[4][i]);
	    }
	    
	    leftEigen1Forms.set(inverse);
	    
	    return rightEigenvectors.mult(eigenValues).mult(leftEigen1Forms).toSquareMatrix();  // 
	    
	    
	} */
	
	@Override
	public double[] mean(double[] left, double[] right){
		if (meanType == MEANROE){
			return roeMean(left, right);
		} else {
			//return left;
			//return meanSimplePrimitive(left, right);
			return super.mean(left, right);
		}
	}
	
	//@Override
	public UpwindingMatrixAndWaveSpeed getAveragedUpwindingMatrixNew(int i, int j, int k, double[] left, double[] right, int direction){
		double maxEigenvalueSuggestion = 0;
		double minEigenvalueSuggestion = 0;
		
		boolean POSITIVTY_FIX = false;
		if (POSITIVTY_FIX){
			HydroState leftState = new HydroState(left, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
			HydroState rightState = new HydroState(right, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
			
			double v = Math.max(leftState.v, rightState.v);
			double c = Math.max(leftState.soundSpeed, rightState.soundSpeed);
			maxEigenvalueSuggestion = v - c;
			maxEigenvalueSuggestion = v + c;
		}
		
		
		return getUpwindingMatrix(i, j, k, mean(left, right), direction, 
				maxEigenvalueSuggestion, minEigenvalueSuggestion, left, right);
		
	}
	
	public double[] meanSimplePrimitive(double[] left, double[] right){
		HydroState le = new HydroState(left , EquationsHydroIdeal.INDEX_XMOM, (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, EquationsHydroIdeal.INDEX_XMOM, (EquationsHydroIdeal) equations);
		
		double gamma = ((EquationsHydroIdeal) equations).gamma();
		double rhoMean = 0.5*(le.rho + ri.rho);
		double vXMean = 0.5*(le.vX + ri.vX);
		double vYMean = 0.5*(le.vY + ri.vY);
		double vZMean = 0.5*(le.vZ + ri.vZ);
		double v2Mean = vXMean*vXMean + vYMean*vYMean + vZMean*vZMean; 
		double pressureMean = 0.5*(le.pressure + ri.pressure);
	
		double[] res = new double[left.length];
		res[EquationsHydroIdeal.INDEX_RHO]    = rhoMean;
		res[EquationsHydroIdeal.INDEX_XMOM]   = rhoMean*vXMean;
		res[EquationsHydroIdeal.INDEX_YMOM]   = rhoMean*vYMean;
		res[EquationsHydroIdeal.INDEX_ZMOM]   = rhoMean*vZMean;
		res[EquationsHydroIdeal.INDEX_ENERGY] = pressureMean/(gamma-1) + 0.5*rhoMean*v2Mean;
		return res;
	}
	
	public double[] roeMean(double[] left, double[] right){
		HydroState le = new HydroState(left , EquationsHydroIdeal.INDEX_XMOM, (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, EquationsHydroIdeal.INDEX_XMOM, (EquationsHydroIdeal) equations);
		
		double gamma = ((EquationsHydroIdeal) equations).gamma();
		double rhoMean = roeMean(le.rho, ri.rho, le.vX, ri.vX);
		double vXMean = roeMean(le.rho, ri.rho, le.vX, ri.vX);
		double vYMean = roeMean(le.rho, ri.rho, le.vY, ri.vY);
		double vZMean = roeMean(le.rho, ri.rho, le.vZ, ri.vZ);
		double v2Mean = vXMean*vXMean + vYMean*vYMean + vZMean*vZMean; 
		double enthalpyMean = roeMean(le.rho, ri.rho, le.enthalpy, ri.enthalpy);
	
		double[] res = new double[left.length];
		res[EquationsHydroIdeal.INDEX_RHO]    = rhoMean;
		res[EquationsHydroIdeal.INDEX_XMOM]   = rhoMean*vXMean;
		res[EquationsHydroIdeal.INDEX_YMOM]   = rhoMean*vYMean;
		res[EquationsHydroIdeal.INDEX_ZMOM]   = rhoMean*vZMean;
		res[EquationsHydroIdeal.INDEX_ENERGY] = ( enthalpyMean + 0.5*(gamma-1)*v2Mean ) * rhoMean / gamma;
		return res;
	}
	
	public double roeMean(double rhoLeft, double rhoRight, double valueLeft, double valueRight){
		return (Math.sqrt(rhoLeft)*valueLeft + Math.sqrt(rhoRight)*valueRight) / 
				(Math.sqrt(rhoLeft) + Math.sqrt(rhoRight));
	}

	
}
