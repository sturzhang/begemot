
public class InitialDataIsentropicIsoenergeticShocktube extends
		InitialDataIsentropic {

	public InitialDataIsentropicIsoenergeticShocktube(Grid grid,
			EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x = g.getX() - grid.xMidpoint();
    	double y = g.getY() - grid.yMidpoint();
    	
    	double vLeft = 0.0;
    	double rhoLeft = 1.0;
    	double rhoRight = 0.3;
    	
    	double energyLeft = ((EquationsIsentropicEuler) equations).getEnergy(rhoLeft, new double[]{vLeft, 0, 0});
    	double vRight = Math.sqrt(2.0/rhoRight*(energyLeft - ((EquationsIsentropicEuler) equations).getPressure(rhoRight) /
    			(((EquationsIsentropicEuler) equations).gamma()-1)));
    	
    	if (x < 0){
    		res[EquationsIsentropicEuler.INDEX_RHO] = rhoLeft;
    		res[EquationsIsentropicEuler.INDEX_XMOM] = vLeft*res[EquationsIsentropicEuler.INDEX_RHO];
        } else {
    		res[EquationsIsentropicEuler.INDEX_RHO] = rhoRight;
    		res[EquationsIsentropicEuler.INDEX_XMOM] = vRight*res[EquationsIsentropicEuler.INDEX_RHO];
        }
    				
    	res[EquationsIsentropicEuler.INDEX_YMOM] = 0.0;
    	res[EquationsIsentropicEuler.INDEX_ZMOM] = 0.0;
    			
    		
    	return res;

	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
