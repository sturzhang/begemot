// VERSION 2022 05 11, 16:10

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import java.util.*;

public class VTKReader {

    protected static String lowercase = "abcdefghijklmnopqrstuvwxyz";
    protected static String uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    protected static String letters = lowercase + uppercase;
    protected static String digits = "+-.0123456789";
    protected static String whitespace = " ";

    protected String text;
    protected double[][][][] data;
    protected double[] xCoord, yCoord;
    
    public VTKReader(String pathToFile)  throws IOException {
	System.err.println("Reading file " + pathToFile);
	//String raw = FileInteraction.readfile(pathToFile);
	Reader fr = new FileReader(pathToFile);
	BufferedReader reader = new BufferedReader(fr, 16384);
	System.err.println("Successfully read file!");

	String tmp, line;
	text = "";
	int currentVariable = -1, indexI = 0, indexJ = 0, indexK = 0, indexVar = 0;
	LinkedList<String> words;
	
	while((line = reader.readLine())!= null) {
	    if (line.length() > 0){
		tmp = line.substring(0,1);
		if (letters.indexOf(tmp) > -1){
		    // tmp is a letter
		    text += line + "\n";
		    
		    if (line.indexOf("DIMENSIONS") > -1){
			String dim = "", rest; // to please compiler
			int dimensionsIndex = 0;
			int[] dimensions = new int[3];
			rest = line.substring(11,line.length()) + " ";
			while (rest.length() > 0){
			    if (whitespace.indexOf(rest.substring(0,1)) > -1){
				dimensions[dimensionsIndex] = Integer. parseInt(dim);
				dim = "";
				dimensionsIndex++;
			    } else {
				dim += rest.substring(0,1);
			    }
			    rest = rest.substring(1,rest.length());
			}

			System.err.println("Found array of dimensions " + dimensions[0] + ", " + dimensions[1] + ", " + dimensions[2]);

			data = new double[dimensions[0]][dimensions[1]][dimensions[2]][5];
			xCoord = new double[dimensions[0]];
			yCoord = new double[dimensions[1]];
			
		    } else if (line.indexOf("DENSITY") > -1){
			System.err.println("Reading density...");
			currentVariable = 0;
			indexI = 0; indexJ = 0; indexK = 0; indexVar = 0;
		    } else if (line.indexOf("VELOCITY") > -1){
			System.err.println("Reading velocity...");
			currentVariable = 1;
			indexI = 0; indexJ = 0; indexK = 0; indexVar = 1;
		    } else if (line.indexOf("PRESSURE") > -1){
			System.err.println("Reading pressure...");
			currentVariable = 2;
			indexI = 0; indexJ = 0; indexK = 0; indexVar = 4;
		    } else if (line.indexOf("X_COORDINATES") > -1){
			System.err.println("Reading x coordinates...");
			currentVariable = 11;
			indexI = 0;
		    } else if (line.indexOf("Y_COORDINATES") > -1){
			System.err.println("Reading y coordinates...");
			currentVariable = 12;
			indexJ = 0;
		    }

		    
		} else if (digits.indexOf(tmp) > -1){
		    // tmp is a digit

		    if (currentVariable >= 0){

			int lastI = 0;
			words = new LinkedList<String>();
			for (int i = 0; i < line.length(); i++){
			    if (whitespace.indexOf(line.substring(i,i+1)) > -1){
				words.add(line.substring(lastI,i));
				lastI = i+1;
			    }
			}

			    
			if (currentVariable > 10){
			    // coordinates
			    if (currentVariable == 11){
				for (String w : words){
				    xCoord[indexI] = Double.parseDouble(w);
				    indexI++;
				    if (indexI >= xCoord.length){ currentVariable = -1; }
				}
			    } else if (currentVariable == 12){
				for (String w : words){
				    yCoord[indexJ] = Double.parseDouble(w);
				    indexJ++;
				    if (indexJ >= yCoord.length){ currentVariable = -1; }
				}
			    }
			} else {
			    // data
			    
			    
			    for (String w : words){
				data[indexI][indexJ][indexK][indexVar] = Double.parseDouble(w);
				if (currentVariable == 1){
				    indexVar++;
				    if (indexVar == 4){ indexVar = 1; indexI++;
					if (indexI >= data.length){ indexI = 0; indexJ++;
					    if (indexJ >= data[0].length){ indexJ = 0; indexK++;
						if (indexK >= data[0][0].length){ currentVariable = -1; } } } }
				} else {
				    indexI++;
				    if (indexI >= data.length){ indexI = 0; indexJ++;
					if (indexJ >= data[0].length){ indexJ = 0; indexK++;
					    if (indexK >= data[0][0].length){ currentVariable = -1; } } } 
				}
			    }
			}
		    }
		    
		    
		} else {
		    // special character
		    text += line + "\n";
		}
	    } else {
		text += line + "\n";
	    }
	}
	reader.close();
	
	//printData(data);
	
	double[][][] res = getData();
	//printData(res);

	
	//System.out.println(text);
    }
    
    protected void printData(double[][][][] data){
	for (int q = 0; q < data[0][0][0].length; q++){
	    // 0: density 1: vx 2: vy 3: vz 4: pressure
	    for (int i = 0; i < data.length; i++){
		for (int j = 0; j < data[i].length; j++){
		    System.out.println(xCoord[i] + " " + yCoord[j] + " " + data[i][j][0][q]);
		}
	    }
	    
	    System.out.println("\n\n");
	    
	}
	for (int i = 0; i < data.length; i+=10){
	    for (int j = 0; j < data[i].length; j+=10){
		System.out.println(xCoord[i] + " " + yCoord[j] + " " + data[i][j][0][1] + " " + data[i][j][0][2]);
	    }
	}
    }

    protected void printData(double[][][] data){
	for (int q = 0; q < data[0][0].length; q++){
	    // 0: density 1: vx 2: vy 3: vz 4: pressure
	    for (int i = 0; i < data.length; i++){
		for (int j = 0; j < data[i].length; j++){
		    System.out.println(xCoord[i] + " " + yCoord[j] + " " + data[i][j][q]);
		}
	    }
	    
	    System.out.println("\n\n");
	    
	}
	for (int i = 0; i < data.length; i+=10){
	    for (int j = 0; j < data[i].length; j+=10){
		System.out.println(xCoord[i] + " " + yCoord[j] + " " + data[i][j][1] + " " + data[i][j][2]);
	    }
	}
    }


    public double[][][] getData(){
	// domain = [0.0025 : 1.0025] x [0.5975 : 0.9975] equivalent to [0:1] x [0:0.4]
	// array dimensions: 201 x 81
	
	int imin = 3;
	int imax = 203;

	int jmin = 122;
	int jmax = data[0].length - 4;
	
	double[][][] res = new double[imax - imin + 1][jmax - jmin + 1][5];
	for (int i = imin; i <= imax; i++){
	    for (int j = jmin; j <= jmax; j++){
		for (int q = 0; q <= 4; q++){
		    res[i-imin][j-jmin][q] = data[i][j][0][q];
		}
	    }
	}

	System.err.println("domain = [" + xCoord[imin] + " : " + xCoord[imax] + "] x [" + yCoord[jmin] + " : " + yCoord[jmax] + "]");
	System.err.println("array dimensions: " + res.length + " x " + res[0].length);

	return res;
    }
}
