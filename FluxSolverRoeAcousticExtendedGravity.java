
public class FluxSolverRoeAcousticExtendedGravity extends FluxSolverRoe {

	protected double gy;
	
	public FluxSolverRoeAcousticExtendedGravity(GridCartesian grid, EquationsAcousticExtended equations, double gy) {
		super(grid, equations);
		this.gy = gy;
	}
	
	@Override
	protected double[] getStateDifference(GridEdgeMapped gridEdge, double[] right, double[] left){
		double[] diff = new double[left.length];
		for (int q = 0; q < left.length; q++){ diff[q] = right[q] - left[q]; }
		
		double dx= ((GridCartesian) grid).ySpacing();
		
		if (gridEdge.normalVector()[0] == 0.0){
			diff[EquationsAcoustic.INDEX_P] = 
				right[EquationsAcoustic.INDEX_P] - left[EquationsAcoustic.INDEX_P] 
			  - 0.5 * dx * gy*gridEdge.normalVector()[1] * (right[EquationsAcousticExtended.INDEX_RHO] + left[EquationsAcousticExtended.INDEX_RHO]);
		}
		
		return diff;
	}

}
