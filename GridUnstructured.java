import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;


public abstract class GridUnstructured extends Grid {

	
	protected int numberOfCells;
	protected LinkedList<GridCell>[] neighbours;
    protected boolean[] validCell;

	
	public GridUnstructured() {
		super();
	}


	protected boolean isValidCell(int k) { return validCell[k]; }

	@Override
	public LinkedList<GridCell> getNeighbours(GridCell cell) {
		return neighbours[cell.k()];
	}
	
	@Override
	public LinkedList<GridEdge> getEdges(GridCell cell) {
		// TODO to be deleted anyway
		return null;
	}


	
	@Override
	public boolean isValidCell(GridCell g){
        return isValidCell(g.k());
    }
	
	protected abstract int indexMin();
	protected abstract int indexMax();

    public abstract String getDetailedDescription();

    @Override
    public String description(){ 
    	String res = "";
    	res += "unstructured grid: ";
    	return "# " + res + " " + getDetailedDescription(); 
    }
	
	@Override
	public void setAllGhostcells() {
    	allGhostCells = new LinkedList<GridCell>();
    	
    	for (int k = 0; k < conservedQuantities[0][0].length; k++){
    		if (!isValidCell(k)){
    			allGhostCells.add(new GridCell(0, 0, k, this));
    		}
    	}
    }

	// TODO actually this can be asked for every single cell (suhc that the number will differ form cell to cell) 
	// which should work without big implementation problems!
	protected abstract int edgesPerCell();
	
	@Override
	public boolean[][][] generateEmptyBooleanArray() {
		return new boolean[1][1][numberOfCells];}

	@Override
	public double[][][][] generateEmptyArray(int numberOfQuantities) {
		return new double[1][1][numberOfCells][numberOfQuantities];}

	@Override
	public double[][][][][][] generateEmptyFluxArray(int numberOfQuantities, int numberOfFluxSolverSteps) {
		return new double[numberOfFluxSolverSteps][1][1][numberOfCells][edgesPerCell()][numberOfQuantities];}

	@Override
	public double[][][][][] generateEmptyFluxArray(int numberOfQuantities) {
		return new double[1][1][numberOfCells][edgesPerCell()][numberOfQuantities]; }

	@Override
	public double[][][][] generateEmptyWaveSpeedArray() {
		return new double[1][1][numberOfCells][edgesPerCell()]; }

	@Override
	public double[][][][][] generateEmptyArrayForInterface(int numberOfQuantities) {
		return new double[1][1][numberOfCells][edgesPerCell()][numberOfQuantities]; }

	
	@Override
	public Iterator<GridCell> iterator() {
        return new GridIterator();
    }
    
    private class GridIterator implements Iterator<GridCell> {
    	private int k;
    	
    	public GridIterator(){
    		k = GridUnstructured.this.indexMin();
    	}
    	
    	public boolean hasNext() {
    		// hasNext should still answer true if it is on the last gridcell (naming confusion; better name: stillOnGrid)
            return (k <= GridUnstructured.this.indexMax());
        }

		@Override
		public GridCell next() {
			int oldK;
			
			if(this.hasNext()) {
				oldK = k;
				
				do { k++; } while (!isValidCell(k) && hasNext());
				return new GridCell(0, 0, oldK, GridUnstructured.this);
            }
            throw new NoSuchElementException();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
    }


}
