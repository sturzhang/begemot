import matrices.SquareMatrix;


public abstract class EquationsAcoustic extends Equations {

	public static final int INDEX_VX = 0;
	public static final int INDEX_VY = 1; 
	public static final int INDEX_VZ = 2;
	public static final int INDEX_P = 3;
	
	protected double soundspeed;

	public EquationsAcoustic(Grid grid, int numberOfPassiveScalars, double soundspeed) {
		super(grid, numberOfPassiveScalars);
		this.soundspeed = soundspeed;
	}

	public abstract SquareMatrix getJacobian(int direction);
	
	public double soundspeed(){ return soundspeed; }

	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		return getJacobian(direction).mult(quantities);
	}

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		// TODO check that all the transform-routines are prepared fo rthe case that a subclass might have more (scalar) variables
		for (int q = 0; q < getNumberOfConservedQuantities(); q++){
			res[q] = quantities[q];
		}
		
		double[] transformedVector = transformVector(quantities[INDEX_VX], quantities[INDEX_VY], quantities[INDEX_VZ], normalVector);
		res[INDEX_VX] = transformedVector[0];
		res[INDEX_VY] = transformedVector[1];
		res[INDEX_VZ] = transformedVector[2];
		
		return res;
	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		for (int q = 0; q < getNumberOfConservedQuantities(); q++){
			res[q] = quantities[q];
		}
		
		double[] transformedVector = transformVectorBack(quantities[INDEX_VX], quantities[INDEX_VY], quantities[INDEX_VZ], normalVector);
		res[INDEX_VX] = transformedVector[0];
		res[INDEX_VY] = transformedVector[1];
		res[INDEX_VZ] = transformedVector[2];
		
		return res;
	}

}
