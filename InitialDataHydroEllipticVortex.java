
public class InitialDataHydroEllipticVortex extends InitialDataHydro {

	protected double aspectRatio, mach;
	
	public InitialDataHydroEllipticVortex(Grid grid, EquationsHydroIdeal equations, double aspectRatio, double mach) {
		super(grid, equations);
		this.aspectRatio = aspectRatio;
		this.mach = mach;
	}

	
	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x, y;
		double rhoV2, pressure;
    	
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
    				
		if (aspectRatio*aspectRatio*x*x + y*y < 0.4*0.4){
			res[EquationsHydroIdeal.INDEX_XMOM] = 2.0* y;
			res[EquationsHydroIdeal.INDEX_YMOM] = -2.0*aspectRatio*x;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			
			pressure = 2.0*aspectRatio*(x*x + y*y) + 1.0/mach/mach;
		} else {
			res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;			
			
			pressure = 2.0*aspectRatio*0.4*0.4/(aspectRatio*aspectRatio*x*x + y*y)*(x*x + y*y) + 1.0/mach/mach;
		}
			
		rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    				
		 
		
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    	
    	for (int ii = 5; ii < res.length; ii++){
    		res[ii] = (x < 0 ? 1.0 : 0.0);
    	}
    	
    	return res;	
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
