
public class FluxSolverEqualEigenvalues extends FluxSolverHydro {

	public FluxSolverEqualEigenvalues(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
    public String getDetailedDescription(){
    	return "Roe-type solver with a diagonal upwinding matrix with all eigenvalues the same";
    }

	
	@Override
	protected FluxAndWaveSpeed flux(HydroState left, HydroState right) {
		double soundSpeedMean = 0.5 * (left.soundSpeed + right.soundSpeed);
		double vMean = 0.5 * (left.v + right.v);
		double vAbsMax = Math.max(Math.abs(left.v), Math.abs(right.v));
		
		double localWaveSpeed = soundSpeedMean + vAbsMax;
		
		
		double[] fluxLeft = ((EquationsHydroIdeal) equations).fluxFunction(left);
    	double[] fluxRight = ((EquationsHydroIdeal) equations).fluxFunction(right);	    	    	
	    		
		double upwindingTerm; 
		double diff[] = HydroState.diff(right, left);
		double[][] upw = upwindingMatrix(Math.abs(vMean), GridCartesian.X_DIR);
		/*double[][] upw = upwindingMatrix(Math.sqrt( Math.pow((left.vX+right.vX)/2, 2) + 
				Math.pow((left.vY+right.vY)/2, 2) + Math.pow((left.vZ+right.vZ)/2, 2)
				), direction); //*/
		
		double[] fluxRes = new double[5];
		for (int q = 0; q <= 4; q++){
			upwindingTerm = 0.0;
			for (int i = 0; i <= 4; i++){
				upwindingTerm += upw[q][i] * diff[i];
			}
    	    fluxRes[q] = 0.5 * (fluxLeft[q] + fluxRight[q]) - 0.5 * upwindingTerm;
    	    		
    	    if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("?");
    	    }
    	}
		
    	return new FluxAndWaveSpeed(fluxRes, localWaveSpeed);
	}
	
	protected double[][] upwindingMatrix(double eigenvalue, int direction){
		double[][] upw = new double[5][5];
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				upw[i][j] = 0.0;
			}
		}
		
		upw[0][0] = eigenvalue;
		upw[1][1] = eigenvalue;
		upw[2][2] = eigenvalue;
		upw[3][3] = eigenvalue;
		upw[4][4] = eigenvalue;

		int indexDirection = EquationsHydroIdeal.getIndexDirection(direction);
		//upw[indexDirection][4] = 0.4; 
		
		return upw;
	}


}
