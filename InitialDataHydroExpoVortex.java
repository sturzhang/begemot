public class InitialDataHydroExpoVortex extends InitialDataHydroVortex {

	protected double MachNumber, rho0, alpha;
	private double p0, v0;
	
	public InitialDataHydroExpoVortex(Grid grid, EquationsHydroIdeal equations, double MachNumber, double rho0) {
		super(grid, equations);
		this.MachNumber = MachNumber;
		
		this.rho0 = rho0;
		alpha = 20.0;
		v0 = alpha*alpha / 0.13;
		p0 = 1.0 / MachNumber / MachNumber * 20.0;
		
	}

    public String getDetailedDescription(){
    	return "Expo vortex with reference Mach number = " + MachNumber + ", constant density = " + rho0 + 
    			" and central pressure = " + p0;
    }
	
	@Override
	protected double speed(double r) {
		return v0 * r*r * Math.exp(-alpha*r);
	}

	@Override
	protected double pressure(double r) {
		/*return p0 - rho0*v0*v0 * Math.exp(-2.0*alpha*r) * 
				(4.0*Math.pow(alpha, 3)*r*r*r + 6*alpha*alpha*r*r + 6.0*alpha*r)
				/ 8 / Math.pow(alpha, 4);*/
		return p0 + (3.0 + Math.exp(-2.0*alpha*r) * (-3.0-2.0*alpha*r*(3.0+alpha*r*(3.0+2*alpha*r)))) 
				/ 8 / Math.pow(alpha,  4) * rho0*v0*v0;
	}


	@Override
	protected double rho(double r) {
		// TODO Auto-generated method stub
		return rho0;
	}

}
