
public class OutputAcousticKineticEnergy extends Output {

	protected boolean firsttime = true;
	protected double kinen0;
	
	public OutputAcousticKineticEnergy(double outputTimeInterval, GridCartesian grid,
			EquationsAcousticSimple equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints the total integral of the kinetic energy";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double kinen = 0;
    	int i, j, k;
		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			kinen += (quantities[i][j][k][EquationsAcousticSimple.INDEX_VX]*quantities[i][j][k][EquationsAcousticSimple.INDEX_VX] + 
					quantities[i][j][k][EquationsAcousticSimple.INDEX_VY]*quantities[i][j][k][EquationsAcousticSimple.INDEX_VY])/2;
		}
		kinen *= ((GridCartesian) grid).xSpacing();
		if (grid.dimensions() >= 2){
			kinen *= ((GridCartesian) grid).ySpacing();
			if (grid.dimensions() == 3){
				kinen *= ((GridCartesian) grid).zSpacing();
			}
		}
		
		if (firsttime){
			firsttime = false;
			kinen0 = kinen;
		}
		
		
		//println(time + " " + Math.abs(kinen - kinen0));  //*/
		println(time + " " + ((kinen - kinen0)/kinen0));
		 
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}

}
