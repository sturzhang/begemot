
public class OutputSampled extends Output {

	protected double[][][][] outputLattice;
	protected int[][][] counter;
	protected int nx, ny, nz;
	boolean simpleAverage = true;
	
	
	public OutputSampled(double outputTimeInterval, Grid grid, Equations equations) {
		super(outputTimeInterval, grid, equations);
		
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double prefactor = (simpleAverage ? 0.2 : 1.5);
		
		// TODO nx, ny, nz cannot be computed in the constructor as the grid is not yet done with initializing itself
		nx = Math.max((int) Math.round((grid.xmax() - grid.xmin())/grid.minSpacing()*prefactor), 1);
		ny = Math.max((int) Math.round((grid.ymax() - grid.ymin())/grid.minSpacing()*prefactor), 1);
		nz = Math.max((int) Math.round((grid.zmax() - grid.zmin())/grid.minSpacing()*prefactor), 1);

		
		outputLattice = new double[nx][ny][nz][quantities[0][0][0].length];
		counter = new int[nx][ny][nz];
		
		double checkRadius;
		
		for (GridCell g : grid){
			if (grid.isValidCell(g)){
				putOnLattice(g.getX(), g.getY(), g.getZ(), quantities[g.i()][g.j()][g.k()]);
				
				if (!simpleAverage){
					checkRadius = 3.0*Math.sqrt(grid.cellSize(g));
					for (double dx = -checkRadius; dx <= checkRadius; dx += checkRadius/20){
						for (double dy = -checkRadius; dy <= checkRadius; dy += checkRadius/20){
							if (grid.isInsideCell(g, g.getX()+dx, g.getY()+dy, g.getZ())){
								putOnLattice(g.getX()+dx, g.getY()+dy, g.getZ(), quantities[g.i()][g.j()][g.k()]);
							}
						}
					}
				}
			}
		}
		computeMeanOnLattice();
		
		outputLattice();
		//System.err.println("done");
	}

	protected void putOnLattice(double x, double y, double z, double[] quantities){
		int latticeI = getXIndex(x);
		int latticeJ = getYIndex(y);
		int latticeK = getZIndex(z);
		
		/*System.err.println(latticeI + " " + latticeJ + " " + latticeK + "|" + 
				outputLattice.length + " " + outputLattice[0].length + " " + outputLattice[0][0].length);
		*/
		if ((latticeI >= 0) && (latticeI < outputLattice.length) &&
		   (latticeJ >= 0) && (latticeJ < outputLattice[0].length) &&
		   (latticeK >= 0) && (latticeK < outputLattice[0][0].length)){


			for (int q = 0; q < quantities.length; q++){
				outputLattice[latticeI][latticeJ][latticeK][q] += quantities[q];
			}
			counter[latticeI][latticeJ][latticeK]++;
		}
	}
	
	protected void computeMeanOnLattice(){
		for (int i = 0; i < nx; i++){
			for (int j = 0; j < ny; j++){
				for (int k = 0; k < nz; k++){
					for (int q = 0; q < outputLattice[i][j][k].length; q++){
						outputLattice[i][j][k][q] /= Math.max(1, counter[i][j][k]);
					}
					
				}
			}
		}
	}
	
	protected void outputLattice(){
		
		
		for (int i = 0; i < nx; i++){
			for (int j = 0; j < ny; j++){
				for (int k = 0; k < nz; k++){
					println("");
					print(getX(i) + " ");
					if (grid.dimensions() >= 2){ print(getY(j) + " "); }
					if (grid.dimensions() >= 3){ print(getZ(k) + " "); }
					
					if (equations instanceof EquationsHydroIdeal){
						double vx, vy, vz, pressure, v2, rhoV2, soundspeed, mach;
						double angularmomentum;
						
						if (outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO] == 0){
							outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 1.0;
						}
						
						vx = outputLattice[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO];
						vy = outputLattice[i][j][k][EquationsHydroIdeal.INDEX_YMOM]/outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO];
						vz = outputLattice[i][j][k][EquationsHydroIdeal.INDEX_ZMOM]/outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO];
						v2 = vx*vx + vy*vy + vz*vz;
						rhoV2 = outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO]*v2;
						pressure = ((EquationsHydroIdeal) equations).getPressure(outputLattice[i][j][k][EquationsHydroIdeal.INDEX_ENERGY], rhoV2);
						if (pressure == 0){ pressure = 1.0; }
						
						soundspeed = ((EquationsHydroIdeal) equations).getSoundSpeedFromPressure(pressure, outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO]);
						mach = Math.sqrt(v2) / soundspeed;
						
						angularmomentum = (getX(i)-grid.xMidpoint()) * outputLattice[i][j][k][EquationsHydroIdeal.INDEX_YMOM] - 
								(getY(j)-grid.yMidpoint()) * outputLattice[i][j][k][EquationsHydroIdeal.INDEX_XMOM];
						
						
						print(outputLattice[i][j][k][EquationsHydroIdeal.INDEX_RHO] + " ");
						print(vx + " " + vy + " " + vz + " ");
						print(pressure + " ");
						print(soundspeed + " ");
						print(mach + " ");
						print(outputLattice[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] + " ");
						print(angularmomentum + " ");
						
						for (int s = equations.getNumberOfConservedQuantities(); s < outputLattice[i][j][k].length; s++){
							print(outputLattice[i][j][k][s] + " ");
						}
					} else { 
						for (int q = 0; q < outputLattice[i][j][k].length; q++){
							print(outputLattice[i][j][k][q] + " ");
						}
					}	
				}
			}
		}
		println("");
		println("");
	}
	
    protected double getX(int i){ return grid.xmin() + 1.0*i / (nx-1) * (grid.xmax() - grid.xmin()); }
    protected double getY(int j){ return grid.ymin() + 1.0*j / (ny-1) * (grid.ymax() - grid.ymin()); }
    protected double getZ(int k){ return grid.zmin() + 1.0*k / (nz-1) * (grid.zmax() - grid.zmin()); }
    
    protected int getXIndex(double x){ return (int) Math.round((x - grid.xmin())/(grid.xmax() - grid.xmin())*(nx-1)); }
    protected int getYIndex(double y){ return (int) Math.round((y - grid.ymin())/(grid.ymax() - grid.ymin())*(ny-1)); }
    protected int getZIndex(double z){ return (int) Math.round((z - grid.zmin())/(grid.zmax() - grid.zmin())*(nz-1)); }

	
	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getDetailedDescription() {
		return "Sampled output for general grids plotting conserved quantities";
	}

}
