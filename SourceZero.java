
public class SourceZero extends SourceInCellOnly {

	public SourceZero(Grid grid) {
		super(grid, null);
	}

    public String getDetailedDescription(){
    	return "(none)";
    }

	
	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell g, double time) {
		double[] res = new double[quantities.length];
		for (int m = 0; m < res.length; m++){
			res[m] = 0.0;
		}
		return res;
	}

}
