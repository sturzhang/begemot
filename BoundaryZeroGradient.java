
public class BoundaryZeroGradient extends Boundary {

	public BoundaryZeroGradient(Grid grid){
		super(grid);
	}
	
    public String getDetailedDescription(){
    	return "Zero-gradient (outflow)";
    }
	
    @Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		
		i = g.i(); j = g.j(); k = g.k();
		nextI = i; nextJ = j; nextK = k;
		
		if (grid instanceof GridCartesian){
			if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); }
			if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); }
			if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
				
			if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }
			if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }
			if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }
	
		} else {
			for (GridCell nb : grid.getNeighbours(g)){
				if (grid.isValidCell(nb)){
					nextI = nb.i(); nextJ = nb.j(); nextK = nb.k();
					break;
				}
			}
		}
		
		/* can be removed if ((j != nextJ) && (i == 50)){
			System.err.println(conservedQuantities[i][j][k][0] + " " + conservedQuantities[nextI][nextJ][nextK][0] + " " + 
						i + " " + j + " -> " + nextI + " " + nextJ);
		}*/
		
		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
		}

	}
	

}
