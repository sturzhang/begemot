public abstract class InitialDataHydro extends InitialData {

	protected EquationsHydro equations;
	
	public InitialDataHydro(Grid grid, EquationsHydro equations) { 
		super(grid);
		this.equations = equations;
	}

	
}
