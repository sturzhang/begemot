
public class OutputPressure extends Output {

	private double mach;
	
	public OutputPressure(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations, double mach, boolean doPrint) {
		super(outputTimeInterval, grid, equations);
		this.mach = mach;
	}

    public String getDetailedDescription(){
    	return "prints the mean value of the pressure";
    }
    
	
	@Override
	protected void printOutput(double time, double[][][][] quantities){
		double rhoV2, pressureMean = 0.0;
    	int counter = 0;
		double divV, divVMean = 0.0;
    	
		for (int i = ((GridCartesian) grid).indexMinX()+1; i <= ((GridCartesian) grid).indexMaxX()-1; i++){
    		for (int j = ((GridCartesian) grid).indexMinY()+1; j <= ((GridCartesian) grid).indexMaxY()-1; j++){
    			for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
    				counter++;
    				rhoV2 = (Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
    				
    				divV = (quantities[i+1][j][k][EquationsHydroIdeal.INDEX_XMOM] -  
   						 	quantities[i-1][j][k][EquationsHydroIdeal.INDEX_XMOM]) / ((GridCartesian) grid).xSpacing() +
   						 	(quantities[i][j+1][k][EquationsHydroIdeal.INDEX_YMOM] -  
   						 	quantities[i][j-1][k][EquationsHydroIdeal.INDEX_YMOM]) / ((GridCartesian) grid).ySpacing();
   					divV /= quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
    				
   					divVMean += divV;
   					
    				pressureMean += 
    						((EquationsHydroIdeal) equations).getPressure(quantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY], rhoV2);
    			}
    		}
    	}
		
		print(mach + " " + (pressureMean/counter) + " " + (divVMean/counter) + " ");  //*/
    	

    	/*int k = 0;
    	int i = 28;
    	int j = 27;
    	
		rhoV2 = (Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM], 2) + 
				 Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM], 2) +
				 Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
				quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		
		pressureMean += 
				equations.getPressure(quantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY], rhoV2);

		
		double vx, vy;
		vx = quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] / 
				quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		vy = quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] / 
				quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
				
		
		print(time + " " + mach + " " + (pressureMean) + " " + vx + " " + vy + " "); //*/
		
		
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

	
	
}
