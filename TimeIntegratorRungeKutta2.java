
public class TimeIntegratorRungeKutta2 extends TimeIntegratorRungeKutta {


	protected double[][] RKCoefficient = new double[3][1];
	protected double[] RKOnestepCoefficient = new double[] {0.0, 1.0, 0.5};

	public TimeIntegratorRungeKutta2(GridCartesian grid, double CFL) {
		super(grid, CFL, 2);
		
		RKCoefficient[2][0] = 0.5;		
	}

	@Override
	protected double RKCoefficient(int step, int n) { return RKCoefficient[step][n]; }

	@Override
	protected double RKOnestepCoefficient(int step) { return RKOnestepCoefficient[step]; }

}
