public abstract class Reconstruction{

	protected Grid grid;
	
    public abstract double[][][][][] perform(double[][][][] conservedQuantities, double[][][][][] interfaceValues);
    
    public abstract int numberOfGhostcells();

    public void setGrid(Grid grid){
    	this.grid = grid;
    }
    
    public abstract String getDetailedDescription();
    
    public String description(){ 
    	return "# Reconstruction used: " + getDetailedDescription(); 
    }
    


    
}