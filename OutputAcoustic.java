
public class OutputAcoustic extends OutputAllQuantities {
	
	public OutputAcoustic(double outputTimeInterval, GridCartesian grid, EquationsAcoustic equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		double vx, vy, vz, pressure;
		
		vx = quantities[EquationsAcoustic.INDEX_VX];
		vy = quantities[EquationsAcoustic.INDEX_VY];
		vz = quantities[EquationsAcoustic.INDEX_VZ];
		pressure = quantities[EquationsAcoustic.INDEX_P];
		
		//if (grid.getY(i, j, k) == 0){
		if (equations instanceof EquationsAcousticExtended){
			print(quantities[EquationsAcousticExtended.INDEX_RHO] + " ");
		}
		
		print(vx + " " + vy + " " + vz + " " );
		print(pressure + " " + (Math.sqrt(vx*vx+vy*vy+vz*vz)) + " ");
		//}
	}

	@Override
	public String getDetailedDescription() {
		return "prints: vx, vy, vz, pressure";
	}

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		// TODO Auto-generated method stub
		
	}

}
