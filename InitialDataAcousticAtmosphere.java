import java.util.Random;


public class InitialDataAcousticAtmosphere extends InitialDataAcoustic {

	protected double gy; // y-component of gravity
	protected boolean randomPerturbation;

	
	public InitialDataAcousticAtmosphere(Grid grid, EquationsAcousticSimple equations, double gy, boolean randomPerturbation) {
		super(grid, equations);
		this.gy = gy;
		this.randomPerturbation = randomPerturbation;
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		Random random = new Random();
		double[] res = new double[q];
		
		res[EquationsAcousticSimple.INDEX_P] = getPressure(g.getY());
    				
		if (randomPerturbation){
			res[EquationsAcousticSimple.INDEX_VX] = (random.nextDouble()-0.5)*1e-10;
		} else {
			res[EquationsAcousticSimple.INDEX_VX] = 0.0;
		}
		res[EquationsAcousticSimple.INDEX_VY] = 0.0;
		res[EquationsAcousticSimple.INDEX_VZ] = 0.0;
		
		return res;
	}
	
    public double getPressure(double y){
    	return 1.0+(-((GridCartesian) grid).ymin+y)*gy;
    }

	@Override
	public String getDetailedDescription() {
		return "COnstant-density hydrostatic atmosphere for linear equations";
	}



}
