
public class InitialDataHydroCheckerboard extends InitialDataHydro {

	double rho;
	double deltarho;
	double pressure;
	double deltapressure;
	double xvel;
	double deltav;
	
	
	public InitialDataHydroCheckerboard(Grid grid, EquationsHydro equations, 
			double rho, double deltarho, double xvel, double deltav, double pressure, double deltapressure) {
		super(grid, equations);
		this.rho = rho;
		this.deltarho = deltarho;
		this.pressure = pressure;
		this.deltapressure = deltapressure;
		this.xvel = xvel;
		this.deltav = deltav;
		
		
	}

	protected double saw(double x, double width){
		while (x > width){ x -= width; }
		
		if (x < width/2){ return x/width*2; } else { return 1-(x-width/2)/width*2; }
	}
	
	protected double straightline(double x, double slope){
		return x*slope;
	}
	
	
	@Override
	public double[] getInitialValue(GridCell g, int q){
		double rhoV2;
		double[] velocity = new double[3];
		double[] res = new double[q];
		double x = g.getX();
		double slope = 0.0; //4000.0;
		double width = 0.1;
		
		double newpressure = pressure + Math.pow(-1.0, g.i()+g.j())*deltapressure;
		//newpressure += Math.sin(grid.getX(i) / (grid.xmax()-grid.xmin()) * 2.0*Math.PI * 5.0);
		//newpressure += (1 - 2.0*Math.abs((5.0*grid.getX(i) % 1.0 )/5-0.5));
		//newpressure += saw(grid.getX(i), 0.2); //(grid.xmax() - grid.xmin())*2.0);
		double newrho = rho + Math.pow(-1.0, g.i()+g.j())*deltarho;
		//newrho += saw(grid.getX(i), 0.25);
		//newrho += 90.0 + grid.getX(i)*90.0;
		//newrho += 200.0 + grid.getX(i)*200.0;
		//newrho += 400.0; // - grid.getX(i)*200.0;
		
		if ((x > 0.5-width) && (x < 0.5)){ 
			newrho += slope*(x-0.5 + width);
		}
		if ((x < 0.5+width) && (x >= 0.5)){
			newrho += slope*width - slope*(x-0.5);
		}
		
		//newrho += 180.0 - grid.getX(i)*90.0;
		double newxvel = xvel + Math.pow(-1.0, g.i()+g.j())*deltav;
		//newxvel += saw(grid.getX(i), 0.5);
		//newxvel += saw(grid.getX(i), (grid.xmax() - grid.xmin())*2.0); // - 0.5;
		//newxvel += straightline(grid.getX(i), -1.0);
		
		velocity[1] = 0.0; //2.0*xvel + 2.0*Math.pow(-1.0, i+j)*deltav;
		velocity[2] = 0.0;
		velocity[0] = newxvel;
    					
		rhoV2 = newrho*(velocity[0]*velocity[0] + velocity[1]*velocity[1] + velocity[2]*velocity[2]);
    				
		res[EquationsHydroIdeal.INDEX_RHO] = newrho;
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(newpressure, rhoV2);
    				
		res[EquationsHydroIdeal.INDEX_XMOM] = newrho*velocity[0];
		res[EquationsHydroIdeal.INDEX_YMOM] = newrho*velocity[1];
		res[EquationsHydroIdeal.INDEX_ZMOM] = newrho*velocity[2];
		
		return res;
	}
	
    public String getDetailedDescription(){
    	return "Ideal gas of density = " + 
    			rho + " pm " + deltarho + " and x-velocity = " + 
    			xvel + " pm " + deltav + " and pressure = " + 
    			pressure + " pm " + deltapressure;
    }

}
