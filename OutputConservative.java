
public class OutputConservative extends OutputAllQuantities {

	public OutputConservative(double outputTimeInterval, GridCartesian grid, Equations equations) {
		super(outputTimeInterval, grid, equations);
	}
	
	public OutputConservative(double outputTimeInterval, double zslice, GridCartesian3D grid, Equations equations) {
		super(outputTimeInterval, zslice, grid, equations);
	}

	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		for (int q = 0; q < quantities.length; q++){
			print(quantities[q] + " ");
		}
	}
	
    public String getDetailedDescription(){
    	return "prints the conserved quantities";
    }

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		// TODO Auto-generated method stub
		
	}


}
