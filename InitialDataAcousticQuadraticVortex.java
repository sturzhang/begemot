
public class InitialDataAcousticQuadraticVortex extends InitialDataAcoustic {

	protected double pconst, width, vmax;
	
	
	public InitialDataAcousticQuadraticVortex(Grid grid, EquationsAcousticSimple equations, double width, double vmax, double pconst) {
		super(grid, equations);
		this.pconst = pconst;
		this.width = width;
		this.vmax = vmax;
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		
		double x, y, r, v;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		r = Math.sqrt(x*x+y*y);
		res[EquationsAcousticSimple.INDEX_P] = pconst;
    		
		
		v = vmax*Math.exp(-(r-width)*(r-width)/0.03/width/width);
		
    	/*if (r < width/2){
    		v = 2.0*vmax*r*r/width/width;
    	} else {
    		if (r < 3.0*width/2){
    			v = vmax-2.0*vmax*Math.pow((r-width)/width, 2);
    		} else {
    			if (r < 2.0*width){
    				v = 2.0*vmax*Math.pow((r-2.0*width)/width, 2);
    			}else {
    				v = 0.0;
    			}
    		}
    	}*/
    	
    	if (r == 0){
    		res[EquationsAcousticSimple.INDEX_VX] = 0.0;
    		res[EquationsAcousticSimple.INDEX_VY] = 0.0;
    	} else  {
    		res[EquationsAcousticSimple.INDEX_VX] = -y/r*v;
    		res[EquationsAcousticSimple.INDEX_VY] = x/r*v;
    	}	
    	res[EquationsAcousticSimple.INDEX_VZ] = 0.0;
    
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Vortex with quadratic, differentiable phi-velocity, at maximum v = " + vmax + ", pressure const = " + pconst + ", width = " + width;
	}

}
