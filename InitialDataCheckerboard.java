public class InitialDataCheckerboard extends InitialData {

	protected double amplitude;
	
	public InitialDataCheckerboard(Grid grid, double amplitude) {
		super(grid);
		this.amplitude = amplitude;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[]res = new double[numberOfConservedQuantities];
		
		for (int q = 0; q < numberOfConservedQuantities; q++){
			//if (q != EquationsHydroIdeal.INDEX_ZMOM){
			if (q == 0){
				//res[q] = amplitude * Math.pow(-1.0, g.j());
				res[q] = amplitude * Math.cos(Math.PI/2 * g.i() + Math.PI/2 * g.j());
			} else {
				res[q] = 0;
			}
		}
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Initial data checkerboard of amplitude " + amplitude;
	}

}
