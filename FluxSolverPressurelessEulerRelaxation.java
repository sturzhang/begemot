
public class FluxSolverPressurelessEulerRelaxation extends FluxSolverDimSplitEdgeNeighboursOnly {

	public FluxSolverPressurelessEulerRelaxation(Grid grid, EquationsPressurelessEuler equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		double uL = left[ EquationsPressurelessEuler.INDEX_XMOM]/left[ EquationsPressurelessEuler.INDEX_RHO];
		double uR = right[EquationsPressurelessEuler.INDEX_XMOM]/right[EquationsPressurelessEuler.INDEX_RHO];
		
		double rhoL = left[ EquationsPressurelessEuler.INDEX_RHO];
		double rhoR = right[EquationsPressurelessEuler.INDEX_RHO];
		
		double EPSILON = grid.minSpacing()*10;
		
		double a = Math.max(EPSILON, Math.max(rhoL*(uL - uR)/2, rhoR*(uL-uR)/2));
		
		double uStar = (uL + uR)/2;
		double PiStar = (uL - uR)*a/2;
		double rhoStar;
		if (uStar > 0){
			rhoStar = 1.0/(1.0/rhoL + (uR - uL)/2/a);
		} else {
			rhoStar = 1.0/(1.0/rhoR + (uR - uL)/2/a);
		}
		
		double[] flux = new double[4];
		flux[EquationsPressurelessEuler.INDEX_RHO ] = rhoStar*uStar;
		flux[EquationsPressurelessEuler.INDEX_XMOM] = rhoStar*uStar*uStar + PiStar;
		
		return new FluxAndWaveSpeed(flux, Math.abs(uStar) + a/rhoStar);
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left,
			double[] right, int direction) {
		// not implemented
		return null;
	}

	@Override
	public String getDetailedDescription() {
		return "Relaxation solver as in Berthon et al. 2006";
	}

}
