
public abstract class EquationsAdvection extends EquationsScalar {

	private double[] normalVector = null;
	
	
	public EquationsAdvection(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}
	
	public abstract double[] velocityWithoutTransformation(int i, int j, int k);
		
	public double[] velocity(int i, int j, int k){
		double[] vel = velocityWithoutTransformation(i,j,k);
		if (normalVector == null){
			return vel;
		} else {
			return transformVector(vel[0], vel[1], vel[2], normalVector);
		}
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k, double[] quantities,
			int direction) {
		return velocity(i,j,k)[direction];
	}	
	
	@Override
	public void transformFixedParameters(double[] normalVector) {
		this.normalVector = normalVector;
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		this.normalVector = null;
	}
	
}
