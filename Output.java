public abstract class Output {
	
	protected double outputTimeInterval;
	protected double timeOfLastOutput;
	protected Grid grid;
	protected boolean forcedOutput = false;
	protected Equations equations;
	protected boolean doPrint = true;
	
	protected Printer printer = new PrinterStdOut(); 
	
	public Output(double outputTimeInterval, Grid grid, Equations equations){
		this.grid = grid;
		this.outputTimeInterval = outputTimeInterval;
		timeOfLastOutput = 0.0;
		this.equations = equations;
		if (outputTimeInterval == 0.0){ forcedOutput = true; }
	}
	
	public double timeTillNextOutput(double time){
		return timeOfLastOutput + outputTimeInterval - time;
	}
	
    public abstract String getDetailedDescription();
    
    public double outputTimeInterval(){ return outputTimeInterval; }
    
    public void setPrinter(Printer printer){ this.printer = printer; };
    
    public String description(){ 
    	return "# Output every " + outputTimeInterval + " " + getDetailedDescription();
    }
    
	public void produce(double time, double[][][][] quantities){
		if ((forcedOutput)|| (time - timeOfLastOutput > outputTimeInterval)){
			printOutput(time, quantities);
			finalizeOutput();
			
			timeOfLastOutput += outputTimeInterval;
		}
	}
	
	public void produceForced(double time, double[][][][] quantities){
		printOutput(time, quantities);
	}
	
	public void print(String text){ if (doPrint) {printer.print(text);} }
	public void println(String text){ if (doPrint) {printer.println(text);} }
	
	
	
	protected abstract void printOutput(double time, double[][][][] quantities);
	protected abstract void finalizeOutput();
}
