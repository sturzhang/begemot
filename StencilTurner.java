
public abstract class StencilTurner {

	protected Grid grid;
	
	public StencilTurner(Grid grid) {
		this.grid = grid;
	}

	public abstract GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge);
	
}
