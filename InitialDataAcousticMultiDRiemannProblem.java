
public class InitialDataAcousticMultiDRiemannProblem extends InitialDataAcoustic {

	protected double vA, vB, vC, vD;
	protected double uxA, uxB, uxC, uxD;
	protected double uyA, uyB, uyC, uyD;
	
	protected int test;
	protected int GOSSE_TEST1 = 1;
	protected int GOSSE_TEST2 = 2;
	protected int GOSSE_TEST3 = 3;
	protected int GOSSE_TEST4 = 4;
	protected int GOSSE_TEST5 = 5;
	
	
	public InitialDataAcousticMultiDRiemannProblem(Grid grid, EquationsAcousticSimple equations) {
		super(grid, equations);
		
		/* 
		 *      A   |   B
		 *      ----------
		 *      D   |   C
		 *      
		 */
		
		test = 0; //GOSSE_TEST3;
		
		if (test == 0){
			vA = 0.0;
			vB = 0.0;
			vC = 0.0;
			vD = 0.0;
			
			uxA = 0.0;
			uxB = 1.0;
			uxC = 1.0;
			uxD = 0.0;
			
			uyA = 0.0;
			uyB = 0.0;
			uyC = 0.0;
			uyD = 0.0;
		} else if (test == GOSSE_TEST1){
			vA = 0.0; vB = 0.0; vC = 0.0; vD = 0.0;
			uxA = 0.0; uxB = 0.0; uxC = 0.0; uxD = 0.0; uyA = -1.0; uyB = 1.0; uyC = -1.0; uyD = 1.0;
		} else if (test == GOSSE_TEST2){
			vA = 0.0; vB = 0.0; vC = 0.0; vD = 0.0;
			uxA = -0.5; uxB = 0.5; uxC = -0.5; uxD = 0.5; uyA = -1.0; uyB = 1.0; uyC = -1.0; uyD = 1.0;
		} else if (test == GOSSE_TEST3){
			vA = 0.0; vB = 0.0; vC = 0.0; vD = 0.0;
			uxA = -1.0; uxB = 1.0; uxC = -1.0; uxD = 1.0; uyA = -1.0; uyB = 1.0; uyC = -1.0; uyD = 1.0;
		} else if (test == GOSSE_TEST4){
			vA = 0.0; vB = 0.0; vC = 0.0; vD = 0.0;
			uxA = -1.0; uxB = 1.0; uxC = -1.0; uxD = 1.0; uyA = -0.5; uyB = 0.5; uyC = -0.5; uyD = 0.5;
		} else if (test == GOSSE_TEST5){
			vA = 0.0; vB = 0.0; vC = 0.0; vD = 0.0;
			uxA = -1.0; uxB = 1.0; uxC = -1.0; uxD = 1.0; uyA = 0.0; uyB = 0.0; uyC = 0.0; uyD = 0.0;
		}
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		double x, y;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		
		/*if (x < -y*0.4){
			res[EquationsAcoustic.INDEX_P] = 0.0;
			res[EquationsAcoustic.INDEX_VX] = 0.0;
			res[EquationsAcoustic.INDEX_VY] = 1.0;			
		} else {
			res[EquationsAcoustic.INDEX_P] = 0.0;
			res[EquationsAcoustic.INDEX_VX] = 0.0;
			res[EquationsAcoustic.INDEX_VY] = 0.0;
		}//*/
		
		double alpha = 0.0/180*Math.PI;
		double cosa = Math.cos(alpha);
		double sina = Math.sin(alpha);
		
		double verticalseparation   = -y*sina; 
		double horizontalseparation =  x*sina; 
		
		double tuxA = cosa * uxA - sina * uyA;
		double tuyA = sina * uxA + cosa * uyA;
		double tuxB = cosa * uxB - sina * uyB;
		double tuyB = sina * uxB + cosa * uyB;
		double tuxC = cosa * uxC - sina * uyC;
		double tuyC = sina * uxC + cosa * uyC;
		double tuxD = cosa * uxD - sina * uyD;
		double tuyD = sina * uxD + cosa * uyD;
		
		
		if (( x <= verticalseparation ) && ( y > horizontalseparation )){
			res[EquationsAcousticSimple.INDEX_P] = vA;
			res[EquationsAcousticSimple.INDEX_VX] = tuxA;
			res[EquationsAcousticSimple.INDEX_VY] = tuyA;
		}
		if (( x > verticalseparation ) && ( y > horizontalseparation )){
			res[EquationsAcousticSimple.INDEX_P] = vB;
			res[EquationsAcousticSimple.INDEX_VX] = tuxB;
			res[EquationsAcousticSimple.INDEX_VY] = tuyB;
		}
		if (( x > verticalseparation ) && ( y <= horizontalseparation )){
			res[EquationsAcousticSimple.INDEX_P] = vC;
			res[EquationsAcousticSimple.INDEX_VX] = tuxC;
			res[EquationsAcousticSimple.INDEX_VY] = tuyC;
		}
		if (( x <= verticalseparation ) && ( y <= horizontalseparation )){
			res[EquationsAcousticSimple.INDEX_P] = vD;
			res[EquationsAcousticSimple.INDEX_VX] = tuxD;
			res[EquationsAcousticSimple.INDEX_VY] = tuyD;
		}
    				
		res[EquationsAcousticSimple.INDEX_VZ] = 0.0; //*/

		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Corner Riemann problem for acoustic system in 2-d";
	}

}
