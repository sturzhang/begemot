public class FluxSolverSplitConsistentGalilei extends FluxSolverRoe {

	public FluxSolverSplitConsistentGalilei(Grid grid, EquationsIsentropicEuler equations) {
		super(grid, equations);
	}
	
	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		double[] flux;
		GridCell g = gridEdge.adjacent1();
		
		double vL = left[EquationsIsentropicEuler.INDEX_XMOM]/left[EquationsIsentropicEuler.INDEX_RHO];
		double vR = right[EquationsIsentropicEuler.INDEX_XMOM]/right[EquationsIsentropicEuler.INDEX_RHO];
		double rhoL = left[EquationsIsentropicEuler.INDEX_RHO];
		double rhoR = right[EquationsIsentropicEuler.INDEX_RHO];
		double cL = ((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(left);
		double cR = ((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(right);
		double shockspeedLeft = Math.min(vL - cL, vR - cR);
		double shockspeedRight = Math.max(vL + cL, vR + cR);
		
		//double rhoStar = (rhoL + rhoR)/2;
		//double vAdvection = (vL + vR)/2;
		
		/*double rhoStar = (rhoR*shockspeedRight - rhoL*shockspeedLeft - rhoR*vR + rhoL*vL)/
				(shockspeedRight - shockspeedLeft);*/
		
		// TODO works only for gamma = 1
		double sqrtK = Math.sqrt(((EquationsIsentropicEuler) equations).K());
		double denominator = 2.0*(sqrtK*(rhoL - rhoR) - rhoL*vL + rhoR*vR);
		double rhoStar;
		if (denominator == 0){
			if (vR == sqrtK){
				rhoStar = rhoL/2; // TODO check this again
			} else {
				rhoStar = (- 2.0*sqrtK*rhoL + rhoL*vL + rhoL*vR)/2/(vR - sqrtK);
			}
		} else {
			rhoStar = (cL * rhoL * (rhoL - rhoR) + cR * (rhoL - rhoR) * rhoR - (rhoL + rhoR) * (rhoL * vL - rhoR * vR))/denominator;
		}
		
		double pL = ((EquationsIsentropicEuler) equations).getPressure(rhoL);
		double pR = ((EquationsIsentropicEuler) equations).getPressure(rhoR);
		double vAdvection = (rhoR*vR*shockspeedRight - rhoL*vL*shockspeedLeft - rhoR*vR*vR - pR + rhoL*vL*vL + pL)/
				(shockspeedRight - shockspeedLeft) / rhoStar;//*/
		
		
		double momStar = 0.5*(rhoL*(vL - vAdvection) + 2.0*rhoStar * vAdvection + rhoR*(vR - vAdvection) );
		
		if (shockspeedLeft > 0){
		    flux = equations.fluxFunction(g.i(), g.j(), g.k(), left, GridCartesian.X_DIR);
		} else {
		    if (shockspeedRight < 0){
		    	flux = equations.fluxFunction(g.i(), g.j(), g.k(), right, GridCartesian.X_DIR);
		    } else {
		    	flux = equations.fluxFunction(g.i(), g.j(), g.k(), new double[] {rhoStar, momStar, 0, 0}, GridCartesian.X_DIR);
		    }
		}
		
		if (momStar != 0){
			//System.err.println(rhoL + " " + rhoR + " | " +rhoStar + " " + momStar + " | " + flux[0] + " " + flux[1]);
		}
				
    	return new FluxAndWaveSpeed(flux, Math.max(Math.abs(shockspeedRight), Math.abs(shockspeedLeft)));
	}

}
