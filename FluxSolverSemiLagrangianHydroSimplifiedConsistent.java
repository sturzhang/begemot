
public class FluxSolverSemiLagrangianHydroSimplifiedConsistent extends
		FluxSolverSemiLagrangianHydroConsistent {

	public FluxSolverSemiLagrangianHydroSimplifiedConsistent(
			GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	
	
	@Override
	protected double[] getAcousticFlux(double uStar, double PiStar, int qq){
		return new double[qq]; // no flux at all, the halfstep is there to compute things, there is no evolution yet
	}
	
	@Override
	protected double[] getAdvectiveFlux(double uStar, double PiStar, double Vjp12, double Vjm12, 
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, 
			int numberOfQuantities){
		double[] fluxRes = new double[numberOfQuantities];
		
		double V = (Vjp12 + Vjm12)/2;
		
		int advectiveDiffusion                = 1;
		int pressurelessEulerDiffusion        = 2;
		int pressurelessEulerVariantDiffusion = 3;

		int typeOfAdvectiveStep = pressurelessEulerDiffusion;

		
		fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_XMOM] = PiStar;
		fluxRes[EquationsHydroIdeal.INDEX_YMOM] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = PiStar*uStar;
		
		
		double absUStar = Math.abs(uStar);
		double signUStar = (uStar == 0 ? 0 : uStar/absUStar);
		
		if (typeOfAdvectiveStep == advectiveDiffusion){
			double velDeriv, velOrthDeriv;
			
			for (int q = 0; q < numberOfQuantities; q++){
				fluxRes[q] += uStar*(ip1jk[q] + ijk[q])/2 - absUStar*(ip1jk[q] - ijk[q])/2;
				
				/*fluxRes[q] += uStar*(ip1jp1k[q] + ijp1k[q] + 2.0*(ip1jk[q] + ijk[q]) + ip1jm1k[q] + ijm1k[q])/8;
				
				velDeriv = absUStar*(ip1jp1k[q] - ijp1k[q] + 2.0*(ip1jk[q] - ijk[q]) + ip1jm1k[q] - ijm1k[q])/8;
				velOrthDeriv = signUStar*V*(ip1jp1k[q] - ip1jm1k[q]  + ijp1k[q] - ijm1k[q] )/8;	
				
				if ((V != 0) && (uStar != 0) && (velOrthDeriv != 0)){
					//System.err.println(V + " " + uStar);
				}
				
				
				if (verbose){
					System.err.println(q + " ---------------- ");
					System.err.println(velDeriv);
					System.err.println(velOrthDeriv);
					System.err.println((velDeriv  +  velOrthDeriv));
				}
					
				fluxRes[q] -= velDeriv;
				fluxRes[q] -= velOrthDeriv; //*/
			}
		} else if (typeOfAdvectiveStep == pressurelessEulerDiffusion){
			double EPSILON = grid.minSpacing()*10;
			
			double uL = ijk[ EquationsHydroIdeal.INDEX_XMOM]/ijk[ EquationsHydroIdeal.INDEX_RHO];
			double uR = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
			
			double rhoL = ijk[ EquationsHydroIdeal.INDEX_RHO];
			double rhoR = ip1jk[EquationsHydroIdeal.INDEX_RHO];
			double eL = ijk[ EquationsHydroIdeal.INDEX_ENERGY];
			double eR = ip1jk[EquationsHydroIdeal.INDEX_ENERGY];
			
			
			double a = Math.max(EPSILON, Math.max(rhoL*(uL-uR)/2, rhoR*(uL-uR)/2));
			
			uStar = (uL + uR)/2;
			
			PiStar = (uL - uR)*a/2;
			//PiStar = 0;
			
			double rhoStar, eStar;
			double myUpw, mxUpw;
			if (uStar > 0){
				rhoStar = 2.0*a*rhoL/(2.0*a + rhoL*(uR - uL));
				//rhoStar = ijk[EquationsHydroIdeal.INDEX_RHO];
				//rhoStar = rhoL;
				
				eStar = 2.0*a*eL/(2.0*a + rhoL*(uR - uL));
				//eStar = ijk[EquationsHydroIdeal.INDEX_ENERGY];
				//eStar = eL; // + rhoL*(uR*uR - uL*uL)/4;
				
				//mxUpw = ijk[EquationsHydroIdeal.INDEX_XMOM];
				
				//myUpw = ijk[EquationsHydroIdeal.INDEX_YMOM];
				myUpw = ijk[EquationsHydroIdeal.INDEX_YMOM]/(1 + rhoL*(uR - uL)/2/a);
			} else {
				rhoStar = 2.0*a*rhoR/(2.0*a - rhoR*(uL - uR));
				//rhoStar = ip1jk[EquationsHydroIdeal.INDEX_RHO];
				//rhoStar = rhoR;
				
				eStar = 1.0/(1.0/eR + (uR - uL)*rhoR/2/a/eR);
				//eStar = ip1jk[EquationsHydroIdeal.INDEX_ENERGY];
				//eStar = eR; // + rhoR*(uL*uL - uR*uR)/4;
				
				//mxUpw = ip1jk[EquationsHydroIdeal.INDEX_XMOM];
				
				myUpw = ip1jk[EquationsHydroIdeal.INDEX_YMOM];
				//myUpw = ip1jk[EquationsHydroIdeal.INDEX_YMOM]/(1 + rhoR*(uR - uL)/2/a);
			}
			
			fluxRes[EquationsHydroIdeal.INDEX_RHO ]   += rhoStar*uStar;
			fluxRes[EquationsHydroIdeal.INDEX_XMOM]   += rhoStar*uStar*uStar; // + PiStar;
			//fluxRes[EquationsHydroIdeal.INDEX_XMOM]   += uStar*mxUpw; // + PiStar;
			fluxRes[EquationsHydroIdeal.INDEX_YMOM]   += uStar * myUpw;
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] += (eStar + PiStar)*uStar;
		} else if (typeOfAdvectiveStep == pressurelessEulerVariantDiffusion){
			double EPSILON = grid.minSpacing()*100;
			
			double uL = ijk[ EquationsHydroIdeal.INDEX_XMOM]/ijk[ EquationsHydroIdeal.INDEX_RHO];
			double uR = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
			/*double uL = (ijp1k[ EquationsHydroIdeal.INDEX_XMOM]/ijp1k[ EquationsHydroIdeal.INDEX_RHO]
					+ 2.0 * ijk[ EquationsHydroIdeal.INDEX_XMOM]/ijk[ EquationsHydroIdeal.INDEX_RHO] 
					+ ijm1k[ EquationsHydroIdeal.INDEX_XMOM]/ijm1k[ EquationsHydroIdeal.INDEX_RHO])/4;
			double uR = (ijm1k[EquationsHydroIdeal.INDEX_XMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO]
					+ 2.0 * ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO]
					+ ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO])/4; //*/
			
			double vT = (ijp1k[ EquationsHydroIdeal.INDEX_YMOM]/ijp1k[ EquationsHydroIdeal.INDEX_RHO] + ip1jp1k[ EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[ EquationsHydroIdeal.INDEX_RHO])/2;
			double vB = (ijm1k[ EquationsHydroIdeal.INDEX_YMOM]/ijm1k[ EquationsHydroIdeal.INDEX_RHO] + ip1jm1k[ EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[ EquationsHydroIdeal.INDEX_RHO])/2;
			
			
			double rhoL = ijk[ EquationsHydroIdeal.INDEX_RHO];
			double rhoR = ip1jk[EquationsHydroIdeal.INDEX_RHO];
			//double eL = ijk[ EquationsHydroIdeal.INDEX_ENERGY];
			//double eR = ip1jk[EquationsHydroIdeal.INDEX_ENERGY];
			
			Vjp12 = vT;
			Vjm12 = vB;
			
			//double a = Math.max(EPSILON, Math.max(rhoL*(uL-uR)/2, rhoR*(uL-uR)/2));
			double a = Math.max(EPSILON, Math.max(-rhoL*(uR - uL + (Vjp12 - Vjm12)/2)/2, 
					-rhoR*(uR - uL + (Vjp12 - Vjm12)/2)/2));
			
			
			uStar = (uL + uR)/2;
			double absUstar = Math.abs(uStar);
			signUStar = Math.signum(uStar);
			
			PiStar = (uL - uR)*a/2;
			
			
			/* fluxRes[EquationsHydroIdeal.INDEX_RHO ]   += uStar/2 * (2.0*a*rhoL/(2.0*a + rhoL*(uR - uL)) + 2.0*a*rhoR/(2.0*a - rhoR*(uL - uR)))
					+ absUstar/2 * (2.0*a*rhoL/(2.0*a + rhoL*(uR - uL)) -  2.0*a*rhoR/(2.0*a - rhoR*(uL - uR)));
					
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] += uStar/2 * ( 2.0*a*eL/(2.0*a + rhoL*(uR - uL)) +  1.0/(1.0/eR + (uR - uL)*rhoR/2/a/eR) + 2.0*PiStar )
					+ absUstar/2 * (2.0*a*eL/(2.0*a + rhoL*(uR - uL)) - 1.0/(1.0/eR + (uR - uL)*rhoR/2/a/eR) );
			*/
			
			//double fL = 1.0/(1 + rhoL*(uR - uL)/2/a); 
			//double fR = 1.0/(1 + rhoR*(uR - uL)/2/a);
			double fL = 1.0/(1 + rhoL*(uR - uL + (Vjp12 - Vjm12)/2)/2/a); 
			double fR = 1.0/(1 + rhoR*(uR - uL + (Vjp12 - Vjm12)/2)/2/a);
			
			// TODO energy flux has addition pistar in principle
			
			for (int q = 0; q < numberOfQuantities; q++){
				/*fluxRes[q] += uStar/2 * (ijk[q]*fL + ip1jk[q]*fR) 
						- absUstar/2 * (ip1jk[q]*fR - ijk[q]*fL)
					- signUStar*V/2 *(ip1jp1k[q]*fR - ip1jm1k[q]*fR  + ijp1k[q]*fL - ijm1k[q]*fL )/4; */
				
				fluxRes[q] += uStar/2 * ((ijp1k[q] + 2.0*ijk[q] + ijm1k[q])/4*fL + (ip1jp1k[q] + 2.0*ip1jk[q] + ip1jm1k[q])/4*fR) 
						- absUstar/2 * ((ip1jp1k[q] + 2.0*ip1jk[q] + ip1jm1k[q])/4*fR - (ijp1k[q] + 2.0*ijk[q] + ijm1k[q])/4*fL)
					- signUStar*V/2 *(ip1jp1k[q]*fR - ip1jm1k[q]*fR  + ijp1k[q]*fL - ijm1k[q]*fL )/4;
					
			}
			
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] += PiStar*uStar;
					
		}
		
		if (verbose){
			System.err.println("#############");
		}
	
		return fluxRes;
	}
	
}
