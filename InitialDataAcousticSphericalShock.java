
public class InitialDataAcousticSphericalShock extends InitialDataAcoustic {

	public InitialDataAcousticSphericalShock(Grid grid,
			EquationsAcousticSimple equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double r;
    	
    	r = Math.sqrt(Math.pow(g.getX() - grid.xMidpoint(), 2) + 
    			      Math.pow(g.getY() - grid.yMidpoint(), 2));
    			
    	if (r < (grid.xmax() - grid.xmin())/4){
    		res[EquationsAcousticSimple.INDEX_P] = 1.0;
    	} else {
    		res[EquationsAcousticSimple.INDEX_P] = 0.0;    					
    	}
    	res[EquationsAcousticSimple.INDEX_VX] = 1.0;
    	res[EquationsAcousticSimple.INDEX_VY] = 0.0;
    	res[EquationsAcousticSimple.INDEX_VZ] = 0.0;
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Spherical shock 'tube'";
	}

}
