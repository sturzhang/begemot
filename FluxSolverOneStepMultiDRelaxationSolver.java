
public class FluxSolverOneStepMultiDRelaxationSolver extends
		FluxSolverOneStepMultiD {

	// scheme from publication "Truly multi-dimensional all-speed schemes for the Euler equations", subm. 2020
	
	public FluxSolverOneStepMultiDRelaxationSolver(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		
		boolean useMultiD = true;
		
		double[] fluxRes = new double[equations.getNumberOfConservedQuantities()];
		
		double uStar, relaxationSpeedRho, PiStar;
		
		double uip1j   = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double uij     = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double uip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double uijp1   = ijp1k[EquationsHydroIdeal.INDEX_XMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double uip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double uijm1   = ijm1k[EquationsHydroIdeal.INDEX_XMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
		
		double vip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double vijp1   = ijp1k[EquationsHydroIdeal.INDEX_YMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double vip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double vijm1   = ijm1k[EquationsHydroIdeal.INDEX_YMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
				
		EquationsHydroIdeal eq = ((EquationsHydroIdeal) equations);
		
		
		
		double pip1j   = eq.getPressureFromConservative(ip1jk);
		double pij     = eq.getPressureFromConservative(ijk);
		double pip1jp1 = eq.getPressureFromConservative(ip1jp1k);
		double pijp1   = eq.getPressureFromConservative(ijp1k);
		double pip1jm1 = eq.getPressureFromConservative(ip1jm1k);
		double pijm1   = eq.getPressureFromConservative(ijm1k);
		
		double cR = eq.getSoundSpeedFromConservative(ip1jk);
		double cL = eq.getSoundSpeedFromConservative(ijk);
		
		//double maxEVal = relaxationSpeed/Math.min(ip1jk[EquationsHydroIdeal.INDEX_RHO], ijk[EquationsHydroIdeal.INDEX_RHO]) + Math.abs(uStar);
		double maxEVal = Math.max(cR, cL) + Math.max(Math.abs(uij), Math.abs(uip1j)); //+ Math.abs(uStar);
				
		/*relaxationSpeed = Math.max(cR*ip1jk[EquationsHydroIdeal.INDEX_RHO], 
				cL*ijk[EquationsHydroIdeal.INDEX_RHO]); */
		relaxationSpeedRho = Math.max(ijk[EquationsHydroIdeal.INDEX_RHO], ip1jk[EquationsHydroIdeal.INDEX_RHO])*maxEVal;
		
		double diffP = 0.25*(pip1jp1 - pijp1 + 2.0*(pip1j - pij) + pip1jm1 - pijm1);
		double diffU = 0.25*(uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1);
		
		double diffV = 0.25*(vijp1 - vijm1 + vip1jp1 - vip1jm1);
		
		double sumP  = 0.25*(pip1jp1 + pijp1 + 2.0*(pip1j + pij) + pip1jm1 + pijm1);
		double sumU  = 0.25*(uip1jp1 + uijp1 + 2.0*(uip1j + uij) + uip1jm1 + uijm1);
		
		if (useMultiD){
			uStar  = 0.5*sumU - 0.5/relaxationSpeedRho*diffP;
			PiStar = 0.5*sumP - relaxationSpeedRho/2*(diffU + diffV);
		} else {		
			uStar = 0.5*(uip1j + uij) - 0.5/relaxationSpeedRho*(pip1j - pij);
			PiStar = 0.5*(pip1j + pij) - relaxationSpeedRho/2*(uip1j - uij);
		}
		
		double uL = ijk[ EquationsHydroIdeal.INDEX_XMOM]/ijk[ EquationsHydroIdeal.INDEX_RHO];
		double uR = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		
		double rhoL = ijk[ EquationsHydroIdeal.INDEX_RHO];
		double rhoR = ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double eL = ijk[ EquationsHydroIdeal.INDEX_ENERGY];
		double eR = ip1jk[EquationsHydroIdeal.INDEX_ENERGY];
		
		
		if (uStar > maxEVal){ // maybe we should only compare to the soundspeed!
			fluxRes[EquationsHydroIdeal.INDEX_RHO] = uL * rhoL;
			fluxRes[EquationsHydroIdeal.INDEX_XMOM] = uL * uL * rhoL + pij;
			fluxRes[EquationsHydroIdeal.INDEX_YMOM] = uL * ijk[EquationsHydroIdeal.INDEX_YMOM];
			fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = uL * ijk[EquationsHydroIdeal.INDEX_ZMOM];
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = uL * (pij + eL);
		} else if (uStar < -maxEVal){
			fluxRes[EquationsHydroIdeal.INDEX_RHO] = uR * rhoR;
			fluxRes[EquationsHydroIdeal.INDEX_XMOM] = uR * uR * rhoR + pip1j;
			fluxRes[EquationsHydroIdeal.INDEX_YMOM] = uR * ip1jk[EquationsHydroIdeal.INDEX_YMOM];
			fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = uR * ip1jk[EquationsHydroIdeal.INDEX_ZMOM];
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = uR * (pip1j + eR);
		} else { //*/
			fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0;
			fluxRes[EquationsHydroIdeal.INDEX_XMOM] = PiStar;
			fluxRes[EquationsHydroIdeal.INDEX_YMOM] = 0;
			fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = 0;
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = PiStar*uStar;
			
			
			
			double EPSILON = grid.minSpacing()*10;
			
			
			
			//double a = Math.max(EPSILON, Math.max(rhoL*(uL-uR)/2, rhoR*(uL-uR)/2));
			//double a = Math.max(rhoL, rhoR) * maxEVal;
			double a = relaxationSpeedRho;
			
			//uStar = (uL + uR)/2;
			
			//PiStar = (uL - uR)*a/2;
			//PiStar = 0;
			//PiStar = (diffU + diffV)*a/2;
			
			double divergence;
			if (useMultiD){
				divergence = diffU + diffV; 
			} else {
				divergence = uR - uL;
			}
			
			double rhoStar, eStar;
			double myUpw, mxUpw;
			if (uStar > 0){
				//rhoStar = rhoL/(1.0 + rhoL*divergence/2/a);
				rhoStar = rhoL/(1.0 + rhoL*divergence/2/a - rhoL*diffP/2/a/a);
				//rhoStar = ijk[EquationsHydroIdeal.INDEX_RHO];
				//rhoStar = rhoL;
	
				myUpw = ijk[EquationsHydroIdeal.INDEX_YMOM] / rhoL;
				//myUpw = ijk[EquationsHydroIdeal.INDEX_YMOM]/(1 + rhoL*(uR - uL)/2/a);
	
				
				//eStar = eL/(1.0 + rhoL*divergence/2/a);
				//eStar = ijk[EquationsHydroIdeal.INDEX_ENERGY];
				//eStar = eL; // + rhoL*(uR*uR - uL*uL)/4;
				eStar = rhoStar * (eL / rhoL - (PiStar * uStar - pij * uij)/ a);
				
				//mxUpw = ijk[EquationsHydroIdeal.INDEX_XMOM];
				
			} else {
				//rhoStar = 2.0*a*rhoR/(2.0*a + rhoR*divergence);
				rhoStar = rhoR/(1 + rhoR*divergence/2/a + rhoR*diffP/2/a/a);
				//rhoStar = ip1jk[EquationsHydroIdeal.INDEX_RHO];
				//rhoStar = rhoR;
	
				myUpw = ip1jk[EquationsHydroIdeal.INDEX_YMOM] / rhoR;
				//myUpw = ip1jk[EquationsHydroIdeal.INDEX_YMOM]/(1 + rhoR*(uR - uL)/2/a);
	
				//eStar = 1.0/(1.0/eR + divergence*rhoR/2/a/eR);
				//eStar = ip1jk[EquationsHydroIdeal.INDEX_ENERGY];
				//eStar = eR; // + rhoR*(uL*uL - uR*uR)/4;
				eStar = rhoStar * (eR / rhoR + (PiStar * uStar - pip1j * uip1j)/ a);
				
				//mxUpw = ip1jk[EquationsHydroIdeal.INDEX_XMOM];
				
			}
			
			fluxRes[EquationsHydroIdeal.INDEX_RHO ]   += rhoStar*uStar;
			fluxRes[EquationsHydroIdeal.INDEX_XMOM]   += rhoStar*uStar*uStar; // + PiStar;
			//fluxRes[EquationsHydroIdeal.INDEX_XMOM]   += uStar*mxUpw; // + PiStar;
			fluxRes[EquationsHydroIdeal.INDEX_YMOM]   += rhoStar * uStar * myUpw;
			fluxRes[EquationsHydroIdeal.INDEX_ENERGY] += (eStar)*uStar; // (eStar + PiStar)*uStar;
		}
		
		return new FluxAndWaveSpeed(fluxRes, maxEVal);
	}

	@Override
	public String getDetailedDescription() {
		return "simple operator split between acoustics and pressureless Euler";
	}

}
