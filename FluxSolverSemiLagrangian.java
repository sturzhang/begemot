
public abstract class FluxSolverSemiLagrangian extends FluxSolverOneStepMultiD {

	protected TimeIntegratorForSemiLagrangian integrator;
	
	public FluxSolverSemiLagrangian(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}


	public void setTimeIntegrator(TimeIntegratorForSemiLagrangian integrator){
		this.integrator = integrator;
	}
	
	

}
