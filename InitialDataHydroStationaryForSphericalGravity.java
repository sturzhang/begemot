
public class InitialDataHydroStationaryForSphericalGravity extends InitialDataHydro {

	private double prefactor;
	
	public InitialDataHydroStationaryForSphericalGravity(Grid grid,
			EquationsHydroIdeal equations, double prefactor) {
		super(grid, equations);
		this.prefactor = prefactor;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		double x, y, r, pressure;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		r = Math.sqrt(x*x + y*y);

		if (r > 0.4) { r=0.4; }
		
		res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;		
    	res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    				
    	res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
    				
    	//pressure = -r*r*r/3.0 + 0.4*r*r/2 + 1.0;
    	pressure = prefactor*(-r*r*r*r*r/5.0 + 0.8*r*r*r*r/4 - 0.16*r*r*r/3 + 0.0004) + 1.0;
    				
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
       						((EquationsHydroIdeal) equations).getEnergy(pressure, 0.0);

    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Stationary initial data for hydro with spherical gravity";
	}

}
