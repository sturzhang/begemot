
public abstract class SourceGravityHydro extends SourceInCellOnly {

	public SourceGravityHydro(GridCartesian grid, EquationsHydroIdeal equations, boolean averaged) {
		super(grid, equations, averaged);
	}

	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell gridcell, double time) {
		double[] res = new double[quantities.length];
		double[] g = gravityacceleration(gridcell.i(),gridcell.j(),gridcell.k()); 
		
		res[EquationsHydroIdeal.INDEX_RHO]    =   0.0;
		res[EquationsHydroIdeal.INDEX_XMOM]   = g[0] * quantities[EquationsHydroIdeal.INDEX_RHO];
		res[EquationsHydroIdeal.INDEX_YMOM]   = g[1] * quantities[EquationsHydroIdeal.INDEX_RHO];
		res[EquationsHydroIdeal.INDEX_ZMOM]   = g[2] * quantities[EquationsHydroIdeal.INDEX_RHO];
		
		double scalarproduct = g[0] * quantities[EquationsHydroIdeal.INDEX_XMOM] + 
				g[1] * quantities[EquationsHydroIdeal.INDEX_YMOM] + 
				g[2] * quantities[EquationsHydroIdeal.INDEX_ZMOM];
		res[EquationsHydroIdeal.INDEX_ENERGY] = scalarproduct;
		
		return res;
	}

	protected abstract double[] gravityacceleration(int i, int j, int k);
	
}
