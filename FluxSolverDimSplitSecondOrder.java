
public abstract class FluxSolverDimSplitSecondOrder extends FluxSolverDimSplit {

	public FluxSolverDimSplitSecondOrder(GridCartesian grid, Equations equations) {
		super(grid, equations, new StencilTurnerOneDSecondOrder(grid));
	}

	/*@Override
	public GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge) {
		int edgeDir = ((GridCartesian) grid).getDirectionPerpendicularToEdge(g, gridEdge.index());		
		GridCell nb = gridEdge.getOtherGridCell(g);
		int offsetX = 0, offsetY = 0, offsetZ = 0;
		if (edgeDir == GridCartesian.X_DIR){ offsetX = 1; }
		if (edgeDir == GridCartesian.Y_DIR){ offsetY = 1; }
		if (edgeDir == GridCartesian.Z_DIR){ offsetZ = 1; }
		
		return new GridCell[]{
				new GridCell(g.i()-offsetX, g.j()-offsetY, g.k()-offsetZ, grid),
				g, 
				nb, 
				new GridCell(nb.i()+offsetX, nb.j()+offsetY, nb.k()+offsetZ, grid) };
	}*/
	
	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[][] quantities, int timeIntegratorStep){
		return flux(gridEdge, quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_LEFTLEFT], 
				quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_LEFT], 
				quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_RIGHT], 
				quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_RIGHTRIGHT]);
	}

	@Override
	protected double[] fluxPassiveScalar(GridCell g, GridEdgeMapped gridEdge,
			GridCell left, GridCell right, int direction) {
		// TODO probably this functionality will be removed soon anyway
		return new double[equations.getNumberOfPassiveScalars()];
	}

	protected abstract FluxAndWaveSpeed flux(GridEdgeMapped gridEdge,      
			double[] leftleft, double[] left, double[] right, double[] rightright);

}
