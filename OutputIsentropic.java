
public class OutputIsentropic extends OutputAllQuantities {

	public OutputIsentropic(double outputTimeInterval, GridCartesian grid, EquationsIsentropicEuler equations) {
		super(outputTimeInterval, grid, equations);
	}

	public OutputIsentropic(double outputTimeInterval, double zproportion, GridCartesian3D grid, Equations equations) {
		super(outputTimeInterval, zproportion, grid, equations);
	}

	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		double vx, vy, vz, pressure, v2, rhoV2, soundspeed, mach;
		double angularmomentum, energy;
		
		vx = quantities[EquationsIsentropicEuler.INDEX_XMOM]/quantities[EquationsIsentropicEuler.INDEX_RHO];
		vy = quantities[EquationsIsentropicEuler.INDEX_YMOM]/quantities[EquationsIsentropicEuler.INDEX_RHO];
		vz = quantities[EquationsIsentropicEuler.INDEX_ZMOM]/quantities[EquationsIsentropicEuler.INDEX_RHO];
		v2 = vx*vx+vy*vy+vz*vz;
		pressure = ((EquationsIsentropicEuler) equations).getPressure(
				quantities[EquationsIsentropicEuler.INDEX_RHO]);

		soundspeed = ((EquationsIsentropicEuler) equations).getSoundSpeedFromPressure(
				pressure, quantities[EquationsIsentropicEuler.INDEX_RHO]);
		mach = Math.sqrt(v2) / soundspeed;
		
		// replaces the entropy
		energy = ((EquationsIsentropicEuler) equations).getEnergy(quantities[EquationsIsentropicEuler.INDEX_RHO], 
				new double[]{vx, vy, vz});
		
		angularmomentum = (grid.getX(i,j,k)-grid.xMidpoint()) * quantities[EquationsIsentropicEuler.INDEX_YMOM] - 
				(grid.getY(i,j,k)-grid.yMidpoint()) * quantities[EquationsIsentropicEuler.INDEX_XMOM];
		
		print(quantities[EquationsIsentropicEuler.INDEX_RHO] + " ");
		print(vx + " " + vy + " " + vz + " ");
		print(pressure + " ");
		print(soundspeed + " ");
		print(mach + " ");
		print(energy + " ");
		print(angularmomentum + " ");
	}

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		// nothing so far
	}

	@Override
	public String getDetailedDescription() {
    	return "prints: rho, vx, vy, vz, pressure, soundspeed, mach number, total energy, angular momentum, vorticity";
	}

}
