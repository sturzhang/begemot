
public class FluxSolverOneStepMultiDConsistentAcoustic extends FluxSolverOneStepMultiDCentralAndDiffusion {

	public FluxSolverOneStepMultiDConsistentAcoustic(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop,
			double[] left, double[] leftbottom, double[] righttop,
			double[] right, double[] rightbottom, int direction) {
		
		int quantities = left.length;
		
		// TODO don't we miss some factors of c here?
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, left, direction);
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, lefttop, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, leftbottom, direction);
		
		double[] fluxRight = equations.fluxFunction(i, j, k, right, direction);
		double[] fluxRightTop = equations.fluxFunction(i, j, k, righttop, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, rightbottom, direction);
		
		//int otherDirection = 0;
		int indexDirection = 0;
		int otherIndexDirection = 0;
		
		if (direction == GridCartesian.X_DIR){ 
			//otherDirection = GridCartesian.Y_DIR; 
			indexDirection = EquationsAcousticSimple.INDEX_VX; 
			otherIndexDirection = EquationsAcousticSimple.INDEX_VY; }
		else if (direction == GridCartesian.Y_DIR){ 
			//otherDirection = GridCartesian.X_DIR; 
			indexDirection = EquationsAcousticSimple.INDEX_VY;
			otherIndexDirection = EquationsAcousticSimple.INDEX_VX; }
		else {System.err.println("ERROR: Multi-d solver implemented only for two spatial dimensions!");};
		
		/*double[] fluxPerpLeftTop = equations.fluxFunction(i, j, k, lefttop, otherDirection);
		double[] fluxPerpLeftBottom = equations.fluxFunction(i, j, k, leftbottom, otherDirection);
		
		double[] fluxPerpRightTop = equations.fluxFunction(i, j, k, righttop, otherDirection);
		double[] fluxPerpRightBottom = equations.fluxFunction(i, j, k, rightbottom, otherDirection);*/
		
		double[] res = new double[quantities];
		
		// first term (Sx)
		int q = indexDirection;
		res[EquationsAcousticSimple.INDEX_P] = (fluxRightTop[q] - fluxLeftTop[q] + 2.0*(fluxRight[q] - fluxLeft[q]) + fluxRightBottom[q] - fluxLeftBottom[q]);
		q = EquationsAcousticSimple.INDEX_P;
		res[indexDirection] = (fluxRightTop[q] - fluxLeftTop[q] + 2.0*(fluxRight[q] - fluxLeft[q]) + fluxRightBottom[q] - fluxLeftBottom[q]);
		
		
		
		q = otherIndexDirection;
		res[EquationsAcousticSimple.INDEX_P] += (fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q]);
		
		res[otherIndexDirection] = (fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q]);
		
		// stability
		double prefactor = 0.125;
		res[EquationsAcousticSimple.INDEX_P] *= prefactor;  // d rho
		res[EquationsAcousticSimple.INDEX_VX] *= prefactor; // d rho u
		res[EquationsAcousticSimple.INDEX_VY] *= prefactor; // d rho v
		
		return res;
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left,
			double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoe usualSolver = new FluxSolverRoe((GridCartesian) grid, equations);
		return usualSolver.averageFlux(i, j, k, left, right, direction);	

	}

	@Override
	public String getDetailedDescription() {
		return "consistent diffusion for acoustic equations";
	}

}
