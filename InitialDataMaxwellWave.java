
public class InitialDataMaxwellWave extends InitialDataMaxwell {

	public InitialDataMaxwellWave(Grid grid, EquationsMaxwell equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double x, y, z, r;
		double[]res = new double[q];
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		z = g.getZ() - grid.zMidpoint();
		
		double lambda = (grid.xmax() - grid.xmin())/2.0;
		
		/*double phi = Math.PI/4; // direction of propagation
		double theta = Math.PI/2;
		double Bz = Math.sin(phi)*Math.sin(theta); // polarization
		double By = Math.cos(phi)*Math.sin(theta);
		double kabs = 2.0*Math.PI/lambda;
		double kx = kabs*Math.cos(phi)*Math.sin(theta);
		double ky = kabs*Math.sin(phi)*Math.sin(theta);
		double kz = kabs*Math.cos(theta); */
		
		
		double kx = 4.0*Math.PI;
		double ky = 2.0*Math.PI;
		double kz = 2.0*Math.PI;
		double kabs = Math.sqrt(kx*kx + ky*ky + kz*kz);
		double Bz = -ky/kabs;
		double By = kx/kabs;
		
		
		double cos = Math.cos(kx*x + ky*y + kz*z);
		double c = equations.lightspeed();
		
		res[EquationsMaxwell.INDEX_EX] = c*(-Bz*ky + By*kz)*cos/kabs;
		res[EquationsMaxwell.INDEX_EY] = c*(By*ky*kz + Bz*kx*kx + Bz*kz*kz)/kx/kabs*cos;
		res[EquationsMaxwell.INDEX_EZ] = -c*(By*kx*kx + By*ky*ky + Bz*ky*kz)/kx/kabs*cos;
		
		res[EquationsMaxwell.INDEX_BX] = -(By*ky + Bz*kz)/kx*cos;
		res[EquationsMaxwell.INDEX_BY] = By*cos;
		res[EquationsMaxwell.INDEX_BZ] = Bz*cos;
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
