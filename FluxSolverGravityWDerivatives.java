import matrices.SquareMatrix;


public class FluxSolverGravityWDerivatives extends FluxSolverRoe {

	FluxSolverRoe mysolver;
	
	public FluxSolverGravityWDerivatives(GridCartesian grid, Equations equations, FluxSolverRoe hydroSolver) {
		super(grid, equations);
		mysolver = hydroSolver;
	}

	@Override
	public UpwindingMatrixAndWaveSpeed getAveragedUpwindingMatrix(int i, int j, int k, double[] left, double[] right,
			int direction) {
		// TODO we should be able to allow for g to point in any direction
		
		if (direction == GridCartesian.Y_DIR){
			// TODO what is the wave speed here?
			return new UpwindingMatrixAndWaveSpeed(new SquareMatrix(left.length), 0.0);
		} else {
			return mysolver.getAveragedUpwindingMatrix(i, j, k, left, right, direction);
		}
	}

	@Override
	public double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction) {
		// TODO we should be able to allow for g to point in any direction
		double[] res = new double[left.length];
		double[] quantityflux = new double[5];
		double[] derivflux = new double[5];
		
		
		if (direction == GridCartesian.Y_DIR){
			// continue here derivflux = 0.5 * jacobian * (left + right) - (0, rho, rho v) g			
			quantityflux = new double[left.length];
		} else {
			// continue here derivflux = 0.0;
			quantityflux = mysolver.averageFlux(i, j, k, left, right, direction);
		}
		return null; // only to make compiler happy, continue here
	}

	@Override
	public String getDetailedDescription() {
		return "Flux solver for gravity well-balancing that, for fluxes not accounted for by the new source terms uses the following solver: " + mysolver.getDetailedDescription();
	}

	

}
