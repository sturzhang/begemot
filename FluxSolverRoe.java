import matrices.*;


public class FluxSolverRoe extends FluxSolverDimSplitEdgeNeighboursOnly {
	
	protected boolean centerOfMassFluxEvaluation = true;
	
	public FluxSolverRoe(Grid grid, Equations equations) {
		super(grid, equations);
	}

	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		return getUpwindingMatrix(i, j, k, quantities, direction, 0.0, 0.0, left, right);
	}
	
	protected SquareMatrix[] getDiagonalization(int i, int j, int k, double[] quantities, int direction){
		return equations.diagonalization(i, j, k, quantities, direction);
	}
	
	protected SquareMatrix getAbsoluteValueOfEigenvalues(SquareMatrix eigenvalues,
			double[] left, double[] right, int i, int j, int k, int direction){
		return eigenvalues.absElementwise();
	}
	
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, 
			double maxEigenvalueSuggestion, double minEigenvalueSuggestion, 
			double[] left, double[] right){
		
		SquareMatrix[] mat = getDiagonalization(i, j, k, quantities, direction);
		double waveSpeed;
		int minEvalIndex, maxEvalIndex;
		
		minEvalIndex = 0;
		maxEvalIndex = minEvalIndex;
		for (int ii = 1; ii < mat[1].rows(); ii++){
			if (mat[1].value(ii, ii) < mat[1].value(minEvalIndex, minEvalIndex)){ minEvalIndex = ii; }
			if (mat[1].value(ii, ii) > mat[1].value(maxEvalIndex, maxEvalIndex)){ maxEvalIndex = ii; }
		}
		
		if (maxEigenvalueSuggestion != minEigenvalueSuggestion){
			mat[1].setElement(minEvalIndex, minEvalIndex, Math.min(mat[1].value(minEvalIndex, minEvalIndex), 
					minEigenvalueSuggestion));
			mat[1].setElement(maxEvalIndex, maxEvalIndex, Math.max(mat[1].value(maxEvalIndex, maxEvalIndex), 
					maxEigenvalueSuggestion));
		}	
		
	
		
		
		mat[1] = getAbsoluteValueOfEigenvalues(mat[1], left, right, i,j,k, direction);
		//mat[1].absMe();
		
		
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(mat[1].value(ii, ii), waveSpeed);
		}
		
		return new UpwindingMatrixAndWaveSpeed(
				mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix(), waveSpeed);
	}
		
	
	public UpwindingMatrixAndWaveSpeed getAveragedUpwindingMatrix(int i, int j, int k, double[] left, double[] right, int direction){
		return getUpwindingMatrix(i, j, k, mean(left, right), direction, left, right);
	}
	
	public double[] mean(double[] left, double[] right){
		double[] res = new double[left.length];
		for (int i = 0; i < res.length; i++){
			res[i] = 0.5 * (left[i] + right[i]);
		}
		return res;
	}
	
	
	public double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction){
		double[] leftF = equations.fluxFunction(i, j, k, left, direction);
		double[] rightF = equations.fluxFunction(i, j, k, right, direction);
		
		double[] res = new double[left.length];
    	for (int q = 0; q < equations.getNumberOfConservedQuantities(); q++){
    		res[q] = 0.5 * (leftF[q] + rightF[q]);
    	}
    	return res;
	}
	
	protected double[] getStateDifference(GridEdgeMapped gridEdge, double[] right, double[] left){
		double[] diff = new double[left.length];
		for (int q = 0; q < left.length; q++){ diff[q] = right[q] - left[q]; }
		return diff;
	}
	
	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		int quantities = equations.getNumberOfConservedQuantities();
		double diff[];
		double[] upwindingTerm = new double[quantities];
		double[] fluxRes;
		boolean detailedOutput = false;
		SquareMatrix upwindingMatrix = new SquareMatrix(quantities);
		UpwindingMatrixAndWaveSpeed tmp;
		GridCell g = gridEdge.adjacent1();
				
		/*double[] centerOfMassSpeed = new double[3];
		if ((centerOfMassFluxEvaluation) && (equations instanceof EquationsHydroIdeal)){
			centerOfMassSpeed[0] = (left[EquationsHydroIdeal.INDEX_XMOM] + right[EquationsHydroIdeal.INDEX_XMOM])/
					(left[EquationsHydroIdeal.INDEX_RHO] + right[EquationsHydroIdeal.INDEX_RHO]);
			centerOfMassSpeed[1] = (left[EquationsHydroIdeal.INDEX_YMOM] + right[EquationsHydroIdeal.INDEX_YMOM])/
					(left[EquationsHydroIdeal.INDEX_RHO] + right[EquationsHydroIdeal.INDEX_RHO]);
			centerOfMassSpeed[2] = (left[EquationsHydroIdeal.INDEX_ZMOM] + right[EquationsHydroIdeal.INDEX_ZMOM])/
					(left[EquationsHydroIdeal.INDEX_RHO] + right[EquationsHydroIdeal.INDEX_RHO]);
			
			left[EquationsHydroIdeal.INDEX_XMOM] -= left[EquationsHydroIdeal.INDEX_RHO]*centerOfMassSpeed[0];
			left[EquationsHydroIdeal.INDEX_YMOM] -= left[EquationsHydroIdeal.INDEX_RHO]*centerOfMassSpeed[1];
			left[EquationsHydroIdeal.INDEX_ZMOM] -= left[EquationsHydroIdeal.INDEX_RHO]*centerOfMassSpeed[2];
			
			right[EquationsHydroIdeal.INDEX_XMOM] -= right[EquationsHydroIdeal.INDEX_RHO]*centerOfMassSpeed[0];
			right[EquationsHydroIdeal.INDEX_YMOM] -= right[EquationsHydroIdeal.INDEX_RHO]*centerOfMassSpeed[1];
			right[EquationsHydroIdeal.INDEX_ZMOM] -= right[EquationsHydroIdeal.INDEX_RHO]*centerOfMassSpeed[2];
		}*/
		
		
		
		diff = getStateDifference(gridEdge, right, left);
		
		tmp = getAveragedUpwindingMatrix(g.i(), g.j(), g.k(), left, right, GridCartesian.X_DIR);
		upwindingMatrix = tmp.matrix();
		
		
		
		/*if ((gridEdge.adjacent1().j() == 51) || (gridEdge.adjacent2().j() == 51)){
			System.err.println("----------------------------------------");
			System.err.println(gridEdge.adjacent1().j() + " " + gridEdge.adjacent2().j());
			System.err.println(upwindingMatrix);
			System.err.println("----------------------------------------");
			//System.err.println("diff = " + diff[0] + ", " + diff[1] + ", " + diff[2] + ", " + diff[3] + ", " + diff[4]);
		}//*/
		
		
		upwindingTerm = upwindingMatrix.mult(0.5).mult(diff);
	    		
		double[] central = averageFlux(g.i(), g.j(), g.k(), left, right, GridCartesian.X_DIR);

		/*if ((gridEdge.adjacent1().j() == 51) || (gridEdge.adjacent2().j() == 51)){
			System.err.println("-----central:-----------------------");
			System.err.println(gridEdge.adjacent1().j() + " " + gridEdge.adjacent2().j());
			System.err.println(central[0] + " " + central[1] + " " + central[2] + " " + central[3] + " " + central[4]);
			System.err.println("left  = " + left[0] + " " + left[1] + " " + left[2] + " " + left[3] + " " + left[4]);
			System.err.println("right = " + right[0] + " " + right[1] + " " + right[2] + " " + right[3] + " " + right[4]);
			System.err.println("diff  = " + diff[0] + " " + diff[1] + " " + diff[2] + " " + diff[3] + " " + diff[4]);
			System.err.println("upw   = " + upwindingTerm[0] + " " + upwindingTerm[1] + " " + upwindingTerm[2] + " " + 
					upwindingTerm[3] + " " + upwindingTerm[4]);
			
		}//*/
		
    	fluxRes = new double[quantities];
    	
    	
    	for (int q = 0; q < quantities; q++){
    		fluxRes[q] = central[q] - upwindingTerm[q];
    		if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("fluxes are NaN");
    	    	detailedOutput = true;
    	    }
    	}
		
    	
    	/*if (g.i()==225){
    		System.err.println(central[0] + " " + central[1] + " " + central[2] + " " + central[3] + " " + central[4]);
    		System.err.println(upwindingTerm[0] + " " + upwindingTerm[1] + " " + upwindingTerm[2] + " " + upwindingTerm[3] + " " + upwindingTerm[4]);
    		System.err.println(diff[0] + " " + diff[1] + " " + diff[2] + " " + diff[3] + " " + diff[4]);
    		System.err.println(left[0] + " " + left[1] + " " + left[2] + " " + left[3] + " " + left[4]);
    		System.err.println(right[0] + " " + right[1] + " " + right[2] + " " + right[3] + " " + right[4]);
    	}//*/
    	
    	
    	
    	/*// dirty hack to block x-fluxes!
    	if (gridEdge.normalVector()[0] != 0){
    		for (int q = 0; q < quantities; q++){
        		fluxRes[q] = 0.0;
    		}
    	}//*/
    		
    	
    	
    	/*if ((gridEdge.adjacent1().j() == 51) || (gridEdge.adjacent2().j() == 51)){
			System.err.println("-----flux:-----------------------");
			System.err.println("flux  = " + fluxRes[0] + " " + fluxRes[1] + " " + fluxRes[2] + " " + fluxRes[3] + " " + fluxRes[4]);
    	}*/
    	
    	
    	//fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0.0;
    	
    	/*
    	
    	// simplest system, remove scalaproduct in constant gravity source
    	
    	
    	//fluxRes[EquationsHydroIdeal.INDEX_RHO] = central[EquationsHydroIdeal.INDEX_RHO];
    	fluxRes[EquationsHydroIdeal.INDEX_XMOM] = - upwindingTerm[EquationsHydroIdeal.INDEX_XMOM];
    	fluxRes[EquationsHydroIdeal.INDEX_YMOM] = - upwindingTerm[EquationsHydroIdeal.INDEX_YMOM];
    	
    	fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = - upwindingTerm[EquationsHydroIdeal.INDEX_ENERGY];
    	
    	if (direction == Grid.X_DIR){
    		fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0.0;
    		fluxRes[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
        }
    	if (direction == Grid.Y_DIR){
    		fluxRes[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
    	}  //*/
    		
    	if (detailedOutput){
        	for (int q = 0; q < quantities; q++){
        	    System.err.println("left: " + left[q] + " right: " + right[q]
        	    					+ " res: " + fluxRes[q] + " central: " + central[q] 
        	    					+ " upwinding: " + upwindingTerm[q]);
        	    
        	}
    	}
    	
    	/*if ((centerOfMassFluxEvaluation) && (equations instanceof EquationsHydroIdeal)){
    		fluxRes
    		
    		return new FluxAndWaveSpeed(fluxRes, tmp.waveSpeed() + Math.max(Math.abs(centerOfMassSpeed[0]), Math.max(
    				Math.abs(centerOfMassSpeed[1]), Math.abs(centerOfMassSpeed[2]))));
    	} else {*/
    	
    	
    	//System.err.println("fv = " + fluxRes[0] + "\t fp = " + fluxRes[3] + "\t frho = " + fluxRes[4]);
    	
    	/*if ((gridEdge.adjacent1().j() == 1) || (gridEdge.adjacent2().j() == 1)){
    		System.err.println(gridEdge.adjacent1().j() + ", " + gridEdge.adjacent2().j() + " | " + 
    				central[0] + " " + central[1] + " " + central[4] + " | " + 
    				fluxRes[0] + " " + fluxRes[1] + " " + fluxRes[4]);
    	}//*/
    	
    	
    		return new FluxAndWaveSpeed(fluxRes, tmp.waveSpeed());
    	//}
	}

	@Override
	public String getDetailedDescription() {
		return "Roe solver";
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right, int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double vLeft = equations.advectionVelocityForPassiveScalar(i,j,k,left, direction);
		double vRight = equations.advectionVelocityForPassiveScalar(i,j,k,right, direction);
		double absMeanV = 0.5 * Math.abs(vLeft + vRight);
		
		
		for (int ii = 0; ii < res.length; ii++){
			s = sc[ii];
			// TODO there was momentum mLeft and mRight standing in the flux average before
			res[ii] = 0.5*(left[s]*vLeft + right[s]*vRight) - 0.5 * absMeanV * (right[s] - left[s]);
		}
		return res;
	}


	
}
