
public class TimeIntegratorJameson extends TimeIntegratorExplicit {

	
	 // Jameson 1991
	
	
	protected double[][][][][] savedQuantities;
	protected double[][][][][] D;
	
	protected TimeIntegratorExplicitConservative myIntegrator;

	
	public TimeIntegratorJameson(Grid grid, double CFL) {
		super(grid, CFL);
		savedQuantities = new double[steps()+1][][][][];
		D = new double[steps()/2][][][][];
		myIntegrator = new TimeIntegratorExplicitConservative(grid, CFL);

	}

	@Override
	public int steps() {
		return 4*2;
	}

	protected double alpha(int k){
		if (k == 1){      return 1.0/3; }
		else if (k == 2){ return 4.0/15; }
		else if (k == 3){ return 5.0/9; }
		else if (k == 4){ return 1.0; }
		else { 
			System.err.println("ERROR: Asked for coef alpha " + k + " in Jameson time integrator");
			return 0.0; }
	}
	
	protected double beta(int k){
		// TODO or maybe 1, 1/2, 0, 0, 0 ??? because beta4 is never used
		
		if (k == 0){                     return 1.0; }
		else if (k == 1){                return 1.0; }
		else if (k == 2){                return 0.5; }
		else if ((k == 3) || (k == 4)) { return 0.0; }
		else { 
			System.err.println("ERROR: Asked for coef beta " + k + " in Jameson time integrator");
			return 0.0; }
	}
	
	
	
	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {
		
		int i, j, k;
		
		/*
		 * savedQuantities[0] stores initial data w^n = w^{n,0}
		 * savedQuantities[2k-1], k=1,2,... stores an intermediate step with evaluation of Q only: w^n - alphak dt Q^{k-1} 
		 * savedQuantities[2k] stores w^{n+1,k} = savedQuantities[2k-1] - alphak dt D^{k-1}
		 * D[k], k = 0,1,2... stores D^k  
		 */
		
		if (step == 1){
			savedQuantities[0] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];

			for (GridCell g : grid){ i = g.i(); j = g.j(); k = g.k();
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					savedQuantities[0][i][j][k][q] = conservedQuantities[i][j][k][q];		
				}
			}
		}
		
		double alpha = (step % 2 == 1 ? alpha((step + 1)/2) : alpha(step/2));
		
		
		if (step % 2 == 1){
			for (GridCell g : grid){ i = g.i(); j = g.j(); k = g.k();
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					conservedQuantities[i][j][k][q] = savedQuantities[0][i][j][k][q];		
				}
			}

			double[][][][] oneStepQuantities = myIntegrator.perform(step, alpha*dt, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
			
			savedQuantities[step] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];
			for (GridCell g : grid){ i = g.i(); j = g.j(); k = g.k();
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					savedQuantities[step][i][j][k][q] = oneStepQuantities[i][j][k][q];
				}
			}

			return savedQuantities[step-1];
		} else {
			for (GridCell g : grid){ i = g.i(); j = g.j(); k = g.k();
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					conservedQuantities[i][j][k][q] = 0; // this is needed for computation of D		
				}
			}
			
			int kk = step / 2;
			
			double[][][][] oneStepQuantities = myIntegrator.perform(step, 1.0, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
			double beta = beta(kk-1);
			
			D[kk-1] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];
			for (GridCell g : grid){ i = g.i(); j = g.j(); k = g.k();
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					D[kk-1][i][j][k][q] = beta*oneStepQuantities[i][j][k][q];
					if (kk > 1){ D[kk-1][i][j][k][q] += (1.0 - beta)*D[kk-2][i][j][k][q]; }
				}
			}

			savedQuantities[step] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];
			for (GridCell g : grid){ i = g.i(); j = g.j(); k = g.k();
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					savedQuantities[step][i][j][k][q] = savedQuantities[step-1][i][j][k][q] - alpha * dt * D[kk-1][i][j][k][q]; 
				}
			}

			return savedQuantities[step];
		}
		

		

		
		

	}

}
