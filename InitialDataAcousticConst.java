
public class InitialDataAcousticConst extends InitialDataAcoustic {

	private double pressure;
	private double[] velocity;

	
	public InitialDataAcousticConst(Grid grid, EquationsAcousticSimple equations,
			double pressure, double[] velocity) {
		super(grid, equations);
		this.pressure = pressure;
		this.velocity = velocity;
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		
		res[EquationsAcousticSimple.INDEX_P] = pressure;
		res[EquationsAcousticSimple.INDEX_VX] = velocity[0];
		res[EquationsAcousticSimple.INDEX_VY] = velocity[1];
		res[EquationsAcousticSimple.INDEX_VZ] = velocity[2];
		
		for (int s = 5; s < q; s++){
			// passive scalars
			//if (grid.getX(i) > grid.xMidpoint()){
				res[s] = Math.pow(-1.0, Math.max(0, Math.round((g.getY() - ((GridCartesian) grid).ymin())/(((GridCartesian) grid).ymax() - ((GridCartesian) grid).ymin())*10)));
			//}
		}
		
		return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "Constnat state of pressure = " + pressure + 
	    			" and velocity v = (" + velocity[0] + ", "
	    			+ velocity[1] + ", " + velocity[2] + ")";
	}

}
