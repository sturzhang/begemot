
public class SourceAcousticExtendedConstantGravity extends SourceAcousticConstantGravity {

	public SourceAcousticExtendedConstantGravity(GridCartesian grid, EquationsAcousticExtended equations, double[] g, boolean averaged) {
		super(grid, equations, g, averaged);
	}

	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell gg, double time) {
		double[] res = new double[quantities.length];
		double[] g = gravityacceleration(gg.i(),gg.j(),gg.k());
		
		res[EquationsAcousticExtended.INDEX_RHO] = 0.0;
		
		res[EquationsAcoustic.INDEX_VX]   = g[0]*quantities[EquationsAcousticExtended.INDEX_RHO];
		res[EquationsAcoustic.INDEX_VY]   = g[1]*quantities[EquationsAcousticExtended.INDEX_RHO];
		res[EquationsAcoustic.INDEX_VZ]   = g[2]*quantities[EquationsAcousticExtended.INDEX_RHO];
		
		res[EquationsAcoustic.INDEX_P] = 0.0;
		
		return res;
	}
}
