import matrices.SquareMatrix;


public abstract class EquationsSpatiallyDependentLinear extends Equations {

	public EquationsSpatiallyDependentLinear(Grid grid,
			int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
		// TODO Auto-generated constructor stub
	}

	public abstract SquareMatrix getJacobian(int direction, GridCell g);
	
	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		return getJacobian(direction, new GridCell(i,j,k, grid)).mult(quantities);
	}
	
	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		// TODO Auto-generated method stub
		return 0;
	}


}
