
public class FluxSolverRoeHydroGravity extends FluxSolverRoe {

	protected double gy;
	
	public FluxSolverRoeHydroGravity(GridCartesian grid, EquationsHydroIdeal equations, double gy) {
		super(grid, equations);
		this.gy = gy;
	}

	@Override
	protected double[] getStateDifference(GridEdgeMapped gridEdge, double[] right, double[] left){
		double[] diff = new double[left.length];
		for (int q = 0; q < left.length; q++){ diff[q] = right[q] - left[q]; }
		
		double dx= ((GridCartesian) grid).ySpacing();
		
		if (gridEdge.normalVector()[0] == 0.0){
			diff[EquationsHydroIdeal.INDEX_ENERGY] = 
				right[EquationsHydroIdeal.INDEX_ENERGY] - left[EquationsHydroIdeal.INDEX_ENERGY] 
			  - 0.5*dx * gy*gridEdge.normalVector()[1] * (right[EquationsHydroIdeal.INDEX_RHO] + left[EquationsHydroIdeal.INDEX_RHO]) / 
					  (((EquationsHydroIdeal) equations).gamma() - 1);
		
			//diff[EquationsHydroIdeal.INDEX_ENERGY] = 0.0;
			
			
			/* if ((gridEdge.adjacent1().j() == 0) || (gridEdge.adjacent2().j() == 0)){
				System.err.println("->" + 
						(right[EquationsHydroIdeal.INDEX_ENERGY]*0.4) + " " + (left[EquationsHydroIdeal.INDEX_ENERGY]*0.4) + 
						" dp = " + (
								(right[EquationsHydroIdeal.INDEX_ENERGY]*0.4 - left[EquationsHydroIdeal.INDEX_ENERGY]*0.4)/
								dx) + 
						" g = " + gy + " " + 
						((right[EquationsHydroIdeal.INDEX_RHO] + left[EquationsHydroIdeal.INDEX_RHO])*0.5)
						+ " res = "
						+ diff[EquationsHydroIdeal.INDEX_ENERGY]
						);
			}	//*/	
		}
		
		
		return diff;
	}
	
}
