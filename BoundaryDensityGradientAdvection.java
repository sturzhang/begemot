
public class BoundaryDensityGradientAdvection extends Boundary {

	protected EquationsHydroIdeal equations;
	protected double rhograd;
	protected InitialData initial;
	
	public BoundaryDensityGradientAdvection(GridCartesian grid, EquationsHydroIdeal equations, InitialData initial, double rhograd) {
		super(grid);
		this.rhograd = rhograd;
		this.initial = initial;
		this.equations = equations;
	}
	
	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k;
		HydroState init;
		double[] initData;
		double vel;
		
		i = g.i(); j = g.j(); k = g.k();
		
		initData = initial.getInitialValue(new GridCell(i, j, k, grid), conservedQuantities[i][j][k].length);
		init = new HydroState(initData, EquationsHydroIdeal.getIndexDirection(GridCartesian.X_DIR), equations);
	
		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = initData[q];
		}
		
								
		vel = conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] /
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		if (i < ((GridCartesian) grid).indexMinX()){ 
//							conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] -= vel*rhograd*Iteration.time();
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 401.0;
		}
		if (i > ((GridCartesian) grid).indexMaxX()){ 
//							conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] -= vel*rhograd*Iteration.time();
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 401.0;
		}
		
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = vel*conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		
		conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = equations.getEnergy(init.pressure, init.v2*
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]);
	}
/*				}
			}
		}
	}*/
	
	@Override
	public String getDetailedDescription() {
		return "Sets values to initial and rho = rhoinitial + rhograd*v*t with rhograd = " + rhograd; 
	}

}
