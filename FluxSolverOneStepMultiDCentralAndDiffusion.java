import matrices.SquareMatrix;


public abstract class FluxSolverOneStepMultiDCentralAndDiffusion extends
		FluxSolverOneStepMultiD {

	public FluxSolverOneStepMultiDCentralAndDiffusion(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	abstract protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop, double[] left,
			double[] leftbottom, double[] righttop, double[] right, double[] rightbottom, int direction);
	
	protected double[] averageFlux(int i, int j, int k, double[] lefttop, double[] left, double[] leftbottom, 
			double[] righttop, double[] right, double[] rightbottom, int direction){
		int quantities = left.length;
		
		double multidaverageleft[] = new double[quantities];
		double multidaverageright[] = new double[quantities];
		
		for (int q = 0; q < quantities; q++){ 
			multidaverageleft[q] = 0.5*left[q]   + 0.25*(lefttop[q]  + leftbottom[q]); 
			multidaverageright[q] = 0.5*right[q] + 0.25*(righttop[q] + rightbottom[q]);
		}
		return averageFlux(i,j,k, multidaverageleft, multidaverageright, direction);
	}
	
	abstract protected double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction);
	
	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1, int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		

		int quantities = ijk.length;
		
		double multidaverageleft[] = new double[quantities];
		double multidaverageright[] = new double[quantities];
		double[] average = new double[quantities];
		
		// TODO check these:
		GridCell g = gridEdge.adjacent1();
		int direction = GridCartesian.X_DIR;
		
		double[] upwindingTerm = getUpwindingTerm(g.i(), g.j(), g.k(), ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, direction);
		double[] fluxRes;
		
		for (int q = 0; q < quantities; q++){ 
			multidaverageleft[q] = 0.5*ijk[q]   + 0.25*(ijp1k[q]  + ijm1k[q]); 
			multidaverageright[q] = 0.5*ip1jk[q] + 0.25*(ip1jp1k[q] + ip1jm1k[q]);
			average[q] = 0.5*(multidaverageleft[q] + multidaverageright[q]);
		}
		
		SquareMatrix eigenvalues = equations.diagonalization(g.i(), g.j(), g.k(), average, direction)[1];
		double maxEVal = Math.abs(eigenvalues.value(0, 0));
		for (int ii = 1; ii < eigenvalues.rows(); ii++){
			maxEVal = Math.max(maxEVal, Math.abs(eigenvalues.value(ii, ii)));
		}
		
		double[] central = averageFlux(g.i(), g.j(), g.k(), ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, direction);
		
    	fluxRes = new double[quantities];
    	for (int q = 0; q < quantities; q++){
    	    fluxRes[q] = central[q] - upwindingTerm[q];
    	    if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("fluxes are NaN");
    	    }
    	}
    	
    	//fluxRes[0] = 0.0;
    	
    	/*boolean verbose = (((g.i() >= ((GridCartesian) grid).indexMaxX()/2-1) && (g.i() <= ((GridCartesian) grid).indexMaxX()/2+1)) 
				&& ( (g.j() >= ((GridCartesian) grid).indexMinY() - 1) && (g.j() <= ((GridCartesian) grid).indexMinY()+1)));*/
    	/*boolean verbose = ((g.i() == 25) 
				&& ( (g.j() == 1) || (g.j() == 2) ));
		if (verbose){
			for (int q = 0; q < quantities; q++){
				System.err.println(q + " " + lefttop[q] + " " + left[q] + " " + leftbottom[q] + " " + 
						righttop[q] + " " + right[q] + " " + rightbottom[q]);
			}
			
			
			System.err.println("flux: " + g.i() + " " + g.j() + " " + fluxRes[1]*gridEdge.size() + " = " + central[1]*gridEdge.size()
					+ " - " + upwindingTerm[1]*gridEdge.size() + 
					" (" + gridEdge.normalVector()[0] + " " + gridEdge.normalVector()[1] + ")");
		}*/
    	
		
    	return new FluxAndWaveSpeed(fluxRes, maxEVal);

	}

	
	

}
