
public class BoundaryFixedToInitial extends Boundary {

	protected InitialData initial;
	
	public BoundaryFixedToInitial(Grid grid, InitialData initial) {
		super(grid);
		this.initial = initial;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k;
		i = g.i(); j = g.j(); k = g.k();
		
		conservedQuantities[i][j][k] = 
				initial.getInitialValue(new GridCell(i, j, k, grid), conservedQuantities[i][j][k].length);
	}

	@Override
	public String getDetailedDescription() {
		return "Boundary values fixed to the values there initially.";
	}

}
