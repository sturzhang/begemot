
public class FluxSolverAcousticCentralSplitUp extends FluxSolverDimSplitEdgeNeighboursOnly {

	protected boolean leftFirst;
	
	public FluxSolverAcousticCentralSplitUp(Grid grid, EquationsAcousticSimple equations, boolean leftFirst) {
		super(grid, equations);
		this.leftFirst = leftFirst;
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		double[] fluxRes = new double[equations.getNumberOfConservedQuantities()];
		GridCell g = gridEdge.adjacent1();
		boolean doLeft;
		double[] flux;
		
		if (timeIntegratorStep == 1){ 
			if (leftFirst){ doLeft = true; } else { doLeft = false; } 
		} else if (timeIntegratorStep == 2){ 
			if (leftFirst){ doLeft = false; } else { doLeft = true; } 
		} else {
			System.err.println("ERROR: The split version of the central flux for the acoustic equations uses only two time integrator steps.");
			return null;
		}
		
		if (doLeft){
			flux = equations.fluxFunction(g.i(), g.j(), g.k(), left, GridCartesian.X_DIR);
		} else {
			flux = equations.fluxFunction(g.i(), g.j(), g.k(), right, GridCartesian.X_DIR);
		}
			
		for (int q = 0; q < fluxRes.length; q++){
    		fluxRes[q] = flux[q]; // to be absolutely sure that we pass value and not pointer (even if this might not matter)
    	}
    	
		return new FluxAndWaveSpeed(fluxRes, ((EquationsAcousticSimple) equations).soundspeed());
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left,
			double[] right, int direction) {
		return null;
	}

	@Override
	public String getDetailedDescription() {
		return "A collocated interpretation of the Yee scheme: the first integrator step uses a one-sided difference, and the second uses the other direction; to be used with a triangular implicit time integrator";
	}

}
