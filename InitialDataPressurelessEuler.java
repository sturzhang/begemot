
public abstract class InitialDataPressurelessEuler extends InitialData {

	protected EquationsPressurelessEuler equations;
	
	public InitialDataPressurelessEuler(Grid grid, EquationsPressurelessEuler equations) {
		super(grid);
		this.equations = equations;
	}

	
}
