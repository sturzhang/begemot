
public class InitialDataAdvectionSineWave extends InitialDataScalarAdvection {

	protected double amplitude, wavelength, width;
	
	public InitialDataAdvectionSineWave(Grid grid, EquationsAdvection equations, double amplitude, double wavelength, double width) {
		super(grid, equations);
		this.amplitude = amplitude;
		this.wavelength = wavelength;
		this.width = width;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		double x = g.getX() - grid.xMidpoint()/2;
		//double y = g.getY() - grid.yMidpoint();
		
		res[0] = amplitude * Math.sin(x/wavelength*2.0*Math.PI) *  Math.exp(-x*x/width/width);
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Initial data for scalar advection with a gaussian modulated sine wave";
	}

}
