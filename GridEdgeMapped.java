
public class GridEdgeMapped extends GridEdge {

	// Mapped because it knows its counterpart
	protected GridEdgeMapped counterpart = null;
	protected int index;
	
	public GridEdgeMapped(Grid grid, GridCell adjacent1, GridCell adjacent2) {
		super(grid, adjacent1, adjacent2);
	}

	public void setCounterPart(GridEdgeMapped counterpart){ this.counterpart = counterpart; }
	
	// TODO der index sollte sich auf adjacent 1 beziehen -- einzig sinnvolle loesung: beide indices abspeichern
	public void setIndex(int index){ this.index = index; }
	
	public GridEdgeMapped counterpart(){ return counterpart; }
	public int index(){ return index; }
}
