
public class InitialDataIsentropicDelaunay extends InitialDataIsentropic {

	public InitialDataIsentropicDelaunay(Grid grid,
			EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		EquationsHydroIdeal eqn = new EquationsHydroIdeal(grid, 0, 1.4);
		InitialDataDelaunay initial = new InitialDataDelaunay(grid, eqn);
		double rho = eqn.getPressureFromConservative(initial.getInitialValue(g, eqn.getNumberOfConservedQuantities()));
		
		double[] res = new double[numberOfConservedQuantities];
		res[EquationsIsentropicEuler.INDEX_RHO] = rho;
		res[EquationsIsentropicEuler.INDEX_XMOM] = 0;
		res[EquationsIsentropicEuler.INDEX_YMOM] = 0;
		res[EquationsIsentropicEuler.INDEX_ZMOM] = 0;
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Delaunay-like initial data in the density, no velocity";
	}

}
