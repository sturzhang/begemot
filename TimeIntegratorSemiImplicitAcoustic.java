import java.util.LinkedList;


public class TimeIntegratorSemiImplicitAcoustic extends TimeIntegratorExplicit {

	private EquationsAcousticSimple equations;
	
	public TimeIntegratorSemiImplicitAcoustic(GridCartesian grid, double CFL, EquationsAcousticSimple equations) {
		super(grid, CFL);
		this.equations = equations;
	}

	@Override
    public String getDetailedDescription(){
    	return "Semi-implicit time integrator for linear acoustics with CFL = " + CFL;
    }

	
	@Override
	public int steps() {
		return 2;
	}

	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities, 
			boolean[][][] excludeFromTimeIntegration) {

		int[] quantities;
		int i, j, k;
		LinkedList<GridEdge> edges;
		double cellSize;
		//GridCellCartesian g;
		//GridEdgeCartesian e;
		
		if (step == 2){
			quantities = new int[] {EquationsAcousticSimple.INDEX_VX, EquationsAcousticSimple.INDEX_VY, EquationsAcousticSimple.INDEX_VZ};
		} else {
			quantities = new int[] {EquationsAcousticSimple.INDEX_P};
		}
		
		for (GridCell g : grid){
			//g = (GridCellCartesian) gg;
			i = g.i(); j = g.j(); k = g.k();
			//edges = grid.getEdges(g);
			cellSize = g.size();

			if (!excludeFromTimeIntegration[i][j][k]){
				for (int dir = 0; dir < grid.summedInterfaceFluxes[i][j][k].length; dir++){
					for (int q = 0; q < quantities.length; q++){
						/*g.addToConservedQuantity(- dt / cellSize * grid.summedInterfaceFluxes[i][j][k][dir][quantities[q]]
								+ dt * sources[i][j][k][quantities[q]] , quantities[q]); //*/
						
						conservedQuantities[i][j][k][quantities[q]] += -dt / cellSize * grid.summedInterfaceFluxes[i][j][k][dir][quantities[q]]
								+ dt * sources[i][j][k][quantities[q]];
						
					}
				}
				
			}
		}
		
		return conservedQuantities;
	}

}
