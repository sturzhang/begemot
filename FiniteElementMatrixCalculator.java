
public class FiniteElementMatrixCalculator {

    protected int Npoints;
    protected double integraldx;
    protected double integraldy;
    protected int PRODUCT = 1;
    protected int DX      = 2;
    protected int DY      = 3;
    protected int DXDX    = 4;
    protected int DXDY    = 5;
    protected int DYDY    = 6;

    protected GridCartesian grid;
    
    protected double integralxmin, integralxmax, integralymin, integralymax;

    
	public FiniteElementMatrixCalculator(GridCartesian grid, int Npoints) {
		this.grid = grid;
		this.Npoints = Npoints;

	    integralxmin = -4.0*grid.xSpacing();
	    integralxmax =  4.0*grid.xSpacing();
	    
	    integralymin = -4.0*grid.ySpacing();
	    integralymax =  4.0*grid.ySpacing();

		integraldx = (integralxmax - integralxmin)/(Npoints - 1);
	    integraldy = (integralymax - integralymin)/(Npoints - 1);
	}

	public double getSimpleDxDxMatrixElement(GridCell g1, GridCell g2){ 
		double offsetX = periodicBoundary(g2.getPosition()[0] - g1.getPosition()[0], grid.sizex());
		double offsetY = periodicBoundary(g2.getPosition()[1] - g1.getPosition()[1], grid.sizey());
		if (Math.abs(offsetY) > 1e-10){
			return 0.0;
		} else {
			if      (Math.abs(offsetX - grid.xSpacing()) < 1e-10){ return -1.0/grid.xSpacing()*grid.ySpacing(); }
			else if (Math.abs(offsetX + grid.xSpacing()) < 1e-10){ return -1.0/grid.xSpacing()*grid.ySpacing(); }
			else if (Math.abs(offsetX                  ) < 1e-10){ return  2.0/grid.xSpacing()*grid.ySpacing(); }
			else { return 0.0; }
		}
	}
	
	public double getSimpleDyDyMatrixElement(GridCell g1, GridCell g2){ 
		double offsetX = periodicBoundary(g2.getPosition()[0] - g1.getPosition()[0], grid.sizex());
		double offsetY = periodicBoundary(g2.getPosition()[1] - g1.getPosition()[1], grid.sizey());
		if (Math.abs(offsetX) > 1e-10){
			return 0.0;
		} else {
			if      (Math.abs(offsetY - grid.ySpacing()) < 1e-10){ return -1.0/grid.ySpacing()*grid.xSpacing(); }
			else if (Math.abs(offsetY + grid.ySpacing()) < 1e-10){ return -1.0/grid.ySpacing()*grid.xSpacing(); }
			else if (Math.abs(offsetY                  ) < 1e-10){ return  2.0/grid.ySpacing()*grid.xSpacing(); }
			else { return 0.0; }
		}
	}

	public double getSimpleDxMatrixElement(GridCell g1, GridCell g2){ 
		double offsetX = periodicBoundary(g2.getPosition()[0] - g1.getPosition()[0], grid.sizex());
		double offsetY = periodicBoundary(g2.getPosition()[1] - g1.getPosition()[1], grid.sizey());
		if (Math.abs(offsetY) > 1e-10){
			return 0.0;
		} else {
			if      (Math.abs(offsetX - grid.xSpacing()) < 1e-10){ return 0.5*grid.ySpacing(); }
			else if (Math.abs(offsetX + grid.xSpacing()) < 1e-10){ return -0.5*grid.ySpacing(); }
			else { return 0.0; }
		}
	}
	
	public double getSimpleDyMatrixElement(GridCell g1, GridCell g2){ 
		double offsetX = periodicBoundary(g2.getPosition()[0] - g1.getPosition()[0], grid.sizex());
		double offsetY = periodicBoundary(g2.getPosition()[1] - g1.getPosition()[1], grid.sizey());
		if (Math.abs(offsetX) > 1e-10){
			return 0.0;
		} else {
			if      (Math.abs(offsetY - grid.ySpacing()) < 1e-10){ return  0.5*grid.xSpacing(); }
			else if (Math.abs(offsetY + grid.ySpacing()) < 1e-10){ return -0.5*grid.xSpacing(); }
			else { return 0.0; }
		}
	}

	
	public double getMassMatrixElement(GridCell g1, GridCell g2){ return getMatrixElement(g1, g2, PRODUCT); }

	public double getDxDxMatrixElement(GridCell g1, GridCell g2){ return getMatrixElement(g1, g2, DXDX); }
	public double getDxDyMatrixElement(GridCell g1, GridCell g2){ return getMatrixElement(g1, g2, DXDY); }
	public double getDyDyMatrixElement(GridCell g1, GridCell g2){ return getMatrixElement(g1, g2, DYDY); }

	public double getDxMatrixElement(GridCell g1, GridCell g2){ return getMatrixElement(g1, g2, DX); }
	public double getDyMatrixElement(GridCell g1, GridCell g2){ return getMatrixElement(g1, g2, DY); }

	
	protected double getMatrixElement(GridCell g1, GridCell g2, int TYPE){
		if (grid.dimensions() == 1){ 
			return productIntegral(
					g2.getPosition()[0] - g1.getPosition()[0], TYPE);
		} else if (grid.dimensions() == 2){
			return productIntegral(
				g2.getPosition()[0] - g1.getPosition()[0], 
				g2.getPosition()[1] - g1.getPosition()[1], TYPE);
		} else {
			System.err.println("ERROR: matrix elements not implemented for d = 3");
			return 0;
		}
	}
	
	
	protected double chooseProduct(double x, double y, double offsetX, double offsetY, int TYPE){
		if (TYPE == PRODUCT){
		    return shape(0,0,x,y)*shape(offsetX,offsetY,x,y);
		} else if (TYPE == DX){
		    return shape(0,0,x,y)*shapeDx(offsetX,offsetY,x,y);
		} else if (TYPE == DY){
		    return shape(0,0,x,y)*shapeDy(offsetX,offsetY,x,y);
		} else if (TYPE == DXDX){
		    return shapeDx(0,0,x,y)*shapeDx(offsetX,offsetY,x,y);
		} else if (TYPE == DXDY){
		    return shapeDx(0,0,x,y)*shapeDy(offsetX,offsetY,x,y);
		} else if (TYPE == DYDY){
		    return shapeDy(0,0,x,y)*shapeDy(offsetX,offsetY,x,y);
		} else {
			return 0;
		}
	}
	
	protected double periodicBoundary(double offset, double size){
		return absmin(offset, offset + size, offset - size);				
	}
	
	protected double productIntegral(double offsetX, int TYPE){
	 	// we assume invariance of the integrals under translation
		double[][] integrand = new double[Npoints][1];
		double x, y = 0;
		
		offsetX = periodicBoundary(offsetX, grid.sizex());
		
		if (Math.abs(offsetX) > integralxmax){
			return 0;
		} else {
		
			for (int i = 0; i < integrand.length; i++){
			    x = getX(i);
			    integrand[i][0] = chooseProduct(x, y, offsetX, 0, TYPE);
			}
			return trapezoidal(integrand)*integraldx;
		
		}
	}
	
	 protected double productIntegral(double offsetX, double offsetY, int TYPE){
		 	// we assume invariance of the integrals under translation
			double[][] integrand = new double[Npoints][Npoints];
			double x, y;

			offsetX = periodicBoundary(offsetX, grid.sizex());
			offsetY = periodicBoundary(offsetY, grid.sizey());

			
			if ((offsetX > integralxmax) || (offsetY > integralymax)) {
				return 0;
			} else {
			
				for (int i = 0; i < integrand.length; i++){
				    x = getX(i);
				    for (int j = 0; j < integrand[i].length; j++){
				    	y = getY(j);
				    	integrand[i][j] = chooseProduct(x, y, offsetX, offsetY, TYPE);
				    }
				}
				return trapezoidal(integrand)*integraldx*integraldy;
				
			}
	 }
			
	 
	 protected double absmin(double a, double b, double c){
		 double min;
		 if (Math.abs(a) < Math.abs(b)){ min = a; } else { min = b; }
		 if (Math.abs(c) < Math.abs(min)){ min = c; }
		 return min;
	 }

	protected double shape(double xC, double yC, double x, double y){
		return P1(xC, yC, x, y);
    }

	    
    protected double getX(int i){ return integralxmin + integraldx*i; }
    protected double getY(int j){ return integralymin + integraldy*j; }

    // TODO use simpson instead
    
    protected double trapezoidal(double[][] integrand){
		double res = 0;
		if (integrand[0].length == 1){
			for (int i = 1; i < integrand.length; i++){
				res += (integrand[i][0] + integrand[i-1][0])/2;
			}
		} else {
			for (int i = 1; i < integrand.length; i++){
				for (int j = 1; j < integrand[i].length; j++){
					res += (integrand[i][j] + integrand[i][j-1] + integrand[i-1][j] + integrand[i-1][j-1])/4;
				}
			}
		}
		return res;
    }
    
    protected double simpson(double[][] integrand){
		double res = 0;
		if (integrand[0].length == 1){
			for (int i = 1; i < integrand.length-1; i++){
				res += (integrand[i+1][0] + 4.0*integrand[i][0] + integrand[i-1][0])/6;
			}
		} else {
			for (int i = 1; i < integrand.length-1; i++){
				for (int j = 1; j < integrand[i].length-1; j++){
					System.err.println("ERROR: Simpson not implemented in 2d!");
					//res += (integrand[i][j] + integrand[i][j-1] + integrand[i-1][j] + integrand[i-1][j-1])/4;
				}
			}
		}
		return res;
    }

    protected double shapeDx(double xC, double yC, double x, double y){
    	return (shape(xC, yC, x+integraldx, y) - shape(xC, yC, x-integraldx, y))/2/integraldx;
    }

    protected double shapeDy(double xC, double yC, double x, double y){
    	return (shape(xC, yC, x, y+integraldy)-shape(xC, yC, x, y-integraldy))/2/integraldy;
    }

    
    protected double P1(double xC, double yC, double x, double y){
    	if (grid.dimensions() == 1){ 
    		if (Math.abs(x - xC) < grid.xSpacing()){
				return 1.0 - Math.abs(x-xC)/grid.xSpacing();
			} else {
				return 0;
			}
    	} else if (grid.dimensions() == 2){
			if ((Math.abs(x - xC) < grid.xSpacing()) && (Math.abs(y - yC) < grid.ySpacing())){
				return (1.0 - Math.abs(x-xC)/grid.xSpacing()) * (1.0 - Math.abs(y-yC)/grid.ySpacing());
			} else {
				return 0;
			}
		} else {
			System.err.println("ERROR: P! not implemented for d = 3");
			return 0;
		}
    }

}
