
public class InitialDataAdvectionSphericalJump extends InitialDataScalarAdvection {

	public InitialDataAdvectionSphericalJump(Grid grid, EquationsAdvection equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		double x = g.getX() - grid.xMidpoint();
		double y = g.getY() - grid.yMidpoint();
		
		double r = Math.sqrt(x*x + y*y);
		
		if (r < 0.4){
			res[0] = 1.0;
		} else {
			res[0] = 0.0;
		}
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Initial data for scalar advection with a grid-centered spherical jump";
	}

}
