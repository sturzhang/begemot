
public abstract class Iteration {

    protected Grid grid;

    protected double time;
    
    public boolean verbose = true;

    public boolean onlyFinalOutput = false;
    public boolean printDescriptions = true;

    protected OutputTextOnly descriptionOutput = new OutputTextOnly();
    
    
	public Iteration() {
		// TODO Auto-generated constructor stub
	}

	public void setGrid(Grid grid){ this.grid = grid; }
	
	public abstract String getDescriptions();

    public double time(){ return time; }
    
    public abstract void run(double maxTime);

}
