
public class OutputConservativeJustOneCell extends Output {

	private int i, j, k;
	
	public OutputConservativeJustOneCell(double outputTimeInterval, GridCartesian grid, Equations equations, 
			int i, int j, int k) {
		super(outputTimeInterval, grid, equations);
		this.i = i;
		this.j = j;
		this.k = k;
	}

	@Override
	public String getDetailedDescription() {
		return "output of quantities in given cell";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		println(time + " "); 
		for (int q = 0; q < quantities[i][j][k].length; q++){
			print(quantities[i][j][k][q] + " ");
		}
	}

	@Override
	protected void finalizeOutput() {
		// nothing

	}

}
