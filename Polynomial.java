public class Polynomial {

	private static int PLUS = 1;
	private static int TIMES = 2;
	private static int DIFF = 3;
	private static int INT = 4;

	private double[] coef;
	
	public Polynomial(double[] coef){ this.coef = coef; }
	public Polynomial(int order){ this.coef = new double[order+1]; }
		
	public void setCoef(int i, double val){ coef[i] = val; }
	
	public int order(){ return coef.length-1; }
	
	public String toString(){
		String res = "";
		for (int i = 0; i < coef.length; i++){
			res += coef[i] + "* x^" + i;
		}
		return res;
	}
	   
	public double getCoef(int i){
		if ((i < coef.length) && (i >= 0)){
			return coef[i];
		} else { 
	        return 0.0;
		}
	}
	
	public Polynomial square(){        return mult(this, this); }
	public Polynomial primitive(){     return performOperation(this, this, INT); }
	public Polynomial differentiate(){ return performOperation(this, this, DIFF); }

	public Polynomial differentiate(int numberOfTimes){
		if (numberOfTimes < 1){
	       return this;
		} else {
	       return differentiate().differentiate(numberOfTimes-1);
		}
	}


	public double eval(double x){
		double res, power;

	    res = 0.0;
	    power = 1.0;
	    for (int i = 0; i < coef.length; i++){
	       res += coef[i] * power;
	       power *= x;
	    }
	    return res;
	}

	public double integrate(double lowerLimit, double upperLimit){
		Polynomial primit = primitive();
	    return primit.eval(upperLimit) - primit.eval(lowerLimit);
	}

	
	
	// static methods -------------------------------------------------------
	
	
	public static Polynomial add(Polynomial p1, Polynomial p2){
		return performOperation(p1, p2, PLUS);
	}

	public static Polynomial mult(Polynomial p1, Polynomial p2){
		return performOperation(p1, p2, TIMES);
	}

	public static Polynomial performOperation(Polynomial p1, Polynomial p2, int operation){
		Polynomial res = new Polynomial(0); // to make compiler happy
		
	    if (operation == PLUS){ res = new Polynomial(Math.max(p1.order(), p2.order())); }
	    if (operation == TIMES){ res = new Polynomial(p1.order() + p2.order()); }
	    if (operation == DIFF){ res = new Polynomial(p1.order()-1); }
	    if (operation == INT){ res = new Polynomial(p1.order()+1); }
	    
	    for (int i = 0; i <= res.order(); i++){
	    	if (operation == PLUS){
	    		res.setCoef(i, p1.getCoef(i) + p2.getCoef(i));
	    	}
	    	if (operation == TIMES){
	          double tmp = 0.0;
	          for (int j = 0; j <= i; j++){
	             tmp += p1.getCoef(j) * p2.getCoef(i-j);
	          }
	          res.setCoef(i, tmp);
	    	}
	    	if (operation == DIFF){
	    		res.setCoef(i, p1.getCoef(i+1)*(i+1));
	    	}
	    	if (operation == INT){
	    		if (i > 0){
	    			res.setCoef(i, p1.getCoef(i-1) / i);
	    		} else {
	    			res.setCoef(i, 0.0); // integration constant, actually
	    		}
	    	}
	    }
	    return res;
	}


}
