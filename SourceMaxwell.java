
public abstract class SourceMaxwell extends SourceInCellOnly {

	public SourceMaxwell(Grid grid, EquationsMaxwell equations) {
		super(grid, equations, false);
	}

	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell gg, double time) {
		double[] res = new double[quantities.length];
		double[] current = current(gg, time);
		
		res[EquationsMaxwell.INDEX_EX]   = current[0];
		res[EquationsMaxwell.INDEX_EY]   = current[1];
		res[EquationsMaxwell.INDEX_EZ]   = current[2];
		
		res[EquationsMaxwell.INDEX_BX] = 0.0;
		res[EquationsMaxwell.INDEX_BY] = 0.0;
		res[EquationsMaxwell.INDEX_BZ] = 0.0;
		
		return res;
	}

	protected abstract double[] current(GridCell g, double time);
	

}
