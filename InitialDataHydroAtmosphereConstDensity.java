
public class InitialDataHydroAtmosphereConstDensity extends InitialDataHydroAtmosphere {

	private double rho;
	private double pressureAtBottom;

	
	public InitialDataHydroAtmosphereConstDensity(GridCartesian grid, EquationsHydroIdeal equations,
			double rho, double pressureAtBottom, double gy, double xVelocity, boolean randomPerturbation) {
		super(grid, equations, gy, xVelocity, randomPerturbation);
		this.rho = rho;
		this.pressureAtBottom = pressureAtBottom;
	}

    public String getDetailedDescription(){
    	return "Static atmosphere with a perfect gas of constant density = " + rho + ", pressure at bottom = " + 
    			pressureAtBottom + ", gravity pointing along the y-axis and having the value g_y = " + gy;
    }

    public double getRho(double y){
    	return rho;
    }
        
    public double getPressure(double y){
    	return pressureAtBottom + y*gy*rho;
    }

	@Override
	public double getRhoDeriv(double y) {
		return 0.0;
	}

	@Override
	public double getPressureDeriv(double y) {
		return gy*rho;
	}

	
}
