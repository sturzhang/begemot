
public class OutputDeviationFromInitial extends OutputAllQuantities {

	protected InitialData initial;
	
	public OutputDeviationFromInitial(double outputTimeInterval,
			GridCartesian grid, Equations equations, InitialData initial) {
		super(outputTimeInterval, grid, equations);
		this.initial = initial;
	}

	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		double[] init = initial.getInitialValue(new GridCell(i, j, k, grid), quantities.length);
		
		for (int q = 0; q < quantities.length; q++){
			print((quantities[q] - init[q]) + " ");
		}
	}

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getDetailedDescription() {
    	return "prints conserved quantities minus the initial ones";
	}

}
