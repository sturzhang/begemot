
public class OutputCheckerboard extends Output {

	public OutputCheckerboard(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations){
		super(outputTimeInterval, grid, equations);
		this.equations = equations;
	}
	
	@Override
	public String getDetailedDescription() {
		return "prints difference between minimum and maximum values of all quantities over time";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		boolean firsttime = true;
		int qq = quantities[0][0][0].length;
		double[] min = new double[qq];
		double[] max = new double[qq];
		double[] diff = new double[qq];
		
		
		for (int i = 0; i < quantities.length; i++){
			for (int j = 0; j < quantities[i].length; j++){
				for (int k = 0; k < quantities[i][j].length; k++){
					if (firsttime){
						for (int q = 0; q < qq; q++){
							min[q] = quantities[i][j][k][q];
						}
						firsttime = false;
					} else {
						for (int q = 0; q < qq; q++){
							min[q] = Math.min(quantities[i][j][k][q], min[q]);
							max[q] = Math.max(quantities[i][j][k][q], max[q]);
						}
					}
					
				}
			}
		}
		
		for (int q = 0; q < qq; q++){
			diff[q] = quantities[((GridCartesian) grid).nx()/2][((GridCartesian) grid).ny()/2][0][q];
			//diff[q] = max[q] - min[q];
		}

		
		println(time + " " + diff[EquationsHydroIdeal.INDEX_RHO] + 
				" " + diff[EquationsHydroIdeal.INDEX_XMOM] + 
				" " + diff[EquationsHydroIdeal.INDEX_YMOM] + 
				" " + diff[EquationsHydroIdeal.INDEX_ZMOM] + 
				" " + diff[EquationsHydroIdeal.INDEX_ENERGY] );


	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
