import matrices.SquareMatrix;


public class FluxSolverOneStepMultiDRoeHydroFineTunedDivergence extends FluxSolverOneStepMultiDCentralAndDiffusion {

	protected int meanType = FluxSolverRoeHydro.MEANSIMPLE; // mainly without effect
	
	public FluxSolverOneStepMultiDRoeHydroFineTunedDivergence(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop, double[] left,
			double[] leftbottom, double[] righttop, double[] right, double[] rightbottom, int direction){

		int quantities = left.length;
		
		
		double multidaverageLeftTop[] = new double[quantities];
		double multidaverageLeftBottom[] = new double[quantities];
		double multidaverageRightTop[] = new double[quantities];
		double multidaverageRightBottom[] = new double[quantities];
		
		double orthogonaldiffTop[] = new double[quantities];
		double orthogonaldiffBottom[] = new double[quantities];
		
		double xAverage[] = new double[quantities];
		double xAverageTop[] = new double[quantities];
		double xAverageBottom[] = new double[quantities];
		
		for (int q = 0; q < quantities; q++){ 
			multidaverageLeftTop[q]  = (left[q]  + lefttop[q])/2;     // { q_{i} }_{j+1/2}/2
			multidaverageLeftBottom[q]  = (leftbottom[q]  + left[q])/2;  // { q_{i} }_{j-1/2}/2
			
			multidaverageRightTop[q] = (right[q] + righttop[q])/2;  // { q_{i+1} }_{j+1/2}/2
			multidaverageRightBottom[q] = (rightbottom[q] + right[q])/2;   // { q_{i+1} }_{j-1/2}/2
			
			xAverage[q] = (left[q] + right[q])/2;
			xAverageTop[q] = (lefttop[q] + righttop[q])/2;
			xAverageBottom[q] = (leftbottom[q] + rightbottom[q])/2;
		}
		
		double averageVelocityLeftTop = (left[EquationsHydroIdeal.INDEX_XMOM]/left[EquationsHydroIdeal.INDEX_RHO] 
				+ lefttop[EquationsHydroIdeal.INDEX_XMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO])/2;
				// { u_{i} }_{j+1/2} /2
		double averageVelocityLeftBottom = (leftbottom[EquationsHydroIdeal.INDEX_XMOM]/leftbottom[EquationsHydroIdeal.INDEX_RHO] 
				+ left[EquationsHydroIdeal.INDEX_XMOM]/left[EquationsHydroIdeal.INDEX_RHO])/2;
				// { u_{i} }_{j-1/2} /2
		
		double averageVelocityRightTop = (right[EquationsHydroIdeal.INDEX_XMOM]/right[EquationsHydroIdeal.INDEX_RHO] 
				+ righttop[EquationsHydroIdeal.INDEX_XMOM]/righttop[EquationsHydroIdeal.INDEX_RHO])/2;
				// { u_{i+1} }_{j+1/2} /2
		double averageVelocityRightBottom = (rightbottom[EquationsHydroIdeal.INDEX_XMOM]/rightbottom[EquationsHydroIdeal.INDEX_RHO] 
				+ right[EquationsHydroIdeal.INDEX_XMOM]/right[EquationsHydroIdeal.INDEX_RHO])/2;
				// { u_{i+1} }_{j-1/2} /2
		
		double orthoVelocityBottom = (leftbottom[EquationsHydroIdeal.INDEX_YMOM]/leftbottom[EquationsHydroIdeal.INDEX_RHO]
				+rightbottom[EquationsHydroIdeal.INDEX_YMOM]/rightbottom[EquationsHydroIdeal.INDEX_RHO]
				+left[EquationsHydroIdeal.INDEX_YMOM]/left[EquationsHydroIdeal.INDEX_RHO]
				+right[EquationsHydroIdeal.INDEX_YMOM]/right[EquationsHydroIdeal.INDEX_RHO])/4;
				// { { v }_{i+1/2} }_{j-1/2}/4
		double orthoVelocityTop = (lefttop[EquationsHydroIdeal.INDEX_YMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO]
				+righttop[EquationsHydroIdeal.INDEX_YMOM]/righttop[EquationsHydroIdeal.INDEX_RHO]
				+left[EquationsHydroIdeal.INDEX_YMOM]/left[EquationsHydroIdeal.INDEX_RHO]
				+right[EquationsHydroIdeal.INDEX_YMOM]/right[EquationsHydroIdeal.INDEX_RHO])/4;
				// { { v }_{i+1/2} }_{j+1/2}/4
		
		double orthoVelocityXAverage       = (left[EquationsHydroIdeal.INDEX_YMOM]/left[EquationsHydroIdeal.INDEX_RHO] 
				+ right[EquationsHydroIdeal.INDEX_YMOM]/right[EquationsHydroIdeal.INDEX_RHO])/2;
		double orthoVelocityXAverageTop    = (lefttop[EquationsHydroIdeal.INDEX_YMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO] 
				+ righttop[EquationsHydroIdeal.INDEX_YMOM]/righttop[EquationsHydroIdeal.INDEX_RHO])/2;
		double orthoVelocityXAverageBottom = (leftbottom[EquationsHydroIdeal.INDEX_YMOM]/leftbottom[EquationsHydroIdeal.INDEX_RHO] 
				+ rightbottom[EquationsHydroIdeal.INDEX_YMOM]/rightbottom[EquationsHydroIdeal.INDEX_RHO])/2;
		
		for (int q = 0; q < quantities; q++){ 
			orthogonaldiffTop[q] = (righttop[q] - right[q] + lefttop[q] - left[q])/2;
			// { { q }_{i+1/2} ]_{j+1/2} / 2
			orthogonaldiffBottom[q] = (right[q] - rightbottom[q] + left[q] - leftbottom[q])/2;
			// { { q }_{i+1/2} ]_{j-1/2} / 2
		}
		
		return combineTermsIntoFlux(quantities, direction, i, j, k,
				multidaverageLeftTop, multidaverageLeftBottom, multidaverageRightTop, multidaverageRightBottom,
				averageVelocityLeftTop, averageVelocityLeftBottom, averageVelocityRightTop, averageVelocityRightBottom,
				orthogonaldiffTop, orthogonaldiffBottom, orthoVelocityTop, orthoVelocityBottom,
				xAverage, xAverageTop, xAverageBottom,
				orthoVelocityXAverage, orthoVelocityXAverageTop, orthoVelocityXAverageBottom);
		
	}
		
	protected double[] combineTermsIntoFlux(int quantities, int direction, int i, int j, int k,
			double[] multidaverageLeftTop, double[] multidaverageLeftBottom, 
			double[] multidaverageRightTop, double[] multidaverageRightBottom,
			double averageVelocityLeftTop, double averageVelocityLeftBottom, 
			double averageVelocityRightTop, double averageVelocityRightBottom,
			double[] orthogonaldiffTop, double[] orthogonaldiffBottom, 
			double orthoVelocityTop, double orthoVelocityBottom,
			double[] xAverage, double[] xAverageTop, double[] xAverageBottom,
			double orthoVelocityXAverage, double orthoVelocityXAverageTop, double orthoVelocityXAverageBottom){
		
		/*
		multidaverageLeftTop     = { q_{i} }_{j+1/2}/2
		multidaverageLeftBottom  = { q_{i} }_{j-1/2}/2
		multidaverageRightTop    = { q_{i+1} }_{j+1/2}/2
		multidaverageRightBottom = { q_{i+1} }_{j-1/2}/2
		averageVelocityLeftTop     = { u_{i} }_{j+1/2} /2
		averageVelocityLeftBottom  =  { u_{i} }_{j-1/2} /2
		averageVelocityRightTop    = { u_{i+1} }_{j+1/2} /2
		averageVelocityRightBottom = { u_{i+1} }_{j-1/2} /2
		orthogonaldiffTop     = { { q }_{i+1/2} ]_{j+1/2} / 2
		orthogonaldiffBottom  = { { q }_{i+1/2} ]_{j-1/2} / 2
		orthoVelocityTop    = { { v }_{i+1/2} }_{j+1/2}/4
		orthoVelocityBottom = { { v }_{i+1/2} }_{j-1/2}/4
		xAverage       = { q }_{i+1/2,j}
		xAverageTop    = { q }_{i+1/2,j+1}
		xAverageBottom = { q }_{i+1/2,j-1}
		*/
		
		SquareMatrix upwindingMatrix = new SquareMatrix(quantities);
		
		double[] upwindingTerm = new double[quantities];
		double[] res = new double[quantities];
		
		double multidaverage[] = new double[quantities];
		double multidaverageLeft[] = new double[quantities];
		double multidaverageRight[] = new double[quantities];
		double multidaverageOrtho[] = new double[quantities];
				
		double diff[] = new double[quantities];
		double orthogonaldiff[] = new double[quantities];
		
		
		for (int q = 0; q < quantities; q++){ 
			multidaverageLeft[q]  = (multidaverageLeftTop[q] + multidaverageLeftBottom[q])/2;
			multidaverageRight[q] = (multidaverageRightTop[q] + multidaverageRightBottom[q])/2;  
			multidaverage[q] = (multidaverageLeft[q] + multidaverageRight[q])/2;  
			multidaverageOrtho[q] = multidaverage[q]; //(righttop[q] + rightbottom[q] + lefttop[q] + leftbottom[q])/4; 
		}
		
		double averageVelocityLeft = (averageVelocityLeftTop + averageVelocityLeftBottom)/2;
		double averageVelocityRight = (averageVelocityRightTop + averageVelocityRightBottom)/2;
		
		for (int q = 0; q < quantities; q++){ 
			//diff[q] = right[q] - left[q];
			diff[q] = multidaverageRight[q] - multidaverageLeft[q];
			
			orthogonaldiff[q] = (orthogonaldiffTop[q] + orthogonaldiffBottom[q])/2;
		}
		diff[EquationsHydroIdeal.INDEX_XMOM] = multidaverageRight[EquationsHydroIdeal.INDEX_RHO]*averageVelocityRight - 
				multidaverageLeft[EquationsHydroIdeal.INDEX_RHO]*averageVelocityLeft; //*/
		/*orthogonaldiff[EquationsHydroIdeal.INDEX_YMOM] = 
				(lefttop[EquationsHydroIdeal.INDEX_RHO]+righttop[EquationsHydroIdeal.INDEX_RHO]+
				left[EquationsHydroIdeal.INDEX_RHO]+right[EquationsHydroIdeal.INDEX_RHO])/4*orthoVelocityTop -
				(leftbottom[EquationsHydroIdeal.INDEX_RHO]+rightbottom[EquationsHydroIdeal.INDEX_RHO]+
				left[EquationsHydroIdeal.INDEX_RHO]+right[EquationsHydroIdeal.INDEX_RHO])/4*orthoVelocityBottom;//*/
		orthogonaldiff[EquationsHydroIdeal.INDEX_YMOM] = 
				(multidaverageLeftTop[EquationsHydroIdeal.INDEX_RHO] 
						+ multidaverageRightTop[EquationsHydroIdeal.INDEX_RHO])/2*orthoVelocityTop -
				(multidaverageLeftBottom[EquationsHydroIdeal.INDEX_RHO] 
						+ multidaverageRightBottom[EquationsHydroIdeal.INDEX_RHO])/2*orthoVelocityBottom;
		
		multidaverage[EquationsHydroIdeal.INDEX_XMOM] = multidaverage[EquationsHydroIdeal.INDEX_RHO]*
				(averageVelocityRight+averageVelocityLeft)/2; //*/
		
		
		UpwindingMatrixAndWaveSpeed tmp = getUpwindingMatrix(i, j, k, multidaverage, direction);
		upwindingMatrix = tmp.matrix();
		upwindingTerm = upwindingMatrix.mult(0.5).mult(diff);
	    		
		multidaverageOrtho[EquationsHydroIdeal.INDEX_YMOM] = (orthoVelocityTop + orthoVelocityBottom)/2*multidaverageOrtho[EquationsHydroIdeal.INDEX_RHO]; 
		
		SquareMatrix ortho = getOrthogonalContribution(multidaverageOrtho, direction);
		double[] multidextra = ortho.mult(0.5).mult(orthogonaldiff);

		
		for (int q = 0; q < quantities; q++){
    		res[q] = upwindingTerm[q] + multidextra[q];
    	}
		
    	return res;

	}
	
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		return usualSolver.getAveragedUpwindingMatrix(i, j, k, quantities, quantities, direction);
	}

	protected SquareMatrix getOrthogonalContribution(double cMean, double vMeanOther) {
		double[][] mat = new double[5][5];
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				mat[i][j] = 0.0;
			}
		}
		
		mat[1][2] = cMean;
		
		mat[1][0] = -cMean*vMeanOther;
		
		//mat[2][2] = sign(vMean)*vMeanOther;
		
		//additional:
		/*mat[1][1] = vMean*sign(vMeanOther);
		mat[4][1] = cMean*cMean*sign(vMeanOther); //*/
		// new:
		/*mat[4][2] = vMean*cMean;
		mat[2][4] = vMean/cMean;//*/
		
		//	mat[2][4] = 1.0;
		
		return new SquareMatrix(mat);
	}
	
	
	protected SquareMatrix getOrthogonalContribution(double[] quantities, int direction) {
		int otherdirection;
		// multiple inheritance -- workaround
		//FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		//SquareMatrix res;
		
		
		
		HydroState qu = new HydroState(quantities, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		double cMean = qu.soundSpeed;
		
		if (direction == GridCartesian.X_DIR){
			double vMean = qu.vX;
			double vMeanOther = qu.vY;
			
			return getOrthogonalContribution(cMean, vMeanOther);
			

		} else {
			System.err.println("ERROR: Asked for perpendicular diffusion matrix of non-x directioN!");
			return null;
		}
		
		
		
	}

	
	
	@Override
	protected double[] averageFlux(int i, int j, int k, double[] lefttop, double[] left, double[] leftbottom, 
			double[] righttop, double[] right, double[] rightbottom, int direction){
		if (direction == GridCartesian.X_DIR){
			double multidaverageleft[] = new double[left.length];
			double multidaverageright[] = new double[left.length];
			
			for (int q = 0; q < left.length; q++){ 
				multidaverageleft[q] = 0.5*left[q]   + 0.25*(lefttop[q]  + leftbottom[q]); 
				multidaverageright[q] = 0.5*right[q] + 0.25*(righttop[q] + rightbottom[q]);
			}
			
			double averageVelocityLeft = (leftbottom[EquationsHydroIdeal.INDEX_XMOM]/leftbottom[EquationsHydroIdeal.INDEX_RHO] 
					+ 2.0*left[EquationsHydroIdeal.INDEX_XMOM]/left[EquationsHydroIdeal.INDEX_RHO] 
					+ lefttop[EquationsHydroIdeal.INDEX_XMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO])/4;
			double averageVelocityRight = (rightbottom[EquationsHydroIdeal.INDEX_XMOM]/rightbottom[EquationsHydroIdeal.INDEX_RHO] 
					+ 2.0*right[EquationsHydroIdeal.INDEX_XMOM]/right[EquationsHydroIdeal.INDEX_RHO] 
					+ righttop[EquationsHydroIdeal.INDEX_XMOM]/righttop[EquationsHydroIdeal.INDEX_RHO])/4;
			
			double EPlusPLeft = (lefttop[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(lefttop) +
					2.0*(left[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(left)) +
					leftbottom[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(leftbottom))/4;
			double EPlusPRight = (righttop[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(righttop) +
					2.0*(right[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(right)) +
					rightbottom[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(rightbottom))/4;
			
			double[] flux = averageFlux(i,j,k, multidaverageleft, multidaverageright, direction);
			
			//System.err.println(((EquationsHydroIdeal) equations).getPressureFromConservative(righttop));
			//System.err.println(((EquationsHydroIdeal) equations).getPressureFromConservative(rightbottom));
			//System.err.println(((EquationsHydroIdeal) equations).getPressureFromConservative(lefttop));
			//System.err.println(((EquationsHydroIdeal) equations).getPressureFromConservative(leftbottom) );
			//System.err.println(flux.length);
			
			/* old flux[EquationsHydroIdeal.INDEX_XMOM] = (righttop[EquationsHydroIdeal.INDEX_XMOM]*righttop[EquationsHydroIdeal.INDEX_XMOM]/
					righttop[EquationsHydroIdeal.INDEX_RHO] + ((EquationsHydroIdeal) equations).getPressureFromConservative(righttop) + 
					rightbottom[EquationsHydroIdeal.INDEX_XMOM]*rightbottom[EquationsHydroIdeal.INDEX_XMOM]/
					rightbottom[EquationsHydroIdeal.INDEX_RHO] + ((EquationsHydroIdeal) equations).getPressureFromConservative(rightbottom) +
					lefttop[EquationsHydroIdeal.INDEX_XMOM]*lefttop[EquationsHydroIdeal.INDEX_XMOM]/
					lefttop[EquationsHydroIdeal.INDEX_RHO] + ((EquationsHydroIdeal) equations).getPressureFromConservative(lefttop) + 
					leftbottom[EquationsHydroIdeal.INDEX_XMOM]*leftbottom[EquationsHydroIdeal.INDEX_XMOM]/
					leftbottom[EquationsHydroIdeal.INDEX_RHO] + ((EquationsHydroIdeal) equations).getPressureFromConservative(leftbottom) ) / 4; //*/
			/* old flux[EquationsHydroIdeal.INDEX_ENERGY] = 
					((righttop[EquationsHydroIdeal.INDEX_XMOM]/righttop[EquationsHydroIdeal.INDEX_RHO] + 
					2.0*right[EquationsHydroIdeal.INDEX_XMOM]/righttop[EquationsHydroIdeal.INDEX_RHO]+
					rightbottom[EquationsHydroIdeal.INDEX_XMOM]/righttop[EquationsHydroIdeal.INDEX_RHO])*
					(righttop[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(righttop) + 
					rightbottom[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(rightbottom)) + 
					(lefttop[EquationsHydroIdeal.INDEX_XMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO] + 
					2.0*left[EquationsHydroIdeal.INDEX_XMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO]+
					leftbottom[EquationsHydroIdeal.INDEX_XMOM]/lefttop[EquationsHydroIdeal.INDEX_RHO])*
					(lefttop[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(lefttop) + 
					leftbottom[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(leftbottom)) 
					)/16; //*/
			flux[EquationsHydroIdeal.INDEX_ENERGY] = (averageVelocityLeft + averageVelocityRight)/2*(EPlusPRight + EPlusPLeft)/2;
			
			return flux;
		} else {
			System.err.println("ERROR: Asked for the average flux in a direction that is not x!");
			return null;
		}
		
	}

	public String getDetailedDescription() {
		return "Multi-d solver based on an extension of the Roe solver for ideal gas, with making sure that at discrete level the diffusion is based on the same divergence as the one appearing in the central flux.";
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		return usualSolver.averageFlux(i, j, k, left, right, direction);		
	}


}
