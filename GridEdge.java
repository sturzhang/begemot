
public class GridEdge {

	/*
	 * 
	 * philosophy: as gridedge is just a placeholder for a coordinate tuple, it should have as 
	 * little properties as possible (that would need to be calculated every time such a gridcell
	 * is created). gridedges will be created over and over again 
	 * and this extra calculations should only be performed when they are really necessary.
	 * therefore all properties that are not absolutely necessary should not be saved but asked for
	 * at the grid. the grid of course should precompute as many of them as possible.
	 * 
	 */

	protected Grid grid;
	protected GridCell adjacent1, adjacent2;
	
	public GridEdge(Grid grid, GridCell adjacent1, GridCell adjacent2) {
		this.grid = grid;
		this.adjacent1 = adjacent1;
		this.adjacent2 = adjacent2;
	}
	
	public boolean adjoins(GridCell g){ return ((g.equals(adjacent1)) || (g.equals(adjacent2))); }
	
	public double[] midpoint(){
		return new double[] { 0.5*(adjacent1.getPosition()[0] + adjacent2.getPosition()[0]), 
				0.5*(adjacent1.getPosition()[1] + adjacent2.getPosition()[1])};
	}
	
	public GridCell adjacent1(){ return adjacent1; }
	public GridCell adjacent2(){ return adjacent2; }

	public boolean equals(GridEdge e){
		return (
				(e.adjacent1().equals(adjacent1) && e.adjacent2().equals(adjacent2)) ||
				(e.adjacent1().equals(adjacent2) && e.adjacent2().equals(adjacent1)) );
	}
	
	public GridCell getOtherGridCell(GridCell g){
		if (g.equals(adjacent1)){
			return adjacent2;
		} else if (g.equals(adjacent2)){
			return adjacent1;
		} else {
			System.err.println("Asked for a non-adjacent cell " + g.printIndices() + " to the edge between " + 
					adjacent1.printIndices() + " and " + adjacent2.printIndices());
			return null;
		} 
	}
	
	
	public double[] normalVector(){ return grid.normalVector(this); }
	
	public double sizeModified(){ return grid.edgeSize(this); }
}
