import matrices.SquareMatrix;


public abstract class FluxSolverOneStepMultiDRoe extends FluxSolverOneStepMultiDCentralAndDiffusion {

	public FluxSolverOneStepMultiDRoe(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	
	abstract protected SquareMatrix getOrthogonalContribution(double[] left, double[] right, int direction);
	abstract protected SquareMatrix getUpwindOrthogonalContribution(double[] left, double[] right, int direction);
	abstract protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] left, double[] right, int direction);
	
	
	@Override
	protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop, double[] left,
			double[] leftbottom, double[] righttop, double[] right, double[] rightbottom, int direction){

		int quantities = left.length;
		double diff[] = new double[quantities];
		double orthogonaldiff[] = new double[quantities];
		double upworthogonaldiff[] = new double[quantities];
		
		double[] upwindingTerm = new double[quantities];
		double[] res = new double[quantities];
		SquareMatrix upwindingMatrix = new SquareMatrix(quantities);
		
		double multidaverageleft[] = new double[quantities];
		double multidaverageright[] = new double[quantities];
		for (int q = 0; q < quantities; q++){ 
			// duplicate
			multidaverageleft[q] = 0.5*left[q]   + 0.25*(lefttop[q]  + leftbottom[q]); 
			multidaverageright[q] = 0.5*right[q] + 0.25*(righttop[q] + rightbottom[q]);
		}
		
		for (int q = 0; q < quantities; q++){ diff[q] = multidaverageright[q] - multidaverageleft[q]; }
		//for (int q = 0; q < quantities; q++){ diff[q] = right[q] - left[q]; }
		
		for (int q = 0; q < quantities; q++){ orthogonaldiff[q] =0.25*(lefttop[q] + righttop[q] - leftbottom[q] - rightbottom[q]); }
				
		for (int q = 0; q < quantities; q++){ upworthogonaldiff[q] =0.25*(-lefttop[q] + righttop[q] + leftbottom[q] - rightbottom[q]); }
		
		
		// here we have the freedom of whether to evaluate the upwinding matrix in the averaged state, or
		// whether to pass just left and right as before
		UpwindingMatrixAndWaveSpeed tmp = getUpwindingMatrix(i, j, k, multidaverageleft, multidaverageright, direction);
		upwindingMatrix = tmp.matrix();
		upwindingTerm = upwindingMatrix.mult(0.5).mult(diff);
	    
		SquareMatrix ortho = getOrthogonalContribution(multidaverageleft, multidaverageright, direction);
		double[] multidextra = ortho.mult(0.5).mult(orthogonaldiff);
		
		SquareMatrix upwortho = getUpwindOrthogonalContribution(multidaverageleft, multidaverageright, direction);
		double[] upwmultidextra = upwortho.mult(0.5).mult(upworthogonaldiff);
		
		
		//double[] multidextra = new double[quantities];
		//for (int q = 0; q < quantities; q++){ multidextra[q] = 0.0; }
		
    	for (int q = 0; q < quantities; q++){
    		res[q] = upwindingTerm[q] + multidextra[q]; // - upwmultidextra[q];
    	}
		
    	return res;

	}


}
