
public class InitialDataFourRegionsDivergence extends InitialDataAcoustic {

	protected double[] NE, NW, SE, SW;
	protected double discDivNE, discDivNW, discDivSE, discDivSW;
	
	public InitialDataFourRegionsDivergence(Grid grid,
			EquationsAcoustic equations, double discDivNE, double discDivNW, double discDivSE, double discDivSW) {
		super(grid, equations);
		this.discDivNE = discDivNE;
		this.discDivNW = discDivNW;
		this.discDivSE = discDivSE;
		this.discDivSW = discDivSW;
	}

	public double discDivNE(){ return discDivNE; }
	public double discDivNW(){ return discDivNW; }
	public double discDivSE(){ return discDivSE; }
	public double discDivSW(){ return discDivSW; }
	
	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double x = g.getX()-grid.xMidpoint();
		double y = g.getY()-grid.yMidpoint();
		double dx = 0.5;
		double dy = 0.5;
		

		/*double uijp1     = 34.0;
		double vijp1     = 4.0;
		double uip1j   = 7.0;
		double vip1j   = 43.0;
		double uij     = 9.0;
		double vij     = 17.0;
		double uim1j   = 11.0;
		double vim1j   = 12.0;
		double uijm1     = 15.0;
		double vijm1     = 16.0;

		
		double uip1jm1   = 13.0;
		double uim1jm1   = 17.0;
		double uip1jp1   = 16.0;
		double uim1jp1   = 51.0;//*/
		
		double uijp1     = 0;
		double vijp1     = 0;
		double uip1j     = 0;
		double vip1j     = 0;
		double uij       = 0;
		double vij       = 0;
		double uim1j     = 0;
		double vim1j     = 0;
		double uijm1     = 0;
		double vijm1     = 0;

		
		double uip1jm1   = 0;
		double uim1jm1   = 0;
		double uip1jp1   = 0;
		double uim1jp1   = 0;

		
		
		double vip1jp1  = vip1j - vijp1 + vij + 2.0*dy*(discDivNE - (uip1jp1 - uijp1 + uip1j - uij)/2/dx);
		double vim1jp1  = - vijp1 + vij + vim1j + 2.0*dy*(discDivNW- (uijp1 - uim1jp1 + uij - uim1j)/2/dx);
		double vip1jm1  =  vip1j + vij - vijm1 - 2.0*dy*(discDivSE - (uip1j - uij + uip1jm1 - uijm1)/2/dx);
		double vim1jm1 = vij - vijm1 + vim1j - 2.0*dy*(discDivSW - (uij - uim1j + uijm1 - uim1jm1)/2/dx);
		
		/*double vip1jm1   = 14.0;
		double vip1jp1   = 2.0;
		double vim1jp1   = 6.0;
				
		double discDiv = 0.0;
		double vim1jm1   = vip1jp1 - vip1jm1 + vim1jp1 + 2.0*vijp1 - 2.0*vijm1 
				- 8.0*discDiv 
				+ (uip1jp1 - uim1jp1 + uip1jm1 - uim1jm1 + 2.0*uip1j - 2.0*uim1j); //*/

		
		
		
		
		
		double HvNE = 0;
		double CuNE = 0;
		double BvNE = 0;
		double FuNE =  - ((CuNE*dy + uij - uijp1)/(dy*dy));
		double EvNE =  - ((BvNE*dx + vij - vip1j)/(dx*dx));
		double BuNE =  - 0.5*dx*dy*HvNE + ( - uij + uip1j)/dx + (vij - vijp1 - vip1j + vip1jp1)/(2.0*dy);
		double CvNE = 0.5*(dx*dy*HvNE + (uij - uijp1 - uip1j + uip1jp1)/dx - (2.0*(vij - vijp1))/dy);
		double FvNE =  - (((dx*dx)*dy*HvNE + uij - uijp1 - uip1j + uip1jp1)/(2.0*dx*dy));
		double EuNE = (dx*(dy*dy)*HvNE - vij + vijp1 + vip1j - vip1jp1)/(2.0*dx*dy);


		double CuNW = 0;
		double BvNW = 0;
		double HvNW = 0;
		double FuNW =  - ((CuNW*dy + uij - uijp1)/(dy*dy));
		double EvNW = (BvNW*dx - vij + vim1j)/(dx*dx);
		double BuNW = 0.5*(dx*dy*HvNW + (2.0*(uij - uim1j))/dx + (vij - vijp1 - vim1j + vim1jp1)/dy);
		double CvNW = 0.5*( - dx*dy*HvNW + ( - uij + uijp1 + uim1j - uim1jp1)/dx - (2.0*(vij - vijp1))/dy);
		double FvNW = ((dx*dx)*dy*HvNW + uij - uijp1 - uim1j + uim1jp1)/(2.0*dx*dy);
		double EuNW = (dx*(dy*dy)*HvNW + vij - vijp1 - vim1j + vim1jp1)/(2.0*dx*dy);


		double CuSW = 0;
		double BvSW = 0;
		double HvSW = 0;
		double FuSW = (CuSW*dy - uij + uijm1)/(dy*dy);
		double EvSW = (BvSW*dx - vij + vim1j)/(dx*dx);
		double BuSW =  - 0.5*dx*dy*HvSW + (uij - uim1j)/dx + ( - vij + vijm1 + vim1j - vim1jm1)/(2.0*dy);
		double CvSW = 0.5*(dx*dy*HvSW + ( - uij + uijm1 + uim1j - uim1jm1)/dx + (2.0*(vij - vijm1))/dy);
		double FvSW = ((dx*dx)*dy*HvSW - uij + uijm1 + uim1j - uim1jm1)/(2.0*dx*dy);
		double EuSW =  - ((dx*(dy*dy)*HvSW + vij - vijm1 - vim1j + vim1jm1)/(2.0*dx*dy));


		double CuSE = 0;
		double BvSE = 0;
		double HvSE = 0;
		double FuSE = (CuSE*dy - uij + uijm1)/(dy*dy);
		double EvSE =  - ((BvSE*dx + vij - vip1j)/(dx*dx));
		double BuSE = 0.5*(dx*dy*HvSE + (2.0*( - uij + uip1j))/dx + ( - vij + vijm1 + vip1j - vip1jm1)/dy);
		double CvSE =  - 0.5*dx*dy*HvSE + (uij - uijm1 - uip1j + uip1jm1)/(2.0*dx) + (vij - vijm1)/dy;
		double FvSE =  - (((dx*dx)*dy*HvSE - uij + uijm1 + uip1j - uip1jm1)/(2.0*dx*dy));
		double EuSE =  - ((dx*(dy*dy)*HvSE - vij + vijm1 + vip1j - vip1jm1)/(2.0*dx*dy));

		
		if (x > 0){
			if (y > 0){
				return new double[] {uij + BuNE*x + CuNE*y - 2.0*FvNE*x*y + EuNE*x*x + FuNE*y*y - HvNE*y*x*x,
						vij + BvNE*x + CvNE*y - 2.0*EuNE*x*y + EvNE*x*x + FvNE*y*y + HvNE*y*y*x, 0, 0};
			} else {
				return new double[] {uij + BuSE*x + CuSE*y - 2.0*FvSE*x*y + EuSE*x*x+ FuSE*y*y - HvSE*y*x*x,
						vij + BvSE*x + CvSE*y - 2.0*EuSE*x*y + EvSE*x*x + FvSE*y*y + HvSE*y*y*x, 0, 0};
			}
		} else {
			if (y > 0){
				return new double[] {uij + BuNW*x + CuNW*y - 2.0*FvNW*x*y + EuNW*x*x+ FuNW*y*y - HvNW*y*x*x,
						vij + BvNW*x + CvNW*y - 2.0*EuNW*x*y + EvNW*x*x + FvNW*y*y + HvNW*y*y*x, 0, 0};
			} else {
				return new double[] {uij + BuSW*x + CuSW*y - 2.0*FvSW*x*y + EuSW*x*x+ FuSW*y*y - HvSW*y*x*x,
						vij + BvSW*x + CvSW*y - 2.0*EuSW*x*y + EvSW*x*x + FvSW*y*y + HvSW*y*y*x, 0, 0};
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "four quadrants, as they apear in the derivation of a consistent-diffusion godunov scheme";
	}

}
