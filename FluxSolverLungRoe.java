
public class FluxSolverLungRoe extends FluxSolverMultiStepMultiDCartesianFirstOrder {

	public FluxSolverLungRoe(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LT, double[] L, double[] LB,
			double[] RT, double[] R, double[] RB, int timeIntegratorStep) {
		
		
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		int quantities = L.length;
		double[] flux1d = new double[quantities];
		double[] flux2d = new double[quantities];
		
		double phi1 = 0, alpha2 = 0;
		
		int RotRichtmayrLW = 1; // morton Roe 2001
		int VPLW2 = 2;          // lung and roe 2014
		int scheme = RotRichtmayrLW;
		
		
		if (scheme == VPLW2){
			phi1 = 1.0/12;
			//alpha2 = -1.0/12;  // reduced stability
			alpha2 = 0.125;
		} else if (scheme == RotRichtmayrLW){
			phi1 = 0.25;   // RR scheme
			alpha2 = 0.25;    // RR scheme
		}
		
		
		
		// needed for vorticity:
		double alpha1 = 0.25;
		double phi2 = 0.25;
				
		
		double dxalpha2[]  = new double[quantities];
		double dxphi2[]    = new double[quantities];
		double muxalpha1[] = new double[quantities];
		double muxphi1[]   = new double[quantities];
		double mixedDiff[] = new double[quantities];
		
		for (int q = 0; q < quantities; q++){ 
			dxalpha2[q] = dx(alpha2, LT[q], L[q], LB[q], RT[q], R[q], RB[q]);
			dxphi2[q] = dx(phi2, LT[q], L[q], LB[q], RT[q], R[q], RB[q]);
			
			muxphi1[q] = mux(phi1, LT[q], L[q], LB[q], RT[q], R[q], RB[q]);
			muxalpha1[q] = mux(alpha1, LT[q], L[q], LB[q], RT[q], R[q], RB[q]);
			
			mixedDiff[q] = RT[q] + LT[q] - RB[q] - LB[q];
			
			flux1d[q] = 0.0;
			flux2d[q] = 0.0;
		}
		
		
		
		if (((EquationsAcousticSimple) equations).symmetric()){
			flux1d[EquationsAcoustic.INDEX_VX] = 0.5*c*muxalpha1[EquationsAcoustic.INDEX_P];
			flux1d[EquationsAcoustic.INDEX_P]  = 0.5*c*muxphi1[EquationsAcoustic.INDEX_VX];
					
			flux2d[EquationsAcoustic.INDEX_VX] = -c*dxphi2[EquationsAcoustic.INDEX_VX] - c/4*mixedDiff[EquationsAcoustic.INDEX_VY];
			flux2d[EquationsAcoustic.INDEX_P]  = -c*dxalpha2[EquationsAcoustic.INDEX_P];
		} else {
			System.err.println("Error: No implementation for asymmetric acoustics!");
		}
			
		return new FluxMultiStepAndWaveSpeed(flux1d, flux2d, c);
		
	}

	private double dx(double factor,
			double LT, double L, double LB, double RT, double R, double RB){
		return (R - L)*(1 - 2.0*factor) + factor * (RT - LT + RB - LB);
	}

	private double mux(double factor,
			double LT, double L, double LB, double RT, double R, double RB){
		return (R + L)*(1 - 2.0*factor) + factor * (RT + LT + RB + LB);
	}

	private double soundspeed(){
		return ((EquationsAcoustic) equations).soundspeed();
	}
	
	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep){
		// we don't actually know whether dx or dy is correct because lung and roe do their analysis on square grids
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (((GridCartesian) grid).dimensions() == 2){
				if (dir == GridCartesian.X_DIR){
					return soundspeed()*dt/((GridCartesian) grid).xSpacing()/2;
				} else if (dir == GridCartesian.Y_DIR){
					return soundspeed()*dt/((GridCartesian) grid).xSpacing()/2;
				} else {
					System.err.println("ERROR: asked for non-x, non-y direction");
					return 0.0;
				}
			} else {
				return 0.0;
			}
		}
	}

	@Override
	public int steps() { return 2; }

	@Override	
	public String getDetailedDescription() {
		return "Generalized Lax-Wendroff-type method from Lung and Roe, 2014";
	}

}
