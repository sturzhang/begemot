public abstract class GridTriangular extends GridUnstructured {

	public GridTriangular(){
		super();
	}
	
	@Override
	public void initialize(Obstacle obstacle) {
		this.myObstacle = obstacle;
		setNeighbours();
	}
	protected abstract void setNeighbours();

	protected int edgesPerCell(){ return 3; }
    
    @Override
    public String getDetailedDescription(){ 
    	String res = "";
    	res += "# Grid used: Triangular, 2-d ";
    	res += numberOfCells + " cells ";
    	
    	return "# " + res + " " + getDetailedDescription(); 
    }

	@Override
	public int dimensions() {
		return 2;
	}
	
	@Override
	public double zmax() { return 0; }
	@Override
	public double zmin() { return 0; }

	
}
