import java.util.LinkedList;


public class CornerPoint {

	protected LinkedList<GeometricGridEdge> adjacentEdges;
	protected double[] position;
	
	public CornerPoint(double[] position) {
		this.position = position;
		adjacentEdges = new LinkedList<GeometricGridEdge>();
	}
	
	public void addEdge(GeometricGridEdge edge){
		adjacentEdges.add(edge);
	}
	
	public double x(){ return position[0]; }
	public double y(){ return position[1]; }
	public double[] position(){ return new double[]{ x(), y(), 0}; }
	
	public LinkedList<GeometricGridEdge> adjacentEdges(){
		return adjacentEdges;
	}
}
