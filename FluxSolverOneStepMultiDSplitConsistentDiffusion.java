
public class FluxSolverOneStepMultiDSplitConsistentDiffusion extends
		FluxSolverOneStepMultiDCentralAndDiffusion {

	public FluxSolverOneStepMultiDSplitConsistentDiffusion(GridCartesian grid,
			EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop,
			double[] left, double[] leftbottom, double[] righttop,
			double[] right, double[] rightbottom, int direction) {
		
		int quantities = left.length;
		double[] res = new double[quantities];
		double diff[] = new double[quantities];
		double perp[] = new double[quantities];
		
		int version = 1;
		
		double advectionVelocity = 0.5 * (
				left[EquationsIsentropicEuler.INDEX_XMOM]/left[EquationsIsentropicEuler.INDEX_RHO] + right[EquationsIsentropicEuler.INDEX_XMOM]/right[EquationsIsentropicEuler.INDEX_RHO]);
		double soundspeed = 0.5*(
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(left) + 
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(right));//*/
		/*double soundspeed = (
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(lefttop) +
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(left) +
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(leftbottom) +
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(righttop) +
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(right) +
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(rightbottom)) / 6; //*/
		
		for (int q = 0; q < quantities; q++){ 
			//diff[q] = right[q] - left[q]; 
			diff[q] = 0.25* righttop[q] + 0.5*right[q] + 0.25*rightbottom[q] - 0.25*lefttop[q] - 0.5*left[q] - 0.25*leftbottom[q];
			perp[q] = lefttop[q] + righttop[q] - leftbottom[q] - rightbottom[q];
		}
		
		for (int q = 0; q < quantities; q++){
			//res[q] = 0.5*Math.abs(advectionVelocity)*diff[q];
			res[q] = 0.5*Math.abs(advectionVelocity)*(diff[q] + perp[q]/4);
		}
		
		
		
		
		
		double meanRho = 0.5*(left[EquationsIsentropicEuler.INDEX_RHO] + right[EquationsIsentropicEuler.INDEX_RHO]);
		double meanMx = 0.5*(left[EquationsIsentropicEuler.INDEX_XMOM] + right[EquationsIsentropicEuler.INDEX_XMOM]);
		double meanMy = 0.5*(left[EquationsIsentropicEuler.INDEX_YMOM] + right[EquationsIsentropicEuler.INDEX_YMOM]);//*/
		
		/*double meanRho = 0.25*(left[EquationsIsentropicEuler.INDEX_RHO] + right[EquationsIsentropicEuler.INDEX_RHO]) + 
				0.125 * (lefttop[EquationsIsentropicEuler.INDEX_RHO] + righttop[EquationsIsentropicEuler.INDEX_RHO] + 
						leftbottom[EquationsIsentropicEuler.INDEX_RHO] + rightbottom[EquationsIsentropicEuler.INDEX_RHO]);
		double meanMx = 0.25*(left[EquationsIsentropicEuler.INDEX_XMOM] + right[EquationsIsentropicEuler.INDEX_XMOM]) + 
				0.125*(lefttop[EquationsIsentropicEuler.INDEX_XMOM] + righttop[EquationsIsentropicEuler.INDEX_XMOM] + 
						leftbottom[EquationsIsentropicEuler.INDEX_XMOM] + rightbottom[EquationsIsentropicEuler.INDEX_XMOM]);//*/
			
		double multidaverageleftvx = 0.5*left[EquationsIsentropicEuler.INDEX_XMOM]/left[EquationsIsentropicEuler.INDEX_RHO]
					+ 0.25*(lefttop[EquationsIsentropicEuler.INDEX_XMOM]/lefttop[EquationsIsentropicEuler.INDEX_RHO]  
							+ leftbottom[EquationsIsentropicEuler.INDEX_XMOM]/leftbottom[EquationsIsentropicEuler.INDEX_RHO]); 
		
		double multidaveragerightvx = 0.5*right[EquationsIsentropicEuler.INDEX_XMOM]/right[EquationsIsentropicEuler.INDEX_RHO]
				+ 0.25*(righttop[EquationsIsentropicEuler.INDEX_XMOM]/righttop[EquationsIsentropicEuler.INDEX_RHO]  
						+ rightbottom[EquationsIsentropicEuler.INDEX_XMOM]/rightbottom[EquationsIsentropicEuler.INDEX_RHO]); 
		
		double perpdiffleftvy = lefttop[EquationsIsentropicEuler.INDEX_YMOM]/lefttop[EquationsIsentropicEuler.INDEX_RHO] -
				leftbottom[EquationsIsentropicEuler.INDEX_YMOM]/leftbottom[EquationsIsentropicEuler.INDEX_RHO]; //*/
		
		double perpdiffrightvy = righttop[EquationsIsentropicEuler.INDEX_YMOM]/righttop[EquationsIsentropicEuler.INDEX_RHO] -
				rightbottom[EquationsIsentropicEuler.INDEX_YMOM]/rightbottom[EquationsIsentropicEuler.INDEX_RHO]; //*/
	
		double pressureRight = ((EquationsIsentropicEuler) equations).getPressure(right[EquationsIsentropicEuler.INDEX_RHO]);
		double pressureLeft = ((EquationsIsentropicEuler) equations).getPressure(left[EquationsIsentropicEuler.INDEX_RHO]);
		
		if (version == 4){
			// inspired by 1-d Roe
			res[EquationsIsentropicEuler.INDEX_RHO]  += 0.5*(
					meanMx / soundspeed *
					( (multidaveragerightvx - multidaverageleftvx)
					+
					(perpdiffrightvy + perpdiffleftvy) ) 
					+ 
					(soundspeed 
					//		- Math.abs(meanMx/meanRho)
					) * 
					(pressureRight - pressureLeft) / soundspeed / soundspeed
					);
					
			res[EquationsIsentropicEuler.INDEX_XMOM] += 0.5*(
					//(2.0*soundspeed*meanMx/meanRho 
							//- Math.abs(meanMx)*meanMx/meanRho/meanRho
					//		) * 
					//(pressureRight - pressureLeft) / soundspeed/soundspeed
					//+
					meanRho * (soundspeed 
							//+ meanMx*meanMx/meanRho/meanRho/soundspeed 
							//- Math.abs(meanMx)/meanRho
							) * 
					( (multidaveragerightvx - multidaverageleftvx)
					+
					(perpdiffrightvy + perpdiffleftvy) ) 
					);
		
		} else if (version == 3){
			res[EquationsIsentropicEuler.INDEX_RHO]  += 0.5*(
					-(meanRho * (multidaveragerightvx - multidaverageleftvx)
					+
					meanRho * (perpdiffrightvy + perpdiffleftvy) ) 
					+ 2.0* (pressureRight - pressureLeft)
					);
			res[EquationsIsentropicEuler.INDEX_XMOM] += 0.5*(
					(1.0 + 2.0*meanMx/meanRho/soundspeed) * (pressureRight - pressureLeft)
					+
					-(meanMx * (multidaveragerightvx - multidaverageleftvx)
					+
					meanMx * (perpdiffrightvy + perpdiffleftvy) ) 
					);
			
		
		} else if (version == 2){
			res[EquationsIsentropicEuler.INDEX_RHO]  += 0.5*(
				pressureRight - pressureLeft
				)/soundspeed;
			res[EquationsIsentropicEuler.INDEX_XMOM] += 0.5*(
				meanMx/meanRho * (pressureRight - pressureLeft)/soundspeed
				//+
				//(meanRho * (multidaveragerightvx - multidaverageleftvx)
				//+
				//meanRho * (perpdiffrightvy + perpdiffleftvy) )*soundspeed 
				);
		
			/*res[EquationsIsentropicEuler.INDEX_YMOM] += 0.5*(
				meanMy/meanRho * (pressureRight - pressureLeft)/soundspeed
				+ 
				(meanRho * (multidaveragerightvx - multidaverageleftvx)
				+
				meanRho * (perpdiffrightvy + perpdiffleftvy) )*soundspeed 
				);*/
		} else if (version == 1) {
			res[EquationsIsentropicEuler.INDEX_RHO]  += 0.5*(
				meanMx * (multidaveragerightvx - multidaverageleftvx)
				+
				meanMx * (perpdiffrightvy + perpdiffleftvy)
				+ 
				pressureRight - pressureLeft
				)/soundspeed;
		}
		return res;
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoe usualSolver = new FluxSolverRoe((GridCartesian) grid, (EquationsIsentropicEuler) equations);
		return usualSolver.averageFlux(i, j, k, left, right, direction);		
	}

	@Override
	public String getDetailedDescription() {
		return "Solver with upwinding for the advective part and stationarity preserving scheme for the rest";
	}

}
