public abstract class FluxSolverOneStep extends FluxSolverFromConservativeOnly {

	public FluxSolverOneStep(Grid grid, Equations equations, StencilTurner stencilTurner) {
		super(grid, equations, stencilTurner);
	}

	public int steps(){ return 1; }
		
	protected abstract double[] fluxPassiveScalar(GridCell g, GridEdgeMapped gridEdge, GridCell left, GridCell right, int direction);


	@Override
	public double[][][][][] sum( double[][][][][][] interfaceFluxes, double dt, double[][][][][] summedInterfaceFluxes, double[][][][] localWaveSpeeds, double [][][][][] additionalInterfaceQuantities){
		summedInterfaceFluxes = interfaceFluxes[0];
		return summedInterfaceFluxes;
	}
	
}