
public class InitialDataHydroShocktubeIsentropic extends
		InitialDataHydroShocktube {

	protected double rhoL, pL, vL, vR, rhoR, pR;
	
	public InitialDataHydroShocktubeIsentropic(Grid grid, EquationsHydroIdeal equations,
			double rhoL, double vL, double pL, double rhoR, double vR) {
		super(grid, equations);
		this.rhoL = rhoL; this.rhoR = rhoR;
		this.vL = vL; this.vR = vR;
		this.pL = pL; this.pR = pL*Math.pow(rhoL/rhoR, -equations.gamma());
	}

	@Override
	public double leftPressure() {
		return pL;
	}

	@Override
	public double leftDensity() {
		return rhoL;
	}

	@Override
	public double leftVx() {
		return vL;
	}

	@Override
	public double rightPressure() {
		return pR;
	}

	@Override
	public double rightDensity() {
		return rhoR;
	}

	@Override
	public double rightVx() {
		return vR;
	}

}
