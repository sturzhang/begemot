
public abstract class FluxSolverLagrangeProjectionTypeCartesian extends FluxSolverFromConservativeOnly {

	
	public FluxSolverLagrangeProjectionTypeCartesian(GridCartesian grid, Equations equations) {
		super(grid, equations, new StencilTurnerMultiD(grid));
	}

	@Override
	public int getNumberOfAdditionalInterfaceQuantities(){ return 1; }
	
	
	protected abstract FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep);
	
	
	@Override
	protected FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge, double[][] quantities, int timeIntegratorStep){
		return fluxmultid(gridEdge, 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJp1K], quantities[StencilTurnerMultiD.NEIGHBOUR_IJK], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJm1K], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jp1K], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1JK], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jm1K],
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJp1Kp1], quantities[StencilTurnerMultiD.NEIGHBOUR_IJKp1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJm1Kp1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jp1Kp1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1JKp1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jm1Kp1],
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJp1Km1], quantities[StencilTurnerMultiD.NEIGHBOUR_IJKm1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJm1Km1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jp1Km1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1JKm1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jm1Km1],
					timeIntegratorStep);
	}
	@Override
	public double[][][][][] sum(double[][][][][][] interfaceFluxes, double dt, double[][][][][] summedInterfaceFluxes, double[][][][] localWaveSpeeds, double [][][][][] additionalInterfaceQuantities) {
		int i,j,k;
				
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
				for (int q = 0; q < interfaceFluxes[0][i][j][k][dir].length; q++){
					summedInterfaceFluxes[i][j][k][dir][q] = interfaceFluxes[0][i][j][k][dir][q] / (1.0 + dt * additionalInterfaceQuantities[i][j][k][dir][0]);
				}
			}
		}
		return summedInterfaceFluxes;
	}

	@Override
	public int steps() {
		return 1;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	

}
