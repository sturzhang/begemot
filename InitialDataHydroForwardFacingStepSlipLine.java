import java.io.IOException;

public class InitialDataHydroForwardFacingStepSlipLine extends InitialDataHydro {

	protected double[][][] data;
	protected boolean useExact = true;
	
	public InitialDataHydroForwardFacingStepSlipLine(Grid grid, EquationsHydro equations) {
		super(grid, equations);
		try {
			VTKReader myReader = new VTKReader("forwardfacingstepdata.vtk");
			data = myReader.getData();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];

		if (useExact) {
			double x = g.getX() - 0.6;
			double y = g.getY() - grid.yMidpoint();
			
			/*double phi = Math.atan2(y, x);
			if ((phi <= 1.461247196082696) && (phi >= 0.2901342234487543)){
			res[EquationsHydroIdeal.INDEX_RHO] = 5.3767691211472375;
			res[EquationsHydroIdeal.INDEX_XMOM] = 4.3426039266523215;
			res[EquationsHydroIdeal.INDEX_YMOM] = 1.2965233496930346;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = 27.42955786201327;
			} else if ((phi <= 0.2901342234487543) && (phi >= -0.5078908123303497)){
			res[EquationsHydroIdeal.INDEX_RHO] = 6.345003101791429;
			res[EquationsHydroIdeal.INDEX_XMOM] = 10.06097910688396;
			res[EquationsHydroIdeal.INDEX_YMOM] = 3.0037955451237948;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = 34.20719572671294;
			} else if ((phi <= -0.5078908123303497) && (phi >= -2.295295023582582)){
			res[EquationsHydroIdeal.INDEX_RHO] = 4.21953990556619;
			res[EquationsHydroIdeal.INDEX_XMOM] = 7.91545713087312;
			res[EquationsHydroIdeal.INDEX_YMOM] = 4.197977757491786;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = 23.815581458078775;
			} else {
			res[EquationsHydroIdeal.INDEX_RHO] = 1.4;
			res[EquationsHydroIdeal.INDEX_XMOM] = 4.199999999999999;
			res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = 8.799999999999999;
			} */
			
			/*double phi = Math.atan2(y, x);
			if ((phi <= 1.5896982982207712) && (phi >= -0.05387624491978382)){
				res[EquationsHydroIdeal.INDEX_RHO] = 5.399310875072428;
				res[EquationsHydroIdeal.INDEX_XMOM] = 4.204286165147764;
				res[EquationsHydroIdeal.INDEX_YMOM] = -0.22673056576420736;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 27.46559383291187;
			} else if ((phi <= -0.05387624491978382) && (phi >= -0.600447807762185)){
				res[EquationsHydroIdeal.INDEX_RHO] = 6.78182362292719;
				res[EquationsHydroIdeal.INDEX_XMOM] = 12.372743991212113;
				res[EquationsHydroIdeal.INDEX_YMOM] = -0.6672426999470034;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 37.143183068022566;
			} else if ((phi <= -0.600447807762185) && (phi >= -2.521918239538141)){
				res[EquationsHydroIdeal.INDEX_RHO] = 3.1732886735150134;
				res[EquationsHydroIdeal.INDEX_XMOM] = 7.725507353102945;
				res[EquationsHydroIdeal.INDEX_YMOM] = 2.515158976321597;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 18.838100819621534;
			} else {
				res[EquationsHydroIdeal.INDEX_RHO] = 1.4;
				res[EquationsHydroIdeal.INDEX_XMOM] = 4.199999999999999;
				res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 8.799999999999999;
			}*/
			
			
			/*double phi = Math.atan2(y, x);
			if ((phi <= 1.5707963266908669) && (phi >= 2.3427038087220353E-10)){
				res[EquationsHydroIdeal.INDEX_RHO] = 3.733333333333334; // state 4
				res[EquationsHydroIdeal.INDEX_XMOM] = 2.8;
				res[EquationsHydroIdeal.INDEX_YMOM] = 4.854720966505972E-10;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 12.300000000000002;
			} else if ((phi <= 2.3427038087220353E-10) && (phi >= -1.0183244192825398)){
				res[EquationsHydroIdeal.INDEX_RHO] = 3.9966189081545562; // state 3
				res[EquationsHydroIdeal.INDEX_XMOM] = 4.22648419696998;
				res[EquationsHydroIdeal.INDEX_YMOM] = 8.07631961308175E-10;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 13.484785086729067;
			} else if ((phi <= -1.0183244192825398) && (phi >= -2.40293822763339)){
				res[EquationsHydroIdeal.INDEX_RHO] = 2.2355838265591856;
				res[EquationsHydroIdeal.INDEX_XMOM] = 3.7135867095902375;
				res[EquationsHydroIdeal.INDEX_YMOM] = 0.8319350223702184;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 8.111280382874305;
			} else {
				res[EquationsHydroIdeal.INDEX_RHO] = 1.4;
				res[EquationsHydroIdeal.INDEX_XMOM] = 2.8;
				res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 5.300000000000001;
			} //*/
			
		/*if (y > 0.01*Math.exp(-(x-0.)*(x-0.)/0.1/0.1)){
			res[EquationsHydroIdeal.INDEX_RHO] = 3.733333333333334;
			res[EquationsHydroIdeal.INDEX_XMOM] = 1.4; // 2.8; //1.4; //0 only few vortices initially; //2.8;
			res[EquationsHydroIdeal.INDEX_YMOM] = 4.854720966505972E-10;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = 4.5 / (1.4-1) + 0.5*res[EquationsHydroIdeal.INDEX_XMOM]*res[EquationsHydroIdeal.INDEX_XMOM]/
					res[EquationsHydroIdeal.INDEX_RHO]/2;// 12.300000000000002;
		} else {
			res[EquationsHydroIdeal.INDEX_RHO] = 3.9966189081545562;
			res[EquationsHydroIdeal.INDEX_XMOM] = 4.22648419696998;
			res[EquationsHydroIdeal.INDEX_YMOM] = 8.07631961308175E-10;
			res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_ENERGY] = 13.484785086729067;
		} */
			
			
			/*double phi = Math.atan2(y, x);
			if ((phi <= 1.5707963267948966) && (phi >= -1.1226575225009583E-12)){
				res[EquationsHydroIdeal.INDEX_RHO] = 4.496654275092938; // state 4
				res[EquationsHydroIdeal.INDEX_XMOM] = 3.36;
				res[EquationsHydroIdeal.INDEX_YMOM] = 4.550769295270241E-16;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 17.63866666666667;
			} else if ((phi <= -1.1226575225009583E-12) && (phi >= -0.7775807549525954)){
				res[EquationsHydroIdeal.INDEX_RHO] = 5.112649029458366; // state 3
				res[EquationsHydroIdeal.INDEX_XMOM] = 6.835446197171663;
				res[EquationsHydroIdeal.INDEX_YMOM] = -3.3852920466870273E-12;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 20.952718445155462;
			} else if ((phi <= -0.7775807549525954) && (phi >= -2.436489584588426)){
				res[EquationsHydroIdeal.INDEX_RHO] = 2.739230210996192;
				res[EquationsHydroIdeal.INDEX_XMOM] = 5.224049078847884;
				res[EquationsHydroIdeal.INDEX_YMOM] = 1.5863981374361422;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 12.080991988312164;
			} else {
				res[EquationsHydroIdeal.INDEX_RHO] = 1.4;
				res[EquationsHydroIdeal.INDEX_XMOM] = 3.36;
				res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 6.532;
			} //*/

			if (y > 0.01*Math.exp(-(x-0.)*(x-0.)/0.1/0.1)){
				res[EquationsHydroIdeal.INDEX_RHO] = 4.496654275092938; // state 4
				res[EquationsHydroIdeal.INDEX_XMOM] = 3.36;
				res[EquationsHydroIdeal.INDEX_YMOM] = 4.550769295270241E-16;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 17.63866666666667;
			} else {
				res[EquationsHydroIdeal.INDEX_RHO] = 5.112649029458366; // state 3
				res[EquationsHydroIdeal.INDEX_XMOM] = 6.835446197171663;
				res[EquationsHydroIdeal.INDEX_YMOM] = -3.3852920466870273E-12;
				res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
				res[EquationsHydroIdeal.INDEX_ENERGY] = 20.952718445155462;
			}

			
		} else {
			// here we need this grid: 0.0, 3.0, 1200, 0.0, 0.4, 160
			// or 0.0, 3.0, 600, 0.0, 0.4, 80 if you remove
			//                         this division by two
			//                              v
			//                              v
			int i = Math.max(Math.min(g.i()/2, 168), 0);
			int j = Math.max(Math.min(g.j()/2, 80), 0);
		
		
			res[EquationsHydroIdeal.INDEX_RHO] = data[i][j][0];
		
			res[EquationsHydroIdeal.INDEX_XMOM] = data[i][j][1] * res[EquationsHydroIdeal.INDEX_RHO];
			res[EquationsHydroIdeal.INDEX_YMOM] = data[i][j][2] * res[EquationsHydroIdeal.INDEX_RHO];
			double pressure = data[i][j][4];
		
			double rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    	
		
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
		}
    	return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "subset of the forward facing step flow which includes an unstable slip line";
	}

}
