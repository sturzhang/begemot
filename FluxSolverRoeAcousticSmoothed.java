import matrices.SquareMatrix;


public class FluxSolverRoeAcousticSmoothed extends FluxSolverRoe {

	protected double alpha;
	
	public FluxSolverRoeAcousticSmoothed(GridCartesian grid, EquationsAcousticSimple equations, double alpha) {
		super(grid, equations);
		this.alpha = alpha;
	}

	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		
		SquareMatrix mat[] = equations.diagonalization(i, j, k, quantities, direction);
		double waveSpeed;
		
		SquareMatrix EVal = mat[1].clone();
		mat[1].absMe();
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(mat[1].value(ii, ii), waveSpeed);
		}


		// this is actually thw firmula obtained with a wrong derivation!
		mat[1] = mat[1].mult(1+alpha/2).sub(EVal.mult(alpha)).toSquareMatrix();
		
		return new UpwindingMatrixAndWaveSpeed(
				mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix(), waveSpeed);
	}
}
