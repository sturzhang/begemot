
public class GridFittedCircle extends GridFromPoints {

	protected double innerRadius, outerRadius;
	protected int nR, nPhi;
	protected CornerPoint[][] pointsLattice;
	
	
	public GridFittedCircle(double innerRadius, double outerRadius, int nR, int nPhi, int ghostcells) {
		super(-outerRadius, outerRadius, -outerRadius, outerRadius, ghostcells);
		this.innerRadius = innerRadius;
		this.outerRadius = outerRadius;
		this.nR = nR; this.nPhi = nPhi;
		
		pointsLattice = new CornerPoint[ghostcells + nR+2][nPhi];
		// the ghostcells are on the outer radius and one on the inner radius 
		//  phi-boundary is closed topologically without boundary condition
	}

	
	/* cells:  | 0  ||  1  |  2  | ...    |  Nr  || Nr +1 ...  | Nr+g   |
	 * points: 0    1      2     3  ...   Nr     Nr+1    ... Nr+g    Nr+g+1
	 * radius:      Ri                           Ro
	 * 
	 * phi:  cells                                   ||   0  |   1   |   2  |  ...     ->
	 *                      --->     ....  |  nPhi-1 ||
	 *      points                                   0       1       2
	 *                                  nPhi-1
	 */


	@Override
	protected void savePoints() {
		double r, phi;
		
		for (int i = 0; i < pointsLattice.length; i++){
			r = innerRadius + 1.0*(i-1) / nR * (outerRadius - innerRadius);
			for (int j = 0; j < pointsLattice[i].length; j++){
				phi = 2.0 * Math.PI * j / pointsLattice[i].length;
				
				pointsLattice[i][j] = new CornerPoint(new double[] {r*Math.cos(phi), r*Math.sin(phi)});
			}
		}

	}

	protected void saveCell(int i, int j, int jOther){
		ListOfCornerPoints tmpList = new ListOfCornerPoints();
		
		tmpList.add(pointsLattice[i-1][j]);
		tmpList.add(pointsLattice[i-1][jOther]);
		tmpList.add(pointsLattice[i  ][jOther]);
		tmpList.add(pointsLattice[i  ][j]);
		
		createGridCell(tmpList, ((i <= nR+1) && (i >= 2)));
	}

	
	/* cells:  | 0  ||  1  |  2  | ...    |  Nr  || Nr +1 ...  | Nr+g   |
	 * points: 0    1      2     3  ...   Nr     Nr+1    ... Nr+g    Nr+g+1
	 * radius:      Ri                           Ro
	 */

	@Override
	protected void divideIntoCells() {
		
		for (int i = 1; i < pointsLattice.length; i++){
			saveCell(i, 0, pointsLattice[i].length-1);
			
			for (int j = 1; j < pointsLattice[i].length; j++){
				saveCell(i, j, j-1);
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Body fitted grid around a circle with " + nR + " cells between r = " + innerRadius + 
				" and r = " + outerRadius + " and " + nPhi + " angular cells.";
	}

	@Override
	protected int edgesPerCell() {
		return 4;
	}
	
	public double innerRadius(){ return innerRadius; }
	public double outerRadius(){ return outerRadius; }
	

}
