
public abstract class PassiveScalar {
	
	protected GridCartesian grid;
	protected boolean doEvolve = true;
	
	public PassiveScalar(GridCartesian grid) {
		this.grid = grid;
	}
	
	public boolean doEvolve(){
		return doEvolve;
	}
	

	
	
}
