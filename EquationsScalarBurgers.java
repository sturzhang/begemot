
public class EquationsScalarBurgers extends EquationsScalar {

	public EquationsScalarBurgers(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}

	@Override
	public double flux(int i, int j, int k, double quantity, int direction) {
		return quantity*quantity/2;
	}

	@Override
	public String getDetailedDescription() {
		return "Scalar Burgers': q^2/2 is the flux in all directions.";
	}

	@Override
	public double jacobian(int i, int j, int k, double quantity, int direction) {
		return quantity;
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		return quantities[0];
	}
	
	@Override
	public void transformFixedParameters(double[] normalVector) {
		// nothing to do
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// nothing to do
	}

}
