
public class InitialDataFourierMode extends InitialData {
		
	protected double kx, ky; 
	protected double[] qhat;
	
	public InitialDataFourierMode(GridCartesian2D grid,
			double lambdax, double lambday, double[] qhat) {
		super(grid);
		kx = (lambdax == 0 ? 0 : Math.PI*2/lambdax);
		ky = (lambday == 0 ? 0 : Math.PI*2/lambday);
		this.qhat = qhat;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];

		double x = g.getX();
		double y = g.getY();
		
		for (int q = 0; q < res.length; q++){
			res[q] = qhat[q] * Math.cos(kx * x + ky * y);
		}
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "two dimensional Fourier mode with kx = " + kx + " and ky = " + ky;
	}

}
