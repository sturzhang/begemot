public abstract class ObstacleHydro extends Obstacle {

	public ObstacleHydro(Grid grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}
	
	@Override
    public double[][][][][] insertRigidWalls(double[][][][][] interfaceFluxes, double[][][][] conservedQuantities){
		GridCell g;
		int freeIndex;
		double[] flux;
		
		
		for (GeometricGridEdge e : boundingEdges){
			g = e.getCell().gridCell();
			
			//freeIndex = nextFreeIndex[g.i()][g.j()][g.k()];
			freeIndex = interfaceFluxes[g.i()][g.j()][g.k()].length-1;
			
			flux = new double[interfaceFluxes[0][0][0][0].length];
			/*if (g.k() == 7298){
				System.err.println("flux: " + flux[1] + " " + flux[2] + " " + flux[3]);
			}//*/
			flux[EquationsHydroIdeal.INDEX_XMOM] = ((EquationsHydroIdeal) equations).getPressureFromConservative(
					conservedQuantities[g.i()][g.j()][g.k()]);
			
			flux = equations.getTransformBack(flux, e.unitNormalVector());
			/*if (g.k() == 7298){
				System.err.println("normal : " + e.unitNormalVector()[0] + " " + e.unitNormalVector()[1]);
				System.err.println("trafo flux: " + flux[1] + " " + flux[2]);
			}//*/
			
			interfaceFluxes[g.i()][g.j()][g.k()][freeIndex]    = 
					FluxAndWaveSpeed.multiply(flux,  e.edgeSize()); 
			//nextFreeIndex[g.i()][g.j()][g.k()]++;
		}
		return interfaceFluxes;
	}
	
	//@Override
    public double[][][][][] insertRigidWallsOld(double[][][][][] fluxes, double[][][][] conservedQuantities){
    	int i, j, k, momentumIndex, direction;
    	int ni, nj, nk;
    	double rhoV2, pressure;
    	
    	for (CellInterface interf : obstacleBoundary){
    		i = interf.fluxi();
			j = interf.fluxj();
			k = interf.fluxk();
			momentumIndex = interf.momentumIndex();
			direction = interf.fluxDirection();
			
			ni = interf.neighbouri();
			nj = interf.neighbourj();
			nk = interf.neighbourk();
			
			rhoV2 = (conservedQuantities[ni][nj][nk][EquationsHydroIdeal.INDEX_XMOM]*
					conservedQuantities[ni][nj][nk][EquationsHydroIdeal.INDEX_XMOM] + 
					conservedQuantities[ni][nj][nk][EquationsHydroIdeal.INDEX_YMOM]*
					conservedQuantities[ni][nj][nk][EquationsHydroIdeal.INDEX_YMOM])/
					conservedQuantities[ni][nj][nk][EquationsHydroIdeal.INDEX_RHO];
			pressure = ((EquationsHydroIdeal) equations).getPressure(conservedQuantities[ni][nj][nk][EquationsHydroIdeal.INDEX_ENERGY], rhoV2);
			
			fluxes[i][j][k][direction][EquationsHydroIdeal.INDEX_RHO] = 0;
			fluxes[i][j][k][direction][EquationsHydroIdeal.INDEX_ENERGY] = 0;
			
			fluxes[i][j][k][direction][EquationsHydroIdeal.INDEX_XMOM] = 0;
			fluxes[i][j][k][direction][EquationsHydroIdeal.INDEX_YMOM] = 0;
			fluxes[i][j][k][direction][EquationsHydroIdeal.INDEX_ZMOM] = 0;
			
			fluxes[i][j][k][direction][momentumIndex] = pressure;
			
			for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
				fluxes[i][j][k][direction][s] = 0;
			}
    	}
    	return fluxes;
    }
    
	@Override
	public void set(double[][][][] conservedQuantities) {
		int xOffset, yOffset, zOffset;
		int normalMomDirection = 0;
		int i, j, k;
		double rhoV2, pressure;
		
		for (i = 0; i < conservedQuantities.length; i++){
			for (j = 0; j < conservedQuantities[i].length; j++){
				for (k = 0; k < conservedQuantities[i][j].length; k++){
					if (excludeFromTimeEvolution[i][j][k]){
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = 0.0;
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] = 0.0;
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
						
						for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
							conservedQuantities[i][j][k][s] = 0;
						}
					}
				}
			}
		}
		
		
		//System.err.println(obstacleBoundary.size());
		for (CellInterface interf : obstacleBoundary){
			xOffset = 0; yOffset = 0; zOffset = 0;
			if (interf.interf() == ReconstructionCartesian.EAST ){ xOffset =  1; normalMomDirection = EquationsHydroIdeal.INDEX_XMOM; }
			if (interf.interf() == ReconstructionCartesian.WEST ){ xOffset = -1; normalMomDirection = EquationsHydroIdeal.INDEX_XMOM; }
			if (interf.interf() == ReconstructionCartesian.NORTH){ yOffset =  1; normalMomDirection = EquationsHydroIdeal.INDEX_YMOM; }
			if (interf.interf() == ReconstructionCartesian.SOUTH){ yOffset = -1; normalMomDirection = EquationsHydroIdeal.INDEX_YMOM; }
			if (interf.interf() == ReconstructionCartesian.FRONT){ zOffset =  1; normalMomDirection = EquationsHydroIdeal.INDEX_ZMOM; }
			if (interf.interf() == ReconstructionCartesian.BACK ){ zOffset = -1; normalMomDirection = EquationsHydroIdeal.INDEX_ZMOM; }
			
			//System.err.println(interf.i()+ " " + xOffset+ " " + interf.j()+ " " + yOffset+ " " + interf.k() +" " +zOffset);
			i = interf.i();
			j = interf.j();
			k = interf.k();
						
			conservedQuantities[i][j][k][normalMomDirection] = 0.0;
				   //-conservedQuantities[i+xOffset][j+yOffset][k+zOffset][normalMomDirection]; 
				    
				/*/ conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_RHO] *
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];*/
			
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] =
					   conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_RHO]; 
				
			
			/*rhoV2 = (conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_XMOM]*
					conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_XMOM]
					+conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_YMOM]*
					conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_YMOM])/
					conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_RHO];

			pressure = equations.getPressure(
					conservedQuantities[i+xOffset][j+yOffset][k+zOffset][EquationsHydroIdeal.INDEX_ENERGY], 
					rhoV2); */

			rhoV2 = (conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]*conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]
					+conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]*conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM])/
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];

			/*conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = equations.getEnergy(
					pressure, rhoV2);*/

			if (0.5*rhoV2 > conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY]){
				System.err.println("---_> ERROR: Boundary condition sets too high a velocity!");
			}
		}
		
	}
}
