
public class GridEdgeCartesian extends GridEdge {

	protected int i, j, k, dir;
	protected double sign, size;
	protected double[] normalVector;
	
	
	// TODO remove precomputed stuff and unused methods here!
	
	
	public GridEdgeCartesian(GridCartesian grid, GridCellCartesian adjacent1, GridCellCartesian adjacent2, double sign) {
		super(grid, adjacent1, adjacent2);
		
		
		this.sign = sign;
		
		seti();
		setj();
		setk();
		setDirAndSize();
		

		
	}
	
	public double sign(){ return sign; }
	
	//@Override
	//public double size(){ return size; }
	
	public double getInterfaceFlux(int q){
		// TODO remove this function
		//dir is used in the wrong way
	  
		 return grid.summedInterfaceFluxes[i][j][k][dir][q];
	}//*/
	
	public int i(){ return i; }
	public int j(){ return j; }
	public int k(){ return k; }
	public int dir(){ return dir; }
	
	public int normalDirection(){
		return dir;
	}
	
	// TODO this can be changed when we really go to weird grids
	public void seti(){   i = Math.max(adjacent1.i(), adjacent2.i()); }
	public void setj(){   j = Math.max(adjacent1.j(), adjacent2.j()); }
	public void setk(){   k = Math.max(adjacent1.k(), adjacent2.k()); }
	public void setDirAndSize(){ 
		double dx = ((GridCartesian) grid).xSpacing();
		double dy = 1.0; 
		double dz = 1.0;
    	
		if (((GridCartesian) grid).dimensions() >= 2){ dy = ((GridCartesian) grid).ySpacing(); }
		if (((GridCartesian) grid).dimensions() >= 3){ dz = ((GridCartesian) grid).zSpacing(); }
    	
    	size = 1.0;
		if (adjacent1.i() != adjacent2.i()){
			dir = GridCartesian.X_DIR;
			size = dy*dz;
		}
		if (adjacent1.j() != adjacent2.j()){ 
			dir = GridCartesian.X_DIR; 
			size = dx*dz;
		}
		if (adjacent1.k() != adjacent2.k()){ 
			dir = GridCartesian.X_DIR; 
			size = dx*dy;
		}
	}
		
	
}
