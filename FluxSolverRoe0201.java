import matrices.*;

public class FluxSolverRoe0201 extends FluxSolverRoeHydro {

		public FluxSolverRoe0201(Grid grid, EquationsHydroIdeal equations, int meanType) {
			super(grid, equations, meanType);
		}
				
		@Override
		protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
			double[][] mat = new double[5][5];
		
			double machParameter = 1.0; // equations.mach(); does not work fully yet

			HydroState state = new HydroState(quantities, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);

			double v2 = state.v2;
			double c = state.soundSpeed;
		    double c2 = c*c;
		    double vX = state.vX; 
		    double vY = state.vY; 
		    double vZ = state.vZ;

		    double rho = 1.0; // rho is actually not needed as it will be transformed away!
			
			double waveSpeed = c/machParameter + Math.sqrt(v2);
			
			double stabilityepsilon = 0.1;
			
			if (direction == GridCartesian.X_DIR){
				double m1n = (vX >= 0? 1.0 : -1.0); 
				
				mat[0][0] = m1n * vX + stabilityepsilon;
				mat[0][1] = -rho;
				mat[0][2] = 0.0;
				mat[0][3] = 0.0;
				mat[0][4] = 0.0;
				
				mat[1][0] = 0.0;
				mat[1][1] = Math.sqrt(v2) + stabilityepsilon;
				mat[1][2] = 0.0;
				mat[1][3] = 0.0;
				mat[1][4] = 1.0 / machParameter/machParameter/rho;
				
				mat[2][0] = 0.0;
				mat[2][1] = 0.0;
				mat[2][2] = m1n*vX + stabilityepsilon;
				mat[2][3] = 0.0;
				mat[2][4] = 0.0;
				
				mat[3][0] = 0.0;
				mat[3][1] = 0.0;
				mat[3][2] = 0.0;
				mat[3][3] = m1n*vX + stabilityepsilon;
				mat[3][4] = 0.0;
				
				mat[4][0] = 0.0;
				mat[4][1] = -c2 * rho;
				mat[4][2] = 0.0;
				mat[4][3] = 0.0;
				mat[4][4] = 2.0*c / machParameter;
			}
			if (direction == GridCartesian.Y_DIR){
				double m1n = (vY >= 0? 1.0 : -1.0); 
				
				mat[0][0] = m1n * vY + stabilityepsilon;
				mat[0][1] = 0.0;
				mat[0][2] = -rho;
				mat[0][3] = 0.0;
				mat[0][4] = 0.0; 	
				
				mat[1][0] = 0.0;
				mat[1][1] = m1n*vY + stabilityepsilon;
				mat[1][2] = 0.0;
				mat[1][3] = 0.0;
				mat[1][4] = 0.0;
				
				mat[2][0] = 0.0;
				mat[2][1] = 0.0;
				mat[2][2] = Math.sqrt(v2) + stabilityepsilon;
				mat[2][3] = 0.0;
				mat[2][4] = 1.0 / machParameter/machParameter/rho;

				mat[3][0] = 0.0;
				mat[3][1] = 0.0;
				mat[3][2] = 0.0;
				mat[3][3] = m1n*vY + stabilityepsilon;
				mat[3][4] = 0.0;
				
				mat[4][0] = 0.0;
				mat[4][1] = 0.0;
				mat[4][2] = -c2 * rho;
				mat[4][3] = 0.0;
				mat[4][4] = 2.0*c/machParameter; 
			}
			if (direction == GridCartesian.Z_DIR){
				double m1n = (vZ >= 0? 1.0 : -1.0); 
				
				mat[0][0] = m1n * vZ + stabilityepsilon;
				mat[0][1] = 0.0;
				mat[0][2] = 0.0;
				mat[0][3] = -rho;
				mat[0][4] = 0.0; 	
				
				mat[1][0] = 0.0;
				mat[1][1] = m1n*vZ + stabilityepsilon;
				mat[1][2] = 0.0;
				mat[1][3] = 0.0;
				mat[1][4] = 0.0;
				
				mat[2][0] = 0.0;
				mat[2][1] = 0.0;
				mat[2][2] = m1n*vZ + stabilityepsilon;
				mat[2][3] = 0.0;
				mat[2][4] = 0.0;

				mat[3][0] = 0.0;
				mat[3][1] = 0.0;
				mat[3][2] = 0.0;
				mat[3][3] = Math.sqrt(v2) + stabilityepsilon;
				mat[3][4] = 1.0 / machParameter/machParameter/rho;

				
				mat[4][0] = 0.0;
				mat[4][1] = 0.0;
				mat[4][2] = 0.0;
				mat[4][3] = -c2 * rho;
				mat[4][4] = 2.0*c/machParameter; 
			}
			
			return new UpwindingMatrixAndWaveSpeed(
					((EquationsHydroIdeal) equations).primToCons(vX, vY, vZ, rho, machParameter, v2, new SquareMatrix(mat)), 
					waveSpeed);
		}
		
		
	}

