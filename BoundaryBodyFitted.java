
public class BoundaryBodyFitted extends Boundary {

	protected InitialData initial;
	protected EquationsHydroIdeal equations;
	
	public BoundaryBodyFitted(GridFittedCircle grid, InitialData initial, EquationsHydroIdeal equations) {
		super(grid);
		this.initial = initial;
		this.equations = equations;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time) {
		int i, j, k;
		i = g.i(); j = g.j(); k = g.k();
		double x = g.getX();
		double y = g.getY();
		double r = ((GridFittedCircle) grid).innerRadius();
		GridCell neighbour = null;
		GridEdge edge;
		double perpAbs;
		double[] perp, par, normal;

				
		if (x*x + y*y < r*r){
			for (GridCell nb : grid.getNeighbours(g)){
				if (nb.getX()*nb.getX() + nb.getY()*nb.getY() > r*r){
					neighbour = nb;
					break;
				}
			}
			
			
			if (neighbour != null){
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 
						conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_RHO];
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = 
						conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_ENERGY];
				
				edge = new GridEdge(grid, g, neighbour);
				normal = edge.normalVector();
				
				perpAbs = normal[0]*conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_XMOM]+
						normal[1]*conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_YMOM]+
						normal[2]*conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_ZMOM];
				perp = new double[3];
				perp[0] = perpAbs * normal[0];
				perp[1] = perpAbs * normal[1];
				perp[2] = perpAbs * normal[2];
				
				par = new double[3];
				par[0] = conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_XMOM] - perp[0];
				par[1] = conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_YMOM] - perp[1];
				par[2] = conservedQuantities[neighbour.i()][neighbour.j()][neighbour.k()][EquationsHydroIdeal.INDEX_ZMOM] - perp[2];
				
				
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = par[0] - perp[0];
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] = par[1] - perp[1];
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] = par[2] - perp[2];
			}
		} else {
			conservedQuantities[i][j][k] = 
				initial.getInitialValue(new GridCell(i, j, k, grid), conservedQuantities[i][j][k].length);
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Boundary values fixed to the values there initially at the outer boundary and slip wall boundary at the inner.";
	}

}
