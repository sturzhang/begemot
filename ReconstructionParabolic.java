
public abstract class ReconstructionParabolic extends ReconstructionCartesian {

	protected boolean firsttime = true;
	protected boolean lockInterfaceValues;
	
	protected static int POSITIVE = 1, NEGATIVE = 0;
	
	public ReconstructionParabolic(boolean lockInterfaceValues) {
		this.lockInterfaceValues = lockInterfaceValues;
	}

	protected abstract void initializeParameters(int nx, int ny, int nz, int ndir, int nq);
	protected abstract double[] eval(GridCell g, int direction, int nq, double pos);
	protected abstract void setParameters(GridCell g, double[][][] interfaceValuesOrderedByDirection, double[] quantities);
	
	@Override
	public double[][][][][] perform(double[][][][] conservedQuantities, double[][][][][] interfaceValues) {
		// possibly set here interfaceValues as means of conservedquantities
		// normally here we use the condervedquantities to come up with the interfacevalues
		// however this time we use both to set internal parameters and do NOT change the interfacevalues
		
		// issue: initialization and boudaries are a problem
		
		double[][][] interfaceValuesOrderedByDirection;
		GridEdgeMapped[] allEdges;
    	double[] normal;
    	int direction = 0; // to make compiler happy
    	
		initializeParameters(conservedQuantities.length, conservedQuantities[0].length, conservedQuantities[0][0].length, 
				grid.dimensions(), conservedQuantities[0][0][0].length);
    	interfaceValuesOrderedByDirection = new double[grid.dimensions()][2][conservedQuantities[0][0][0].length];
		
	
		int i1, j1, k1, k2, j2, i2;
		
		if ((firsttime) || (lockInterfaceValues)){ 
			// setting the values of interfaceValues to be averages of the adjacent conserved values
			for (GridCell g : grid){
				allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
				for (int edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++){
					for (int q = 0; q < conservedQuantities[0][0][0].length; q++){
						i1 = allEdges[edgeIndex].adjacent1().i(); i2 = allEdges[edgeIndex].adjacent2().i();
						j1 = allEdges[edgeIndex].adjacent1().j(); j2 = allEdges[edgeIndex].adjacent2().j();
						k1 = allEdges[edgeIndex].adjacent1().k(); k2 = allEdges[edgeIndex].adjacent2().k();
			
						interfaceValues[g.i()][g.j()][g.k()][edgeIndex][q] = (conservedQuantities[i1][j1][k1][q] + 
			    					 							              conservedQuantities[i2][j2][k2][q]) / 2;
					}
				}
			}
		}	
		firsttime = false;
					
		for (GridCell g : grid){
			allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
			for (int edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++){
				normal = allEdges[edgeIndex].normalVector();
				for (int q = 0; q < conservedQuantities[0][0][0].length; q++){
					if (Math.abs(normal[0]) == 1.0){
						direction = GridCartesian.X_DIR;		    					
					} else if (Math.abs(normal[1]) == 1.0){
						direction = GridCartesian.Y_DIR;
					} else if (Math.abs(normal[2]) == 1.0){
						direction = GridCartesian.Z_DIR;
					} else {
						System.err.println("ERROR: Problem at edge normal determination (reconstruction)!");
					}
					if (normal[0] + normal[1] + normal[2] > 0){
						interfaceValuesOrderedByDirection[direction][POSITIVE] = interfaceValues[g.i()][g.j()][g.k()][edgeIndex];
					} else {
						interfaceValuesOrderedByDirection[direction][NEGATIVE] = interfaceValues[g.i()][g.j()][g.k()][edgeIndex];
					}
				}
			}
						
			setParameters(g, interfaceValuesOrderedByDirection, conservedQuantities[g.i()][g.j()][g.k()]);
		}
		
		return interfaceValues;
	}
	
	
	public double[] eval(double pos, int direction, GridEdgeMapped edge){
		double adj1 = 0, adj2 = 0; // to make compiler happy
		
		if (direction == GridCartesian.X_DIR){ adj1 = edge.adjacent1().getX(); adj2 = edge.adjacent2().getX(); }
		if (direction == GridCartesian.Y_DIR){ adj1 = edge.adjacent1().getY(); adj2 = edge.adjacent2().getY(); }
		if (direction == GridCartesian.Z_DIR){ adj1 = edge.adjacent1().getZ(); adj2 = edge.adjacent2().getZ(); }
		
		/* ISSUE: one of the adjacents can turn out to be a ghostcell.
		 * in this case we do a boundary workaround here which mimics periodic boundaries 
		 * it however also only is consistent of boundary has been chosen to be BOUNDARYPERIODIC */
		
		GridCell g;
		// TODO a bit dirty!
		if (Math.abs(pos - adj1) < Math.abs(pos - adj2)){
			g = edge.adjacent1();
		} else {
			g = edge.adjacent2();
		}
		
		if (direction == GridCartesian.X_DIR){ pos -= g.getX(); }
		if (direction == GridCartesian.Y_DIR){ pos -= g.getY(); }
		if (direction == GridCartesian.Z_DIR){ pos -= g.getZ(); }

		// --- begin of boundary workaround ----------------------------------------------------
		int nextI = g.i(), nextJ = g.j(), nextK = g.k();
		if (g.i() < ((GridCartesian) grid).indexMinX()){ nextI += ((GridCartesian) grid).nx(); }
		if (g.j() < ((GridCartesian) grid).indexMinY()){ nextJ += ((GridCartesian) grid).ny(); }
		if (g.k() < ((GridCartesian) grid).indexMinZ()){ nextK += ((GridCartesian) grid).nz(); }
							
		if (g.i() > ((GridCartesian) grid).indexMaxX()){ nextI -= ((GridCartesian) grid).nx(); }
		if (g.j() > ((GridCartesian) grid).indexMaxY()){ nextJ -= ((GridCartesian) grid).ny(); }
		if (g.k() > ((GridCartesian) grid).indexMaxZ()){ nextK -= ((GridCartesian) grid).nz(); }
		g = new GridCell(nextI, nextJ, nextK, grid);
		// --- end of boundary workaround ------------------------------------------------------
		
		return eval(g, direction, grid.conservedQuantities[0][0][0].length, pos);
	}

	@Override
	public int numberOfGhostcells() { return 1; }

	

}
