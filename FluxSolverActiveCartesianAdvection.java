public class FluxSolverActiveCartesianAdvection extends FluxSolverActiveCartesian {

	public FluxSolverActiveCartesianAdvection(GridCartesian grid, EquationsAdvectionConstant equations, 
			ReconstructionParabolic reconstruction) {
		super(grid, equations, reconstruction);
	}
	
	public FluxSolverActiveCartesianAdvection(GridCartesian grid, EquationsAdvectionConstant equations, 
			ReconstructionParabolic reconstruction, int fluxTimeIntegration) {
		super(grid, equations, reconstruction, fluxTimeIntegration);
	}

	@Override
	protected double[] forwardToEvolution(double pos, double timeInterval, Reconstruction recon, int direction, GridEdgeMapped edge) {
		return ((EquationsAdvectionConstant) equations).evolve1d(
				pos, timeInterval, (ReconstructionParabolic) reconstruction, direction, edge);
	}


}
