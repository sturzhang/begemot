
public class SourceAcousticConstantGravity extends SourceGravityAcoustic {
	
	protected double[] g;
	
	public SourceAcousticConstantGravity(GridCartesian grid, EquationsAcoustic equations, double[] g, boolean averaged) {
		super(grid, equations, averaged);
		this.g = g;
	}

	public double gy(){
		return g[GridCartesian.Y_DIR];
	}
	
	public double[] g(){ return g; }
	
	@Override
    public String getDetailedDescription(){
    	return "const gravity g = (" + g[0] + ", " + g[1] + ", " + g[2] + ")";
    }

	
	@Override
	protected double[] gravityacceleration(int i, int j, int k) {
		return g;
	}

}
