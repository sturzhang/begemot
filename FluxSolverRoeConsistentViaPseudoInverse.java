import matrices.SquareMatrix;


public class FluxSolverRoeConsistentViaPseudoInverse extends FluxSolverOneStepMultiD {

	public FluxSolverRoeConsistentViaPseudoInverse(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}
	
	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep) {
		
		int quantities = equations.getNumberOfConservedQuantities();

		
		double[] fluxDiff    = new double[quantities];
		double[] perp    = new double[quantities];
		double[] central = new double[quantities];
		double[] fluxPerpYDiff = new double[quantities];
		double[] fluxPerpZDiff = new double[quantities];
		double[] upwindingTermX = new double[quantities];
		double[] upwindingTermY = new double[quantities];
		double[] upwindingTermZ = new double[quantities];
		double[] fluxRes;
		
		double maxSpeed;
		
		boolean detailedOutput = false;
		
		SquareMatrix signJx = new SquareMatrix(quantities);
		SquareMatrix signJy = new SquareMatrix(quantities);
		SquareMatrix signJz = new SquareMatrix(quantities);
		UpwindingMatrixAndWaveSpeed tmpX, tmpY, tmpZ;
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		
		int direction = GridCartesian.X_DIR;
		int otherDirection = GridCartesian.Y_DIR;
		int thirdDirection = GridCartesian.Z_DIR;
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, ijk, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, ip1jk, direction);
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, ijp1k, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, ijm1k, direction);
		double[] fluxRightTop = equations.fluxFunction(i, j, k, ip1jp1k, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, direction);

		double[] fluxLeftKp1 = fluxLeft, fluxRightKp1 = fluxRight, fluxLeftTopKp1 = fluxLeftTop, fluxLeftBottomKp1 = fluxLeftBottom, fluxRightTopKp1 = fluxRightTop, fluxRightBottomKp1 = fluxRightBottom;
		double[] fluxLeftKm1 = fluxLeft, fluxRightKm1 = fluxRight, fluxLeftTopKm1 = fluxLeftTop, fluxLeftBottomKm1 = fluxLeftBottom, fluxRightTopKm1 = fluxRightTop, fluxRightBottomKm1 = fluxRightBottom;
		
		if (grid.dimensions() == 3){
			fluxLeftKp1        = equations.fluxFunction(i, j, k, ijkp1,     direction);
			fluxRightKp1       = equations.fluxFunction(i, j, k, ip1jkp1,   direction);
			fluxLeftTopKp1     = equations.fluxFunction(i, j, k, ijp1kp1,   direction);
			fluxLeftBottomKp1  = equations.fluxFunction(i, j, k, ijm1kp1,   direction);
			fluxRightTopKp1    = equations.fluxFunction(i, j, k, ip1jp1kp1, direction);
			fluxRightBottomKp1 = equations.fluxFunction(i, j, k, ip1jm1kp1, direction);

			fluxLeftKm1        = equations.fluxFunction(i, j, k, ijkm1,     direction);
			fluxRightKm1       = equations.fluxFunction(i, j, k, ip1jkm1,   direction);
			fluxLeftTopKm1     = equations.fluxFunction(i, j, k, ijp1km1,   direction);
			fluxLeftBottomKm1  = equations.fluxFunction(i, j, k, ijm1km1,   direction);
			fluxRightTopKm1    = equations.fluxFunction(i, j, k, ip1jp1km1, direction);
			fluxRightBottomKm1 = equations.fluxFunction(i, j, k, ip1jm1km1, direction);
		}
		
		// perpendicular fluxes: 
		//double[] fluxPerpLeftTop = equations.fluxFunction(i, j, k, ijp1k, otherDirection);
		//double[] fluxPerpLeftBottom = equations.fluxFunction(i, j, k, ijm1k, otherDirection);
		
		//double[] fluxPerpRightTop = equations.fluxFunction(i, j, k, ip1jp1k, otherDirection);
		//double[] fluxPerpRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, otherDirection);
		
		double fluxAverageLeft, fluxAverageRight, fluxPerpYDiffLeft, fluxPerpYDiffRight, fluxPerpZDiffLeft, fluxPerpZDiffRight;
		
		for (int q = 0; q < quantities; q++){ 
			fluxAverageLeft = average3Point(
					average3Point(fluxLeftTopKp1[q], fluxLeftKp1[q], fluxLeftBottomKp1[q]),
					average3Point(fluxLeftTop[q],    fluxLeft[q],    fluxLeftBottom[q]),
					average3Point(fluxLeftTopKm1[q], fluxLeftKm1[q], fluxLeftBottomKm1[q]));
			fluxAverageRight = average3Point(
					average3Point(fluxRightTopKp1[q], fluxRightKp1[q], fluxRightBottomKp1[q]),
					average3Point(fluxRightTop[q],    fluxRight[q],    fluxRightBottom[q]),
					average3Point(fluxRightTopKm1[q], fluxRightKm1[q], fluxRightBottomKm1[q]));
			
			fluxDiff[q] = fluxAverageRight - fluxAverageLeft; 
			
			fluxPerpYDiffLeft = average3Point(
					(fluxLeftTopKp1[q] - fluxLeftBottomKp1[q])/2,
					(fluxLeftTop[q]    - fluxLeftBottom[q])/2,
					(fluxLeftTopKm1[q] - fluxLeftBottomKm1[q])/2 );
			fluxPerpYDiffRight = average3Point(
					(fluxRightTopKp1[q] - fluxRightBottomKp1[q])/2,
					(fluxRightTop[q]    - fluxRightBottom[q])/2,
					(fluxRightTopKm1[q] - fluxRightBottomKm1[q])/2 );
			
			fluxPerpZDiffLeft = average3Point(
					(fluxLeftTopKp1[q] - fluxLeftTopKm1[q])/2, 
					(fluxLeftKp1[q] - fluxLeftKm1[q])/2, 
					(fluxLeftBottomKp1[q] - fluxLeftBottomKm1[q])/2);
			fluxPerpZDiffRight = average3Point(
					(fluxRightTopKp1[q] - fluxRightTopKm1[q])/2, 
					(fluxRightKp1[q] - fluxRightKm1[q])/2, 
					(fluxRightBottomKp1[q] - fluxRightBottomKm1[q])/2);
					
			fluxPerpYDiff[q] = 0.5*(fluxPerpYDiffLeft + fluxPerpYDiffRight);
			fluxPerpZDiff[q] = 0.5*(fluxPerpZDiffLeft + fluxPerpZDiffRight);
			
			// central[q] = 0.5*(fluxPerpYDiffLeft + fluxPerpYDiffRight);        WHY
			central[q] = 0.5 * (fluxAverageRight + fluxAverageLeft);
		}
		
		tmpX = getAveragedSignMatrix(g.i(), g.j(), g.k(), ijk, ip1jk, GridCartesian.X_DIR);
		signJx = tmpX.matrix();
		upwindingTermX = signJx.mult(fluxDiff);
	    maxSpeed = tmpX.waveSpeed();
		
		tmpY = getAveragedSignMatrix(g.i(), g.j(), g.k(), ijk, ip1jk, GridCartesian.Y_DIR);
		signJy = tmpY.matrix();
		upwindingTermY = signJy.mult(fluxPerpYDiff);
		maxSpeed = Math.max(tmpY.waveSpeed(), maxSpeed);
		
		if (grid instanceof GridCartesian3D){
			tmpZ = getAveragedSignMatrix(g.i(), g.j(), g.k(), ijk, ip1jk, GridCartesian.Z_DIR);
			signJz = tmpZ.matrix();
			upwindingTermZ = signJz.mult(fluxPerpZDiff);
			maxSpeed = Math.max(tmpZ.waveSpeed(), maxSpeed);
		} else {
			upwindingTermZ = new double[quantities];
		}
	    
    	fluxRes = new double[quantities];
    	
    	
    	for (int q = 0; q < quantities; q++){
    		fluxRes[q] = central[q] - upwindingTermX[q]/2 - upwindingTermY[q]/2 ; //- upwindingTermZ[q]/2;
    		if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("fluxes are NaN");
    	    	detailedOutput = true;
    	    }
    	}

    		
    	if (detailedOutput){
        	for (int q = 0; q < quantities; q++){
        	    System.err.println("left: " + ijk[q] + " right: " + ip1jk[q]
        	    					+ " res: " + fluxRes[q] + " central: " + central[q] 
        	    					+ " upwinding: " + upwindingTermX[q] + " " + upwindingTermY[q] + " " + upwindingTermZ[q]);
        	    
        	}
    	}
    	
 
    	return new FluxAndWaveSpeed(fluxRes, maxSpeed);
 	}
		
	protected SquareMatrix[] getDiagonalization(int i, int j, int k, double[] quantities, int direction){
		return equations.diagonalization(i, j, k, quantities, direction);
	}
	
	protected SquareMatrix getSignOfEigenvalues(SquareMatrix eigenvalues,
			double[] left, double[] right, int ii, int jj, int kk, int direction){
		//return eigenvalues.smoothSgnElementwise();
		
		// harten-hyman fix:
		double delta;
		SquareMatrix leftEval  = equations.diagonalization(ii, jj, kk, left,  direction)[1];
		SquareMatrix rightEval = equations.diagonalization(ii, jj, kk, right, direction)[1];
		double absEval, eval;
		SquareMatrix res = new SquareMatrix(eigenvalues.rows());
		
		int status = 1;
		
		for (int k = 0; k < eigenvalues.rows(); k++){
			eval = eigenvalues.value(k,k);
			
			delta = Math.max(Math.max(0, eigenvalues.value(k, k) - leftEval.value(k, k)),
					rightEval.value(k,k) - eigenvalues.value(k, k));
			if (eval == 0){
				if (status == 1) {status = 0;}
				res.setElement(k, k, 0);
				
				//res.setElement(k, k, delta);
			} else {
				//res.setElement(k, k, getCubicMollifiedSign(eval));
				
			    absEval = Math.abs(eval);
				res.setElement(k, k, eval / absEval); //*/
				
				
				/*if (absEval < delta){
					status = -1;
					res.setElement(k, k, delta/eval);
				} else {
					res.setElement(k, k, absEval/eval);
				} //*/
			}
		}
		
		return res;
	}
	
	
	protected double getCubicMollifiedSign(double a){
		double delta = 1e-7;
		if (a > delta){
			return 1.0;
		} else if (a >= 0){
			return -6.0*( Math.pow(a/delta, 3)/3 - Math.pow(a/delta, 2)/2);
		} else if (a > -delta){
			return -6.0*( Math.pow(a/delta, 3)/3 + Math.pow(a/delta, 2)/2);
		} else {
			return -1.0;
		}
	}
	
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		SquareMatrix[] mat = getDiagonalization(i, j, k, quantities, direction);
		double waveSpeed;
		
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("beforehand Eigenvalue #" + ii + " NaN: ");
			}
		}
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		SquareMatrix signs = getSignOfEigenvalues(mat[1], left, right, i,j,k, direction);
		
		SquareMatrix res = mat[0].mult(signs).mult(mat[2]).toSquareMatrix();
		
		
		
		return new UpwindingMatrixAndWaveSpeed(
				res, waveSpeed);
	}
		
	
	public UpwindingMatrixAndWaveSpeed getAveragedSignMatrix(int i, int j, int k, double[] left, double[] right, int direction){
		
		
		return getUpwindingMatrix(i, j, k, mean(left, right), direction, left, right);
	} 
	
	public double[] mean(double[] left, double[] right){
		double[] res = new double[left.length];
		
		for (int i = 0; i < res.length; i++){
			res[i] = 0.5 * (left[i] + right[i]);
		}
		return res;
	}
	
	protected double average3Point(double l, double m, double r){
		return 0.25*(l + m*2 + r);
	}

	@Override
	public String getDetailedDescription() {
		return "Roe solver extended to multiple dimensions with stationarity consistent diffusion";
	}



	
}
