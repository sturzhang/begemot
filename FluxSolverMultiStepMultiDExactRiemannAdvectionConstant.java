
public class FluxSolverMultiStepMultiDExactRiemannAdvectionConstant extends
		FluxSolverMultiStepMultiDExactRiemann {

	public FluxSolverMultiStepMultiDExactRiemannAdvectionConstant(GridCartesian grid, EquationsAdvectionConstant equations) {
		super(grid, equations);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] lefttop, double[] left, double[] leftbottom, 
			double[] righttop, double[] right, double[] rightbottom, int timeIntegratorStep) {
		
		int direction = GridCartesian.X_DIR;
		
		int q = 0;
		
		
		
		double vel = ((EquationsAdvectionConstant) equations).velocity()[direction];
		int otherDirection = (direction == GridCartesian.X_DIR ? GridCartesian.Y_DIR : GridCartesian.X_DIR); 
		double velortho = ((EquationsAdvectionConstant) equations).velocity()[otherDirection];
		
		double flux1d = vel/2*(left[q] + right[q]) - Math.abs(vel)/2*(right[q] - left[q]);
		
		double meanortho = righttop[q] + lefttop[q] - (rightbottom[q] + leftbottom[q]) 
				- sign(velortho) *
				(  righttop[q] + lefttop[q] - 2 * ( right[q] + left[q] ) + rightbottom[q] + leftbottom[q] );
		
		double diffortho = righttop[q] - lefttop[q] - (rightbottom[q] - leftbottom[q]) 
				- sign(velortho) *
				(  righttop[q] - lefttop[q] - 2 * ( right[q] - left[q] ) + rightbottom[q] - leftbottom[q] );
		
		double flux2d = -vel/4*(meanortho - sign(vel) * diffortho);
		
		return new FluxMultiStepAndWaveSpeed(new double[] {flux1d}, new double[] {flux2d}, vel);
	}

	@Override
	public String getDetailedDescription() {
		return "Flux obtained from an exact solution of the Riemann problems for linear advection in multi-d.";
	}

}
