import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public abstract class GridCartesian extends Grid {

	// TODO what the hell is this actually?
    public static final int Interface_LEFT  = 0;
    public static final int Interface_RIGHT = 1;

    public static final int X_DIR = 0;
    public static final int Y_DIR = 1;
    public static final int Z_DIR = 2;
    
    protected int nx, ny, nz;
    protected double xmin, xmax, ymin, ymax, zmin, zmax;
    protected int ghostcellsX, ghostcellsY, ghostcellsZ;
    
    protected double[] edgeSize;
    protected double cellSize;
    
    protected int[][][][] perpendicularDirection;
    
    protected double xSpacing, ySpacing, zSpacing;
    
    public GridCartesian(){
		super();
	}

    @Override
    public void initialize(Obstacle obstacle){
    	this.myObstacle = obstacle;
		
    	edgeSize = new double[3];
    	
    	xSpacing = (xmax - xmin) / nx;
        ySpacing = (ymax - ymin) / ny;
        zSpacing = (zmax - zmin) / nz;
        
    	double dx = (xSpacing() != 0 ? xSpacing() : 1.0);
    	double dy = (ySpacing() != 0 ? ySpacing() : 1.0);
    	double dz = (zSpacing() != 0 ? zSpacing() : 1.0);
    	
    	cellSize = dx*dy*dz;
    	
    	edgeSize[X_DIR] = cellSize/dx;
    	edgeSize[Y_DIR] = cellSize/dy;
    	edgeSize[Z_DIR] = cellSize/dz;
    	
    }
    
    // TODO remove this function
    @Override
    public LinkedList<GridEdge> getEdges(GridCell cell){
    	LinkedList<GridEdge> res = new LinkedList<GridEdge>();
    	
    	res.add(new GridEdgeCartesian(this, (GridCellCartesian) cell, new GridCellCartesian(cell.i()+1, cell.j(), cell.k(), this), +1.0));
    	res.add(new GridEdgeCartesian(this, (GridCellCartesian) cell, new GridCellCartesian(cell.i()-1, cell.j(), cell.k(), this), -1.0));
		if (dimensions() >= 2){
    		res.add(new GridEdgeCartesian(this, (GridCellCartesian) cell, new GridCellCartesian(cell.i(), cell.j()+1, cell.k(), this), +1.0));
    		res.add(new GridEdgeCartesian(this, (GridCellCartesian) cell, new GridCellCartesian(cell.i(), cell.j()-1, cell.k(), this), -1.0));
       	}
    	if (dimensions() >= 3){
    		res.add(new GridEdgeCartesian(this, (GridCellCartesian) cell, new GridCellCartesian(cell.i(), cell.j(), cell.k()+1, this), +1.0));
    		res.add(new GridEdgeCartesian(this, (GridCellCartesian) cell, new GridCellCartesian(cell.i(), cell.j(), cell.k()-1, this), -1.0));
    	}
    	
    	return res;
    }//*/
    
    @Override
    protected void setAdditionalEdgeRelationship(){
    	GridEdgeMapped[] allEdges;
    	GridEdge gridEdge;
    	int direction = 0; // to make compiler happy
    	double[] normal;
    	boolean isDirectionSet;
    	
    	perpendicularDirection = new int[interfaceValues.length][interfaceValues[0].length][interfaceValues[0][0].length][this.dimensions()*2];
    	
    	for (GridCell g : this){

    		allEdges = getAllEdges(g.i(), g.j(), g.k());
    		for (int edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++){
    			gridEdge = allEdges[edgeIndex];
    			normal = gridEdge.normalVector();
    			
    			isDirectionSet = false;
    			for (int i = 0; i <= 2; i++){
    				if (Math.abs(normal[i]) == 1.0){ 
    					direction = i; 
    					isDirectionSet = true;
    					break; 
    				}
    			}
    			if (!isDirectionSet){
    				System.err.println("ERROR: Could not determine the orientation of the edge!");
    			}
        		perpendicularDirection[g.i()][g.j()][g.k()][edgeIndex] = direction;
    		}
    	}
    }
    
    public int getDirectionPerpendicularToEdge(GridCell g, int edgeIndex){
    	return perpendicularDirection[g.i()][g.j()][g.k()][edgeIndex];
    }
    
    public int getDirectionPerpendicularToEdge(GridEdgeMapped edge){
    	return perpendicularDirection[edge.adjacent1().i()][edge.adjacent1().j()][edge.adjacent1().k()][edge.index()];
    }
    
    // TODO make this a precomputed thing
    @Override
    public LinkedList<GridCell> getNeighbours(GridCell cell){
    	LinkedList<GridCell> res = new LinkedList<GridCell>();
    	
    	if (cell.i() < this.indexMaxXIncludingGhostcells()){ res.add(new GridCellCartesian(cell.i()+1, cell.j(), cell.k(), this)); }
    	if (cell.i() > 0          ){ res.add(new GridCellCartesian(cell.i()-1, cell.j(), cell.k(), this)); }
		if (dimensions() >= 2){
			if (cell.j() < this.indexMaxYIncludingGhostcells()){ res.add(new GridCellCartesian(cell.i(), cell.j()+1, cell.k(), this)); }
			if (cell.j() > 0          ){ res.add(new GridCellCartesian(cell.i(), cell.j()-1, cell.k(), this)); }
       	}
    	if (dimensions() >= 3){
    		if (cell.k() < this.indexMaxZIncludingGhostcells()){ res.add(new GridCellCartesian(cell.i(), cell.j(), cell.k()+1, this)); }
    		if (cell.k() > 0          ){ res.add(new GridCellCartesian(cell.i(), cell.j(), cell.k()-1, this)); }
    	}
    	
    	return res;
    }
    
    
    @Override
    public double cellSize(GridCell cell){
    	return cellSize;
    }
    
    public double[] normalVector(GridEdge e){
    	return new double[]{ e.adjacent2().i() - e.adjacent1().i(),
			                 e.adjacent2().j() - e.adjacent1().j(),
                             e.adjacent2().k() - e.adjacent1().k() };
    }
    
    public double edgeSize(int normalDirection){
    	return edgeSize[normalDirection];
    }
    
    @Override
    public double edgeSize(GridEdge edge){
    	if (edge instanceof GridEdgeCartesian){
    		return edgeSize(((GridEdgeCartesian) edge).normalDirection());
    	} else {
    		// in principle this can easily happen if the GridEdge has been created somewhere else
    		     if (edge.adjacent1().i() != edge.adjacent2().i()){ return cellSize / xSpacing; } 
    		else if (edge.adjacent1().j() != edge.adjacent2().j()){ return cellSize / ySpacing; } 
    		else if (edge.adjacent1().k() != edge.adjacent2().k()){ return cellSize / zSpacing; }
    		else { System.out.println("ERROR: Unable to determine edge size!"); return 0; }
    	}
    }
    
    public Iterator<GridCell> iterator() {
        return new GridIterator();
    }
    
    private class GridIterator implements Iterator<GridCell> {
    	private int i, j, k;
    	
    	public GridIterator(){
    		i = GridCartesian.this.indexMinX();
    		j = GridCartesian.this.indexMinY();
    		k = GridCartesian.this.indexMinZ();
    	}
    	
    	public boolean hasNext() {
    		// hasNext should still answer true if it is on the last gridcell (naming confusion; better name: stillOnGrid)
            return (i <= GridCartesian.this.indexMaxX() && j <= GridCartesian.this.indexMaxY() && k <= GridCartesian.this.indexMaxZ());
        }

		@Override
		public GridCellCartesian next() {
			int oldI, oldJ, oldK;
			
			if(this.hasNext()) {
				oldI = i; oldJ = j; oldK = k;
				
				
				// TODO this can be optimized for dimensionally reduced grids
				k++;
				if (GridCartesian.this.dimensions() < 3 || k > GridCartesian.this.indexMaxZ()){
					k = GridCartesian.this.indexMinZ(); j++;
					if (GridCartesian.this.dimensions() < 2 || j > GridCartesian.this.indexMaxY()){
						j = GridCartesian.this.indexMinY(); i++;
					}
				}
				return new GridCellCartesian(oldI, oldJ, oldK, GridCartesian.this);
            }
            throw new NoSuchElementException();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}
    }
    
    public abstract String getDetailedDescription();

    
    @Override
    public String description(){ 
    	String res = "";
    	res += "# Grid used: ";
    	res += nx + " x " + ny + " x " + nz + " cells ";
    	res += "with " + xmin + " < x < " + xmax;
    	res += ", " + ymin + " < y < " + ymax;
    	res += ", " + zmin + " < z < " + zmax;
    	res += "; ghostcells in x-direction: " + ghostcellsX;
    	res += "; ghostcells in y-direction: " + ghostcellsY;
    	res += "; ghostcells in z-direction: " + ghostcellsZ;
    	
    	return "# " + res + " " + getDetailedDescription(); 
    }
    
    public void doubleResolution(){
    	nx *= 2;
    	if (dimensions() >= 2){ ny *= 2; }
    	if (dimensions() == 3){ nz *= 2; }
    }
    
    public void halvenResolution(){
    	nx /= 2;
    	if (dimensions() >= 2){ ny /= 2; }
    	if (dimensions() == 3){ nz /= 2; }
    }
    
    public int indexMaxXIncludingGhostcells(){ return nx+2*ghostcellsX-1; }
    public int indexMaxYIncludingGhostcells(){ return ny+2*ghostcellsY-1; }
    public int indexMaxZIncludingGhostcells(){ return nz+2*ghostcellsZ-1; }
    
    @Override
    public boolean[][][] generateEmptyBooleanArray(){ 
    	return new boolean[nx+2*ghostcellsX][ny+2*ghostcellsY][nz+2*ghostcellsZ]; }
    
    @Override
    public double[][][][] generateEmptyArray(int numberOfQuantities){ 
    	return new double[nx+2*ghostcellsX][ny+2*ghostcellsY][nz+2*ghostcellsZ][numberOfQuantities]; }
    
    @Override
    public double[][][][][][] generateEmptyFluxArray(int numberOfQuantities, int numberOfFluxSolverSteps){ 
    	return new double[numberOfFluxSolverSteps][nx+2*ghostcellsX+1][ny+2*ghostcellsY+1][nz+2*ghostcellsZ+1][2*dimensions()][numberOfQuantities]; }
    
    @Override
    public double[][][][][] generateEmptyFluxArray(int numberOfQuantities){ 
    	return new double[nx+2*ghostcellsX+1][ny+2*ghostcellsY+1][nz+2*ghostcellsZ+1][2*dimensions()][numberOfQuantities]; }
    
    @Override
    public double[][][][] generateEmptyWaveSpeedArray(){
    	return new double[nx+2*ghostcellsX+1][ny+2*ghostcellsY+1][nz+2*ghostcellsZ+1][2*dimensions()]; }
    
    @Override
    public double[][][][][] generateEmptyArrayForInterface(int numberOfQuantities){
    	return new double[nx+2*ghostcellsX][ny+2*ghostcellsY][nz+2*ghostcellsZ][2*dimensions()][numberOfQuantities]; }

    public abstract int dimensions();
    
    public int lastGhostCellXIndex(){ return nx + 2*ghostcellsX-1; }
    public int lastGhostCellYIndex(){ return ny + 2*ghostcellsY-1; }
    public int lastGhostCellZIndex(){ return nz + 2*ghostcellsZ-1; }
    
    
    public int indexMinX(){ return ghostcellsX; }
    public int indexMinY(){ return ghostcellsY; }
    public int indexMinZ(){ return ghostcellsZ; }
    
    public int indexMaxX(){ return ghostcellsX+nx-1; }
    public int indexMaxY(){ return ghostcellsY+ny-1; }
    public int indexMaxZ(){ return ghostcellsZ+nz-1; }
    
    @Override
    public boolean isValidCell(GridCell g){
        return isValidCell(g.i(), g.j(), g.k());
    }
    
    @Override
	public void setAllGhostcells() {
    	allGhostCells = new LinkedList<GridCell>();
    	
    	for (int i = 0; i < conservedQuantities.length; i++){
    		for (int j = 0; j < conservedQuantities[i].length; j++){
    			for (int k = 0; k < conservedQuantities[i][j].length; k++){
										
					if (!isValidCell(i,j,k)){
						allGhostCells.add(new GridCellCartesian(i, j, k, this));
					}
    			}
    		}
    	}
    }
    
    public boolean isValidCell(int i, int j, int k){
    	return ( (i >= indexMinX()) && (i <= indexMaxX()) && 
    			 (j >= indexMinY()) && (j <= indexMaxY()) && 
    			 (k >= indexMinZ()) && (k <= indexMaxZ()) );
    }
    
    public double minSpacing(){
    	double spacing = xSpacing();
    	if (dimensions() >= 2){
    		spacing = Math.min(spacing, ySpacing());
    	}
    	if (dimensions() == 3){
    		spacing = Math.min(spacing, zSpacing());
    	}
    	return spacing;
    }
    
    public double spacing(int direction){
    	if (direction == GridCartesian.X_DIR){
    		return xSpacing;
    	} else if (direction == GridCartesian.Y_DIR){
    		return ySpacing;
    	} else if (direction == GridCartesian.Z_DIR){
    		return zSpacing;
    	} else {
    		System.err.println("ERROR: Asked for grid spacing in a direction that does not exist!");
    		return 0.0;
    	}
    }
    public double xSpacing(){ return xSpacing; }
    public double ySpacing(){ return ySpacing; }
    public double zSpacing(){ return zSpacing; }
    
    
    // TODO these methods should in the long run be deleted from here and those from GridCell used
    //public double getX(int i){ return xmin + (i - ghostcellsX + 0.5) / nx * (xmax - xmin); }
    //public double getY(int j){ return ymin + (j - ghostcellsY + 0.5) / ny * (ymax - ymin); }
    //public double getZ(int k){ return zmin + (k - ghostcellsZ + 0.5) / nz * (zmax - zmin); }
    
    @Override
    public double getX(int i, int j, int k){ return xmin + (i - ghostcellsX + 0.5) / nx * (xmax - xmin); }
    @Override
    public double getY(int i, int j, int k){ return ymin + (j - ghostcellsY + 0.5) / ny * (ymax - ymin); }
    @Override
    public double getZ(int i, int j, int k){ return zmin + (k - ghostcellsZ + 0.5) / nz * (zmax - zmin); }
    
    // TODO these methods should in the long run return a GridCell
    public int getXIndex(double x){ return (int) Math.round((x - xmin)/(xmax - xmin)*nx + ghostcellsX - 0.5); }
    public int getYIndex(double y){ return (int) Math.round((y - ymin)/(ymax - ymin)*ny + ghostcellsY - 0.5); }
    public int getZIndex(double z){ return (int) Math.round((z - zmin)/(zmax - zmin)*nz + ghostcellsZ - 0.5); }
        
    
    @Override
    public double xmax(){ return xmax; }
    @Override
    public double xmin(){ return xmin; }
    @Override
    public double ymax(){ return ymax; }
    @Override
    public double ymin(){ return ymin; }
    @Override
    public double zmax(){ return zmax; }
    @Override
    public double zmin(){ return zmin; }    
    
    public double sizex(){ return xmax - xmin; }
    public double sizey(){ return ymax - ymin; }
    public double sizez(){ return zmax - zmin; }
    
    
    public int nx(){ return nx; }
    public int ny(){ return ny; }
    public int nz(){ return nz; }
    
    
    public int ghostcellsX() { return ghostcellsX; }
    public int ghostcellsY() { return ghostcellsX; }
    public int ghostcellsZ() { return ghostcellsX; }
    
}