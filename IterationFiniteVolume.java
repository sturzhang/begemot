public class IterationFiniteVolume extends Iteration {

    public TimeIntegratorFiniteVolume timeIntegrator;
    
	public SetupList setups = new SetupList();
    
    @Override
    public void run(double maxTime){
    	
    	// TODO there should be just one obstacle possible, just as there is just one grid
    	grid.initialize(setups.getFirst().obstacle());
		setups.initializeArrays(grid);
    	grid.setAllGhostcells();
    	grid.setAllEdges();
		
    	//((GridTriangularPseudoCartesianFromPoints) grid).output();
    	
    	
    	
    	// TODO remove pointvalue evolution as a necessary ingredient of a setup
		double dt = 0.0;
		time = 0.0;
		double endSystemTime, startSystemTime = System.currentTimeMillis();
		int stepCounter;
		
		setups.fillConservedQuantities();
		setups.setBoundaries(time);
		
		

    	//setDivergenceFreeVelocity();
    	
    	
		
		
		// TODO
		setups.initializeObstacle();
		
		/*// added in order to allow for initial data satisfying exact central derivative relations
		setups.setBoundaries();
		for (Setup s : setups){
			s.setExactCentralDerivative();
		}//*/
		
		
		for (Setup s : setups){ s.output().doPrint = verbose; }
		if (verbose) { 
			if (printDescriptions){
				descriptionOutput.print(getDescriptions());
				descriptionOutput.print(setups.getDescriptions());
			}
		} 
		
		if (!onlyFinalOutput) {setups.produceForcedOutput(time);} 
		
		
		
		stepCounter = 0;
		do {
			setups.setBoundaries(time);
		    //setups.setObstacle();
		    
			
			//setups.setWaveSpeeds();

		    for (int timeIntegratorStep = 1; timeIntegratorStep <= timeIntegrator.steps(); timeIntegratorStep++){
				setups.performReconstruction();

				setups.setFluxesAndWaveSpeeds(timeIntegratorStep);

				// TODO TODODT move this out of the timeIntegrator loop
				if (timeIntegratorStep == 1) { 
					dt = timeIntegrator.getDtSuggestion(setups.curMaxSignalSpeed(), grid);
					if (setups.timeTillNextOutput(time) > 1e-10){
						dt = Math.min(dt, setups.timeTillNextOutput(time));
					}
				}
				
				//moved:
				setups.setSources(time, dt, timeIntegratorStep);
				
				setups.sumFluxSolverSteps(grid, dt);
				
				setups.setObstacleViaRigidWalls();
				
				setups.performTimeIntegration(timeIntegrator, dt, timeIntegratorStep);
				setups.setBoundaries(time);
			}
		    
		    time += (Double.isNaN(dt) ? 0 : dt);
		    stepCounter++;
		    if (verbose) { System.err.println("# dt = " + dt + ";\t time = " + time + ";\t n = " + stepCounter); }
		    
		    if (stepCounter >= 0){
		    //if ((stepCounter < 100000) && (stepCounter < 100003)){
		    	
		    	//setups.produceForcedOutput(time);
		    	
		    	/*(new OutputSingleCell(0.01, grid, (EquationsHydroIdeal) setups.getFirst().equations(), 
		    			1, 1, grid.indexMinZ())).produceForced(time, setups.getFirst().conservedQuantities);
		    			//*/ 
		    }
		    
		    //setups.produceForcedOutput(time); 
		    if (!onlyFinalOutput) {setups.produceOutput(time);} 

		    
			//break;
		
		    //if (stepCounter > 1) {break;}
		} while ((time < maxTime) && !(Double.isNaN(dt)));
        
		setups.produceForcedOutput(time); 
		setups.setFinalConservedQuantities();
		
		endSystemTime = System.currentTimeMillis();
		
		System.err.println("Runtime: " + ((endSystemTime-startSystemTime)/60/1000) + " min");
		if (verbose) { System.out.println("# ... Iteration finished after " + ((endSystemTime-startSystemTime)/60/1000) + " min"); }
    }

    @Override
	public String getDescriptions(){	    
    	String res = "";
    	
    	res += grid.description() + " (" + grid.getClass().getName() + ")" + "\n";
    	res += timeIntegrator.description() + " (" + timeIntegrator.getClass().getName() + ")" + "\n";
    	
    	return res;
    }
    
    protected void setDivergenceFreeVelocity(){
    	// works for two-d grids and acoustic equations only
    	
    	for (int i = 1; i < grid.conservedQuantities.length-1; i++){
			for (int j = 2; j < grid.conservedQuantities[i].length-1; j++){
				grid.conservedQuantities[i][j][0][EquationsAcousticSimple.INDEX_VY] = 
						grid.conservedQuantities[i][j-2][0][EquationsAcousticSimple.INDEX_VY] - 
						grid.conservedQuantities[i+1][j-1][0][EquationsAcousticSimple.INDEX_VX] +
						grid.conservedQuantities[i-1][j-1][0][EquationsAcousticSimple.INDEX_VX];
			}
		}
    	
    	double testsum = 0;
    	for (int i = 1; i < grid.conservedQuantities.length-1; i++){
			for (int j = 1; j < grid.conservedQuantities[i].length-1; j++){
				testsum += Math.abs(
						grid.conservedQuantities[i+1][j][0][EquationsAcousticSimple.INDEX_VX] - 
						grid.conservedQuantities[i-1][j][0][EquationsAcousticSimple.INDEX_VX] + 
						grid.conservedQuantities[i][j+1][0][EquationsAcousticSimple.INDEX_VY] - 
						grid.conservedQuantities[i][j-1][0][EquationsAcousticSimple.INDEX_VY]
						);
			}
		}
    	System.err.println("testsum for divfree flow: " + testsum);
    	
    }
}