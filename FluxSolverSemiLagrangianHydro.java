
public class FluxSolverSemiLagrangianHydro extends FluxSolverSemiLagrangian {

	protected double[][][][] oldUStar;
	protected double[][][][] oldPiStar;
		
	protected boolean verbose = false;
	
	public FluxSolverSemiLagrangianHydro(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
		oldUStar = grid.generateEmptyWaveSpeedArray();
		oldPiStar = grid.generateEmptyWaveSpeedArray();
	}

	protected double[] interfaceValuesAndRelaxationSpeed(double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, GridEdge gridEdge){
		
		EquationsHydroIdeal eq = ((EquationsHydroIdeal) equations);
		
		double uStar, relaxationSpeed, PiStar;
		
		double uR = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double uL = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double pR = eq.getPressureFromConservative(ip1jk);
		double pL = eq.getPressureFromConservative(ijk);
		
		double cR = eq.getSoundSpeedFromConservative(ip1jk);
		double cL = eq.getSoundSpeedFromConservative(ijk);
		
		relaxationSpeed = Math.max(cR*ip1jk[EquationsHydroIdeal.INDEX_RHO], 
				cL*ijk[EquationsHydroIdeal.INDEX_RHO]);
		
		uStar = 0.5*(uR + uL) - 0.5/relaxationSpeed*(pR - pL);
		PiStar = 0.5*(pR + pL) - relaxationSpeed/2*(uR - uL);
				
		double uStarForAdvection = uStar;
		
		return new double[]{uStar, PiStar, relaxationSpeed, uStarForAdvection};
	}
	
	protected double[] getAcousticFlux(double uStar, double PiStar, int numberOfQuantities){
		double[] fluxRes = new double[numberOfQuantities];
		
		fluxRes[EquationsHydroIdeal.INDEX_RHO] = -uStar; // the flux is actually 0, but we need this to compute the compression factor
		fluxRes[EquationsHydroIdeal.INDEX_XMOM] = PiStar;
		fluxRes[EquationsHydroIdeal.INDEX_YMOM] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = PiStar*uStar;
		return fluxRes;
	}
	
	protected double[] getAdvectiveFlux(double uStar, double PiStar, double Vjp12, double Vjm12, 
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, 
			int numberOfQuantities){
		double[] fluxRes = new double[numberOfQuantities];
		
		double absUStar = Math.abs(uStar);
		double signUStar = (uStar == 0 ? 0 : uStar/absUStar);
		double V = (Vjp12 + Vjm12)/2;
		
		double velDeriv, velOrthDeriv;
		
		for (int q = 0; q < numberOfQuantities; q++){
			fluxRes[q] = uStar*(ip1jk[q] + ijk[q])/2 - absUStar*(ip1jk[q] - ijk[q])/2;
			/*fluxRes[q] = uStar*(ip1jp1k[q] + ijp1k[q] + 2.0*(ip1jk[q] + ijk[q]) + ip1jm1k[q] + ijm1k[q])/8;
			
			velDeriv = absUStar*(ip1jp1k[q] - ijp1k[q] + 2.0*(ip1jk[q] - ijk[q]) + ip1jm1k[q] - ijm1k[q])/8;
			velOrthDeriv = signUStar*V*(ip1jp1k[q] - ip1jm1k[q]  + ijp1k[q] - ijm1k[q] )/8;	
				
			fluxRes[q] -= velDeriv;
			fluxRes[q] -= velOrthDeriv; //*/
		}
		
		if (verbose){
			System.err.println("#############");
		}
		
		return fluxRes;
	}
	
	
	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		int quantities = ijk.length;
		double[] fluxRes = new double[quantities];
		double maxEVal;
		
		double relaxationSpeed;
		GridCell g = gridEdge.adjacent1();
		int indexOfThisEdge = -1;
		GridEdgeMapped[] allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
		for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
			if (gridEdge == allEdges[dir]){ indexOfThisEdge = dir; break; }
		}
		if (indexOfThisEdge == -1){
			System.err.println("ERROR: Edge not found in list!");
		}
		GridCell g2 = gridEdge.adjacent2();
		int indexOfThisEdgeInOtherCell = -1;
		allEdges = grid.getAllEdges(g2.i(), g2.j(), g2.k());
		for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
			if (gridEdge.counterpart() == allEdges[dir]){ indexOfThisEdgeInOtherCell = dir; break; }
		}
		if (indexOfThisEdgeInOtherCell == -1){
			System.err.println("ERROR: Edge in other cell not found in list!");
		}			
		
		
		double uStar, PiStar;
		
		if (timeIntegratorStep == 1){
			double[] tmp = interfaceValuesAndRelaxationSpeed(
					ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, ijp1kp1, ijkp1,
					ijm1kp1, ip1jp1kp1, ip1jkp1, ip1jm1kp1, ijp1km1, ijkm1,
					ijm1km1, ip1jp1km1, ip1jkm1, ip1jm1km1, gridEdge);
			
			uStar = tmp[0];
			PiStar = tmp[1];
			relaxationSpeed = tmp[2];
			
			
			oldUStar[g.i() ][g.j() ][g.k() ][indexOfThisEdge           ]  = uStar; //tmp[3];
			//oldUStar[g2.i()][g2.j()][g2.k()][indexOfThisEdgeInOtherCell]  = -uStar; // not needed to retrieve it in the next step, but needed for perpendicular component
			
			oldPiStar[g.i()][g.j()][g.k()][indexOfThisEdge] = PiStar;
			
			fluxRes = getAcousticFlux(uStar, PiStar, quantities);
			
		
			if ((Double.isNaN(relaxationSpeed)) || (Double.isNaN(uStar))){
				System.err.println("ERROR: acoustic flux is NaN");
			}
			
			maxEVal = relaxationSpeed/Math.min(ip1jk[EquationsHydroIdeal.INDEX_RHO], ijk[EquationsHydroIdeal.INDEX_RHO]) + Math.abs(uStar);
			
		} else if (timeIntegratorStep == 2){
			uStar = oldUStar[g.i()][g.j()][g.k()][indexOfThisEdge];
			PiStar = oldPiStar[g.i()][g.j()][g.k()][indexOfThisEdge];
			
			
			fluxRes = getAdvectiveFlux(uStar, PiStar, 0, 0, ijp1k, ijk, ijm1k, ip1jp1k,
					ip1jk, ip1jm1k, ijp1kp1, ijkp1,
					ijm1kp1, ip1jp1kp1, ip1jkp1,
					ip1jm1kp1, ijp1km1, ijkm1,
					ijm1km1, ip1jp1km1, ip1jkm1,
					ip1jm1km1, quantities);		
			
			if (Double.isNaN(uStar)){
				System.err.println("ERROR: advective flux is NaN");
			}
			maxEVal = Math.abs(uStar);
		} else {
			System.err.println("ERROR: No flux computation foreseen for timeIntegratorStep > 2!");
			maxEVal = 0;
		}
		return new FluxAndWaveSpeed(fluxRes, maxEVal);
	}
	
	protected FluxAndWaveSpeed fluxmultidForTesting(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		int quantities = ijk.length;
		double[] fluxRes = new double[quantities];
		double maxEVal;
		
		boolean experimentalAcousticTurnedOff = false;
		boolean experimentalMode              = false;
		
		if (
				(Math.abs(gridEdge.midpoint()[0] - 0.5) < 0.0001) &&
				(Math.abs(gridEdge.midpoint()[1] - 0.8) < 0.008) ){ 
			/*System.err.println(gridEdge + ";" + gridEdge.midpoint()[0] + ", " + gridEdge.midpoint()[1]);
			verbose = true; //*/ 
		} else { verbose = false; }
			
		
		//double uR, uL, pR, pL, cR, cL;
		double relaxationSpeed;
		GridCell g = gridEdge.adjacent1();
		int indexOfThisEdge = -1;
		GridEdgeMapped[] allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
		for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
			if (gridEdge == allEdges[dir]){ indexOfThisEdge = dir; break; }
		}
		if (indexOfThisEdge == -1){
			System.err.println("ERROR: Edge not found in list!");
		}
		GridCell g2 = gridEdge.adjacent2();
		int indexOfThisEdgeInOtherCell = -1;
		allEdges = grid.getAllEdges(g2.i(), g2.j(), g2.k());
		for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
			if (gridEdge.counterpart() == allEdges[dir]){ indexOfThisEdgeInOtherCell = dir; break; }
		}
		if (indexOfThisEdgeInOtherCell == -1){
			System.err.println("ERROR: Edge in other cell not found in list!");
		}			
		
		
		double uStar, PiStar;
		
		if (timeIntegratorStep == 1){
			double[] tmp = interfaceValuesAndRelaxationSpeed(
					ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, ijp1kp1, ijkp1,
					ijm1kp1, ip1jp1kp1, ip1jkp1, ip1jm1kp1, ijp1km1, ijkm1,
					ijm1km1, ip1jp1km1, ip1jkm1, ip1jm1km1, gridEdge);
			
			uStar = tmp[0];
			relaxationSpeed = tmp[2];
			
			
			oldUStar[g.i() ][g.j() ][g.k() ][indexOfThisEdge           ]  = tmp[0]; //tmp[3];
			oldUStar[g2.i()][g2.j()][g2.k()][indexOfThisEdgeInOtherCell]  = -tmp[0]; // not needed to retrieve it in the next step, but needed for perpendicular component
			
			oldPiStar[g.i()][g.j()][g.k()][indexOfThisEdge] = tmp[1];
						
			PiStar = tmp[1];
			
			
			fluxRes = getAcousticFlux(uStar, PiStar, quantities);
			
		
			if (experimentalAcousticTurnedOff){
				fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0;
				fluxRes[EquationsHydroIdeal.INDEX_XMOM] = 0;
				fluxRes[EquationsHydroIdeal.INDEX_YMOM] = 0;
				fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = 0;
				fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = 0;
			}
			
			if ((Double.isNaN(relaxationSpeed)) || (Double.isNaN(uStar))){
				System.err.println("ERROR: NaN");
			}
			
			maxEVal = relaxationSpeed/Math.min(ip1jk[EquationsHydroIdeal.INDEX_RHO], ijk[EquationsHydroIdeal.INDEX_RHO]) + Math.abs(uStar);
			
		} else if (timeIntegratorStep == 2){
			uStar = oldUStar[g.i()][g.j()][g.k()][indexOfThisEdge];
			PiStar = oldPiStar[g.i()][g.j()][g.k()][indexOfThisEdge];
			
			
			double V = 0;
			double Vjp12 = 0, Vjm12 = 0;
			
			if (experimentalMode){
				double uX, uY;
				double[] pos = gridEdge.midpoint();
				double x = pos[0] - grid.xMidpoint();
				double y = pos[1] - grid.yMidpoint();
				double r = Math.sqrt(x*x+y*y);
				if (r < 0.2){      uX = -5.0*y;           uY = 5.0*x; }
				else if (r < 0.4){ uX = -(2 - 5.0*r)*y/r; uY = (2 - 5.0*r)*x/r; }
				else {             uX = 0;                uY = 0; } //*/
			
				double uNormal = uX * gridEdge.normalVector()[0] + uY * gridEdge.normalVector()[1];
				//uStar = uNormal;
				double uPerpX = uX - uNormal * gridEdge.normalVector()[0];
				double uPerpY = uY - uNormal * gridEdge.normalVector()[1];
				//V = Math.sqrt(uPerpX*uPerpX + uPerpY*uPerpY);
				V = -uPerpX * gridEdge.normalVector()[1] + uPerpY * gridEdge.normalVector()[0];
				
				Vjp12 = V; Vjm12 = V; // TODO just to please the compiler
				
				//V = Math.signum(ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO])*V;
				//V = 0; 
			} else {
				// ########################## copy form experimental mode
				/*double uX, uY;
				double[] pos = gridEdge.midpoint();
				double x = pos[0] - grid.xMidpoint();
				double y = pos[1] - grid.yMidpoint();
				double r = Math.sqrt(x*x+y*y);
				if (r < 0.2){      uX = -5.0*y;           uY = 5.0*x; }
				else if (r < 0.4){ uX = -(2 - 5.0*r)*y/r; uY = (2 - 5.0*r)*x/r; }
				else {             uX = 0;                uY = 0; } 
				double uNormal = uX * gridEdge.normalVector()[0] + uY * gridEdge.normalVector()[1];
				double uPerpX = uX - uNormal * gridEdge.normalVector()[0];
				double uPerpY = uY - uNormal * gridEdge.normalVector()[1];
				double Vexact = -uPerpX * gridEdge.normalVector()[1] + uPerpY * gridEdge.normalVector()[0];//*/
				// ########################## end of copy
				
				int edgeCounterjp12, edgeCounterjm12, indexPerp;
				indexPerp = (gridEdge.normalVector()[0] == 1.0 ? 1 : 0);
				
				edgeCounterjp12 = 0;
				edgeCounterjm12 = 0;
				
				Vjp12 = 0;
				Vjm12 = 0;
				
				for (int edgeI = 0; edgeI <= 1; edgeI++){
					if (edgeI == 0){ g = gridEdge.adjacent1(); } else { g = gridEdge.adjacent2(); }
					allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
					for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
						if (allEdges[dir] != null){
							if (allEdges[dir].normalVector()[0] * gridEdge.normalVector()[0] + 
									allEdges[dir].normalVector()[1] * gridEdge.normalVector()[1] == 0){
								
								/*if ((allEdges[dir].midpoint()[0] - gridEdge.midpoint()[0])*allEdges[dir].normalVector()[0] + 
										(allEdges[dir].midpoint()[1] - gridEdge.midpoint()[1])*allEdges[dir].normalVector()[1] > 0){
									System.err.println(allEdges[dir].midpoint()[0] + " " + allEdges[dir].midpoint()[1]);
									System.err.println(gridEdge.midpoint()[0] + " " + gridEdge.midpoint()[1]);
									System.err.println(allEdges[dir].normalVector()[0] + " " + allEdges[dir].normalVector()[1]);*/
								if ((allEdges[dir].normalVector()[0] < 0) || (allEdges[dir].normalVector()[1] < 0)){
									
									Vjp12 += oldUStar[g.i()][g.j()][g.k()][dir]*allEdges[dir].normalVector()[indexPerp];
									edgeCounterjp12++;
								} else {
									Vjm12 += oldUStar[g.i()][g.j()][g.k()][dir]*allEdges[dir].normalVector()[indexPerp];
									edgeCounterjm12++;
								}
								
							}
						}
					}
					//if (edgeCounterjp12 + edgeCounterjm12 > 4){ System.err.println("ERROR: Too many edges!"); }

					if (edgeCounterjp12 > 2){ System.err.println("ERROR: Too many edges (p)!"); }
					if (edgeCounterjm12 > 2){ System.err.println("ERROR: Too many edges (m)!"); }
				}
				
				
				
				Vjp12 /= edgeCounterjp12;
				Vjm12 /= edgeCounterjm12;
				if (gridEdge.normalVector()[0] == 0.0){
					Vjp12 = -Vjp12;
					Vjm12 = -Vjm12;
				}
				V = (Vjp12 + Vjm12)/2;
				
				
				/*if ((V != 0) && (Vexact != 0) && (Math.signum(V) != Math.signum(Vexact))){
					V = Vexact;
				}//*/
				/*if ((V != 0) || (Vexact != 0)){
					//if (gridEdge.normalVector()[0] == 0.0){
						System.err.println(V + " " + Vexact + " " + uStar + " " + uNormal);
					//}
				}//*/
				/*if ((V != 0) && (Vexact != 0) && (Math.signum(V) != Math.signum(Vexact))){
					double emidx, emidy;
					
					emidx = gridEdge.midpoint()[0];
					emidy = gridEdge.midpoint()[1];
					
					System.err.println(V + " " + Vexact + " | " + emidx + ", " + emidy + " (" + 
							(Math.sqrt((emidx-0.5)*(emidx-0.5) + (emidy-0.5)*(emidy-0.5))) + ")");
					
					//if (emidx > 0.5){
						V = Vexact;
					//}
					
					V = 0;
					indexPerp = (gridEdge.normalVector()[0] == 1.0 ? 1 : 0);
					System.err.println(gridEdge.normalVector()[0] + " " + gridEdge.normalVector()[1]);
					if (gridEdge.normalVector()[0] == 1.0){
						System.err.println(" ");
					}
					
					edgeCounter = 0;
					
					for (int edgeI = 0; edgeI <= 1; edgeI++){
						if (edgeI == 0){ g = gridEdge.adjacent1(); } else { g = gridEdge.adjacent2(); }
						allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
						for (int dir = 0; dir < ((GridCartesian) grid).dimensions()*2; dir++){
							if (allEdges[dir] != null){
								if (allEdges[dir].normalVector()[0] * gridEdge.normalVector()[0] + 
										allEdges[dir].normalVector()[1] * gridEdge.normalVector()[1] == 0){
									
									System.err.println(allEdges[dir].normalVector()[0] + "\t" + gridEdge.normalVector()[0]);
									System.err.println(allEdges[dir].normalVector()[1] + "\t" + gridEdge.normalVector()[1]);
									System.err.println(g.i() + " " + g.j() + " " + g.k() + " " + dir);
									System.err.println(g);
									for (int ddir = 0; ddir <= 3; ddir++){
										System.err.println("> " + oldUStar[g.i()][g.j()][g.k()][ddir]);
									}
									
									// TODO it is unclear whether we maybe have to turn something around here!
									V += oldUStar[g.i()][g.j()][g.k()][dir]*allEdges[dir].normalVector()[indexPerp];
									edgeCounter++;
								}
							}
						}
						if (edgeCounter > 4){ System.err.println("ERROR: Too many edges!"); }
					}
					
				}//*/
			}
				
			
			fluxRes = getAdvectiveFlux(uStar, PiStar, Vjp12, Vjm12, ijp1k, ijk, ijm1k, ip1jp1k,
					ip1jk, ip1jm1k, ijp1kp1, ijkp1,
					ijm1kp1, ip1jp1kp1, ip1jkp1,
					ip1jm1kp1, ijp1km1, ijkm1,
					ijm1km1, ip1jp1km1, ip1jkm1,
					ip1jm1km1, quantities);		
			
			/*if (experimentalMode){
				fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = 0;
				fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0;
			}//*/
			//fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = 0;
			//fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0;
			
			if (Double.isNaN(uStar)){
				System.err.println("ERROR: NaN");
			}
			maxEVal = Math.abs(uStar);
		} else {
			System.err.println("ERROR: No flux computation foreseen for timeIntegratorStep > 2!");
			maxEVal = 0;
		}
		return new FluxAndWaveSpeed(fluxRes, maxEVal);
	}

	
	@Override
	public String getDetailedDescription() {
		return "Semi-Lagrangian hydro flux solver!";
	}

	
}
