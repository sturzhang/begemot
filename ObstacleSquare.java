
public class ObstacleSquare extends ObstacleHydro {

	protected double sideLength;	
	
	public ObstacleSquare(GridCartesian grid, EquationsHydroIdeal equations, double sideLength) {
		super(grid, equations);
		this.sideLength  = sideLength;
	}

	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {
		
		for (int i = ((GridCartesian) grid).indexMinX(); i < ((GridCartesian) grid).indexMaxX(); i++){
			for (int j = ((GridCartesian) grid).indexMinY(); j < ((GridCartesian) grid).indexMaxY(); j++){
				if (	(Math.abs(grid.getX(i,j,0) - grid.xMidpoint()) < sideLength/2) &&
						(Math.abs(grid.getY(i,j,0) - grid.yMidpoint()) < sideLength/2)     ){ 
					excludeFromTimeEvolution[i][j][((GridCartesian) grid).indexMinZ()] = true; 
				}
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Impermeable boundary in the form of a square in the x-y-plane centered on the grid and with length of a side = " + sideLength;
	}

}
