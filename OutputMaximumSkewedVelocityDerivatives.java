
public class OutputMaximumSkewedVelocityDerivatives extends Output {

	public OutputMaximumSkewedVelocityDerivatives(double outputTimeInterval, GridCartesian grid, EquationsAcousticSimple equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints the maximum of |partial_y v_x| and |partial_x v_y|";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double dyumax = 0, dxvmax = 0, dxumax = 0, dyvmax = 0;
		double maxsum = 0;
		double dyu, dxv, dyv, dxu;
		double dxuNorm = 0;
		double dyuNorm = 0;
		
    	int i, j, k;
    	int maxi = 0, maxj = 0, maxk = 0;
		boolean firstcell = true;
    	
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			dyu = (quantities[i][j+1][k][EquationsAcousticSimple.INDEX_VX]
				  -quantities[i][j-1][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2;
			dxv = (quantities[i+1][j][k][EquationsAcousticSimple.INDEX_VY]
			      -quantities[i-1][j][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2;
			dxu = (quantities[i+1][j][k][EquationsAcousticSimple.INDEX_VX]
				  -quantities[i-1][j][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).xSpacing()/2;
			dyv = (quantities[i][j+1][k][EquationsAcousticSimple.INDEX_VY]
			      -quantities[i][j-1][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).ySpacing()/2;
			
			dxuNorm += Math.abs(dxu);
			dyuNorm += Math.abs(dyu);
			
			if (firstcell){
				dyumax = Math.abs(dyu); dxvmax = Math.abs(dxv);
				dxumax = Math.abs(dxu); dyvmax = Math.abs(dyv);
				maxsum = Math.abs(dyu + dxv);
			} else {
				dyumax = Math.max(dyumax, Math.abs(dyu)); dxvmax = Math.max(dxvmax, Math.abs(dxv));
				dxumax = Math.max(dxumax, Math.abs(dxu)); dyvmax = Math.max(dyvmax, Math.abs(dyv));
				
				if (Math.abs(dyu + dxv) > maxsum){
					maxsum = Math.abs(dyu + dxv);
					maxi = i; maxj = j; maxk = k;
				}
			}
			
			firstcell = false;
		}
		dxuNorm *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing();
		dyuNorm *= ((GridCartesian) grid).xSpacing()*((GridCartesian) grid).ySpacing();
		
		//println(time + " " + dyumax + " " + dxvmax + " " + maxsum + " " + dxumax + " " + dyvmax + " " + maxi + " "  + maxj + " " + maxk);  //*/
		println(time + " " + dxuNorm + " " + dyuNorm);
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}

}
