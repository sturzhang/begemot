
public class SourceSphericalGravity extends SourceGravityHydro {

	protected double[][][][] g;
	protected double prefactor;
	
	public SourceSphericalGravity(GridCartesian grid, EquationsHydroIdeal equations, double prefactor) {
		super(grid, equations, false);
		this.prefactor = prefactor;
		
		double x, y, r;
		g = new double[grid.indexMaxX()+2][grid.indexMaxY()+2][grid.indexMaxZ()+2][3];
	
		for (int i = grid.indexMinX(); i <= grid.indexMaxX()+1; i++){
			for (int j = grid.indexMinY(); j <= grid.indexMaxY()+1; j++){
				for (int k = grid.indexMinZ(); k <= grid.indexMaxZ()+1; k++){
					x = grid.getX(i,j,k) - grid.xMidpoint();
					y = grid.getY(i,j,k) - grid.yMidpoint();
					r = Math.sqrt(x*x + y*y); 
				    g[i][j][k][0] = r > 0 ? prefactor*gravity(r)*x/r : 0.0;
				    g[i][j][k][1] = r > 0 ? prefactor*gravity(r)*y/r : 0.0;
					g[i][j][k][2] = 0.0;
				}
			}
		}
			
	}

	
	protected double gravity(double r){
		return r < 0.4 ? -r*r * (r-0.4)*(r-0.4) : 0.0;
	}
	
	public double prefactor(){ return prefactor; }
	
	
	@Override
	protected double[] gravityacceleration(int i, int j, int k) {
		return g[i][j][k];
	}

	@Override
	public String getDetailedDescription() {
		return "Spherical gravity potential in 2-d";
	}

}
