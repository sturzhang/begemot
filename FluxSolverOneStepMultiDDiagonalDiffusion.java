import matrices.SquareMatrix;


public class FluxSolverOneStepMultiDDiagonalDiffusion extends
		FluxSolverOneStepMultiDRoe {

	protected int meanType;
	
	public FluxSolverOneStepMultiDDiagonalDiffusion(GridCartesian grid, EquationsHydroIdeal equations, int meanType) {
		super(grid, equations);
		this.meanType = meanType;
	}

	@Override
	protected SquareMatrix getOrthogonalContribution(double[] left, double[] right, int direction) {
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		
		double[][] mat = new double[5][5];
		for (int i = 0; i <= 4; i++){
			for (int j = 0; j <= 4; j++){
				mat[i][j] = 0.0;
			}
		}
		
		HydroState le = new HydroState(left, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		
		double cMean, vMean, vMeanOther;
				
		cMean = usualSolver.roeMean(le.rho, ri.rho, le.soundSpeed, ri.soundSpeed);
		vMean = usualSolver.roeMean(le.rho, ri.rho, le.vX, ri.vX);
		vMeanOther = usualSolver.roeMean(le.rho, ri.rho, le.vY, ri.vY);
		
		double lambdamax = cMean + Math.abs(vMean); 
		mat[1][0] = -cMean*vMeanOther;
		mat[1][2] = lambdamax;
		
		
		return new SquareMatrix(mat);
	}

	@Override
	protected SquareMatrix getUpwindOrthogonalContribution(double[] left,
			double[] right, int direction) {
		return new SquareMatrix(left.length);
	}

	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j,
			int k, double[] left, double[] right, int direction) {
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		
		
		double[][] mat = new double[5][5];
		for (int ii = 0; ii <= 4; ii++){
			for (int jj = 0; jj <= 4; jj++){
				mat[ii][jj] = 0.0;
			}
		}
		
		HydroState le = new HydroState(left, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		HydroState ri = new HydroState(right, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
		
		double cMean, vMean, vMeanOther;
		
		cMean = usualSolver.roeMean(le.rho, ri.rho, le.soundSpeed, ri.soundSpeed);
		vMean = usualSolver.roeMean(le.rho, ri.rho, le.vX, ri.vX);
		vMeanOther = usualSolver.roeMean(le.rho, ri.rho, le.vY, ri.vY);
		
		double lambdamax = cMean + Math.abs(vMean);
		
		mat[0][0] =         Math.abs(vMean);
		mat[1][0] = -cMean*vMean;
		mat[1][1] =  lambdamax;
		mat[2][2] =         Math.abs(vMean);
		mat[3][3] =         Math.abs(vMean);
		mat[4][4] =  lambdamax;
		
		
		return new UpwindingMatrixAndWaveSpeed(new SquareMatrix(mat), lambdamax);
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction) {
		FluxSolverRoeHydro usualSolver = new FluxSolverRoeHydro((GridCartesian) grid, (EquationsHydroIdeal) equations, meanType);
		return usualSolver.averageFlux(i, j, k, left, right, direction);		
	}

	@Override
	public String getDetailedDescription() {
		return "Multi-d solver based on a Rusanov solver with non-equal diagonal entries.";
	}

}
