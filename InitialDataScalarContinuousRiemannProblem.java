
public class InitialDataScalarContinuousRiemannProblem extends
		InitialDataScalar {

	public InitialDataScalarContinuousRiemannProblem(Grid grid,
			EquationsScalar equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double x = g.getX(); // - grid.xMidpoint();
		
		double width = 1.0;
		double qL = 1.0;
		double qR = -2.0;
		double quadraticCoefficient = -10.0;
		
		return new double[] {
				x < 0 ? qL : (x > width ? qR : (
						qL + x*(qR-qL)/width + x*(x-width)*quadraticCoefficient
						))				
		};
	}

	
	// a = qR-qL/width
	@Override
	public String getDetailedDescription() {
		return "Riemann Problem :) with the two states joined by a parabola";
	}

}
