
public class EquationsAdvectionWithHydroIdeal extends EquationsAdvectionFromDifferentSetup {

	protected EquationsHydroIdeal equations;
	
	public EquationsAdvectionWithHydroIdeal(Grid grid, int numberOfPassiveScalars,
			Setup hydroSetup, EquationsHydroIdeal equations) {
		super(grid, numberOfPassiveScalars, hydroSetup);
		this.equations = equations;
	}

	@Override
	protected double[] getVelocityFromConservedQuantities(double[] conservedQuantities) {
		return equations.getVelocity(conservedQuantities);
	}

	@Override
	public String getDetailedDescription() {
		return "Passive scalar to be advected alongside with the flow of an ideal fluid.";
	}

}
