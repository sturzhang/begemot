package matrices;

public class DiagonalMatrix extends SquareMatrix {

	public DiagonalMatrix(int dimensions){
		super(dimensions);
	}
	
	public static DiagonalMatrix identity(int dim){
		double[] eval = new double[dim];
		for (int i = 0; i < dim; i++){ eval[i] = 1.0; }
		return new DiagonalMatrix(eval);
	}
	
	public DiagonalMatrix clone(){
		double[] res = new double[rows()];
		for (int i = 0; i < rows(); i++){
			res[i] = arr[i][i];
		}
		return new DiagonalMatrix(res);
	}

	
	public DiagonalMatrix(double[] eigenvalues){
		super(eigenvalues.length);
		for (int i = 0; i < rows(); i++){
			arr[i][i] = eigenvalues[i];
		}
	}
	
	public void invertMe(){
		for (int i = 0; i < rows(); i++){
			if (arr[i][i] != 0.0){
				arr[i][i] = 1.0 / arr[i][i];
			}
		}		
	}
	
	
}
