package matrices;

public class Matrix {
	
	protected double[][] arr;

	public Matrix(int rows, int columns){
		arr = new double[rows][columns];
		for (int i = 0; i < rows; i++){
			for (int j = 0; j < columns; j++){
				arr[i][j] = 0.0;
			}	
		}
	}
	
	public Matrix(double[][] arr){
		this.arr = arr;
	}
	
	public Matrix clone(){
		return new Matrix(arr);
	}
	
	public int rows(){ return arr.length; }
	public int columns(){ return arr[0].length; }
	public double value(int i, int j){ return arr[i][j]; }

	public void absMe(){
		for (int i = 0; i < rows(); i++){
			for (int j = 0; j < columns(); j++){
				arr[i][j] = Math.abs(arr[i][j]);
			}	
		}
	}
	
	@Override
	public String toString(){
		String res = "";
		for (int i = 0; i < arr.length; i++){
			if (i> 0){ res += "\n"; }
			for (int j = 0; j < arr[i].length; j++){
				if (j > 0){ res += ", "; }
				res += arr[i][j];
			}
		}
		return res;
	}

	public double sumOfSquaredElements(){
		double res = 0.0;
		for (int i = 0; i < rows(); i++){
			for (int j = 0; j < columns(); j++){
				res += arr[i][j]*arr[i][j];
			}	
		}
		return res;
	}
	
	public void print(){
		System.out.println("");
		for (int i = 0; i < rows(); i++){
			for (int j = 0; j < columns(); j++){
				System.out.print(arr[i][j] + " ");
			}	
		}
	}
	
	public boolean isNaN(){
		boolean res = false;
		for (int i = 0; i < rows(); i++){
			for (int j = 0; j < columns(); j++){
				res = (res || Double.isNaN(arr[i][j]));
			}	
		}
		return res;
	}
		
	public void transposeMe(){
		double[][] res = new double[columns()][rows()];
		for (int i = 0; i < rows(); i++){
			for (int j = 0; j < columns(); j++){
				res[j][i] = arr[i][j];
			}	
		}
		arr = res;
	}
	
	public double[][] toArray(){
		return arr;
	}

	
	public double[] mult(double[] vec){
		double[] res = new double[rows()];
	
		if (vec.length == columns()){
			for (int i = 0; i < rows(); i++){
				res[i] = 0.0;
				for (int j = 0; j < columns(); j++){
					res[i] += arr[i][j] * vec[j];
				}
			}
		} else {
			System.err.println("Wrong dimensions when multiplying a matrix and a vector!");
		}
		return res;
	}

	public Matrix mult(Matrix mat){
		double[][] res = new double[rows()][mat.columns()];
	
		if (mat.rows() == columns()){
			for (int i = 0; i < rows(); i++){
				for (int j = 0; j < mat.columns(); j++){
					res[i][j] = 0.0;
					for (int k = 0; k < columns(); k++){
						res[i][j] += arr[i][k] * mat.value(k, j);
					}
				}
			}
		} else {
			System.err.println("Wrong dimensions when multiplying two matrices!");
		}
		return new Matrix(res);
	}

	public Matrix add(Matrix mat){
		double[][] res = new double[rows()][columns()];
	
		if ((mat.rows() == rows()) && (mat.columns() == columns())){
			for (int i = 0; i < rows(); i++){
				for (int j = 0; j < columns(); j++){
					res[i][j] = arr[i][j] + mat.value(i, j);
				}
			}
		} else {
			System.err.println("Wrong dimensions when adding two matrices!");
		}
		return new Matrix(res);
	}

	public Matrix sub(Matrix mat){
		return this.add(mat.mult(-1.0));
	}
	
	public Matrix mult(double num){
		double[][] res = new double[rows()][columns()];

		for (int i = 0; i < rows(); i++){
			for (int j = 0; j < columns(); j++){
				res[i][j] = arr[i][j] * num;
			}
		}
		return new Matrix(res);
	}

	
	public void set(double[][] arr){
		this.arr = arr;
	}
	
	public SquareMatrix toSquareMatrix(){
		if (rows() == columns()){
			return new SquareMatrix(arr);
		} else {
			System.err.println("Trying to convert a non-square matrix into a square one!");
			return null;
		}
	}
	
}
