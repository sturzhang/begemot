package matrices;

public class SquareMatrix extends Matrix {

	public SquareMatrix(double[][] arr){
		super(arr);
	}
	
	public SquareMatrix(int dimensions){
		super(dimensions, dimensions);
	}
	
	public SquareMatrix clone(){
		return new SquareMatrix(arr);
	}

	public void setElement(int i, int j, double val){
		arr[i][j] = val;
	}
	
	public static SquareMatrix id(int dimensions){
		double[][] arr = new double[dimensions][dimensions];
		for (int i = 0; i < dimensions; i++){
			arr[i][i] = 1.0;
		}
		return new SquareMatrix(arr);
	}
	
	public SquareMatrix absElementwise(){
		double[][] newArr = new double[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++){
			for (int j = 0; j < arr[0].length; j++){
				newArr[i][j] = Math.abs(arr[i][j]);
			}
		}
		return new SquareMatrix(newArr);
	}

	public SquareMatrix sgnElementwise(){
		double[][] newArr = new double[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++){
			for (int j = 0; j < arr[0].length; j++){
				newArr[i][j] = (arr[i][j] == 0 ? 0 : arr[i][j]/Math.abs(arr[i][j])); 
			}
		}
		return new SquareMatrix(newArr);
	}
	
	public SquareMatrix smoothSgnElementwise(){
		double[][] newArr = new double[arr.length][arr[0].length];
		for (int i = 0; i < arr.length; i++){
			for (int j = 0; j < arr[0].length; j++){
				newArr[i][j] = smoothsign(arr[i][j]); 
			}
		}
		return new SquareMatrix(newArr);
	}
	
	protected double smoothsign(double x){
		double delta = 1e-1;
		if (x < -delta){
			return -1.0;
		} else if (x > delta){
			return 1.0;
		} else {
			return -1.0 + (x + delta)/delta; 
		}
	}
}
