
public class InitialDataHydroTornado extends InitialDataHydro {

	protected InitialDataHydroVortex vortex2d;
	protected double gz, rho0, p0;
	protected double D;
	
	public InitialDataHydroTornado(Grid grid, EquationsHydro equations, InitialDataHydroVortex vortex2d, 
			double gz, double pressurescaleheight) {
		super(grid, equations);
		
		// TODO how about a chekc here about the grid being at least 3d?
		
		this.vortex2d = vortex2d;
		p0 = vortex2d.pressure(0);
		D = pressurescaleheight;
		this.gz = gz;
		rho0 = vortex2d.rho(0);
		
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double r, x, y, z;
		double rhoV2;
    	
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		z = g.getZ();
		
		r = Math.sqrt(x*x + y*y); 
		
		res[EquationsHydroIdeal.INDEX_RHO] = rho(r, z);
    				
		if (r == 0.0){
			res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		} else {
			res[EquationsHydroIdeal.INDEX_XMOM] = -rho(r, z) * vortex2d.speed(r) * y / r;
			res[EquationsHydroIdeal.INDEX_YMOM] =  rho(r, z) * vortex2d.speed(r) * x / r;
		}
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;

		rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    				
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure(r, z), rhoV2);
    	
    	return res;	
	}

	protected double rho(double r, double z){
		// ATTENTION: the upgrade works for a 2-d vortex of constant density only (this is enforced here by evaluating at r = 0)
		return rho0 * Math.exp(-z/D - (vortex2d.pressure(r) - vortex2d.pressure(0)) / (vortex2d.rho(0) * gz * D));
	}
	
	protected double pressure(double r, double z){
		return p0 - D * gz * rho(r, z);
	}
	
	@Override
	public String getDetailedDescription() {
		return "Upgrade of the 2-d vortex described as \"" + vortex2d.getDetailedDescription() + "\" to a hydrostatic vortex tube" + 
				"with gz = " + gz + ", pressurescaleheight = " + D + ", rho(0,0) = " + rho0 + ", p0 = " + p0;
	}

}
