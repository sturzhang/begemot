
public class CornerPointPair {

	// TODO make GeometricGridEdge inherit this!
	
	protected CornerPoint p1, p2;
	
	public CornerPointPair(CornerPoint p1, CornerPoint p2) {
		this.p1 = p1;
		this.p2 = p2;
	}

	public CornerPoint point1(){ return p1; }
	public CornerPoint point2(){ return p2; }
	
}

