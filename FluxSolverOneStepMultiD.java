
public abstract class FluxSolverOneStepMultiD extends FluxSolverOneStep {

	// TODO this class should turn into a vertex flux at some point... perhaps
		
	
	public FluxSolverOneStepMultiD(GridCartesian grid, Equations equations) {
		super(grid, equations, new StencilTurnerMultiD(grid));
	}

	/*protected abstract FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k);
	//*/
	
	protected abstract FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep);
	
	
	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[][] quantities, int timeIntegratorStep){
		return fluxmultid(gridEdge, 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJp1K], quantities[StencilTurnerMultiD.NEIGHBOUR_IJK], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJm1K], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jp1K], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1JK], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jm1K],
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJp1Kp1], quantities[StencilTurnerMultiD.NEIGHBOUR_IJKp1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJm1Kp1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jp1Kp1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1JKp1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jm1Kp1],
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJp1Km1], quantities[StencilTurnerMultiD.NEIGHBOUR_IJKm1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_IJm1Km1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jp1Km1], 
					quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1JKm1], quantities[StencilTurnerMultiD.NEIGHBOUR_Ip1Jm1Km1],
					timeIntegratorStep);
	}

	
	
	// TODO this implementation of passive scalar is the same as in FluxSolverRoe -- change it to be multi-d
	@Override
	protected double[] fluxPassiveScalar(GridCell g, GridEdgeMapped gridEdge, GridCell left, GridCell right, int direction) {
		//protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right, int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double vLeft = equations.advectionVelocityForPassiveScalar(left.i(),left.j(),left.k(),
				left.interfaceValue(), direction);
		double vRight = equations.advectionVelocityForPassiveScalar(right.i(),right.j(),right.k(),
				right.interfaceValue(), direction);
		double absMeanV = 0.5 * Math.abs(vLeft + vRight);
		
		
		for (int ii = 0; ii < res.length; ii++){
			s = sc[ii];
			// TODO there was momentum mLeft and mRight standing in the flux average before
			res[ii] = 0.5*(left.interfaceValue()[s]*vLeft + right.interfaceValue()[s]*vRight) 
					- 0.5 * absMeanV * (right.interfaceValue()[s] - left.interfaceValue()[s]);
		}
		return res;
	}
	
	
	
	
}
