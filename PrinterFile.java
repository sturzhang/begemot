
public class PrinterFile extends Printer {

	protected String pathToFile;
	
	public PrinterFile(String pathToFile) {
		FileInteraction.VERBOSE = false;
		this.pathToFile = pathToFile;
		FileInteraction.writefile(pathToFile, "");
	}

	@Override
	public void print(String text) {
		FileInteraction.append(pathToFile, text);
	}

	@Override
	public void println(String text) {
		FileInteraction.append(pathToFile, "\n" + text);

	}

}
