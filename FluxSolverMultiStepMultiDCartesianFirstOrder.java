
public abstract class FluxSolverMultiStepMultiDCartesianFirstOrder extends FluxSolverMultiStepMultiDCartesian {

	
	
	public FluxSolverMultiStepMultiDCartesianFirstOrder(GridCartesian grid, Equations equations) {
		super(grid, equations, new StencilTurnerMultiD(grid));
	}

	@Override
	protected FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge,
			double[][] quantities, int timeIntegratorStep) {
		return fluxmultid(gridEdge, 
				quantities[StencilTurnerMultiD.NEIGHBOUR_LEFTTOP], quantities[StencilTurnerMultiD.NEIGHBOUR_LEFT], 
				quantities[StencilTurnerMultiD.NEIGHBOUR_LEFTBOTTOM], quantities[StencilTurnerMultiD.NEIGHBOUR_RIGHTTOP], 
				quantities[StencilTurnerMultiD.NEIGHBOUR_RIGHT], quantities[StencilTurnerMultiD.NEIGHBOUR_RIGHTBOTTOM], timeIntegratorStep);
	}
	
	protected abstract FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] lefttop, double[] left, double[] leftbottom, 
			double[] righttop, double[] right, double[] rightbottom, int timeIntegratorStep);
	
}
