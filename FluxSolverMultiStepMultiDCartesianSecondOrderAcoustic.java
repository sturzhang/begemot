
public class FluxSolverMultiStepMultiDCartesianSecondOrderAcoustic extends
		FluxSolverMultiStepMultiDCartesianSecondOrder {

	public FluxSolverMultiStepMultiDCartesianSecondOrderAcoustic(
			GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	public int steps() { return 2; }

	protected double average(double LL, double L, double C, double R, double RR){
		return (- LL + 6.0*L + 14.0*C + 6.0*R - RR)/24;		
	}
	
	protected double averageForCrossDerivative(double LL, double L, double R, double RR){
		return (LL + 5.0*L + 5.0*R + RR)/12;		
	}
	
	protected double wideSecondDerivative(double LL, double C, double RR){
		return (LL - 2.0*C + RR)/4;		
	}
	
	protected double secondDerivative(double LL, double L, double R, double RR){
		return (LL - L - R + RR)/2;		
	}
	
	protected double average3(double LL, double L, double R, double RR){
		return (LL + 3.0*L + 3.0*R + RR)/8;		
	}
	
	protected double firstDerivative(double LL, double L, double R, double RR){
		return (-LL - L + R + RR)/4;		
	}
	
	
	
	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LLTT, double[] LLT, double[] LL, double[] LLB,
			double[] LLBB, double[] LTT, double[] LT, double[] L, double[] LB,
			double[] LBB, double[] RTT, double[] RT, double[] R, double[] RB,
			double[] RBB, double[] RRTT, double[] RRT, double[] RR,
			double[] RRB, double[] RRBB) {
		int qq = L.length;
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		
		if (((EquationsAcousticSimple) equations).symmetric()){
			System.err.println("ERROR: Consistent second order solver implemented only for asymmetric acoustic system!");
		}
		
		// TODO doe snot really work on non-square grids
		double dx = ((GridCartesian) grid).xSpacing();
			
				
		double[] average1 = new double[qq]; 
		double[] average2par = new double[qq]; double[] average2perp = new double[qq];
		
		
		double[] upwinding1par = new double[qq]; double[] upwinding1perp = new double[qq];
		double[] upwinding2par = new double[qq]; double[] upwinding2perp1 = new double[qq]; double[] upwinding2perp2 = new double[qq];
		
		for (int q = 0; q < qq; q++){
			average1[q] = average(
				(-LLTT[q] + 7.0*LTT[q] + 7.0*RTT[q] - RRTT[q]),
				(-LLT[q]  + 7.0*LT[q]  + 7.0*RT[q]  - RRT[q]),
				(-LL[q]   + 7.0*L[q]   + 7.0*R[q]   - RR[q]),
				(-LLB[q]  + 7.0*LB[q]  + 7.0*RB[q]  - RRB[q]),
				(-LLBB[q] + 7.0*LBB[q] + 7.0*RBB[q] - RRBB[q])
				)/12;
			average2par[q] = average( 
					(-LLTT[q] - 3.0*LTT[q] + 3.0*RTT[q] + RRTT[q]), 
					(-LLT[q]  - 3.0*LT[q]  + 3.0*RT[q]  + RRT[q]),
					(-LL[q]   - 3.0*L[q]   + 3.0*R[q]   + RR[q]),
					(-LLB[q]  - 3.0*LB[q]  + 3.0*RB[q]  + RRB[q]),
					(-LLBB[q] - 3.0*LBB[q] + 3.0*RBB[q] + RRBB[q])
					)/dx/6;
			
			average2perp[q] = averageForCrossDerivative(
					(LLBB[q] - 8.0*LLB[q] + 8.0*LLT[q] - LLTT[q]),
					(LBB[q] - 8.0*LB[q] + 8.0*LT[q] - LTT[q]),
					(RBB[q] - 8.0*RB[q] + 8.0*RT[q] - RTT[q]),
					(RRBB[q] - 8.0*RRB[q] + 8.0*RRT[q] - RRTT[q])
					)/dx/12;
			
			upwinding1par[q] = average(
					(-LLTT[q] + 3.0*LTT[q] - 3.0*RTT[q] + RRTT[q]),
					(-LLT[q]  + 3.0*LT[q]  - 3.0*RT[q]  + RRT[q]),
					(-LL[q]   + 3.0*L[q]   - 3.0*R[q]   + RR[q]),
					(-LLB[q]  + 3.0*LB[q]  - 3.0*RB[q]  + RRB[q]),
					(-LLBB[q] + 3.0*LBB[q] - 3.0*RBB[q] + RRBB[q])
					)
					+
					wideSecondDerivative(
							(-LLTT[q] - LTT[q] + RTT[q] + RRTT[q]),
							(-LL[q]   - L[q]   + R[q]   + RR[q]),
							(-LLBB[q] - LBB[q] + RBB[q] + RRBB[q])
					)/4;
			
			upwinding1perp[q] = secondDerivative(
					(LLBB[q] - 8.0*LLB[q] + 8.0*LLT[q] - LLTT[q]),
					(LBB[q]  - 8.0*LB[q]  + 8.0*LT[q]  - LTT[q]),
					(RBB[q]  - 8.0*RB[q]  + 8.0*RT[q]  - RTT[q]),
					(RRBB[q] - 8.0*RRB[q] + 8.0*RRT[q] - RRTT[q])
					)/12
					+
					average3(
					(-LLBB[q] + 2.0*LLB[q] - 2.0*LLT[q] + LLTT[q]),
					(-LBB[q]  + 2.0*LB[q]  - 2.0*LT[q]  + LTT[q]),
					(-RBB[q]  + 2.0*RB[q]  - 2.0*RT[q]  + RTT[q]),
					(-RRBB[q] + 2.0*RRB[q] - 2.0*RRT[q] + RRTT[q])
					)/2;
			
			upwinding2par[q] = average(
					(LLTT[q] - LTT[q] - RTT[q] + RRTT[q]),
					(LLT[q] - LT[q] - RT[q] + RRT[q]),
					(LL[q] - L[q] - R[q] + RR[q]),
					(LLB[q] - LB[q] - RB[q] + RRB[q]),
					(LLBB[q] - LBB[q] - RBB[q] + RRBB[q])
					)/2;
			
			upwinding2perp1[q] = wideSecondDerivative(
					(-LLBB[q] + 7.0*LBB[q] + 7.0*RBB[q] - RRBB[q]),
					(-LL[q] + 7.0*L[q] + 7.0*R[q] - RR[q]),
					(-LLTT[q] + 7.0*LTT[q] + 7.0*RTT[q] - RRTT[q])
					)/12;
			
			upwinding2perp2[q] = firstDerivative(
					(LLBB[q] - 8.0*LLB[q] + 8.0*LLT[q] - LLT[q]), 
					(LBB[q] - 8.0*LB[q] + 8.0*LT[q] - LT[q]),
					(RBB[q] - 8.0*RB[q] + 8.0*RT[q] - RT[q]),
					(RRBB[q] - 8.0*RRB[q] + 8.0*RRT[q] - RRT[q])
					)/12;
		}
		
		
		double[] flux1 = new double[qq];
		double[] flux2 = new double[qq];
		
		flux1[EquationsAcousticSimple.INDEX_VX] = average1[EquationsAcousticSimple.INDEX_P] 
				+ c/12*(upwinding1par[EquationsAcousticSimple.INDEX_VX] + upwinding1perp[EquationsAcousticSimple.INDEX_VY]); 
		flux1[EquationsAcousticSimple.INDEX_VY] = 0;
		flux1[EquationsAcousticSimple.INDEX_P]  = average1[EquationsAcousticSimple.INDEX_VX]*c*c
				+ c/12*upwinding1par[EquationsAcousticSimple.INDEX_P];
		
		flux2[EquationsAcousticSimple.INDEX_VX] = - 0.5*c*c * (average2par[EquationsAcousticSimple.INDEX_VX] + average2perp[EquationsAcousticSimple.INDEX_VY])
				+ c/6/dx*(upwinding2par[EquationsAcousticSimple.INDEX_P] + upwinding2perp1[EquationsAcousticSimple.INDEX_P]);
		flux2[EquationsAcousticSimple.INDEX_VY] = 0;
		flux2[EquationsAcousticSimple.INDEX_P]  = - 0.5*c*c * average2par[EquationsAcousticSimple.INDEX_P]
				+ c*c*c/6/dx*(upwinding2par[EquationsAcousticSimple.INDEX_VX] + upwinding2perp2[EquationsAcousticSimple.INDEX_VY]);
		
		
		return new FluxMultiStepAndWaveSpeed(flux1, flux2, c);
	}

	
	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep) {
		return Math.pow(dt, fluxSolverStep);
	}
	
	
	@Override
	public String getDetailedDescription() {
		return "Second order version of a consistent flux for the acoustic equations";
	}

	
}
