import matrices.SquareMatrix;


public class FluxSolverRusanov extends FluxSolverRoe {

	public FluxSolverRusanov(Grid grid, Equations equations) {
		super(grid, equations);
	}
	
	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, 
			double maxEigenvalueSuggestion, double minEigenvalueSuggestion, 
			double[] left, double[] right){
		
		SquareMatrix[] matleft = getDiagonalization(i, j, k, left, direction);
		SquareMatrix[] matright = getDiagonalization(i, j, k, right, direction);
		double waveSpeed;
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < matleft[1].rows(); ii++){
			waveSpeed = Math.max( matleft[1].value(ii, ii), waveSpeed);
			waveSpeed = Math.max(matright[1].value(ii, ii), waveSpeed);
		}

		for (int ii = 0; ii < matleft[1].rows(); ii++){
			matleft[1].setElement(ii, ii, waveSpeed);
		}
			
		return new UpwindingMatrixAndWaveSpeed(
				matleft[1], waveSpeed);
	}

}
