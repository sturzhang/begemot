
public class InitialDataHydroVortexInteraction extends InitialDataHydro {

	protected InitialDataHydroGresho myGresho;
	protected double driftVxmax, mach;
	
	public InitialDataHydroVortexInteraction(GridCartesian grid, EquationsHydroIdeal equations, double rho, double mach, double driftVxmax) {
		super(grid, equations);
		myGresho = new InitialDataHydroGresho(grid, equations, mach, rho, rho);
		this.driftVxmax = driftVxmax;
		this.mach = mach;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double x = g.getX() - grid.xmin();
		
		if (grid.xmax() - grid.xmin() < 2.0){
			System.err.println("Too small grid for this problem!!");
		}
		
		int icenter;
		double[] res = new double[qq];
		
		if (x < 0.5){
			icenter = ((GridCartesian) grid).getXIndex(0.25 + grid.xmin());
			res = myGresho.getInitialValue(new GridCell(g.i() - icenter + ((GridCartesian) grid).getXIndex(grid.xMidpoint()), g.j(), g.k(), grid), qq);
		} else {
			icenter = ((GridCartesian) grid).getXIndex(0.75 + grid.xmin());
			res = myGresho.getInitialValue(new GridCell(g.i() - icenter + ((GridCartesian) grid).getXIndex(grid.xMidpoint()), g.j(), g.k(), grid), qq);
		}
				
		if (x < 1.0){
			res[EquationsHydroIdeal.INDEX_XMOM] += driftVxmax * Math.pow(x - 1.0, 2);
		}
		
		for (int s = 5; s < qq; s++){
			// passive scalars
			res[s] = g.getY() - grid.yMidpoint();			
		}
		return res;		
	}

	@Override
	public String getDetailedDescription() {
		return "Two Mach: " + mach + " vortices with relative speed that decreases from " + driftVxmax + " to 0 at x = 1";
	}

}
