
public class OutputMaxMachNumber extends Output {
		
	public OutputMaxMachNumber(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
    	return "prints maximum value of local mach number over time";
	}

	
	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double soundspeed, rhoV2, pressure;
		double maxmachnumber = 0.0;
		
		for (int i = 0; i < quantities.length; i++){
			for (int j = 0; j < quantities[i].length; j++){
				for (int k = 0; k < quantities[i][j].length; k++){

					rhoV2 = (Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM], 2) + 
								Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM], 2) +
								Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM], 2)) / 
								quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
					pressure = ((EquationsHydroIdeal) equations).getPressure(quantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY], rhoV2);
					soundspeed = ((EquationsHydroIdeal) equations).getSoundSpeedFromPressure(pressure, quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]);
										
					maxmachnumber = Math.max(maxmachnumber, 
							Math.sqrt(rhoV2 / quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]) / soundspeed);
					
					
				}
			}
		}
		
		println(time + " " + maxmachnumber);

	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
