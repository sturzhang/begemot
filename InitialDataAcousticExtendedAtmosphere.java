import java.util.Random;


public class InitialDataAcousticExtendedAtmosphere extends InitialDataAcousticExtended {

	protected double gamma, K, rhobottom, gy;	
	
	public InitialDataAcousticExtendedAtmosphere(Grid grid,
			EquationsAcousticExtended equations, double gy, double K, double polytropicExponent, double rhobottom) {
		super(grid, equations);
		this.gamma = polytropicExponent;
		this.K = K;
		this.gy = gy;
		this.rhobottom = rhobottom;
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		Random random = new Random();
		double[] res = new double[q];
		
		double vy = 0.234;
		
		res[EquationsAcousticExtended.INDEX_RHO] = getDensity(g.getY());
		res[EquationsAcousticSimple.INDEX_P] = getPressure(res[EquationsAcousticExtended.INDEX_RHO]);
    				
		//if (randomPerturbation){
		//	res[EquationsAcousticSimple.INDEX_VX] = (random.nextDouble()-0.5)*1e-10;
		//} else {
			res[EquationsAcousticSimple.INDEX_VX] = 0.0;
		//}
		res[EquationsAcousticSimple.INDEX_VY] = vy;
		res[EquationsAcousticSimple.INDEX_VZ] = 0.0;
		
		return res;
	}

	protected double getDensity(double y){
		if (gamma == 1.0){
			return rhobottom*Math.exp(gy * y / gamma / K);
		} else {
			return Math.pow((gamma-1)*gy*y/gamma/K + Math.pow(rhobottom, gamma-1), 1.0/(gamma-1));
		}
	}
	
	protected double getPressure(double rho){
		return K*Math.pow(rho, gamma);
	}
	
	@Override
	public String getDetailedDescription() {
		return "Polytropic atmosphere for extended linear acoustic system with K = " + K  + " and gamma = " + gamma;
	}

}
