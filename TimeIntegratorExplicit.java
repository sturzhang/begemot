
public abstract class TimeIntegratorExplicit extends TimeIntegratorFiniteVolume {

	protected double CFL;
	
	public TimeIntegratorExplicit(Grid grid, double CFL) {
		super(grid);
		this.CFL = CFL;
	}

	public double getDtSuggestion(double maxSignalSpeed, Grid grid) {
		return CFL * grid.minSpacing() / maxSignalSpeed;
	}
	
    public String getDetailedDescription(){
    	return "Explicit time integrator with CFL = " + CFL;
    }


}
