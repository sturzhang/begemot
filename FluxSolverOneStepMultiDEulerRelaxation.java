
public class FluxSolverOneStepMultiDEulerRelaxation extends FluxSolverOneStepMultiD {

	public FluxSolverOneStepMultiDEulerRelaxation(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		
		double[] fluxRes = new double[equations.getNumberOfConservedQuantities()];
		
		double uip1j   = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double uij     = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double uip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double uijp1   = ijp1k[EquationsHydroIdeal.INDEX_XMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double uip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double uijm1   = ijm1k[EquationsHydroIdeal.INDEX_XMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
		
		double vij   = ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double vip1j   = ip1jk[EquationsHydroIdeal.INDEX_YMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		
		double vip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double vijp1   = ijp1k[EquationsHydroIdeal.INDEX_YMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double vip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double vijm1   = ijm1k[EquationsHydroIdeal.INDEX_YMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
				
		EquationsHydroIdeal eq = ((EquationsHydroIdeal) equations);
		
		double pip1j   = eq.getPressureFromConservative(ip1jk);
		double pij     = eq.getPressureFromConservative(ijk);
		double pip1jp1 = eq.getPressureFromConservative(ip1jp1k);
		double pijp1   = eq.getPressureFromConservative(ijp1k);
		double pip1jm1 = eq.getPressureFromConservative(ip1jm1k);
		double pijm1   = eq.getPressureFromConservative(ijm1k);
		
		double cR = eq.getSoundSpeedFromConservative(ip1jk);
		double cL = eq.getSoundSpeedFromConservative(ijk);
		
		double rhoL = ijk[ EquationsHydroIdeal.INDEX_RHO];
		double rhoR = ip1jk[EquationsHydroIdeal.INDEX_RHO];
		
		double diffP = 0.25*(pip1jp1 - pijp1 + 2.0*(pip1j - pij) + pip1jm1 - pijm1);
		double diffU = 0.25*(uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1);
		
		double diffV = 0.25*(vijp1 - vijm1 + vip1jp1 - vip1jm1);
		
		
		double cMin = (cL + cR)/2; // what shall i use here?
		double aL, aR;
		if (pip1j > pij){
			aL = rhoL * (Math.max(cL, cMin) + 
					Math.max(0, (eq.gamma()+1)/2 * ((pip1j - pij)/rhoR/cR - (diffU + diffV)))
					); // what does subscript plus mean?
			aR = rhoR * (Math.max(cR, cMin) + 
					Math.max(0, (eq.gamma()+1)/2 * (-(pip1j - pij)/aL - (diffU + diffV)))); // what does subscript plus mean?
		} else {
			aR = rhoR * (Math.max(cR, cMin) + 
					Math.max(0, (eq.gamma()+1)/2 * (-(pip1j - pij)/rhoL/cL - (diffU + diffV)))); // what does subscript plus mean?
			aL = rhoL * (Math.max(cL, cMin) + 
					Math.max(0, (eq.gamma()+1)/2 * ((pip1j - pij)/aR - (diffU + diffV)))); // what does subscript plus mean?
		}
		
		//double maxEVal = relaxationSpeed/Math.min(ip1jk[EquationsHydroIdeal.INDEX_RHO], ijk[EquationsHydroIdeal.INDEX_RHO]) + Math.abs(uStar);
		//double maxEVal = Math.max(cR, cL) + Math.max(Math.abs(uij), Math.abs(uip1j)); //+ Math.abs(uStar);
		double maxEVal = Math.max(aL/rhoL, aR/rhoR) + Math.max(Math.abs(uij), Math.abs(uip1j));
				
		
		
		//double sumP  = 0.25*(pip1jp1 + pijp1 + 2.0*(pip1j + pij) + pip1jm1 + pijm1);
		//double sumU  = 0.25*(uip1jp1 + uijp1 + 2.0*(uip1j + uij) + uip1jm1 + uijm1);
		
		double uStar, PiStar;
		
		uStar  = (aL * uij + aR * uip1j - diffP) / (aL + aR);
		PiStar = (aR * pij + aL * pip1j - aL * aR * (diffU+diffV)) / (aL + aR);
		
		/*fluxRes[EquationsHydroIdeal.INDEX_RHO] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_XMOM] = PiStar;
		fluxRes[EquationsHydroIdeal.INDEX_YMOM] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ZMOM] = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = PiStar*uStar;
		*/
		
		
		double epsL = ijk[ EquationsHydroIdeal.INDEX_ENERGY]/rhoL - 0.5*(uij*uij + vij*vij);
		double epsR = ip1jk[EquationsHydroIdeal.INDEX_ENERGY]/rhoR - 0.5*(uip1j*uip1j + vip1j*vip1j);
		
		
		double rhoStar, epsStar, vStar;
		
		double diffPForCompression = diffP;
		
		if (uStar > 0){
			rhoStar = 1.0 / (1.0/rhoL + (aR * (diffU+diffV) - diffPForCompression) / aL / (aL + aR));
			epsStar = epsL + (PiStar*PiStar - pij*pij) / 2 / aL / aL;
			vStar = vij;
		} else {
			rhoStar = 1.0 / (1.0/rhoR + (aL * (diffU+diffV) + diffPForCompression) / aR / (aL + aR));
			epsStar = epsR + (PiStar*PiStar - pip1j*pip1j) / 2 / aR / aR;
			vStar = vip1j;
		}
		
		double eStar = rhoStar * epsStar + 0.5*rhoStar * (uStar*uStar + vStar*vStar);
		
		fluxRes[EquationsHydroIdeal.INDEX_RHO ]   += rhoStar*uStar;
		fluxRes[EquationsHydroIdeal.INDEX_XMOM]   += rhoStar*uStar*uStar + PiStar;
		fluxRes[EquationsHydroIdeal.INDEX_YMOM]   += rhoStar*uStar * vStar;
		fluxRes[EquationsHydroIdeal.INDEX_ENERGY] += (eStar + PiStar)*uStar;
		
		return new FluxAndWaveSpeed(fluxRes, maxEVal);
		
	}

	@Override
	public String getDetailedDescription() {
		return "Relaxation solver for Euler from Boileau Chalons Massot / Bouchut";
	}

}
