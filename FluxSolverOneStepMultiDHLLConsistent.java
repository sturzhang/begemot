
public class FluxSolverOneStepMultiDHLLConsistent extends FluxSolverOneStepMultiD {

	protected boolean averageRoe = true;

	
	public FluxSolverOneStepMultiDHLLConsistent(GridCartesian grid, EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "Stationarity-consistent extension to the HLL flux";
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		
		
		int quantities = ijk.length;
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		int otherDirection = GridCartesian.Y_DIR;
		
		
		double meanU, meanV, meanC;
		if (averageRoe){
			double sqrtRhoL = Math.sqrt( ijk[EquationsIsentropicEuler.INDEX_RHO]);
			double sqrtRhoR = Math.sqrt(ip1jk[EquationsIsentropicEuler.INDEX_RHO]);
			meanU = (sqrtRhoL * ijk[EquationsIsentropicEuler.INDEX_XMOM]/ijk[EquationsIsentropicEuler.INDEX_RHO] + 
				sqrtRhoR * ip1jk[EquationsIsentropicEuler.INDEX_XMOM]/ijk[EquationsIsentropicEuler.INDEX_RHO])/
				(sqrtRhoL + sqrtRhoR);
			meanV = (sqrtRhoL * ijk[EquationsIsentropicEuler.INDEX_YMOM]/ijk[EquationsIsentropicEuler.INDEX_RHO] + 
				sqrtRhoR * ip1jk[EquationsIsentropicEuler.INDEX_YMOM]/ijk[EquationsIsentropicEuler.INDEX_RHO])/
				(sqrtRhoL + sqrtRhoR);
			double meanRho = 0.5*(ijk[EquationsIsentropicEuler.INDEX_RHO] + ip1jk[EquationsIsentropicEuler.INDEX_RHO]);
			meanC = ((EquationsIsentropicEuler) equations).getSoundSpeedFromPressure(
				((EquationsIsentropicEuler) equations).getPressure(meanRho), meanRho);
		} else {
			meanU = Math.max(ijk[EquationsIsentropicEuler.INDEX_XMOM]/ijk[EquationsIsentropicEuler.INDEX_RHO], 
					ip1jk[EquationsIsentropicEuler.INDEX_XMOM]/ip1jk[EquationsIsentropicEuler.INDEX_RHO]);
			meanV = Math.max(ijk[EquationsIsentropicEuler.INDEX_YMOM]/ijk[EquationsIsentropicEuler.INDEX_RHO], 
					ip1jk[EquationsIsentropicEuler.INDEX_YMOM]/ip1jk[EquationsIsentropicEuler.INDEX_RHO]);
			meanC = Math.max( ((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(ijk), 
				((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(ip1jk) );
		}
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, ijk, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, ip1jk, direction);
		double[] resFlux;
		
		if (meanU - meanC > 0){
			resFlux = fluxLeft; 
		} else if (meanU + meanC < 0){
			resFlux = fluxRight;
		} else {
			double[] fluxLeftTop = equations.fluxFunction(i, j, k, ijp1k, direction);
			double[] fluxLeftBottom = equations.fluxFunction(i, j, k, ijm1k, direction);
			
			double[] fluxRightTop = equations.fluxFunction(i, j, k, ip1jp1k, direction);
			double[] fluxRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, direction);
			
			// perpendicular fluxes:
			double[] fluxPerpLeftTop = equations.fluxFunction(i, j, k, ijp1k, otherDirection);
			double[] fluxPerpLeftBottom = equations.fluxFunction(i, j, k, ijm1k, otherDirection);
			
			double[] fluxPerpRightTop = equations.fluxFunction(i, j, k, ip1jp1k, otherDirection);
			double[] fluxPerpRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, otherDirection);
			
			
			double verticalAverageFluxLeft, verticalAverageFluxRight, diff, perpdiffaverage;
			double[] completediff = new double[quantities];
			double[] average = new double[quantities];
			
			for (int q = 0; q < quantities; q++){
				verticalAverageFluxLeft  = 0.25*( fluxLeftTop[q]  + 2.0*fluxLeft[q]  + fluxLeftBottom[q]  );
				verticalAverageFluxRight = 0.25*( fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q] );
				average[q] = 0.5*(verticalAverageFluxLeft + verticalAverageFluxRight);
				diff    = verticalAverageFluxRight - verticalAverageFluxLeft;
				
				perpdiffaverage = 0.25*(fluxPerpRightTop[q] - fluxPerpRightBottom[q] + fluxPerpLeftTop[q] - fluxPerpLeftBottom[q]);
				completediff[q] = diff + perpdiffaverage;
			}
			
					
			resFlux = average;
			for (int q = 0; q < quantities; q++){
				resFlux[q] -= meanU/2/meanC * completediff[q];
			}
				
				
			resFlux[EquationsIsentropicEuler.INDEX_RHO] += meanU/meanC * completediff[EquationsIsentropicEuler.INDEX_RHO] 
					- 1.0/2/meanC * completediff[EquationsIsentropicEuler.INDEX_XMOM];
			resFlux[EquationsIsentropicEuler.INDEX_XMOM] += (meanU*meanU - meanC*meanC)/2/meanC * completediff[EquationsIsentropicEuler.INDEX_RHO];
			
			
			/*if (meanU == 0){
				resFlux[EquationsIsentropicEuler.INDEX_YMOM] = - meanC/2 * (right[EquationsIsentropicEuler.INDEX_YMOM] - left[EquationsIsentropicEuler.INDEX_YMOM]);
			} else {
				resFlux[EquationsIsentropicEuler.INDEX_YMOM] += 
					(meanU*meanU + meanC*meanC)/2/meanC * meanV/meanU * completediff[EquationsIsentropicEuler.INDEX_RHO] 
					- meanV/2/meanC * completediff[EquationsIsentropicEuler.INDEX_XMOM]
					+ (meanU*meanU - meanC*meanC)/2/meanC/meanU * completediff[EquationsIsentropicEuler.INDEX_YMOM] ;
			}//*/

		}
		
		return new FluxAndWaveSpeed(resFlux, meanC + Math.abs(meanU));
	}

}
