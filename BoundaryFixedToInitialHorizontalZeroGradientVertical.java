
public class BoundaryFixedToInitialHorizontalZeroGradientVertical extends Boundary {

	protected InitialData initial;
	
	public BoundaryFixedToInitialHorizontalZeroGradientVertical(Grid grid, InitialData initial) {
		super(grid);
		this.initial = initial;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		i = g.i(); j = g.j(); k = g.k();
		nextI = i; nextJ = j; nextK = k;
		boolean outsideHorizontal = false;
		
		if (grid instanceof GridCartesian){
			if (j < ((GridCartesian) grid).indexMinY()){ nextJ += ((GridCartesian) grid).ny(); }
			if (k < ((GridCartesian) grid).indexMinZ()){ nextK += ((GridCartesian) grid).nz(); }
				
			if (j > ((GridCartesian) grid).indexMaxY()){ nextJ -= ((GridCartesian) grid).ny(); }
			if (k > ((GridCartesian) grid).indexMaxZ()){ nextK -= ((GridCartesian) grid).nz(); }

			if ((i < ((GridCartesian) grid).indexMinX()) || (i > ((GridCartesian) grid).indexMaxX())){
				outsideHorizontal = true;
			}
		} else {
			if ((g.getX() < grid.xmin()) || (g.getX() > grid.xmax())){
				outsideHorizontal = true;
			} else {
				for (GridCell nb : grid.getNeighbours(g)){
					if (grid.isValidCell(nb)){
						nextI = nb.i(); nextJ = nb.j(); nextK = nb.k();
						break;
					}
				}
			}
		}
		if (outsideHorizontal){
			conservedQuantities[i][j][k] = 
				initial.getInitialValue(new GridCell(i, j, k, grid), conservedQuantities[i][j][k].length);
		} else {
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
			}
		}

		
	}

	@Override
	public String getDetailedDescription() {
		return "Boundary values left and right fixed to the values there initially, other boundaries made zero gradient.";
	}


}
