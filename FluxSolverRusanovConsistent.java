import matrices.SquareMatrix;


public class FluxSolverRusanovConsistent extends FluxSolverRoeConsistentViaPseudoInverse {

	public FluxSolverRusanovConsistent(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}
	
	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		
		SquareMatrix[] matleft = getDiagonalization(i, j, k, left, direction);
		SquareMatrix[] matright = getDiagonalization(i, j, k, right, direction);
		double maxEigenvalue = 0;
		
		for (int ii = 0; ii < matleft[1].rows(); ii++){
			maxEigenvalue = Math.max( matleft[1].value(ii, ii), maxEigenvalue);
			maxEigenvalue = Math.max(matright[1].value(ii, ii), maxEigenvalue);
		}
		
		SquareMatrix[] mat = getDiagonalization(i, j, k, quantities, direction);
		double waveSpeed;
		
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("beforehand Eigenvalue #" + ii + " NaN: ");
			}
		}
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		
		SquareMatrix inverse = getInverseOfEigenvalues(mat[1], left, right, i,j,k, direction);
		SquareMatrix res = mat[0].mult(inverse).mult(mat[2]).mult(maxEigenvalue).toSquareMatrix();
		
		// todo we could pass maxEigenvalue here!
		return new UpwindingMatrixAndWaveSpeed(
				res, waveSpeed);
	}

	protected SquareMatrix getInverseOfEigenvalues(SquareMatrix eigenvalues,
			double[] left, double[] right, int ii, int jj, int kk, int direction){
		SquareMatrix res = new SquareMatrix(eigenvalues.rows());
		double eval;
		
		for (int k = 0; k < eigenvalues.rows(); k++){
			eval = eigenvalues.value(k,k);
			
			if (eval == 0){
				res.setElement(k, k, 0);
			} else {
				res.setElement(k, k, 1.0/eval);
			}
		}
		return res;
	}
	
}
