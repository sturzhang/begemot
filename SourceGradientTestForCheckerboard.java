
public class SourceGradientTestForCheckerboard extends SourceInCellOnly {

	protected double sigma;
	protected double velocity;
	protected double density;
	
	public SourceGradientTestForCheckerboard(GridCartesian grid, EquationsHydroIdeal equations, 
			double sigma, double velocity, double density) {
		super(grid, equations);
		this.sigma = sigma;
		this.velocity = velocity;
		this.density = density;
	}

	@Override
	public String getDetailedDescription() {
		return "Artificial source terms to stabilize the linear pressure test setup for checkerboards in 1-d";
	}

	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell g, double time) {
		double[] res = new double[quantities.length];
		for (int m = 0; m < res.length; m++){
			res[m] = 0.0;
		}
		double x = grid.getX(g.i(),g.j(),g.k());
		
		/*double minus = 1.0; //(grid.getX(i) > 0.5 ? 1.0 : -1.0);
		
		res[EquationsHydroIdeal.INDEX_XMOM]   = minus*sigma;
		res[EquationsHydroIdeal.INDEX_ENERGY] = velocity * equations.gamma() / (equations.gamma() - 1) * minus*sigma;
		*/
		
		res[EquationsHydroIdeal.INDEX_RHO]    = density*sigma;
		res[EquationsHydroIdeal.INDEX_XMOM]   = 2.0*sigma*(velocity + sigma*x);
		res[EquationsHydroIdeal.INDEX_ENERGY] = 1.5*density*Math.pow(velocity + sigma*x, 2)*sigma;
		
		
		return res;
	}

}
