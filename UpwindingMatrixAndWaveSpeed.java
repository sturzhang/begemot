import matrices.*;

public class UpwindingMatrixAndWaveSpeed {

	protected SquareMatrix matrix;
	protected double waveSpeed;
	
	public UpwindingMatrixAndWaveSpeed(SquareMatrix matrix, double waveSpeed) {
		this.matrix = matrix;
		this.waveSpeed = waveSpeed;
	}
	
	public SquareMatrix matrix(){ return matrix; }
	
	public double waveSpeed(){ return waveSpeed; }

}
