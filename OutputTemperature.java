
public class OutputTemperature extends Output {

	protected double y;
	
	public OutputTemperature(double outputTimeInterval, GridCartesian grid,
			EquationsHydroIdeal equations, double y) {
		super(outputTimeInterval, grid, equations);
		this.y = y;
	}

    public String getDetailedDescription(){
    	return "prints p/rho at y = " + y + " over time";
    }

	
	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		int i = ((GridCartesian) grid).indexMinX();
		int j = ((GridCartesian) grid).getYIndex(y);
		int k = ((GridCartesian) grid).indexMinZ();
		
		double temp = ((EquationsHydroIdeal) equations).getPressureFromConservative(quantities[i][j][k])/
				quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
		
		println(time + " " + temp);

	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}
}
