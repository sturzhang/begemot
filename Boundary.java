public abstract class Boundary {

	protected Grid grid;
	public double test;
	
	public Boundary(Grid grid){
		this.grid = grid;
	}
	
	protected abstract void fillCell(GridCell g, double[][][][] conservedQuantities, double time);
		
    public abstract String getDetailedDescription();
    
    public String description(){ 
    	return "# Boundary used: " + getDetailedDescription(); 
    }
    
	public void set(double[][][][] conservedQuantities, double[][][][][] pointValues, double time) {
		for (GridCell g : grid.getAllGhostcells()){
			fillCell(g, conservedQuantities, time);
		}
	}

}