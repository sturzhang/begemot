
public class OutputNone extends Output {

	public OutputNone(GridCartesian grid) {
		super(10000.0, grid, null);
	}

	@Override
	public String getDetailedDescription() {
		return "No output";
	}

	@Override
	public void print(String text) {
		
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {

	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
