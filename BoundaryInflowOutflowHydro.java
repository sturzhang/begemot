
public class BoundaryInflowOutflowHydro extends Boundary {

	protected double[] inflowVel;
	protected double inflowRho;
	protected double outflowPressure;
	
	protected EquationsHydroIdeal equations;

	public BoundaryInflowOutflowHydro(GridCartesian grid, EquationsHydroIdeal equations, 
			double inflowRho, double outflowPressure, double[] inflowVel) {
		super(grid);
		this.equations = equations;
		
		this.inflowVel = inflowVel;
		this.inflowRho = inflowRho;
		this.outflowPressure = outflowPressure;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		boolean inflow, outflow;
		double rhoV2;
		
		i = g.i(); j = g.j(); k = g.k();
		
		nextI = i; nextJ = j; nextK = k;
		inflow = false;
		outflow = false;
			
		if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); inflow = true; }
		if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
			
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); outflow = true; }
		if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }

		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
		}

		if (inflow){
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = inflowRho;
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = inflowRho*inflowVel[0];
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] = inflowRho*inflowVel[1];
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] = inflowRho*inflowVel[2];
			
			if (time > 5){
				//if (grid.getY(j) < 0.1*grid.yMidpoint()){
					for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
						// passive scalars
						conservedQuantities[i][j][k][s] = Math.pow(-1.0, Math.max(0, Math.round((grid.getY(i,j,k) - grid.ymin())/(grid.ymax() - grid.ymin())*10)));
					}
				//}
			}
		} 
		if (outflow) {
			rhoV2 = (conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]*
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] + 
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]*
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM])/
					conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
			
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = 
					equations.getEnergy(outflowPressure, rhoV2);
		} 
	}

	@Override
	public String getDetailedDescription() {
		return "inflow vel = " + 
	inflowVel[0] + ", " + inflowVel[1] + ", " + inflowVel[2] + ", rho = " + inflowRho + 
	" and outflow pressure " + outflowPressure + ", zero gradient everywhere else";
	}


}
