
public class TimeIntegratorForSemiLagrangianHydroSimplifiedConsistent extends TimeIntegratorForSemiLagrangian {

	public TimeIntegratorForSemiLagrangianHydroSimplifiedConsistent(Grid grid,
			double CFL, FluxSolverSemiLagrangianHydroSimplifiedConsistent fluxSolver) {
		super(grid, CFL, fluxSolver);
	}

	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {

		if (step == 1){
			return conservedQuantities;
		} else if (step == 2){
			return myIntegrator.perform(step, dt, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
		} else {
			System.err.println("ERROR: time integrator should have only two steps!");
			return null;
		}
					
	}
	
	@Override
	public String getDetailedDescription(){
    	return super.getDetailedDescription() + " (time integrator for semi-Lagrangian hydro solver: first step needed for intercell velocities only, i.e. effectively a one-step time integrator)";
    }

}
