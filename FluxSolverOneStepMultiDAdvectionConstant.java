public class FluxSolverOneStepMultiDAdvectionConstant extends FluxSolverOneStepMultiD {
	
	public FluxSolverOneStepMultiDAdvectionConstant(GridCartesian grid, EquationsAdvectionConstant equations) {
		super(grid, equations);
	}
	
	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge, 
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		

		/*System.err.println(gridEdge.adjacent1() + " --- " + gridEdge.adjacent2());
		System.err.println(lefttop[0] + ", " + righttop[0]);
		System.err.println(left[0] + ", " + right[0]);
		System.err.println(leftbottom[0] + ", " + rightbottom[0]); //*/
		
		int q = 0;
		int direction = GridCartesian.X_DIR;
		int otherDirection = (direction == GridCartesian.X_DIR ? GridCartesian.Y_DIR : GridCartesian.X_DIR); 
		double diff, orthogonaldiff, upworthogonaldiff;
		
		double multidaverageleft, multidaverageright;
		
		double upwindingTerm, fluxRes, upwindingMatrix;
		
		multidaverageleft  =  0.5*ijk[q]  + 0.25*(ijp1k[q]  + ijm1k[q]); 
		multidaverageright = 0.5*ip1jk[q] + 0.25*(ip1jp1k[q] + ip1jm1k[q]); 
		
		diff = multidaverageright - multidaverageleft;
		//diff = right[q] - left[q]; 
		
		orthogonaldiff    = 0.25*(ijp1k[q] + ip1jp1k[q] - ijm1k[q] - ip1jm1k[q]); 	
		upworthogonaldiff = 0.25*(-ijp1k[q] + ip1jp1k[q] + ijm1k[q] - ip1jm1k[q]); 
		
		double vel = ((EquationsAdvectionConstant) equations).velocity()[direction];
		
		upwindingMatrix = Math.abs(vel);
		//double waveSpeed = vel;
		double waveSpeed = Math.max(vel, ((EquationsAdvectionConstant) equations).velocity()[otherDirection]);
		
		upwindingTerm = upwindingMatrix/2*diff;
	    
		double central = vel/2*(multidaverageleft + multidaverageright);
		//double central = vel/2*(left[q] + right[q]);
		
		double ortho = vel*sign(((EquationsAdvectionConstant) equations).velocity()[otherDirection]);
		double multidextra = ortho/2*orthogonaldiff;
		
		double upwortho = Math.abs(vel)*sign(((EquationsAdvectionConstant) equations).velocity()[otherDirection]);
		double upwmultidextra = upwortho/2*upworthogonaldiff;
				
		fluxRes = central - upwindingTerm - multidextra; // + upwmultidextra;
    	if (Double.isNaN(fluxRes)){
    	    System.err.println("fluxes are NaN");
    	}
    	
    	return new FluxAndWaveSpeed(new double[] {fluxRes}, waveSpeed);
	}

	@Override
	public String getDetailedDescription() {
		return "Solver based on corner fluxes for up to 2-d scalar advection";
	}


	
	

}
