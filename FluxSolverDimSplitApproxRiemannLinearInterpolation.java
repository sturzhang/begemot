
public class FluxSolverDimSplitApproxRiemannLinearInterpolation extends
		FluxSolverDimSplitEdgeNeighboursOnly {

	public FluxSolverDimSplitApproxRiemannLinearInterpolation(Grid grid,
			EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		GridCell g = gridEdge.adjacent1();
		double[] mean = new double[2];
		double shockspeedLeft = left[EquationsIsentropicEuler.INDEX_XMOM]/left[EquationsIsentropicEuler.INDEX_RHO] 
				- ((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(left);
		if (shockspeedLeft > 0){
		    return new FluxAndWaveSpeed(equations.fluxFunction(g.i(), g.j(), g.k(), left, GridCartesian.X_DIR), shockspeedLeft);
		} else {
		    double shockspeedRight = right[EquationsIsentropicEuler.INDEX_XMOM]/right[EquationsIsentropicEuler.INDEX_RHO] 
		    		+ ((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(right);
		    if (shockspeedRight < 0){
		    	return new FluxAndWaveSpeed(equations.fluxFunction(g.i(), g.j(), g.k(), right, GridCartesian.X_DIR), shockspeedRight);
		    } else {
		    	for (int q = 0; q <= 1; q++){
		    		mean[q] = right[q] - shockspeedRight/(shockspeedLeft-shockspeedRight) * (left[q] - right[q]);
		    	}
		    	return new FluxAndWaveSpeed(equations.fluxFunction(g.i(), g.j(), g.k(), new double[] {mean[0], mean[1], 0, 0}, GridCartesian.X_DIR),
		    			Math.max(Math.abs(shockspeedLeft), shockspeedRight));
		    }
		}
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left,
			double[] right, int direction) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetailedDescription() {
		return "Approximate Riemann solver for isentropic equations which inserts a linear function between two states (a bit like an all-rarefaction solver).";
	}

}
