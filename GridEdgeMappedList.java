import java.util.LinkedList;


public class GridEdgeMappedList{ // implements Iterable<GridEdgeMapped>{

	protected GridEdgeMapped[][][][] allEdges;
	
	public GridEdgeMappedList(int n1, int n2, int n3, int ndir) {
		allEdges = new GridEdgeMapped[n1][n2][n3][ndir];
	}

	public void initialize(Grid grid){
		int[][][] nextFreeIndex = new int[allEdges.length][allEdges[0].length][allEdges[0][0].length];
		LinkedList<GridCell> neighbours;
    	
		
		for (int i = 0; i < allEdges.length; i++){
    		for (int j = 0; j < allEdges[i].length; j++){
    			for (int k = 0; k < allEdges[i][j].length; k++){
    				for (int l = 0; l < allEdges[i][j][k].length; l++){
    					allEdges[i][j][k][l] = null;
    				}
    			}
    		}
		}
		
		// This structure will see the internal edges twice, and the edges of the boundary only once!
    	for (GridCell g : grid){
    		neighbours = grid.getNeighbours(g);
    		for (GridCell nb : neighbours){
    			set(g.i(), g.j(), g.k(), nextFreeIndex[g.i()][g.j()][g.k()], new GridEdgeMapped(grid, g, nb));
    			// by convention the index should be referring to adjacent1
    			nextFreeIndex[g.i()][g.j()][g.k()]++;
    		}
    	}
	}
	
	public void set(int i, int j, int k, int dir, GridEdgeMapped edge){
		allEdges[i][j][k][dir] = edge;
		edge.setIndex(dir);
	}
	
	public GridEdgeMapped[] get(int i, int j, int k){
		return allEdges[i][j][k];
	}
	
	public GridEdgeMapped get(int i, int j, int k, int dir){
		return allEdges[i][j][k][dir];
	}
}
