
public class InitialDataHydroQuadraticVortex extends InitialDataHydroVortex {

	protected double MachNumber, rho0;
	private double p0;
	
	public InitialDataHydroQuadraticVortex(Grid grid, EquationsHydroIdeal equations, double MachNumber, double rho0) {
		super(grid, equations);
		this.MachNumber = MachNumber;
				
		this.rho0 = rho0;
		p0 = 1.0 / MachNumber / MachNumber - rho0*0.917;
	}

    public String getDetailedDescription(){
    	return "Gresho vortex with reference Mach number = " + MachNumber + ", constant density = " + rho0 + 
    			"and central pressure = " + p0;
    }
	
	@Override
	protected double speed(double r) {
		if (r < 0.4){ return -r*(r-0.4)/0.04; }
		else { return 0.0; }
	}

	@Override
	protected double pressure(double r) {
	    if (r > 0.4) { r = 0.4; }
	    return p0 + rho0 / 0.04/0.04 * (r*r*r*r/4 - 0.8*r*r*r/3 + 0.04*r*r/2);
	}

	protected double rho(double r) {
		return rho0;
	
		
	}

}
