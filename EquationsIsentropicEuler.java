import matrices.DiagonalMatrix;
import matrices.SquareMatrix;


public class EquationsIsentropicEuler extends EquationsHydro {

	private double gamma;
	private double K;
	
	
	public static final int INDEX_RHO = 0;
	public static final int INDEX_XMOM = 1;
	public static final int INDEX_YMOM = 2;
	public static final int INDEX_ZMOM = 3;
	
	
	public EquationsIsentropicEuler(Grid grid, int numberOfPassiveScalars, double gamma, double K) {
		super(grid, numberOfPassiveScalars);
		this.K = K;
		this.gamma = gamma;
	}

	public double gamma(){ return gamma; }
	public double K(){ return K; }
	
	public double getPressure(double rho){
		return K*Math.pow(rho,  gamma);
	}
	
	public double getEnergy(double rho, double[] v){
		return getPressure(rho)/(gamma-1) + 0.5*rho*(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	}
	
	
	public double getPressureDeriv(double rho){
		return K*gamma*Math.pow(rho, gamma-1);
	}
	
	public double getSoundSpeedFromPressure(double pressure, double rho){
		return Math.sqrt(gamma * pressure / rho);
	}
	
	public double getSoundSpeedFromConservative(double[] quantities){
		double rho = quantities[INDEX_RHO];
		return getSoundSpeedFromPressure(getPressure(rho), rho);
	}
	
	@Override
	public int getNumberOfConservedQuantities() {
		return 4;
	}

	@Override
	public String getDetailedDescription() {
		return "Shallow water equations / Isentropic Euler equations with gamma = " + gamma + ", K = " + K;
	}

	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		int indexDirection = getIndexDirection(direction);
		double[] res = new double[quantities.length];
		res[INDEX_RHO]  = quantities[indexDirection];
		double pressure = getPressure(quantities[INDEX_RHO]);
		res[INDEX_XMOM] = quantities[indexDirection]*quantities[INDEX_XMOM]/quantities[INDEX_RHO];
		res[INDEX_YMOM] = quantities[indexDirection]*quantities[INDEX_YMOM]/quantities[INDEX_RHO];
		res[INDEX_ZMOM] = quantities[indexDirection]*quantities[INDEX_ZMOM]/quantities[INDEX_RHO];
		res[indexDirection] += pressure;
		return res;
	}

	public int getIndexDirection(int direction){
		int indexDirection = -1; // init to make compiler happy
		if (direction == GridCartesian.X_DIR){ indexDirection = EquationsHydroIdeal.INDEX_XMOM; }	
	    if (direction == GridCartesian.Y_DIR){ indexDirection = EquationsHydroIdeal.INDEX_YMOM; }	
	    if (direction == GridCartesian.Z_DIR){ indexDirection = EquationsHydroIdeal.INDEX_ZMOM; }	
	    return indexDirection;
	}
	
	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		SquareMatrix[] mat = new SquareMatrix[3];
		double arrmat[][][] = new double[2][4][4];
		double[] eval = new double[4];
		int indexDirection = getIndexDirection(direction);
		double soundSpeedX = 0.0, soundSpeedY = 0.0, soundSpeedZ = 0.0;
		
		int pos = 0; // to make compiler happy
		double vDir = 0.0;
		
		double soundSpeed = getPressureDeriv(quantities[INDEX_RHO]);
		double vx = quantities[INDEX_XMOM]/quantities[INDEX_RHO];
		double vy = quantities[INDEX_YMOM]/quantities[INDEX_RHO];	
		double vz = quantities[INDEX_ZMOM]/quantities[INDEX_RHO];
								// rewriting trick
		if (indexDirection == INDEX_XMOM){ soundSpeedX = soundSpeed;  }
		if (indexDirection == INDEX_YMOM){ soundSpeedY = soundSpeed;  }
		if (indexDirection == INDEX_ZMOM){ soundSpeedZ = soundSpeed;  }
	
		if (direction == GridCartesian.X_DIR){ pos = 1; vDir = vx; }
		if (direction == GridCartesian.Y_DIR){ pos = 2; vDir = vy; }
		if (direction == GridCartesian.Z_DIR){ pos = 3; vDir = vz; }
		
		for (int ii = 0; ii <= 3; ii++){
			for (int jj = 0; jj <= 3; jj++){
				arrmat[0][ii][jj] = 0.0;
				arrmat[1][ii][jj] = 0.0;
			}
			eval[ii] = 0.0;
		}
		
		arrmat[0][0][0] = 1.0;
		arrmat[0][1][1] = 1.0;
		arrmat[0][2][2] = 1.0;
		arrmat[0][3][3] = 1.0;

		arrmat[1][0][0] = 1.0;
		arrmat[1][1][1] = 1.0;
		arrmat[1][2][2] = 1.0;
		arrmat[1][3][3] = 1.0;

		arrmat[0][1][0] = vx - soundSpeedX;
		arrmat[0][2][0] = vy - soundSpeedY;
		arrmat[0][3][0] = vz - soundSpeedZ;
		
		arrmat[0][0][pos] = 1.0;
		arrmat[0][1][pos] = vx + soundSpeedX;
		arrmat[0][2][pos] = vy + soundSpeedY;
		arrmat[0][3][pos] = vz + soundSpeedZ;
		
		arrmat[1][0][0] = (vDir+soundSpeed)/2/soundSpeed;
		arrmat[1][0][pos] = -0.5/soundSpeed;
		
		arrmat[1][1][0] = -vx;
		arrmat[1][1][0] = -vy;
		arrmat[1][1][0] = -vz;
		arrmat[1][pos][0] = (-vDir+soundSpeed)/2/soundSpeed;
		
		arrmat[1][pos][pos] = 0.5/soundSpeed;
		
		eval[0] = vDir - soundSpeed;
		eval[1] = vDir;
		eval[2] = vDir;
		eval[3] = vDir;
		eval[pos] = vDir + soundSpeed;
		
		mat[0] = new SquareMatrix(arrmat[0]);
		mat[1] = new DiagonalMatrix(eval);
		mat[2] = new SquareMatrix(arrmat[1]);

		return mat;
		
		
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k,
			double[] quantities, int direction) {
		return quantities[getIndexDirection(direction)]/quantities[INDEX_RHO];
	}

	
	protected SquareMatrix primToCons(double vX, double vY, double vZ, double rho,
			SquareMatrix mat){
		double dUdVarr[][] = new double[4][4];
		double dVdUarr[][] = new double[4][4];
		
		for (int i = 0; i < dUdVarr.length; i++){
			for (int j = 0; j < dUdVarr.length; j++){
				dUdVarr[i][j] = 0.0;
				dVdUarr[i][j] = 0.0;
			}
		}
		
		dUdVarr[0][3] = 1.0;
		
		dUdVarr[1][3] = vX;
		dUdVarr[2][3] = vY;
		dUdVarr[3][3] = vZ;
				
		dUdVarr[1][0] = rho;
		dUdVarr[2][1] = rho;
		dUdVarr[3][2] = rho;
		
		
		dVdUarr[3][0] = 1.0;
		
		dVdUarr[0][0] = -vX/rho;
		dVdUarr[1][0] = -vY/rho;
		dVdUarr[2][0] = -vZ/rho;
				
		dVdUarr[0][1] = 1.0/rho;
		dVdUarr[1][2] = 1.0/rho;
		dVdUarr[2][3] = 1.0/rho;
		
		SquareMatrix dUdV = new SquareMatrix(dUdVarr);
		SquareMatrix dVdU = new SquareMatrix(dVdUarr);
	
		return dUdV.mult(mat.mult(dVdU)).toSquareMatrix();
	}
	
	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		res[INDEX_RHO] = quantities[INDEX_RHO];
		
		double[] transformedVector = transformVector(quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], normalVector);
		res[INDEX_XMOM] = transformedVector[0];
		res[INDEX_YMOM] = transformedVector[1];
		res[INDEX_ZMOM] = transformedVector[2];
		
		return res;

	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		res[INDEX_RHO] = quantities[INDEX_RHO];
		
		double[] transformedVector = transformVectorBack(quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], normalVector);
		res[INDEX_XMOM] = transformedVector[0];
		res[INDEX_YMOM] = transformedVector[1];
		res[INDEX_ZMOM] = transformedVector[2];
		
		return res;
	}
	
}
