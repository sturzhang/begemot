import matrices.SquareMatrix;


public class FluxSolverOneStepMultiDRoeHydroVertexFocused extends FluxSolverOneStepMultiDRoeHydroFineTunedDivergence {

	public FluxSolverOneStepMultiDRoeHydroVertexFocused(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	
	@Override
	protected double[] combineTermsIntoFlux(int quantities, int direction, int i, int j, int k,
			double[] multidaverageLeftTop, double[] multidaverageLeftBottom, 
			double[] multidaverageRightTop, double[] multidaverageRightBottom,
			double averageVelocityLeftTop, double averageVelocityLeftBottom, 
			double averageVelocityRightTop, double averageVelocityRightBottom,
			double[] orthogonaldiffTop, double[] orthogonaldiffBottom, 
			double orthoVelocityTop, double orthoVelocityBottom,
			double[] xAverage, double[] xAverageTop, double[] xAverageBottom, 
			double orthoVelocityXAverage, double orthoVelocityXAverageTop, double orthoVelocityXAverageBottom){
		
		/*
		multidaverageLeftTop     = { q_{i} }_{j+1/2}/2
		multidaverageLeftBottom  = { q_{i} }_{j-1/2}/2
		multidaverageRightTop    = { q_{i+1} }_{j+1/2}/2
		multidaverageRightBottom = { q_{i+1} }_{j-1/2}/2
		averageVelocityLeftTop     = { u_{i} }_{j+1/2} /2
		averageVelocityLeftBottom  =  { u_{i} }_{j-1/2} /2
		averageVelocityRightTop    = { u_{i+1} }_{j+1/2} /2
		averageVelocityRightBottom = { u_{i+1} }_{j-1/2} /2
		orthogonaldiffTop     = { { q }_{i+1/2} ]_{j+1/2} / 2
		orthogonaldiffBottom  = { { q }_{i+1/2} ]_{j-1/2} / 2
		orthoVelocityTop    = { { v }_{i+1/2} }_{j+1/2}/4
		orthoVelocityBottom = { { v }_{i+1/2} }_{j-1/2}/4
		xAverage       = { q }_{i+1/2,j}
		xAverageTop    = { q }_{i+1/2,j+1}
		xAverageBottom = { q }_{i+1/2,j-1}
		 */
		
		//SquareMatrix upwindingMatrix;
		double[] res = new double[quantities];
		
		
		
		for (int index = 0; index <= 1; index++){
			double[] diff = new double[quantities];
			double[] orthogonaldiff = new double[quantities];
			double[] multidaverage = new double[quantities];
			//double[] multidaverageOrtho = new double[quantities];
			double orthoVelocity = 0; // to please compiler
			
			if (index == 0){
				for (int q = 0; q < quantities; q++){ 
					diff[q] = multidaverageRightTop[q] - multidaverageLeftTop[q];
					multidaverage[q] = (multidaverageLeftTop[q] + multidaverageRightTop[q])/2;
					orthogonaldiff[q] = orthogonaldiffTop[q];
					//multidaverageOrtho[q] = multidaverage[q];
				}
				diff[EquationsHydroIdeal.INDEX_XMOM] = 
						multidaverageRightTop[EquationsHydroIdeal.INDEX_RHO]*averageVelocityRightTop - 
						multidaverageLeftTop[ EquationsHydroIdeal.INDEX_RHO]*averageVelocityLeftTop;
				multidaverage[EquationsHydroIdeal.INDEX_XMOM] = multidaverage[EquationsHydroIdeal.INDEX_RHO]*
						(averageVelocityRightTop + averageVelocityLeftTop)/2; 
				orthogonaldiff[EquationsHydroIdeal.INDEX_YMOM] = 
						xAverageTop[EquationsHydroIdeal.INDEX_RHO]*orthoVelocityXAverageTop -
						xAverage[EquationsHydroIdeal.INDEX_RHO]*orthoVelocityXAverage;
				/*multidaverageOrtho[EquationsHydroIdeal.INDEX_YMOM] = 
						orthoVelocityTop*multidaverageOrtho[EquationsHydroIdeal.INDEX_RHO];*/ 
				orthoVelocity = orthoVelocityTop;
				
				
			} else if (index == 1){
				for (int q = 0; q < quantities; q++){ 
					diff[q] = multidaverageRightBottom[q] - multidaverageLeftBottom[q];
					multidaverage[q] = (multidaverageLeftBottom[q] + multidaverageRightBottom[q])/2;
					orthogonaldiff[q] = orthogonaldiffBottom[q];
					//multidaverageOrtho[q] = multidaverage[q];
				}
				diff[EquationsHydroIdeal.INDEX_XMOM] = 
						multidaverageRightBottom[EquationsHydroIdeal.INDEX_RHO]*averageVelocityRightBottom - 
						multidaverageLeftBottom[ EquationsHydroIdeal.INDEX_RHO]*averageVelocityLeftBottom;
				multidaverage[EquationsHydroIdeal.INDEX_XMOM] = multidaverage[EquationsHydroIdeal.INDEX_RHO]*
						(averageVelocityRightBottom + averageVelocityLeftBottom)/2; 
				orthogonaldiff[EquationsHydroIdeal.INDEX_YMOM] = 
						xAverage[EquationsHydroIdeal.INDEX_RHO]*orthoVelocityXAverage -
						xAverageBottom[EquationsHydroIdeal.INDEX_RHO]*orthoVelocityXAverageBottom;
				/*multidaverageOrtho[EquationsHydroIdeal.INDEX_YMOM] = 
						orthoVelocityBottom*multidaverageOrtho[EquationsHydroIdeal.INDEX_RHO];*/ 
				orthoVelocity = orthoVelocityBottom;
				
			}
			
			//upwindingMatrix = new SquareMatrix(quantities);
			//double[] upwindingTerm = new double[quantities];
			
			UpwindingMatrixAndWaveSpeed tmp = getUpwindingMatrix(i, j, k, multidaverage, direction);
			SquareMatrix upwindingMatrix = tmp.matrix();
			double[] upwindingTerm = upwindingMatrix.mult(0.5).mult(diff);
	    
			
			//SquareMatrix ortho = getOrthogonalContribution(multidaverageOrtho, direction);
			
			HydroState qu = new HydroState(multidaverage, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);
			SquareMatrix ortho = getOrthogonalContribution(qu.soundSpeed, orthoVelocity);
			double[] multidextra = ortho.mult(0.5).mult(orthogonaldiff);
			
		
			for (int q = 0; q < quantities; q++){
				res[q] += (upwindingTerm[q] + multidextra[q])/2;
			}
		}
		
		return res;

	}
}
