import java.util.LinkedList;


public class SetupList extends LinkedList<Setup>{
	
	
	public void initializeArrays(Grid grid){  
		for (Setup s : this){ s.initializeArrays(grid); }
	}
	
	/*
		int counter = 0;
		int fluxSolversteps = 0;
		
		for (Setup s : this){ 
			s.setFirstIndexOfConservedquantities(counter);
			counter += s.equations().getTotalNumberOfConservedQuantities();
			fluxSolversteps = Math.max(s.fluxSolver().steps(), fluxSolversteps);
		} 
			
	    grid.excludeFromTimeEvolution = grid.generateEmptyBooleanArray();
	    	
	    grid.conservedQuantities      = grid.generateEmptyArray( counter );
    	grid.interfaceValues          = grid.generateEmptyArrayForInterface( counter );
		grid.pointValues              = grid.generateEmptyArrayForInterface( counter );
	    	
    	grid.summedInterfaceFluxes    = grid.generateEmptyFluxArray( counter );
    	grid.interfaceFluxes          = grid.generateEmptyFluxArray( counter, fluxSolversteps );
    	grid.sources                  = grid.generateEmptyArray( counter );
			
    	grid.localWaveSpeeds          = grid.generateEmptyWaveSpeedArray();
	    
	
	}*/
	
	public void fillConservedQuantities(){     for (Setup s : this){ s.fillConservedQuantities();     } }
	public void setBoundaries(double time){               for (Setup s : this){ s.setBoundaries(time);               } }
	public void setObstacle(){                 for (Setup s : this){ s.setObstacle();                 } }
	public void performReconstruction(){       for (Setup s : this){ s.performReconstruction();       } }
	//public void evolvePointValues(){           for (Setup s : this){ s.evolvePointValues();           } }	
	public void initializeObstacle(){          for (Setup s : this){ s.initializeObstacle();          } }

	//public void setWaveSpeeds(){ for (Setup s : this){ s.setWaveSpeeds();      } }
	public void setFluxesAndWaveSpeeds(int timeIntegratorStep){      
												for (Setup s : this){ s.setFluxesAndWaveSpeeds(timeIntegratorStep);      } }
	public void setSources(double time, double dt, int timeIntegratorStep){       
		for (Setup s : this){ s.setSources(time, dt, timeIntegratorStep);                  } }
	public void setObstacleViaRigidWalls(){    for (Setup s : this){ s.setObstacleViaRigidWalls();    } }
	public void setFinalConservedQuantities(){ for (Setup s : this){ s.setFinalConservedQuantities(); } }
	
	public String getDescriptions(){
		String res = "";
		int counter = 1;
		for (Setup s : this){ 
			res += "### Setup " + counter + ": " + " (" + s.getClass().getName()      + ")" + "\n";
			res += s.getDescriptions();
			counter++;
		} 
		return res;
	}
	

	public double timeTillNextOutput(double time){
		double res = this.getFirst().output().timeTillNextOutput(time);
		for (Setup s : this){ 
			res = Math.min(res, s.output().timeTillNextOutput(time));
		}
		return res;
	}
	
	// TODO here we have to introduce extra columns instead
	
	public void produceOutput(double time){             
		for (Setup s : this){ 
			s.produceOutput(time);
			//s.output().println(" "); s.output().println(" ");
		} 
	}
	public void produceForcedOutput(double time){       for (Setup s : this){ s.produceForcedOutput(time); } }
	
	public void performTimeIntegration(TimeIntegratorFiniteVolume timeIntegrator, double dt, int timeIntegratorStep){    
		for (Setup s : this){ s.performTimeIntegration(timeIntegrator, dt, timeIntegratorStep); } 
	}
		
	public void sumFluxSolverSteps(Grid grid, double dt){        for (Setup s : this){ s.sumFluxSolverSteps(grid, dt); } }
	
	public double curMaxSignalSpeed(){
		double res = 0;
		for (Setup s : this){ 
			res = Math.max(res, s.fluxSolver().curMaxSignalSpeed()); 
		}
		return res;
	}
	
	
	/*public int getTotalNumberOfConservedQuantities(){
		int res = 0;
		for (Setup s : this){
			res += s.equations().getNumberOfConservedQuantities();
		}
		return res;
	}
	
	public int getMaxFluxSolverSteps(){
		int res = 0;
		for (Setup s : this){
			res = Math.max(res, s.fluxSolver().steps());
		}
		return res;
	}*/
}
