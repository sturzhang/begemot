
public abstract class FluxSolverHydro extends FluxSolverDimSplitEdgeNeighboursOnly {

	public FluxSolverHydro(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		int indexDirection = EquationsHydroIdeal.getIndexDirection(GridCartesian.X_DIR);
		
	    return flux(new HydroState(left, indexDirection, (EquationsHydroIdeal) equations), 
	    		new HydroState(right, indexDirection, (EquationsHydroIdeal) equations));
	}

	protected abstract FluxAndWaveSpeed flux(HydroState left, HydroState right);
	
	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right,	int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double mLeft, mRight, absMeanV;
		
		mLeft = left[EquationsHydroIdeal.getIndexDirection(direction)];
		mRight = right[EquationsHydroIdeal.getIndexDirection(direction)];
		
		absMeanV = Math.abs(0.5 * mLeft / left[EquationsHydroIdeal.INDEX_RHO] + 
							0.5 * mRight / right[EquationsHydroIdeal.INDEX_RHO]);
		
		
		for (int ii = 0; ii < res.length; ii++){
			s = sc[ii];
			res[i] = 0.5*(left[s]*mLeft + right[s]*mRight) - 0.5 * absMeanV * (right[s] - left[s]);
		}
		return res;
	}
	

	
}
