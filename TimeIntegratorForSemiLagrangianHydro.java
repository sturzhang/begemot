
public class TimeIntegratorForSemiLagrangianHydro extends TimeIntegratorForSemiLagrangian {

	protected double[][][] compressionFactor;
	
	public TimeIntegratorForSemiLagrangianHydro(Grid grid, double CFL, FluxSolverSemiLagrangianHydro fluxSolver) {
		super(grid, CFL, fluxSolver);
	}

	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities, 
			boolean[][][] excludeFromTimeIntegration) {
		
		int i, j, k;
		
		if (!fluxSolverKnowsMe){
			fluxSolver.setTimeIntegrator(this);
			fluxSolverKnowsMe = true;
		}
		
		if (step == 1){
			savedQuantities[0] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];
			compressionFactor = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length];
			
			for (GridCell g : grid){
				i = g.i(); j = g.j(); k = g.k();
				
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					savedQuantities[0][i][j][k][q] = conservedQuantities[i][j][k][q];
				}
				//we intend to save L here!
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = 1.0;
			}
			// in savedQuantities[0] we save rho, rhou, rhov, rhoE at initial time
			
			savedQuantities[step] = myIntegrator.perform(step, dt, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
			// in savedQuantities[1]=conservedquantities we save L, L rhou, L rhov, L rhoE
			for (GridCell g : grid){
				i = g.i(); j = g.j(); k = g.k();

				//compressionFactor[i][j][k] = 1.0;
				compressionFactor[i][j][k] = savedQuantities[step][i][j][k][EquationsHydroIdeal.INDEX_RHO];
				
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					savedQuantities[step][i][j][k][q] = savedQuantities[step][i][j][k][q] / compressionFactor[i][j][k];
				}
					
				savedQuantities[step][i][j][k][EquationsHydroIdeal.INDEX_RHO] = 
						savedQuantities[0][i][j][k][EquationsHydroIdeal.INDEX_RHO] / compressionFactor[i][j][k];
			}
		
			// in savedQuantities[1]=conservedquantities we save rho, rhou, rhov, rhoE at halfstep
		} else if (step == 2){
			for (GridCell g : grid){
				i = g.i(); j = g.j(); k = g.k();

				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					conservedQuantities[i][j][k][q] = conservedQuantities[i][j][k][q] * compressionFactor[i][j][k];
				}
			}
			// in conservedQuantities=conservedquantities we save L rho, L rhou, L rhov, L rhoE at halfstep
			
			savedQuantities[step] = myIntegrator.perform(step, dt, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
		} else {
			System.err.println("ERROR: time integrator should have only two steps!");
		}
					
		
		return savedQuantities[step];
		
		
	}

	@Override
	public String getDetailedDescription(){
    	return super.getDetailedDescription() + " (time integrator for semi-Lagrangian hydro solver with " + steps() + " steps)";
    }

}

	
