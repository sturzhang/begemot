
public class ReconstructionParabolic1D extends ReconstructionParabolic {

	protected double[][][][][] param1, param2, param0;
	
	public ReconstructionParabolic1D(boolean lockInterfaceValues) {
		super(lockInterfaceValues);
	}
	
	@Override
	protected void initializeParameters(int nx, int ny, int nz, int ndir, int nq){
		param0 = new double[nx][ny][nz][ndir][nq];
		param1 = new double[nx][ny][nz][ndir][nq];
		param2 = new double[nx][ny][nz][ndir][nq];
	}
	
	@Override
	protected void setParameters(GridCell g, double[][][] interfaceValuesOrderedByDirection, double[] quantities){
		double spacing, secondDerivTmp;
		
		for (int direction = 0; direction < grid.dimensions(); direction++){
			spacing = ((GridCartesian) grid).spacing(direction);
			for (int q = 0; q < quantities.length; q++){
				
				secondDerivTmp = interfaceValuesOrderedByDirection[direction][POSITIVE][q] + 
						         interfaceValuesOrderedByDirection[direction][NEGATIVE][q]
									- 2.0* quantities[q]; 
				
				param0[g.i()][g.j()][g.k()][direction][q] = quantities[q] - 0.25 * secondDerivTmp;
				param1[g.i()][g.j()][g.k()][direction][q] = (interfaceValuesOrderedByDirection[direction][POSITIVE][q] - 
						              interfaceValuesOrderedByDirection[direction][NEGATIVE][q]) / spacing;
				param2[g.i()][g.j()][g.k()][direction][q] = 3.0* secondDerivTmp / spacing / spacing;
			}
		}
		
	}
	
	@Override
	protected double[] eval(GridCell g, int direction, int nq, double pos){
		double[] res = new double[nq];
		for (int q = 0; q < res.length; q++){
			res[q] = param0[g.i()][g.j()][g.k()][direction][q] + 
					 param1[g.i()][g.j()][g.k()][direction][q]*pos + 
					 param2[g.i()][g.j()][g.k()][direction][q]*pos*pos; 
		}
		return res;
	}
	
	@Override
	public String getDetailedDescription() {
		return "Continuous parabolic recontruction dimension by dimension";
	}
}
