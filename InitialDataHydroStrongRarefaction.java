
public class InitialDataHydroStrongRarefaction extends
		InitialDataHydroShocktube {

	public InitialDataHydroStrongRarefaction(Grid grid,
			EquationsHydroIdeal equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	// Toro's book, p. 225, Test 2
	
	@Override
	public double leftDensity() {  return 1.0; }
	@Override
	public double leftVx() {       return -2.0; }
	@Override
	public double leftPressure() { return 0.4; }

	@Override
	public double rightDensity() {  return 1.0; }
	@Override
	public double rightVx() {       return 2.0; }
	@Override
	public double rightPressure() { return 0.4; }

	

}
