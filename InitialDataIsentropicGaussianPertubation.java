
public class InitialDataIsentropicGaussianPertubation extends InitialDataIsentropic {

	protected double rhoBackground, rhoAmplitude, rhoWidth;
	
	public InitialDataIsentropicGaussianPertubation(Grid grid, EquationsIsentropicEuler equations, double amplitude, double offset, double width) {
		super(grid, equations);
		this.rhoAmplitude = amplitude;
		this.rhoBackground = offset;
		this.rhoWidth = width;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		double x = g.getX() - grid.xMidpoint();
		
    	res[EquationsIsentropicEuler.INDEX_RHO] = rhoBackground + Math.exp(- x*x/rhoWidth/rhoWidth)*rhoAmplitude;
    				
    	res[EquationsIsentropicEuler.INDEX_XMOM] = 0.0;
    	res[EquationsIsentropicEuler.INDEX_YMOM] = 0.0;
    	res[EquationsIsentropicEuler.INDEX_ZMOM]= 0.0;
    	
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "gaussian pressure pertubation for isentropic Euler equations with background = " + 
				rhoBackground + ", amplitude = " + rhoAmplitude + " and width = " + rhoWidth;
	}

}
