
public class OutputHydro extends OutputAllQuantities {

	public OutputHydro(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations) {
		super(outputTimeInterval, grid, equations);
	}
	
	public OutputHydro(double outputTimeInterval, double zproportion, GridCartesian3D grid, EquationsHydroIdeal equations) {
		super(outputTimeInterval, zproportion, grid, equations);
	}
	
	
    public String getDetailedDescription(){
    	return "prints: rho, vx, vy, vz, pressure, soundspeed, mach number, total energy, angular momentum, entropy";
    }


	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		double vx, vy, vz, pressure, v2, rhoV2, soundspeed, mach;
		double angularmomentum, entropy;
		
		vx = quantities[EquationsHydroIdeal.INDEX_XMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		vy = quantities[EquationsHydroIdeal.INDEX_YMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		vz = quantities[EquationsHydroIdeal.INDEX_ZMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		v2 = vx*vx+vy*vy+vz*vz;
		rhoV2 = quantities[EquationsHydroIdeal.INDEX_RHO]*v2;
		pressure = ((EquationsHydroIdeal) equations).getPressure(quantities[EquationsHydroIdeal.INDEX_ENERGY], rhoV2);

		soundspeed = ((EquationsHydroIdeal) equations).getSoundSpeedFromPressure(pressure, quantities[EquationsHydroIdeal.INDEX_RHO]);
		mach = Math.sqrt(v2) / soundspeed;
		
		angularmomentum = (grid.getX(i,j,k)-grid.xMidpoint()) * quantities[EquationsHydroIdeal.INDEX_YMOM] - 
				(grid.getY(i,j,k)-grid.yMidpoint()) * quantities[EquationsHydroIdeal.INDEX_XMOM];
		
		entropy = pressure / Math.pow(quantities[EquationsHydroIdeal.INDEX_RHO], ((EquationsHydroIdeal) equations).gamma());
		
		print(quantities[EquationsHydroIdeal.INDEX_RHO] + " ");
		print(vx + " " + vy + " " + vz + " ");
		print(pressure + " ");
		print(soundspeed + " ");
		print(mach + " ");
		print(quantities[EquationsHydroIdeal.INDEX_ENERGY] + " ");
		print(angularmomentum + " ");
		print(entropy + " ");
		

		
	}

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		double vx, vy, vz, pressure, v2, rhoV2, soundspeed, energy, lastenergy = 0.0;
		int i, j, k;
		
		int[] iindices = new int[] {30, 33};
		int[] jindices = new int[] {30, 33};
		
		k = 0;
		for (int ii = 0; ii < iindices.length; ii++){
			i = iindices[ii];
			j = jindices[ii];
			
			
			vx = quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
			vy = quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
			vz = quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM]/quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
			v2 = vx*vx+vy*vy+vz*vz;
			rhoV2 = quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]*v2;
			energy = quantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY];
			
			System.err.println("");
			System.err.println("i = " + i + ", j = " + j);
			System.err.println("e = " + energy + ", rhov^2 = " + rhoV2);
			if (ii > 0){
				System.err.println("Difference in energy = " + (energy - lastenergy));
			}
			lastenergy = energy;
		
			pressure = ((EquationsHydroIdeal) equations).getPressure(energy, rhoV2);
		}
	}

}
