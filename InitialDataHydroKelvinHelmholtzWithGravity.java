
public class InitialDataHydroKelvinHelmholtzWithGravity extends InitialDataHydro {

	protected double vMagnitude, pressureTop, perpendicularVelocityPerturbation;
	protected double rhoTop, rhoBottom, pressureBottom, gy;
	
	public InitialDataHydroKelvinHelmholtzWithGravity(GridCartesian grid,
			EquationsHydroIdeal equations, double vMagnitude,
			double pressureTop, double perpendicularVelocityPerturbation, double rhoTop, double rhoBottom,
			double gy) {
		super(grid, equations);
		this.vMagnitude = vMagnitude;
		this.pressureTop = pressureTop;
		this.perpendicularVelocityPerturbation = perpendicularVelocityPerturbation;
		this.rhoTop = rhoTop;
		this.rhoBottom = rhoBottom;
		this.pressureBottom = pressureTop - (rhoTop + rhoBottom) * gy *  (grid.ymax() - grid.yMidpoint());
		this.gy = gy;
	}
	
	
	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		
		
		double rho;
		double vy = perpendicularVelocityPerturbation*Math.sin(g.getX()/0.5*Math.PI);
		if (g.getY() < grid.yMidpoint()){
		    rho = rhoBottom;
		    res[EquationsHydroIdeal.INDEX_RHO] = rhoBottom;
		    res[EquationsHydroIdeal.INDEX_XMOM] = rhoBottom*vMagnitude;
		    res[EquationsHydroIdeal.INDEX_YMOM] = rhoBottom*vy;
		    
		    res[EquationsHydroIdeal.INDEX_ENERGY] = 
					((EquationsHydroIdeal) equations).getEnergy(pressureTop - rhoTop * gy *  (grid.ymax() - grid.yMidpoint()) 
							- rhoBottom * gy *  (grid.yMidpoint() - g.getY()), rhoBottom*(vMagnitude*vMagnitude + vy*vy));
		} else {
			if ( Math.pow(g.getX() - 2.5, 2) + Math.pow(g.getY() - 0.8, 2) <= 0.08*0.08) {
				rho = rhoBottom;
			} else {
				rho = rhoTop;
			}
		    res[EquationsHydroIdeal.INDEX_RHO] = rho;
		    res[EquationsHydroIdeal.INDEX_XMOM] = -rho*vMagnitude;
		    res[EquationsHydroIdeal.INDEX_YMOM] = -rho*vy;
		    
		    res[EquationsHydroIdeal.INDEX_ENERGY] = 
					((EquationsHydroIdeal) equations).getEnergy(pressureTop - rho * gy * (grid.ymax() - g.getY()), rho*(vMagnitude*vMagnitude + vy*vy));
		}
		
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
		
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Kelvin Helmholtz instability setup with densities = " + rhoTop + ", " + rhoBottom + " and pressure at the top = " + pressureTop + 
				" and an initial perturbation in the perpendicular velocity";
	}

}
