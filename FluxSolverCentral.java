import matrices.SquareMatrix;


public class FluxSolverCentral extends FluxSolverRoe { 

	public FluxSolverCentral(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] val, int direction, double[] left, double[] right) {
		double waveSpeed;
		SquareMatrix mat[] = equations.diagonalization(i, j, k, val, direction);
		
		mat[1].absMe();
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			waveSpeed = Math.max(mat[1].value(ii, ii), waveSpeed);
		}
		
		return new UpwindingMatrixAndWaveSpeed(new SquareMatrix(val.length), waveSpeed);
	}


	@Override
	public String getDetailedDescription() {
		return "Central scheme";
	}

}
