
public class SourceRBV2 extends SourceMultiD {

	protected double[][][][][] average;
	protected boolean[][][] averagesSet;
	
	public SourceRBV2(GridCartesian2D grid, Equations equations) {
		super(grid, equations);
		
		average = new double[2][][][][];
		for (int i = 0; i < average.length; i++){ average[i] = grid.generateEmptyArray(equations.getNumberOfConservedQuantities()); }
		averagesSet = getInitializedAveragesSet(average[0].length, average[0][0].length, average[0][0][0].length, false);
	}

	

	@Override
	protected double[] getSourceTerm(double[] LT, double[] T,
			double[] RT, double[] L, double[] C, double[] R,
			double[] LB, double[] B, double[] RB,
			GridCell g, double time, double dt, int timeIntegratorStep) {
		
		
		int i = g.i(); int j = g.j(); int k = g.k();
		int quantities = equations.getNumberOfConservedQuantities();
		double averageCurrent;
		double[] res = new double[quantities];
		double averagePrevious, averageOld;
		
		for (int q = 0; q < quantities; q++){ 
			averageCurrent = average(
					average(LT[q], T[q], RT[q]), average(L[q], C[q], R[q]), average(LB[q], B[q], RB[q])
					);
			
			if (timeIntegratorStep == 2){
				if (averagesSet[i][j][k]){
					average[0][i][j][k][q] = average[1][i][j][k][q];
					averagesSet[i][j][k] = true;
				} else { // is this the correct choice?
					average[0][i][j][k][q] = averageCurrent;
				}
				average[1][i][j][k][q] = averageCurrent;
			}
			
			averagePrevious = average[1][i][j][k][q];
			averageOld      = average[0][i][j][k][q];
			
			res[q] = (1.5*averageCurrent - 2.0*averagePrevious + 0.5*averageOld)/dt;
		}
		
		return res;
	}

	public boolean[][][] getInitializedAveragesSet(int dimX, int dimY, int dimZ, boolean value){
		boolean[][][] averagesSet = new boolean[dimX][dimY][dimZ];
		for (int i = 0; i < averagesSet.length; i++){
			for (int j = 0; j < averagesSet[i].length; j++){
				for (int k = 0; k < averagesSet[i][j].length; k++){
					averagesSet[i][j][k] = value;
				}	
			}	
		}
		return averagesSet;
	}
	
	@Override
	public String getDetailedDescription() {
		return "Source terms for the implementation of the RBV2 scheme";
	}

}
