import matrices.DiagonalMatrix;
import matrices.SquareMatrix;

public class EquationsAcousticSimple extends EquationsAcoustic {

	// TODO implement a superclass EquationsLinear which implement all the signJx, ... matrices
	// such that they do not need to be computed each time
	
	protected boolean symmetric;
	protected double vx, vy, vz;
	
	public EquationsAcousticSimple(Grid grid, int numberOfPassiveScalars, double soundspeed, boolean symmetric, double[] v) {
		super(grid, numberOfPassiveScalars, soundspeed);

		this.symmetric = symmetric;
		this.vx = v[0];
		this.vy = v[1];
		this.vz = v[2];
	}

	public boolean symmetric() { return symmetric; }
	
	public static int indexVelocity(int direction){
		return direction;
	}
	
	public double vx(){ return vx; }
	public double vy(){ return vy; }
	public double vz(){ return vz; }
		
	public int getNumberOfConservedQuantities() {
		return 4;
	}

	public String getDetailedDescription() {
		return "Acoustic system (derived as the p-v subsystem of linearized hydrodynamics) with a constant soundspeed of " + 
				soundspeed + " and an advection velocity (" + vx + ", " + vy + ", " + vz + ")";
	}
	
	public SquareMatrix getJacobian(int direction){
		double vDir = 0.0;
		int pos = 0; // to make compiler happy
		if (direction == GridCartesian.X_DIR){ pos = 0; vDir = vx; }
		if (direction == GridCartesian.Y_DIR){ pos = 1; vDir = vy; }
		if (direction == GridCartesian.Z_DIR){ pos = 2; vDir = vz; }
		
		double[][] J = new double[4][4];
		
		J[0][0] = vDir;
		J[1][1] = vDir;
		J[2][2] = vDir;
		J[3][3] = vDir;

		
		if (symmetric){
			J[pos][3] = soundspeed;
			J[3][pos] = soundspeed;
		} else {			
			J[pos][3] = 1.0;
			J[3][pos] = soundspeed*soundspeed;
		}
		
				
		return new SquareMatrix(J);
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		SquareMatrix[] mat = new SquareMatrix[3];
		double arrmat[][][] = new double[2][4][4];
		double[] eval = new double[4];
		
		int pos = 0; // to make compiler happy
		double vDir = 0.0;
		double c = soundspeed;
		
		if (direction == GridCartesian.X_DIR){ pos = 0; vDir = vx; }
		if (direction == GridCartesian.Y_DIR){ pos = 1; vDir = vy; }
		if (direction == GridCartesian.Z_DIR){ pos = 2; vDir = vz; }
		
		for (int ii = 0; ii <= 3; ii++){
			for (int jj = 0; jj <= 3; jj++){
				arrmat[0][ii][jj] = 0.0;
				arrmat[1][ii][jj] = 0.0;
			}
			eval[ii] = vDir;
		}
		
		arrmat[0][0][0] = 1.0;
		arrmat[0][1][1] = 1.0;
		arrmat[0][2][2] = 1.0;

		arrmat[1][0][0] = 1.0;
		arrmat[1][1][1] = 1.0;
		arrmat[1][2][2] = 1.0;

		arrmat[0][  3][pos] =  1.0;
		arrmat[0][  3][  3] =  1.0;

		arrmat[1][pos][  3] =  0.5;
		arrmat[1][  3][  3] =  0.5;

		eval[pos] += -c;
		eval[3]   +=  c;
		if (symmetric){
			arrmat[0][pos][pos] = -1.0;
			arrmat[0][pos][  3] =  1.0;
			
			arrmat[1][pos][pos] = -0.5;
			arrmat[1][  3][pos] =  0.5;
		} else {
			arrmat[0][pos][pos] = -1.0/c;
			arrmat[0][pos][  3] =  1.0/c;
			
			arrmat[1][pos][pos] = -0.5*c;
			arrmat[1][  3][pos] =  0.5*c;
		}
		
		//eval[pos] += vDir;
		//eval[3]   += vDir;

		mat[0] = new SquareMatrix(arrmat[0]);
		mat[1] = new DiagonalMatrix(eval);
		mat[2] = new SquareMatrix(arrmat[1]);

		return mat;
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k, double[] quantities,
			int direction) {
		if (direction == GridCartesian.X_DIR){ return vx(); }
		if (direction == GridCartesian.Y_DIR){ return vy(); }
		if (direction == GridCartesian.Z_DIR){ return vz(); } else {return 0.0;} 
	}
	
	@Override
	public void transformFixedParameters(double[] normalVector) {
		double[] transformedVelocity = transformVector(vx, vy, vz, normalVector);
		vx = transformedVelocity[0]; vy = transformedVelocity[1]; vz = transformedVelocity[2];
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		double[] transformedVelocity = transformVectorBack(vx, vy, vz, normalVector);
		vx = transformedVelocity[0]; vy = transformedVelocity[1]; vz = transformedVelocity[2];
	}

	public double[] evolve1d(double pos, double time, ReconstructionParabolic recon, int direction, 
			GridEdgeMapped edge){
		double[] qLeft  = recon.eval(pos - time * soundspeed, direction, edge);
		double[] qRight = recon.eval(pos + time * soundspeed, direction, edge);
		double[] res = new double[qLeft.length];
		int vIndex = 0; // to make compiler happy
		if      (direction == GridCartesian.X_DIR){ vIndex = INDEX_VX; }
		else if (direction == GridCartesian.Y_DIR){ vIndex = INDEX_VY; }
		else if (direction == GridCartesian.Z_DIR){ vIndex = INDEX_VZ; }
		
		res[INDEX_P] = 0.5*(qLeft[INDEX_P] + qRight[INDEX_P]) - 0.5*soundspeed*(qRight[vIndex] - qLeft[vIndex]);
		res[vIndex]  = 0.5*(qLeft[vIndex] + qRight[vIndex]) - 0.5*(qRight[INDEX_P] - qLeft[INDEX_P])/soundspeed;
		// TODO fill other components?
		
		return res;
	}


	
}
