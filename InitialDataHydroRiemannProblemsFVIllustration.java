
public class InitialDataHydroRiemannProblemsFVIllustration extends InitialDataHydro {

	public InitialDataHydroRiemannProblemsFVIllustration(Grid grid, EquationsHydro equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
    	double pressure, rhoV2, v;
    	double x = (g.getX() - grid.xmin())/(grid.xmax() - grid.xmin());
    	double y = (g.getY() - grid.ymin())/(grid.ymax() - grid.ymin());
    	
    	boolean useSlopes = false;
    	double slopeRho = 0, slopeV = 0, slopeP = 0;
    	
    	double rho1 = 1.0;
    	double v1 = 0.0;
    	double p1 = 1.0;
    			
    	double rho2 = 0.125;
    	double v2 = 0.0;
    	double p2 = 0.1;
    			
    	double rho3 = 0.7;
    	double v3 = 1.5;
    	double p3 = 0.5;
    			
    	double rho4 = 1.3;
    	double v4 = -0.4;
    	double p4 = 1.0;
    			
    	
    	if (x < 0.3) {
    		if (useSlopes) { slopeRho = -0.1; slopeV = 0.0; slopeP = -0.3; }
    		
    		res[EquationsHydroIdeal.INDEX_RHO] = rho1 + slopeRho * (x - 0.4);
    		pressure = p1 + slopeP * (x - 0.4); v = v1 + slopeV * (x - 0.4);
    	} else if (x < 0.5) {
    		if (useSlopes) { slopeRho = 0.0; slopeV = 0.0; slopeP = 0.0; }

    		res[EquationsHydroIdeal.INDEX_RHO] = rho2 + slopeRho * (x - 0.6);
    		pressure = p2 + slopeP * (x - 0.6); v = v2 + slopeV * (x - 0.6);
    	} else if (x < 0.7) {
    		if (useSlopes) { slopeRho = 1.8; slopeV = -1.4; slopeP = 1.3; }

    		res[EquationsHydroIdeal.INDEX_RHO] = rho3 + slopeRho * (x - 0.8);
    		pressure = p3 + slopeP * (x - 0.8); v = v3 + slopeV * (x - 0.8);
    	} else {
    		if (useSlopes) { slopeRho = 0.5; slopeV = -0.9; slopeP = 1.3; }

    		res[EquationsHydroIdeal.INDEX_RHO] = rho4 + slopeRho * (x - 1);
    		pressure = p4 + slopeP * (x - 1); v = v4 + slopeV * (x - 1);
    	} 
    	
		res[EquationsHydroIdeal.INDEX_XMOM] = v*res[EquationsHydroIdeal.INDEX_RHO];
    	res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    				
    	rhoV2 = Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2)/res[EquationsHydroIdeal.INDEX_RHO];
    	
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    		
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Collection of Riemann problems to illustrate the ideas of the Godunov method";
	}

}
