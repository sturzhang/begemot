
public abstract class SourceMultiD extends Source {

	public SourceMultiD(GridCartesian grid, Equations equations) {
		super(grid, equations);
		if (grid.dimensions() > 2){
			System.err.println("ERROR: Multi-d sources only implemented for 2d grids!");
		}
	}

	protected abstract double[] getSourceTerm(double[] lefttop, double[] top, double[] righttop,
			double[] left, double[] center, double[] right, 
			double[] leftbottom, double[] bottom, double[] rightbottom, 
			GridCell g, double time, double dt, int timeIntegratorStep);

	@Override
	public double[][][][] get(double[][][][] quantities, double[][][][] sources, double time, double dt, int timeIntegratorStep) {
    	GridCartesian2D grid = ((GridCartesian2D) this.grid);
		int i,j,k;
    	
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
    		sources[g.i()][g.j()][g.k()] = getSourceTerm(
    				quantities[i-1][j+1][k],quantities[i][j+1][k],quantities[i+1][j+1][k],  
    				quantities[i-1][j][k],  quantities[i][j  ][k],quantities[i+1][j  ][k],
    				quantities[i-1][j-1][k],quantities[i][j-1][k],quantities[i+1][j-1][k],    				
    				g, time, dt, timeIntegratorStep);
    	}
		
		return sources;

	}

}
