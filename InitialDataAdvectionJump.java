
public class InitialDataAdvectionJump extends InitialDataScalarAdvection {

	
	public InitialDataAdvectionJump(Grid grid, EquationsAdvection equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		double x = g.getX() - grid.xMidpoint();
		//double x = g.getY() - grid.yMidpoint();
		
		if (x < 0){
			res[0] = 1.0;
		} else {
			res[0] = 0.0;
		}
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Initial data for scalar advection with a grid-centered jump";
	}

}
