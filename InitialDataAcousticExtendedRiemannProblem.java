
public class InitialDataAcousticExtendedRiemannProblem extends
		InitialDataAcousticExtended {

	public InitialDataAcousticExtendedRiemannProblem(Grid grid,
			EquationsAcousticExtended equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double r;
    	
    	r = Math.sqrt(Math.pow(g.getX() - grid.xMidpoint(), 2) + 
    			      Math.pow(g.getY() - grid.yMidpoint(), 2));
    			
    	if (r < (grid.xmax() - grid.xmin())/4){
    	//if (Math.abs(g.getX() - grid.xMidpoint()) < 1e-3){
    		res[EquationsAcousticExtended.INDEX_RHO] = 1.0;
    		res[EquationsAcousticExtended.INDEX_P]   = 0.0;
    		res[EquationsAcousticExtended.INDEX_VX]  = 0.0;
        } else {
        	res[EquationsAcousticExtended.INDEX_RHO] = 0.0;
        	res[EquationsAcousticExtended.INDEX_P]   = 0.0;    					
    		res[EquationsAcousticExtended.INDEX_VX]  = 0.0;
        }
    	res[EquationsAcousticExtended.INDEX_VY] = 0.0;
    	res[EquationsAcousticExtended.INDEX_VZ] = 0.0;
    	
    	
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Riemann Problem";
	}

}
