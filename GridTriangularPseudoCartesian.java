import java.util.LinkedList;


public class GridTriangularPseudoCartesian extends GridTriangular {

	protected double cellSize;
    protected GridCartesian2D myCartesianGrid;
	
    protected int LEFT  = 0;
    protected int RIGHT = 1;
    
    protected int[][]   cartesianIndex;
    protected int[]     triangleLocation;
    protected int[][]   left, right;
    protected double diagonalLength;
    protected double[] unitNormalVectorFromLeftToRight;
    
    protected int firstValidCell;
	protected int lastValidCell;
	
	@SuppressWarnings("unchecked")
	public GridTriangularPseudoCartesian(double xmin, double xmax, int nx, 
			double ymin, double ymax, int ny, int ghostcells) {
    	super();
		
    	myCartesianGrid = new GridCartesian2D(xmin, xmax, nx, ymin, ymax, ny, ghostcells);
		myCartesianGrid.initialize(myObstacle);
		numberOfCells = (myCartesianGrid.indexMaxXIncludingGhostcells()+1)*
				(myCartesianGrid.indexMaxYIncludingGhostcells()+1)*2;
    	
		cellSize = myCartesianGrid.xSpacing()*myCartesianGrid.ySpacing()/2;
    	diagonalLength = Math.sqrt(myCartesianGrid.xSpacing()*myCartesianGrid.xSpacing() + 
    			myCartesianGrid.ySpacing()*myCartesianGrid.ySpacing());
    	unitNormalVectorFromLeftToRight = new double[] { myCartesianGrid.ySpacing()/diagonalLength, 
    													 myCartesianGrid.xSpacing()/diagonalLength, 
    													 0.0 };
    	
    	cartesianIndex   = new int[numberOfCells][2];
    	triangleLocation = new int[numberOfCells];
    	validCell        = new boolean[numberOfCells];
    	left             = new int[myCartesianGrid.indexMaxXIncludingGhostcells()+1][myCartesianGrid.indexMaxYIncludingGhostcells()+1];
    	right            = new int[myCartesianGrid.indexMaxXIncludingGhostcells()+1][myCartesianGrid.indexMaxYIncludingGhostcells()+1];
    	// workaround from http://stackoverflow.com/questions/13047123/workaround-for-generic-arrays-in-java
    	neighbours       = (LinkedList<GridCell>[]) new LinkedList<?>[numberOfCells];
    	
    	boolean validCellFound = false;
    	
    	firstValidCell = 0;
    	lastValidCell = 0;
    	
    	int k = 0;
    	for (int i = 0; i <= myCartesianGrid.indexMaxXIncludingGhostcells(); i++){
    		for (int j = 0; j <= myCartesianGrid.indexMaxYIncludingGhostcells(); j++){
    			cartesianIndex[k  ][0] = i;
    			cartesianIndex[k  ][1] = j;
    			cartesianIndex[k+1][0] = i;
    			cartesianIndex[k+1][1] = j;
    			
    			triangleLocation[k]   = LEFT;
    			left[i][j]            = k;
    			triangleLocation[k+1] = RIGHT;
    			right[i][j]           = k+1;

    			validCell[k]   = myCartesianGrid.isValidCell(i, j, 0);
    			if ( (!validCellFound) && (validCell[k])){ validCellFound = true; firstValidCell = k; }
    			validCell[k+1] = validCell[k];
    			if (validCell[k+1]){ lastValidCell = k+1; }
    			
    			k+=2;
    		}
    	}
    	

	}

	@Override
	public double cellSize(GridCell cell) { return cellSize; }

	@Override
	public double edgeSize(GridEdge edge) {
		int k1 = edge.adjacent1().k();
		int i1 = cartesianIndex[k1][0];
		int j1 = cartesianIndex[k1][1];
		
		int k2 = edge.adjacent2().k();
		int i2 = cartesianIndex[k2][0];
		int j2 = cartesianIndex[k2][1];
		
		
		     if ((i1 == i2) && (j1 == j2)){ return diagonalLength; }
		else if ((i1 != i2) && (j1 == j2)){ return myCartesianGrid.ySpacing(); }
		else if ((i1 == i2) && (j1 != j2)){ return myCartesianGrid.xSpacing(); }
		else { System.err.println("ERROR: Asked for edge length between non-adjacent cells! "); return 0.0; }
				     
	}

	@Override
	public double[] normalVector(GridEdge edge) {
		int k1 = edge.adjacent1().k();
		int i1 = cartesianIndex[k1][0]; int j1 = cartesianIndex[k1][1];
		
		int k2 = edge.adjacent2().k(); 
		int i2 = cartesianIndex[k2][0]; int j2 = cartesianIndex[k2][1];
		
		double sign = 1.0;
		if ((i1 == i2) && (j1 == j2)){
			if (triangleLocation[k1] == RIGHT){ sign = -1.0; }
			return new double[] { sign*unitNormalVectorFromLeftToRight[0], 
								  sign*unitNormalVectorFromLeftToRight[1], 
								  sign*unitNormalVectorFromLeftToRight[2]}; 
		} else if ((i1 != i2) && (j1 == j2)){ 
			if (triangleLocation[k1] == LEFT){ sign = -1.0; }
			return new double[] { sign, 0.0, 0.0 }; 
		}
		else if ((i1 == i2) && (j1 != j2)){ 
			if (triangleLocation[k1] == LEFT){ sign = -1.0; }
			return new double[] { 0.0, sign, 0.0 }; 
		}
		else { System.err.println("ERROR: Asked for normal Vector between non-adjacent cells! "); return null; }
				    
	}

	@Override
	public double minSpacing() { return myCartesianGrid.minSpacing(); }

	@Override
	public String getDetailedDescription() {
		// TODO continue here!
		return "pseudo-cartesian grid with a general setup just as: \n " + 
				myCartesianGrid.getDetailedDescription();
	}

	@Override
	protected int indexMin() { return firstValidCell; }
	@Override
	protected int indexMax() { return lastValidCell; }

	@Override
	protected void setNeighbours() {
		int i, j;
    	for (int k = 0; k < triangleLocation.length; k++){
    		i = cartesianIndex[k][0];
    		j = cartesianIndex[k][1];
    		
    		neighbours[k] = new LinkedList<GridCell>();
    		
    		if (triangleLocation[k] == LEFT){
				// the if conditions are not dramatic, as in case i = 0 this is a ghostcell 
    			// and will never be wanting to know its neighbours
    			
    			            neighbours[k].add(new GridCell(0, 0, right[i  ][j], this));
    			if (i > 0){ neighbours[k].add(new GridCell(0, 0, right[i-1][j], this)); }
    			if (j > 0){ neighbours[k].add(new GridCell(0, 0, right[i][j-1], this)); }
    		} else {
    			    neighbours[k].add(new GridCell(0, 0, left[i  ][j], this));
    			if (i < myCartesianGrid.indexMaxXIncludingGhostcells()){ 
    				neighbours[k].add(new GridCell(0, 0, left[i+1][j], this)); }
    			if (j < myCartesianGrid.indexMaxYIncludingGhostcells()){ 
    				neighbours[k].add(new GridCell(0, 0, left[i][j+1], this)); }
    		}
    	}
	}
 
	@Override
	public double getX(int i, int j, int k) {
		double offset = (triangleLocation[k]==LEFT ? -myCartesianGrid.xSpacing()/3 : myCartesianGrid.xSpacing()/3);
		return myCartesianGrid.getX(cartesianIndex[k][0], cartesianIndex[k][1], 0)+offset;
	}

	@Override
	public double getY(int i, int j, int k) {
		double offset = (triangleLocation[k]==LEFT ? -myCartesianGrid.ySpacing()/3 : myCartesianGrid.ySpacing()/3);
		return myCartesianGrid.getY(cartesianIndex[k][0], cartesianIndex[k][1], 0)+offset;
	}

	@Override
	public double getZ(int i, int j, int k) {
		return myCartesianGrid.getZ(cartesianIndex[k][0], cartesianIndex[k][1], 0);
	}

    @Override
    public double xmax(){ return myCartesianGrid.xmax(); }
    @Override
    public double xmin(){ return myCartesianGrid.xmin(); }
    @Override
    public double ymax(){ return myCartesianGrid.ymax(); }
    @Override
    public double ymin(){ return myCartesianGrid.ymin(); }




}
