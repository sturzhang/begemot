import matrices.SquareMatrix;


public class FluxSolverRusanovMultiD extends FluxSolverOneStepMultiD {

	public FluxSolverRusanovMultiD(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		
		double[] diff = new double[ijk.length];
		double[] perp = new double[ijk.length];
		double[] central = new double[ijk.length];
		
		
		
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		
		SquareMatrix[] matleft  = equations.diagonalization(i, j, k, ijk,  direction);
		SquareMatrix[] matright = equations.diagonalization(i, j, k, ip1jk, direction);
		double waveSpeed;
		
		waveSpeed = 0.0;
		for (int ii = 0; ii < matleft[1].rows(); ii++){
			waveSpeed = Math.max( matleft[1].value(ii, ii), waveSpeed);
			waveSpeed = Math.max(matright[1].value(ii, ii), waveSpeed);
		}

		double[] fluxLeft = equations.fluxFunction(i, j, k, ijk, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, ip1jk, direction);
		
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, ijp1k, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, ijm1k, direction);
		
		double[] fluxRightTop = equations.fluxFunction(i, j, k, ip1jp1k, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, direction);

		for (int q = 0; q < ijk.length; q++){
			/*diff[q] = right[q] - left[q];
			central[q] = 0.5*(fluxLeft[q] + fluxRight[q]); //*/
			
			diff[q] = 0.5*(ip1jk[q] - ijk[q]) + 0.25*(ip1jp1k[q] - ijp1k[q] + ip1jm1k[q] - ijm1k[q]);
			perp[q] = 0.125*(ip1jp1k[q] + ijp1k[q] - ip1jm1k[q] - ijm1k[q]);
			central[q] = 0.125*(fluxLeftTop[q] + 2.0*fluxLeft[q] + fluxLeftBottom[q] 
					+ fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q]); //*/
		}
		
		for (int ii = 0; ii < matleft[1].rows(); ii++){
			matleft[1].setElement(ii, ii, waveSpeed);
		}
		matleft[1].setElement(EquationsHydroIdeal.INDEX_YMOM, EquationsHydroIdeal.INDEX_YMOM, 0);
		matleft[1].setElement(EquationsHydroIdeal.INDEX_ZMOM, EquationsHydroIdeal.INDEX_ZMOM, 0); //*/
			
		double[] upwindingTerm = matleft[1].mult(0.5).mult(diff);
	    
    	double[] fluxRes = new double[ijk.length];
    	    	
    	for (int q = 0; q < ijk.length; q++){
    		fluxRes[q] = central[q] - upwindingTerm[q];
    	}
    	
		return new FluxAndWaveSpeed(fluxRes, waveSpeed);
	}

	@Override
	public String getDetailedDescription() {
		return "Rusanov solver with a multi-d extension that makes the diffusion of the vector become a gradient";
	}

}
