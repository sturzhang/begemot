
public class ObstacleAcousticCylinder extends ObstacleAcoustic {

	protected ObstacleHydroCylinder myCylinder;
	
	public ObstacleAcousticCylinder(GridCartesian grid, EquationsAcousticSimple equations, double radius) {
		super(grid, equations);
		
		// multiple inheritance workaround
		myCylinder = new ObstacleHydroCylinder(grid, new EquationsHydroIdeal(grid, 0, 1.4), radius);  
	}

	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {
		myCylinder.setObstacleInterior(excludeFromTimeEvolution);
	}

	@Override
	public String getDetailedDescription() {
		return "obstacle in the form of a circle in the x-y-plane centered on the grid and with radius = " + myCylinder.radius();
	}

}
