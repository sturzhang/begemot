
public abstract class FluxSolverVertex extends FluxSolverOneStepMultiD {

	public FluxSolverVertex(GridCartesian2D grid, Equations equations) {
		super(grid, equations);
	}
	
	// TODO add passive scalar
	/*@Override
	protected double[] fluxPassiveScalar(double[] left, double[] right, int direction) {
		FluxSolverRoeAcoustic usualSolver = new FluxSolverRoeAcoustic(grid, (EquationsAcoustic) equations);
		return usualSolver.fluxPassiveScalarAcoustic(left, right, direction);
	}*/
	
	// TODO this implementation of passive scalar is the same as in FluxSolverRoe -- change it to be multi-d
	@Override
	protected double[] fluxPassiveScalar(GridCell g, GridEdgeMapped gridEdge, GridCell left, GridCell right, int direction) {
		//protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right, int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double vLeft = equations.advectionVelocityForPassiveScalar(left.i(),left.j(),left.k(),
				left.interfaceValue(), direction);
		double vRight = equations.advectionVelocityForPassiveScalar(right.i(),right.j(),right.k(),
				right.interfaceValue(), direction);
		double absMeanV = 0.5 * Math.abs(vLeft + vRight);
		
		
		for (int ii = 0; ii < res.length; ii++){
			s = sc[ii];
			// TODO there was momentum mLeft and mRight standing in the flux average before
			res[ii] = 0.5*(left.interfaceValue()[s]*vLeft + right.interfaceValue()[s]*vRight) 
					- 0.5 * absMeanV * (right.interfaceValue()[s] - left.interfaceValue()[s]);
		}
		return res;
	}
		
	
	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1,
			int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		
		
		
		VertexFluxAndWaveSpeed fluxAndWaveSpeedTop, fluxAndWaveSpeedBottom;
		double[][] cornerFluxes;
		double[] resFlux;
		
		if (grid.dimensions() >= 3){ 
			System.err.println("Vertex fluxes not implemented for 3-d");
		}		

		
		/* 	for a short moment, cornerFluxes[i][j][k] store the vertex flux at
			(i-12, j-12), with every cornerFluxes[i][j][k] being an array of the structure
			[dir][q], i.e. the flux in direction dir of quantity q
		*/
		
		// double[] NE, double[] NW, double[] SW, double[] SE
		
		// TODO here we do work twice!
		fluxAndWaveSpeedTop = vertexflux(ip1jp1k, ijp1k, ijk, ip1jk);
		fluxAndWaveSpeedBottom = vertexflux(ip1jk, ijk, ijm1k, ip1jm1k);
		
		resFlux = new double[fluxAndWaveSpeedTop.flux()[0].length];
		for (int q = 0; q < resFlux.length; q++){
			resFlux[q] = 0.5*(fluxAndWaveSpeedTop.flux[GridCartesian.X_DIR][q] + fluxAndWaveSpeedBottom.flux[GridCartesian.X_DIR][q]);
		}
		
			
		// TODO make wavespeeds consistent
		return new FluxAndWaveSpeed(resFlux, fluxAndWaveSpeedTop.waveSpeeds()[0]);
	}
	
	
	protected abstract VertexFluxAndWaveSpeed vertexflux(double[] NE, double[] NW, double[] SW, double[] SE);
	
	//@Override
    public double[][][][][] getAndSetWaveSpeedsOld(double[][][][][] interfaceValues, double[][][][][] interfaceFluxes, double[][][][] localWaveSpeed){
    	// this is a problem here -- we would need values somewhere inside the cell, and not the interpolated values next to the boundary
    	GridCartesian myGrid = (GridCartesian) grid;
		
		VertexFluxAndWaveSpeed fluxAndWaveSpeed;
		double[][][][][] cornerFluxes = new double[interfaceFluxes.length][interfaceFluxes[0].length][interfaceFluxes[0][0].length][interfaceFluxes[0][0][0].length][interfaceFluxes[0][0][0][0].length];
		
    	for (int i = myGrid.indexMinX(); i <= myGrid.indexMaxX()+1; i++){
    		for (int j = myGrid.indexMinY(); j <= Math.min(myGrid.indexMaxY()+1, myGrid.lastGhostCellYIndex()); j++){
    			for (int k = myGrid.indexMinZ(); k <= Math.min(myGrid.indexMaxZ()+1, myGrid.lastGhostCellZIndex()); k++){

    					/* 	for a short moment, cornerFluxes[i][j][k] store the vertex flux at
    						(i-12, j-12), with every cornerFluxes[i][j][k] being an array of the structure
    						[dir][q], i.e. the flux in direction dir of quantity q
    					*/
    				fluxAndWaveSpeed = vertexflux( 
						interfaceValues[i][j  ][k][GridCartesian.Interface_RIGHT], 
						interfaceValues[i-1][j][k][GridCartesian.Interface_LEFT], 
						interfaceValues[i-1][j-1][k][GridCartesian.Interface_RIGHT], 
						interfaceValues[i  ][j-1][k][GridCartesian.Interface_LEFT]);
    				
    				cornerFluxes[i][j][k] = fluxAndWaveSpeed.flux();
    				
    				// TODO here the localWaveSpeeds are not set for every boundary
    				localWaveSpeed[i][j][k] = fluxAndWaveSpeed.waveSpeeds(); 
    				
    				for (int s = 0; s < localWaveSpeed[i][j][k].length; s++){
    					chooseCurMaxSignalSpeed(localWaveSpeed[i][j][k][s]);
    				}
    				
    				if (myGrid.dimensions() >= 3){ 
    					System.err.println("Vertex fluxes not implemented for 3-d");
    				}		
    			}
    		}
    	}

    	double fluxX, fluxY;
    	
    	for (int i = myGrid.indexMinX(); i <= myGrid.indexMaxX()+1; i++){
    		for (int j = myGrid.indexMinY(); j <= Math.min(myGrid.indexMaxY()+1, myGrid.lastGhostCellYIndex()); j++){
    			for (int k = myGrid.indexMinZ(); k <= Math.min(myGrid.indexMaxZ()+1, myGrid.lastGhostCellZIndex()); k++){
    				// combine vertex fluxes into interface fluxes by averaging
    				
    				for (int q = 0; q < interfaceFluxes[i][j][k][0].length; q++){
    					fluxX = (cornerFluxes[i][j][k][0][q] + cornerFluxes[i][j+1][k][0][q]) / 2;
    					fluxY = (cornerFluxes[i][j][k][1][q] + cornerFluxes[i+1][j][k][1][q]) / 2;
    					    					
    					interfaceFluxes[i][j][k][0][q] = -myGrid.edgeSize(GridCartesian.X_DIR)*fluxX;
    					interfaceFluxes[i][j][k][1][q] = -myGrid.edgeSize(GridCartesian.Y_DIR)*fluxY;
    					
    					interfaceFluxes[i-1][j][k][0+2][q] = -interfaceFluxes[i][j][k][0][q];
    					interfaceFluxes[i][j-1][k][1+2][q] = -interfaceFluxes[i][j][k][1][q];
    				}
    			}
    		}
    	}
    	
    	return interfaceFluxes;

    }
}
