
public class InitialDataAcousticSixRegionsRiemannProblem extends InitialDataAcoustic {

	protected double[] lefttop;
	protected double[] left;
	protected double[] leftbottom;
	
	protected double[] righttop;
	protected double[] right;
	protected double[] rightbottom;
	
	public InitialDataAcousticSixRegionsRiemannProblem(Grid grid, EquationsAcousticSimple equations,
			double[] lefttop, double[] left,
			double[] leftbottom, double[] righttop, double[] right,
			double[] rightbottom) {
		super(grid, equations);
		this.left = left;
		this.right = right;
		this.lefttop = lefttop;
		this.righttop = righttop;
		this.leftbottom = leftbottom;
		this.rightbottom = rightbottom;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double x = g.getX();
		double y = g.getY();
		
		if (x < 0){
			if (y > 0.5){
				return new double[] {lefttop[0], lefttop[1], lefttop[2], lefttop[3]};
			} else if (y > -0.5){
				return new double[] {left[0], left[1], left[2], left[3]};
			} else {
				return new double[] {leftbottom[0], leftbottom[1], leftbottom[2], leftbottom[3]};
			}
		} else {
			if (y > 0.5){
				return new double[] {righttop[0], righttop[1], righttop[2], righttop[3]};
			} else if (y > -0.5){
				return new double[] {right[0], right[1], right[2], right[3]};
			} else {
				return new double[] {rightbottom[0], rightbottom[1], rightbottom[2], rightbottom[3]};
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Six regions as they appear in a cell-interface Riemann problem.";
	}

}
