
public class InitialDataMaxwellZero extends InitialDataMaxwell {

	public InitialDataMaxwellZero(Grid grid, EquationsMaxwell equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[]res = new double[q];
		
		res[EquationsMaxwell.INDEX_EX] = 0;
		res[EquationsMaxwell.INDEX_EY] = 0;
		res[EquationsMaxwell.INDEX_EZ] = 0;
		
		res[EquationsMaxwell.INDEX_BX] = 0;
		res[EquationsMaxwell.INDEX_BY] = 0;
		res[EquationsMaxwell.INDEX_BZ] = 0;
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "vacuum initial data";
	}

}
