import java.util.LinkedList;

public class Grid1D extends GridCartesian {

    public Grid1D(double xmin, double xmax, int nx, int ghostcellsX){
    	this.xmin = xmin;
    	this.xmax = xmax;
    	
    	this.ghostcellsX = ghostcellsX;
    	this.nx = nx;

    	ny = 1;
    	nz = 1;
    	ghostcellsY = 0;
    	ghostcellsZ = 0;
    }

    public int dimensions(){ return 1; }
    
    public String getDetailedDescription(){
    	return "(1-d grid)";
    }

	
    

}