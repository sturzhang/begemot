
public class InitialDataHydroTaylorGreen extends InitialDataHydro {


	protected double mach;
	
	public InitialDataHydroTaylorGreen(GridCartesian2D grid, EquationsHydro equations, double mach) {
		super(grid, equations);
		this.mach = mach;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x, y;
		double rhoV2;
		
		x = (g.getX() - grid.xmin())/(grid.xmax() - grid.xmin());
		y = (g.getY() - grid.yMidpoint())/(grid.ymax() - grid.ymin());
		
		if ((grid.xmax() - grid.xmin()) != (grid.ymax() - grid.ymin())){
			System.err.println("WARNING: The Taylor-Green vortex should be run on a square domain!");
		}
		
		double waveNumber = 1.0;
		double p0 = (1.0 / ((EquationsHydroIdeal) equations).gamma() / mach/mach - 0.5); //100.0;
		
		double u = Math.sin(2.0 * Math.PI * waveNumber * x) * Math.cos(2.0*Math.PI * waveNumber * y);
		double v = -Math.cos(2.0 * Math.PI * waveNumber * x) * Math.sin(2.0 * Math.PI * waveNumber * y);
		
		res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
		
		res[EquationsHydroIdeal.INDEX_XMOM] = res[EquationsHydroIdeal.INDEX_RHO] * u;
		res[EquationsHydroIdeal.INDEX_YMOM] = res[EquationsHydroIdeal.INDEX_RHO] * v;
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;

		double pressure = p0 - 0.5 * (
				Math.sin(2.0 * Math.PI * waveNumber * x)*Math.sin(2.0 * Math.PI * waveNumber * x) + 
				Math.sin(2.0 * Math.PI * waveNumber * y)*Math.sin(2.0 * Math.PI * waveNumber * y));
		
		rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    	
		
    	res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    	
    	return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "Taylor Green vortex, see e.g. Shu et al., J.Sci.Comp. 24/1, 2005";
	}

}
