
public abstract class InitialDataScalar extends InitialData {

	protected EquationsScalar equations;
	
	public InitialDataScalar(Grid grid, EquationsScalar equations) {
		super(grid);
		this.equations = equations;
	}


}
