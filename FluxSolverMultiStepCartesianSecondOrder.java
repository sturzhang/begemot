
public abstract class FluxSolverMultiStepCartesianSecondOrder extends FluxSolverMultiStepCartesian {

	
	public FluxSolverMultiStepCartesianSecondOrder(GridCartesian grid, Equations equations) {
		super(grid, equations, new StencilTurnerOneDSecondOrder(grid));
	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep) {
		return Math.pow(dt, fluxSolverStep);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge,
			double[][] quantities, int timeIntegratorStep) {
		return flux(gridEdge, quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_LEFTLEFT], 
				quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_LEFT], 
				quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_RIGHT], 
				quantities[StencilTurnerOneDSecondOrder.NEIGHBOUR_RIGHTRIGHT]);
	}
	
	protected abstract FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge, 
			double[] leftleft, double[] left, double[] right, double[] rightright);

	

	
}
