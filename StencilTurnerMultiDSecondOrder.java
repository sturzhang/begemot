
public class StencilTurnerMultiDSecondOrder extends StencilTurner {

	public static final int NEIGHBOUR_LLTT = 0; 
	public static final int NEIGHBOUR_LLT  = 1;  
	public static final int NEIGHBOUR_LL   = 2; 
	public static final int NEIGHBOUR_LLB  = 3; 
	public static final int NEIGHBOUR_LLBB = 4; 

	public static final int NEIGHBOUR_LTT = 5;  // left top-top
	public static final int NEIGHBOUR_LT  = 6;  // left top
	public static final int NEIGHBOUR_L   = 7; // left
	public static final int NEIGHBOUR_LB  = 8; // left bottom
	public static final int NEIGHBOUR_LBB = 9; // left bottom-bottom

	public static final int NEIGHBOUR_RTT = 10; 
	public static final int NEIGHBOUR_RT  = 11;  
	public static final int NEIGHBOUR_R   = 12; 
	public static final int NEIGHBOUR_RB  = 13; 
	public static final int NEIGHBOUR_RBB = 14; 

	public static final int NEIGHBOUR_RRTT = 15; 
	public static final int NEIGHBOUR_RRT  = 16;  
	public static final int NEIGHBOUR_RR   = 17; 
	public static final int NEIGHBOUR_RRB  = 18; 
	public static final int NEIGHBOUR_RRBB = 19; 
	
	public StencilTurnerMultiDSecondOrder(GridCartesian grid) {
		super(grid);
		// TODO Auto-generated constructor stub
	}

	@Override
	public GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge) {
		// TODO duplicate code, same as in FluxSolverneStepMultiD
		
		int edgeDir = ((GridCartesian) grid).getDirectionPerpendicularToEdge(g, gridEdge.index());		
		GridCell nb = gridEdge.getOtherGridCell(g);
		int offsetX = 0, offsetY = 0;
		
		int offsetXpar = 0, offsetYpar = 0;
		if (edgeDir == GridCartesian.X_DIR){ offsetXpar = 1; }
		if (edgeDir == GridCartesian.Y_DIR){ offsetYpar = 1; }
		
		if (edgeDir == GridCartesian.X_DIR){ offsetY = 1; }
		if (edgeDir == GridCartesian.Y_DIR){ offsetX = -1; } // needed for rotation
		if (grid.dimensions() == 1){ offsetY = 0; }
		else if (grid.dimensions() > 2){
			System.err.println("ERROR: MultiDSecondOrder stencil turner not implemented for 3-d grids!"); 
		}
		
		return new GridCell[]{
				new GridCell(g.i()+2*offsetX-offsetXpar, g.j()+2*offsetY-offsetYpar, g.k(), grid),
				new GridCell(g.i()+  offsetX-offsetXpar, g.j()+  offsetY-offsetYpar, g.k(), grid),
				new GridCell(g.i()          -offsetXpar, g.j()+         -offsetYpar, g.k(), grid), 
				new GridCell(g.i()-  offsetX-offsetXpar, g.j()-  offsetY-offsetYpar, g.k(), grid),
				new GridCell(g.i()-2*offsetX-offsetXpar, g.j()-2*offsetY-offsetYpar, g.k(), grid),
				
				
				new GridCell(g.i()+2*offsetX, g.j()+2*offsetY, g.k(), grid),
				new GridCell(g.i()+  offsetX, g.j()+  offsetY, g.k(), grid),
				g, 
				new GridCell(g.i()-  offsetX, g.j()-  offsetY, g.k(), grid),
				new GridCell(g.i()-2*offsetX, g.j()-2*offsetY, g.k(), grid),
				
				
				new GridCell(nb.i()+2*offsetX, nb.j()+2*offsetY, nb.k(), grid),
				new GridCell(nb.i()+  offsetX, nb.j()+  offsetY, nb.k(), grid),
				nb, 
				new GridCell(nb.i()-  offsetX, nb.j()-  offsetY, nb.k(), grid),
				new GridCell(nb.i()-2*offsetX, nb.j()-2*offsetY, nb.k(), grid),
				
		
				new GridCell(nb.i()+2*offsetX+offsetXpar, nb.j()+2*offsetY+offsetYpar, nb.k(), grid),
				new GridCell(nb.i()+  offsetX+offsetXpar, nb.j()+  offsetY+offsetYpar, nb.k(), grid),
				new GridCell(nb.i()+         +offsetXpar, nb.j()+         +offsetYpar, nb.k(), grid), 
				new GridCell(nb.i()-  offsetX+offsetXpar, nb.j()-  offsetY+offsetYpar, nb.k(), grid),
				new GridCell(nb.i()-2*offsetX+offsetXpar, nb.j()-2*offsetY+offsetYpar, nb.k(), grid)  };
				
	}

}
