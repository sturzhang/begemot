
public class OutputSingleCell extends Output {

	protected int i, j, k;
	
	public OutputSingleCell(double outputTimeInterval, GridCartesian grid,
			EquationsHydroIdeal equations, int i, int j, int k) {
		super(outputTimeInterval, grid, equations);
		this.i = i; this.j = j; this.k = k;
	}

	@Override
	public String getDetailedDescription() {
		return "Prints the contents of just cell (" + i + ", " + j + ", " + k + ").";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {

		//printCell(time, quantities[i][j][k], i, j, k);
		printCell(time, quantities[i+1][j][k], i+1, j, k);
		
		/* printCell(time, quantities[i+1][j][k], i+1, j, k);
		printCell(time, quantities[i-1][j][k], i-1, j, k);
		printCell(time, quantities[i][j+1][k], i, j+1, k);
		printCell(time, quantities[i][j-1][k], i, j-1, k); //*/
		
		
	}
	
	protected void printCell(double time, double[] quantities, int i, int j, int k){
		double vx, vy, vz, pressure, v2, rhoV2, soundspeed, mach;
		
		println("");
		//print("###### cell " + i + " " + j + " " + k + " ");
		
		vx = quantities[EquationsHydroIdeal.INDEX_XMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		vy = quantities[EquationsHydroIdeal.INDEX_YMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		vz = quantities[EquationsHydroIdeal.INDEX_ZMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		v2 = vx*vx+vy*vy+vz*vz;
		rhoV2 = quantities[EquationsHydroIdeal.INDEX_RHO]*v2;
		pressure = ((EquationsHydroIdeal) equations).getPressure(quantities[EquationsHydroIdeal.INDEX_ENERGY], rhoV2);

		soundspeed = ((EquationsHydroIdeal) equations).getSoundSpeedFromPressure(pressure, quantities[EquationsHydroIdeal.INDEX_RHO]);
		mach = Math.sqrt(v2) / soundspeed;
		
		print(time + " ");
		print(quantities[EquationsHydroIdeal.INDEX_RHO] + " ");
		print(vx + " " + vy + " " + vz + " ");
		print(pressure + " ");
		print(soundspeed + " ");
		print(mach + " ");
		print(quantities[EquationsHydroIdeal.INDEX_ENERGY] + " ");
		
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
