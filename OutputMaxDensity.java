
public class OutputMaxDensity extends Output {

	public OutputMaxDensity(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints minimum and maximum value of density over time";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		boolean firsttime = true;
		double mindensity = 0.0, maxdensity = 0.0;
		
		for (int i = 0; i < quantities.length; i++){
			for (int j = 0; j < quantities[i].length; j++){
				for (int k = 0; k < quantities[i][j].length; k++){
					if (firsttime){
						mindensity = quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
						maxdensity = mindensity;
						firsttime = false;
					} else {
						mindensity = Math.min(mindensity, quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]);
						maxdensity = Math.max(maxdensity, quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO]);
					}
					
				}
			}
		}
		
		println(time + " " + mindensity + " " + maxdensity);


	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
