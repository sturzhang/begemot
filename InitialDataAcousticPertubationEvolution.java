
public class InitialDataAcousticPertubationEvolution extends InitialDataAcoustic {

	public InitialDataAcousticPertubationEvolution(Grid grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		
		res[EquationsAcousticSimple.INDEX_P] = 1.0 + (g.i()==100 ? 0.1 : 0.0);
		res[EquationsAcousticSimple.INDEX_VX] = 1.0;
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Pertubation evolution test";
	}

}
