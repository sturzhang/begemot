
public class FluxMultiStepAndWaveSpeed {
	
	public static int UNIDIRECTIONAL = 0;
	public static int MULTID = 1;
	
	protected double[][] flux;
	protected double[] additionalQuantities;
	
	protected double waveSpeed;
	
	public FluxMultiStepAndWaveSpeed(double[][] flux, double waveSpeed) {
		this.flux = flux;
		this.waveSpeed = waveSpeed;
	}
	
	public FluxMultiStepAndWaveSpeed(double[] flux, double waveSpeed) {
		this.flux = new double[1][];
		this.flux[UNIDIRECTIONAL] = flux;
		
		this.waveSpeed = waveSpeed;
	}

	public FluxMultiStepAndWaveSpeed(double[][] flux, double waveSpeed, double[] additionalQuantities) {
		this.flux = flux;
		this.waveSpeed = waveSpeed;
		this.additionalQuantities = additionalQuantities;
	}
	
	public FluxMultiStepAndWaveSpeed(double[] flux1d, double[] flux2d, double waveSpeed) {
		this.flux = new double[2][];
		flux[UNIDIRECTIONAL] = flux1d;
		flux[MULTID] = flux2d;
		
		this.waveSpeed = waveSpeed;
	}
	
	public void setFlux(double[] flux, int index){
		this.flux[index] = flux; 
	};
	
	
	public double[][] fluxMultiStep(){ return flux; }
	
	public double waveSpeed(){ return waveSpeed; }

	public double[] additionalQuantities(){ return additionalQuantities; }
}
