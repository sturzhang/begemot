
public class FluxSolverSemiLagrangianHydroConsistent extends
		FluxSolverSemiLagrangianHydro {

	public FluxSolverSemiLagrangianHydroConsistent(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}
	
	@Override
	protected double[] interfaceValuesAndRelaxationSpeed(double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, GridEdge gridEdge){
		
		EquationsHydroIdeal eq = ((EquationsHydroIdeal) equations);
		
		double uStar, relaxationSpeed, PiStar;
		
		double uip1j   = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double uij     = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double uip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double uijp1   = ijp1k[EquationsHydroIdeal.INDEX_XMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double uip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double uijm1   = ijm1k[EquationsHydroIdeal.INDEX_XMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
		
		double vip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double vijp1   = ijp1k[EquationsHydroIdeal.INDEX_YMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double vip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double vijm1   = ijm1k[EquationsHydroIdeal.INDEX_YMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
				
		double pip1j   = eq.getPressureFromConservative(ip1jk);
		double pij     = eq.getPressureFromConservative(ijk);
		double pip1jp1 = eq.getPressureFromConservative(ip1jp1k);
		double pijp1   = eq.getPressureFromConservative(ijp1k);
		double pip1jm1 = eq.getPressureFromConservative(ip1jm1k);
		double pijm1   = eq.getPressureFromConservative(ijm1k);
		
		double cR = eq.getSoundSpeedFromConservative(ip1jk);
		double cL = eq.getSoundSpeedFromConservative(ijk);
		
		relaxationSpeed = Math.max(cR*ip1jk[EquationsHydroIdeal.INDEX_RHO], 
				cL*ijk[EquationsHydroIdeal.INDEX_RHO]);
		
		double diffP = 0.25*(pip1jp1 - pijp1 + 2.0*(pip1j - pij) + pip1jm1 - pijm1);
		double diffU = 0.25*(uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1);
		
		double diffV = 0.25*(vijp1 - vijm1 + vip1jp1 - vip1jm1);
		
		double sumP  = 0.25*(pip1jp1 + pijp1 + 2.0*(pip1j + pij) + pip1jm1 + pijm1);
		double sumU  = 0.25*(uip1jp1 + uijp1 + 2.0*(uip1j + uij) + uip1jm1 + uijm1);
		
		
		uStar  = 0.5*sumU - 0.5/relaxationSpeed*diffP;
		
		
		
		PiStar = 0.5*sumP - relaxationSpeed/2*(diffU + diffV);
				
		/*double mxip1j   = ip1jk[EquationsHydroIdeal.INDEX_XMOM];
		double mxij     = ijk[EquationsHydroIdeal.INDEX_XMOM];
		double mxip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_XMOM];
		double mxijp1   = ijp1k[EquationsHydroIdeal.INDEX_XMOM];
		double mxip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_XMOM];
		double mxijm1   = ijm1k[EquationsHydroIdeal.INDEX_XMOM];

		double myip1j   = ip1jk[EquationsHydroIdeal.INDEX_YMOM];
		double myij     = ijk[EquationsHydroIdeal.INDEX_YMOM];
		double myip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_YMOM];
		double myijp1   = ijp1k[EquationsHydroIdeal.INDEX_YMOM];
		double myip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_YMOM];
		double myijm1   = ijm1k[EquationsHydroIdeal.INDEX_YMOM];

		double diffMx = 0.25*(mxip1jp1 - mxijp1 + 2.0*(mxip1j - mxij) + mxip1jm1 - mxijm1);
		double diffMy = 0.25*(myijp1 - myijm1 + myip1jp1 - myip1jm1);
		
		double vip1j   = ip1jk[EquationsHydroIdeal.INDEX_YMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double vij     = ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		
		double sumV  = 0.25*(vip1jp1 + vijp1 + 2.0*(vip1j + vij) + vip1jm1 + vijm1);
		*/
		
		double uStarForAdvection;
		
		
		//uStarForAdvection = uStar - 0.5/relaxationSpeed*(sumU*diffMx + sumV*diffMy);
		
		/*double[] pos = gridEdge.midpoint();
		double x = pos[0] - grid.xMidpoint();
		double y = pos[1] - grid.yMidpoint();
		
		//System.err.println(x + " " + y);
		
		double uX, uY;
		
		double r = Math.sqrt(x*x+y*y);
		if (r < 0.2){      uX = -5.0*y;           uY = 5.0*x; }
		else if (r < 0.4){ uX = -(2 - 5.0*r)*y/r; uY = (2 - 5.0*r)*x/r; }
		else {             uX = 0;                uY = 0; } //*/
		
		//uStarForAdvection = uX * gridEdge.normalVector()[0] + uY * gridEdge.normalVector()[1]; 
		
		
		uStarForAdvection = uStar;
		
		return new double[]{uStar, PiStar, relaxationSpeed, uStarForAdvection};
	}

}
