public abstract class FluxSolverMultiStepMultiDExactRiemann extends FluxSolverMultiStepMultiDCartesianFirstOrder {

	public FluxSolverMultiStepMultiDExactRiemann(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	public int steps() { return 2; }
	
	// TODO this implementation of passive scalar is the same as in FluxSolverRoe -- change it to be multi-d
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right, int direction) {
		double[] res = new double[equations.getNumberOfPassiveScalars()];
		int[] sc = equations.indicesOfPassiveScalars();
		int s;
		double vLeft = equations.advectionVelocityForPassiveScalar(i,j,k,left, direction);
		double vRight = equations.advectionVelocityForPassiveScalar(i,j,k,right, direction);
		double absMeanV = 0.5 * Math.abs(vLeft + vRight);
		
		
		for (int ii = 0; ii < res.length; ii++){
			s = sc[ii];
			// TODO there was momentum mLeft and mRight standing in the flux average before
			res[i] = 0.5*(left[s]*vLeft + right[s]*vRight) - 0.5 * absMeanV * (right[s] - left[s]);
		}
		return res;
	}


	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep){
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (((GridCartesian) grid).dimensions() == 2){
				if (dir == GridCartesian.X_DIR){
					return 0.5*
									speedY*
									dt/((GridCartesian) grid).ySpacing();
				} else if (dir == GridCartesian.Y_DIR){
					return 0.5*
									speedX*
									dt/((GridCartesian) grid).xSpacing();
				} else {
					System.err.println("ERROR: asked for non-x, non-y direction");
					return 0.0;
				}
			} else {
				return 0.0;
			}
		}
	}

}
