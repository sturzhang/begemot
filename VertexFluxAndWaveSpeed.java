
public class VertexFluxAndWaveSpeed {

	protected double[][] flux;
	protected double[] waveSpeeds;
	
	public VertexFluxAndWaveSpeed(double[][] flux, double[] waveSpeeds) {
		this.flux = flux;
		this.waveSpeeds = waveSpeeds;
	}
	
	public double[][] flux(){ return flux; }
	
	public double[] waveSpeeds(){ return waveSpeeds; }


}
