
public class InitialDataHydroSoundwave extends InitialDataHydro {

	protected double mach, soundspeed;
	protected double rhoBackground;
	protected double pBackground = 1.0;
	protected double wavenumber;
	
	public InitialDataHydroSoundwave(Grid grid, EquationsHydroIdeal equations, double mach, double wavelength) {
		super(grid, equations);
		this.mach = mach;
		rhoBackground = equations.gamma();
		this.soundspeed = Math.sqrt(equations.gamma() * pBackground / rhoBackground);
		this.wavenumber = 2.0*Math.PI/wavelength;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
    	double pressure, rhoV2;
    	
    	double phase = mach*Math.cos(wavenumber*g.getX());
    	
    	res[EquationsHydroIdeal.INDEX_RHO] = rhoBackground*(1.0 + phase);
    	pressure = pBackground + rhoBackground*soundspeed*soundspeed*phase;
    				
    	res[EquationsHydroIdeal.INDEX_XMOM] = res[EquationsHydroIdeal.INDEX_RHO] * soundspeed * phase;
    	res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_ZMOM]= 0.0;
    	rhoV2 = Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) / res[EquationsHydroIdeal.INDEX_RHO];	
    	
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    			
    		
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Low Mach soundwaves";
	}

}
