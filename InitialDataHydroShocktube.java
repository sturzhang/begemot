
public abstract class InitialDataHydroShocktube extends InitialDataHydro {

	public InitialDataHydroShocktube(Grid grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}
	
	public abstract double leftPressure();
	public abstract double leftDensity();
	public abstract double leftVx();
	
	public abstract double rightPressure();
	public abstract double rightDensity();
	public abstract double rightVx();
	
	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double pressure, rhoV2, v;
    	double x = g.getX() - grid.xMidpoint();
    	double y = g.getY() - grid.yMidpoint();
    	
    	double angle = 0.0; //15.0/180*Math.PI;
    	
    	if (x < y*Math.tan(angle)){
    		res[EquationsHydroIdeal.INDEX_RHO] = leftDensity();
    		pressure = leftPressure();
    		v = leftVx();
    	} else {
    		res[EquationsHydroIdeal.INDEX_RHO] = rightDensity();
    		pressure = rightPressure();
    		v = rightVx();
    	}
    				
		res[EquationsHydroIdeal.INDEX_XMOM] = v*res[EquationsHydroIdeal.INDEX_RHO];
    	res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    				
    	rhoV2 = Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2)/res[EquationsHydroIdeal.INDEX_RHO];
    	
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    		
    	return res;

	}
	
	@Override
	public String getDetailedDescription(){
		return "Shock tube test. Left state (rho, v, p) = (" + leftDensity() + ", " + leftVx() + ", " + leftPressure() + 
				"), right state (rho, v, p) = (" +  rightDensity() + ", " + rightVx() + ", " + rightPressure() + ")";
	}

}
