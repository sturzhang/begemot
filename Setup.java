
public class Setup {

    protected Equations              equations;
    protected InitialData            initialData;
    protected Boundary               boundary;
    protected FluxSolver             fluxSolver;
    protected Reconstruction         reconstruction;
    protected Output                 output;
    protected Source                 source;
    protected Obstacle               obstacle;
    
    protected double[][][][]   finalConservedQuantities;
    protected double[][][][]   initialConservedQuantities; 
    
    protected Grid grid;
    
    protected int firstIndexOfConservedquantities;
    
    public void setFirstIndexOfConservedquantities(int firstIndexOfConservedquantities){
    	this.firstIndexOfConservedquantities = firstIndexOfConservedquantities;
    }
     
    public int firstIndexOfConservedquantities(){ return firstIndexOfConservedquantities; }
    
    public void initializeArrays(Grid grid){
    	this.grid = grid;
    	
    	grid.excludeFromTimeEvolution = grid.generateEmptyBooleanArray();
    	
    	grid.conservedQuantities      = grid.generateEmptyArray( equations.getTotalNumberOfConservedQuantities() );
    	grid.interfaceValues          = grid.generateEmptyArrayForInterface( equations.getTotalNumberOfConservedQuantities() );
		grid.pointValues              = grid.generateEmptyArrayForInterface( equations.getTotalNumberOfConservedQuantities() );
    	
    	grid.summedInterfaceFluxes    = grid.generateEmptyFluxArray( equations.getTotalNumberOfConservedQuantities() );
    	grid.interfaceFluxes          = grid.generateEmptyFluxArray( equations.getTotalNumberOfConservedQuantities(), fluxSolver.steps() );
    	grid.sources                  = grid.generateEmptyArray( equations.getTotalNumberOfConservedQuantities() );
		
    	grid.additionalInterfaceQuantities = grid.generateEmptyFluxArray( fluxSolver.getNumberOfAdditionalInterfaceQuantities() );
    	
    	grid.localWaveSpeeds          = grid.generateEmptyWaveSpeedArray();
    }//*/
    
    public void fillConservedQuantities(){    	
    	initialData.fillConservedQuantities( grid.conservedQuantities ); // todo maybe better with return parameter
    	initialConservedQuantities = new double[grid.conservedQuantities.length][grid.conservedQuantities[0].length][grid.conservedQuantities[0][0].length][grid.conservedQuantities[0][0][0].length];
    	

    	for (int i = 0; i < grid.conservedQuantities.length; i++){
			for (int j = 0; j < grid.conservedQuantities[i].length; j++){
				for (int k = 0; k < grid.conservedQuantities[i][j].length; k++){
					for (int q = 0; q < grid.conservedQuantities[i][j][k].length; q++){
						initialConservedQuantities[i][j][k][q] = grid.conservedQuantities[i][j][k][q];
					}
				}
			}
		}
		
    }
    
    public void produceForcedOutput(double time){
    	output.produceForced(time, grid.conservedQuantities);
    }
    
    public void produceOutput(double time){
    	output.produce(time, grid.conservedQuantities);
    }

    
    // TODO hack in order to insert exact central derivative
    public void setExactCentralDerivative(){
    	initialData.setExactCentralDerivative(grid.conservedQuantities);
    }
    
    
    public void setBoundaries(double time){
    	boundary.set( grid.conservedQuantities, grid.pointValues, time );
    }
    
    public void setObstacle(){
    	obstacle.set( grid.conservedQuantities );
    }
    
    public void setObstacleViaRigidWalls(){
    	grid.summedInterfaceFluxes = obstacle.insertRigidWalls( grid.summedInterfaceFluxes, grid.conservedQuantities );
    }
    
    public void initializeObstacle(){
    	grid.excludeFromTimeEvolution = obstacle.setObstacle(grid.excludeFromTimeEvolution);
		fluxSolver.setCellsExcludedFromTimeEvolution(grid.excludeFromTimeEvolution);
    }
    
    public void performReconstruction(){
    	grid.interfaceValues = reconstruction.perform( grid.conservedQuantities, grid.interfaceValues );
    }
    
    /*public void evolvePointValues(){
    	grid.pointValues = pointValueEvolution.perform(grid.conservedQuantities, grid.pointValues);
    }*/
    
    /*public void setWaveSpeeds(){
    	fluxSolver.resetCurMaxSignalSpeed();
    	fluxSolver.setWaveSpeeds( 
    			grid.interfaceValues, grid.interfaceFluxes, grid.localWaveSpeeds, grid.additionalInterfaceQuantities);
    }*/
    
    public void setFluxesAndWaveSpeeds(int timeIntegratorStep){
    	fluxSolver.resetCurMaxSignalSpeed();
    	grid.interfaceFluxes = fluxSolver.getAndSetWaveSpeeds( 
    			grid.interfaceValues, grid.interfaceFluxes, grid.localWaveSpeeds, timeIntegratorStep, grid.additionalInterfaceQuantities);
    }
    
    public void setSources(double time, double dt, int timeIntegratorStep){
    	grid.sources = source.get( grid.conservedQuantities, grid.sources, time, dt, timeIntegratorStep);
    }
    
    public void sumFluxSolverSteps(Grid grid, double dt){
    	grid.summedInterfaceFluxes = fluxSolver.sum ( grid.interfaceFluxes, dt, 
				grid.generateEmptyFluxArray( equations.getTotalNumberOfConservedQuantities()),
				grid.localWaveSpeeds, grid.additionalInterfaceQuantities);
    }
    
    public void performTimeIntegration(TimeIntegratorFiniteVolume timeIntegrator, double dt, int timeIntegratorStep){
    	grid.conservedQuantities = timeIntegrator.perform( 
    			timeIntegratorStep, dt, grid.summedInterfaceFluxes, grid.sources, 
    			grid.conservedQuantities, grid.excludeFromTimeEvolution );
		/* TODO retest semiimplicit solvers now */
    }
    
    public void setFinalConservedQuantities(){
    	// TODO this only sets the pointer
    	finalConservedQuantities = grid.conservedQuantities;
    }
    
    public String getDescriptions(){
    	String res = "";
    	
    	res += equations.description()      + " (" + equations.getClass().getName()      + ")" + "\n";
    	res += initialData.description()    + " (" + initialData.getClass().getName()    + ")" + "\n";
    	res += boundary.description()       + " (" + boundary.getClass().getName()       + ")" + "\n";
    	res += fluxSolver.description()     + " (" + fluxSolver.getClass().getName()     + ")" + "\n";
    	res += reconstruction.description() + " (" + reconstruction.getClass().getName() + ")" + "\n";
    	res += output.description()         + " (" + output.getClass().getName()         + ")" + "\n";
    	res += source.description()         + " (" + source.getClass().getName()         + ")" + "\n" ;
    	res += obstacle.description()       + " (" + obstacle.getClass().getName()       + ")" + "\n";
    	
    	return res;
    }
    
    public double[][][][] getFinalConservedQuantities(){   return finalConservedQuantities; }
    public double[][][][] getConservedQuantities(){        return grid.conservedQuantities; }
    public double[][][][] getInitialConservedQuantities(){ return initialConservedQuantities; }
    
    
	public Equations  equations()  { return equations;  }
	public FluxSolver fluxSolver() { return fluxSolver; }
	public Output     output()     { return output;     }	
	public Obstacle   obstacle()   { return obstacle;   }
    
}
