import matrices.SquareMatrix;


public class FluxSolverHLL extends FluxSolverRoe {

	protected boolean simple;
	
	public FluxSolverHLL(Grid grid, Equations equations, boolean simple) {
		super(grid, equations);
	}
	
	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		int quantities = equations.getNumberOfConservedQuantities();
		double[] fluxRes;
		GridCell g = gridEdge.adjacent1();
		boolean detailedOutput = false;
		
		SquareMatrix[] matLeft = getDiagonalization(g.i(), g.j(), g.k(), left, GridCartesian.X_DIR);
		SquareMatrix[] matRight = getDiagonalization(g.i(), g.j(), g.k(), right, GridCartesian.X_DIR);
		
		double maxEvalLeft = matLeft[1].value(0, 0), minEvalLeft = matLeft[1].value(0, 0);
		double maxEvalRight = matRight[1].value(0, 0), minEvalRight = matRight[1].value(0, 0);
		for (int i = 1; i < quantities; i++){
			maxEvalLeft  = Math.max(maxEvalLeft,  matLeft[1 ].value(i, i));
			maxEvalRight = Math.max(maxEvalRight, matRight[1].value(i, i));
			minEvalLeft  = Math.min(minEvalLeft,  matLeft[1 ].value(i, i));
			minEvalRight = Math.min(minEvalRight, matRight[1].value(i, i));
		}
		double sMinus, sPlus;
		
		if (simple){
			sPlus  = Math.max(maxEvalLeft, maxEvalRight);
			sMinus = -sPlus;
		} else {
			sMinus = Math.min(minEvalLeft, minEvalRight);
			sPlus = Math.max(maxEvalLeft, maxEvalRight);
		}
		
		double[] leftF  = equations.fluxFunction(g.i(), g.j(), g.k(), left,  GridCartesian.X_DIR);
		double[] rightF = equations.fluxFunction(g.i(), g.j(), g.k(), right, GridCartesian.X_DIR);
		
				
		if (sMinus > 0){
			fluxRes = leftF;
		} else if (sPlus < 0){
			fluxRes = rightF;
		} else {
			fluxRes = new double[quantities];
			for (int q = 0; q < quantities; q++){ 
				fluxRes[q] = 0.5*(leftF[q] + rightF[q]) + sMinus*sPlus/(sPlus - sMinus)*(right[q] - left[q]);
				if (!simple){
					fluxRes[q] += 0.5*(sPlus + sMinus)/(sPlus - sMinus)*(leftF[q] - rightF[q]);
				}
	    		if (Double.isNaN(fluxRes[q])){
	    	    	System.err.println("fluxes are NaN");
	    	    	detailedOutput = true;
	    	    }
			}
		}
				
		if (detailedOutput){
        	for (int q = 0; q < quantities; q++){
        	    System.err.println("left: " + left[q] + " right: " + right[q]
        	    					+ " res: " + fluxRes[q]);
        	    
        	}
    	}
		
    	return new FluxAndWaveSpeed(fluxRes, sPlus);
	}
	
}
