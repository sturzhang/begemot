import matrices.SquareMatrix;


public class FluxSolverRoeConsistentViaPseudoInverseHydro extends FluxSolverRoeConsistentViaPseudoInverse {

	public FluxSolverRoeConsistentViaPseudoInverseHydro(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}
	

	@Override
	public UpwindingMatrixAndWaveSpeed getAveragedSignMatrix(int i, int j, int k, double[] left, double[] right, int direction){
		
		// a bit of Roe average testing
		boolean doAverageTest = false;
		
		if (doAverageTest){
			double[] fluxLeft = equations.fluxFunction(i, j, k,  left, direction);
			double[] fluxRight = equations.fluxFunction(i, j, k, right, direction);
			double[] diff     = new double[left.length];
			double[] fluxDiff = new double[left.length];
			double error = 0;
			double diffSqrNorm = 0;
			
			for (int q = 0; q < left.length; q++){
				diff[q] = right[q] - left[q];
				diffSqrNorm += diff[q]*diff[q];
				fluxDiff[q] = fluxRight[q] - fluxLeft[q];
			}
			SquareMatrix[] diag = equations.diagonalization(i, j, k, mean(left,  right), direction);
			SquareMatrix jacobian = diag[0].mult(diag[1]).mult(diag[2]).toSquareMatrix();
			
			
			if (diffSqrNorm > 1e-3){
				double[] matDiff = jacobian.mult(diff);
				
				for (int q = 0; q < left.length; q++){
					error += Math.pow(matDiff[q] - fluxDiff[q], 2);
				}
				error = Math.sqrt(error);
				
				if (error > 1e-10){
					/*System.err.println("----------------");
					System.err.println("left = " + left[0] + " " + left[1] + " " + left[2] + " " + left[3] + " " + left[4]);
					System.err.println("right = " + right[0] + " " + right[1] + " " + right[2] + " " + right[3] + " " + right[4]);
					System.err.println("fluxleft = " + fluxLeft[0] + " " + fluxLeft[1] + " " + fluxLeft[2] + " " + fluxLeft[3] + " " + fluxLeft[4]);
					System.err.println("fluxright = " + fluxRight[0] + " " + fluxRight[1] + " " + fluxRight[2] + " " + fluxRight[3] + " " + fluxRight[4]);
					
					System.err.println(jacobian);
					System.err.println("---");
					System.err.println(diag[0]);
					System.err.println("---");
					System.err.println(diag[1]);
					System.err.println("---");
					System.err.println(diag[2]);
					System.err.println("matdiff = " + matDiff[0] + " " + matDiff[1] + " " + matDiff[2] + " " + matDiff[3] + " " + matDiff[4]);
				
					*/
				
					for (int q = 0; q < left.length; q++){
						System.err.println(q + " " + matDiff[q] + " =? " + fluxDiff[q] + "\t " + (matDiff[q] - fluxDiff[q]));
					}
				}
			}
			// end of testing
		}
		
		
		
		return getUpwindingMatrix(i, j, k, mean(left, right), direction, left, right);
	}
	
	@Override
	public double[] mean(double[] left, double[] right){
		double[] res = new double[left.length];
		
		//if (equations instanceof EquationsHydroIdeal){
		
			double sqrtRhoL = Math.sqrt(left[EquationsHydroIdeal.INDEX_RHO]);
			double sqrtRhoR = Math.sqrt(right[EquationsHydroIdeal.INDEX_RHO]);
			double meanUx = (left[EquationsHydroIdeal.INDEX_XMOM]/sqrtRhoL + right[EquationsHydroIdeal.INDEX_XMOM]/sqrtRhoR)/
				(sqrtRhoL + sqrtRhoR);
			double meanUy = (left[EquationsHydroIdeal.INDEX_YMOM]/sqrtRhoL + right[EquationsHydroIdeal.INDEX_YMOM]/sqrtRhoR)/
					(sqrtRhoL + sqrtRhoR);
			double meanUz = (left[EquationsHydroIdeal.INDEX_ZMOM]/sqrtRhoL + right[EquationsHydroIdeal.INDEX_ZMOM]/sqrtRhoR)/
					(sqrtRhoL + sqrtRhoR);
					
			double EPlusPL = left[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(left);
			double EPlusPR = right[EquationsHydroIdeal.INDEX_ENERGY] + ((EquationsHydroIdeal) equations).getPressureFromConservative(right);
			double meanEnthalpy = (EPlusPL / sqrtRhoL + EPlusPR / sqrtRhoR) / (sqrtRhoL + sqrtRhoR);
			
			//System.err.println(meanUx + " " + meanUy + " " + meanEnthalpy);
			
			double rho = 0.5*(left[EquationsHydroIdeal.INDEX_RHO] + right[EquationsHydroIdeal.INDEX_RHO]); // irrelevant
			res[EquationsHydroIdeal.INDEX_RHO]  = rho;
			res[EquationsHydroIdeal.INDEX_XMOM] = rho*meanUx;
			res[EquationsHydroIdeal.INDEX_YMOM] = rho*meanUy;
			res[EquationsHydroIdeal.INDEX_ZMOM] = rho*meanUz;
			double gamma = ((EquationsHydroIdeal) equations).gamma();
			double M2 = res[EquationsHydroIdeal.INDEX_XMOM]*res[EquationsHydroIdeal.INDEX_XMOM] + 
					res[EquationsHydroIdeal.INDEX_YMOM]*res[EquationsHydroIdeal.INDEX_YMOM] + 
					res[EquationsHydroIdeal.INDEX_ZMOM]*res[EquationsHydroIdeal.INDEX_ZMOM];
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((gamma-1) * M2 + 2.0*meanEnthalpy*rho*rho) / 2/gamma/rho; //*/
		//} else { 
		/*	for (int i = 0; i < res.length; i++){
				res[i] = 0.5 * (left[i] + right[i]);
			}
		//} */
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Roe solver extended to multiple dimensions with stationarity consistent diffusion, explicit use of Roe average for ideal hydro";
	}



	
}
