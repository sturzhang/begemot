
public class InitialDataScalarGuckenheimer extends InitialDataScalar {

	public InitialDataScalarGuckenheimer(Grid grid, EquationsScalar equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		if (g.getX() > 0){
			if (g.getY() < 0){
				return new double[] {-1.0};
			} else {
				return new double[] { 0.0};
			}
		} else {
			if (g.getY() > -g.getX()){
				return new double[] { 0.0};
			} else {
				return new double[] { 1.0};
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Guckenheimer's 2-d Riemann problem.";
	}

}
