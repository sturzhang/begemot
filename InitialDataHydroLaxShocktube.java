
public class InitialDataHydroLaxShocktube extends InitialDataHydroShocktube {

	public InitialDataHydroLaxShocktube(Grid grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}


	@Override
	public double leftPressure() { return 3.528; }
	@Override
	public double leftDensity() {  return 0.445; }
	@Override
	public double leftVx() {       return 0.698; 
	}
	

	@Override
	public double rightPressure() { return 0.571; }
	@Override
	public double rightDensity() {  return 0.5; }
	@Override
	public double rightVx() {       return 0;
	}
}
