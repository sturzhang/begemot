
public class InitialDataAcousticGaussianGresho extends InitialDataAcoustic {

	private double width, radialvel, gaussianAmplitude, poffset;
	
	public InitialDataAcousticGaussianGresho(Grid grid, EquationsAcoustic equations, double gaussianAmplitude, 
				double width, double radialvel, double poffset) {
		super(grid, equations);
		this.width = width;
		this.radialvel = radialvel;
		this.gaussianAmplitude = gaussianAmplitude;
		this.poffset = poffset;
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double x, y, r, v;
		double[]res = new double[q];
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
    	
		r = Math.sqrt(x*x+y*y);
    	
		res[EquationsAcoustic.INDEX_P] = gaussianAmplitude * Math.exp(- (x*x + y*y) / width/width) + poffset;
    				
		if (r < width){
			v = r/width;
		} else {
			if (r < 2*width){
				v = 2-r/width;
			} else {
				v = 0.0;
			}
		}
		
		if (r == 0){
			res[EquationsAcoustic.INDEX_VX] = 0.0;
			res[EquationsAcoustic.INDEX_VY] = 0.0;
		} else  {
			res[EquationsAcoustic.INDEX_VX] = -y/r*v + x/r*radialvel;
			res[EquationsAcoustic.INDEX_VY] = x/r*v + y/r*radialvel;
		}	
		res[EquationsAcoustic.INDEX_VZ] = 0.0;
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Analogon of Gresho vortex for the acoustic system, combined with Gaussian pressure ditribution with a width of " + width + ", " + 
				" an amplitude "+ gaussianAmplitude + ", a pressure offset of " + poffset + " and a radial velocity " + radialvel;
	}

}
