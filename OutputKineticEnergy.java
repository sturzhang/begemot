
public class OutputKineticEnergy extends Output {
	
	protected double mach;
	
	public OutputKineticEnergy(double outputTimeInterval, GridCartesian grid, EquationsHydroIdeal equations, double mach) {
		super(outputTimeInterval, grid, equations);
		this.mach = mach;
	}
	
    public String getDetailedDescription(){
    	return "prints sum of kinetic energy over all cells and its mean over time";
    }

	
	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double kinen = 0;
		int counter = 0;
		
		for (int i = 0; i < quantities.length; i++){
			for (int j = 0; j < quantities[i].length; j++){
				for (int k = 0; k < quantities[i][j].length; k++){
					counter++;
					kinen += 0.5 * (
							Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM], 2) + 
							Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM], 2) +
							Math.pow(quantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM], 2)) / 
							quantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
				}
			}
		}
		
		println(time + " " + kinen + " " + (kinen/counter) + " " + mach);

	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
