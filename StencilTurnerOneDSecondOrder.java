
public class StencilTurnerOneDSecondOrder extends StencilTurner {

	public static final int NEIGHBOUR_LEFTLEFT   = 0;
	public static final int NEIGHBOUR_LEFT       = 1;
	public static final int NEIGHBOUR_RIGHT      = 2;
	public static final int NEIGHBOUR_RIGHTRIGHT = 3;

	
	public StencilTurnerOneDSecondOrder(GridCartesian grid) {
		super(grid);
	}

	@Override
	public GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge) {
		int edgeDir = ((GridCartesian) grid).getDirectionPerpendicularToEdge(g, gridEdge.index());		
		GridCell nb = gridEdge.getOtherGridCell(g);
		int offsetX = 0, offsetY = 0, offsetZ = 0;
		if (edgeDir == GridCartesian.X_DIR){ offsetX = 1; }
		if (edgeDir == GridCartesian.Y_DIR){ offsetY = 1; }
		if (edgeDir == GridCartesian.Z_DIR){ offsetZ = 1; }
		
		return new GridCell[]{
				new GridCell(g.i()-offsetX, g.j()-offsetY, g.k()-offsetZ, grid),
				g, 
				nb,
				new GridCell(nb.i()+offsetX, nb.j()+offsetY, nb.k()+offsetZ, grid) };
	}
	
	

}
