
public class InitialDataHydroObliqueShock extends InitialDataHydro {

	public InitialDataHydroObliqueShock(Grid grid, EquationsHydroIdeal equations){ super(grid, equations); }

	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double pressure, rhoV2, vx, vy, rho;
    	double footpointX = grid.xmax();// - (grid.xmax() - grid.xmin())/10;
    	double angle = 10.0 * Math.PI / 180;
    	
    	double pressureLeft = 0.71;
    	double vLeft = 0.0;
    	double rhoLeft = 1.0;
    	double shockSpeed = 0.5;
    	
    	double[] valuesRight = ((EquationsHydroIdeal) equations).valuesBehindStationary2DShock(
    			rhoLeft, new double[] {vLeft + shockSpeed, 0}, pressureLeft, 
    			new double[] {Math.sin(angle), -Math.cos(angle)});
    	
    	if (g.getX() < (g.getY() - grid.ymin())/Math.tan(angle) + footpointX){
    		rho = rhoLeft;
    		vx = vLeft;
    		vy = 0;
    		pressure = pressureLeft;
        } else {
        	rho = valuesRight[0];
        	vx = valuesRight[1] - shockSpeed;
        	vy = valuesRight[2];
    		pressure = valuesRight[3];
        }
    				
    	res[EquationsHydroIdeal.INDEX_RHO] = rho;
		res[EquationsHydroIdeal.INDEX_XMOM] = vx*rho;
    	res[EquationsHydroIdeal.INDEX_YMOM] = vy*rho;
    	res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    				
    	rhoV2 = (vx*vx + vy*vy)*rho;
    	
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    			
    		
    	return res;
    }

	@Override
	public String getDetailedDescription() {
		return "Oblique shock setup";
	}

}
