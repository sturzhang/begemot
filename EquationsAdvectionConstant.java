

public class EquationsAdvectionConstant extends EquationsAdvection {

	private double[] vel;
	// do not use vel[] directly, use the velocity() method instead, because otherwise no trafo will be done
	
	public EquationsAdvectionConstant(Grid grid, int numberOfPassiveScalars, double[] vel) {
		super(grid, numberOfPassiveScalars);
		this.vel = vel;
	}

	@Override
	public double[] velocityWithoutTransformation(int i, int j, int k) {
		return vel;
	}
	
	public double[] velocity() {
		return velocity(0,0,0);
	}
	

	@Override
	public String getDetailedDescription() {
		return "Linear advection with velocity (" + vel[0] + ", " + vel[1] + ", " + vel[2] + ")";
	}

	@Override
	public double flux(int i, int j, int k, double quantity, int direction) {
		return quantity * velocity()[direction];
	}

	@Override
	public double jacobian(int i, int j, int k, double quantity, int direction) {
		return velocity()[direction];
	}
	
	public double[] evolve1d(double pos, double time, ReconstructionParabolic recon, int direction, 
			GridEdgeMapped edge){
		return recon.eval(pos - time * vel[direction], direction, edge);
	}


}
