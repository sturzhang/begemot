import matrices.DiagonalMatrix;
import matrices.SquareMatrix;


public class EquationsHydroIdeal extends EquationsHydro {

	double gamma;
	double mach;
	
	public static final int INDEX_RHO = 0;
	public static final int INDEX_XMOM = 1;
	public static final int INDEX_YMOM = 2;
	public static final int INDEX_ZMOM = 3;
	public static final int INDEX_ENERGY = 4;

	public EquationsHydroIdeal(Grid grid, int numberOfPassiveScalars, double gamma) {
		super(grid, numberOfPassiveScalars);
		this.gamma = gamma;
		mach = 1.0;
	}

	@Override
	public String getDetailedDescription(){
		return "Ideal Hydrodynamics with the EOS of a perfect gas with gamma = " + gamma;
	}
	
	public static int getIndexDirection(int direction){
		int indexDirection = -1; // init to make compiler happy
		if (direction == GridCartesian.X_DIR){ indexDirection = EquationsHydroIdeal.INDEX_XMOM; }	
	    if (direction == GridCartesian.Y_DIR){ indexDirection = EquationsHydroIdeal.INDEX_YMOM; }	
	    if (direction == GridCartesian.Z_DIR){ indexDirection = EquationsHydroIdeal.INDEX_ZMOM; }	
	    return indexDirection;
	}
	
	public EquationsHydroIdeal(Grid grid, int numberOfPassiveScalars, double gamma, double mach) {
		super(grid, numberOfPassiveScalars);
		this.gamma = gamma;
		this.mach = mach;
	}
	

	public double gamma(){ return gamma; }
	public double mach(){ return mach; }

	public int getNumberOfConservedQuantities() { return 5; }

	public double[] valuesBehindStationary2DShock(double rho, double[] v, double pressure, double[] normal){
		double[] tangent = new double[]{normal[1], -normal[0]};

		double v1par = v[0]*tangent[0] + v[1]*tangent[1];
		double v1ort = v[0]*normal[0]  + v[1]*tangent[1];

		double c = this.getSoundSpeedFromPressure(pressure, rho);
		double x12 = (v1ort*v1ort*(gamma-1) + 2*c*c)/(v1ort*v1ort*(gamma+1));

		double v2par = v1par;
		double v2ort = v1ort * x12;

		double[] res = new double[4];  // rho, vx, vy, p
		res[0] = rho / x12;
		res[1] = v2par * tangent[0] + v2ort * normal[0];
		res[2] = v2par * tangent[1] + v2ort * normal[1];

		double y12 = gamma*v1ort*v1ort/c/c*(1 - x12) + 1;
		res[3] = y12 * pressure;
		
		return res;

		
	}
	
	
	// conversion functions ############################################################################
	
	public double getEnergy(double pressure, double rhoV2) { return pressure / (gamma - 1) + 0.5 * rhoV2 * mach*mach; }

	public double getEnthalpy(double energy, double rho, double pressure){ return (energy + pressure) / rho; }

	public double getPressure(double energy, double rhoV2){ return (gamma - 1) * (energy - 0.5 * rhoV2 * mach*mach); }
	
	public double getPressureFromConservative(double rho, double xmom, double ymom, double zmom, double energy){ 
		double rhoV2 = (xmom*xmom + ymom*ymom + zmom*zmom)/rho;
		return (gamma - 1) * (energy - 0.5 * rhoV2 * mach*mach); 
	}
	
	public double getPressureFromConservative(double[] quantities){
		return getPressureFromConservative(
				quantities[EquationsHydroIdeal.INDEX_RHO], 
				quantities[EquationsHydroIdeal.INDEX_XMOM], 
				quantities[EquationsHydroIdeal.INDEX_YMOM], 
				quantities[EquationsHydroIdeal.INDEX_ZMOM], 
				quantities[EquationsHydroIdeal.INDEX_ENERGY]); 
	}
		
	
	public double getSoundSpeedFromPressure(double pressure, double rho){ return Math.sqrt(gamma * pressure / rho); }
	
	public double getSoundSpeedFromConservative(double rho, double xmom, double ymom, double zmom, double energy){
		return Math.sqrt(gamma * getPressureFromConservative(rho, xmom, ymom, zmom, energy) / rho); 
	}
	
	public double getSoundSpeedFromConservative(double[] quantities){
		return Math.sqrt(gamma * getPressureFromConservative(quantities) / quantities[EquationsHydroIdeal.INDEX_RHO]); 
	}
	
	public double getMachFromConservative(double rho, double xmom, double ymom, double zmom, double energy){
		return Math.sqrt(xmom*xmom + ymom+ymom + zmom*zmom)/rho/getSoundSpeedFromConservative(rho, xmom, ymom, zmom, energy); 
	}
	
	public double getMachFromConservative(double[] quantities){
		return Math.sqrt(quantities[INDEX_XMOM]*quantities[INDEX_XMOM] + 
				quantities[INDEX_YMOM]*quantities[INDEX_YMOM] + 
				quantities[INDEX_ZMOM]*quantities[INDEX_ZMOM])/quantities[INDEX_RHO]/
				getSoundSpeedFromConservative(quantities[INDEX_RHO], quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], quantities[INDEX_ENERGY]); 
	}
	
	
	public double getRhoV2(double[] quantities){
		return (quantities[INDEX_XMOM]*quantities[INDEX_XMOM] + 
				quantities[INDEX_YMOM]*quantities[INDEX_YMOM] +
				quantities[INDEX_ZMOM]*quantities[INDEX_ZMOM]) / quantities[INDEX_RHO];
	}
	
	public double[] getVelocity(double[] quantities){
		return new double[]{quantities[INDEX_XMOM] / quantities[INDEX_RHO],  
							quantities[INDEX_YMOM] / quantities[INDEX_RHO], 
							quantities[INDEX_ZMOM] / quantities[INDEX_RHO]};
	}
	
	public double getSoundSpeedFromEnthalpy(double enthalpy, double v2){
		if (enthalpy - 0.5*v2 < 0){ System.out.println("# Square of sound speed negative!"); }
		return Math.sqrt((gamma-1)*(enthalpy - 0.5*v2*mach*mach));
	}
	

	
	
	
	// TODO move the methods calculating the jacobian ans its abs from the solver to here
	// unused! FluxSolverRoeHydro mysolver = new FluxSolverRoeHydro(Iteration.grid, this, FluxSolverRoeHydro.MEANROE);

		

	
	
	/*public double[] getAbsAcousticEigenvaluesFor1DFlow(double vDirection, double soundSpeed){
		double[] eigenValue = new double[5];
		
		eigenValue[0] = Math.abs(vDirection - soundSpeed/mach);
	    eigenValue[1] = Math.abs(vDirection + soundSpeed/mach); 
		eigenValue[2] = 0.0;
		eigenValue[3] = 0.0;
		eigenValue[4] = 0.0;
			    
		return eigenValue;
	}// delete once not used anywhere */

	

	
	
	
	
	// flux function ###################################################################################################

	
	public double[] fluxFunction1DFlowWithoutPressure(int whichMomentumAlignedWithDirection, double[] quantities){
		double[] res = new double[5];
		double pressure = getPressure(quantities[INDEX_ENERGY], getRhoV2(quantities));
		
		res[INDEX_RHO]    = quantities[whichMomentumAlignedWithDirection];
		res[INDEX_XMOM]   = quantities[whichMomentumAlignedWithDirection]*quantities[INDEX_XMOM]/quantities[INDEX_RHO];
		res[INDEX_YMOM]   = quantities[whichMomentumAlignedWithDirection]*quantities[INDEX_YMOM]/quantities[INDEX_RHO];
		res[INDEX_ZMOM]   = quantities[whichMomentumAlignedWithDirection]*quantities[INDEX_ZMOM]/quantities[INDEX_RHO];
		
		res[INDEX_ENERGY] = quantities[whichMomentumAlignedWithDirection]/quantities[INDEX_RHO]*
				(quantities[INDEX_ENERGY] + pressure);
		return res;
	}

	public double[] fluxFunction1DFlowWithoutPressure(HydroState state){
		double[] res = new double[5];
		
		res[INDEX_RHO]    = state.v * state.rho;
		res[INDEX_XMOM]   = state.v * state.vX * state.rho;
		res[INDEX_YMOM]   = state.v * state.vY * state.rho;
		res[INDEX_ZMOM]   = state.v * state.vZ * state.rho;
		
		res[INDEX_ENERGY] = state.v*(state.energy + state.pressure);
		return res;
	}

	
	/*public double[][] Jacobian(double[] state, int direction){
		return mysolver.getJacobian(state, direction);
	}*/ // TODO is this needed?

	
	public double[] fluxFunction(HydroState state) {
		double[] res = fluxFunction1DFlowWithoutPressure(state);
		res[state.indexDirection] += state.pressure / (mach*mach);
		return res;
	}
	
	
	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		int whichMomentumAlignedWithDirection = getIndexDirection(direction);
		
		double[] res = fluxFunction1DFlowWithoutPressure(whichMomentumAlignedWithDirection, quantities);
	
		double pressure = getPressure(quantities[INDEX_ENERGY], getRhoV2(quantities));
		res[whichMomentumAlignedWithDirection] += pressure / (mach*mach);
		
		return res;
	}
	
	protected SquareMatrix primToCons(double vX, double vY, double vZ, double rho, double machParameter, double v2,
			SquareMatrix mat){
		double dUdVarr[][] = new double[5][5];
		double dVdUarr[][] = new double[5][5];
		
		dUdVarr[0][0] = 1.0;
		dUdVarr[1][0] = vX;
		dUdVarr[1][1] = rho;
		dUdVarr[2][0] = vY;
		dUdVarr[2][2] = rho;
		dUdVarr[3][0] = vZ;
		dUdVarr[3][3] = rho;
		dUdVarr[4][0] = machParameter*machParameter * v2/2;
		dUdVarr[4][1] = machParameter*machParameter * rho*vX;
		dUdVarr[4][2] = machParameter*machParameter * rho*vY;
		dUdVarr[4][3] = machParameter*machParameter * rho*vZ;
		dUdVarr[4][4] = 1.0/(gamma() - 1);
		
		dVdUarr[0][0] = 1.0;
		dVdUarr[1][0] = -vX/rho;
		dVdUarr[1][1] = 1.0/rho;
		dVdUarr[2][0] = -vY/rho;
		dVdUarr[2][2] = 1.0/rho;
		dVdUarr[3][0] = -vZ/rho;
		dVdUarr[3][3] = 1.0/rho;
		dVdUarr[4][0] = machParameter*machParameter * (gamma()-1)*v2/2;
		dVdUarr[4][1] = -machParameter*machParameter * (gamma()-1)*vX;
		dVdUarr[4][2] = -machParameter*machParameter * (gamma()-1)*vY;
		dVdUarr[4][3] = -machParameter*machParameter * (gamma()-1)*vZ;
		dVdUarr[4][4] = (gamma() - 1);
	
		SquareMatrix dUdV = new SquareMatrix(dUdVarr);
		SquareMatrix dVdU = new SquareMatrix(dVdUarr);
	
		return dUdV.mult(mat.mult(dVdU)).toSquareMatrix();
	}
	
	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		return diagonalizationWithModification(i, j, k, quantities, direction,
				1.0);
	}
	
	public SquareMatrix[] diagonalizationWithModification(int i, int j, int k, double[] quantities, int direction,
			double thornberModificationFactor) {
		SquareMatrix[] mat = new SquareMatrix[3];
		double[] eval = new double[5];
		int momentumDirection = getIndexDirection(direction);
		HydroState state = new HydroState(quantities, momentumDirection, this);
		
		double[][] inverse = new double[5][5];
		
		for (int ii = 0; ii < inverse.length; ii++){
			for (int jj = 0; jj < inverse.length; jj++){
				inverse[ii][jj] = 0.0;
			}
			eval[ii] = 0.0;
		}
		
		double[] unitVector = new double[]{0.0,
				(direction == GridCartesian.X_DIR) ? 1.0 : 0.0,
				(direction == GridCartesian.Y_DIR) ? 1.0 : 0.0,
				(direction == GridCartesian.Z_DIR) ? 1.0 : 0.0, 0.0};	
	
		SquareMatrix rightEigenvectors = new SquareMatrix(5);
		SquareMatrix leftEigen1Forms = new SquareMatrix(5);
		SquareMatrix eigenValues = new SquareMatrix(5);
		
		double[] eigenValuesArr;
		
		rightEigenvectors.set(getJacobianEigenvectorsFor1DFlowWithModification(momentumDirection, state.vX, state.vY, state.vZ, state.v2,
				state.soundSpeed, state.enthalpy, thornberModificationFactor)); 
    		    
		if (Double.isNaN(state.soundSpeed)){
			System.err.println("Sound speed NaN " + state.pressure + " " + state.rho + " " + state.energy + " " + i + " " + j + " " + k);
		}
		
	    eigenValuesArr = getJacobianEigenvaluesFor1DFlow(state.v, state.soundSpeed);
		//eigenValuesArr = getJacobianEigenvaluesFor1DFlow(Math.max(state.vX, state.vY), state.soundSpeed);
	    
	    /*if (returnSign){
	    	for (int k = 0; k < eigenValuesArr.length; k++){
	    		eigenValuesArr[k] = (eigenValuesArr[k] >= 0) ? 1.0 : -1.0;
	    	}
	    	eigenValues = new DiagonalMatrix(eigenValuesArr);
	    } else {*/   // remove when not needed
	    	
	    eigenValues = new DiagonalMatrix(eigenValuesArr);
	    	    
	    inverse[4][0] = (state.enthalpy - state.v2) * (gamma() - 1) / state.soundSpeed / state.soundSpeed;
	    inverse[4][1] =	state.vX * (gamma() - 1) / state.soundSpeed / state.soundSpeed; 
	    inverse[4][2] = state.vY * (gamma() - 1) / state.soundSpeed / state.soundSpeed; 
	    inverse[4][3] = state.vZ * (gamma() - 1) / state.soundSpeed / state.soundSpeed; 
	    inverse[4][4] = -(gamma() - 1) / state.soundSpeed / state.soundSpeed;
	    	    
	    if (direction == GridCartesian.X_DIR){
	    	inverse[2] = new double[]{- state.vY, 0.0, 1.0, 0.0, 0.0};
	    	inverse[3] = new double[]{- state.vZ, 0.0, 0.0, 1.0, 0.0};
	    } else if (direction == GridCartesian.Y_DIR){
	    	inverse[2] = new double[]{- state.vX, 1.0, 0.0, 0.0, 0.0};
	    	inverse[3] = new double[]{- state.vZ, 0.0, 0.0, 1.0, 0.0};
	    } else if (direction == GridCartesian.Z_DIR){
	    	inverse[2] = new double[]{- state.vX, 1.0, 0.0, 0.0, 0.0};
	    	inverse[3] = new double[]{- state.vY, 0.0, 1.0, 0.0, 0.0};
	    } else {
	    	System.err.println("ERORR: Something wrong with directions!");
	    }
	    	
	    inverse[0] = new double[]{1.0 + state.v / state.soundSpeed, 0.0, 0.0, 0.0, 0.0};
	    for (int ii = 0; ii <= 4; ii++){
	    	inverse[0][ii] = 0.5 * (inverse[0][ii]  - unitVector[ii]/state.soundSpeed - inverse[4][ii]);
	    }
	    
	    inverse[1] = new double[]{1.0 - state.v / state.soundSpeed, 0.0, 0.0, 0.0, 0.0};
	    for (int ii = 0; ii <= 4; ii++){
	    	inverse[1][ii] = 0.5 * (inverse[1][ii] + unitVector[ii]/state.soundSpeed - inverse[4][ii]);
	    }
	    
	    leftEigen1Forms.set(inverse);
	    
	    mat[0] = rightEigenvectors;
	    mat[1] = eigenValues;
	    mat[2] = leftEigen1Forms;
	    
	    return mat; 
	}
	  
	public double[] getJacobianEigenvaluesFor1DFlow(double vDirection, double soundSpeed){
		double[] eigenValue = new double[5];
			
		eigenValue[0] = vDirection - soundSpeed/mach;
	    eigenValue[1] = vDirection + soundSpeed/mach; 
		eigenValue[2] = vDirection;
		eigenValue[3] = vDirection;
		eigenValue[4] = vDirection;
			    
		return eigenValue;
	}
	
	public double[][] getJacobianEigenvectorsFor1DFlow(int whichMomentumAlignedWithDirection, 
			double vX, double vY, double vZ, double v2, double soundSpeed, double enthalpy){
		return getJacobianEigenvectorsFor1DFlowWithModification(whichMomentumAlignedWithDirection, 
				vX, vY, vZ, v2, soundSpeed, enthalpy,
				1.0);
	}
	
	public double[][] getJacobianEigenvectorsFor1DFlowWithModification(int whichMomentumAlignedWithDirection, 
			double vX, double vY, double vZ, double v2, double soundSpeed, double enthalpy,
			double thornberModificationFactor){
		double[][] eigenVector = new double[5][5];
		
		double soundSpeedX = 0.0, soundSpeedY = 0.0, soundSpeedZ = 0.0;  // not physical, but just a writing trick
		double nx = 0.0; double ny = 0.0; double nz = 0.0; // only works with cartesian directions, NOT general normals
		
		if (whichMomentumAlignedWithDirection == INDEX_XMOM){ 
			soundSpeedX = soundSpeed; nx = 1.0; }
		if (whichMomentumAlignedWithDirection == INDEX_YMOM){ 
			soundSpeedY = soundSpeed; ny = 1.0; }
		if (whichMomentumAlignedWithDirection == INDEX_ZMOM){ 
			soundSpeedZ = soundSpeed; nz = 1.0; }
		
		eigenVector[INDEX_RHO][0] = 1.0;
		eigenVector[INDEX_XMOM][0] = vX - thornberModificationFactor*soundSpeedX;
		eigenVector[INDEX_YMOM][0] = vY - thornberModificationFactor*soundSpeedY;
		eigenVector[INDEX_ZMOM][0] = vZ - thornberModificationFactor*soundSpeedZ;
		eigenVector[INDEX_ENERGY][0] = enthalpy - vX * soundSpeedX - vY * soundSpeedY - vZ * soundSpeedZ;
    	    
	    eigenVector[INDEX_RHO][1] = 1.0;
	    eigenVector[INDEX_XMOM][1] = vX + thornberModificationFactor*soundSpeedX;
	    eigenVector[INDEX_YMOM][1] = vY + thornberModificationFactor*soundSpeedY;
	    eigenVector[INDEX_ZMOM][1] = vZ + thornberModificationFactor*soundSpeedZ;
		eigenVector[INDEX_ENERGY][1] = enthalpy + vX * soundSpeedX + vY * soundSpeedY + vZ * soundSpeedZ;
			    
	    eigenVector[INDEX_RHO][2] = 0.0;
	    eigenVector[INDEX_XMOM][2] = 1.0-nx;
	    eigenVector[INDEX_YMOM][2] = 1.0-ny-nz;	
	    eigenVector[INDEX_ZMOM][2] = 0.0;
		eigenVector[INDEX_ENERGY][2] = (1.0-nx)*vX + (1.0-ny-nz)*vY;
	    
	    eigenVector[INDEX_RHO][3] = 0.0;
	    eigenVector[INDEX_XMOM][3] = 0.0;
	    eigenVector[INDEX_YMOM][3] = 1.0-nx-ny;	
	    eigenVector[INDEX_ZMOM][3] = 1.0-nz;
		eigenVector[INDEX_ENERGY][3] = (1.0-nx-ny)*vY + (1.0-nz)*vZ;
	    
	    eigenVector[INDEX_RHO][4] = 1.0;
	    eigenVector[INDEX_XMOM][4] = vX;
	    eigenVector[INDEX_YMOM][4] = vY;
	    eigenVector[INDEX_ZMOM][4] = vZ;
	    eigenVector[INDEX_ENERGY][4] = 0.5*v2;
	    
	    return eigenVector;
	}
	
	

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k, double[] quantities, int direction) {
		int indexDirection = getIndexDirection(direction);
		return quantities[indexDirection] / quantities[INDEX_RHO];
	}
	
	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		res[INDEX_RHO] = quantities[INDEX_RHO];
		res[INDEX_ENERGY] = quantities[INDEX_ENERGY];
		
		double[] transformedVector = transformVector(quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], normalVector);
		res[INDEX_XMOM] = transformedVector[0];
		res[INDEX_YMOM] = transformedVector[1];
		res[INDEX_ZMOM] = transformedVector[2];
		
		return res;

	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		res[INDEX_RHO] = quantities[INDEX_RHO];
		res[INDEX_ENERGY] = quantities[INDEX_ENERGY];
		
		
		double[] transformedVector = transformVectorBack(quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], normalVector);
		res[INDEX_XMOM] = transformedVector[0];
		res[INDEX_YMOM] = transformedVector[1];
		res[INDEX_ZMOM] = transformedVector[2];
		
		return res;
	}
	
}
