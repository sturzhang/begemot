import java.util.LinkedList;


public abstract class GridFromPoints extends GridUnstructured {

	protected GeometricGridCell[] gridCells;
	protected LinkedList<GeometricGridCell> gridCellstmp;
	
	protected LinkedList<CornerPoint> points;
	LinkedList<ListOfCornerPoints> pointsOfCell;
	
    protected int firstValidCell;
	protected int lastValidCell;
    
    protected double minSpacing;
    protected double xmin, xmax, ymin, ymax;
    protected int ghostcells;
    
	public GridFromPoints(double xmin, double xmax, double ymin, double ymax, int ghostcells) {
		super();
		
		this.xmin = xmin; this.xmax = xmax; this.ymin = ymin; this.ymax = ymax;
		this.ghostcells = ghostcells;
		
		points = new LinkedList<CornerPoint>();
		gridCellstmp = new LinkedList<GeometricGridCell>();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(Obstacle obstacle) {
		this.myObstacle = obstacle;
		
		pointsOfCell = new LinkedList<ListOfCornerPoints>();
		
		savePoints();
		
		/* rm xmin = points.getFirst().x(); xmax = xmin;
    	ymin = points.getFirst().y(); ymax = ymin;
    	for (CornerPoint p : points){
    		xmin = Math.min(xmin, p.x()); xmax = Math.max(xmax, p.x());
    		ymin = Math.min(ymin, p.y()); ymax = Math.max(ymax, p.y());
    	}*/
    	
		
		divideIntoCells();
		
		gridCells = gridCellstmp.toArray(new GeometricGridCell[] {null} );
		for (int k = 0; k < gridCells.length; k++){
			gridCells[k].setGridCell(new GridCell(0, 0, k, this));
		}
		
		numberOfCells = gridCellstmp.size();
		
		validCell        = new boolean[numberOfCells];
		for (int k = 0; k < gridCells.length; k++){
			validCell[k] = gridCells[k].isValid();
		}
		
		    	
    	firstValidCell = 0;
    	lastValidCell = 0;
    	boolean validCellFound = false;
    	GridCell adjacentCell;
    	
    	minSpacing = gridCells[0].inscribedCircleRadius();
    	
    	
    	// workaround from http://stackoverflow.com/questions/13047123/workaround-for-generic-arrays-in-java
    	neighbours       = (LinkedList<GridCell>[]) new LinkedList<?>[numberOfCells];
		
    	
		for (int k = 0; k < gridCells.length; k++){
			if ( (!validCellFound) && (validCell[k])){ validCellFound = true; firstValidCell = k; }
			if (validCell[k]){ lastValidCell = k; }
			
			minSpacing = Math.min(minSpacing, gridCells[k].inscribedCircleRadius());
			
			
			neighbours[k] = new LinkedList<GridCell>();
			
			for (GeometricGridEdge e : gridCells[k].edges()){
				if (e.hasNeighbour()){
					adjacentCell = e.getOtherGeometricGridCell(gridCells[k]).gridCell();
					neighbours[k].add(adjacentCell);
				}
			}
		}
	}
	protected abstract void savePoints();
	protected abstract void divideIntoCells();

	
	protected void createGridCell(ListOfCornerPoints pointsOfCel, boolean isValid){
		LinkedList<GeometricGridEdge> tmpEdgeList;
		
		pointsOfCell.add(pointsOfCel);
		tmpEdgeList = pointsOfCel.getEdges();
		// rm edgesByCelltmp.add(tmpEdgeList);
		gridCellstmp.add(new GeometricGridCell(tmpEdgeList, pointsOfCel, isValid));
		// rm validCelltmp.add(isValid);
	}
	
	protected GeometricGridEdge findEdge(GridCell a, GridCell b){
		GeometricGridEdge res = null;
		
		for (GeometricGridEdge e : gridCells[a.k()].edges()){
			if (e.hasNeighbour()){
				if (e.getOtherGeometricGridCell(gridCells[a.k()]) == gridCells[b.k()]){
					res = e;
					break;
				}
			}
		}
		return res;
	}
	
	@Override
	public double edgeSize(GridEdge edge) {
		return findEdge(edge.adjacent1(), edge.adjacent2()).edgeSize();
	}

	@Override
	public double[] normalVector(GridEdge edge) {
		return findEdge(edge.adjacent1(), edge.adjacent2()).unitNormalVector();	    
	}
	
    @Override
    public boolean isInsideCell(GridCell g, double x, double y, double z){
    	GeometricGridCell cell = gridCells[g.k()];
    	LinkedList<GeometricGridEdge> cellEdges = cell.edges();
    	boolean res = true;
    	
    	double crossing, distance;
    	for (GeometricGridEdge e : cellEdges){
    		crossing = ( e.point2().x() - e.point1().x() ) * ( x - e.point1().x() ) + 
    				   ( e.point2().y() - e.point1().y() ) * ( y - e.point1().y() );
    		distance = e.unitNormalVector()[0] * ( e.point1().x()-x ) +
    				   e.unitNormalVector()[1] * ( e.point1().y()-y );
    		
    		if ((crossing >= 0) && (crossing <= e.edgeSize()) && (distance > 0)){
    			// ok
    		} else {
    			res = false;
    		}
    	}
    	return res;
    }

	@Override
	public double cellSize(GridCell cell) { return gridCells[cell.k()].cellSize(); }
	@Override
	protected int indexMin() { return firstValidCell; }
	@Override
	protected int indexMax() { return lastValidCell; }
	@Override
	public double minSpacing() { return minSpacing; }

	
	@Override
	public double getX(int i, int j, int k) { return gridCells[k].center()[0]; }
	@Override
	public double getY(int i, int j, int k) { return gridCells[k].center()[1]; }
	@Override
	public double getZ(int i, int j, int k) { return zmax(); }

    @Override
    public double xmax(){ return xmax; }
    @Override
    public double xmin(){ return xmin; }
    @Override
    public double ymax(){ return ymax; }
    @Override
    public double ymin(){ return ymin; }

	@Override
	public double zmax() { return 0; }
	@Override
	public double zmin() { return 0; }

	@Override
	public int dimensions() { return 2; }

	public void output(){
		boolean firsttime = true;
		ListOfCornerPoints cellPoints;
		
			/*if (firsttime){
				firsttime = false;
			} else {
				
			}//*/
			
		
		for (int k = 0; k < gridCells.length; k++){
			//if ( (k == 125) || (k == 128) || (k == 130) || (k == 433)){
				for (GeometricGridEdge e : gridCells[k].edges()){
					//System.out.println(e.point1().x() + " " + e.point1().y());
					//System.out.println(e.point2().x() + " " + e.point2().y());
					
					e.output();
				}
			//}
		}
				System.out.println("");
				System.out.println("");
				
				
				for (GeometricGridEdge e : myObstacle.getBoundingEdges()){
					e.output();
				}
				/*cellPoints = gridCells[k].points();
				for (CornerPoint p : cellPoints){
					System.out.println(p.x() + " " + p.y());
				}
				System.out.println(cellPoints.getCurrent().x() + " " + cellPoints.getCurrent().y());*/
				
		/*		for (int k = 0; k < gridCells.length; k++){
					if ( (k == 125) || (k == 128) || (k == 130) || (k == 433)){

						for (GeometricGridEdge e : gridCells[k].edges()){
							System.out.println(e.midpoint()[0] + " " + e.midpoint()[1] + " " + e.unitNormalVector()[0] + " " + e.unitNormalVector()[1]);
						}
					}
				}
			*/

	}

}
