import java.util.LinkedList;


public abstract class Obstacle { //extends Boundary {

	protected LinkedList<CellInterface> obstacleBoundary;
	protected Equations equations;	
	boolean[][][] excludeFromTimeEvolution;
	
	protected LinkedList<GeometricGridEdge> boundingEdges = new LinkedList<GeometricGridEdge>();
	
	protected Grid grid;
		
	public Obstacle(Grid grid) {
		//super(grid);
		this.grid = grid;
		this.equations = null;
	}

	public void addBoundingEdge(GeometricGridEdge edge){
		boundingEdges.add(edge);
	}
	
	public LinkedList<GeometricGridEdge> getBoundingEdges(){
		return boundingEdges;
	}
	
	// TODO this should become an abstract method at some point
	public boolean isInside(double[] position){
		return false;
	}
	
	
	public Obstacle(Grid grid, Equations equations) {
		//super(grid);
		this.grid = grid;
		
		this.equations = equations;
		obstacleBoundary = new LinkedList<CellInterface>();
	}

	public abstract void set(double[][][][] conservedQuantities);
	
	
	protected abstract void setObstacleInterior(boolean[][][] excludeFromTimeEvolution);
    public abstract double[][][][][] insertRigidWalls(double[][][][][] fluxes, double[][][][] conservedQuantities);
	
	
	protected void setObstacleBoundaries(){
		// this method only works for obstacles in 2-d
		obstacleBoundary = new LinkedList<CellInterface>();
		
		
		int k = ((GridCartesian) grid).indexMinZ();
		for (int i = 0; i < excludeFromTimeEvolution.length; i++){
			for (int j = 0; j < excludeFromTimeEvolution[i].length; j++){
			
				if (excludeFromTimeEvolution[i][j][k] && !excludeFromTimeEvolution[i-1][j][k]){
					obstacleBoundary.add(new CellInterface(i, j, k, ReconstructionCartesian.WEST));
				}
				if (excludeFromTimeEvolution[i][j][k] && !excludeFromTimeEvolution[i+1][j][k]){
					obstacleBoundary.add(new CellInterface(i, j, k, ReconstructionCartesian.EAST));
				}
				if (excludeFromTimeEvolution[i][j][k] && !excludeFromTimeEvolution[i][j+1][k]){
					obstacleBoundary.add(new CellInterface(i, j, k, ReconstructionCartesian.NORTH));
				}
				if (excludeFromTimeEvolution[i][j][k] && !excludeFromTimeEvolution[i][j-1][k]){
					obstacleBoundary.add(new CellInterface(i, j, k, ReconstructionCartesian.SOUTH));
				}
				
			}
		}

	}

	
	
	
    public abstract String getDetailedDescription();

    public String description(){ 
    	return "# Obstacle used: " + getDetailedDescription(); 
    }

	
	public boolean[][][] setObstacle(boolean[][][] excludeFromTimeEvolution){
		/* we need a copy of this array
		* this workaround removes the necessity of remembering to set it in every implementation of the abstract method
		*/
		setObstacleInterior(excludeFromTimeEvolution);
		this.excludeFromTimeEvolution = excludeFromTimeEvolution;
		setObstacleBoundaries();
		return excludeFromTimeEvolution;
	}
	

}
