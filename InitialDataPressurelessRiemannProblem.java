
public class InitialDataPressurelessRiemannProblem extends InitialDataPressurelessEuler {

	public InitialDataPressurelessRiemannProblem(Grid grid, EquationsPressurelessEuler equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x = g.getX() - grid.xMidpoint();
    	//double y = g.getY() - grid.yMidpoint();
    	
		double v;
    	
		res[EquationsPressurelessEuler.INDEX_RHO] = 0.5;
		
    	if (x < -0.5){     v = -0.5;    } 
    	else if (x < 0){   v = 0.4;     }
    	else if (x < 0.8){ v = 0.4 - x; }
    	else {             v = -0.4;    }
    				
    	res[EquationsPressurelessEuler.INDEX_XMOM] = v*res[EquationsPressurelessEuler.INDEX_RHO];
    	res[EquationsPressurelessEuler.INDEX_YMOM] = 0.0;
    	res[EquationsPressurelessEuler.INDEX_ZMOM] = 0.0;
    			
    		
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Riemann problem from Berthon et al. 2006 (test no. 1)";
	}

}
