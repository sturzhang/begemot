import matrices.Matrix;
import matrices.SquareMatrix;

import org.ejml.data.DMatrixRMaj;
import org.ejml.data.DMatrixSparseCSC;
import org.ejml.sparse.csc.CommonOps_DSCC;


public class IterationFiniteElement extends Iteration {

	protected static double EPSILON = 1e-10;
	
	protected String outputPath = "dat/test.dat";

	public InitialData initialData;
	public TimeIntegratorFiniteElement timeIntegrator;
	public Equations equations;
	
	protected double waveSpeed; // TODO works only for linear eqautions
	
	public IterationFiniteElement() {
		// nothing
	}

	@Override
	public String getDescriptions() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void run(double maxTime) {
		grid.initialize(null);

		FileInteraction.VERBOSE = false;
    	FileInteraction.writefile(outputPath, "");
    	
    	
    	// ---- dealing with the equations ----
    	// TODO implementation valid for linear equations only
		
    	setWaveSpeed(0);
    	
    	SquareMatrix[] tmpMat = getInvAbsAndJacobian(GridCartesian.X_DIR);
		SquareMatrix invAbsJx = tmpMat[0];
    	SquareMatrix Jx       = tmpMat[1];
		
    	tmpMat = getInvAbsAndJacobian(GridCartesian.Y_DIR);
		SquareMatrix invAbsJy = tmpMat[0];
    	SquareMatrix Jy       = tmpMat[1];
    	
    	SquareMatrix tauX = invAbsJx.mult(((GridCartesian) grid).xSpacing()/2).toSquareMatrix();
    	SquareMatrix tauY = invAbsJy.mult(((GridCartesian) grid).ySpacing()/2).toSquareMatrix();
    	
    	//double tauX = ((GridCartesian) grid).xSpacing()/2/Math.abs(U);
    	//double tauY = (V == 0 ? 0 : ((GridCartesian) grid).ySpacing()/2/Math.abs(V));
    	
    	
    	
    	
    	// ---- initialization of arrays ----

    	
    	int gridSize = ((GridCartesian) grid).nx()*((GridCartesian) grid).ny();
    	int size = gridSize*equations.getNumberOfConservedQuantities();
    	
    	DMatrixSparseCSC massMatrix = new DMatrixSparseCSC(gridSize,gridSize,3*gridSize);
    	
    	DMatrixSparseCSC DxMatrix = new DMatrixSparseCSC(gridSize,gridSize,2*gridSize);
    	DMatrixSparseCSC DyMatrix = new DMatrixSparseCSC(gridSize,gridSize,2*gridSize);
    	
    	DMatrixSparseCSC DxDxMatrix = new DMatrixSparseCSC(gridSize,gridSize,3*gridSize);
    	DMatrixSparseCSC DxDyMatrix = new DMatrixSparseCSC(gridSize,gridSize,3*gridSize);
    	DMatrixSparseCSC DyDyMatrix = new DMatrixSparseCSC(gridSize,gridSize,3*gridSize);
    	
    	
    	
    	DMatrixRMaj solution = new DMatrixRMaj(size,1);
    	
    	
    	
    	// ---- end of initialization of arrays ----

    	
    	// grid.setAllGhostcells(); // TODO ??
    	// grid.setAllEdges(); ? necessary? 
		
    	
    	
    	double dx = ((GridCartesian) grid).xSpacing(); //(xmax - xmin) / (size-1);
    	double dy = ((GridCartesian) grid).ySpacing();
    	
    	double cfl = 0.2;
    	double dt = cfl*dx/waveSpeed;
    	
    	double outputTimeInterval = 10.0;
    	
    	
    	initialData.fillConservedQuantities(grid.conservedQuantities);
		
    	
    	System.err.println("Set initial data");
    	
    	int counter = 0;
    	for (GridCell g : grid) {
    		for (int q = 0; q < equations.getNumberOfConservedQuantities(); q++){
    			solution.set(counter, 0, grid.conservedQuantities[g.i()][g.j()][g.k()][q]);
    			System.err.println(counter + " / " + size);
        		counter++;
    		}
    	}
    	
    	// TODO we do not really include boundaries here
    	
    	System.err.println("Copied initial data");
    	
    	FiniteElementMatrixCalculator matrixCalc = new FiniteElementMatrixCalculator((GridCartesian) grid, 40);
    	
    	int counter1 = 0;
    	int counter2;
    	double tmp;
    	for (GridCell g1 : grid){
    		counter2 = 0;
    		for (GridCell g2 : grid){
    			
    			/*tmp = matrixCalc.getMassMatrixElement(g1, g2);
    			if (Math.abs(tmp) > EPSILON){ massMatrix.set(counter1, counter2, tmp); } //*/
    			
    			if (counter1 == counter2){ massMatrix.set(counter1, counter2, dx*dy); }
    			
    			tmp = matrixCalc.getDxMatrixElement(  g1, g2);
    			//tmp = matrixCalc.getSimpleDxMatrixElement(  g1, g2);
    			if (Math.abs(tmp) > EPSILON){ DxMatrix.set(  counter1, counter2, tmp); }
    			
    			tmp = matrixCalc.getDxDxMatrixElement(g1, g2);
    			//tmp = matrixCalc.getSimpleDxDxMatrixElement(g1, g2);
    			if (Math.abs(tmp) > EPSILON){ DxDxMatrix.set(counter1, counter2, tmp); }
    			
    			if (grid.dimensions() > 1){
	    			tmp = matrixCalc.getDyMatrixElement(  g1, g2);
	    			//tmp = matrixCalc.getSimpleDyMatrixElement(  g1, g2);
	    			if (Math.abs(tmp) > EPSILON){ DyMatrix.set(  counter1, counter2, tmp); }
	    			
	    			tmp = matrixCalc.getDxDyMatrixElement(g1, g2);
	    			if (Math.abs(tmp) > EPSILON){ DxDyMatrix.set(counter1, counter2, tmp); } //*/
	    			
	    			tmp = matrixCalc.getDyDyMatrixElement(g1, g2);
	    			//tmp = matrixCalc.getSimpleDyDyMatrixElement(g1, g2);
	    			if (Math.abs(tmp) > EPSILON){ DyDyMatrix.set(counter1, counter2, tmp); }
    			}
    			
    			counter2++;
    		}
    		System.err.println(counter1 + " / " + size);
    		
    		counter1++;
    	}
    	
    	
    	DMatrixSparseCSC DxMatrixTranspose = new DMatrixSparseCSC(size,size,3*size);
    	CommonOps_DSCC.transpose(DxMatrix, DxMatrixTranspose, null);
    	DMatrixSparseCSC DyMatrixTranspose = new DMatrixSparseCSC(size,size,3*size);
    	CommonOps_DSCC.transpose(DyMatrix, DyMatrixTranspose, null);
    	DMatrixSparseCSC DxDyMatrixTranspose = new DMatrixSparseCSC(size,size,3*size);
    	CommonOps_DSCC.transpose(DxDyMatrix, DxDyMatrixTranspose, null);
    	
    	
    	System.err.println("computed matrices");
    	
    	// periodic boundary conditions
    	/*massMatrix.set(0, gridSize-1, massMatrix.get(row, col));
    	massMatrix.set(0, 1, 0);
    	massMatrix.set(size-1, size-1, 0);
    	massMatrix.set(size-1, size-2, 0); //*/
    	
    	println(solution);
    	    	
    	massMatrix = insert(SquareMatrix.id(equations.getNumberOfConservedQuantities()), massMatrix);
    	DxMatrixTranspose = insert(tauX.mult(Jx), DxMatrixTranspose);
    	DyMatrixTranspose = insert(tauY.mult(Jy), DyMatrixTranspose);
    	
    	DMatrixSparseCSC leftHandMatrix1 = new DMatrixSparseCSC(size,size,
    			size*equations.getNumberOfConservedQuantities()*equations.getNumberOfConservedQuantities());
    	CommonOps_DSCC.add(1.0, massMatrix,     1.0, DxMatrixTranspose, leftHandMatrix1, null, null);
    	DMatrixSparseCSC leftMatrix = new DMatrixSparseCSC(size,size,
    			size*equations.getNumberOfConservedQuantities()*equations.getNumberOfConservedQuantities());
    	CommonOps_DSCC.add(1.0, leftHandMatrix1, 1.0, DyMatrixTranspose, leftMatrix, null, null);
    	
    	
    	//timeIntegrator.setLeftMatrix(leftMatrix);
    	timeIntegrator.setLeftMatrix(massMatrix);
    	System.err.println("computed left matrix");
    	
    	DxMatrix   = insert(Jx, DxMatrix);
    	DxDxMatrix = insert(tauX.mult(Jx).mult(Jx), DxDxMatrix);
    	DxDyMatrix = insert(tauX.mult(Jx).mult(Jy), DxDyMatrix);
    	
    	DMatrixSparseCSC rightHandMatrixTmp1 = new DMatrixSparseCSC(size,size,
    			size*equations.getNumberOfConservedQuantities()*equations.getNumberOfConservedQuantities());
    	CommonOps_DSCC.add(1.0,   DxMatrix,            1.0, DxDxMatrix, rightHandMatrixTmp1, null, null);
    	DMatrixSparseCSC rightHandMatrixTmp12 = new DMatrixSparseCSC(size,size,
    			size*equations.getNumberOfConservedQuantities()*equations.getNumberOfConservedQuantities());
    	CommonOps_DSCC.add(1.0, rightHandMatrixTmp1, 1.0, DxDyMatrix, rightHandMatrixTmp12, null, null);
    	
    	DyMatrix            = insert(Jy, DyMatrix);
    	DxDyMatrixTranspose = insert(tauY.mult(Jy).mult(Jx), DxDyMatrixTranspose);
    	DyDyMatrix          = insert(tauY.mult(Jy).mult(Jy), DyDyMatrix);
    	
    	DMatrixSparseCSC rightHandMatrixTmp2 = new DMatrixSparseCSC(size,size,
    			size*equations.getNumberOfConservedQuantities()*equations.getNumberOfConservedQuantities());
    	CommonOps_DSCC.add(1.0,   DyMatrix,            1.0, DyDyMatrix,          rightHandMatrixTmp2, null, null);
    	DMatrixSparseCSC rightHandMatrixTmp22 = new DMatrixSparseCSC(size,size,
    			size*equations.getNumberOfConservedQuantities()*equations.getNumberOfConservedQuantities());
    	CommonOps_DSCC.add(1.0, rightHandMatrixTmp2, 1.0, DxDyMatrixTranspose, rightHandMatrixTmp22, null, null);
    	    	
    	
    	DMatrixSparseCSC rightHandMatrixTmp = new DMatrixSparseCSC(size,size,3*size);
    	CommonOps_DSCC.add(1.0, rightHandMatrixTmp12, 1.0, rightHandMatrixTmp22, rightHandMatrixTmp, null, null);
    	
    	timeIntegrator.setRightMatrix(rightHandMatrixTmp);
    	System.err.println("computed right matrix");
    	
    	double timeSinceOutput = 0; 
    	
    	
    	for (double time = 0; time < maxTime; time += dt){
    		
	    	solution = timeIntegrator.perform(dt, solution, 3);
	    	
	    	System.err.println("time = " + time);
	    	
	    	timeSinceOutput += dt;
	    	if (timeSinceOutput > outputTimeInterval){
	    		println("\n");
	    		println("\n");
	    		println(solution);
	    		timeSinceOutput -= outputTimeInterval;
	    	}
    	}
    	
    	println("\n");
    	println("\n");
    	println(solution);
	
	}

	protected void setWaveSpeed(double value){ waveSpeed = value; }
	
	public void setOutputPath(String path){ outputPath = path; }
	
	protected SquareMatrix[] getInvAbsAndJacobian(int direction){
		// TODO works only for linear equations
		double[] quantities = new double[equations.getNumberOfConservedQuantities()];
    	SquareMatrix[] mat = equations.diagonalization(0,0,0, quantities, direction);
    	
    	SquareMatrix invAbsEval = new SquareMatrix(mat[1].rows());
    	double eval, absEval;
    	for (int ii = 0; ii < mat[1].rows(); ii++){
    		eval = mat[1].value(ii,ii);
    		absEval = Math.abs(eval);
			setWaveSpeed(Math.max(absEval, waveSpeed));
			if (eval == 0){
				invAbsEval.setElement(ii, ii, 0);
			} else {
				invAbsEval.setElement(ii, ii, 1.0 / absEval);
			}
		}
		
    	SquareMatrix[] res = new SquareMatrix[2];
		res[0] = mat[0].mult(invAbsEval).mult(mat[2]).toSquareMatrix();
		res[1] = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		return res;
	}
	
    protected void println(DMatrixRMaj mat){
    	String res = "";
    	int counter = 0;
    	
    	for (GridCell g : grid){
    		res += g.getPosition()[0] + " " + g.getPosition()[1] + " ";
    		for (int q = 0; q < equations.getNumberOfConservedQuantities(); q++){
    			res += mat.get(equations.getNumberOfConservedQuantities()*counter + q, 0) + " ";
    		}
    		res += "\n";
    		counter++;
    	}
    	
    	/*for (int i = 0; i < mat.getNumRows(); i++){
    		for (int j = 0; j < mat.getNumCols(); j++){
    			res += mat.get(i, j) + " ";
    		}
    		res += "\n";
    	}//*/
    	println(res);
    }
    
    protected DMatrixSparseCSC insert(Matrix small, DMatrixSparseCSC big){
    	DMatrixSparseCSC res = new DMatrixSparseCSC(big.getNumRows()*small.rows(), big.getNumCols()*small.columns(), big.getNumElements()*small.rows()*small.columns());
    	for (int i = 0; i < big.getNumRows(); i++){
    		for (int j = 0; j < big.getNumCols(); j++){
    			 if (big.isAssigned(i, j)){
    				 
    				 for (int ii = 0; ii < small.rows(); ii++){
    					 for (int jj = 0; jj < small.columns(); jj++){
    						 res.set(i*small.rows()    + ii, 
    								 j*small.columns() + jj, 
    								 big.get(i, j)*small.value(ii, jj));
    					 }
    				 }
    				 
    			 }
    		}
    	}
    	return res;
    }

    protected void println(String s){
    	FileInteraction.append(outputPath, s);
    	System.out.println(s);	
    }
    
	protected double[][][][] matrixToArray(DMatrixRMaj mat){
		int counter = 0;
		double[][][][] res = new double[((GridCartesian) grid).nx()][((GridCartesian) grid).nx()][((GridCartesian) grid).nx()][equations.getNumberOfConservedQuantities()];
		for (GridCell g : grid){
    		for (int q = 0; q < equations.getNumberOfConservedQuantities(); q++){
    			res[g.i()][g.j()][g.k()][q] = mat.get(equations.getNumberOfConservedQuantities()*counter + q, 0);
    		}
    		counter++;
    	}
		return res;
	}
}
