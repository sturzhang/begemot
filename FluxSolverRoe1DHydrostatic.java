import matrices.SquareMatrix;


public class FluxSolverRoe1DHydrostatic extends FluxSolverRoe {

	protected SourceConstantGravity source;
	
	public FluxSolverRoe1DHydrostatic(GridCartesian grid, EquationsHydroIdeal equations, SourceConstantGravity source) {
		super(grid, equations);
		this.source = source;
	}

	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		SquareMatrix mat = new SquareMatrix(5);
		
		double c = ((EquationsHydroIdeal) equations).getSoundSpeedFromConservative(quantities); 
		double v = quantities[EquationsHydroIdeal.INDEX_XMOM]/quantities[EquationsHydroIdeal.INDEX_RHO];
		double absV = Math.abs(v);
		double gamma = ((EquationsHydroIdeal) equations).gamma();
		
		double extraTerm = -((GridCartesian) grid).ySpacing() * source.gy() / 12;
		
		mat.setElement(0,0, absV); mat.setElement(0,1, 0.0); mat.setElement(0,4, (gamma-1)/c*(1 - absV / c));
		
		mat.setElement(1,0, - c*v + v*v + extraTerm); mat.setElement(1,1, c); mat.setElement(1,4, 2.0*v/c*(gamma-1)*(1 - v/c));
		
		mat.setElement(4,0, 0.0); mat.setElement(4,1, 0.0); mat.setElement(4,4, c);
		
		return new UpwindingMatrixAndWaveSpeed(
				mat.toSquareMatrix(), absV + c);
	}

	@Override
	public String getDetailedDescription() {
		return "flux solver that is similar to subsonic roe, but is takes special care about gravity";
	}

}
