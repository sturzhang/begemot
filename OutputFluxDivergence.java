
public class OutputFluxDivergence extends Output {

	public OutputFluxDivergence(double outputTimeInterval, GridCartesian grid, Equations equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "Computes a discrete version of the flux differences (which vanishes for stationary flows)";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double[] flux = new double[quantities[0][0][0].length];
		double verticalAverageFluxLeft, verticalAverageFluxRight;
		double[] fluxLeft = null, fluxRight = null, fluxLeftTop = null, fluxLeftBottom = null, fluxRightTop = null, fluxRightBottom = null;
		int counter = 0; 
		
		for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
			for (int j = ((GridCartesian) grid).indexMinY(); j <= ((GridCartesian) grid).indexMaxY(); j++){
				for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
					counter++;
					
					for (int direction = 0; direction < grid.dimensions(); direction++){
						if (direction == GridCartesian.X_DIR){
							fluxLeft  = equations.fluxFunction(i, j, k, quantities[i-1][j][k], direction);
							fluxRight = equations.fluxFunction(i, j, k, quantities[i+1][j][k], direction);
		
							fluxLeftTop    = equations.fluxFunction(i, j, k, quantities[i-1][j+1][k], direction);
							fluxLeftBottom = equations.fluxFunction(i, j, k, quantities[i-1][j-1][k], direction);
							
							fluxRightTop    = equations.fluxFunction(i, j, k, quantities[i+1][j+1][k], direction);
							fluxRightBottom = equations.fluxFunction(i, j, k, quantities[i+1][j-1][k], direction);
						} else if (direction == GridCartesian.Y_DIR){
							fluxLeft  = equations.fluxFunction(i, j, k, quantities[i][j-1][k], direction);
							fluxRight = equations.fluxFunction(i, j, k, quantities[i][j+1][k], direction);
		
							fluxLeftTop    = equations.fluxFunction(i, j, k, quantities[i-1][j-1][k], direction);
							fluxLeftBottom = equations.fluxFunction(i, j, k, quantities[i+1][j-1][k], direction);
							
							fluxRightTop    = equations.fluxFunction(i, j, k, quantities[i-1][j+1][k], direction);
							fluxRightBottom = equations.fluxFunction(i, j, k, quantities[i+1][j+1][k], direction);
						} else {
							System.err.println("ERROR: Flux divegence output not implemented for three dimensions!");
						}
							
						for (int q = 0; q < quantities[i][j][k].length; q++){
							verticalAverageFluxLeft  = 0.25*( fluxLeftTop[q]  + 2.0*fluxLeft[q]  + fluxLeftBottom[q]  );
							verticalAverageFluxRight = 0.25*( fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q] );
							flux[q] += Math.abs(0.5*(verticalAverageFluxRight - verticalAverageFluxLeft));
						}
					}
				}
			}
		}
		
		println(time + " " + (flux[0]/counter) + " " + (flux[1]/counter) + " " + (flux[2]/counter));
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}

}
