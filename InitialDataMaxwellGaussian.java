
public class InitialDataMaxwellGaussian extends InitialDataMaxwell {

	public InitialDataMaxwellGaussian(Grid grid, EquationsMaxwell equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double x, y, r;
		double[]res = new double[q];
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
    	
		r = Math.sqrt(x*x+y*y);
    	
		double gaussianAmplitude = 1.0;
		double width = 0.2;
		double amplitude = gaussianAmplitude * Math.exp(- r*r / width/width);
    				
		res[EquationsMaxwell.INDEX_EX] = -y*amplitude;
		res[EquationsMaxwell.INDEX_EY] = x*amplitude;
		res[EquationsMaxwell.INDEX_EZ] = 0;
		
		res[EquationsMaxwell.INDEX_BX] = 0;
		res[EquationsMaxwell.INDEX_BY] = 0;
		res[EquationsMaxwell.INDEX_BZ] = 0;
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Gaussian in the electric field";
	}

}
