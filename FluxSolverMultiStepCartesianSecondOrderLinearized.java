import matrices.SquareMatrix;

public abstract class FluxSolverMultiStepCartesianSecondOrderLinearized extends FluxSolverMultiStepCartesianSecondOrder {

	public FluxSolverMultiStepCartesianSecondOrderLinearized(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge, 
			double[] leftleft, double[] left, double[] right, double[] rightright){

		// TODO this method is very similar to the one in DimSplitSecondOrder!
		
		FluxMultiStepAndWaveSpeed res;
		double[][] interfaceValues = getInterfaceValues(gridEdge, leftleft, left, right, rightright);
		
		GridCell g = gridEdge.adjacent1();
		res = flux(g.i(), g.j(), g.k(), 
				left, right, 
				interfaceValues[0], interfaceValues[1], interfaceValues[2],				
				GridCartesian.X_DIR);
		
		return res;
	}
	
	
	protected abstract double[][] getInterfaceValues(GridEdgeMapped gridEdge, 
			double[] leftleftValue, double[] leftValue, double[] rightValue, double[] rightrightValue);
	
	
	protected FluxMultiStepAndWaveSpeed flux(int i, int j, int k, double[] left, double[] right, 
			double[] interfaceValueLeft, double[] interfaceValue, double[] interfaceValueRight,
			int direction){
		double[][] flux = new double[steps()][left.length];
		//double c = ((EquationsAcoustic) equations).soundspeed();
		
		
		// TODO WARNING this is not working in general (because of transformation)!!
		// this has to enter the factor!
		double dx = ((GridCartesian) grid).xSpacing();
		
		
		double[] term = new double[left.length];
		double[] termAbs = new double[left.length];
		
		SquareMatrix[] mat = equations.diagonalization(i,j,k, interfaceValue, direction);
		SquareMatrix absEVal = mat[1].absElementwise();
		
		for (int fluxSolverStep = 0; fluxSolverStep < steps(); fluxSolverStep++){
			if (fluxSolverStep == 0){
				// TODO or fluxFunction
				flux[fluxSolverStep] = mat[0].mult(mat[1].mult(mat[2].mult(interfaceValue)));
			} else if (fluxSolverStep == 1){			
				for (int q = 0; q < left.length; q++){
					term[q] = interfaceValueLeft[q] - interfaceValueRight[q] + 3.0*right[q]  - 3.0*left[q];
					termAbs[q] = interfaceValueLeft[q] + 4.0*interfaceValue[q]  + interfaceValueRight[q] 
							- 3.0*right[q] - 3.0*left[q];
				}
				term    = absEVal.mult(mat[2].mult(term));
				termAbs = mat[1].mult(mat[2].mult(termAbs));
				for (int q = 0; q < left.length; q++){
					term[q] = term[q] + termAbs[q];
				}
							
				flux[fluxSolverStep] = mat[0].mult(-0.5/dx).mult(absEVal.mult(term));
			} else if (fluxSolverStep == 2){
				for (int q = 0; q < left.length; q++){
					term[q] = interfaceValueLeft[q] + 2.0*interfaceValue[q] + interfaceValueRight[q] 
							- 2.0*right[q]  - 2.0*left[q];
					termAbs[q] = interfaceValueLeft[q] - interfaceValueRight[q] + 2.0*right[q] - 2.0*left[q];
				}
				term    = absEVal.mult(mat[2].mult(term));
				termAbs = mat[1].mult(mat[2].mult(termAbs));
				for (int q = 0; q < left.length; q++){
					term[q] = term[q] + termAbs[q];
				}
							
				flux[fluxSolverStep] = mat[0].mult(0.5/dx/dx).mult(absEVal.mult(mat[1].mult(term)));
			} else {
				System.err.println("ERROR: Flux solver is being called with more steps than possible");
				return null;
			}
		}

		double maxSpeed = mat[1].value(0, 0);
		for (int ii = 1; ii < mat[1].rows(); ii++){
			maxSpeed = Math.max(maxSpeed, absEVal.value(ii,ii));
		}
		
		return new FluxMultiStepAndWaveSpeed(flux, maxSpeed);
	}

	@Override
	public int steps() {
		return 3;
	}

	@Override
	public String getDetailedDescription() {
		return "Continuous reconstruction in 1-d evolved exactly accoridng to Godunov procedure; to lowest order in dt this is a central flux";
	}

}
