
public class SourceGravityWithDerivatives extends SourceInCellOnly {

	SourceGravityHydro simpleSource;
	
	public SourceGravityWithDerivatives(GridCartesian grid, EquationsHydroIdealWith1dDeriv equations, SourceGravityHydro source) {
		super(grid, equations);
		simpleSource = source;
	}

	@Override
	public String getDetailedDescription() {
		return "Source term for hydro with gravity, with explicit evolution of the spatial derivatives";
	}
	
	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell gridcell, double time) {
		double[] res = new double[quantities.length];
		double[] ressimple = simpleSource.getSourceTerm(quantities, gridcell, time);
		
		// TODO we should be able to perform this with g pointing in any direction!
		double[][] jac = null; // continue here equations.JacobianWOutDeriv(quantities, grid.Y_DIR);
		double[] jacPsi = new double[quantities.length];
		for (int ii = 0; ii < jac.length; ii++){
			jacPsi[ii] = 0.0;
			for (int jj = 0; jj < jac.length; jj++){
				jacPsi[ii] += jac[ii][jj]*quantities[jj+5];
			}
		}
		
		res[EquationsHydroIdeal.INDEX_RHO]    = ressimple[EquationsHydroIdeal.INDEX_RHO] - jacPsi[EquationsHydroIdeal.INDEX_RHO];
		res[EquationsHydroIdeal.INDEX_XMOM]   = ressimple[EquationsHydroIdeal.INDEX_XMOM] - jacPsi[EquationsHydroIdeal.INDEX_XMOM];
		res[EquationsHydroIdeal.INDEX_YMOM]   = ressimple[EquationsHydroIdeal.INDEX_YMOM] - jacPsi[EquationsHydroIdeal.INDEX_YMOM];
		res[EquationsHydroIdeal.INDEX_ZMOM]   = ressimple[EquationsHydroIdeal.INDEX_ZMOM] - jacPsi[EquationsHydroIdeal.INDEX_ZMOM];
		res[EquationsHydroIdeal.INDEX_ENERGY] = ressimple[EquationsHydroIdeal.INDEX_ENERGY] - jacPsi[EquationsHydroIdeal.INDEX_ENERGY];
		
		return res;

	}

}
