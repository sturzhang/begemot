
public abstract class SourceGravityAcoustic extends SourceInCellOnly {

	public SourceGravityAcoustic(GridCartesian grid, EquationsAcoustic equations) {
		super(grid, equations);
	}
	public SourceGravityAcoustic(GridCartesian grid, EquationsAcoustic equations, boolean averaged) {
		super(grid, equations);
	}
	
	@Override
	protected double[] getSourceTerm(double[] quantities, GridCell gg, double time) {
		double[] res = new double[quantities.length];
		double[] g = gravityacceleration(gg.i(),gg.j(),gg.k());
		
		res[EquationsAcoustic.INDEX_VX]   = g[0];
		res[EquationsAcoustic.INDEX_VY]   = g[1];
		res[EquationsAcoustic.INDEX_VZ]   = g[2];
		
		res[EquationsAcoustic.INDEX_P] = 0.0;
		
		return res;
	}

	protected abstract double[] gravityacceleration(int i, int j, int k);
	
}
