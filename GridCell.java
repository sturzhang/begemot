
public class GridCell {
	
	/*
	 * 
	 * philosophy: as gridcell is just a placeholder for a coordinate tuple, it should have as 
	 * little properties as possible (that would need to be calculated every time such a gridcell
	 * is created). gridcells will be created over and over again (anew in every loop for example)
	 * and this extra calculations should only be performed when they are really necessary.
	 * therefore all properties that are not absolutely necessary should not be saved but asked for
	 * at the grid. the grid of course should precompute as many of them as possible.
	 * 
	 */
	
	protected int i, j, k; 
	protected Grid grid;
	
	public GridCell(int i, int j, int k, Grid grid){
		this.grid = grid;
		this.i = i;
		this.j = j;
		this.k = k;
	}

	public int i(){ return i; }
	public int j(){ return j; }
	public int k(){ return k; }

	public double getX(){ return grid.getX(i, j, k); }
	public double getY(){ return grid.getY(i, j, k); }
	public double getZ(){ return grid.getZ(i, j, k); }
	
	public double[] getPosition(){ return new double[]{getX(), getY(), getZ()}; } 
	
	public double size(){
		return grid.cellSize(this);
	}
	
	public void addToConservedQuantity(double val, int q){
		grid.conservedQuantities[i][j][k][q] += val;
	}
	
	public double[] interfaceValue(){
		// TODO reconstruction does not yet work here!!
		
		if ((i == -1) || (j == -1) || (k == -1)) {
		System.err.println(i + " " + j + " " + k); }
		return grid.interfaceValues[i][j][k][ReconstructionCartesian.EAST];
	}

	public boolean equals(GridCell g){
		return ((i == g.i()) && (j == g.j()) && (k == g.k()));
	}
	
    @Override
    public String toString(){
    	
    	String res                          = getX() + " ";
    	if (grid.dimensions() >= 2){ res += getY() + " "; }
    	if (grid.dimensions() >= 3){ res += getZ() + " "; }
    	return res;
    }
    
    public String printIndices(){
    	return i + " " + j + " " + k;
    }
	
}
