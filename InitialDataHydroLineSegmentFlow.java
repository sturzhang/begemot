
public class InitialDataHydroLineSegmentFlow extends InitialDataHydro {

	double alpha, vinfty;
	
	public InitialDataHydroLineSegmentFlow(Grid grid, EquationsHydroIdeal equations, double alpha, double vinfty,
			BoundaryLineSegmentFlow boundary) {
		super(grid, equations);
		this.alpha = alpha;
		this.vinfty = vinfty;
		
		if (equations.gamma != 2.0){
			System.out.println("ERROR: gamma should be 2 for potential flow!");
		}
		if ((boundary.alpha() != alpha) || (boundary.vinfty() != vinfty)){
			System.out.println("ERROR: Mismathc between the boundary condition and the initial data!");
		}
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		double x, y, vx, vy;
		double Im1, Im2, Imx, Imy, r1, r2, theta1, theta2;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		r1 = Math.sqrt((x-2)*(x-2) + y*y);
		r2 = Math.sqrt((x+2)*(x+2) + y*y);
		theta1 = Math.atan2(y, x-2);
		theta2 = Math.atan2(y, x+2);
    			
		Im1 = Math.sin((theta1+theta2)/2) / 2 / Math.sqrt(r1*r2);
		Im2 = Math.cos((theta1+theta2)/2) / 2 / Math.sqrt(r1*r2);
    			
		Imx = Im1 * (x * (r2/r1 + r1/r2) + 2 * (r1/r2 - r2/r1)) - Im2 * y * (1.0/r1/r1 + 1.0/r2/r2);
		Imy = Im1 * y * (r2/r1 + r1/r2) + Im2 * (x*(1.0/r1/r1 + 1.0/r2/r2) + 2*(1.0/r2/r2 - 1.0/r1/r1));
    			
		vx = vinfty*(Math.cos(alpha) + Math.sin(alpha)*Imx);
		vy = vinfty*                   Math.sin(alpha)*Imy;
    			    			
		res[EquationsHydroIdeal.INDEX_RHO] = 1.0;					
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
					
		res[EquationsHydroIdeal.INDEX_XMOM] = vx*res[EquationsHydroIdeal.INDEX_RHO];
		res[EquationsHydroIdeal.INDEX_YMOM] = vy*res[EquationsHydroIdeal.INDEX_RHO];
    				
		res[EquationsHydroIdeal.INDEX_ENERGY] = 1000.0;

    	return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "Potential flow around a line segment from Churchill-Ward book (p.388, Ex.12 of Ch.10)";
	}

}
