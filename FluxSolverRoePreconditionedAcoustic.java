
import matrices.SquareMatrix;


public class FluxSolverRoePreconditionedAcoustic extends FluxSolverRoe { 

	private String schemeFlag;
	
	public FluxSolverRoePreconditionedAcoustic(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
		schemeFlag = "";
	}
	
	public FluxSolverRoePreconditionedAcoustic(GridCartesian grid, EquationsAcousticSimple equations, String schemeFlag) {
		super(grid, equations);
		this.schemeFlag = schemeFlag;
	}
		
	@Override
	public UpwindingMatrixAndWaveSpeed getAveragedUpwindingMatrix(int i, int j, int k, double[] left, double[] right,
			int direction) {
		int pos = 0; // to make compiler happy
		double vDir = 0.0;
		if (direction == GridCartesian.X_DIR){ pos = 0; vDir = (left[EquationsAcousticSimple.INDEX_VX]+right[EquationsAcousticSimple.INDEX_VX])/2; }
		if (direction == GridCartesian.Y_DIR){ pos = 1; vDir = (left[EquationsAcousticSimple.INDEX_VY]+right[EquationsAcousticSimple.INDEX_VY])/2; }
		if (direction == GridCartesian.Z_DIR){ pos = 2; vDir = (left[EquationsAcousticSimple.INDEX_VZ]+right[EquationsAcousticSimple.INDEX_VZ])/2; }
		double[][] mat = new double[4][4];
		
		
		if (((EquationsAcousticSimple) equations).symmetric()) { System.err.println("Solver is not meant to be applied to the symmetrized form of the acoustic equations!S"); }
		
		if (schemeFlag == ""){
			mat[3][pos] = -((EquationsAcousticSimple) equations).soundspeed()*((EquationsAcousticSimple) equations).soundspeed();
			mat[pos][3] = 1.0;
		}
		if (schemeFlag == "1001"){
			mat[3  ][3  ] = 200.0; //equations.soundspeed();
			mat[pos][pos] = ((EquationsAcousticSimple) equations).soundspeed();
		}
		if (schemeFlag == "0200"){
			/*mat[0][0] = Math.abs(vDir);
			mat[1][1] = Math.abs(vDir);
			mat[2][2] = Math.abs(vDir);
			mat[3][3] = Math.abs(vDir); //*/
			
			mat[3][pos] = -((EquationsAcousticSimple) equations).soundspeed()*((EquationsAcousticSimple) equations).soundspeed();
			mat[pos][3] = 1.0;
			
			//mat[pos][pos] = vDir;
		}
		if (schemeFlag == "0201"){
			/*mat[0][0] = Math.abs(vDir);
			mat[1][1] = Math.abs(vDir);
			mat[2][2] = Math.abs(vDir); //*/
			
			//mat[pos][pos] = 1.0;
			//mat[3][pos] = -((EquationsAcoustic) equations).soundspeed()*((EquationsAcoustic) equations).soundspeed();
			//mat[3][pos] = equations.soundspeed()*equations.soundspeed();
			mat[pos][3] = 1.0;
			mat[3][3] = 2.0*((EquationsAcousticSimple) equations).soundspeed();
			
			//mat[pos][pos] = vDir;
		}
		if (schemeFlag == "0001"){
			mat[3][pos] = -((EquationsAcousticSimple) equations).soundspeed()*((EquationsAcousticSimple) equations).soundspeed();
			mat[pos][3] = 0.0;
			mat[3][3] = 2.0*((EquationsAcousticSimple) equations).soundspeed();
			//mat[pos][pos] = vDir;
		}
		if (schemeFlag == "0000"){
			
		}
		
		
		return new UpwindingMatrixAndWaveSpeed(
				new SquareMatrix(mat),
				((EquationsAcousticSimple) equations).soundspeed());
	}

	@Override
	public String getDetailedDescription() {
		return "Roe-type solver with the Miczek et al preconditioned for the acoustic system";
	}

}
