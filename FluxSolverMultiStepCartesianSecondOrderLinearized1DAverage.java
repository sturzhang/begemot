
public class FluxSolverMultiStepCartesianSecondOrderLinearized1DAverage extends
		FluxSolverMultiStepCartesianSecondOrderLinearized {

	public FluxSolverMultiStepCartesianSecondOrderLinearized1DAverage(
			GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected double[][] getInterfaceValues(GridEdgeMapped gridEdge, 
			double[] leftValue, double[] leftleft, double[] rightValue, double[] rightright){
		
		
		double[][] res = new double[3][leftValue.length];
		for (int q = 0; q < leftValue.length; q++){
			res[0][q]  = average(leftleft[q],   leftValue[q]);
			res[1][q]  = average(leftValue[q],  rightValue[q]);
			res[2][q]  = average(rightValue[q], rightright[q]);
		}
		return res;
	}
	
	protected double average(double left, double right){
		return 0.5*(left + right);
	}

}
