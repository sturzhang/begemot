import matrices.SquareMatrix;


public class FluxSolverRoeHLLEfix extends FluxSolverRoe {

	public FluxSolverRoeHLLEfix(Grid grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected SquareMatrix getAbsoluteValueOfEigenvalues(SquareMatrix eigenvalues,
			double[] left, double[] right, int ii, int jj, int kk, int direction){
		SquareMatrix leftEval  = equations.diagonalization(ii, jj, kk, left,  direction)[1];
		SquareMatrix rightEval = equations.diagonalization(ii, jj, kk, right, direction)[1];
		SquareMatrix res = new SquareMatrix(eigenvalues.rows());

		double maxEval = eigenvalues.value(0,0), minEval = eigenvalues.value(0,0);
		double maxEvalRight = rightEval.value(0, 0);
		double minEvalLeft  = leftEval.value(0, 0);
		for (int k = 1; k < eigenvalues.rows(); k++){
			maxEval = Math.max(maxEval, eigenvalues.value(k, k));
			minEval = Math.min(minEval, eigenvalues.value(k, k));
			maxEvalRight = Math.max(maxEvalRight, rightEval.value(k, k));
			minEvalLeft = Math.min(minEvalLeft,  leftEval.value(k, k));
		}
		double bplus  = Math.max(0, Math.max(maxEval, maxEvalRight));
		double bminus = Math.min(0, Math.min(minEval, minEvalLeft));
	
		for (int k = 0; k < eigenvalues.rows(); k++){
			res.setElement(k, k, ((bplus + bminus)*eigenvalues.value(k,k) - 2.0*bplus*bminus)/ (bplus - bminus));
		}
		return res;

		
	}
		
}
