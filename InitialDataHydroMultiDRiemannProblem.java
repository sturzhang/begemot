
public class InitialDataHydroMultiDRiemannProblem extends InitialDataHydro {

	double pA, pB, pC, pD;
	double rhoA, rhoB, rhoC, rhoD;
	double uxA, uxB, uxC, uxD;
	double uyA, uyB, uyC, uyD;

	
	public InitialDataHydroMultiDRiemannProblem(Grid grid,
			EquationsHydroIdeal equations) {
		super(grid, equations);
		
		/* 
		 *      A   |   B
		 *      ----------
		 *      D   |   C
		 *      
		 */
		
		
		pB = 0.15;
		pA = 0.15;
		pD = 0.15;
		pC = 0.15;
		
		rhoB = 0.5;
		rhoA = 1.5;
		rhoD = 0.75;
		rhoC = 1.0;
		
		uxB = -0.75;
		uxA = -0.75;
		uxD = 0.75;
		uxC = 0.75;
		
		uyB = -0.5;
		uyA = 0.5;
		uyD = 0.5;
		uyC = -0.5;
		
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		double x, y;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		if (( x <= 0 ) && ( y > 0 )){
			res[EquationsHydroIdeal.INDEX_RHO] = rhoA;
			res[EquationsHydroIdeal.INDEX_XMOM] = rhoA * uxA;
			res[EquationsHydroIdeal.INDEX_YMOM] = rhoA * uyA;
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(
					pA,  
					rhoA * (uxA*uxA + uyA*uyA));	
		}
		if (( x > 0 ) && ( y > 0 )){
			res[EquationsHydroIdeal.INDEX_RHO] = rhoB;
			res[EquationsHydroIdeal.INDEX_XMOM] = rhoB * uxB;
			res[EquationsHydroIdeal.INDEX_YMOM] = rhoB * uyB;
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(
					pB,  
					rhoB * (uxB*uxB + uyB*uyB));	
		}
		if (( x > 0 ) && ( y <= 0 )){
			res[EquationsHydroIdeal.INDEX_RHO] = rhoC;
			res[EquationsHydroIdeal.INDEX_XMOM] = rhoC * uxC;
			res[EquationsHydroIdeal.INDEX_YMOM] = rhoC * uyC;
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(
					pC,  
					rhoC * (uxC*uxC + uyC*uyC));	
		}
		if (( x <= 0 ) && ( y <= 0 )){
			res[EquationsHydroIdeal.INDEX_RHO] = rhoD;
			res[EquationsHydroIdeal.INDEX_XMOM] = rhoD * uxD;
			res[EquationsHydroIdeal.INDEX_YMOM] = rhoD * uyD;
			res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(
					pD,  
					rhoD * (uxD*uxD + uyD*uyD));	
		}
    				
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0; //*/

		return res;
		
		
	}

	@Override
	public String getDetailedDescription() {
		return "4 quadrants having different initial values";
	}

}
