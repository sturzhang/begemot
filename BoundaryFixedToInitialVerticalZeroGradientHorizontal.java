
public class BoundaryFixedToInitialVerticalZeroGradientHorizontal extends
		Boundary {
	
	protected InitialData initial;
	
	public BoundaryFixedToInitialVerticalZeroGradientHorizontal(GridCartesian grid, InitialData initial) {
		super(grid);
		this.initial = initial;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		
		i = g.i(); j = g.j(); k = g.k();

		nextI = i; nextJ = j; nextK = k;
		
		
		if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
			
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }
		
		if ((j < ((GridCartesian) grid).indexMinY()) || (j > ((GridCartesian) grid).indexMaxY())){
			conservedQuantities[i][j][k] = 
					initial.getInitialValue(new GridCell(i, j, k, grid), conservedQuantities[i][j][k].length);
		} else {
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
			}	
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Boundary values on top and bottom fixed to the values there initially, other boundaries made zero gradient.";
	}

}
