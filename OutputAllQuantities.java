
public abstract class OutputAllQuantities extends Output {

	protected int zproportion = -1;
	
	public OutputAllQuantities(double outputTimeInterval, GridCartesian grid, Equations equations) {
		super(outputTimeInterval, grid, equations);
	}

	public OutputAllQuantities(double outputTimeInterval, double zproportion, GridCartesian3D grid, Equations equations) {
		super(outputTimeInterval, grid, equations);
		this.zproportion = (int) Math.round(zproportion*grid.ny);
	}

	protected abstract void printOneCell(double[] quantities, int i, int j, int k);
	protected abstract void printForTestPurpose(double[][][][] quantities);

	public void print(String text){ if (doPrint) { // System.out.print(text);}  }
		printer.print(text); }
	}
	public void println(String text){ if (doPrint) { // System.out.println(text);} } 
		printer.println(text); }
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}
	
	protected void printOutput(double time, double[][][][] quantities){
		double a, realPartEigenvalue;
		int i, j, k;
		double[] horizontalDensityAverage = new double[quantities[0].length];
		double[] horizontalPressureAverage = new double[quantities[0].length];
		double[] horizontalVxAverage = new double[quantities[0].length];
		double[] horizontalVyAverage = new double[quantities[0].length];
		
		int counter;
		
		println("");
		println("");

		if ((grid.dimensions() == 2) && (equations instanceof EquationsHydroIdeal)){
			counter = 0;
			for (GridCell g : grid){
				i = g.i(); j = g.j(); k = g.k();
				horizontalDensityAverage[j] += quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				horizontalPressureAverage[j] += 
						((EquationsHydroIdeal) equations).getPressureFromConservative(quantities[i][j][((GridCartesian) grid).indexMinZ()]);
				horizontalVxAverage[j] += quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_XMOM]/
						quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				horizontalVyAverage[j] += quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_YMOM]/
						quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				
				
				counter++;
			}
			//System.err.println(counter + " " + (grid.nx()*grid.ny()));
			
			for (j = 0; j < horizontalDensityAverage.length; j++){
				horizontalDensityAverage[j] /= ((GridCartesian) grid).nx();
				horizontalPressureAverage[j] /= ((GridCartesian) grid).nx();
				horizontalVyAverage[j] /= ((GridCartesian) grid).nx();
				horizontalVxAverage[j] /= ((GridCartesian) grid).nx();
			}
		}		

		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();//*/
		
		/*GridCell g;
		for (i = 0; i < grid.conservedQuantities.length; i++){
			for (j = 0; j <grid.conservedQuantities[i].length; j++){ // = 1; j++){ //
				for (k = 0; k < grid.conservedQuantities[i][j].length; k++){
				g = new GridCell(i,j,k,grid);//*/

			if ((grid.dimensions() < 3) || 
					( ((zproportion >= 0) && (k == zproportion)) || (zproportion == -1) )){
				println("");
				print(g.toString());
				printOneCell(quantities[i][j][k], i, j, k);

				// passive scalars
				for (int s = equations.getNumberOfConservedQuantities(); s < quantities[i][j][k].length; s++){
					print(quantities[i][j][k][s] + " ");
				}
			}

			// extra printouts
			if ((grid.dimensions() == 1) && (equations instanceof EquationsHydroIdeal)){
				a = -0.5*(
						quantities[i+1][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO] - 
						quantities[i  ][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO]);
				realPartEigenvalue = Math.abs(quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_XMOM])/
						quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO]
								- 1.5*a/quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				print(realPartEigenvalue + " ");
			} else if (grid.dimensions() == 2){
				print(horizontalDensityAverage[g.j()] + " ");
				print(horizontalPressureAverage[g.j()] + " ");
				print(horizontalVxAverage[g.j()] + " ");
				print(horizontalVyAverage[g.j()] + " ");
				
			}

		}
		//	}}
	}				



	protected void printOutputOld(double time, double[][][][] quantities){
		double a, realPartEigenvalue;

		println("");

		if (grid.dimensions() == 1){
			println("");
			//for (int i = 0; i < quantities.length; i++){
			for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
				println("");

				print(grid.getX(i,0,0) + " "); 
				printOneCell(quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()], 
						i, ((GridCartesian) grid).indexMinY(), ((GridCartesian) grid).indexMinZ());

				for (int s = equations.getNumberOfConservedQuantities(); s < quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()].length; s++){
					// passive scalars
					print(quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][s] + " ");
				}

				a = -0.5*(
						quantities[i+1][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO] - 
						quantities[i  ][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO]);
				realPartEigenvalue = Math.abs(quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_XMOM])/
						quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO]
								- 1.5*a/quantities[i][((GridCartesian) grid).indexMinY()][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				print(realPartEigenvalue + " ");

			}
		}
		if (grid.dimensions() == 2){
			println("");

			double horizontalDensityAverage[] = new double[quantities[0].length];
			for (int j = 0; j < quantities[0].length; j++){
				horizontalDensityAverage[j] = 0.0;
				for (int i = 0; i < quantities.length; i++){
					horizontalDensityAverage[j] += quantities[i][j][((GridCartesian) grid).indexMinZ()][EquationsHydroIdeal.INDEX_RHO];
				}
				horizontalDensityAverage[j] /= quantities.length; 
			}

			//printForTestPurpose(quantities);


			for (GridCell g : grid){
				// TODO this can actually replace all loops when the switch for lower dim grids is done in the Iterator automatically
				println("");

				print(g.toString());
				printOneCell(quantities[g.i()][g.j()][g.k()], g.i(), g.j(), g.k());

				for (int s = equations.getNumberOfConservedQuantities(); s < quantities[g.i()][g.j()][g.k()].length; s++){
					// passive scalars
					print(quantities[g.i()][g.j()][g.k()][s] + " ");
				}

				print(horizontalDensityAverage[g.j()] + " ");

			}	
		}
		if (grid.dimensions() == 3){
			println("");



			for (int i = 0; i < quantities.length; i++){
				for (int j = 0; j < quantities[i].length; j++){


					//if ( ((zproportion >= 0) && (j == zproportion)) || (zproportion == -1)){

					for (int k = 0; k < quantities[i][j].length; k++){

						if ( ((zproportion >= 0) && (k == zproportion)) || (zproportion == -1)){


							println("");
							print(grid.getX(i,j,k) + " " + grid.getY(i,j,k) + " " + grid.getZ(i,j,k) + " "); 

							printOneCell(quantities[i][j][k], i, j, k);

							for (int s = equations.getNumberOfConservedQuantities(); s < quantities[i][j][((GridCartesian) grid).indexMinZ()].length; s++){
								// passive scalars
								print(quantities[i][j][k][s] + " ");
							}

						}
					}
				}
			}
		}
	}

}
