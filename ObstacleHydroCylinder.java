public class ObstacleHydroCylinder extends ObstacleHydro {

	protected double radius;
	protected double xCenter, yCenter;
	
	public ObstacleHydroCylinder(Grid grid, EquationsHydroIdeal equations, double radius) {
		super(grid, equations); 
		this.radius = radius;
	}

	public double radius(){
		return radius;
	}
	
	@Override
	public boolean isInside(double[] position){
		double r = Math.sqrt(Math.pow(position[0] - (grid.yMidpoint() - grid.ymin() + grid.xmin()), 2) + Math.pow(position[1] - grid.yMidpoint(), 2));
		return (r <= radius);
	}
	
	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {
		double r;
		
		for (int i = ((GridCartesian) grid).indexMinX(); i < ((GridCartesian) grid).indexMaxX(); i++){
			for (int j = ((GridCartesian) grid).indexMinY(); j < ((GridCartesian) grid).indexMaxY(); j++){
				r = Math.sqrt(Math.pow(grid.getX(i,j,0) - grid.xMidpoint(), 2) + Math.pow(grid.getY(i,j,0) - grid.yMidpoint(), 2));
				if (r < radius){ excludeFromTimeEvolution[i][j][((GridCartesian) grid).indexMinZ()] = true; }
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Impermeable boundary in the form of a circle in the x-y-plane centered on the grid and with radius = " + radius;
	}


}
