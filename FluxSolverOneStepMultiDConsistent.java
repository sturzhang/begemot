import matrices.SquareMatrix;


public class FluxSolverOneStepMultiDConsistent extends FluxSolverOneStepMultiDCentralAndDiffusion {

	public FluxSolverOneStepMultiDConsistent(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop,
			double[] left, double[] leftbottom, double[] righttop,
			double[] right, double[] rightbottom, int direction) {
		
		int quantities = left.length;
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, left, direction);
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, lefttop, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, leftbottom, direction);
		
		double[] fluxRight = equations.fluxFunction(i, j, k, right, direction);
		double[] fluxRightTop = equations.fluxFunction(i, j, k, righttop, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, rightbottom, direction);
		
		if (direction != GridCartesian.X_DIR){
			System.err.println("not x-direction");
		}
		
		int scalarIndex = 0;
		int otherDirection = 0;
		int indexDirection = 0;
		int otherIndexDirection = 0;
		
		double soundspeed = 0.0;
		
		if (direction == GridCartesian.X_DIR){ 
			otherDirection = GridCartesian.Y_DIR; 
		
			if (equations.getClass().getName() == "EquationsIsentropicEuler"){
				indexDirection = EquationsIsentropicEuler.INDEX_XMOM; 
				otherIndexDirection = EquationsIsentropicEuler.INDEX_YMOM; 
				scalarIndex = EquationsIsentropicEuler.INDEX_RHO; 
				soundspeed = 0.5*(
						((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(left) + 
						((EquationsIsentropicEuler) equations).getSoundSpeedFromConservative(right));
				
			} else if (equations.getClass().getName() == "EquationsAcousticSimple"){
				indexDirection = EquationsAcousticSimple.INDEX_VX; 
				otherIndexDirection = EquationsAcousticSimple.INDEX_VY; 
				scalarIndex = EquationsAcousticSimple.INDEX_P;
				soundspeed = ((EquationsAcousticSimple) equations).soundspeed();
			} else {
				System.err.println("Consistent solver not implemented for these equations");
			}
		
		}
		/*else if (direction == GridCartesian.Y_DIR){ 
			otherDirection = GridCartesian.X_DIR; 
			indexDirection = EquationsIsentropicEuler.INDEX_YMOM;
			otherIndexDirection = EquationsIsentropicEuler.INDEX_XMOM; }*/
		else {
			System.err.println("ERROR: Called solver for non-x direction");
		}
		
		//double[] fluxPerpLeft = equations.fluxFunction(i, j, k, left, otherDirection);
		double[] fluxPerpLeftTop = equations.fluxFunction(i, j, k, lefttop, otherDirection);
		double[] fluxPerpLeftBottom = equations.fluxFunction(i, j, k, leftbottom, otherDirection);
		
		//double[] fluxPerpRight = equations.fluxFunction(i, j, k, right, otherDirection);
		double[] fluxPerpRightTop = equations.fluxFunction(i, j, k, righttop, otherDirection);
		double[] fluxPerpRightBottom = equations.fluxFunction(i, j, k, rightbottom, otherDirection);
		
		double[] res = new double[quantities];
		
		int q = indexDirection;
		res[scalarIndex] = (fluxRightTop[q] - fluxLeftTop[q] + 2.0*(fluxRight[q] - fluxLeft[q]) + fluxRightBottom[q] - fluxLeftBottom[q]);
		// 1-dres[scalarIndex] = 4.0*(fluxRight[q] - fluxLeft[q]);
		
		//q = otherIndexDirection;
		res[scalarIndex] += (fluxPerpRightTop[q] - fluxPerpRightBottom[q] + fluxPerpLeftTop[q] - fluxPerpLeftBottom[q]);
		
		q = scalarIndex;
		res[indexDirection]      = (fluxRightTop[q] - fluxLeftTop[q] + 2.0*(fluxRight[q] - fluxLeft[q]) + fluxRightBottom[q] - fluxLeftBottom[q]);
		// 1-dres[indexDirection]      = 4.0*(fluxRight[q] - fluxLeft[q]);
		res[indexDirection]     += (fluxPerpRightTop[q] - fluxPerpRightBottom[q] + fluxPerpLeftTop[q] - fluxPerpLeftBottom[q]);
		
		// stability
		SquareMatrix[] diag = equations.diagonalization(i, j, k, left, direction);
		SquareMatrix eval = diag[1];		
		SquareMatrix signeval = new SquareMatrix(eval.columns());
		
		double largestEigenvalue = Math.abs(eval.value(0, 0));
		for (int ii = 1; ii < eval.rows(); ii++){
			largestEigenvalue = Math.max(largestEigenvalue, Math.abs(eval.value(ii, ii)));
		}
		eval = equations.diagonalization(i, j, k, right, direction)[1];		
		for (int ii = 0; ii < eval.rows(); ii++){
			largestEigenvalue = Math.max(largestEigenvalue, Math.abs(eval.value(ii, ii)));
		}
		
		if (equations.getClass().getName() == "EquationsIsentropicEuler"){
			
			/*
			// CFL (worse if dx smaller!) unstable for linearized regime of isentropic with supersonic velocities 
			double prefactor = 0.125*largestEigenvalue;
			res[scalarIndex] *= prefactor/soundspeed/soundspeed;  // d rho
			res[indexDirection] *= prefactor; // d rho u //*/
			//res[EquationsIsentropicEuler.INDEX_YMOM] *= prefactor; // d rho v
			
			/*double prefactor = 0.125;
			res[scalarIndex] *= prefactor/(2*soundspeed - largestEigenvalue);  // d rho
			res[indexDirection] *= prefactor*largestEigenvalue; // d rho u*/
			
			// CFL (worse if dx smaller!) unstable for linearized regime of isentropic with supersonic velocities 
			double prefactor = 0.125;
			res[scalarIndex] *= prefactor/soundspeed;  // d rho
			res[indexDirection] *= prefactor*soundspeed; // d rho u //*/
			
			/*double rhoL =  left[EquationsIsentropicEuler.INDEX_RHO];
			double rhoR = right[EquationsIsentropicEuler.INDEX_RHO];
			double pressureL = ((EquationsIsentropicEuler) equations).getPressure(rhoL); 
			double pressureR = ((EquationsIsentropicEuler) equations).getPressure(rhoR); 
			double vLeft = left[EquationsIsentropicEuler.INDEX_XMOM]/rhoL;
			double soundspeedNonlin;
			if (rhoR == rhoL){
				soundspeedNonlin = soundspeed;
			} else {
				soundspeedNonlin = Math.abs(vLeft - Math.sqrt( (pressureR - pressureL) * rhoR / rhoL / (rhoR - rhoL) ));
			}
			res[scalarIndex] *= prefactor/soundspeedNonlin;  // d rho
			res[indexDirection] *= prefactor*soundspeedNonlin; // d rho u //*/
			
		} else if (equations.getClass().getName() == "EquationsAcousticSimple"){
			double prefactor = 0.125;
			
			if (((EquationsAcousticSimple) equations).symmetric()){
				System.err.println("ERROR: Consistent solver for acoustic equations only implemented for non-symmetric acoustics");
			}
			
			res[scalarIndex] *= prefactor*soundspeed;
			res[indexDirection] *= prefactor/soundspeed;
		}
			
		return res;
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left, double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoe usualSolver = new FluxSolverRoe((GridCartesian) grid, equations);
		return usualSolver.averageFlux(i, j, k, left, right, direction);	
	}

	@Override
	public String getDetailedDescription() {
		return "consistent diffusion -- first try";
	}

}
