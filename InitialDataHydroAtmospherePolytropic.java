
public class InitialDataHydroAtmospherePolytropic extends InitialDataHydroAtmosphere {

	protected double Gamma;
	protected double K, rhoAtBottom;
	
	public InitialDataHydroAtmospherePolytropic(GridCartesian grid, EquationsHydroIdeal equations, 
			double rhoAtBottom, double pressureAtBottom, double gy, double xVelocity,
			double Gamma) {
		super(grid, equations, gy, xVelocity, false);
		this.Gamma = Gamma;
		this.K = pressureAtBottom / Math.pow(rhoAtBottom, Gamma);
		this.rhoAtBottom = rhoAtBottom;
	}
	
	@Override
	public double getRho(double y) {
		return Math.pow(Math.pow(rhoAtBottom, Gamma-1) + gy*y * (Gamma-1)/K/Gamma, 1.0/(Gamma-1));
	}

	@Override
	public double getPressure(double y) {
		return K*Math.pow(getRho(y), Gamma);
	}

	@Override
	public double getRhoDeriv(double y) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getPressureDeriv(double y) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getDetailedDescription() {
		return "Polytropic atmosphere with index Gamma = " + Gamma;
	}

}
