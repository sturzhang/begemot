
public class FluxSolverMultiStepCartesianSecondOrderLinearized2DAverage extends
		FluxSolverMultiStepCartesianSecondOrderLinearized {

	public FluxSolverMultiStepCartesianSecondOrderLinearized2DAverage(
			GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected double[][] getInterfaceValues(GridEdgeMapped gridEdge, 
			double[] leftValue, double[] leftleft, double[] rightValue, double[] rightright) {
		// TODO ATTENTION here we have just removed things!
		// this has to be implemented properly in the future
		

		//double[] outwardNormalVector = grid.getAllEdges(g.i(), g.j(), g.k())[edgeIndex].normalVector(); 
		//int edgeDir = ((GridCartesian) grid).getDirectionPerpendicularToEdge(g, edgeIndex);		
		//int offsetX = 0, offsetY = 0, offsetZ = 0;
		
		//if (edgeDir == GridCartesian.X_DIR){ offsetX = 1; }
		//if (edgeDir == GridCartesian.Y_DIR){ offsetY = 1; }
		//if (edgeDir == GridCartesian.Z_DIR){ offsetZ = 1; }

		if (grid.dimensions() != 2){
			System.err.println("ERROR: Usage of solver restricted to 2-dimensional grids only!");
		}
		
		//int offsetPerpX = 1 - offsetX;
		//int offsetPerpY = 1 - offsetY;
			
		
		/*double[] leftleft = equations.getTransform(new GridCell(
				left.i()-offsetX, left.j()-offsetY, left.k()-offsetZ, grid).interfaceValue(), outwardNormalVector);
		double[] leftlefttop = equations.getTransform(new GridCell(
				left.i()-offsetX+offsetPerpX, left.j()-offsetY+offsetPerpY, left.k()-offsetZ, grid).interfaceValue(), outwardNormalVector);
		double[] leftleftbottom = equations.getTransform(new GridCell(
				left.i()-offsetX-offsetPerpX, left.j()-offsetY-offsetPerpY, left.k()-offsetZ, grid).interfaceValue(), outwardNormalVector);
		
		double[] leftValueTop = equations.getTransform(new GridCell(
				left.i()+offsetPerpX, left.j()+offsetPerpY, left.k(), grid).interfaceValue(), outwardNormalVector);
		double[] leftValueBottom = equations.getTransform(new GridCell(
				left.i()-offsetPerpX, left.j()-offsetPerpY, left.k(), grid).interfaceValue(), outwardNormalVector);
		
		
		double[] rightright = equations.getTransform(new GridCell(
				right.i()+offsetX, right.j()+offsetY, right.k()+offsetZ, grid).interfaceValue(), outwardNormalVector);
		double[] rightrighttop = equations.getTransform(new GridCell(
				right.i()+offsetX+offsetPerpX, right.j()+offsetY+offsetPerpY, right.k()+offsetZ, grid).interfaceValue(), outwardNormalVector);
		double[] rightrightbottom = equations.getTransform(new GridCell(
				right.i()+offsetX-offsetPerpX, right.j()+offsetY-offsetPerpY, right.k()+offsetZ, grid).interfaceValue(), outwardNormalVector);
		
		double[] rightValueTop = equations.getTransform(new GridCell(
				right.i()+offsetPerpX, right.j()+offsetPerpY, right.k(), grid).interfaceValue(), outwardNormalVector);
		double[] rightValueBottom = equations.getTransform(new GridCell(
				right.i()-offsetPerpX, right.j()-offsetPerpY, right.k(), grid).interfaceValue(), outwardNormalVector);
		*/
		
		// --- quick fix--remove after proper implementation
		double[] leftlefttop = new double[leftValue.length], leftValueTop = new double[leftValue.length], rightValueTop = new double[leftValue.length], rightrighttop = new double[leftValue.length];
		double[] leftleftbottom = new double[leftValue.length], leftValueBottom = new double[leftValue.length], rightValueBottom = new double[leftValue.length], rightrightbottom = new double[leftValue.length];
		for (int q = 0; q < leftValue.length; q++){
			leftlefttop[q] = leftleft[q];     leftleftbottom[q] = leftleft[q];
			leftValueTop[q] = leftValue[q];   leftValueBottom[q] = leftValue[q];
			rightValueTop[q] = rightValue[q]; rightValueBottom[q] = rightValue[q];
			rightrighttop[q] = rightright[q]; rightrightbottom[q] = rightright[q];
		}
		// --- end of quick fix
		
		
		
		
		double[][] res = new double[3][leftValue.length];
		for (int q = 0; q < leftValue.length; q++){
			res[0][q]  = average(leftlefttop[q],   leftValueTop[q],  leftleft[q],   leftValue[q],  leftleftbottom[q],   leftValueBottom[q]);
			res[1][q]  = average(leftValueTop[q],  rightValueTop[q], leftValue[q],  rightValue[q], leftValueBottom[q],  rightValueBottom[q]);
			res[2][q]  = average(rightValueTop[q], rightrighttop[q], rightValue[q], rightright[q], rightValueBottom[q], rightrightbottom[q]);
		}
		return res;
		
		
	}

	protected double average(double lefttop, double righttop,
			double left, double right,
			double leftbottom, double rightbottom){
		return 0.125*(lefttop + righttop + leftbottom + rightbottom) + 0.25*(left + right);
	}
}
