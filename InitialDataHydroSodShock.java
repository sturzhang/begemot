public class InitialDataHydroSodShock extends InitialDataHydroShocktube {

	public InitialDataHydroSodShock(Grid grid, EquationsHydroIdeal equations){ super(grid, equations); }


	@Override
	public double leftPressure() { return 1.0; }
	@Override
	public double leftDensity() {  return 1.0; }
	@Override
	public double leftVx() {       return 0; // + 0.2; 
	}
	

	@Override
	public double rightPressure() { return 0.1; }
	@Override
	public double rightDensity() {  return 0.125; }
	@Override
	public double rightVx() {       return 0; // + 0.2; 
	}


}