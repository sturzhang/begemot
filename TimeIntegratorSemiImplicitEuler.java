
public class TimeIntegratorSemiImplicitEuler extends TimeIntegratorExplicit {

	private EquationsHydroIdeal equations;
	
	public TimeIntegratorSemiImplicitEuler(GridCartesian grid, double CFL, EquationsHydroIdeal equations) {
		super(grid, CFL);
		this.equations = equations;
	}

	@Override
    public String getDetailedDescription(){
    	return "Semi-implicit time integrator for the Euler equations with CFL = " + CFL;
    }

	
	// todo make a superclass of semiimplicit integrators!
	
	@Override
	public int steps() {
		return 2;
	}

	@Override
	public double[][][][] perform(int step, double dt, 
			double[][][][][] interfaceFluxes, double[][][][] sources, double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {
		int[] quantities;
		int i, j, k;
		double cellSize;

		if (step == 1){
			quantities = new int[] {EquationsHydroIdeal.INDEX_XMOM, EquationsHydroIdeal.INDEX_YMOM, EquationsHydroIdeal.INDEX_ZMOM};
		} else {
			quantities = new int[] {EquationsHydroIdeal.INDEX_RHO, EquationsHydroIdeal.INDEX_ENERGY};
		}
		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			cellSize = g.size();
						
			if (!excludeFromTimeIntegration[i][j][k]){
				for (int dir = 0; dir < grid.summedInterfaceFluxes[i][j][k].length; dir++){
					for (int q = 0; q < quantities.length; q++){	
						//g.addToConservedQuantity(- dt / cellSize * grid.summedInterfaceFluxes[i][j][k][dir][quantities[q]]
						//		+ dt * sources[i][j][k][quantities[q]] , quantities[q]);
						conservedQuantities[i][j][k][quantities[q]] += -dt / cellSize * grid.summedInterfaceFluxes[i][j][k][dir][quantities[q]]
								+ dt * sources[i][j][k][quantities[q]];
			
					}

				}
			}
			
			
		}
		
		return conservedQuantities;
	}

}
