
public class InitialDataIsentropicSimpleShock extends InitialDataIsentropic {

	private double speed, leftRho, leftV;
	
	public InitialDataIsentropicSimpleShock(Grid grid, EquationsIsentropicEuler equations,
			double speed, double leftRho, double leftV) {
		super(grid, equations);
		this.speed = speed;
		this.leftRho = leftRho;
		this.leftV = leftV;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x = g.getX() - grid.xMidpoint();
    	double rightRho, rightV;
    	
    	double newV = leftV - speed;
    	rightRho = getRhoStationaryShock(leftRho, newV);
    	rightV = leftRho*newV / rightRho + speed;
    	
    	System.err.println("(" + leftRho + ", " + leftV + ") - (" + rightRho + ", " + rightV + ")");
    	
    	/*leftRho = 0.378;
    	leftV = 0.844;
    	double rightRho = 0.125;
    	double rightV = -0.2;
    	// speed = 0.75
    	//*/
    	
    	/*leftRho = 0.378;
    	leftV = 0.844-1.33;
    	rightRho = 0.125;
    	rightV = -1.53;
    	// speed = 0.0
    	//*/
    	
    	
    	if (x < 0){
    		res[EquationsIsentropicEuler.INDEX_RHO] = leftRho;
    		res[EquationsIsentropicEuler.INDEX_XMOM] = leftV*res[EquationsIsentropicEuler.INDEX_RHO];
        } else {
    		res[EquationsIsentropicEuler.INDEX_RHO] = rightRho;
    		res[EquationsIsentropicEuler.INDEX_XMOM] = rightV*res[EquationsIsentropicEuler.INDEX_RHO];
        }
    				
    	res[EquationsIsentropicEuler.INDEX_YMOM] = 0.0;
    	res[EquationsIsentropicEuler.INDEX_ZMOM] = 0.0;
    			
    		
    	return res;
	}

	private double getRhoStationaryShock(double rhoL, double vL){
		double fluxLeft = rhoL * vL*vL + equations.getPressure(rhoL);
		boolean verbose = true;
		
		double guessRhoR = 0.125; // Math.pow(rhoL*vL*vL/equations.K(), 1.0/equations.gamma());
		
		for (int i = 0; i < 10; i++){
			if (verbose){ System.err.println(guessRhoR); }
			guessRhoR = Newton(guessRhoR, fluxLeft, rhoL, vL);
		}
		return guessRhoR;
	}
	
	private double Newton(double rhoR, double fluxL, double rhoL, double vL){
		double func = rhoL*rhoL*vL*vL/rhoR + equations.getPressure(rhoR) - fluxL;
		double deriv = -rhoL*rhoL*vL*vL/rhoR/rhoR + equations.gamma()*equations.getPressure(rhoR)/rhoR;
		
		return rhoR - func/deriv;
	}
	
	
	@Override
	public String getDetailedDescription() {
		return "Computes a shock moving at speed " + speed + " with the left state being (rho, v) = (" + leftRho + ", " + leftV + ")";
	}

}
