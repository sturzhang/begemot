public class FluxSolverOneStepMultiDAcousticBichar extends FluxSolverOneStepMultiD {

	public FluxSolverOneStepMultiDAcousticBichar(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}
	

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, 
			double[] ip1jp1k, double[] ip1jk, double[] ip1jm1k,
			double[] ijp1kp1, double[] ijkp1, double[] ijm1kp1, 
			double[] ip1jp1kp1, double[] ip1jkp1, double[] ip1jm1kp1,
			double[] ijp1km1, double[] ijkm1, double[] ijm1km1, 
			double[] ip1jp1km1, double[] ip1jkm1, double[] ip1jm1km1, int timeIntegratorStep) {
		
		if (grid instanceof GridCartesian3D){ System.err.println(this.getClass().getCanonicalName() + " not implemented for 3d grid"); }
		
		return  new FluxAndWaveSpeed(
				multidflux(ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k),
				((EquationsAcousticSimple) equations).soundspeed() + Math.max(
						Math.max( Math.abs(((EquationsAcousticSimple) equations).vx()), 
								Math.abs(((EquationsAcousticSimple) equations).vy()) ),
						Math.abs(((EquationsAcousticSimple) equations).vz())
						));
	}

	protected double[] multidflux(double[] LT, double[] L, double[] LB, double[] RT, double[] R, double[] RB){
		int quantities = L.length;
		double[] fluxRes = new double[quantities];
		double diffX[] = new double[quantities];
		double mixeddiffX[] = new double[quantities];
		double sumX[] = new double[quantities];
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		
		for (int q = 0; q < quantities; q++){ 
			diffX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] - (0.25*LT[q] + 0.5*L[q] + 0.25*LB[q]);
			//diffX[q] = R[q] - L[q];
			sumX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] + 0.25*LT[q] + 0.5*L[q] + 0.25*LB[q];
			//sumX[q] = R[q] + L[q];
			mixeddiffX[q] = (LT[q] + RT[q] - (LB[q] + RB[q]));
		}
		
		
		if (((EquationsAcousticSimple) equations).symmetric()){
			fluxRes[EquationsAcousticSimple.INDEX_VX] = 0.5*c*sumX[EquationsAcousticSimple.INDEX_P];
			fluxRes[EquationsAcousticSimple.INDEX_VY] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_VZ] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_P ] = 0.5*c*sumX[EquationsAcousticSimple.INDEX_VX];
		} else {
			fluxRes[EquationsAcousticSimple.INDEX_VX] = 0.5*sumX[EquationsAcousticSimple.INDEX_P];
			fluxRes[EquationsAcousticSimple.INDEX_VY] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_VZ] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_P ] = 0.5*c*c*sumX[EquationsAcousticSimple.INDEX_VX];
		}
			
		fluxRes[EquationsAcousticSimple.INDEX_VX] -= 0.5*c*diffX[EquationsAcousticSimple.INDEX_VX];
		fluxRes[EquationsAcousticSimple.INDEX_P ] -= 0.5*c*diffX[EquationsAcousticSimple.INDEX_P];
				
		//if (((EquationsAcousticSimple) equations).symmetric()){
		//	System.out.println("Error: FluxSolverBichar not implemented for symmetric acoustic system!");
		//} else {
			fluxRes[EquationsAcousticSimple.INDEX_VX] -= 0.125*c*mixeddiffX[EquationsAcousticSimple.INDEX_VY];
		//}	
		
		return fluxRes;
	}
	
	
	@Override
	public String getDetailedDescription() {
		return "Roe' multi-d solver implemented in a way to avoid the vertex fluxes";
	}





}
