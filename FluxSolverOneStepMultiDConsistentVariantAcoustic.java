
public class FluxSolverOneStepMultiDConsistentVariantAcoustic extends
		FluxSolverOneStepMultiDCentralAndDiffusion {

	public FluxSolverOneStepMultiDConsistentVariantAcoustic(GridCartesian grid,
			Equations equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected double[] getUpwindingTerm(int i, int j, int k, double[] lefttop,
			double[] left, double[] leftbottom, double[] righttop,
			double[] right, double[] rightbottom, int direction) {
		
		int quantities = left.length;
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, left, direction);
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, lefttop, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, leftbottom, direction);
		
		double[] fluxRight = equations.fluxFunction(i, j, k, right, direction);
		double[] fluxRightTop = equations.fluxFunction(i, j, k, righttop, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, rightbottom, direction);
		
		int indexDirection = EquationsAcousticSimple.INDEX_VX; 
		int otherIndexDirection = EquationsAcousticSimple.INDEX_VY;
		
		double c = ((EquationsAcoustic) equations).soundspeed();
		double[] res = new double[quantities];
		
		// first term (Sx)
		int q = indexDirection;
		res[EquationsAcousticSimple.INDEX_P] = 
				c*(fluxRightTop[q] - fluxLeftTop[q] + 2.0*(fluxRight[q] - fluxLeft[q]) + fluxRightBottom[q] - fluxLeftBottom[q])/8;
		q = EquationsAcousticSimple.INDEX_P;
		res[indexDirection] = 
				(fluxRightTop[q] - fluxLeftTop[q] + 2.0*(fluxRight[q] - fluxLeft[q]) + fluxRightBottom[q] - fluxLeftBottom[q])/8;
		
		// second term (Sy)
		q = otherIndexDirection;
		res[EquationsAcousticSimple.INDEX_P] += 
				c*(fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q])/8;
		q = EquationsAcousticSimple.INDEX_P;
		res[otherIndexDirection] += 
				(fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q])/8;
			
				
		// third term ( {Sx, Sy}/2 )
		q = otherIndexDirection;
		res[indexDirection] += 
				(fluxRightTop[q] - fluxRightBottom[q] - fluxLeftTop[q] + fluxLeftBottom[q])/8;
		q = indexDirection;
		res[otherIndexDirection] += 
				(fluxRightTop[q] - fluxRightBottom[q] - fluxLeftTop[q] + fluxLeftBottom[q])/8;
		
			
		return res;
	}

	@Override
	protected double[] averageFlux(int i, int j, int k, double[] left,
			double[] right, int direction) {
		// multiple inheritance -- workaround
		FluxSolverRoe usualSolver = new FluxSolverRoe((GridCartesian) grid, equations);
		return usualSolver.averageFlux(i, j, k, left, right, direction);	

	}

	@Override
	public String getDetailedDescription() {
		return "consistent diffusion for acoustic equations; variant inspired by linear advection scheme";
	}

}
