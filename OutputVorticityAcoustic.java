
public class OutputVorticityAcoustic extends Output {

	
	protected InitialData initial;
	
	
	public OutputVorticityAcoustic(double outputTimeInterval, Grid grid,
			EquationsAcousticSimple equations, InitialData initial) {
		super(outputTimeInterval, grid, equations);
		this.initial = initial;
	}

	@Override
	public String getDetailedDescription() {
		return "prints the central-averaged vorticity";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		int i,j,k;
		double dxv, dyu, dxvinit, dyuinit;
		double err = 0;
		int counter = 0;
		
		double[][][][] init = new double[quantities.length][quantities[0].length][quantities[0][0].length][quantities[0][0][0].length];
		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			init[i][j][k] = initial.getInitialValue(new GridCell(i, j, k, grid), quantities.length);
		}
		
		/*println("");
		println("");//*/

		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			dyu = getdyucentralaveraged(quantities[i+1][j+1][k], quantities[i+1][j][k], quantities[i+1][j-1][k],
					quantities[i][j+1][k], quantities[i][j][k], quantities[i][j-1][k],
					quantities[i-1][j+1][k], quantities[i-1][j][k], quantities[i-1][j-1][k]);
			dxv = getdxvcentralaveraged(quantities[i+1][j+1][k], quantities[i+1][j][k], quantities[i+1][j-1][k],
					quantities[i][j+1][k], quantities[i][j][k], quantities[i][j-1][k],
					quantities[i-1][j+1][k], quantities[i-1][j][k], quantities[i-1][j-1][k]);
			
			/*dyu = 0.25*(quantities[i+1][j+1][k][EquationsAcousticSimple.INDEX_VX]
				  -     quantities[i+1][j-1][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2 + 
				  0.5*(quantities[i][j+1][k][EquationsAcousticSimple.INDEX_VX]
				  -    quantities[i][j-1][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2 + 
				  0.25*(quantities[i-1][j+1][k][EquationsAcousticSimple.INDEX_VX]
				  -     quantities[i-1][j-1][k][EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2;
			
			
			dxv = 0.25*(quantities[i+1][j+1][k][EquationsAcousticSimple.INDEX_VY]
			      -     quantities[i-1][j+1][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2 + 
			      0.5*(quantities[i+1][j][k][EquationsAcousticSimple.INDEX_VY]
			      -    quantities[i-1][j][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2 + 
				  0.25*(quantities[i+1][j-1][k][EquationsAcousticSimple.INDEX_VY]
				  -     quantities[i-1][j-1][k][EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2;//*/
			
			
			dyuinit = getdyucentralaveraged(init[i+1][j+1][k], init[i+1][j][k], init[i+1][j-1][k],
					init[i][j+1][k], init[i][j][k], init[i][j-1][k],
					init[i-1][j+1][k], init[i-1][j][k], init[i-1][j-1][k]);
			dxvinit = getdxvcentralaveraged(init[i+1][j+1][k], init[i+1][j][k], init[i+1][j-1][k],
					init[i][j+1][k], init[i][j][k], init[i][j-1][k],
					init[i-1][j+1][k], init[i-1][j][k], init[i-1][j-1][k]); //*/
			
			//println(g.getX() + " " + g.getY() + " " + (dxv - dyu) + " " + (dxvinit - dyuinit) + " " + ((dxv - dyu) - (dxvinit - dyuinit)));
			
			err += Math.abs((dxv - dyu) - (dxvinit - dyuinit));
			counter++;
			
			/*if ((i == 61) && (j == 102)){
				err += Math.abs((dxv - dyu) - (dxvinit - dyuinit));
				counter++;
			}//*/
		}

		err /= counter;
		println(time + " " + err); //*/
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}
	
	protected double getdyucentralaveraged(double[] quantitiesip1jp1, double[] quantitiesip1j, double[] quantitiesip1jm1,
			double[] quantitiesijp1, double[] quantitiesij, double[] quantitiesijm1,
			double[] quantitiesim1jp1, double[] quantitiesim1j, double[] quantitiesim1jm1){
		return 
				0.25*(quantitiesip1jp1[EquationsAcousticSimple.INDEX_VX]
						  -     quantitiesip1jm1[EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2 + 
						  0.5*(quantitiesijp1[EquationsAcousticSimple.INDEX_VX]
						  -    quantitiesijm1[EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2 + 
						  0.25*(quantitiesim1jp1[EquationsAcousticSimple.INDEX_VX]
						  -     quantitiesim1jm1[EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2;
	
	}
	
	protected double getdxvcentralaveraged(double[] quantitiesip1jp1, double[] quantitiesip1j, double[] quantitiesip1jm1,
			double[] quantitiesijp1, double[] quantitiesij, double[] quantitiesijm1,
			double[] quantitiesim1jp1, double[] quantitiesim1j, double[] quantitiesim1jm1){
		return
				0.25*(quantitiesip1jp1[EquationsAcousticSimple.INDEX_VY]
					      -     quantitiesim1jp1[EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2 + 
					      0.5*(quantitiesip1j[EquationsAcousticSimple.INDEX_VY]
					      -    quantitiesim1j[EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2 + 
						  0.25*(quantitiesip1jm1[EquationsAcousticSimple.INDEX_VY]
						  -     quantitiesim1jm1[EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2;
		
	}
	
	protected double getdyucentral(double[] quantitiesip1jp1, double[] quantitiesip1j, double[] quantitiesip1jm1,
			double[] quantitiesijp1, double[] quantitiesij, double[] quantitiesijm1,
			double[] quantitiesim1jp1, double[] quantitiesim1j, double[] quantitiesim1jm1){
		return 
				(quantitiesijp1[EquationsAcousticSimple.INDEX_VX] - 
						quantitiesijm1[EquationsAcousticSimple.INDEX_VX])/((GridCartesian) grid).ySpacing()/2;
	
	}
	
	protected double getdxvcentral(double[] quantitiesip1jp1, double[] quantitiesip1j, double[] quantitiesip1jm1,
			double[] quantitiesijp1, double[] quantitiesij, double[] quantitiesijm1,
			double[] quantitiesim1jp1, double[] quantitiesim1j, double[] quantitiesim1jm1){
		return
				(quantitiesip1j[EquationsAcousticSimple.INDEX_VY]
					      -    quantitiesim1j[EquationsAcousticSimple.INDEX_VY])/((GridCartesian) grid).xSpacing()/2;
		
	}
}
