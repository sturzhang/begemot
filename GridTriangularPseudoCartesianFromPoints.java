import java.util.LinkedList;
import java.util.Random;


public class GridTriangularPseudoCartesianFromPoints extends GridFromPoints {

	protected CornerPoint[][][] pointsLattice;
    protected int SW = 0; 
    protected int NW = 1; 
    protected int SE = 2; 
    protected int NE = 3;
	
    protected GridCartesian2D myCartesianGrid;

    
	public GridTriangularPseudoCartesianFromPoints(double xmin, double xmax, int nx, double ymin, double ymax, int ny, int ghostcells) {
    	super(xmin, xmax, ymin, ymax, ghostcells);
		
    	myCartesianGrid = new GridCartesian2D(xmin, xmax, nx, ymin, ymax, ny, ghostcells);
		myCartesianGrid.initialize(myObstacle);
		
		pointsLattice = new CornerPoint[myCartesianGrid.indexMaxXIncludingGhostcells()+1][myCartesianGrid.indexMaxYIncludingGhostcells()+1][4];
	}

	protected int edgesPerCell(){ return 3; }
	
    protected void savePoints(){
    	double dx = myCartesianGrid.xSpacing(), dy = myCartesianGrid.ySpacing();
		
    	for (int i = 0; i <= myCartesianGrid.indexMaxXIncludingGhostcells(); i++){
    		for (int j = 0; j <= myCartesianGrid.indexMaxYIncludingGhostcells(); j++){  
    			savePoint(i, j, SW, dx, dy);  
		
    			if (i == myCartesianGrid.indexMaxXIncludingGhostcells()){  savePoint(i, j, SE, dx, dy);  }
			
    			if (j == myCartesianGrid.indexMaxYIncludingGhostcells()){
    				savePoint(i, j, NW, dx, dy);
    				if (i == myCartesianGrid.indexMaxXIncludingGhostcells()){ savePoint(i, j, NE, dx, dy); }
    			}
			
    		}
    	}
    }
    
    protected void saveTriangle(int i, int j, int dir1, int dir2, int dir3){
    	ListOfCornerPoints tmpList = new ListOfCornerPoints();
		tmpList.add(pointsLattice[i][j][dir1]);
		tmpList.add(pointsLattice[i][j][dir2]);
		tmpList.add(pointsLattice[i][j][dir3]);
		
		createGridCell(tmpList, myCartesianGrid.isValidCell(i, j, 0));
    }
		

            
    @Override
	protected void divideIntoCells(){	
		Random random = new Random();
		int counter, counter2;
		int[] insideObstacle = new int[4];
		int[] outsideObstacle = new int[4];
		LinkedList<CornerPointPair> pointsAtObstacleBoundingEdge = new LinkedList<CornerPointPair>();
		
		for (int i = 0; i <= myCartesianGrid.indexMaxXIncludingGhostcells(); i++){
			for (int j = 0; j <= myCartesianGrid.indexMaxYIncludingGhostcells(); j++){
				counter = 0; counter2 = 0;
				for (int dir = 0; dir <= 3; dir++){
					if (myObstacle.isInside(pointsLattice[i][j][dir].position())){
						insideObstacle[counter] = dir; counter++;
					} else {
						outsideObstacle[counter2] = dir; counter2++;
					}
				}

				
				if (counter == 4){
					// do nothing
				} else if (counter == 3){
					
				} else if (counter == 2){
					// save edge between outsideObstacle-points to obstacle:
					// PROBLEM: here the edge to be saved actually might not yet exist!!!
					// SOLUTION: save points first, and when all the edges are computed add the edge

					pointsAtObstacleBoundingEdge.add(new CornerPointPair(
							pointsLattice[i][j][outsideObstacle[0]], pointsLattice[i][j][outsideObstacle[1]]));
				} else if (counter == 1){
					saveTriangle(i, j, outsideObstacle[0], outsideObstacle[1], outsideObstacle[2]);
					// save diagonal edge to obstacle:
					if ((insideObstacle[0] == NW) || (insideObstacle[0] == SE)){
						// positive slope
						for (GeometricGridEdge e : pointsLattice[i][j][SW].adjacentEdges()){
							if (e.adjoins(pointsLattice[i][j][NE])){ myObstacle.addBoundingEdge(e); }
						}
					} else {
						// negative slope
						for (GeometricGridEdge e : pointsLattice[i][j][SE].adjacentEdges()){
							if (e.adjoins(pointsLattice[i][j][NW])){ myObstacle.addBoundingEdge(e); }
						}
					}
				} else {
					if (random.nextDouble() < 0.5){
						saveTriangle(i, j, NW, NE, SW);
						saveTriangle(i, j, NE, SE, SW);
					} else {
						saveTriangle(i, j, NW, SE, SW);
						saveTriangle(i, j, NW, NE, SE);
					}
				}
			}
		}
		
		for (CornerPointPair pair : pointsAtObstacleBoundingEdge){
			for (GeometricGridEdge e : pair.point1().adjacentEdges()){
				if (e.adjoins(pair.point2())){ myObstacle.addBoundingEdge(e); }
			}
		}
	}
		
	@Override
	public String getDetailedDescription() {
		// TODO continue here!
		return "pseudo-cartesian grid with a general setup just as: \n " + 
				myCartesianGrid.getDetailedDescription();
	}

	

	
	
    protected void savePoint(int i, int j, int direction, double dx, double dy){
		CornerPoint point;
		int offsetX = 0, offsetY = 0; // to make compiler happy
		int newDirection = 0; // to make compiler happy
		
		if (direction == SW){ offsetX = -1; offsetY = -1; }
		if (direction == NW){ offsetX = -1; offsetY =  1; }
		if (direction == SE){ offsetX =  1; offsetY = -1; }
		if (direction == NE){ offsetX =  1; offsetY =  1; }
		
		
		point = new CornerPoint(new double[] { myCartesianGrid.getX(i, j, 0)+dx*offsetX/2, 
                myCartesianGrid.getY(i, j, 0)+dy*offsetY/2, 0 });
		
		points.add(point);
		pointsLattice[i][j][direction] = point;
		
		if (direction == SW){ newDirection = SE; }
		if (direction == NW){ newDirection = NE; }
		if (direction == SE){ newDirection = SW; }
		if (direction == NE){ newDirection = NW; }
		if ((i+offsetX >= 0) && (i+offsetX <= myCartesianGrid.indexMaxXIncludingGhostcells())){
			pointsLattice[i+offsetX][j        ][newDirection] = point;
		}
			
		if (direction == SW){ newDirection = NW; }
		if (direction == NW){ newDirection = SW; }
		if (direction == SE){ newDirection = NE; }
		if (direction == NE){ newDirection = SE; }
		if ((j+offsetY >= 0) && (j+offsetY <= myCartesianGrid.indexMaxYIncludingGhostcells())){
			pointsLattice[i        ][j+offsetY][newDirection] = point;
		}
			
		if (direction == SW){ newDirection = NE; }
		if (direction == NW){ newDirection = SE; }
		if (direction == SE){ newDirection = NW; }
		if (direction == NE){ newDirection = SW; }
		if ((i+offsetX >= 0) && (i+offsetX <= myCartesianGrid.indexMaxXIncludingGhostcells()) &&
				(j+offsetY >= 0) && (j+offsetY <= myCartesianGrid.indexMaxYIncludingGhostcells())){
			pointsLattice[i+offsetX][j+offsetY][newDirection] = point;
		}
		
	}
	
}
