
public class BoundaryReflective extends Boundary {

	protected EquationsHydroIdeal equations;
	
	public BoundaryReflective(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid);
		this.equations = equations;
	}
	
    public String getDetailedDescription(){
    	return "Reflective";
    }
    
    @Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		boolean switchX, switchY, switchZ;
		
		i = g.i(); j = g.j(); k = g.k();
		
		switchX = false; switchY = false; switchZ = false;
		nextI = i; nextJ = j; nextK = k;
			
		if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); switchX = true; }
		if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); switchY = true; }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); switchZ = true; }
			
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); switchX = true; }
		if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); switchY = true; }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); switchZ = true; }

		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
		}
			
		if (switchX){
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] *= -1.0;
		}
		if (switchY){
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] *= -1.0;
		}
		if (switchZ){
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] *= -1.0;
		}
	}
					
					

}
