
public class BoundaryFixedToInitialButZeroGradientOnTheLeft extends Boundary {

	protected InitialData initial;

	public BoundaryFixedToInitialButZeroGradientOnTheLeft(GridCartesian grid, InitialData initial) {
		super(grid);
		this.initial = initial;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time) {
		int i, j, k, nextI, nextJ, nextK;
		
		i = g.i(); j = g.j(); k = g.k();

		nextI = i; nextJ = j; nextK = k;
		
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }
		
		if ((j < ((GridCartesian) grid).indexMinY()) || (j > ((GridCartesian) grid).indexMaxY()) ||
				(i < ((GridCartesian) grid).indexMinX()) || (k < ((GridCartesian) grid).indexMinZ()) ||
				(k > ((GridCartesian) grid).indexMaxZ()) ){
			conservedQuantities[i][j][k] = 
					initial.getInitialValue(new GridCell(i, j, k, grid), conservedQuantities[i][j][k].length);
		} else {
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
			}	
		}	
	}	

	@Override
	public String getDetailedDescription() {
		return "Boundary values fixed to the values initially, apart from the right boundary, which is zero gradient";
	}

}
