
public class HydroState {

	public double rho;
	public double v;
	public double vX, vY, vZ;
	public double v2;
	public double energy;
	public double enthalpy;
	public double soundSpeed;
	public double pressure;
	public int indexDirection;
	
	public HydroState(double rho, double v, double p, EquationsHydroIdeal equations){
		// essentially 1-d
		this.indexDirection = GridCartesian.X_DIR;
		
		this.rho  = rho;
		this.v = v;
		vX = v;
    	vY = 0;
    	vZ = 0;
				
		v2  = vX*vX + vY*vY + vZ*vZ;
						
		pressure = p;
		energy  = equations.getEnergy(pressure, rho*v2);
		enthalpy = equations.getEnthalpy(energy, rho, pressure);
		
		soundSpeed = equations.getSoundSpeedFromPressure(pressure, rho);
	}

	
	public HydroState(double[] quantities, int indexDirection, EquationsHydroIdeal equations){
		this.indexDirection = indexDirection;
		
		rho  = quantities[EquationsHydroIdeal.INDEX_RHO];
		
		
		v = quantities[indexDirection]/rho;
		vX = quantities[EquationsHydroIdeal.INDEX_XMOM]  / rho;
    	vY = quantities[EquationsHydroIdeal.INDEX_YMOM]  / rho;
    	vZ = quantities[EquationsHydroIdeal.INDEX_ZMOM]  / rho;
				
		v2  = vX*vX + vY*vY + vZ*vZ;
						
		energy  = quantities[EquationsHydroIdeal.INDEX_ENERGY];
		pressure = equations.getPressure(energy, rho*v2);
		enthalpy = equations.getEnthalpy(energy, rho, pressure);
		
		soundSpeed = equations.getSoundSpeedFromPressure(pressure, rho);
	}

	public static double[] diff(HydroState right, HydroState left){
		double[] diff = new double[5];
		
		diff[0] = right.rho - left.rho;
		diff[1] = right.rho * right.vX - left.rho * left.vX;
		diff[2] = right.rho * right.vY - left.rho * left.vY;
		diff[3] = right.rho * right.vZ - left.rho * left.vZ;
		diff[4] = right.energy - left.energy;
		return diff;
	}
	
}
