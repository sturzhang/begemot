
public class InitialDataDelaunay extends InitialDataHydro {

	public InitialDataDelaunay(Grid grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double x, y;
		
		// grid should be 1.2 x 2.0
		
		x = g.getX();
		y = g.getY();
		
		double offsetLine = 0.4;
		double slopeLine = 1.45;
		
		// solving offset + xCrossing * slope = 2
		double xCrossing = (2.0 - offsetLine)/slopeLine;
		double lengthLine = xCrossing*Math.sqrt(1 + slopeLine*slopeLine);
		
		double radius2 = lengthLine / 9.2;
		double radius1 = radius2 / 1.4;
		
		double[] center;
		double pressure = 0;
		int upper;
		
		if (y > offsetLine + x*slopeLine){
			upper = 1;
		} else {
			upper = 0;
		}
			
		// TGB values
		double value13 = 0.0;	//0.04 0.04 0.04
		double value11 = 0.07;	//0.18 0.18 0.19
		double value12 = 0.14;	//0.26 0.18 0.29
		double value17 = 0.2;	//0.18 0.31 0.62
		double value20 = 0.3;	//0.50 0.59 0.72
		double value14 = 0.4;	//0.69 0.71 0.73
		double value22 = 0.45;	//0.58 0.58 0.60
		double value6  = 0.5;	//0.47 0.63 0.68
		double value15 = 0.55;	//0.11 0.29 0.32
		double value4  = 0.6;	//0.25 0.35 0.31
		double value5  = 0.7;	//0.52 0.68 0.41
		double value16 = 0.77;	//0.73 0.75 0.38
		double value18 = 0.85;	//0.80 0.53 0.08
		double value19 = 1.0;	//0.70 0.29 0.12
		
		double[][][] valueCircle = new double[2][5][4];
		// lower circles
		// [upper][circle][radius]
		valueCircle[0][0][0] = value6;
		valueCircle[0][1][0] = value4;
		valueCircle[0][2][0] = value6;
		valueCircle[0][3][0] = value4;
		valueCircle[0][4][0] = value6;
		
		valueCircle[1][0][0] = value5;
		valueCircle[1][1][0] = value6;
		valueCircle[1][2][0] = value5;
		valueCircle[1][3][0] = value6;
		valueCircle[1][4][0] = value5;
		
		
		valueCircle[0][0][1] = value18;
		valueCircle[1][1][1] = value18;
		valueCircle[0][2][1] = value18;
		valueCircle[1][3][1] = value18;
		valueCircle[0][4][1] = value18;

		valueCircle[1][0][1] = value19;
		valueCircle[0][1][1] = value20;
		valueCircle[1][2][1] = value19;
		valueCircle[0][3][1] = value20;
		valueCircle[1][4][1] = value19;
		
		
		valueCircle[0][0][2] = value13;
		valueCircle[1][0][2] = value17;
		valueCircle[0][0][3] = value14;
		valueCircle[0][4][2] = value13;
		valueCircle[1][4][2] = value17;
		valueCircle[0][4][3] = value14;
		
		
		valueCircle[0][2][2] = value16;
		valueCircle[1][2][2] = value11;
		valueCircle[0][2][3] = value15;
		valueCircle[1][2][3] = value12;
		
		
		
		boolean pressureSet = false;
		for (int i = 0; i <= 4; i++){
			center = getPositionAlongLine(radius2 * (2*i + 1), offsetLine, slopeLine);
			if ((x-center[0])*(x-center[0]) + (y-center[1])*(y-center[1]) < radius1*radius1 ){
				pressure = valueCircle[upper][i][0];
				pressureSet = true;
			} else if ((x-center[0])*(x-center[0]) + (y-center[1])*(y-center[1]) < radius2*radius2 ){
				pressure = valueCircle[upper][i][1];
				pressureSet = true;
			}
		}
		
		double radius3 = radius2*2;
		double radius4 = radius2*3;
		
		if (!pressureSet){
			for (int i = 0; i <= 4; i+=4){
				center = getPositionAlongLine(radius2 * (2*i + 1), offsetLine, slopeLine);
				if ((x-center[0])*(x-center[0]) + (y-center[1])*(y-center[1]) < radius3*radius3 ){
					pressure = valueCircle[upper][i][2];
					pressureSet = true;
				} else if ((upper == 0) &&
						 ((x-center[0])*(x-center[0]) + (y-center[1])*(y-center[1]) < radius4*radius4 )){
					pressure = valueCircle[upper][i][3];
					pressureSet = true;
				}
			}
		}
		
		
		if (!pressureSet){
			int i = 2;
			center = getPositionAlongLine(radius2 * (2*i + 1), offsetLine, slopeLine);
			if ((x-center[0])*(x-center[0]) + (y-center[1])*(y-center[1]) < (radius3 * (0.9 + 0.1*upper))*(radius3 * (0.9 + 0.1*upper)) ){
				pressure = valueCircle[upper][i][2];
				pressureSet = true;
			} else if ((x-center[0])*(x-center[0]) + (y-center[1])*(y-center[1]) < (radius4 * (0.9 + 0.1*upper))*(radius4 * (0.9 + 0.1*upper)) ){
				pressure = valueCircle[upper][i][3];
				pressureSet = true;
			}
			
		}
		
		if (!pressureSet){
			pressure = value22;
		}
		
		pressure += 1.0;
		pressure *= 5.0;
		
		double[] res = new double[numberOfConservedQuantities];
		
		res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
		res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0; 
		res[EquationsHydroIdeal.INDEX_ENERGY] = ((EquationsHydroIdeal) equations).getEnergy(pressure, 0);
		
		return res;
	}
	
	protected double[] getPositionAlongLine(double distance, double offset, double slope){
		
		// we are solving x*x + y*y = distance*distance
		// i.e. x^2 + (x slope)^2 = distance^2
		// i.e. x = distance / sqrt(1 + slope^2)
		
		double x = distance / Math.sqrt(1 + slope*slope);
		return new double[]{x, offset + x*slope};
	}

	@Override
	public String getDetailedDescription() {
		return "Delaunay-like multi-d Riemann Problem in the pressure";
	}

}
