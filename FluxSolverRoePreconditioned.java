import matrices.*;

public class FluxSolverRoePreconditioned extends FluxSolverRoeHydro {

	private double mach;
		
	public FluxSolverRoePreconditioned(GridCartesian grid, EquationsHydroIdeal equations, double mach, int meanType) {
		super(grid, equations, meanType);
		this.mach = mach;
	}
	
	@Override
	protected double sign(double x){
		if (x < 0){
			return -1.0;
		} else {
			return 1.0;
		}
	}
	
	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		double[][] mat = new double[5][5];
		HydroState state = new HydroState(quantities, EquationsHydroIdeal.getIndexDirection(direction), (EquationsHydroIdeal) equations);

		double machParameter = 1.0; // equations.mach(); does not work fully yet

		double v2 = state.v2;
		double c = state.soundSpeed;
	    double c2 = c*c;
	    double vX = state.vX; 
	    double vY = state.vY; 
	    double vZ = state.vZ;

		
		double rho = 1.0;
	    	//double limitedMach = Math.min(1.0, Math.max(cutoffMach, Math.sqrt(v2) / c));  // cutoffMach
	    //double limitedMach = Math.max(cutoffMach*cutoffMach, Math.min(cutoffMach, Math.sqrt(v2) / c));
	    
	    double machLocal = Math.sqrt(v2) / c;
	    
	    double limitedMach, machCutoff;
	    machCutoff = 1e-10;
	    if (machLocal > mach){
	    	limitedMach = mach;
	    } else {
	    	if (machLocal > machCutoff){
	    		limitedMach = machLocal; //*machLocal;  //cutoffMach*cutoffMach;
	    	} else {
	    		limitedMach = machCutoff;
	    	}
	    }
	    //double limitedMach = Math.min(cutoffMach, Math.max(cutoffMach*cutoffMach/2, machLocal));
	    
	    if (Math.sqrt(v2) / c > 0.05){
	    	//System.err.println("1.0 > 0.05 > " + Math.sqrt(v2) / c + "?");
	    }
	    
		double delta = 1.0/limitedMach - 1;  //cutoffMach  
		
		double waveSpeed = c/machParameter + Math.sqrt(v2);
		
		double stabilityepsilon = 0.0;
		
		double freeDiagRho, freeDiagPerp, oneOverMDiag, rhoCSqrDiag;
		
		freeDiagRho  = Math.sqrt(v2);
		freeDiagPerp = Math.sqrt(v2);
		oneOverMDiag = Math.sqrt(v2);
		rhoCSqrDiag  = Math.sqrt(v2); 
		
		if (direction == GridCartesian.X_DIR){
			double m1n = (vX >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vX*vX*machParameter*machParameter);
			
			//mat[0][0] = m1n * vX + stabilityepsilon;
			//mat[0][0] = m1n * vX;
			mat[0][0] = Math.sqrt(v2);
			//mat[0][0] = 0.5;
			//mat[0][0] = freeDiagRho;
			mat[0][1] = 0.0;
			//mat[0][1] = -rho;
			//mat[0][1] = rho;
			//mat[0][1] = (rho * (-c2 * delta + c * vX * machParameter + delta*vX*vX*machParameter*machParameter)) / (c * tau) ;
			mat[0][2] = 0.0;
			mat[0][3] = 0.0;
			//mat[0][4] = 1.0 / c;
			mat[0][4] = 0.0;
			//mat[0][4] = Math.abs(vX)*c2;
			//mat[0][4] = -m1n*vX/c2 + 1.0/tau/machParameter;
			/*mat[0][4] = (-m1n*machParameter*tau*vX * (c + delta*machParameter*vX) + 
					c * (tau*tau + delta*(-c2*delta + c*vX*machParameter + delta*machParameter*machParameter*vX*vX))) / 
					(c2*machParameter*tau*(c + delta*machParameter*vX)); // */
			
			mat[1][0] = 0.0;
			
			//mat[1][1] = 2.0*c/machParameter;
			//mat[1][1] = c/machParameter;
			//mat[1][1] = c / delta / machParameter;
			//mat[1][1] = c / Math.sqrt(1 + delta*delta) / machParameter;
			//mat[1][1] = c2/tau/machParameter;
			//mat[1][1] = vX;
			//mat[1][1] = Math.sqrt(v2) + stabilityepsilon;
			//mat[1][1] = oneOverMDiag;
			//mat[1][1] = 0.5;
			mat[1][1] = 0.0;

			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			
			//mat[1][4] = (1.0 / machParameter/machParameter - (c-vX)*(c-vX) / 2/c/c) / rho;
			//mat[1][4] = 0.0;
			mat[1][4] = 1.0 / machParameter/machParameter/rho;
			//mat[1][4] = vX/c / machParameter/rho;
			//mat[1][4] = Math.sqrt(Math.abs(vX*vX*vX)/c/c/c / machParameter)/rho;
			//mat[1][4] = vX*vX/c/c/rho;
			/*mat[1][4] = (c*c2*delta + machParameter*tau*tau*vX) / 
					 (c2*machParameter*machParameter*rho*tau + c * delta*machParameter*machParameter*machParameter*rho*tau*vX);
				//*/	
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			//mat[2][2] = m1n*vX + stabilityepsilon;
			//mat[2][2] = Math.sqrt(v2);
			//mat[2][2] = freeDiagPerp;
			//mat[2][2] = 0.5;
			mat[2][2] = 0.0;
			mat[2][3] = 0.0;
			mat[2][4] = 0.0;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			//mat[3][3] = m1n*vX + stabilityepsilon;
			//mat[3][3] = Math.sqrt(v2);
			mat[3][3] = 0.0;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			
			//mat[4][1] = 0.0;
			//mat[4][1] = c2 * rho;
			mat[4][1] = -c2 * rho;
			//mat[4][1] = -0.5*c2 * rho;
			//mat[4][1] = (c*rho * (-c2*delta + c*vX*machParameter + delta*vX*vX*machParameter*machParameter)) / tau;
			
			mat[4][2] = 0.0;
			mat[4][3] = 0.0;
			
			//mat[4][4] = 2.0*c / machParameter;
			//mat[4][4] = c / machParameter;
			//mat[4][4] = vX;
			mat[4][4] = 0.0;
			//mat[4][4] = 0.5;
			//mat[4][4] = Math.sqrt(v2);
			//mat[4][4] = rhoCSqrDiag;
			//mat[4][4] = c / delta / machParameter;
			//mat[4][4] = c2 / machParameter / tau;
			//mat[4][4] = (c *delta*vX + (-c*c2*delta*delta + c*tau*tau) / machParameter / (c + machParameter*delta*vX)) / tau;
		}
		if (direction == GridCartesian.Y_DIR){
			double m1n = (vY >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vY*vY*machParameter*machParameter);
			
			//mat[0][0] = m1n * vY + stabilityepsilon;
			//mat[0][0] = Math.sqrt(v2);
			mat[0][0] = m1n * vY;
			//mat[0][0] = 0.5;
			//mat[0][0] = freeDiagRho;
			mat[0][1] = 0.0;
			mat[0][2] = -10.0;
			//mat[0][2] = -rho;
			//mat[0][2] = rho;
			//mat[0][2] = (rho * (-c2 * delta + machParameter*c*vY + delta*vX*vX*machParameter*machParameter)) / (c * tau);
			mat[0][3] = 0.0;
			//mat[0][4] = 1.0 / c;
			mat[0][4] = 0.0; 	
			//mat[0][4] = Math.abs(vY)*c2;
			//mat[0][4] = -m1n*vY/c2+1.0/machParameter/tau;
			/*mat[0][4] = (-m1n *machParameter* tau * vY * (c + delta*machParameter * vY) + 
					c*(tau*tau + delta*(-c2*delta + machParameter*c*vY + delta * machParameter*machParameter * vY*vY)))/ 
					(c2 * machParameter * tau * (c + machParameter*delta*vY)); //*/
			
			mat[1][0] = 0.0;
			//mat[1][1] = m1n*vY + stabilityepsilon;
			//mat[1][1] = Math.sqrt(v2);
			//mat[1][1] = freeDiagPerp;
			//mat[1][1] = 0.5;
			mat[1][1] = 0.0;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = 0.0;
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			
			//mat[2][2] = 2.0*c/machParameter;
			//mat[2][2] = c/machParameter;
			//mat[2][2] = c / delta / machParameter;
			//mat[2][2] = c / Math.sqrt(1 + delta*delta) / machParameter;
			//mat[2][2] = c2/tau/machParameter;
			//mat[2][2] = vY;
			//mat[2][2] = Math.sqrt(v2) + stabilityepsilon;
			//mat[2][2] = oneOverMDiag;
			//mat[2][2] = 0.5;
			mat[2][2] = 0.0;

			mat[2][3] = 0.0;
			
			//mat[2][4] = (1.0 / machParameter/machParameter - (c-vY)*(c-vY) / 2/c/c) / rho;
			mat[2][4] = 1.0 / machParameter/machParameter/rho;
			//mat[2][4] = vY*vY/c/c/rho;
			//mat[2][4] = 0.0;
			//mat[2][4] = vY/c / machParameter/rho;
			//mat[2][4] = Math.sqrt(Math.abs(vY*vY*vY)/c/c/c / machParameter);
			/*mat[2][4] = (c*c2*delta + machParameter*tau*tau*vY) / 
					(c2 * machParameter*machParameter * rho * tau + c * delta * machParameter*machParameter*machParameter * rho * tau * vY);
			//*/
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			//mat[3][3] = m1n*vY + stabilityepsilon;
			//mat[3][3] = Math.sqrt(v2);
			mat[3][3] = 0.0;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = 0.0;
			
			//mat[4][2] = 0.0;
			mat[4][2] = -2.0*c2 * rho;
			//mat[4][2] = c2 * rho;
			//mat[4][2] = (c*rho * (-c2*delta + machParameter*c*vY + delta*vY*vY*machParameter*machParameter)) / tau;
			
			mat[4][3] = 0.0;
			
			
			//mat[4][4] = 2.0*c/machParameter;
			//mat[4][4] = c/machParameter;
			//mat[4][4] = vY;
			//mat[4][4] = Math.sqrt(v2);
			mat[4][4] = 0.0;
			//mat[4][4] = rhoCSqrDiag;
			//mat[4][4] = 0.5;
			//mat[4][4] = c / delta / machParameter;
			//mat[4][4] = c2 / machParameter / tau;
			//mat[4][4] = (c *delta*vY + (-c*c2*delta*delta + c*tau*tau) / machParameter/ (c + machParameter*delta*vY)) / tau;		
		}
		if (direction == GridCartesian.Z_DIR){
			double m1n = (vZ >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vZ*vZ*machParameter*machParameter);
			
			mat[0][0] = m1n * vZ + stabilityepsilon;
			mat[0][1] = 0.0;
			mat[0][2] = 0.0;
			mat[0][3] = 0.0;
			mat[0][4] = 0.0; 	
			
			mat[1][0] = 0.0;
			mat[1][1] = m1n*vZ + stabilityepsilon;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = 0.0;

			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = m1n*vZ + stabilityepsilon;
			mat[2][3] = 0.0;
			mat[2][4] = 0.0;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = Math.sqrt(v2) + stabilityepsilon;
			mat[2][4] = vZ*vZ/c/c/rho;
			
			mat[4][0] = 0.0;
			mat[4][1] = 0.0;
			mat[4][2] = 0.0;
			mat[4][3] = -c2 * rho;
			mat[4][4] = 2.0*c/machParameter;
		}
		
		double dUdVarr[][] = new double[5][5];
		double dVdUarr[][] = new double[5][5];
		
		dUdVarr[0][0] = 1.0;
		dUdVarr[1][0] = vX;
		dUdVarr[1][1] = rho;
		dUdVarr[2][0] = vY;
		dUdVarr[2][2] = rho;
		dUdVarr[3][0] = vZ;
		dUdVarr[3][3] = rho;
		dUdVarr[4][0] = machParameter*machParameter * v2/2;
		dUdVarr[4][1] = machParameter*machParameter * rho*vX;
		dUdVarr[4][2] = machParameter*machParameter * rho*vY;
		dUdVarr[4][3] = machParameter*machParameter * rho*vZ;
		dUdVarr[4][4] = 1.0/(((EquationsHydroIdeal) equations).gamma() - 1);
		
		dVdUarr[0][0] = 1.0;
		dVdUarr[1][0] = -vX/rho;
		dVdUarr[1][1] = 1.0/rho;
		dVdUarr[2][0] = -vY/rho;
		dVdUarr[2][2] = 1.0/rho;
		dVdUarr[3][0] = -vZ/rho;
		dVdUarr[3][3] = 1.0/rho;
		dVdUarr[4][0] = machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*v2/2;
		dVdUarr[4][1] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vX;
		dVdUarr[4][2] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vY;
		dVdUarr[4][3] = -machParameter*machParameter * (((EquationsHydroIdeal) equations).gamma()-1)*vZ;
		dVdUarr[4][4] = (((EquationsHydroIdeal) equations).gamma() - 1);

		SquareMatrix dUdV = new SquareMatrix(dUdVarr);
		SquareMatrix dVdU = new SquareMatrix(dVdUarr);

		/*
		if (direction == Grid.X_DIR){
			double m1n = (vX >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vX*vX*machParameter*machParameter);
			
			mat[0][0] = 0.0; //m1n * vX;
			mat[0][1] = -10.0;
			mat[0][2] = -10.0;
			mat[0][3] = 0.0;
			mat[0][4] = 0.0;
			
			mat[1][0] = 0.0;
			mat[1][1] = 0.0;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = (((EquationsHydroIdeal) equations).gamma() - 1) / machParameter/machParameter;
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = 0.0;
			mat[2][3] = 0.0;
			mat[2][4] = 0.0;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = 0.0;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = -1.4 / (((EquationsHydroIdeal) equations).gamma() - 1);
			mat[4][2] = 0.0;
			mat[4][3] = 0.0;
			mat[4][4] = 0.0;
		}
		if (direction == Grid.Y_DIR){
			double m1n = (vY >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vY*vY*machParameter*machParameter);
			
			mat[0][0] = 0.0; // m1n * vY;
			mat[0][1] = -10.0;
			mat[0][2] = -10.0;
			mat[0][3] = 0.0;
			mat[0][4] = 0.0; 	
			
			mat[1][0] = 0.0;
			mat[1][1] = 0.0;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			mat[1][4] = 0.0;
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = 0.0;
			mat[2][3] = 0.0;
			mat[2][4] = (((EquationsHydroIdeal) equations).gamma() - 1) / machParameter/machParameter;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = 0.0;
			mat[3][4] = 0.0;
			
			mat[4][0] = 0.0;
			mat[4][1] = 0.0;
			mat[4][2] = -1.4 / (((EquationsHydroIdeal) equations).gamma() - 1);
			mat[4][3] = 0.0;
			mat[4][4] = 0.0;
		}
		
		return new UpwindingMatrixAndWaveSpeed(new SquareMatrix(mat), waveSpeed);
		//*/
		
		return new UpwindingMatrixAndWaveSpeed(
				dUdV.mult(new SquareMatrix(mat)).mult(dVdU).toSquareMatrix(),
				waveSpeed); //*/
	}
	
	
	protected UpwindingMatrixAndWaveSpeed absJacobianOld(int direction, double vX, double vY, double vZ, double enthalpy){
		double cutoffMach = 1e-8;
		double[] unitVector = new double[]{0.0,
				(direction == GridCartesian.X_DIR) ? 1.0 : 0.0,
				(direction == GridCartesian.Y_DIR) ? 1.0 : 0.0,
				(direction == GridCartesian.Z_DIR) ? 1.0 : 0.0, 0.0};	
		double vDirection = 0.0; // init to make compiler happy
		if (direction == GridCartesian.X_DIR){ 
	    	vDirection = vX;
	    }	
	    if (direction == GridCartesian.Y_DIR){ 
	    	vDirection = vY;
		}	
	    if (direction == GridCartesian.Z_DIR){ 
	    	vDirection = vZ;
		}	

		
	    double rho = 1.0;
		double v2 = vX*vX + vY*vY + vZ*vZ;
		double soundSpeed = ((EquationsHydroIdeal) equations).getSoundSpeedFromEnthalpy(enthalpy, v2);
	    double soundSpeedSqr = Math.pow(soundSpeed, 2);
		double[][] Aarr = new double[5][5];
		
		Aarr[0][0] = vDirection;
		for (int i = 0; i <=4; i++){
			Aarr[0][i] += rho*unitVector[i];
		}
		
		Aarr[1][1] = vDirection;
		Aarr[1][4] = unitVector[1]/rho;
		Aarr[2][2] = vDirection;
		Aarr[2][4] = unitVector[2]/rho;
		Aarr[3][3] = vDirection;
		Aarr[3][4] = unitVector[3]/rho;
		Aarr[4][4] = vDirection;
		for (int i = 0; i <=4; i++){
			Aarr[4][i] += rho*soundSpeedSqr*unitVector[i];
		}

		SquareMatrix A = new SquareMatrix(Aarr);
		
		double[][] dUdVarr = new double[5][5];
		double[][] dVdUarr = new double[5][5];
		
		dUdVarr[0][0] = 1.0;
		dUdVarr[1][0] = vX;
		dUdVarr[1][1] = rho;
		dUdVarr[2][0] = vY;
		dUdVarr[2][2] = rho;
		dUdVarr[3][0] = vZ;
		dUdVarr[3][3] = rho;
		dUdVarr[4][0] = v2/2;
		dUdVarr[4][1] = rho*vX;
		dUdVarr[4][2] = rho*vY;
		dUdVarr[4][3] = rho*vZ;
		dUdVarr[4][4] = 1.0/(((EquationsHydroIdeal) equations).gamma() - 1);
		
		dVdUarr[0][0] = 1.0;
		dVdUarr[1][0] = -vX/rho;
		dVdUarr[1][1] = 1.0/rho;
		dVdUarr[2][0] = -vY/rho;
		dVdUarr[2][2] = 1.0/rho;
		dVdUarr[3][0] = -vZ/rho;
		dVdUarr[3][3] = 1.0/rho;
		dVdUarr[4][0] = (((EquationsHydroIdeal) equations).gamma()-1)*v2/2;
		dVdUarr[4][1] = -(((EquationsHydroIdeal) equations).gamma()-1)*vX;
		dVdUarr[4][2] = -(((EquationsHydroIdeal) equations).gamma()-1)*vY;
		dVdUarr[4][3] = -(((EquationsHydroIdeal) equations).gamma()-1)*vZ;
		dVdUarr[4][4] = (((EquationsHydroIdeal) equations).gamma() - 1);

		SquareMatrix dUdV = new SquareMatrix(dUdVarr);
		SquareMatrix dVdU = new SquareMatrix(dVdUarr);
		
		double limitedMach = Math.min(1.0, Math.max(cutoffMach, Math.sqrt(v2) / soundSpeed));
		double delta = 1.0/limitedMach - 1;  //  
		double tau = Math.sqrt(soundSpeedSqr * (1.0 + delta*delta) - delta*delta*vDirection*vDirection);
		DiagonalMatrix EigenvaluesSign = new DiagonalMatrix(
				new double[] {sign(vDirection), sign(vDirection), sign(vDirection), sign(vDirection - tau), sign(vDirection + tau)});
		
		double waveSpeed = soundSpeed + Math.abs(vDirection);
		
		
		int indexDirection = 0; // init to make compiler happy
		if (direction == GridCartesian.X_DIR){ indexDirection = EquationsHydroIdeal.INDEX_XMOM; }	
	    if (direction == GridCartesian.Y_DIR){ indexDirection = EquationsHydroIdeal.INDEX_YMOM; }	
	    if (direction == GridCartesian.Z_DIR){ indexDirection = EquationsHydroIdeal.INDEX_ZMOM; }	
	    		
		double[][] Rarr = new double[5][5];
		double[][] RInversearr = new double[5][5];
			
		int row = 0;
		for (int i = 2; i >= 0; i--){
			Rarr[row][i] = 1.0;
			RInversearr[row][i] = 1.0;
			
			row++;
			if (unitVector[row] == 1.0){ row++; }
		}
		SquareMatrix RInverse = new SquareMatrix(RInversearr);
		RInverse.transposeMe();
		RInversearr = RInverse.toArray();
		
		Rarr[0][3] = 1.0/soundSpeedSqr;
		Rarr[indexDirection][3] = - (soundSpeed*delta + tau) / (rho * soundSpeed * (soundSpeed + delta * vDirection));
		Rarr[4][3] = 1.0;
		
		Rarr[0][4] = 1.0/soundSpeedSqr;
		Rarr[indexDirection][4] = (-soundSpeed*delta + tau) / (rho * soundSpeed * (soundSpeed + delta * vDirection));
		Rarr[4][4] = 1.0;
		
		RInversearr[2][4] = -1.0 / soundSpeedSqr;
		
		RInversearr[3][indexDirection] = - rho*soundSpeed * (soundSpeed + delta*vDirection) / (2.0*tau);
		RInversearr[3][4] = 0.5 - soundSpeed * delta / (2.0 * tau);
		
		RInversearr[4][indexDirection] = rho*soundSpeed * (soundSpeed + delta*vDirection) / (2.0*tau);
		RInversearr[4][4] = 0.5 + soundSpeed * delta / (2.0 * tau);
		
		SquareMatrix R = new SquareMatrix(Rarr);
		RInverse = new SquareMatrix(RInversearr);
		
		/*
		if (dUdV.isNaN()){
	    	System.err.println("?");
	    }
		if (dUdV.mult(A).isNaN()){
	    	System.err.println("?");
	    }
		if (dUdV.mult(A).mult(R).isNaN()){
	    	System.err.println("?");
	    }
		if (dUdV.mult(A).mult(R).mult(EigenvaluesSign).isNaN()){
	    	System.err.println("?");
	    }
		if (dUdV.mult(A).mult(R).mult(EigenvaluesSign).mult(RInverse).isNaN()){
	    	System.err.println("?");
	    }
		if (dUdV.mult(A).mult(R).mult(EigenvaluesSign).mult(RInverse).mult(dVdU).isNaN()){
	    	System.err.println("?");
	    }*/
		
		
		//dUdV.mult(R).mult(dVdU).print();
		//System.out.println(" ");
		//new SquareMatrix(equations.getJacobianEigenvectorsFor1DFlow(1, vX, vY, vZ, soundSpeed, enthalpy)).print();
		
		/*DiagonalMatrix id = DiagonalMatrix.identity(5); 
		double testInverse;
		testInverse = R.mult(RInverse).sub(id).sumOfSquaredElements();
		if (testInverse > 1e-10){
			R.mult(RInverse).sub(id).print();
		}*/
		
		return new UpwindingMatrixAndWaveSpeed(
				dUdV.mult(A).mult(R).mult(EigenvaluesSign).mult(RInverse).mult(dVdU).toSquareMatrix(),
				waveSpeed);
				
	}
	

	
}
