
public abstract class Printer {

	public Printer() {

	}

	public abstract void print(String text);
	public abstract void println(String text);
	
	
}
