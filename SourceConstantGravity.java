
public class SourceConstantGravity extends SourceGravityHydro {

	protected double[] g;
	
	public SourceConstantGravity(GridCartesian grid, EquationsHydroIdeal equations, double[] g, boolean averaged) {
		super(grid, equations, averaged);
		this.g = g;
	}

	@Override
    public String getDetailedDescription(){
    	return "const gravity g = (" + g[0] + ", " + g[1] + ", " + g[2] + ")";
    }

	
	@Override
	protected double[] gravityacceleration(int i, int j, int k) {
		return g;
	}

	public double gx(){ return g[0]; }
	public double gy(){ return g[1]; }
	public double gz(){ return g[2]; }
	
	
}
