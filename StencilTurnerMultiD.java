
public class StencilTurnerMultiD extends StencilTurner {

	public static final int NEIGHBOUR_LEFTTOP     = 0;
	public static final int NEIGHBOUR_LEFT        = 1;
	public static final int NEIGHBOUR_LEFTBOTTOM  = 2;
	public static final int NEIGHBOUR_RIGHTTOP    = 3;
	public static final int NEIGHBOUR_RIGHT       = 4;
	public static final int NEIGHBOUR_RIGHTBOTTOM = 5;

	
	// the flux is to be evaluated at i+frac12, i.e. NEIGHBOUR_IJK = NEIGHBOUR_LEFT and NEIGHBOUR_Ip1JK = NEIGHBOUR_RIGHT
	public static final int NEIGHBOUR_IJp1K      = 0;
	public static final int NEIGHBOUR_IJK        = 1;
	public static final int NEIGHBOUR_IJm1K      = 2;
	public static final int NEIGHBOUR_Ip1Jp1K    = 3;
	public static final int NEIGHBOUR_Ip1JK      = 4;
	public static final int NEIGHBOUR_Ip1Jm1K    = 5;
	
	public static final int NEIGHBOUR_IJp1Kp1    = 6;
	public static final int NEIGHBOUR_IJKp1      = 7;
	public static final int NEIGHBOUR_IJm1Kp1    = 8;
	public static final int NEIGHBOUR_Ip1Jp1Kp1  = 9;
	public static final int NEIGHBOUR_Ip1JKp1    = 10;
	public static final int NEIGHBOUR_Ip1Jm1Kp1  = 11;
	
	public static final int NEIGHBOUR_IJp1Km1    = 12;
	public static final int NEIGHBOUR_IJKm1      = 13;
	public static final int NEIGHBOUR_IJm1Km1    = 14;
	public static final int NEIGHBOUR_Ip1Jp1Km1  = 15;
	public static final int NEIGHBOUR_Ip1JKm1    = 16;
	public static final int NEIGHBOUR_Ip1Jm1Km1  = 17;
		
	
	public StencilTurnerMultiD(GridCartesian grid) {
		super(grid);
	}

	@Override
	public GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge){
		//int edgeDir = ((GridCartesian) grid).getDirectionPerpendicularToEdge(g, gridEdge.index());		
		GridCell nb = gridEdge.getOtherGridCell(g);
		GridCell leftCell, rightCell;
		
		int offsetX = 0, offsetY = 0, offsetZ = 0;
		
		if (Math.abs(gridEdge.normalVector()[0]) == 1.0){
			if (gridEdge.normalVector()[0]*(nb.i() - g.i()) > 0){
				leftCell = g; rightCell = nb;
			} else {
				leftCell = nb; rightCell = g;
			}
			if (gridEdge.normalVector()[0] > 0){
				offsetY = 1;
				offsetZ = 1;
			} else {
				offsetY = -1;
				offsetZ = -1;
			}
			
		} else if (Math.abs(gridEdge.normalVector()[1]) == 1.0){
			if (gridEdge.normalVector()[1]*(nb.j() - g.j()) > 0){
				leftCell = g; rightCell = nb;
			} else {
				leftCell = nb; rightCell = g;
			}
			if (gridEdge.normalVector()[1] > 0){
				offsetX = -1;
				offsetZ = 1;
			} else {
				offsetX = 1;
				offsetZ = -1;
			}
			
		} else if (Math.abs(gridEdge.normalVector()[2]) == 1.0){
			if (gridEdge.normalVector()[2]*(nb.k() - g.k()) > 0){
				leftCell = g; rightCell = nb;
			} else {
				leftCell = nb; rightCell = g;
			}
			if (gridEdge.normalVector()[1] > 0){
				offsetX = 1;
				offsetY = -1;
			} else {
				offsetX = -1;
				offsetY = 1;
			}
			
		} else {
			System.err.println("ERROR: Cartesian grid seemingly has odd normal vectors!" + gridEdge.normalVector()[0] + " " + gridEdge.normalVector()[1]);
			leftCell = null; rightCell = null;
		}
		
		
		//if (edgeDir == GridCartesian.X_DIR){ offsetY = 1; }
		//if (edgeDir == GridCartesian.Y_DIR){ offsetX = -1; } // needed for rotation
		
		if (grid.dimensions() == 1){ offsetY = 0; offsetZ = 0; }
		else if (grid.dimensions() == 2){ offsetZ = 0; }
		
		
		return new GridCell[]{
				new GridCell( leftCell.i()+offsetX,  leftCell.j()+offsetY,  leftCell.k(), grid),
				leftCell, 
				new GridCell( leftCell.i()-offsetX,  leftCell.j()-offsetY,  leftCell.k(), grid),
				new GridCell(rightCell.i()+offsetX, rightCell.j()+offsetY, rightCell.k(), grid),
				rightCell, 
				new GridCell(rightCell.i()-offsetX, rightCell.j()-offsetY, rightCell.k(), grid),
				
				new GridCell( leftCell.i()+offsetX,  leftCell.j()+offsetY,  leftCell.k()+offsetZ, grid),
				new GridCell( leftCell.i(),          leftCell.j(),          leftCell.k()+offsetZ, grid), 
				new GridCell( leftCell.i()-offsetX,  leftCell.j()-offsetY,  leftCell.k()+offsetZ, grid),
				new GridCell(rightCell.i()+offsetX, rightCell.j()+offsetY, rightCell.k()+offsetZ, grid),
				new GridCell(rightCell.i(),         rightCell.j(),         rightCell.k()+offsetZ, grid), 
				new GridCell(rightCell.i()-offsetX, rightCell.j()-offsetY, rightCell.k()+offsetZ, grid),
				
				new GridCell( leftCell.i()+offsetX,  leftCell.j()+offsetY,  leftCell.k()-offsetZ, grid),
				new GridCell( leftCell.i(),          leftCell.j(),          leftCell.k()-offsetZ, grid), 
				new GridCell( leftCell.i()-offsetX,  leftCell.j()-offsetY,  leftCell.k()-offsetZ, grid),
				new GridCell(rightCell.i()+offsetX, rightCell.j()+offsetY, rightCell.k()-offsetZ, grid),
				new GridCell(rightCell.i(),         rightCell.j(),         rightCell.k()-offsetZ, grid), 
				new GridCell(rightCell.i()-offsetX, rightCell.j()-offsetY, rightCell.k()-offsetZ, grid)};
				
		
	}

}
