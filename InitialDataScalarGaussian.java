
public class InitialDataScalarGaussian extends InitialDataScalar {

	public InitialDataScalarGaussian(Grid grid, EquationsScalar equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double x = g.getX() - grid.xMidpoint();
	
		double width = 0.3;
		double amplitude = 1.0;
		
		return new double[] {amplitude*Math.exp(-x*x/width/width)};
	}


	@Override
	public String getDetailedDescription() {
		return "Scalar initial data: Gaussian in 1-d";
	}

}
