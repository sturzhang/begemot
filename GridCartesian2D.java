
public class GridCartesian2D extends GridCartesian {

	public GridCartesian2D(double xmin, double xmax, int nx, 
		double ymin, double ymax, int ny, int ghostcells){
		super();
		
    	this.xmin = xmin;
    	this.xmax = xmax;
    	this.ymin = ymin;
    	this.ymax = ymax;
    	
    	ghostcellsX = ghostcells;
    	ghostcellsY = ghostcells;
    	
    	this.nx = nx;
    	this.ny = ny;

    	nz = 1;
    	ghostcellsZ = 0;
    }

	public int dimensions(){ return 2; }

    public String getDetailedDescription(){
    	return "(2-d grid)";
    }

}


