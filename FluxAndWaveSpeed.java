
public class FluxAndWaveSpeed extends FluxMultiStepAndWaveSpeed {

	
	public FluxAndWaveSpeed(double[] flux, double waveSpeed) {
		super(flux, waveSpeed);
	}
	
	public double[] flux(){ return flux[0]; }
	
	public void setFlux(double[] flux){
		this.flux = new double[1][];
		this.flux[0] = flux; 
	};
	
	public static double[] multiply( double[] fl, double val){
		double[] res = new double[fl.length];
		for (int i = 0; i < fl.length; i++){
			res[i] = fl[i]*val;
		}
		return res;
	}
	
	public static double[] add( double[] fl, double[] fl2){
		double[] res = new double[fl.length];
		for (int i = 0; i < fl.length; i++){
			res[i] = fl[i] + fl2[i];
		}
		return res;
	}
}
