
public class InitialDataConstFlowIsentropic extends InitialDataHydro {

	private double rho;
	private double[] velocity;
	
	public InitialDataConstFlowIsentropic(Grid grid, EquationsIsentropicEuler equations, 
			double rho, double[] velocity) {
		super(grid, equations);
		this.rho = rho;
		this.velocity = velocity;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		res[EquationsIsentropicEuler.INDEX_RHO] = rho;
					
		res[EquationsIsentropicEuler.INDEX_XMOM] = rho*velocity[0];
		res[EquationsIsentropicEuler.INDEX_YMOM] = rho*velocity[1];
		res[EquationsIsentropicEuler.INDEX_ZMOM] = rho*velocity[2];
		
		for (int s = 5; s < numberOfConservedQuantities; s++){
			// passive scalars
			//if (grid.getX(i) > grid.xMidpoint()){
				res[s] = Math.pow(-1.0, Math.max(0, Math.round((g.getY() - grid.ymin())/(grid.ymax() - grid.ymin())*10)));
			//}
		}
		
		return res;	
	}

	@Override
	public String getDetailedDescription() {
    	return "Ideal (isentropic) gas of density = " + rho +  
    			" flowing in a stationary manner at a velocity v = (" + velocity[0] + ", "
    			+ velocity[1] + ", " + velocity[2] + ")";
	}

}
