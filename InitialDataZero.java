import java.util.Random;


public class InitialDataZero extends InitialData {

	protected double randomNoiseAmplitude;
	
	public InitialDataZero(Grid grid, double randomNoiseAmplitude) {
		super(grid);
		this.randomNoiseAmplitude = randomNoiseAmplitude;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[]res = new double[numberOfConservedQuantities];
		
		Random r = new Random();
		
		for (int q = 0; q < numberOfConservedQuantities; q++){
			if (q != EquationsHydroIdeal.INDEX_ZMOM){
				res[q] = r.nextDouble() * randomNoiseAmplitude;
			} else {
				res[q] = 0;
			}
		}
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Initial data all zero with random noise at amplitude " + randomNoiseAmplitude;
	}

}
