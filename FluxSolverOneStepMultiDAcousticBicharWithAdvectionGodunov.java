
public class FluxSolverOneStepMultiDAcousticBicharWithAdvectionGodunov extends
		FluxSolverOneStepMultiDAcousticBichar {

	public FluxSolverOneStepMultiDAcousticBicharWithAdvectionGodunov(GridCartesian grid, EquationsAcousticSimple equations){
		super(grid, equations);
	}
	
	@Override
	protected double[] multidflux(double[] LT, double[] L, double[] LB, double[] RT, double[] R, double[] RB){
		// TODO this is mostly a copy from superclass, but for the sake of a self-contained implementation tolerated
		
		int quantities = L.length;
		double[] fluxRes = new double[quantities];
		double diffX[] = new double[quantities];
		double mixeddiffX[] = new double[quantities];
		double sumX[] = new double[quantities];
		double centralAverageLeft[] = new double[quantities];
		double centralAverageRight[] = new double[quantities];
		double centralDiffLeft[] = new double[quantities];
		double centralDiffRight[] = new double[quantities];
		double secondDiffLeft[] = new double[quantities];
		double secondDiffRight[] = new double[quantities];
		
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		
		double U = ((EquationsAcousticSimple) equations).vx();
		double V = ((EquationsAcousticSimple) equations).vy();
		
		
		for (int q = 0; q < quantities; q++){ 
			diffX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] - (0.25*LT[q] + 0.5*L[q] + 0.25*LB[q]);
			sumX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] + 0.25*LT[q] + 0.5*L[q] + 0.25*LB[q];
			mixeddiffX[q] = (LT[q] + RT[q] - (LB[q] + RB[q]));
			
			centralAverageLeft[q] = (LT[q] + LB[q])/2;
			centralAverageRight[q] = (RT[q] + RB[q])/2;
			centralDiffLeft[q] = LT[q] - LB[q];
			centralDiffRight[q] = RT[q] - RB[q];
			secondDiffLeft[q] = LT[q] - 2.0*L[q] + LB[q];
			secondDiffRight[q] = RT[q] - 2.0*R[q] + RB[q];
		}
		
		
		/*if (((EquationsAcousticSimple) equations).symmetric()){
			fluxRes[EquationsAcousticSimple.INDEX_VX] = 0.5*c*sumX[EquationsAcousticSimple.INDEX_P];
			fluxRes[EquationsAcousticSimple.INDEX_VY] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_VZ] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_P ] = 0.5*c*sumX[EquationsAcousticSimple.INDEX_VX];
		} else {
			fluxRes[EquationsAcousticSimple.INDEX_VX] = 0.5*sumX[EquationsAcousticSimple.INDEX_P];
			fluxRes[EquationsAcousticSimple.INDEX_VY] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_VZ] = 0.0;
			fluxRes[EquationsAcousticSimple.INDEX_P ] = 0.5*c*c*sumX[EquationsAcousticSimple.INDEX_VX];
		}
		*/
		
		double absU = Math.abs(U);
		double absV = Math.abs(V);
		
		
		fluxRes[EquationsAcoustic.INDEX_VX] = 0;
		fluxRes[EquationsAcoustic.INDEX_VY] = 0;
		
		fluxRes[EquationsAcoustic.INDEX_VX] += 0.5*U*(centralAverageLeft[EquationsAcoustic.INDEX_VX] 
				+ centralAverageRight[EquationsAcoustic.INDEX_VX]);
		fluxRes[EquationsAcoustic.INDEX_VX] -= 0.5*absU*(centralAverageRight[EquationsAcoustic.INDEX_VX] 
				- centralAverageLeft[EquationsAcoustic.INDEX_VX]);
		fluxRes[EquationsAcoustic.INDEX_VX] += 0.25*absU*absV/V*(centralDiffRight[EquationsAcoustic.INDEX_VX] 
				- centralDiffLeft[EquationsAcoustic.INDEX_VX]);
		fluxRes[EquationsAcoustic.INDEX_VX] -= 0.25*U*absV/V*(centralDiffRight[EquationsAcoustic.INDEX_VX] 
				+ centralDiffLeft[EquationsAcoustic.INDEX_VX]);
		
		/*fluxRes[EquationsAcoustic.INDEX_VX] += 0.125*U*U*absV/V/V*(centralDiffRight[EquationsAcoustic.INDEX_VY] 
				+ centralDiffLeft[EquationsAcoustic.INDEX_VY]);
		fluxRes[EquationsAcoustic.INDEX_VX] -= 0.125*U*U/V*(secondDiffRight[EquationsAcoustic.INDEX_VY] 
				+ secondDiffLeft[EquationsAcoustic.INDEX_VY]);
		fluxRes[EquationsAcoustic.INDEX_VX] -= 0.125*absU*absV*U/V/V*(centralDiffRight[EquationsAcoustic.INDEX_VY] 
				- centralDiffLeft[EquationsAcoustic.INDEX_VY]);
		fluxRes[EquationsAcoustic.INDEX_VX] += 0.125*absU*U/V*(secondDiffRight[EquationsAcoustic.INDEX_VY] 
				- secondDiffLeft[EquationsAcoustic.INDEX_VY]);//*/

		
		fluxRes[EquationsAcoustic.INDEX_VY] += 0.5*U*(L[EquationsAcoustic.INDEX_VY] + R[EquationsAcoustic.INDEX_VY]);
		fluxRes[EquationsAcoustic.INDEX_VY] -= 0.5*absU*(R[EquationsAcoustic.INDEX_VY] - L[EquationsAcoustic.INDEX_VY]);
		
		
		fluxRes[EquationsAcoustic.INDEX_VY] += 0.125*V*(secondDiffRight[EquationsAcoustic.INDEX_VX] 
				+ secondDiffLeft[EquationsAcoustic.INDEX_VX]);
		fluxRes[EquationsAcoustic.INDEX_VY] -= 0.125*V*absU/U*(secondDiffRight[EquationsAcoustic.INDEX_VX] 
				- secondDiffLeft[EquationsAcoustic.INDEX_VX]);
		fluxRes[EquationsAcoustic.INDEX_VY] += 0.125*absU*absV/U*(centralDiffRight[EquationsAcoustic.INDEX_VX] 
				- centralDiffLeft[EquationsAcoustic.INDEX_VX]);
		fluxRes[EquationsAcoustic.INDEX_VY] -= 0.125*absV*(centralDiffRight[EquationsAcoustic.INDEX_VX] 
				+ centralDiffLeft[EquationsAcoustic.INDEX_VX]);

		
		fluxRes[EquationsAcoustic.INDEX_P] = 0;
		
		return fluxRes;
	}

	
	
	@Override
	public String getDetailedDescription() {
		return "Roe' multi-d solver implemented in a way to avoid the vertex fluxes; augmented with advective diffusion derived as a Godunov scheme";
	}
}
