
public class OutputAcousticJustOneCell extends Output {

	protected int i,j,k;
	protected InitialData initial;
	
	public OutputAcousticJustOneCell(double outputTimeInterval, Grid grid,
			EquationsAcoustic equations, int i, int j, int k, InitialData initial) {
		super(outputTimeInterval, grid, equations);
		this.i = i;
		this.j = j;
		this.k = k;
		this.initial = initial;
	}

	@Override
	public String getDetailedDescription() {
		return "output all values of one cell (" + i + ", " + j + ", " + k + ") over time";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double vx, vy, vz, pressure;
		double x, y;
		
		// TODO copy from OutputAcoustic
		
		vx = quantities[i][j][k][EquationsAcoustic.INDEX_VX];
		vy = quantities[i][j][k][EquationsAcoustic.INDEX_VY];
		vz = quantities[i][j][k][EquationsAcoustic.INDEX_VZ];
		pressure = quantities[i][j][k][EquationsAcoustic.INDEX_P];
			
		x = grid.getX(i, j, k)-grid.xMidpoint();
		y = grid.getY(i, j, k)-grid.xMidpoint();
		
		println("");
		print(time + " ");
		print(x + " ");
		print(y + " ");
		print(grid.getZ(i, j, k) + " ");
		print(vx + " " + vy + " " + vz + " " );
		print(pressure + " ");
		print(Math.sqrt(vx*vx+vy*vy+vz*vz) + " ");
		
		if (initial instanceof InitialDataFourRegionsDivergence){
			double discDivNE = ((InitialDataFourRegionsDivergence) initial).discDivNE();
			double discDivNW = ((InitialDataFourRegionsDivergence) initial).discDivNW();
			double discDivSE = ((InitialDataFourRegionsDivergence) initial).discDivSE();
			double discDivSW = ((InitialDataFourRegionsDivergence) initial).discDivSW();
			
			double deltaNorth = discDivNE - discDivNW;
			double deltaSouth = discDivSE - discDivSW;
			//System.err.println(deltaNorth + " " + deltaSouth);
			double r = Math.sqrt(x*x + y*y);
			
			double[] init = initial.getInitialValue(new GridCell(i,j,k, grid), quantities[i][j][k].length);
			
			//if ((x > 0) && (y > 0)){
				double theoryValueU = init[EquationsAcoustic.INDEX_VX];
				if (time > r){
					theoryValueU += (r - x)*deltaNorth*2/4 + (time - r)*(deltaNorth + deltaSouth)/4;
				} else if (time > x){
					theoryValueU += (time - x)*deltaNorth*2/4;
				}  
				
				/*double theoryValueU = (time-x)*(deltaNorth + deltaSouth)/4 
						+ init[EquationsAcoustic.INDEX_VX];//*/
				
				
				print(theoryValueU + " ");
			//}
		}
		
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub

	}

}
