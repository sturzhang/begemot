
public class InitialDataHydroShocktubeLevequeEntropyTest extends
		InitialDataHydroShocktube {

	public InitialDataHydroShocktubeLevequeEntropyTest(Grid grid,
			EquationsHydroIdeal equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	// Leveque test case
	@Override
	public double leftDensity() {  return 3.0; }
	@Override
	public double leftVx() {       return 0.9; }
	@Override
	public double leftPressure() { return 3.0; }

	@Override
	public double rightDensity() {  return 1.0; }
	@Override
	public double rightVx() {       return 0.9; }
	@Override
	public double rightPressure() { return 1.0; }


}
