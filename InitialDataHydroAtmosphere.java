import java.util.Random;


public abstract class InitialDataHydroAtmosphere extends InitialDataHydroWithDerivatives {

	protected double gy; // y-component of gravity
	protected boolean randomPerturbation;
	protected double xVelocity;
	
	public InitialDataHydroAtmosphere(GridCartesian grid, EquationsHydroIdeal equations, double gy, double xVelocity, boolean randomPerturbation) {
		super(grid, equations);
		save(gy, xVelocity, randomPerturbation);
	}

	public InitialDataHydroAtmosphere(GridCartesian grid, EquationsHydroIdealWith1dDeriv equations, double gy, double xVelocity, boolean randomPerturbation) {
		super(grid, equations);
		save(gy, xVelocity, randomPerturbation);
	}

	private void save(double gy, double xVelocity, boolean randomPerturbation){
		this.gy = gy;
		this.randomPerturbation = randomPerturbation;
		this.xVelocity = xVelocity;
	}
	
    public abstract double getRho(double y);
    public abstract double getPressure(double y);
    
    public abstract double getRhoDeriv(double y);
    public abstract double getPressureDeriv(double y);
    
    
    public double getEnergy(double pressure){
    	return ((EquationsHydroIdeal) equations).getEnergy(pressure, 0.0);
    }
    
    public double getEnergyDeriv(double y){
    	return getPressureDeriv(y)/(((EquationsHydroIdeal) equations).gamma()-1);
    }
    
    protected double variable(double x, double y){
    	return x;    	
    }
    
    
	@Override
	public double[] getInitialValue(GridCell g, int q){
		Random random = new Random();
		double[] res = new double[q];
		double x = variable(g.getX(), g.getY());
		
		double pressure = getPressure(x);
		double rho = getRho(x);
		
		double checkerboardAmplitude = 0.0;
		
		/*if ((Math.abs(g.i() - ((GridCartesian) grid).nx()/2) < ((GridCartesian) grid).nx()/4) && 
				(Math.abs(g.j() - ((GridCartesian) grid).ny()/2) < ((GridCartesian) grid).ny()/4)){
			pressure += checkerboardAmplitude*Math.pow(-1.0, g.i()+g.j());
			rho      -= checkerboardAmplitude*Math.pow(-1.0, g.i()+g.j());
		}//*/
		
		res[EquationsHydroIdeal.INDEX_RHO] = rho;
		res[EquationsHydroIdeal.INDEX_ENERGY] = getEnergy(pressure);
    		
		
		//res[EquationsHydroIdeal.INDEX_XMOM] =  1e-10*Math.pow(-1.0, i+j);
		//res[EquationsHydroIdeal.INDEX_YMOM] = -1e-10*Math.pow(-1.0, i+j);
		
		/*if ((i==grid.indexMinX()) && (j==grid.indexMinY())) {
			res[EquationsHydroIdeal.INDEX_XMOM] = 1e-10;
			//res[EquationsHydroIdeal.INDEX_YMOM] = 1.5e-10;
			
		}*/
		
		
		//if ((randomPerturbation) && (i == grid.indexMinX()) && (j == grid.indexMinY())){
		if (randomPerturbation){
			res[EquationsHydroIdeal.INDEX_XMOM] = (random.nextDouble()-0.5)*1e-10;
		} else {
			res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		}
		res[EquationsHydroIdeal.INDEX_XMOM] += res[EquationsHydroIdeal.INDEX_RHO]*xVelocity;
		res[EquationsHydroIdeal.INDEX_YMOM] = 0.0; //*/
		
		/*if ((Math.abs(g.i() - ((GridCartesian) grid).nx()/2) < ((GridCartesian) grid).nx()/4) && 
				(Math.abs(g.j() - ((GridCartesian) grid).ny()/2) < ((GridCartesian) grid).ny()/4)){
			res[EquationsHydroIdeal.INDEX_YMOM] += checkerboardAmplitude*Math.pow(-1.0, g.i()+g.j());
			res[EquationsHydroIdeal.INDEX_XMOM] -= checkerboardAmplitude*Math.pow(-1.0, g.i()+g.j());
		}//*/
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    		
		if (equations.getNumberOfConservedQuantities() > 5){
			res[EquationsHydroIdealWith1dDeriv.INDEX_RHO_DERIV] = getRhoDeriv(x);
			res[EquationsHydroIdealWith1dDeriv.INDEX_ENERGY_DERIV] = getEnergyDeriv(x);
	    				
			res[EquationsHydroIdealWith1dDeriv.INDEX_XMOM_DERIV] = 0.0;
			res[EquationsHydroIdealWith1dDeriv.INDEX_YMOM_DERIV] = 0.0;
			res[EquationsHydroIdealWith1dDeriv.INDEX_ZMOM_DERIV] = 0.0;
		}
		return res;
	}

}
