
public abstract class TimeIntegratorRungeKutta extends TimeIntegratorExplicit {

	protected int steps;
	protected double[][][][][] savedQuantities;
	protected TimeIntegratorExplicitConservative myIntegrator;
	
	public TimeIntegratorRungeKutta(Grid grid, double CFL, int steps) {
		super(grid, CFL);
		this.steps = steps;
		// note that savedQuantities[0] is the conservedQuantities as they were before the integration
		savedQuantities = new double[steps+1][][][][];
		myIntegrator = new TimeIntegratorExplicitConservative(grid, CFL);
	}

	@Override
	public int steps() {
		return steps;
	}

	protected abstract double RKCoefficient(int step, int n);
	protected abstract double RKOnestepCoefficient(int step);
	
	
	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities, 
			boolean[][][] excludeFromTimeIntegration) {
		
		int i, j, k;
		
		if (step == 1){
			savedQuantities[0] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];

			for (GridCell g : grid){
				i = g.i(); j = g.j(); k = g.k();
				
				if (!excludeFromTimeIntegration[i][j][k]){				
					for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
						savedQuantities[0][i][j][k][q] = conservedQuantities[i][j][k][q];
					}
					
				}
			}
		}

		double[][][][] oneStepQuantities = myIntegrator.perform(step, dt, interfaceFluxes, sources, conservedQuantities, excludeFromTimeIntegration);
		
		
		savedQuantities[step] = new double[conservedQuantities.length][conservedQuantities[0].length][conservedQuantities[0][0].length][conservedQuantities[0][0][0].length];

		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				savedQuantities[step][i][j][k][q] = 0.0;
				for (int s = 0; s <= step-2; s++){
					savedQuantities[step][i][j][k][q] +=
							savedQuantities[s][i][j][k][q]*RKCoefficient(step, s);
				}
				savedQuantities[step][i][j][k][q] += oneStepQuantities[i][j][k][q]*
						RKOnestepCoefficient(step);
				
			}
		}
		
		return savedQuantities[step];
	}

	public String getDetailedDescription(){
    	return super.getDetailedDescription() + " (" + steps + "-step RK)";
    }
}
