import matrices.SquareMatrix;

public class FluxSolverDimSplitLaxWendroffRichtmyer extends FluxSolverDimSplitEdgeNeighboursOnly {

	public FluxSolverDimSplitLaxWendroffRichtmyer(Grid grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		int quantities = equations.getNumberOfConservedQuantities();
		double diff[];
		double[] upwindingTerm = new double[quantities];
		double[] fluxRes;
		boolean detailedOutput = false;
		GridCell g = gridEdge.adjacent1();
		
		double[] leftF = equations.fluxFunction(g.i(), g.j(), g.k(), left, GridCartesian.X_DIR);
		double[] rightF = equations.fluxFunction(g.i(), g.j(), g.k(), right, GridCartesian.X_DIR);
		
		return null;
		
		// TODO CONTINUE IMPLEMENTATION
		/*fluxRes = new double[quantities];
		for (int q = 0; q < quantities; q++){
			fluxRes[q] = 0.5 * () - 0.5 * (leftF[q] + rightF[q]);
	    	}
	    	return res;
	    	fluxRes[q] = central[q] - upwindingTerm[q];
    		if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("fluxes are NaN");
    	    	detailedOutput = true;
    	    }
    	}
		
		
		
		diff = getStateDifference(gridEdge, right, left);
		
		
		upwindingTerm = upwindingMatrix.mult(0.5).mult(diff);
	    		
		double[] central = averageFlux(g.i(), g.j(), g.k(), left, right, GridCartesian.X_DIR);

		
    	
    	for (int q = 0; q < quantities; q++){
    		fluxRes[q] = central[q] - upwindingTerm[q];
    		if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("fluxes are NaN");
    	    	detailedOutput = true;
    	    }
    	} //*/
		
    		
    	/*if (detailedOutput){
        	for (int q = 0; q < quantities; q++){
        	    System.err.println("left: " + left[q] + " right: " + right[q]
        	    					+ " res: " + fluxRes[q] + " central: " + central[q] 
        	    					+ " upwinding: " + upwindingTerm[q]);
        	    
        	}
    	}
    	
    	
    	
    		return new FluxAndWaveSpeed(fluxRes, tmp.waveSpeed()); //*/
    	//}
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right, int direction) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDetailedDescription() {
		return "Dimensionally split implementation of the two-step Richtmyer Lax.Wendroff method";
	}

}
