
public class GeometricGridEdge {

	protected CornerPoint point1, point2;
	protected double[] unitNormalVector;
	protected double edgeSize; 
	protected GeometricGridCell adjacentGridCell;
	protected GeometricGridEdge edgeInOtherGridCell;
	
	public GeometricGridEdge(CornerPoint p1, CornerPoint p2) {
		point1 = p1; point2 = p2;
		
		edgeSize = Math.sqrt((p1.x() - p2.x())*(p1.x() - p2.x()) + (p1.y() - p2.y())*(p1.y() - p2.y()));
		this.unitNormalVector = new double[] { -(p1.y() - p2.y())/edgeSize, (p1.x() - p2.x())/edgeSize, 0 };
	}
	
	public void adjustNormal(GeometricGridEdge adjacentEdge){
		CornerPoint commonPoint = null;
		CornerPoint otherPreviousPoint = null;
		CornerPoint myOtherPoint = null;
		
		if (point1 == adjacentEdge.point1()){ commonPoint = point1; myOtherPoint = point2; otherPreviousPoint = adjacentEdge.point2(); }
		if (point1 == adjacentEdge.point2()){ commonPoint = point1; myOtherPoint = point2; otherPreviousPoint = adjacentEdge.point1(); }
		if (point2 == adjacentEdge.point1()){ commonPoint = point2; myOtherPoint = point1; otherPreviousPoint = adjacentEdge.point2(); }
		if (point2 == adjacentEdge.point2()){ commonPoint = point2; myOtherPoint = point1; otherPreviousPoint = adjacentEdge.point1(); }
		// we assume here that such a point exists!!
		 
		double originalSenseOfRotation = crossProduct(new double[] {otherPreviousPoint.x() - commonPoint.x(),
																	otherPreviousPoint.y() - commonPoint.y()} ,
										adjacentEdge.unitNormalVector());
		double newSenseOfRotation = crossProduct(new double[] {myOtherPoint.x() - commonPoint.x(),
															   myOtherPoint.y() - commonPoint.y()} ,
										unitNormalVector());
		if (originalSenseOfRotation*newSenseOfRotation > 0){
			reverseNormalVector();
		}
		
	}
	
	protected double crossProduct(double[] a, double[] b){
		return a[0]*b[1] - a[1]*b[0];
	}
	
	public double[] midpoint(){
		return new double[] {(point1.x() + point2.x())/2, (point1.y() + point2.y())/2, 0};
	}
	
	public CornerPoint point1(){             return point1; }
	public CornerPoint point2(){             return point2; }
	public double[]    unitNormalVector(){   return unitNormalVector; }
	public double      edgeSize(){           return edgeSize; }

	public GeometricGridCell getCell(){	                                     return adjacentGridCell; }
	public GeometricGridCell getOtherGeometricGridCell(GeometricGridCell g){ return edgeInOtherGridCell.getCell(); }
	
	public boolean adjoins(CornerPoint p){ return ((p == point1) || (p == point2)); }
	public boolean hasNeighbour(){         return (edgeInOtherGridCell != null); }

	public void setDuplicateGridEdge(GeometricGridEdge edgeInOtherGridCell){ this.edgeInOtherGridCell = edgeInOtherGridCell; }
	public void setCell(GeometricGridCell cell){                             this.adjacentGridCell = cell; }
	
	public void reverseNormalVector(){
		unitNormalVector[0] *= -1.0;
		unitNormalVector[1] *= -1.0;
		unitNormalVector[2] *= -1.0;
	}
	
	public void output(){
		double x, y;
		for (int s = 0; s <= 20; s++){
			x = point1().x() + 1.0*s/20*(point2().x() - point1().x());
			y = point1().y() + 1.0*s/20*(point2().y() - point1().y());
			
			
			System.out.println(x + " " + y);
		}
	}
}
