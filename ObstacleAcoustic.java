
public abstract class ObstacleAcoustic extends Obstacle {

	public ObstacleAcoustic(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	public double[][][][][] insertRigidWalls(double[][][][][] fluxes,
			double[][][][] conservedQuantities) {
		return fluxes;
	}

	@Override
	public void set(double[][][][] conservedQuantities) {
		int i, j, k;
		
		for (i = 0; i < conservedQuantities.length; i++){
			for (j = 0; j < conservedQuantities[i].length; j++){
				for (k = 0; k < conservedQuantities[i][j].length; k++){
					if (excludeFromTimeEvolution[i][j][k]){
						
						conservedQuantities[i][j][k][EquationsAcousticSimple.INDEX_VX] = 0.0;
						conservedQuantities[i][j][k][EquationsAcousticSimple.INDEX_VY] = 0.0;
						conservedQuantities[i][j][k][EquationsAcousticSimple.INDEX_VZ] = 0.0;
						
						conservedQuantities[i][j][k][EquationsAcousticSimple.INDEX_P] = 1.0;
						
						for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
							conservedQuantities[i][j][k][s] = 0;
						}
					}
				}
			}
		}
	}

}
