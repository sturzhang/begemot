import matrices.*;


public abstract class EquationsScalar extends Equations {

	public EquationsScalar(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}

	@Override
	public int getNumberOfConservedQuantities() {
		return 1;
	}

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		return new double[] {quantities[0]};
	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		return new double[] {quantities[0]};
	}
	
	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		return new double[] {flux(i, j, k, quantities[0], direction)};
	}
	
	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction) {
		SquareMatrix mat[] = new SquareMatrix[3];
		mat[0] = SquareMatrix.id(1);
		mat[2] = SquareMatrix.id(1);
		mat[1] = new DiagonalMatrix(new double[]{jacobian(i, j, k, quantities[0], direction)});
		return mat;
	}

	
	public abstract double flux(int i, int j, int k, double quantity, int direction);
	public abstract double jacobian(int i, int j, int k, double quantity, int direction);
	
	public double RankineHugoniotSpeed(int i, int j, int k, double left, double right, int direction){
		if (right == left){
			return 0.0;
		} else {
			return (flux(i, j, k, right, direction) - flux(i, j, k, left, direction)) / (right - left);
		}
	}
}
