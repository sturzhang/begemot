
public class FluxSolverRusanovHydroGravity extends FluxSolverRusanov {

	protected double gy;
	
	public FluxSolverRusanovHydroGravity(Grid grid, Equations equations, double gy) {
		super(grid, equations);
		this.gy = gy;
	}

	@Override
	protected double[] getStateDifference(GridEdgeMapped gridEdge, double[] right, double[] left){
		double[] diff = new double[left.length];
		for (int q = 0; q < left.length; q++){ diff[q] = right[q] - left[q]; }
		
		if (gridEdge.normalVector()[0] == 0.0){
			diff[EquationsHydroIdeal.INDEX_ENERGY] = 
				right[EquationsHydroIdeal.INDEX_ENERGY] - left[EquationsHydroIdeal.INDEX_ENERGY] 
			  - 0.5 * gy*gridEdge.normalVector()[1] * (right[EquationsAcousticExtended.INDEX_RHO] + left[EquationsAcousticExtended.INDEX_RHO]) / 
					  (((EquationsHydroIdeal) equations).gamma() - 1);
		}
		return diff;
	}
}
