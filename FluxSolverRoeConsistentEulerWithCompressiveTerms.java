
public class FluxSolverRoeConsistentEulerWithCompressiveTerms extends FluxSolverLagrangeProjectionTypeCartesian {

	protected boolean useMultidOperators = true;
	protected FluxSolverRoeConsistentViaPseudoInverse baseSolver;
	
	public FluxSolverRoeConsistentEulerWithCompressiveTerms(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
		baseSolver = new FluxSolverRoeConsistentViaPseudoInverse(grid, equations);
	}

	
	@Override
	public String getDetailedDescription() {
		return "Central flux + stationarity-consistent diffusion (derivative of fluxes times sign of Jac), taking compressive terms into account by an extra denominator, as customary in LagProj methods";
	}


	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		
		GridCell c = gridEdge.adjacent1();
		int i = c.i(), j = c.j(), k = c.k();
		
		HydroState left = new HydroState(ijk, EquationsHydroIdeal.getIndexDirection(GridCartesian.X_DIR), (EquationsHydroIdeal) equations);
		HydroState right = new HydroState(ip1jk, EquationsHydroIdeal.getIndexDirection(GridCartesian.X_DIR), (EquationsHydroIdeal) equations);
		
		//double localWaveSpeed = left.soundSpeed + Math.sqrt(left.v2);
		//double localWaveSpeed = left.soundSpeed*Math.sqrt(2.0/((EquationsHydroIdeal) equations).gamma()) + Math.abs(left.vX) + Math.abs(left.vY);
		
		
		double dx = ((GridCartesian) grid).xSpacing();
		double dy = ((GridCartesian) grid).ySpacing();
		
		//double advectionSpeedX = (right.v + left.v)/2;
		//double absU = Math.abs(advectionSpeedX);
		
		
		/*int nCons = equations.getNumberOfConservedQuantities();
		double[][] fluxRes = new double[1][nCons];
				
		double[] fLeft = new double[nCons], fRight = new double[nCons];
		double[] fLeftTop = new double[nCons], fLeftBot = new double[nCons], fRightTop = new double[nCons], fRightBot = new double[nCons];
		double[] fLeftBack = new double[nCons], fRightBack = new double[nCons];
		double[] fLeftTopBack = new double[nCons], fLeftBotBack = new double[nCons], fRightTopBack = new double[nCons], fRightBotBack = new double[nCons];
		double[] fLeftFront = new double[nCons], fRightFront = new double[nCons];
		double[] fLeftTopFront = new double[nCons], fLeftBotFront = new double[nCons], fRightTopFront = new double[nCons], fRightBotFront = new double[nCons];
		
	    fLeft  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijk, GridCartesian.X_DIR);
	    fRight = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jk, GridCartesian.X_DIR);
	    fLeftTop  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijp1k, GridCartesian.X_DIR);
	    fRightTop = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jp1k, GridCartesian.X_DIR);
	    fLeftBot  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijm1k, GridCartesian.X_DIR);
	    fRightBot = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jm1k, GridCartesian.X_DIR);

	    fLeftBack  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijkm1, GridCartesian.X_DIR);
	    fRightBack = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jkm1, GridCartesian.X_DIR);
	    fLeftTopBack  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijp1km1, GridCartesian.X_DIR);
	    fRightTopBack = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jp1km1, GridCartesian.X_DIR);
	    fLeftBotBack  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijm1km1, GridCartesian.X_DIR);
	    fRightBotBack = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jm1km1, GridCartesian.X_DIR);

	    fLeftFront  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijkp1, GridCartesian.X_DIR);
	    fRightFront = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jkp1, GridCartesian.X_DIR);
	    fLeftTopFront  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijp1kp1, GridCartesian.X_DIR);
	    fRightTopFront = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jp1kp1, GridCartesian.X_DIR);
	    fLeftBotFront  = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ijm1kp1, GridCartesian.X_DIR);
	    fRightBotFront = ((EquationsHydroIdeal) equations).fluxFunction(i, j, k, ip1jm1kp1, GridCartesian.X_DIR);

		for (int q = 0; q < nCons; q++){
			if (useMultidOperators){
				fluxRes[0][q] = 0.5*(
					  (fLeftTopBack[q] + fRightTopBack[q] + 2.0*(fLeftBack[q] + fRightBack[q]) + fLeftBotBack[q] + fRightBotBack[q])/4
					  +2.0*(fLeftTop[q] + fRightTop[q] + 2.0*(fLeft[q] + fRight[q]) + fLeftBot[q] + fRightBot[q])/4
					  +(fLeftTopFront[q] + fRightTopFront[q] + 2.0*(fLeftFront[q] + fRightFront[q]) + fLeftBotFront[q] + fRightBotFront[q])/4)/4;
			} else {
				fluxRes[0][q] = 0.5*(fLeft[q] + fRight[q]);
			}
			fluxRes[0][q] -= 0.5*absU*(ip1jk[q] - ijk[q]); 
					
    	    if (Double.isNaN(fluxRes[0][q])){
    	    	System.err.println("?");
    	    }
    	}
		//*/
	    
	    double divergence;
	    if (useMultidOperators) {
	    	divergence = (velAvg(ip1jp1kp1,ip1jkp1,ip1jm1kp1, EquationsHydroIdeal.INDEX_XMOM) - velAvg(ijp1kp1,ijkp1,ijm1kp1, EquationsHydroIdeal.INDEX_XMOM)
				  + 2.0*(velAvg(ip1jp1k,ip1jk,ip1jm1k, EquationsHydroIdeal.INDEX_XMOM) - velAvg(ijp1k,ijk,ijm1k, EquationsHydroIdeal.INDEX_XMOM))
				  + velAvg(ip1jp1km1,ip1jkm1,ip1jm1km1, EquationsHydroIdeal.INDEX_XMOM) - velAvg(ijp1km1,ijkm1,ijm1km1, EquationsHydroIdeal.INDEX_XMOM))/4/4/dx
			+
			(velAvg(ip1jp1kp1,ip1jp1k,ip1jp1km1,EquationsHydroIdeal.INDEX_YMOM) + velAvg(ijp1kp1,ijp1k,ijp1km1,EquationsHydroIdeal.INDEX_YMOM)
			 - velAvg(ip1jm1kp1,ip1jm1k,ip1jm1km1,EquationsHydroIdeal.INDEX_YMOM) - velAvg(ijm1kp1,ijm1k,ijm1km1,EquationsHydroIdeal.INDEX_YMOM))/4/4/dy;
	    } else {
	    	divergence = (ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO] - ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO])/dx +
	    			(ip1jp1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO] - ip1jm1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO]
	    			 +ijp1k[EquationsHydroIdeal.INDEX_YMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO] - ijm1k[EquationsHydroIdeal.INDEX_YMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO])/4/dy;
	    }
	    
	    
		if (grid.dimensions() > 2){
			double dz = ((GridCartesian) grid).zSpacing();
			
			divergence += (velAvg(ip1jp1kp1,ip1jkp1,ip1jm1kp1, EquationsHydroIdeal.INDEX_ZMOM) + velAvg(ijp1kp1,ijkp1,ijm1kp1, EquationsHydroIdeal.INDEX_ZMOM)
					- velAvg(ip1jp1km1,ip1jkm1,ip1jm1km1, EquationsHydroIdeal.INDEX_ZMOM) - velAvg(ijp1km1,ijkm1,ijm1km1, EquationsHydroIdeal.INDEX_ZMOM))/4/4/dz;
		}
		
		
		double[][] fluxRes = new double[1][];
		FluxAndWaveSpeed fluxAndWaveSpeed = baseSolver.fluxmultid(gridEdge, 
				ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, 
				ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, 
				ijp1k, ijk, ijm1k, ip1jp1k, ip1jk, ip1jm1k, timeIntegratorStep);
		fluxRes[0] = fluxAndWaveSpeed.flux();
		double localWaveSpeed = fluxAndWaveSpeed.waveSpeed();
		
		return new FluxMultiStepAndWaveSpeed(fluxRes, localWaveSpeed, new double[] {divergence});
	}

	
	protected double velAvg(double[] left, double[] center, double[] right, int MOM){
		return left[MOM]/left[EquationsHydroIdeal.INDEX_RHO] + 2.0 * center[MOM]/center[EquationsHydroIdeal.INDEX_RHO] + right[MOM]/right[EquationsHydroIdeal.INDEX_RHO];
	}


}
