
public class ReconstructionPiecewiseConstant extends Reconstruction {

	protected int numberOfGhostCells;
	
	public ReconstructionPiecewiseConstant(){
		numberOfGhostCells = 1;
	}
	
	public ReconstructionPiecewiseConstant(int numberOfGhostCells){
		this.numberOfGhostCells = numberOfGhostCells;
	}
	
	public double[][][][][] perform(double[][][][] conservedQuantities, double[][][][][] interfaceValues) {

		
		// TODO this extra loop for the cartesian case does not set the interfacevalues of the ghostcells further out
		// in general this is not possible but for widestencil solvers we need this, without a particular reconstruction
		// a bit of a dirty workaround needed for FluxSolverMultiStepCartesianAcoustic
		/*if (grid instanceof GridCartesian){
			for (int i = ((GridCartesian) grid).indexMinX()-1; i <= ((GridCartesian) grid).indexMaxX()+1; i++){
				for (int j = Math.max(((GridCartesian) grid).indexMinY()-1, 0); j <= Math.min(((GridCartesian) grid).indexMaxY()+1, ((GridCartesian) grid).lastGhostCellYIndex()); j++){
					// these special loop boundaries are needed for the 1-d & 2-d case when z (and y) directions do not have ghostcells
					for (int k = Math.max(((GridCartesian) grid).indexMinZ()-1, 0); k <= Math.min(((GridCartesian) grid).indexMaxZ()+1, ((GridCartesian) grid).lastGhostCellZIndex()); k++){
						
						for (int interf = 0; interf < interfaceValues[i][j][k].length; interf++){
							for (int q = 0; q < interfaceValues[i][j][k][interf].length; q++){	
								interfaceValues[i][j][k][interf][q] = conservedQuantities[i][j][k][q]; 
							}
						}
						
	    			}
	    		}
		   	}
		} else {//*/
			for (int i = 0; i < conservedQuantities.length; i++){
				for (int j = 0; j < conservedQuantities[0].length; j++){
					for (int k = 0; k < conservedQuantities[0][0].length; k++){
						
						for (int interf = 0; interf < interfaceValues[i][j][k].length; interf++){
							for (int q = 0; q < interfaceValues[i][j][k][interf].length; q++){	
								interfaceValues[i][j][k][interf][q] = conservedQuantities[i][j][k][q]; 
							}
						}
						
	    			}
	    		}
		   	}
			
		//}
			
			return interfaceValues;
	}

	public int numberOfGhostcells() {	return numberOfGhostCells;	}

    public String getDetailedDescription(){
    	return "Piecewise constant";
    }

}
