
public abstract class InitialDataMaxwell extends InitialData {

	protected EquationsMaxwell equations;
	
	public InitialDataMaxwell(Grid grid, EquationsMaxwell equations) {
		super(grid);
		this.equations = equations;
	}

	
}
