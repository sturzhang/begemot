
public class BoundaryZeroGradientWithBottom extends Boundary {

	protected double[] quantitiesAtBottom;
	
	public BoundaryZeroGradientWithBottom(GridCartesian grid, double[] quantitiesAtBottom) {
		super(grid);
		this.quantitiesAtBottom = quantitiesAtBottom;
	}

    public String getDetailedDescription(){
    	String res = "";
    	for (int i = 0; i < quantitiesAtBottom.length; i++){
    		res += (i == 0 ? "" : ", ") + quantitiesAtBottom[i];
    	}
    	return "Fixed to " + res + "at bottom and zero-gradient (outflow) everywhere else";
    }

	
    @Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k;
		int nextI, nextJ, nextK;
		
		i = g.i(); j = g.j(); k = g.k();
		
		if (j < ((GridCartesian) grid).indexMinY()){
			for (int q = 0; q < Math.min(conservedQuantities[i][j][k].length, quantitiesAtBottom.length); q++){
				conservedQuantities[i][j][k][q] = quantitiesAtBottom[q];
			}
			
		} else {
			nextI = i; nextJ = j; nextK = k;
			
			if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); }
			if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); }
			if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
			
			if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }
			if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }
			if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }
		
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
			}
			
		}
		
	}

}
