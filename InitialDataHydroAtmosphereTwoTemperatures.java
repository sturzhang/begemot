import java.util.Random;


public class InitialDataHydroAtmosphereTwoTemperatures extends InitialDataHydroAtmosphere {

	private double DeltaT = 1.0;
	private double w = 0.02;
	
	public InitialDataHydroAtmosphereTwoTemperatures(GridCartesian grid,
			EquationsHydroIdeal equations, double gy, double xVelocity) {
		super(grid, equations, gy, xVelocity, true);
	}

	@Override
	public String getDetailedDescription() {
    	return "Static atmosphere with a perfect gas with a transition between two temperatures of width delta T = " + 
    			DeltaT + " and delta y = " + w;
	}
 
	@Override
	public double getRho(double y) {
		return getPressure(y) / getTemperature(y);
	}

	public double getTemperature(double y){
		Random rnd = new Random();
		return 1.0 + DeltaT * sinh((y-0.5)/w) / cosh((y-0.5)/w);
	}
	
	@Override
	public double getPressure(double y) {
		return Math.exp( 
				(y -0.5 - DeltaT*w*Math.log(cosh((y-0.5)/w) + DeltaT*sinh((y-0.5)/w))) 
				/ 
				(1.0 - DeltaT*DeltaT)*gy
				); 
	}
	
	private double cosh(double x){ return (Math.exp(x) + Math.exp(-x))/2.0; }
	private double sinh(double x){ return (Math.exp(x) - Math.exp(-x))/2.0; }

	@Override
	public double getRhoDeriv(double y) {
		return (getTemperature(y)*getPressureDeriv(y) - getPressure(y)*getTemperatureDeriv(y)) 
				/ getTemperature(y) / getTemperature(y);
	}

	@Override
	public double getPressureDeriv(double y) {
		double eps = 1e-6;
		return (getPressure(y+eps) - getPressure(y-eps))/2/eps;
	}
	
	public double getTemperatureDeriv(double y){
		double eps = 1e-6;
		return (getTemperature(y+eps) - getTemperature(y-eps))/2/eps;
	}
	
	

}
