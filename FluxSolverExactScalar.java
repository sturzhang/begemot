
public class FluxSolverExactScalar extends FluxSolverDimSplitEdgeNeighboursOnly {

	public FluxSolverExactScalar(GridCartesian grid, EquationsScalar equations) {
		super(grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "Exact Riemann solver for (nonlinear) scalar conservation laws (without entropy fix).";
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		GridCell g = gridEdge.adjacent1();
		
		// TODO this won't work for multi-d, n'est-ce pas?
		int direction = GridCartesian.X_DIR;
		
		double speed = ((EquationsScalar) equations).RankineHugoniotSpeed(g.i(), g.j(), g.k(), left[0], right[0], direction);
		
		if (speed > 0){
			return new FluxAndWaveSpeed( new double[] {((EquationsScalar) equations).flux(g.i(), g.j(), g.k(), left[0], direction)}, speed);
		} else {
			return new FluxAndWaveSpeed( new double[] {((EquationsScalar) equations).flux(g.i(), g.j(), g.k(), right[0], direction)}, speed);
		}
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right,
			int direction) {
		// TODO define here an instance of the advection equation and solve it with some solver
		
		return new double[equations.getNumberOfPassiveScalars()];
	}

	

}
