
public abstract class FluxSolverFromConservativeOnly extends FluxSolver {

	protected StencilTurner stencilTurner;
	
	public FluxSolverFromConservativeOnly(Grid grid, Equations equations, StencilTurner stencilTurner) {
		super(grid, equations);
		this.stencilTurner = stencilTurner;
	}

	protected abstract FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge, double[][] quantities, int timeIntegratorStep);

	// TODODT protected abstract double waveSpeed(double[] quantities);

	/*@Override
	public void setWaveSpeeds(double[][][][][] interfaceValues, 
			double[][][][][][] interfaceFluxes, double[][][][] localWaveSpeeds, double[][][][][] additionalInterfaceQuantities){
    	for (GridCell g : grid){
    		for (int direction = 0; direction < grid.dimensions(); direction++){
    			// TODODT chooseCurMaxSignalSpeed(waveSpeed(interfaceValues[g.i()][g.j()][g.k()][direction]));
    		}
    	}
	}//*/
	
	@Override
	public double[][][][][][] getAndSetWaveSpeeds(double[][][][][] interfaceValues, 
			double[][][][][][] interfaceFluxes, double[][][][] localWaveSpeeds, int timeIntegratorStep, double[][][][][] additionalInterfaceQuantities){
		FluxMultiStepAndWaveSpeed fluxAndWaveSpeed = new FluxAndWaveSpeed(null, 0); // to make compiler happy
    	double[] outwardNormalVector;
    	GridEdgeMapped gridEdge;
    	double[][] fluxScalar = new double[grid.dimensions()][];
    	double[][] projectedFlux = new double[0][0];
    	double[] projectedFluxScalar = new double[0]; // to make compiler happy
    	double[][] resFlux;
    	boolean[][][] gridCellVisited = new boolean[interfaceValues.length][interfaceValues[0].length][interfaceValues[0][0].length];
    	int freeIndex;
    	GridEdgeMapped[] allEdges;
    	GridCell nb;
    	GridCell[] stencil;
    	double[][] quantities;
    	
    	for (int i = 0; i < gridCellVisited.length; i++){
    		for (int j = 0; j < gridCellVisited[0].length; j++){
    			for (int k = 0; k < gridCellVisited[0][0].length; k++){
    				gridCellVisited[i][j][k] = false;
    			}}}
    	
    	
    	for (GridCell g : grid){
    		gridCellVisited[g.i()][g.j()][g.k()] = true;
    	
    		
    		allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
    		for (int edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++){
    			gridEdge = allEdges[edgeIndex];
    			nb = gridEdge.getOtherGridCell(g);
    			if (!gridCellVisited[nb.i()][nb.j()][nb.k()]){
    				stencil = stencilTurner.getStencil(g, gridEdge);
    				outwardNormalVector = gridEdge.normalVector(); 
	    			// points from g to nb, i.e. outward normal!
	    			
    				/*if (equations instanceof EquationsAdvection){
		    			for (int dir = 0; dir < grid.dimensions(); dir++){
		    				if (outwardNormalVector[dir] > 0){
		    					fluxAndWaveSpeed[dir] = flux(g, gridEdge, g, nb, dir);
		    					fluxScalar[dir] = fluxPassiveScalar(g, gridEdge, g, nb, dir);
		    				} else {
		    					fluxAndWaveSpeed[dir] = flux(g, gridEdge, nb, g, dir);
		    					fluxScalar[dir] = fluxPassiveScalar(g, gridEdge, nb, g, dir);
		    				}
		    				
		    				if (dir == 0){
		    					projectedFlux       = FluxAndWaveSpeed.multiply(fluxAndWaveSpeed[dir].flux(), outwardNormalVector[dir]);
		    					projectedFluxScalar = FluxAndWaveSpeed.multiply(fluxScalar[dir],              outwardNormalVector[dir]);
		    				} else {
		    					projectedFlux       = FluxAndWaveSpeed.add(projectedFlux, 
		    											FluxAndWaveSpeed.multiply(fluxAndWaveSpeed[dir].flux(), outwardNormalVector[dir]));
		    					projectedFluxScalar = FluxAndWaveSpeed.add(projectedFluxScalar, 
										FluxAndWaveSpeed.multiply(fluxScalar[dir],                              outwardNormalVector[dir]));
		    				}
		    			}
	    			} else {*/
    					
    					quantities = new double[stencil.length][];
    					
    					equations.transformFixedParameters(outwardNormalVector);
    					
    					for (int stencilIndex = 0; stencilIndex < stencil.length; stencilIndex++){
    						quantities[stencilIndex] = stencil[stencilIndex].interfaceValue();
    						quantities[stencilIndex] = equations.getTransform(quantities[stencilIndex], outwardNormalVector);
    					}
    					    				
	    				fluxAndWaveSpeed = flux(gridEdge, quantities, timeIntegratorStep);
	    				
	    				equations.transformFixedParametersBack(outwardNormalVector);
    						    				
	    				for (int fluxStep = 0; fluxStep < steps(); fluxStep++){
	    					fluxAndWaveSpeed.setFlux(equations.getTransformBack(fluxAndWaveSpeed.fluxMultiStep()[fluxStep], 
	    							outwardNormalVector), fluxStep);
	    				}
	    				
	    				projectedFlux = fluxAndWaveSpeed.fluxMultiStep();
	    				
	    				// TODO what to do with scalar flux?
	    				/*for (int dir = 0; dir < grid.dimensions(); dir++){
	    					if (outwardNormalVector[dir] > 0){
		    					fluxScalar[dir] = fluxPassiveScalar(g, gridEdge, g, nb, dir);
		    				} else {
		    					fluxScalar[dir] = fluxPassiveScalar(g, gridEdge, nb, g, dir);
		    				}
		    				
		    				if (dir == 0){
		    					projectedFluxScalar = FluxAndWaveSpeed.multiply(fluxScalar[dir], outwardNormalVector[dir]);
		    				} else {
		    					projectedFluxScalar = FluxAndWaveSpeed.add(projectedFluxScalar, 
										FluxAndWaveSpeed.multiply(fluxScalar[dir],               outwardNormalVector[dir]));
		    				}
		    			}*/
	    			
	    			
	    			
	    			
	    			
	    			//resFlux = combine(projectedFlux, projectedFluxScalar);
	    			// TODO
	    			resFlux = projectedFlux;
	    				
	    			freeIndex = edgeIndex;
	    			
	    			for (int fluxStep = 0; fluxStep < steps(); fluxStep++){
	    				interfaceFluxes[fluxStep][g.i()][g.j()][g.k()][freeIndex]    = 
	    						FluxAndWaveSpeed.multiply(resFlux[fluxStep],  gridEdge.sizeModified());
	    			}
	    			
	    			
	    			if (fluxAndWaveSpeed.additionalQuantities() != null) {
	    				additionalInterfaceQuantities[g.i()][g.j()][g.k()][freeIndex] = new double[fluxAndWaveSpeed.additionalQuantities().length];	
	    				for (int counter = 0; counter < fluxAndWaveSpeed.additionalQuantities().length; counter++) {
	    					additionalInterfaceQuantities[g.i()][g.j()][g.k()][freeIndex][counter] = fluxAndWaveSpeed.additionalQuantities()[counter];
	    				}
	    				// additionalInterfaceQuantities[g.i()][g.j()][g.k()][freeIndex] = fluxAndWaveSpeed.additionalQuantities();
	    			}	
	    			
	    			
	    			
	    			// TODO how to take into account the wave speeds correctly?
	    			localWaveSpeeds[g.i()][g.j()][g.k()][freeIndex] = fluxAndWaveSpeed.waveSpeed();
	    			
	    			if (gridEdge.counterpart() != null){
						freeIndex = gridEdge.counterpart().index();
						for (int fluxStep = 0; fluxStep < steps(); fluxStep++){
			    			interfaceFluxes[fluxStep][nb.i()][nb.j()][nb.k()][freeIndex] = 
			   					FluxAndWaveSpeed.multiply(resFlux[fluxStep], -gridEdge.sizeModified());
						}
		    			// TODO how to take into account the wave speeds correctly?
		    			localWaveSpeeds[nb.i()][nb.j()][nb.k()][freeIndex] = fluxAndWaveSpeed.waveSpeed();
		    			
		    			if (fluxAndWaveSpeed.additionalQuantities() != null) {
		    				additionalInterfaceQuantities[nb.i()][nb.j()][nb.k()][freeIndex] = new double[fluxAndWaveSpeed.additionalQuantities().length];
		    				for (int counter = 0; counter < fluxAndWaveSpeed.additionalQuantities().length; counter++) {
		    					additionalInterfaceQuantities[nb.i()][nb.j()][nb.k()][freeIndex][counter] = fluxAndWaveSpeed.additionalQuantities()[counter];
		    				}
		    			
		    				//additionalInterfaceQuantities[nb.i()][nb.j()][nb.k()][freeIndex] = fluxAndWaveSpeed.additionalQuantities();
		    			}
	    			}	    			
	    			
	    			// TODODT THESE THINGS NOW HAPPEN IN setWaveSpeeds
	    			if (fluxAndWaveSpeed.waveSpeed() == Double.NaN){
	    				System.err.println("ERROR: NaN");
	    			}
					chooseCurMaxSignalSpeed( fluxAndWaveSpeed.waveSpeed() );
					// UNTIL HERE
					
					
					// TODO
					//if (grid.dimensions() >= 2) {chooseCurMaxSignalSpeed( fluxAndWaveSpeed[1].waveSpeed() ); }
					//if (grid.dimensions() >= 3) {chooseCurMaxSignalSpeed( fluxAndWaveSpeed[2].waveSpeed() ); }
    			}	
    		}
    		//}
    		
    	}
    	
    	return interfaceFluxes;
    }
}
