
public class TimeIntegratorRungeKutta3 extends TimeIntegratorRungeKutta {

	protected double[][] RKCoefficient = new double[4][2];
	protected double[] RKOnestepCoefficient = new double[] {0.0, 1.0, 0.25, 2.0/3};
	
	public TimeIntegratorRungeKutta3(Grid grid, double CFL) {
		super(grid, CFL, 3);
		
		RKCoefficient[2][0] = 0.75;
		
		RKCoefficient[3][0] = 1.0/3;
		RKCoefficient[3][1] = 0.0;
	}

	@Override
	protected double RKCoefficient(int step, int n) { return RKCoefficient[step][n]; }

	@Override
	protected double RKOnestepCoefficient(int step) { return RKOnestepCoefficient[step]; }

}
