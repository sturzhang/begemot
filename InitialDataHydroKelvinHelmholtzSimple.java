
public class InitialDataHydroKelvinHelmholtzSimple extends InitialDataHydro {

	protected double vMagnitude, pressure, perpendicularVelocityPerturbation;
	protected double rhoTop, rhoBottom;
	
	public InitialDataHydroKelvinHelmholtzSimple(Grid grid,
			EquationsHydroIdeal equations, double vMagnitude,
			double pressure, double perpendicularVelocityPerturbation, double rhoTop, double rhoBottom) {
		super(grid, equations);
		this.vMagnitude = vMagnitude;
		this.pressure = pressure;
		this.perpendicularVelocityPerturbation = perpendicularVelocityPerturbation;
		this.rhoTop = rhoTop;
		this.rhoBottom = rhoBottom;
	}
	
	
	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		
		
		double rho;
		double vy = perpendicularVelocityPerturbation*Math.sin(g.getX()/0.5*Math.PI);
		if (g.getY() < grid.yMidpoint()){
		    rho = rhoBottom;
		    res[EquationsHydroIdeal.INDEX_RHO] = rhoBottom;
		    res[EquationsHydroIdeal.INDEX_XMOM] = rhoBottom*vMagnitude;
		    res[EquationsHydroIdeal.INDEX_YMOM] = rhoBottom*vy;
		    
		    res[EquationsHydroIdeal.INDEX_ENERGY] = 
					((EquationsHydroIdeal) equations).getEnergy(pressure, rhoBottom*(vMagnitude*vMagnitude + vy*vy));
		} else {
		    rho = rhoTop;
		    res[EquationsHydroIdeal.INDEX_RHO] = rhoTop;
		    res[EquationsHydroIdeal.INDEX_XMOM] = -rhoTop*vMagnitude;
		    res[EquationsHydroIdeal.INDEX_YMOM] = -rhoTop*vy;
		    
		    res[EquationsHydroIdeal.INDEX_ENERGY] = 
					((EquationsHydroIdeal) equations).getEnergy(pressure, rhoTop*(vMagnitude*vMagnitude + vy*vy));
		}
		
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
		
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Kelvin Helmholtz instability setup with densities = " + rhoTop + ", " + rhoBottom + " and pressure = " + pressure + 
				" and an initial perturbation in the perpendicular velocity";
	}

}
