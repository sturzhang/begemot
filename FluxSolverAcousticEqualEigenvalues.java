
public class FluxSolverAcousticEqualEigenvalues extends FluxSolverDimSplitEdgeNeighboursOnly {

	public FluxSolverAcousticEqualEigenvalues(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	public String getDetailedDescription() {
    	return "Upwind solver with a diagonal upwinding matrix with all eigenvalues the same";
	}

	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep) {
		int i = gridEdge.adjacent1().i(); int j = gridEdge.adjacent1().j();  int k = gridEdge.adjacent1().k();  
		EquationsAcousticSimple myEqn = (EquationsAcousticSimple) equations;
		double vAbsMax = Math.max(Math.abs(myEqn.vx()), Math.abs(myEqn.vy()));
		
		double localWaveSpeed = myEqn.soundspeed() + vAbsMax;
		
		double[] fluxLeft  = myEqn.fluxFunction(i, j, k, left,  GridCartesian.X_DIR);
    	double[] fluxRight = myEqn.fluxFunction(i, j, k, right, GridCartesian.X_DIR);	    	    	
	    		
    	double diff[] = new double[fluxLeft.length];
		double upwindingTerm; 
		double[][] upw = upwindingMatrix(Math.abs(myEqn.vx()), fluxLeft.length);
		for (int q = 0; q < fluxLeft.length; q++){
			diff[q] = right[q] - left[q];
		}
		
		/*double[][] upw = upwindingMatrix(Math.sqrt( Math.pow((left.vX+right.vX)/2, 2) + 
				Math.pow((left.vY+right.vY)/2, 2) + Math.pow((left.vZ+right.vZ)/2, 2)
				), direction); //*/
		
		double[] fluxRes = new double[fluxLeft.length];
		for (int q = 0; q < fluxLeft.length; q++){
			upwindingTerm = 0.0;
			for (int qq = 0; qq < fluxLeft.length; qq++){
				upwindingTerm += upw[q][qq] * diff[qq];
			}
    	    fluxRes[q] = 0.5 * (fluxLeft[q] + fluxRight[q]) - 0.5 * upwindingTerm;
    	    		
    	    if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("?");
    	    }
    	}
		
    	return new FluxAndWaveSpeed(fluxRes, localWaveSpeed);
	}

	@Override
	protected double[] fluxPassiveScalar(int i, int j, int k, double[] left,
			double[] right, int direction) {
		// TODO Auto-generated method stub
		return null;
	}

	protected double[][] upwindingMatrix(double eigenvalue, int size){
		double[][] upw = new double[size][size];
		for (int i = 0; i < size; i++){
			for (int j = 0; j < size; j++){
				upw[i][j] = 0.0;
			}
		}
		
		for (int i = 0; i < size; i++){
			upw[i][i] = eigenvalue;
		}		
		return upw;
	}

}
