
public class InitialDataSinusoidal extends InitialData {

	protected double lambdax, lambday;
	protected double[] amplitude;
	protected double[] offset;
	protected double xDiscontinuity;
	
	public InitialDataSinusoidal(Grid grid, double lambdax, double lambday, double[] amplitude, double[] offset, double xDiscontinuity) {
		super(grid);	
		this.lambdax = lambdax;
		this.lambday = lambday;
		this.amplitude = amplitude;
		this.offset = offset;
		this.xDiscontinuity = xDiscontinuity;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x, y, dx;
    	
    	
    	x = g.getX();
    	//dx = grid.xSpacing();
    	if (grid.dimensions() > 1){
    		y = g.getY();
    	} else {y = 0.0;}
    	
    	
    	//y = y - grid.yMidpoint();
    	
    	for (int q = 0; q < qq; q++){
			res[q] = amplitude[q] * Math.sin(2*Math.PI*x/lambdax + 2*Math.PI*y/lambday) + offset[q];
    	}
    	
    	// 0.0, 0.0, 0.1, 0.0, 0.0}, {1.0, 0.0, 0.0, 0.0, 1.0}
    	/*res[0] = 1.0;
    	res[1] = 0.0;
    	res[2] = 0.15 + (Math.abs(y) > 0.2 ? 0 : -0.1);
    	res[3] = 0.0;
    	res[4] = 1.0;*/
    	
    	//if (Math.abs(x - xDiscontinuity) > 0.2){
    	//for (int q = 0; q < qq; q++){
    			//res[q] = amplitude[q] + offset[q];
    			
    			// function x^5 - 3x, primitive x^6/6 - 3x^2/2
    			/*res[q] = ( Math.pow(x+dx/2,6)/6 - 3.0*Math.pow(x+dx/2, 2)/2 - 
    					(Math.pow(x-dx/2, 6)/6 - 3.0*Math.pow(x-dx/2, 2)/2) ) / dx;*/
    			
    			/*res[q] = (  - 3.0*Math.pow(x+dx/2, 2)/2 - 
    					( - 3.0*Math.pow(x-dx/2, 2)/2) ) / dx;*/
    			
    		//res[q] = amplitude[q] * Math.sin(2*Math.PI*x/lambdax + 2*Math.PI*y/lambday) + offset[q];
    			//res[q] = amplitude[q] * Math.sin(2*Math.PI*x/lambdax) + offset[q];
    		//}
    	//} else {
    	//	for (int q = 0; q < qq; q++){
    	//		res[q] = offset[q];
    	//	}
    	//}			
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Sine function in all quantities with wavelength for x: " + lambdax + " and wavelength for y: " + lambday;
	}

}
