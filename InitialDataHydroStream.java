
public class InitialDataHydroStream extends InitialDataHydro {

	public InitialDataHydroStream(Grid grid, EquationsHydro equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	protected double speed(double x) {
		if (Math.abs(x) > 0.2){ return 0.0; }
		else { return 1.0 - 5.0*Math.abs(x); }
	}
	
	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x, y;
		double rhoV2;
    	
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		double angle = Math.PI / 180 * 5;
		
		double rho = 1.0;
		
		res[EquationsHydroIdeal.INDEX_RHO] = rho;
    				
		res[EquationsHydroIdeal.INDEX_YMOM] = rho * speed(Math.sin(angle)*y - Math.cos(angle)*x) * Math.cos(angle);
		
		res[EquationsHydroIdeal.INDEX_XMOM] = rho * speed(Math.sin(angle)*y - Math.cos(angle)*x) * Math.sin(angle);
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;

		rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    	
		double pressure = 10.0;
		
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    	
    	for (int ii = 5; ii < res.length; ii++){
    		res[ii] = (x < 0 ? 1.0 : 0.0);
    	}
    	
    	return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "Stream directed upward and having the profile of a Gresho vortex";
	}

}
