
public abstract class SourceInCellOnly extends Source {

	protected boolean averaged;
	
	public SourceInCellOnly(Grid grid, Equations equations, boolean averaged) {
		super(grid, equations);
		this.averaged = averaged;
	}

	public SourceInCellOnly(Grid grid, Equations equations) {
		super(grid, equations);
		averaged = false;
	}
	
	
	protected abstract double[] getSourceTerm(double[] quantities, GridCell g, double time);

	
	@Override
	public double[][][][] get(double[][][][] quantities, double[][][][] sources, double time, double dt, int timeIntegratorStep){
    	
    	if (averaged){
    		double[][][][] tmpsources = new double[sources.length][sources[0].length][sources[0][0].length][sources[0][0][0].length];
    		
    		for (int i = 0; i < quantities.length; i++){
        		for (int j = 0; j < quantities[i].length; j++){
        			for (int k = 0; k < quantities[i][j].length; k++){
        				tmpsources[i][j][k] = getSourceTerm(quantities[i][j][k], new GridCell(i,j,k, grid), time);
        			}
        		}
    		}
    		
    		for (GridCell g : grid){
    			for (int q = 0; q < sources[0][0][0].length; q++){
    				/*sources[g.i()][g.j()][g.k()][q] = average(
        					average(tmpsources[g.i()-1][g.j()+1][g.k()][q], tmpsources[g.i()][g.j()+1][g.k()][q], tmpsources[g.i()+1][g.j()+1][g.k()][q]),
        					average(tmpsources[g.i()-1][g.j()  ][g.k()][q], tmpsources[g.i()][g.j()  ][g.k()][q], tmpsources[g.i()+1][g.j()  ][g.k()][q]),
        					average(tmpsources[g.i()-1][g.j()-1][g.k()][q], tmpsources[g.i()][g.j()-1][g.k()][q], tmpsources[g.i()+1][g.j()-1][g.k()][q])    				
        				);//*/
    				
    				sources[g.i()][g.j()][g.k()][q] = 
        					average(tmpsources[g.i()][g.j()+1][g.k()][q], tmpsources[g.i()][g.j()][g.k()][q], tmpsources[g.i()][g.j()-1][g.k()][q]);
    			}
        	}
    	} else {
    	
	    	/*for (int i = grid.indexMinX(); i <= grid.indexMaxX()+1; i++){
	    		for (int j = grid.indexMinY(); j <= Math.min(grid.indexMaxY()+1, grid.lastGhostCellYIndex()); j++){
	    			for (int k = grid.indexMinZ(); k <= Math.min(grid.indexMaxZ()+1, grid.lastGhostCellZIndex()); k++){
	    		// removed during transfer to unstructured grids -- watch out the +1 -- what is its purpose??
	    		*/	
	    		
	    	for (GridCell g : grid){
	    		sources[g.i()][g.j()][g.k()] = getSourceTerm(quantities[g.i()][g.j()][g.k()], g, time);
	    	}
	    	//	}
	    	//}

    	}
    	
    	//System.err.println("source = " + sources[1][4][0][2]);
    	
    	return sources;
    }
	
	

}
