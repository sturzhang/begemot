
public class OutputTextOnly extends Output {

	public OutputTextOnly() {
		super(10000.0, null, null);
	}
 
	@Override
	public String getDetailedDescription() {
		return "Text-only output";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		// nothing
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
