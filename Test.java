

public class Test {

	protected double finaltime;
	protected OutputErrorNorm output;
	
	protected Reconstruction reconstruction;
	protected GridCartesian grid;
	protected Equations equations;
	protected Source source;
	protected Obstacle obstacle;
	protected InitialData initialData;
	protected Boundary boundary;
	protected FluxSolver fluxSolver;
	protected TimeIntegratorFiniteVolume timeIntegrator;
	
	// ObstacleHydro previously here, removed during transition to unstructured grids -> restore when obstacle gets new implementation
	
	protected IterationFiniteVolume iteration;
	
	
	public Test(Reconstruction reconstruction,
    		GridCartesian grid, Equations equations, Source source, Obstacle obstacle, 
    		InitialData initialData, Boundary boundary, FluxSolver fluxSolver,
    		TimeIntegratorFiniteVolume timeIntegrator, OutputErrorNorm output,
    		double finaltime){
		iteration = new IterationFiniteVolume();
		
		this.reconstruction = reconstruction; 
		this.grid = grid; 
		this.equations = equations; 
		this.source = source; 
		this.obstacle = obstacle;
		this.initialData = initialData; 
		this.boundary = boundary; 
		this.fluxSolver = fluxSolver;
		this.timeIntegrator = timeIntegrator;
		this.output = output;
		
		
    	this.finaltime = finaltime;
   }
	
	public boolean passed(){
		double[] firstRun = errorNorm();
		grid.doubleResolution();
		double[] secondRun = errorNorm();
		
		for (int q = 0; q < firstRun.length; q++) {
			System.out.println("error norms:" + firstRun[q] + " " + secondRun[q]);
			System.err.println("error norms:" + firstRun[q] + " " + secondRun[q]);
		}
		
		grid.halvenResolution();
		
		boolean passed = true;
		for (int q = 0; q < firstRun.length; q++) {
			passed = passed && (secondRun[q] <= firstRun[q] * 0.9);
		}
		return passed;
	}
	
	protected double[] errorNorm(){
		Setup hydroSetup = new Setup();
		iteration.setups.clear();
		
		hydroSetup.reconstruction = reconstruction;
		iteration.grid              = grid;
    	hydroSetup.reconstruction.setGrid(iteration.grid);
    	
    	hydroSetup.equations = equations;
    	hydroSetup.source = source;
    	hydroSetup.obstacle = obstacle; 
    	hydroSetup.initialData = initialData;
 
    	hydroSetup.boundary = boundary;
    	hydroSetup.fluxSolver = fluxSolver;
    	
    	iteration.timeIntegrator = timeIntegrator;
    	//this.output = new OutputErrorNorm(finaltime, Iteration.grid, equations, Iteration.initialData, 1);
    	hydroSetup.output = this.output;
    	
    	iteration.verbose = false;

    	iteration.setups.add(hydroSetup);
    	
    	//System.err.println(description());
    	
    	iteration.run(finaltime);
		return output.errorNorm();
	}
	
	public String description(){ return iteration.getDescriptions() + "\n" + iteration.setups.getDescriptions(); };
		
}
