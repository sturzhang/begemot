import matrices.DiagonalMatrix;
import matrices.SquareMatrix;


public class FluxSolverRoeIsentropicModified extends FluxSolverRoe {

	public FluxSolverRoeIsentropicModified(GridCartesian grid, EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	protected UpwindingMatrixAndWaveSpeed getUpwindingMatrix(int i, int j, int k, double[] quantities, int direction, double[] left, double[] right){
		double rho = quantities[EquationsIsentropicEuler.INDEX_RHO];
		double vX = quantities[EquationsIsentropicEuler.INDEX_XMOM]/rho;
		double vY = quantities[EquationsIsentropicEuler.INDEX_YMOM]/rho;
		double vZ = quantities[EquationsIsentropicEuler.INDEX_ZMOM]/rho;
		double v2 = vX*vX + vY*vY + vZ*vZ;
		double v = Math.sqrt(v2);
		double machParameter = 1.0;
		
		double gamma = ((EquationsIsentropicEuler) equations).gamma();
		double pressure = ((EquationsIsentropicEuler) equations).getPressure(rho);
		double c2 = gamma * pressure / rho;
		double c = Math.sqrt(c2);
		
		double waveSpeed = c/machParameter + v;
				
		/* test setup ----------------------------------------------------------------------------
		
		direction = Grid.X_DIR;
		vX = 1.0;
		v = 1.0;
		rho = 3.0;
		
		// end of test setup --------------------------------------------------------------------- */

		
		double[] Projarr = new double[4];
		Projarr[0] = 1.0;
		Projarr[1] = 1.0;
		Projarr[2] = 1.0;
		Projarr[direction] = rho;
		Projarr[3] = c2;
				
		DiagonalMatrix Proj = new DiagonalMatrix(Projarr);
		DiagonalMatrix ProjInv = Proj.clone();
		ProjInv.invertMe();
		
		double[][] mat = new double[4][4];
		
		double machCutoff = 1e-6;
	    double machLocal = Math.sqrt(v2) / c;
	    double limitedMach;

	    
	    if (machLocal > 1.0){
	    	limitedMach = 1.0;
	    } else {
	    	if (machLocal > machCutoff){
	    		limitedMach = machLocal; 
	    	} else {
	    		limitedMach = machCutoff;
	    	}
	    }
	    
		double delta = 1.0/limitedMach - 1;  //cutoffMach  

		
		if (direction == GridCartesian.X_DIR){
			double m1n = (vX >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vX*vX*machParameter*machParameter);


			mat[0][0] = c2/tau/machParameter;
			//mat[0][0] = v;
			mat[0][1] = 0.0;
			mat[0][2] = 0.0;
			mat[0][3] = 1.0 / machParameter/machParameter;
			// more stable without:
			/*mat[0][3] = (c*c2*delta + machParameter*tau*tau*vX) / 
					 (c2*machParameter*machParameter*rho*tau + c * delta*machParameter*machParameter*machParameter*rho*tau*vX); //*/
			
			mat[1][0] = 0.0;
			mat[1][1] = m1n*vX;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;
			
			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = m1n*vX;
			mat[2][3] = 0.0;
			
			mat[3][0] = -c2;
			//doesn't work mat[3][0] = (c*rho * (-c2*delta + c*vX*machParameter + delta*vX*vX*machParameter*machParameter)) / tau;
			mat[3][1] = 0.0;
			mat[3][2] = 0.0;
			mat[3][3] = c2 / machParameter / tau;
			//mat[3][3] = 2.0*c / machParameter;
			//mat[3][3] = v;
		}
		if (direction == GridCartesian.Y_DIR){
			double m1n = (vY >= 0? 1.0 : -1.0); 
			double tau = Math.sqrt(c2 * (1.0 + delta*delta) - delta*delta*vY*vY*machParameter*machParameter);
			
			mat[0][0] = m1n*vY;
			mat[0][1] = 0.0;
			mat[0][2] = 0.0;
			mat[0][3] = 0.0;
			
			//mat[1][0] = 0.0;
			mat[1][1] = c2/tau/machParameter;
			mat[1][1] = v;
			mat[1][2] = 0.0;
			mat[1][3] = 1.0 / machParameter/machParameter;
			// more stable without
			/*mat[1][3] = (c*c2*delta + machParameter*tau*tau*vY) / 
					(c2 * machParameter*machParameter * rho * tau + c * delta * machParameter*machParameter*machParameter * rho * tau * vY); //*/

			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = m1n*vY;
			mat[2][3] = 0.0;
			
			mat[3][0] = 0.0;
			mat[3][1] = -c2;
			//doesn't work mat[3][1] = (c*rho * (-c2*delta + machParameter*c*vY + delta*vY*vY*machParameter*machParameter)) / tau;
			mat[3][2] = 0.0;
			//mat[3][3] = 2.0*c/machParameter;
			mat[3][3] = c2 / machParameter / tau;
			//mat[3][3] = v;
		}
		if (direction == GridCartesian.Z_DIR){
			double m1n = (vZ >= 0? 1.0 : -1.0); 
			mat[0][0] = m1n*vZ;
			mat[0][1] = 0.0;
			mat[0][2] = 0.0;
			mat[0][3] = 0.0;
			
			mat[1][0] = 0.0;
			mat[1][1] = m1n*vZ;
			mat[1][2] = 0.0;
			mat[1][3] = 0.0;

			mat[2][0] = 0.0;
			mat[2][1] = 0.0;
			mat[2][2] = v;
			mat[2][3] = 1.0 / machParameter/machParameter;
			
			mat[3][0] = 0.0;
			mat[3][1] = 0.0;
			mat[3][2] = -c2;
			mat[3][3] = 2.0*c/machParameter; 
			//mat[3][3] = v;
		}
		
		SquareMatrix res1 = new SquareMatrix(mat);
		SquareMatrix res = ProjInv.mult(res1).mult(Proj).toSquareMatrix();
		SquareMatrix res2 = ((EquationsIsentropicEuler) equations).primToCons(vX, vY, vZ, rho, res);
		
		/*System.err.println("v=" + v + ", c2=" + c2);
		System.err.println(res.toString());
		System.err.println("");
		System.err.println(res2.toString());*/
		
		
		return new UpwindingMatrixAndWaveSpeed(
				res2,
				waveSpeed);
	}
	
}
