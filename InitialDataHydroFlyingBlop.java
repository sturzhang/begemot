
public class InitialDataHydroFlyingBlop extends InitialDataHydro {

	private double width;
	
	public InitialDataHydroFlyingBlop(Grid grid, EquationsHydroIdeal equations, double width) {
		super(grid, equations);
		this.width = width;
	}

	public double[] getInitialValue(GridCell g, int q){
		double x, y;
		double[] res = new double[q];
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;

		if ((Math.abs(x) < width) && (Math.abs(y) < width)){
			res[EquationsHydroIdeal.INDEX_RHO] = 2.0;
			res[EquationsHydroIdeal.INDEX_XMOM] = 4.0;
		} else {
			// background
			res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
			res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		}
					
		res[EquationsHydroIdeal.INDEX_ENERGY] = 10.0;

		return res;
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
