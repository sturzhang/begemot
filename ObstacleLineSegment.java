
public class ObstacleLineSegment extends ObstacleHydro {

	private double distFromCenter;
	
	public ObstacleLineSegment(GridCartesian grid, EquationsHydroIdeal equations, double distFromCenter) {
		super(grid, equations);
		this.distFromCenter = distFromCenter;
	}

	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {
		// nothing yet
	}
	
	@Override
	protected void setObstacleBoundaries() {

		/*for (int i = 0; i < interfaceFluxes.length; i++){
			int j = grid.ny/2;
			for (int k = 0; k < interfaceFluxes[i][j].length; k++){
				for (int d = 0; d < grid.dimensions(); d++){
					for (int q = 0; q < interfaceFluxes[i][j][k][d].length; q++){
						interfaceFluxes[i][j][k][d][q] = 0.0;
					}
				}
			}
		}
		return interfaceFluxes;*/
	}

	@Override
	public String getDetailedDescription() {
		return "Boundary in the form of a line segment in the center of the grid, having a total length of "+ (2.0*distFromCenter);
	}


}
