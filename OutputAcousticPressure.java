public class OutputAcousticPressure extends Output {

	protected double mach;
	private boolean firsttime = true;
	private double initialval;
	private double initialvx = 0, initialvy = 0; // to please the compiler
	
	
	public OutputAcousticPressure(double outputTimeInterval, GridCartesian grid, EquationsAcousticSimple equations, double mach) {
		super(outputTimeInterval, grid, equations);
		this.mach = mach;
	}

	@Override
	public String getDetailedDescription() {
    	return "prints the point value of the pressure";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double rhoV2, pressureMean = 0.0, pressureMax = 0.0, pressureMin = 0.0, pressureGradX, pressureGradXMax = 0.0;
		//boolean firsttime = true;
    	int counter = 0;
		double divV, divVMean = 0.0;
    	
		/*for (int i = grid.indexMinX()+1; i <= grid.indexMaxX()-1; i++){
    		for (int j = grid.indexMinY()+1; j <= grid.indexMaxY()-1; j++){
    			for (int k = grid.indexMinZ(); k <= grid.indexMaxZ(); k++){
    				counter++;
    				
					divV = (quantities[i+1][j][k][EquationsAcoustic.INDEX_VX] -  
							quantities[i-1][j][k][EquationsAcoustic.INDEX_VX]) / (2.0*grid.xSpacing()) +
						 		(quantities[i][j+1][k][EquationsAcoustic.INDEX_VY] -  
						 				quantities[i][j-1][k][EquationsAcoustic.INDEX_VY]) / (2.0*grid.ySpacing());
					
					divVMean += divV;
					
					pressureGradX = (quantities[i+1][j][k][EquationsAcoustic.INDEX_P] -  
							quantities[i-1][j][k][EquationsAcoustic.INDEX_P]) / (2.0*grid.xSpacing());
					
					
					pressureMean += quantities[i][j][k][EquationsAcoustic.INDEX_P];

					if (firsttime){
    					pressureMax = quantities[i][j][k][EquationsAcoustic.INDEX_P];
    					pressureMin = quantities[i][j][k][EquationsAcoustic.INDEX_P];
    					
    					pressureGradXMax = pressureGradX;
    					firsttime = false;
    				} else {
    					pressureMax = Math.max(pressureMax, quantities[i][j][k][EquationsAcoustic.INDEX_P]);
    					pressureMin = Math.min(pressureMax, quantities[i][j][k][EquationsAcoustic.INDEX_P]);
    					
    					pressureGradXMax = Math.max(pressureGradXMax, pressureGradX);
    				}
    			}
    		}
    	} 
    	
    	//print(mach + " " + (pressureMean/counter) + " " + (divVMean/counter) + " "); 
		println(time + " " + mach + " " + (pressureMax) + " " + (pressureGradXMax) + " " + ((pressureMax - pressureMin)/pressureMax) );  //*/
		
    	//*/

		
		int i = 30;
		int j = 34;
		int k = ((GridCartesian) grid).indexMinZ();
		
		//System.err.println(grid.getX(i, j, k) + " " + grid.getY(i, j, k));
		
		divV = (quantities[i+1][j][k][EquationsAcousticSimple.INDEX_VX] -  
				 	quantities[i-1][j][k][EquationsAcousticSimple.INDEX_VX]) / ((GridCartesian) grid).xSpacing() +
				 	(quantities[i][j+1][k][EquationsAcousticSimple.INDEX_VY] -  
				 	quantities[i][j-1][k][EquationsAcousticSimple.INDEX_VY]) / ((GridCartesian) grid).ySpacing();
			
		divVMean += divV;
			
		pressureMean += quantities[i][j][k][EquationsAcousticSimple.INDEX_P];

		if (firsttime){
			firsttime = false;
			initialval = pressureMean;
			initialvx = quantities[i][j][k][EquationsAcousticSimple.INDEX_VX];
			initialvy = quantities[i][j][k][EquationsAcousticSimple.INDEX_VY];
		}
		
		println(time + " " + Math.abs(pressureMean-initialval) + " " + divVMean + " " + k);  //*/
		
		/*println(time + " " + 
				Math.abs(quantities[i][j][k][EquationsAcoustic.INDEX_VX]-initialvx) + " " + 
			Math.abs(quantities[i][j][k][EquationsAcoustic.INDEX_VY]-initialvy)); //*/
		
		// 
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
