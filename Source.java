public abstract class Source {
	
	protected Grid grid;
	protected Equations equations;
	
	public Source(Grid grid, Equations equations){
		this.grid = grid;
		this.equations = equations;
	}
	
    public abstract String getDetailedDescription();
    
    public String description(){ 
    	return "# Source terms: " + getDetailedDescription(); 
    }
    
    public abstract double[][][][] get(double[][][][] quantities, double[][][][] sources, double time, double dt, int timeIntegratorStep);
    
    
    protected double average(double L, double C, double R){
		return 0.25*L + 0.5*C + 0.25*R;
	}
    
}
