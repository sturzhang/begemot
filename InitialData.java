public abstract class InitialData {

	protected Grid grid;
	
	public InitialData(Grid grid){
		this.grid = grid;
	}
	
	public abstract double[] getInitialValue(GridCell g, int numberOfConservedQuantities);
	
    public void fillConservedQuantities(double[][][][] conservedQuantities){
    	int q = conservedQuantities[0][0][0].length;
    	for (GridCell g : grid){
			conservedQuantities[g.i()][g.j()][g.k()] = getInitialValue(g, q);
		}
    	
    }

    public void setExactCentralDerivative(double[][][][] conservedQuantities){
    	// hack to make exact initial data
    	double dy = ((GridCartesian) grid).ySpacing(); 
    	double gy = -1.0;
    	
    	
    	// EquationsHydroIdeal
    	/*for (int i = 0; i < conservedQuantities.length; i++){
	    	for (int j = 2; j < conservedQuantities[1].length; j++){
				conservedQuantities[i][j][0][EquationsHydroIdeal.INDEX_ENERGY] = 
						conservedQuantities[i][j-2][0][EquationsHydroIdeal.INDEX_ENERGY] 
						+ gy *
				0.25* ( conservedQuantities[i][j-2][0][EquationsHydroIdeal.INDEX_RHO] + 
					2.0*conservedQuantities[i][j-1][0][EquationsHydroIdeal.INDEX_RHO] + 
					    conservedQuantities[i][j  ][0][EquationsHydroIdeal.INDEX_RHO]) / 0.4 * dy * 2;
				
				
				
				conservedQuantities[i][j][0][EquationsHydroIdeal.INDEX_XMOM] = 0.0;
				conservedQuantities[i][j][0][EquationsHydroIdeal.INDEX_YMOM] = 0.0;
			}
    	}//*/
    	
    	
    	
    	// EquationsAcousticExtended
    	/*for (int i = 0; i < conservedQuantities.length; i++){
	    	for (int j = 2; j < conservedQuantities[1].length; j++){
	    		conservedQuantities[i][j][0][EquationsAcousticExtended.INDEX_P] = 
						conservedQuantities[i][j-2][0][EquationsAcousticExtended.INDEX_P] 
						+ gy *
				0.25* ( conservedQuantities[i][j-2][0][EquationsAcousticExtended.INDEX_RHO] + 
					2.0*conservedQuantities[i][j-1][0][EquationsAcousticExtended.INDEX_RHO] + 
					    conservedQuantities[i][j  ][0][EquationsAcousticExtended.INDEX_RHO]) * dy * 2;
				
				
				
				conservedQuantities[i][j][0][EquationsAcousticExtended.INDEX_VX] = 0.0;
				conservedQuantities[i][j][0][EquationsAcousticExtended.INDEX_VY] = 0.0;
			}
    	}//*/
    }
    
    public abstract String getDetailedDescription();
    
    public String description(){ 
    	return "# Initial data: " + getDetailedDescription(); 
    }
    
}