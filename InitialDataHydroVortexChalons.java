
public class InitialDataHydroVortexChalons extends InitialDataHydro {

	public InitialDataHydroVortexChalons(Grid grid, EquationsHydro equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x, y;
		double rhoV2, pressure;
		
		x = g.getX();
		y = g.getY();
		
		
		res[EquationsHydroIdeal.INDEX_RHO] = 1.0 - 0.5 * Math.tan(y - 0.5);
    	
		res[EquationsHydroIdeal.INDEX_XMOM] = res[EquationsHydroIdeal.INDEX_RHO] * 
				2.0 * Math.sin(Math.PI * x)*Math.sin(Math.PI * x)  *   Math.sin(Math.PI * y)*Math.cos(Math.PI * y);
		res[EquationsHydroIdeal.INDEX_YMOM] = res[EquationsHydroIdeal.INDEX_RHO] * 
			   -2.0 * Math.sin(Math.PI * x)*Math.cos(Math.PI * x)  *   Math.sin(Math.PI * y)*Math.sin(Math.PI * y);
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;

		pressure = 1000.0;
				
		rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    				
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2);
    	
    	return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "Low Mach test case by Chalons, also in Corots PhD thesis";
	}

	
}
