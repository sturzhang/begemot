
public abstract class TimeIntegratorForSemiLagrangian extends TimeIntegratorExplicit {

	protected double[][][][][] savedQuantities;
	protected TimeIntegratorExplicitConservative myIntegrator;
	protected FluxSolverSemiLagrangian fluxSolver;
	protected boolean fluxSolverKnowsMe = false;
	
	
	
	public TimeIntegratorForSemiLagrangian(Grid grid, double CFL, FluxSolverSemiLagrangian fluxSolver) {
		super(grid, CFL);
		savedQuantities = new double[steps()+1][][][][];
		myIntegrator = new TimeIntegratorExplicitConservative(grid, CFL);
		this.fluxSolver = fluxSolver;
	}

	@Override
	public int steps() { return 2; }


	public double[][][][] initialQuantities(){ return savedQuantities[0]; }
	

}
