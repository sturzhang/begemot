
public class EquationsAdvectionGresho extends EquationsAdvectionSpatiallyDependent {

	protected double width;
	
	public EquationsAdvectionGresho(Grid grid, int numberOfPassiveScalars, double width) {
		super(grid, numberOfPassiveScalars);
		this.width = width;
	}

	public double width(){ return width; }
	
	public double[] getVelocity(double[] position){
		double x = position[0] - grid.xMidpoint();
		double y = position[1] - grid.yMidpoint();
    	
		double r = Math.sqrt(x*x+y*y);
    	double v = 0.0;
		
    	if (r == 0){
    		return new double[] {0,0,0};
    	} else {
			if (r < width){
				v = r/width;
			} else {
				if (r < 2*width){
					v = 2-r/width;
				}
			}
			
			return new double[] {-y/r*v, x/r*v, 0};
    	}
	}
	
	@Override
	public String getDetailedDescription() {
		return "Gresho-like advection velocity";
	}

}
