import matrices.SquareMatrix;


public class FluxSolverMultiStepMultiDLaxWendroffLimited2 extends
		FluxSolverMultiStepMultiDCartesianSecondOrder {

	public FluxSolverMultiStepMultiDLaxWendroffLimited2(GridCartesian grid,
			Equations equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	protected double limiter(double jumpL, double jumpC, double jumpR, double speed){
		double r;
		if (jumpC == 0){
			r = 0;
		} else {
			r = speed > 0 ? jumpL/jumpC : jumpR/jumpC;
		}
		
		return limiter(r); 
	}
	
	protected double limiter(double r){
		// return 1.0; // lax wendroff
		// return r;  // beam warming
		return Math.max(0, Math.min(1.0, r)); // minmod
	}
	
	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LLTT, double[] LLT, double[] LL, double[] LLB,
			double[] LLBB, double[] LTT, double[] LT, double[] L, double[] LB,
			double[] LBB, double[] RTT, double[] RT, double[] R, double[] RB,
			double[] RBB, double[] RRTT, double[] RRT, double[] RR,
			double[] RRB, double[] RRBB) {
		
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		int quantities = equations.getNumberOfConservedQuantities();
		
		double[] diff    = new double[quantities];
		double[] centralFlux = new double[quantities];
		double[] limitedDiff = new double[quantities];
		
		double[] flux1d = new double[quantities];
		double[] flux2d = new double[quantities];
		
		SquareMatrix[] mat = equations.diagonalization(i, j, k, mean(L, R), direction);
		SquareMatrix Jx = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		SquareMatrix absJx = mat[0].mult(mat[1].absElementwise()).mult(mat[2]).toSquareMatrix();
		
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, L, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, R, direction);
		for (int q = 0; q < quantities; q++){ 
			diff[q] = R[q] - L[q]; 
			centralFlux[q] = 0.5*(fluxLeft[q] + fluxRight[q]);
		}
		
		double[] charLL = mat[2].mult(LL);
		double[] charL  = mat[2].mult(L );
		double[] charR  = mat[2].mult(R );
		double[] charRR = mat[2].mult(RR);
		
		for (int q = 0; q < quantities; q++){
			limitedDiff[q] = limiter(charL[q] - charLL[q], charR[q] - charL[q], charRR[q] - charR[q], mat[1].value(q, q)) * diff[q];
		}
		limitedDiff = mat[0].mult(limitedDiff);
		
		
		/*for (int q = 0; q < quantities; q++){
			limitedDiff[q] = limiter(L[q] - LL[q], R[q] - L[q], RR[q] - R[q], mat[1].value(q, q)) * diff[q];
		}*/
		
		
		
		
		
		double waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		double[] term1 = absJx.mult(0.5).mult(diff);
		double[] term2 = absJx.mult(0.5).mult(limitedDiff);
		for (int q = 0; q < quantities; q++){
			flux1d[q] = centralFlux[q] - term1[q] + term2[q];
		}
		
		//term1 = absJx.mult(Jx).mult(0.25).mult(slopeDiff);
		term1 = Jx.mult(Jx).mult(0.5).mult(limitedDiff);
		
		for (int q = 0; q < quantities; q++){
    		flux2d[q] = - term1[q];
    		
    		if ((Double.isNaN(flux2d[q])) || (Double.isNaN(flux1d[q]))){
    	    	System.err.println("fluxes are NaN");
    	    }
    	}
		
		return new FluxMultiStepAndWaveSpeed(flux1d, flux2d, waveSpeed);


	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep) {
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (dir == GridCartesian.X_DIR){ // we don't know which one to divide by dx, and which by dy
				return dt/((GridCartesian) grid).xSpacing();
			} else if (dir == GridCartesian.Y_DIR){
				return dt/((GridCartesian) grid).xSpacing();
			} else {
				System.err.println("ERROR: asked for non-x, non-y direction");
				return 0.0;
			}
		}
	}

	@Override
	public int steps() { return 2; }


	@Override
	public String getDetailedDescription() {
		return "Lax-Wendroff/Beam-Warming-type method for advection with flux limiting ( = minmod)";
	}

}
