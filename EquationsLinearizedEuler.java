import matrices.SquareMatrix;


public class EquationsLinearizedEuler extends EquationsSpatiallyDependentLinear {

	
	protected EquationsHydroIdeal myEuler;
	protected InitialData myInitial;
	
	public EquationsLinearizedEuler(Grid grid, int numberOfPassiveScalars, double gamma, InitialData initial) {
		// you should only enter stationary solutions of euler in here as background; time dependent background not available
		super(grid, numberOfPassiveScalars);
		myEuler = new EquationsHydroIdeal(grid, 0, gamma);
		myInitial = initial;
	}

	@Override
	public SquareMatrix getJacobian(int direction, GridCell g) {
		SquareMatrix[] diag = diagonalization(g.i(), g.j(), g.k(), null, direction); 
		SquareMatrix res = diag[0].mult(diag[1]).mult(diag[2]).toSquareMatrix();
		
		boolean includeAllTerms = false;
		
		if (!includeAllTerms){
			// quadratic terms
			res.setElement(1, 0, 0.0);
			res.setElement(2, 0, 0.0);
			res.setElement(4, 0, 0.0);
			res.setElement(4, 1, 0.0);
			res.setElement(4, 2, 0.0);
			
			// constant terms
			/*res.setElement(0, 1, 0.0);
			res.setElement(0, 2, 0.0);
			res.setElement(1, 4, 0.0);
			res.setElement(2, 4, 0.0); //*/
			
			// linear terms
			/*res.setElement(1, 1, 0.0);
			res.setElement(2, 2, 0.0); //*/
			/*res.setElement(3, 3, 0.0);
			res.setElement(4, 4, 0.0);//*/
					
			/*res.setElement(2, 1, 0.0);
			res.setElement(1, 2, 0.0); //*/
			
			/*System.err.println(res);
			System.err.println("");System.err.println("");//*/
		}
			
		return res;
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k,
			double[] quantities, int direction) {
		// quantities are not used!
		return myEuler.diagonalization(i, j, k, 
				myInitial.getInitialValue(new GridCell(i,j,k, grid), getNumberOfConservedQuantities()), 
				direction);
	}

	@Override
	public int getNumberOfConservedQuantities() { return myEuler.getNumberOfConservedQuantities(); }

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		return myEuler.getTransform(quantities, normalVector);
	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		return myEuler.getTransformBack(quantities, normalVector);
	}

	@Override
	public void transformFixedParameters(double[] normalVector) {
		// nothing to do
	}

	@Override
	public void transformFixedParametersBack(double[] normalVector) {
		// nothing to do
	}

	@Override
	public String getDetailedDescription() {
		return "Linearized Euler around the data: " + myInitial.getDetailedDescription();
	}

}
