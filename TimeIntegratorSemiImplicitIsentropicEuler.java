
public class TimeIntegratorSemiImplicitIsentropicEuler extends
		TimeIntegratorExplicit {

	private EquationsIsentropicEuler equations;
	
	
	public TimeIntegratorSemiImplicitIsentropicEuler(Grid grid, double CFL, EquationsIsentropicEuler equations) {
		super(grid, CFL);
		this.equations = equations;
	}

	@Override
    public String getDetailedDescription(){
    	return "Semi-implicit time integrator for the isentropic Euler equations with CFL = " + CFL;
    }
	
	@Override
	public int steps() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {
		int[] quantities;
		int i, j, k;
		double cellSize;

		if (step == 2){
			quantities = new int[] {EquationsIsentropicEuler.INDEX_XMOM, EquationsIsentropicEuler.INDEX_YMOM, EquationsIsentropicEuler.INDEX_ZMOM};
		} else {
			quantities = new int[] {EquationsIsentropicEuler.INDEX_RHO};
		}
		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			cellSize = g.size();
						
			if (!excludeFromTimeIntegration[i][j][k]){
				for (int dir = 0; dir < grid.summedInterfaceFluxes[i][j][k].length; dir++){
					for (int q = 0; q < quantities.length; q++){	
						g.addToConservedQuantity(- dt / cellSize * grid.summedInterfaceFluxes[i][j][k][dir][quantities[q]]
								+ dt * sources[i][j][k][quantities[q]] , quantities[q]);
					
					}

				}
					
			}
		}
		
		return conservedQuantities;
	}
	

}
