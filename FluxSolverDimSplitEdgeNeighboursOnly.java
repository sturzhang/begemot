
public abstract class FluxSolverDimSplitEdgeNeighboursOnly extends FluxSolverDimSplit {

	
	public FluxSolverDimSplitEdgeNeighboursOnly(Grid grid, Equations equations) {
		super(grid, equations, new StencilTurnerOneD(grid));
	}
	
	@Override
	protected FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[][] quantities, int timeIntegratorStep){
		return flux(gridEdge, quantities[StencilTurnerOneD.NEIGHBOUR_LEFT], quantities[StencilTurnerOneD.NEIGHBOUR_RIGHT], timeIntegratorStep);
	}
	
	/*@Override
	public GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge) {
		return new GridCell[]{g, gridEdge.getOtherGridCell(g)};
	}*/
	
	@Override
	protected double[] fluxPassiveScalar(GridCell g, GridEdgeMapped gridEdge, GridCell left, GridCell right, int direction){
		return fluxPassiveScalar(g.i(), g.j(), g.k(), left.interfaceValue(), right.interfaceValue(), direction);
	}
	
	protected abstract FluxAndWaveSpeed flux(GridEdgeMapped gridEdge, double[] left, double[] right, int timeIntegratorStep);
    protected abstract double[] fluxPassiveScalar(int i, int j, int k, double[] left, double[] right, int direction);

}
