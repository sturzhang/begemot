
public class FluxSolverMultiStepMultiDExactRiemannAcoustic extends
		FluxSolverMultiStepMultiDExactRiemann {

	public FluxSolverMultiStepMultiDExactRiemannAcoustic(GridCartesian grid, EquationsAcousticSimple equations) {
		super(grid, equations);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LT, double[] L, double[] LB, 
			double[] RT, double[] R, double[] RB, int timeIntegratorStep) {
		
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		int quantities = L.length;
		double[] flux1d = new double[quantities];
		double[] flux2d = new double[quantities];
		
		int velIndex, velIndexOrtho;
		velIndex      = EquationsAcousticSimple.INDEX_VX;
		velIndexOrtho = EquationsAcousticSimple.INDEX_VY;
		
		double diffSimple[] = new double[quantities];
		double sumSimple[] = new double[quantities];
		double tripleDiff[] = new double[quantities];
		double tripleSum[] = new double[quantities];
		double diffDiff[] = new double[quantities];
		double sumDiff[] = new double[quantities];
		
		for (int q = 0; q < quantities; q++){ 
			diffSimple[q] = R[q] - L[q];
			sumSimple[q]  = R[q] + L[q];
			
			tripleDiff[q]    = RT[q]-LT[q] - 2.0*(R[q]-L[q]) + RB[q]-LB[q];
			tripleSum[q]     = RT[q]+LT[q] + 2.0*(R[q]+L[q]) + RB[q]+LB[q];
			diffDiff[q] = RT[q] - LT[q] - (RB[q] - LB[q]);
			sumDiff[q] = RT[q] + LT[q] - (RB[q] + LB[q]);
		}
		
		flux1d[EquationsAcousticSimple.INDEX_VZ] = 0.0;
		flux2d[EquationsAcousticSimple.INDEX_VZ] = 0.0;
		
			
		if (((EquationsAcousticSimple) equations).symmetric()){
			flux1d[velIndex]                  = 0.5*c*sumSimple[EquationsAcousticSimple.INDEX_P] 
					- 0.5*c*diffSimple[velIndex];
			flux1d[velIndexOrtho]             = 0.0;
			flux1d[EquationsAcousticSimple.INDEX_P] = 0.5*c*sumSimple[velIndex]
					- 0.5*c*diffSimple[EquationsAcousticSimple.INDEX_P];
		
			flux2d[velIndex] = c*(
						- tripleDiff[velIndex]/(2.0*Math.PI) 
						- 0.25*sumDiff[velIndexOrtho] 
						+ 0.25*tripleSum[EquationsAcousticSimple.INDEX_P] - sumSimple[EquationsAcousticSimple.INDEX_P]  
						);
			flux2d[velIndexOrtho]             = 0.0;
			flux2d[EquationsAcousticSimple.INDEX_P] = c*(
					0.25 * diffDiff[velIndexOrtho] - tripleDiff[EquationsAcousticSimple.INDEX_P]/(2.0*Math.PI) 
					+ 0.0);
		} else {
			System.err.println("Error: No implementation for asymmetric acoustics!");
		}
			
		return new FluxMultiStepAndWaveSpeed(flux1d, flux2d, c);
		
	}

	@Override
	public String getDetailedDescription() {
		return "Exact Riemann solver for 2-d advection";
	}





}
