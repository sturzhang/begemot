
public class OutputSixRegionsIntegratedFlux extends Output {

	protected double[] integral;
	double lasttime;
	
	public OutputSixRegionsIntegratedFlux(double outputTimeInterval, GridCartesian grid,
			EquationsAcousticSimple equations) {
		super(outputTimeInterval, grid, equations);
		integral = new double[]{0.0, 0.0, 0.0, 0.0};
		lasttime = 0.0;
	}

	@Override
	public String getDetailedDescription() {
		return "Integrated quantity over the interfaces bounded by 6 regions.";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		println("");
		println("");
		double y;
		double dt = time - lasttime;
		lasttime = time;
		
		System.err.println("Integrating..." + integral[0] + " " + dt);
		
		int i = ((GridCartesian) grid).getXIndex(grid.xMidpoint());
		for (int j = 0; j < quantities[i].length; j++){
			y = grid.getY(i,j,0);
			if ((y > -0.5) && (y < 0.5)){
				
				for (int q = 0; q < integral.length; q++){
					integral[q] += quantities[i][j][((GridCartesian) grid).indexMinZ()][q] * dt * ((GridCartesian) grid).ySpacing();
				}
			}
		}
	}


	public void printIntegrationResult(){
		println("");
		
		//print(grid.getX(i) + " " + grid.getY(j) + " ");
		for (int q = 0; q < integral.length; q++){
			print((integral[q] / lasttime) + " ");
		}
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
