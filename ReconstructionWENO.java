
public class ReconstructionWENO extends Reconstruction {

	protected int degOfPolynomial;
	
	protected double EPSILON = 1e-6;
	
	protected int widthOfReconstructionStencil;
	protected double[][] reconstructionCoefficient;
	protected double[] wenoSmoothCaseWeights;
	protected double[][][] wenoOscillationIndicatorCoefficient;
	
	public ReconstructionWENO(int degOfPolynomial) {
		this.degOfPolynomial = degOfPolynomial;
		this.widthOfReconstructionStencil = 1 + degOfPolynomial;
		
		double[][] tmpreconstructionCoefficient;
		
		tmpreconstructionCoefficient = precalculateReconstructionCoefficients(2*widthOfReconstructionStencil-1);
		reconstructionCoefficient = precalculateReconstructionCoefficients(widthOfReconstructionStencil);
		
		/*for (int i = 0; i < reconstructionCoefficient.length; i++){
			for (int j = 0; j < reconstructionCoefficient[i].length; j++){
				System.err.println(i + " " + j + " " + reconstructionCoefficient[i][j]);
			}
		}*/
		
		double sum, testsum = 0.0;
		wenoSmoothCaseWeights = new double[widthOfReconstructionStencil]; 
		for (int s = 0; s <= widthOfReconstructionStencil-1; s++){
			sum = 0.0;
	    	for (int r = Math.max(s-widthOfReconstructionStencil+1, 0); r <= s-1; r++){
	    		sum += wenoSmoothCaseWeights[r] * reconstructionCoefficient[r+1][widthOfReconstructionStencil-s+r-1];
	    	}

	    	wenoSmoothCaseWeights[s] = (tmpreconstructionCoefficient[widthOfReconstructionStencil][2*widthOfReconstructionStencil-2-s] 
	    			- sum) / reconstructionCoefficient[s+1][widthOfReconstructionStencil-1];
	    	       
	    	testsum += wenoSmoothCaseWeights[s];
		}
		
		/*for (int i = 0; i < reconstructionCoefficient.length; i++){
			System.out.println(i + " " + wenoSmoothCaseWeights[i]);
		}*/
			
		
		if (Math.abs(testsum - 1.0) > 1e-4){
	       System.err.println("ERROR: WENO smooth case weights probably wrong!");
		}
			
		
		wenoOscillationIndicatorCoefficient = precalculateOscillationIndicatorCoefficients(widthOfReconstructionStencil);
		
		/*for (int i = 0; i < wenoOscillationIndicatorCoefficient.length; i++){
			for (int j = 0; j < wenoOscillationIndicatorCoefficient[i].length; j++){
				for (int k = 0; k < wenoOscillationIndicatorCoefficient[j].length; k++){
					System.err.println(i + " " + j + " " + k + " " + wenoOscillationIndicatorCoefficient[i][j][k]);
				}
			}
		}
		System.err.println("done");*/
	}

	
	private double[][] precalculateReconstructionCoefficients(int k){
	    double sum, sumUpstairs, productUpstairs, productDownstairs;
	    
	    double[][] wenoInterpolationCoefficient = new double[k+1][k];
	 
	    for (int r = -1; r <= k-1; r++){
	    	for (int j = 0; j <= k-1; j++){
	    		sum = 0.0;
	    		for (int m = j+1; m <= k; m++){
	             
	    			sumUpstairs = 0.0;
	    			for (int l = 0; l <= k; l++){
	    				if (l != m){
	    					productUpstairs = 1.0;
	    					for (int q = 0; q <= k; q++){
	    						if ((q != m) && (q != l)){
	    							productUpstairs *= (r-q+1);
	    						}
	    					}
	    					sumUpstairs += productUpstairs;
	    				}
	    			}
	             
	    			productDownstairs = 1.0;
	    			for (int l = 0; l <= k; l++){
	    				if (l != m){
	    					productDownstairs *= (m-l);
	    				}
	    			}
	             
	    			sum += sumUpstairs / productDownstairs;
	    		}
	    		wenoInterpolationCoefficient[r+1][j] = sum;   
	    		// c_{rj} = wenoInterpolationCoefficient[r+1][j]
	    	}
	    }
	    return wenoInterpolationCoefficient;
	}
	
	private double[][][] precalculateOscillationIndicatorCoefficients(int k){
		// this routine actually is able to calculate the c-coefficients, too, but is not as simply reusable as the other one
	    // (you need to calculate them twice with differnet k for the smooth-case-weights, so take the other one for it)
	    int  i;
	    Polynomial E, V, Vprime;
	    Polynomial[] allV = new Polynomial[k];
	    Polynomial sumUpstairs, productUpstairs;
	    double productDownstairs;
	    
	    double[][][] res = new double[k][k][k];
	    i = 0;

	    for (int r = 0; r <= k-1; r++){
	    	for (int j = 0; j <= k-1; j++){
	    		V = new Polynomial(1);
	          
	    		for (int m = j+1; m <= k; m++){
	    			sumUpstairs = new Polynomial(1);
	             
	             
	    			for (int ell = 0; ell <= k; ell++){
	    				if (ell != m){
	                   
	    					productUpstairs = new Polynomial(new double[]{1.0});
	                   
	    					for (int q = 0; q <= k; q++){
	    						if ((q != m) && (q != ell)){
	    							E = new Polynomial(new double[] {-i+r-q+0.5, 1.0});
	                         
	    							productUpstairs = Polynomial.mult(E, productUpstairs);
	                      
	    						}
	    					}
	                   
	    					sumUpstairs = Polynomial.add(sumUpstairs, productUpstairs);
	    				}
	    			}
	             
	    			productDownstairs = 1.0;
	    			for (int ell = 0; ell <= k; ell++){
	    				if (ell != m){
	    					productDownstairs *= (m-ell);
	    				}
	    			}
	             
	    			E = new Polynomial(new double[] {1.0 / productDownstairs});
	    			
	    			sumUpstairs = Polynomial.mult(sumUpstairs, E);
	    			V = Polynomial.add(V, sumUpstairs);
	    		}
	          
	    		allV[j] = V;
	    	}
	    

	    	for (int j = 0; j <= k-1; j++){
	    		V = allV[j];
	          
		    	for (int jprime = 0; jprime <= k-1; jprime++){
		    		Vprime = allV[jprime];
		    			             
		    		res[r][j][jprime] = 0.0;
		    		for (int ell = 1; ell <= k-1; ell++){
		    			res[r][j][jprime] += Polynomial.mult(V.differentiate(ell), Vprime.differentiate(ell)).integrate(-0.5, 0.5);
		    		}
		    	}
	    	}
	    
	    }   
	    return res;
	}
	
	@Override
	public double[][][][][] perform(double[][][][] conservedQuantities, double[][][][][] interfaceValues) {
		double sum;	
		int xToggle = 0, yToggle = 0, zToggle = 0;
		int interfaceOrientation = 0, POSITIVE = 0, NEGATIVE = 1;
		// positive means (+) interface, negative means (-) interface
		double[] interpolation   = new double[widthOfReconstructionStencil];
		double[] beta            = new double[widthOfReconstructionStencil];
		double[] summationWeight = new double[widthOfReconstructionStencil];
		double sumOfSummationWeights, smoothCaseWeight;
		
		for (int interf = 0; interf < grid.dimensions()*2; interf++){
			
			// TODO here you can use the newfunction getInterfaceIndex in Reconstruction
			if (interf == ReconstructionCartesian.NORTH){ yToggle = 1; interfaceOrientation = POSITIVE; }
			if (interf == ReconstructionCartesian.SOUTH){ yToggle = 1; interfaceOrientation = NEGATIVE; }
			if (interf == ReconstructionCartesian.EAST ){ xToggle = 1; interfaceOrientation = POSITIVE; }
			if (interf == ReconstructionCartesian.WEST ){ xToggle = 1; interfaceOrientation = NEGATIVE; }
			if (interf == ReconstructionCartesian.FRONT){ zToggle = 1; interfaceOrientation = POSITIVE; }
			if (interf == ReconstructionCartesian.BACK ){ zToggle = 1; interfaceOrientation = NEGATIVE; }
		
			for (int q = 0; q < conservedQuantities[0][0][0].length; q++){
				for (int i = ((GridCartesian) grid).indexMinX()-1; i <= ((GridCartesian) grid).indexMaxX()+1; i++){
					// these special loop boundaries are needed for the 1-d & 2-d case when z (and y) directions do not have ghostcells
					for (int j = Math.max(((GridCartesian) grid).indexMinY()-1, 0); j <= Math.min(((GridCartesian) grid).indexMaxY()+1, ((GridCartesian) grid).lastGhostCellYIndex()); j++){
						for (int k = Math.max(((GridCartesian) grid).indexMinZ()-1, 0); k <= Math.min(((GridCartesian) grid).indexMaxZ()+1, ((GridCartesian) grid).lastGhostCellZIndex()); k++){
						
							for (int r = 0; r <= widthOfReconstructionStencil - 1; r++){
								// meaning that the leftmost index of the stencil is index - r
								sum = 0.0;
				                for (int jj = 0; jj <= widthOfReconstructionStencil - 1; jj++){
				                	
				                	// the +1 in the usage of the reconstruction coefficient is needed because index -1, 
				                	// mathematically legitimate cannot be realized with java arrays and thus the
				                	// definition of c_{rj} is shifted by one in the first index, s.t.
				                	// c_{rj} = reconstructionCoefficient[r+1][j]
				                	sum += reconstructionCoefficient[r-interfaceOrientation+1][jj] * 
				                			conservedQuantities[
			                			                    i + xToggle * (jj - r)][
			                			                    j + yToggle * (jj - r)][
			                			                    k + zToggle * (jj - r)][q];
				                }
			                    interpolation[r] = sum;
							}
						
			                for (int r = 0; r <= widthOfReconstructionStencil-1; r++){
			                	beta[r] = 0.0;
			                    for (int jj = 0; jj <= widthOfReconstructionStencil-1; jj++){
			                    	for (int jprime = 0; jprime <= widthOfReconstructionStencil-1; jprime++){
			                    		beta[r] += wenoOscillationIndicatorCoefficient[r][jj][jprime] * 
			                    				conservedQuantities[
			                    				         i + xToggle*(-r+jj)][
			                    				         j + yToggle*(-r+jj)][
			                    				         k + zToggle*(-r+jj)][q] * 
			                    				conservedQuantities[
			                    				         i + xToggle*(-r+jprime)][
			                    				         j + yToggle*(-r+jprime)][
			                    				         k + zToggle*(-r+jprime)][q];
			                    	}
			                    }
			                }

			                sumOfSummationWeights = 0;
			                for (int r = 0; r <= widthOfReconstructionStencil - 1; r++){
			                	if (interfaceOrientation == POSITIVE){
			                		smoothCaseWeight = wenoSmoothCaseWeights[r];
			                	} else {
			                        smoothCaseWeight = wenoSmoothCaseWeights[widthOfReconstructionStencil - 1 - r];
			                	}

			                    summationWeight[r] = smoothCaseWeight / Math.pow(beta[r] + EPSILON, 2);
			                    sumOfSummationWeights += summationWeight[r];
			                }
			                        
			                sum = 0.0;
			                for (int r = 0; r <= widthOfReconstructionStencil - 1; r++){
			                	sum += summationWeight[r] * interpolation[r] / sumOfSummationWeights;
			                }
							
							interfaceValues[i][j][k][interf][q] = sum;
							
						}  
					}
				}
			}
		}
		
		return interfaceValues;
	}

	@Override
	public int numberOfGhostcells() {
		return degOfPolynomial+1;
	}

	@Override
	public String getDetailedDescription() {
		return "WENO" + (2*degOfPolynomial+1);
	}

}
