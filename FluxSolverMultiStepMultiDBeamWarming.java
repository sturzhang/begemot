import matrices.SquareMatrix;


public class FluxSolverMultiStepMultiDBeamWarming extends FluxSolverMultiStepMultiDCartesianSecondOrder {

	public FluxSolverMultiStepMultiDBeamWarming(GridCartesian grid,
			Equations equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	protected double[] get(double[][][] val, int i, int j){ return val[i+1][j+2]; }
	
	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LLTT, double[] LLT, double[] LL, double[] LLB,
			double[] LLBB, double[] LTT, double[] LT, double[] L, double[] LB,
			double[] LBB, double[] RTT, double[] RT, double[] R, double[] RB,
			double[] RBB, double[] RRTT, double[] RRT, double[] RR,
			double[] RRB, double[] RRBB) {
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		int otherDirection = GridCartesian.Y_DIR;
		
		int quantities = equations.getNumberOfConservedQuantities();
		
		double[][][] val = new double[4][5][L.length];
		val[0][4] = LLTT; val[1][4] = LTT; val[2][4] = RTT; val[3][4] = RRTT;
		val[0][3] = LLT ; val[1][3] = LT ; val[2][3] = RT ; val[3][3] = RRT;
		val[0][2] = LL  ; val[1][2] = L  ; val[2][2] = R  ; val[3][2] = RR;
		val[0][1] = LLB ; val[1][1] = LB ; val[2][1] = RB ; val[3][1] = RRB ;
		val[0][0] = LLBB; val[1][0] = LBB; val[2][0] = RBB; val[3][0] = RRBB;
		
		
		double[] flux1d = new double[quantities];
		double[] flux2d = new double[quantities];
		
		double[] central = new double[quantities];
		double[] diff = new double[quantities];
		double[] diff2 = new double[quantities];
		double[] diff3 = new double[quantities];
		double[] perp = new double[quantities];
		double[] perp2 = new double[quantities];
		double[] perp3 = new double[quantities];
		
		double[] verticalAverage = new double[4];
		double[] verticalDiff = new double[4];
		double[] verticalDiff2 = new double[4];
		double[] verticalDiff3 = new double[4];

		
		for (int q = 0; q < quantities; q++){
			for (int col = 0; col < 4; col++){
				/*verticalAverage[col] = 
						(- val[col][0][q] + 2.0*val[col][1][q] + 6.0*val[col][2][q] + 2.0*val[col][3][q] - val[col][4][q])/8; //*/
				verticalAverage[col] = val[col][2][q];
				
				verticalDiff[col] = 
						(- val[col][0][q] + 2.0*val[col][1][q] - 2.0*val[col][3][q] + val[col][4][q])/2;
				verticalDiff2[col] = 
						(val[col][0][q] - 2.0*val[col][2][q] + val[col][4][q])/4;
				verticalDiff3[col] = 
						(-val[col][0][q] + val[col][4][q])/4;
			}
			central[q] = (- verticalAverage[0] + 3.0*verticalAverage[1] + 3.0*verticalAverage[2] - verticalAverage[3])/4;
			perp[q]    = (- verticalDiff[0]    + 3.0*verticalDiff[1]    + 3.0*verticalDiff[2]    - verticalDiff[3]   )/4;
			perp2[q]   = (- verticalDiff2[0]   + 3.0*verticalDiff2[1]   + 3.0*verticalDiff2[2]   - verticalDiff2[3]  )/4;
			perp3[q]   = (- verticalDiff3[0]   + 3.0*verticalDiff3[1]   + 3.0*verticalDiff3[2]   - verticalDiff3[3]  )/4;
			
			diff[q]    = (- verticalAverage[0] + 3.0*verticalAverage[1] - 3.0*verticalAverage[2] + verticalAverage[3]);
			diff2[q]   = (- verticalAverage[0] +     verticalAverage[1] -     verticalAverage[2] + verticalAverage[3])/2;
			diff3[q]   = (  verticalAverage[0] -     verticalAverage[1] -     verticalAverage[2] + verticalAverage[3])/2;
		}
		
		SquareMatrix[] mat = equations.diagonalization(i, j, k, mean(L, R), direction);
		SquareMatrix Jx = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		SquareMatrix absJx = mat[0].mult(mat[1].absElementwise()).mult(mat[2]).toSquareMatrix();
		double waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		mat = equations.diagonalization(i, j, k, mean(L, R), otherDirection);
		SquareMatrix Jy = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		for (int ii = 0; ii < mat[1].rows(); ii++){
			mat[1].setElement(ii, ii, Math.signum(mat[1].value(ii,ii)));
		}
		SquareMatrix signJy = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		SquareMatrix absJy = Jy.mult(signJy).toSquareMatrix();
		
		double[] centralFlux = Jx.mult(central);
		double[] term1 = absJx.mult(0.25).mult(diff);
		double[] term2 = signJy.mult(Jx).mult(0.25).mult(perp);
		for (int q = 0; q < quantities; q++){
			flux1d[q] = centralFlux[q] + term1[q]; // + term2[q];
		}
		//U central + absU diff/4 + signV U perp/4
		
		term1 = Jx.mult(Jx).mult(0.5).mult(diff2);
		term2 = Jx.mult(absJx).mult(0.5).mult(diff3);
		double[] term3 = absJy.mult(Jx).mult(0.5).mult(perp2);
		double[] term4 = Jy.mult(Jx).mult(0.5).mult(perp3);
		for (int q = 0; q < quantities; q++){
			flux2d[q] = -term1[q] + term2[q]; // + term3[q] -term4[q];
		}
		
		return new FluxMultiStepAndWaveSpeed(flux1d, flux2d, waveSpeed);

	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt,
			int fluxSolverStep) {
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (dir == GridCartesian.X_DIR){ // we don't know which one to divide by dx, and which by dy
				return dt/((GridCartesian) grid).xSpacing();
			} else if (dir == GridCartesian.Y_DIR){
				return dt/((GridCartesian) grid).xSpacing();
			} else {
				System.err.println("ERROR: asked for non-x, non-y direction");
				return 0.0;
			}
		}
	}

	@Override
	public int steps() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public String getDetailedDescription() {
		return "Beam Warming scheme with statioanrity consistent derivative discretizations";
	}

	
}
