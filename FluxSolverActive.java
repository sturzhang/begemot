import matrices.SquareMatrix;


public abstract class FluxSolverActive extends FluxSolver {
	
	protected Reconstruction reconstruction;
	protected double[][][][][] oldInterfaceValues, halfTimeStepInterfaceValues;
	protected boolean memorySpaceForOldInterfaceValuesAllocated = false;
	
	public static int SIMPSON       = 2;
	public static int TRAPEZOIDAL   = 1;
	public static int ENDOFTIMESTEP = 0;
	protected int fluxTimeIntegration;	
	
	public FluxSolverActive(Grid grid, Equations equations, Reconstruction reconstruction) {
		// TODO remove PointValues from Grid, and PointValueEvolution class
		super(grid, equations);
		this.reconstruction = reconstruction;
		fluxTimeIntegration = ENDOFTIMESTEP;
	}
	
	public FluxSolverActive(Grid grid, Equations equations, Reconstruction reconstruction, int fluxTimeIntegration) {
		// TODO remove PointValues from Grid, and PointValueEvolution class
		super(grid, equations);
		this.reconstruction = reconstruction;
		this.fluxTimeIntegration = fluxTimeIntegration;
	}
	
	@Override
	public double[][][][][][] getAndSetWaveSpeeds(double[][][][][] interfaceValues,
			double[][][][][][] interfaceFluxes, double[][][][] localWaveSpeeds, int timeIntegratorStep, double[][][][][] additionalInterfaceQuantities) {
		/* ATTENTION:
		 * we misuse this function as follows: here we only set the wavespeeds, and when the 
		 * flux summation is called we actually set the fluxes!
		 */
    	SquareMatrix eigenvalues;
		
    	for (GridCell g : grid){
			for (int direction = 0; direction < grid.dimensions(); direction++){
				eigenvalues = equations.diagonalization(g.i(), g.j(), g.k(), grid.conservedQuantities[g.i()][g.j()][g.k()], direction)[1];
				for (int i = 0; i < eigenvalues.rows(); i++){
					chooseCurMaxSignalSpeed( eigenvalues.value(i, i) );
				}
			}
		}
		// dummy return
		return interfaceFluxes;
	}

	@Override
	public double[][][][][] sum(double[][][][][][] interfaceFluxes, double dt,
			double[][][][][] summedInterfaceFluxes, double[][][][] localWaveSpeeds, double[][][][][] additionalInterfaceQuantities) {
    	GridEdgeMapped[] allEdges;
    	double[][] fluxes;
    	double[] flux;
    	double[] normal;
    	double[] interfaceValue = new double[grid.interfaceValues[0][0][0][0].length];
    	
    	// not needed of the pointvalues are set as means of the conservedQuantities
    	//grid.interfaceValues = pointValueEvolution.perform(grid.conservedQuantities, grid.pointValues);
    	if (!memorySpaceForOldInterfaceValuesAllocated){
    		oldInterfaceValues = new double[grid.interfaceValues.length][grid.interfaceValues[0].length][grid.interfaceValues[0][0].length][grid.interfaceValues[0][0][0].length][grid.interfaceValues[0][0][0][0].length];
    		halfTimeStepInterfaceValues = new double[grid.interfaceValues.length][grid.interfaceValues[0].length][grid.interfaceValues[0][0].length][grid.interfaceValues[0][0][0].length][grid.interfaceValues[0][0][0][0].length];
    		memorySpaceForOldInterfaceValuesAllocated = true;
    	}
    	
    	cloneInterfaceValues(oldInterfaceValues);
    	
    	if (fluxTimeIntegration == SIMPSON){
    		performTimeEvolutionOfInterfaceValues(dt/2);
    		cloneInterfaceValues(halfTimeStepInterfaceValues);
    		
    		reconstruction.perform(grid.conservedQuantities, grid.interfaceValues);
    		performTimeEvolutionOfInterfaceValues(dt/2);
    	} else {
    		performTimeEvolutionOfInterfaceValues(dt);
    	}
    	
    	
		for (GridCell g : grid){
    		allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
    		for (int edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++){
    			normal = allEdges[edgeIndex].normalVector();

    			// TODO here fluxes are computed twice (keep track of already known ones and set both at the same time!)
    			
   				if (fluxTimeIntegration == ENDOFTIMESTEP){
   					interfaceValue = grid.interfaceValues[g.i()][g.j()][g.k()][edgeIndex];
   				} else if (fluxTimeIntegration == TRAPEZOIDAL){
	   	    		for (int q = 0; q < interfaceValue.length; q++){
						interfaceValue[q] = 0.5*(
								grid.interfaceValues[g.i()][g.j()][g.k()][edgeIndex][q] + 
								oldInterfaceValues[g.i()][g.j()][g.k()][edgeIndex][q]);
					}
   				} else if (fluxTimeIntegration == SIMPSON){
   					for (int q = 0; q < interfaceValue.length; q++){
						interfaceValue[q] = (grid.interfaceValues[g.i()][g.j()][g.k()][edgeIndex][q] +
									 	 4.0*halfTimeStepInterfaceValues[g.i()][g.j()][g.k()][edgeIndex][q] +
								             oldInterfaceValues[g.i()][g.j()][g.k()][edgeIndex][q]) / 6;
					}
   				}
	   	    		
				fluxes = new double[grid.dimensions()][];
    			for (int dir = 0; dir < grid.dimensions(); dir++){
    				fluxes[dir] = equations.fluxFunction(g.i(), g.j(), g.k(), interfaceValue, dir);
    			}
    			flux = new double[fluxes[0].length];
    			for (int dir = 0; dir < grid.dimensions(); dir++){
    				for (int q = 0; q < flux.length; q++){
    					flux[q] += fluxes[dir][q] * normal[dir] * allEdges[edgeIndex].sizeModified();
    				}
    			}
    			
    			summedInterfaceFluxes[g.i()][g.j()][g.k()][edgeIndex] = flux;
    		}
		}

		return summedInterfaceFluxes;
	}

	protected abstract void performTimeEvolutionOfInterfaceValues(double timeInterval);
	
	protected void cloneInterfaceValues(double[][][][][] cloneTarget){
		for (int i = 0; i < grid.interfaceValues.length; i++){
			for (int j = 0; j < grid.interfaceValues[i].length; j++){
				for (int k = 0; k < grid.interfaceValues[i][j].length; k++){
					for (int edgeIndex = 0; edgeIndex < grid.interfaceValues[i][j][k].length; edgeIndex++){
						for (int q = 0; q < grid.interfaceValues[i][j][k][edgeIndex].length; q++){
							cloneTarget[i][j][k][edgeIndex][q] = grid.interfaceValues[i][j][k][edgeIndex][q]; 
						}
					}
				}
			}
		}
	}
}
