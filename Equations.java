import matrices.SquareMatrix;

public abstract class Equations {

	private int numberOfPassiveScalars;
	protected Grid grid;
	
	
	public Equations(Grid grid, int numberOfPassiveScalars){
		this.numberOfPassiveScalars = numberOfPassiveScalars;
		this.grid = grid;
	}
	
	public abstract double[] fluxFunction(int i, int j, int k, double[] quantities, int direction);
	
	public abstract SquareMatrix[] diagonalization(int i, int j, int k, double[] quantities, int direction);
	public abstract double advectionVelocityForPassiveScalar(int i, int j, int k, 
				double[] quantities, int direction);
	
    public int getTotalNumberOfConservedQuantities(){
    	return getNumberOfPassiveScalars() + getNumberOfConservedQuantities(); 
    }
        
    public abstract int getNumberOfConservedQuantities();
    
    public abstract double[] getTransform(double[] quantities, double[] normalVector);
    public abstract double[] getTransformBack(double[] quantities, double[] normalVector);
    
    public abstract void transformFixedParameters(double[] normalVector);
    public abstract void transformFixedParametersBack(double[] normalVector);
    
    
    public int[] indicesOfPassiveScalars(){
    	int[] res = new int[getNumberOfPassiveScalars()];
    	for (int i = 0; i < res.length; i++){ res[i] = i + getNumberOfConservedQuantities(); }
    	return res;
    }
    
    public double[] transformVector(double vx, double vy, double vz, double[] normalVector){
    	double[] res = new double[3];
    	double[] thirdVector = new double[] {0.0, 0.0, -1.0};
    	
    	// TODO this wouldn't work in 3-d, right?
    	// make these things precomputed
    	
    	double[] secondVector = new double[3];
		secondVector[0] = normalVector[1]*thirdVector[2] - normalVector[2]*thirdVector[1]; 
		secondVector[1] = normalVector[2]*thirdVector[0] - normalVector[0]*thirdVector[2]; 
		secondVector[2] = normalVector[0]*thirdVector[1] - normalVector[1]*thirdVector[0]; 
    	
		res[0] = normalVector[GridCartesian.X_DIR] * vx +
				normalVector[GridCartesian.Y_DIR] * vy + 
				normalVector[GridCartesian.Z_DIR] * vz;

		
		
		res[1] = secondVector[GridCartesian.X_DIR] * vx +
				secondVector[GridCartesian.Y_DIR] * vy + 
				secondVector[GridCartesian.Z_DIR] * vz;
		
		res[2] = thirdVector[GridCartesian.X_DIR] * vx +
				thirdVector[GridCartesian.Y_DIR] * vy + 
				thirdVector[GridCartesian.Z_DIR] * vz;
		
		return res;
    }
    
    public double[] transformVectorBack(double v1, double v2, double v3, double[] normalVector){
    	double[] res = new double[3];
    	
    	res[0] = normalVector[GridCartesian.X_DIR] * v1;
		res[1] = normalVector[GridCartesian.Y_DIR] * v1; 
		res[2] = normalVector[GridCartesian.Z_DIR] * v1;

    	double[] thirdVector = new double[] {0.0, 0.0, -1.0};
    	
    	double[] secondVector = new double[3];
		secondVector[0] = normalVector[1]*thirdVector[2] - normalVector[2]*thirdVector[1]; 
		secondVector[1] = normalVector[2]*thirdVector[0] - normalVector[0]*thirdVector[2]; 
		secondVector[2] = normalVector[0]*thirdVector[1] - normalVector[1]*thirdVector[0]; 
    			
		res[0] += secondVector[GridCartesian.X_DIR] * v2;
		res[1] += secondVector[GridCartesian.Y_DIR] * v2; 
		res[2] += secondVector[GridCartesian.Z_DIR] * v2;
		
		res[0] += thirdVector[GridCartesian.X_DIR] * v3;
		res[1] += thirdVector[GridCartesian.Y_DIR] * v3; 
		res[2] += thirdVector[GridCartesian.Z_DIR] * v3;

		return res;
    }
    
    public int getNumberOfPassiveScalars(){
    	return numberOfPassiveScalars;
    }
    
    public abstract String getDetailedDescription();
    
    public String description(){
    	return "# Equations used: " + getDetailedDescription();
    }  

}