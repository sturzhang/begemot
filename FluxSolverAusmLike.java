
public class FluxSolverAusmLike extends FluxSolverHydro {

	public FluxSolverAusmLike(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	} 
	 
    public String getDetailedDescription(){
    	return "dissected AUSM+up solver for ideal gas";
    }

    @Override
	protected FluxAndWaveSpeed flux(HydroState left, HydroState right) {
		double soundSpeedMean = 0.5 * (left.soundSpeed + right.soundSpeed);
		double vMean = 0.5 * (left.v + right.v);
		double vAbsMax = Math.max(Math.abs(left.v), Math.abs(right.v));
		
		double localWaveSpeed = soundSpeedMean + vAbsMax;
		
		double Kp = 0.0; // stability only if < 0.5
		double prefactor = vMean; // - (soundSpeedLeft + soundSpeedRight)/2*Kp*(pressureRight - pressureLeft)/
				//(rhoMean * soundSpeedMean*soundSpeedMean);
		
		/*double prefactor;
		if (vMean == 0.0){
			prefactor = 1.0;
		} else {
			prefactor = 1.0 - (soundSpeedLeft + soundSpeedRight)/2*Kp*(pressureRight - pressureLeft)/
				(rhoMean * soundSpeedMean*soundSpeedMean) / vMean;
		}*/
		
		
		/*double[] fluxLeft = equations.fluxFunction1DFlowWithoutPressure(left);
    	double[] fluxRight = equations.fluxFunction1DFlowWithoutPressure(right);	    	    	
	    
		double[] fluxLeftWOutV = new double[5], fluxRightWOutV = new double[5];
		for (int q = 0; q <= 4; q++){
	    	if (fluxLeft[q] != 0.0){ 
	    		fluxLeftWOutV[q] = fluxLeft[q] / vLeft; 
	    		if (q == 4){ fluxLeftWOutV[q] -= pressureLeft; } 
	    	} else { fluxLeftWOutV[q] = 0.0; }
	    	if (fluxRight[q] != 0.0){ 
	    		fluxRightWOutV[q] = fluxRight[q] / vRight; 
	    		if (q == 4){ fluxRightWOutV[q] -= pressureRight; } 
		    } else { fluxRightWOutV[q] = 0.0; }
		}*/
		
		
		
		double[] fluxLeft = ((EquationsHydroIdeal) equations).fluxFunction(left);
    	double[] fluxRight = ((EquationsHydroIdeal) equations).fluxFunction(right);	    	    	
	    
		
		double[] fluxRes = new double[5];
		double diff[] = HydroState.diff(right, left);
		
		for (int q = 0; q <= 4; q++){
    	    //fluxRes[q] = 0.5 * prefactor * (fluxLeft[q] + fluxRight[q]) - 
    	    		//0.5 * Math.abs(prefactor) * (fluxRight[q] - fluxLeft[q]);
    	    		
    		
    	    //fluxRes[q] = 0.5 * (fluxLeft[q] + fluxRight[q]) - 
    	    //		0.5 * Math.abs(prefactor) * (fluxRightWOutV[q] - fluxLeftWOutV[q]);
    	    
    	    fluxRes[q] = 0.5 * (fluxLeft[q] + fluxRight[q]) - 0.5 * Math.abs(prefactor) * diff[q];
    	    		
    	    if (Double.isNaN(fluxRes[q])){
    	    	System.err.println("?");
    	    }
    	}
		//fluxRes[indexDirection] += 0.5 *(pressureRight + pressureLeft);
    	
    	return new FluxAndWaveSpeed(fluxRes, localWaveSpeed);

	}



}
