import matrices.SquareMatrix;


public class EquationsPressurelessEuler extends EquationsHydro {

	public static final int INDEX_RHO = 0;
	public static final int INDEX_XMOM = 1;
	public static final int INDEX_YMOM = 2;
	public static final int INDEX_ZMOM = 3;
	
	
	public EquationsPressurelessEuler(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}
	
	public static int getIndexDirection(int direction){
		int indexDirection = -1; // init to make compiler happy
		if (direction == GridCartesian.X_DIR){ indexDirection = EquationsHydroIdeal.INDEX_XMOM; }	
	    if (direction == GridCartesian.Y_DIR){ indexDirection = EquationsHydroIdeal.INDEX_YMOM; }	
	    if (direction == GridCartesian.Z_DIR){ indexDirection = EquationsHydroIdeal.INDEX_ZMOM; }	
	    return indexDirection;
	}

	@Override
	public double[] fluxFunction(int i, int j, int k, double[] quantities, int direction) {
		int whichMomentumAlignedWithDirection = getIndexDirection(direction);
		double[] res = new double[4];
		
		res[INDEX_RHO]    = quantities[whichMomentumAlignedWithDirection];
		res[INDEX_XMOM]   = quantities[whichMomentumAlignedWithDirection]*quantities[INDEX_XMOM]/quantities[INDEX_RHO];
		res[INDEX_YMOM]   = quantities[whichMomentumAlignedWithDirection]*quantities[INDEX_YMOM]/quantities[INDEX_RHO];
		res[INDEX_ZMOM]   = quantities[whichMomentumAlignedWithDirection]*quantities[INDEX_ZMOM]/quantities[INDEX_RHO];
		
		return res;
	}

	@Override
	public SquareMatrix[] diagonalization(int i, int j, int k,
			double[] quantities, int direction) {
		
		System.err.println("ERROR: Diagonalization not implemented for pressureless Euler!");
		
		return null;
	}

	@Override
	public double advectionVelocityForPassiveScalar(int i, int j, int k, double[] quantities, int direction) {
		int indexDirection = getIndexDirection(direction);
		return quantities[indexDirection] / quantities[INDEX_RHO];
	}

	@Override
	public int getNumberOfConservedQuantities() { return 4; }

	@Override
	public double[] getTransform(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		res[INDEX_RHO] = quantities[INDEX_RHO];
		
		double[] transformedVector = transformVector(quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], normalVector);
		res[INDEX_XMOM] = transformedVector[0];
		res[INDEX_YMOM] = transformedVector[1];
		res[INDEX_ZMOM] = transformedVector[2];
		
		return res;

	}

	@Override
	public double[] getTransformBack(double[] quantities, double[] normalVector) {
		double[] res = new double[quantities.length];
		res[INDEX_RHO] = quantities[INDEX_RHO];
		
		
		double[] transformedVector = transformVectorBack(quantities[INDEX_XMOM], quantities[INDEX_YMOM], quantities[INDEX_ZMOM], normalVector);
		res[INDEX_XMOM] = transformedVector[0];
		res[INDEX_YMOM] = transformedVector[1];
		res[INDEX_ZMOM] = transformedVector[2];
		
		return res;
	}

	@Override
	public String getDetailedDescription(){
		return "Ideal pressureless hydrodynamics";
	}

}
