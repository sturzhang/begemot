
public class InitialDataHydroGresho extends InitialDataHydroVortex {

	protected double MachNumber, rho0;
	private double p0;
	protected double rhovariability;
	
	
	public InitialDataHydroGresho(Grid grid, EquationsHydroIdeal equations, double MachNumber, double rhoouter, double rhocenter,
			double[] offsetSpeed, boolean addPressurePerturbation) {
		super(grid, equations, offsetSpeed, addPressurePerturbation);
		init(MachNumber, rhoouter, rhocenter);
	}
	
	public InitialDataHydroGresho(Grid grid, EquationsHydroIdeal equations, double MachNumber, double rhoouter, double rhocenter) {
		super(grid, equations);
		init(MachNumber, rhoouter, rhocenter);
	}
	
	private void init(double MachNumber, double rhoouter, double rhocenter){
		this.MachNumber = MachNumber;
		this.rhovariability = (rhocenter - rhoouter) / 0.04;
		
		/*this.rho0 = rho0 * MachNumber*MachNumber;
		p0 = rho0 / equations.gamma() - 0.5;*/
		
		this.rho0 = rhoouter;
		p0 = rho0 / ((EquationsHydroIdeal) equations).gamma() / MachNumber / MachNumber - 0.5;
	}

    public String getDetailedDescription(){
    	return "Gresho vortex with reference Mach number = " + MachNumber + ", constant density = " + rho0 + 
    			"and central pressure = " + p0;
    }
	
	@Override
	protected double speed(double r) {
		if (r < 0.2){ return 5.0 * r; }
		if (r < 0.4){ return (2.0 - 5.0*r); }
		else { return 0.0; }
	}

	@Override
	protected double pressure(double r) {
		//if (r < 0.2){ return p0 + 12.5 * r*r; }
		if (r < 0.2){ return p0 + r*r*(rhovariability * (6.25*r*r - 10.0/3*r + 0.5 - 1.0/12) + 12.5*rho0 ); }
		if (r < 0.4){ return p0 + 4.0*Math.log(5.0 * r) + 4 - 20.0*r + 12.5 * r*r; }
		else { return p0 + 4.0*Math.log(2.0) - 2.0; }
	}

	protected double rho(double r) {
		if (r < 0.2){ return rho0 + rhovariability * (r-0.2)*(r-0.2); }
		else { return rho0; }
	}

}
