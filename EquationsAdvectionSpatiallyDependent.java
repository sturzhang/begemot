
public abstract class EquationsAdvectionSpatiallyDependent extends EquationsAdvection {

	public EquationsAdvectionSpatiallyDependent(Grid grid, int numberOfPassiveScalars) {
		super(grid, numberOfPassiveScalars);
	}

	public abstract double[] getVelocity(double[] position);
	
	@Override
	public double[] velocityWithoutTransformation(int i, int j, int k) {
		return getVelocity(new GridCell(i,j,k, grid).getPosition());
	}
	
	@Override
	public double flux(int i, int j, int k, double quantity, int direction) {
		return quantity * velocity(i,j,k)[direction];
	}

	@Override
	public double jacobian(int i, int j, int k, double quantity, int direction) {
		return velocity(i,j,k)[direction];
	}

}
