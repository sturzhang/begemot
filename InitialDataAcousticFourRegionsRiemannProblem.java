
public class InitialDataAcousticFourRegionsRiemannProblem extends
		InitialDataAcoustic {

	/*
	 *  u1 v1 | u1 v3   u2 v3 | u2 v5
	 *  -----------------------------
	 *  u3 v1 | u3 v3   u4 v3 | u4 v5
	 *  u3 v2 | u3 v4   u4 v4 | u4 v6
	 *  -----------------------------
	 *  u5 v2 | u5 v4   u6 v4 | u6 v6
	 * 
	 */
	
	protected double u1, u2, u3, u4, u5, u6;
	protected double v1, v2, v3, v4, v5, v6;
	
	public InitialDataAcousticFourRegionsRiemannProblem(Grid grid, EquationsAcousticSimple equations) {
		super(grid, equations);
		
		// u4 - u3 + v3 - v4 = 0
		
		u1 = 1.0;
		u2 = 2.0;
		u3 = 3.0;
		u4 = 3.4;
		u5 = 1.4;
		u6 = -1.0;
		
		v1 = 1.0;
		v2 = 2.0;
		v3 = 3.0;
		v4 = -1.0; //u4 - u3 + v3;
		v5 = 1.4;
		v6 = -1.0;
		
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		double x, y;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		
		
		res[EquationsAcoustic.INDEX_P] = 0.0;
		if (x < -0.5){
			if (y > 0.5){
				res[EquationsAcoustic.INDEX_VX] = u1; res[EquationsAcoustic.INDEX_VY] = v1;
			} else if (y > 0){
				res[EquationsAcoustic.INDEX_VX] = u3; res[EquationsAcoustic.INDEX_VY] = v1;
			} else if (y > -0.5){
				res[EquationsAcoustic.INDEX_VX] = u3; res[EquationsAcoustic.INDEX_VY] = v2;
			} else {
				res[EquationsAcoustic.INDEX_VX] = u5; res[EquationsAcoustic.INDEX_VY] = v2;
			}
		} else if (x < 0){
			if (y > 0.5){
				res[EquationsAcoustic.INDEX_VX] = u1; res[EquationsAcoustic.INDEX_VY] = v3;
			} else if (y > 0){
				res[EquationsAcoustic.INDEX_VX] = u3; res[EquationsAcoustic.INDEX_VY] = v3;
			} else if (y > -0.5){
				res[EquationsAcoustic.INDEX_VX] = u3; res[EquationsAcoustic.INDEX_VY] = v4;
			} else {
				res[EquationsAcoustic.INDEX_VX] = u5; res[EquationsAcoustic.INDEX_VY] = v4;
			}
		} else if (x < 0.5){
			if (y > 0.5){
				res[EquationsAcoustic.INDEX_VX] = u2; res[EquationsAcoustic.INDEX_VY] = v3;
			} else if (y > 0){
				res[EquationsAcoustic.INDEX_VX] = u4; res[EquationsAcoustic.INDEX_VY] = v3;
			} else if (y > -0.5){
				res[EquationsAcoustic.INDEX_VX] = u4; res[EquationsAcoustic.INDEX_VY] = v4;
			} else {
				res[EquationsAcoustic.INDEX_VX] = u6; res[EquationsAcoustic.INDEX_VY] = v4;
			}
			/*
			 *  u1 v1 | u1 v3   u2 v3 | u2 v5
			 *  -----------------------------
			 *  u3 v1 | u3 v3   u4 v3 | u4 v5
			 *  u3 v2 | u3 v4   u4 v4 | u4 v6
			 *  -----------------------------
			 *  u5 v2 | u5 v4   u6 v4 | u6 v6
			 * 
			 */
		} else {
			if (y > 0.5){
				res[EquationsAcoustic.INDEX_VX] = u2; res[EquationsAcoustic.INDEX_VY] = v5;
			} else if (y > 0){
				res[EquationsAcoustic.INDEX_VX] = u4; res[EquationsAcoustic.INDEX_VY] = v5;
			} else if (y > -0.5){
				res[EquationsAcoustic.INDEX_VX] = u4; res[EquationsAcoustic.INDEX_VY] = v6;
			} else {
				res[EquationsAcoustic.INDEX_VX] = u6; res[EquationsAcoustic.INDEX_VY] = v6;
			}
		}
		
		
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Setup similar to jump-inside-cell recon";
	}

}
