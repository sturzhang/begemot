
public class InitialDataAcousticDeltaPeak extends InitialDataAcoustic {

	public InitialDataAcousticDeltaPeak(Grid grid, EquationsAcousticSimple equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		int i0, j0;
		double[] res = new double[q];
		
		
		/*i0 = (grid.indexMinX() + grid.indexMaxX()) / 2;
		j0 = (grid.indexMinY() + grid.indexMaxY()) / 2; */
		
		res[EquationsAcousticSimple.INDEX_VX] = 0.0;
		res[EquationsAcousticSimple.INDEX_VY] = 0.0;
				
		/*if ((g.i() == i0) && (g.j() == j0)){
			res[EquationsAcoustic.INDEX_P] = 1.0 + 1.0 / (grid.xSpacing()*grid.ySpacing());
		} else {
			res[EquationsAcoustic.INDEX_P] = 1.0;
		}*/
		double x = g.getX() - grid.xMidpoint();
		double y = g.getY() - grid.yMidpoint();
		double r2 = x*x + y*y;
		double width = 0.01;
		
		//res[EquationsAcousticSimple.INDEX_P] = 1.0 + 1.0 / (width*width) * Math.exp(- r2 / width/width);
    	
		/*res[EquationsAcousticSimple.INDEX_P] = 1.0;
		for (int i = -3; i <= 3; i++){
			for (int j = -3; j <= 3; j++){
				res[EquationsAcousticSimple.INDEX_P] += Math.exp(- ( (x - 0.03*i)*(x - 0.03*i) + (y - 0.03*j)*(y - 0.03*j)   ) / width/width)/ (width*width);
			}
		}*/
		
		if ((x >= -0.09) && (x < 0.09) &&(y >= -0.09) && (y < 0.09)){
			res[EquationsAcousticSimple.INDEX_P] = 10000.0;
		} else {
			res[EquationsAcousticSimple.INDEX_P] = 0.0;
		}
		
		
		
		return res;
    	
    	
    	
    	
	}

	@Override
	public String getDetailedDescription() {
		return "Delta-peak initial acoustic data.";
	}

}
