
public class InitialDataAdvectionGaussian extends InitialDataScalarAdvection {

	protected double amplitude;
	protected double width;
	protected double offset;
	
	public InitialDataAdvectionGaussian(Grid grid, EquationsAdvection equations, 
			double amplitude, double width, double offset) {
		super(grid, equations);
		this.amplitude = amplitude;
		this.width = width;
		this.offset = offset;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		double x = g.getX() - grid.xMidpoint()/2;
		double y = g.getY() - grid.yMidpoint()/2;
		
		res[0] = amplitude * Math.exp(-(x*x + y*y)/width/width) + offset;
		//res[0] = amplitude * Math.exp(-(x*x)/width/width) + offset;
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Initial data for scalar advection with a grid-centered gaussian";
	}

}
