import matrices.SquareMatrix;


public class FluxSolverEulerEqualEigenvaluesMultiD extends FluxSolverOneStepMultiD {

	// TODO rename class
	
	public FluxSolverEulerEqualEigenvaluesMultiD(GridCartesian grid,
			Equations equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		
		double U = 0, V = 0, waveSpeed = 0; 
		
		if (equations instanceof EquationsHydroIdeal) {
			EquationsHydroIdeal myEqn = (EquationsHydroIdeal) equations; 
			U = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
			V = ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
			waveSpeed = myEqn.getSoundSpeedFromConservative(ijk) + Math.sqrt(U*U + V*V);
		} else if (equations instanceof EquationsAcousticSimple){
			EquationsAcousticSimple myEqn = (EquationsAcousticSimple) equations; 
			U = myEqn.vx();
			V = myEqn.vy();
			waveSpeed = myEqn.soundspeed() + Math.sqrt(U*U + V*V);
		} else {
			System.err.println("ERROR: Equal eigenvalues (multid version) not implemented for equations of type " + equations.getClass().getName());
		}
		
		double[] diff = new double[ijk.length];
		double[] perp = new double[ijk.length];
		double[] central = new double[ijk.length];
		
		
		
		
		double absU = Math.abs(U);
		double sgnV = Math.signum(V);
		
		
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, ijk, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, ip1jk, direction);
		
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, ijp1k, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, ijm1k, direction);
		
		double[] fluxRightTop = equations.fluxFunction(i, j, k, ip1jp1k, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, ip1jm1k, direction);

		for (int q = 0; q < ijk.length; q++){
			//diff[q] = ip1jk[q] - ijk[q];
			//central[q] = 0.5*(fluxLeft[q] + fluxRight[q]);
			//perp[q] = 0;
			
			diff[q] = 0.5*(ip1jk[q] - ijk[q]) + 0.25*(ip1jp1k[q] - ijp1k[q] + ip1jm1k[q] - ijm1k[q]);
			perp[q] = 0.25*(ip1jp1k[q] + ijp1k[q] - ip1jm1k[q] - ijm1k[q]);//*/
			central[q] = 0.125*(fluxLeftTop[q] + 2.0*fluxLeft[q] + fluxLeftBottom[q] 
					+ fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q]); //*/
		}
			
    	double[] fluxRes = new double[ijk.length];
    	    	
    	for (int q = 0; q < ijk.length; q++){
    		fluxRes[q] = central[q] - 0.5*absU*diff[q] - 0.5*sgnV*U*perp[q];
    	}
    	
		return new FluxAndWaveSpeed(fluxRes, waveSpeed);
	}

	@Override
	public String getDetailedDescription() {
		return "Upwind solver with a diagonal upwinding matrix with all eigenvalues the same, multi-dimensionally extended so as to match the upwind scheme when restricted to an entirely 1d-situation";
	}

	

}
