
public class ObstacleHydroSphericalSource extends Obstacle {

	protected double radius;
	protected double radialspeed, density, pressure;
	
	public ObstacleHydroSphericalSource(GridCartesian grid, EquationsHydroIdeal equations, double radius, 
			double density, double pressure, double radialspeed) {
		super(grid, equations);
		this.radius = radius;
		this.radialspeed = radialspeed;
		this.pressure = pressure;
		this.density = density;
	}

	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {
		double r;
		
		for (int i = ((GridCartesian) grid).indexMinX(); i < ((GridCartesian) grid).indexMaxX(); i++){
			for (int j = ((GridCartesian) grid).indexMinY(); j < ((GridCartesian) grid).indexMaxY(); j++){
				r = Math.sqrt(Math.pow(grid.getX(i,j,0) - grid.xMidpoint(), 2) + Math.pow(grid.getY(i,j,0) - grid.yMidpoint(), 2));
				if (r < radius){ excludeFromTimeEvolution[i][j][((GridCartesian) grid).indexMinZ()] = true; }
			}
		}
	}

	@Override
	public double[][][][][] insertRigidWalls(double[][][][][] fluxes,
			double[][][][] conservedQuantities) {
		return fluxes;
	}

	@Override
	public String getDetailedDescription() {
		return "Source in the form of a circle in the x-y-plane with radial speed " + radialspeed + " having radius = " + radius;
	}

	@Override
	public void set(double[][][][] conservedQuantities) {
		double x, y, r, rhoV2;
		
		for (int i = 0; i < conservedQuantities.length; i++){
			for (int j = 0; j < conservedQuantities[i].length; j++){
				x = grid.getX(i,j,0)-grid.xMidpoint();
				y = grid.getY(i,j,0)-grid.yMidpoint();
				r = Math.sqrt(x*x+y*y);
				for (int k = 0; k < conservedQuantities[i][j].length; k++){
					if (excludeFromTimeEvolution[i][j][k]){
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO] = density;
						
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] = radialspeed * density * x/radius;
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] = radialspeed * density * y/radius;
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
						
						rhoV2 = (conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM]*
								conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_XMOM] + 
								conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM]*
								conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM])/
								conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_RHO];
						
						conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_ENERGY] = ( (EquationsHydroIdeal) equations).getEnergy(
								pressure, rhoV2);
						
						for (int s = 5; s < conservedQuantities[i][j][k].length; s++){
							conservedQuantities[i][j][k][s] = 0;
						}
					}
				}
			}
		}
	}

}
