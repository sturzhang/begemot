
public class TimeIntegratorExplicitCubicSpline extends TimeIntegratorExplicit {

	protected EquationsAdvectionConstant equations;
	
	public TimeIntegratorExplicitCubicSpline(Grid1D grid, double CFL, 
			EquationsAdvectionConstant equations) {
		super(grid, CFL);
		this.equations = equations;
	}

	@Override
	public int steps() {
		return 1;
	}

	@Override
	public double[][][][] perform(int step, double dt,
			double[][][][][] interfaceFluxes, double[][][][] sources,
			double[][][][] conservedQuantities,
			boolean[][][] excludeFromTimeIntegration) {
		Grid1D mygrid = (Grid1D) grid;
		
		int j = mygrid.indexMinY();
		int k = mygrid.indexMinZ();
		double nu = dt / mygrid.xSpacing();
		
		double nuCubic = 3*nu*nu - 2*nu*nu*nu;
		
		for (int i = mygrid.indexMinX(); i <= mygrid.indexMaxX(); i++){
			if (!excludeFromTimeIntegration[i][j][k]){
				
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					conservedQuantities[i][j][k][q] += 
							-nuCubic * (interfaceFluxes[i+1][j][k][0][q] - interfaceFluxes[i][j][k][0][q])
							+dt * sources[i][j][k][q];
				}
			}
		}
		
		return conservedQuantities;
	}

}
