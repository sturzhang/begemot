import org.ejml.data.DMatrixRMaj;
import org.ejml.data.DMatrixSparseCSC;
import org.ejml.sparse.csc.CommonOps_DSCC;


public class TimeIntegratorFiniteElementForwardEuler extends TimeIntegratorFiniteElement {

	
	public TimeIntegratorFiniteElementForwardEuler(Grid grid) {
		super(grid);
	}

	
	// A = leftMatrix
	// solving A \dot q + B q = 0 via
	// solving A (q^{n+1} - q^n)/dt= - B q^n 
	// i.e. A q^{n+1} = (- dt B + A) q^n
	
	@Override
	public DMatrixRMaj perform(double dt, DMatrixRMaj solutionNow, int multipleEstimateSparsity) {
		int size = leftMatrix.getNumRows();
		
		solver.setA(leftMatrix); 
		
		DMatrixSparseCSC prefactor = new DMatrixSparseCSC(size,size,multipleEstimateSparsity*size);
		// = -dt B + A
    	CommonOps_DSCC.add(-dt, rightMatrix, 1.0, leftMatrix, prefactor, null, null);
    	    	
    	
    	DMatrixRMaj rightHandSide = new DMatrixRMaj(size,1);
    	CommonOps_DSCC.mult(prefactor, solutionNow, rightHandSide);
		
    	DMatrixRMaj solutionNext = new DMatrixRMaj(size,1);
    	solver.solve(rightHandSide, solutionNext);
    	
    	return solutionNext;
	}

	@Override
	public String getDetailedDescription() {
		return "Forward Euler";
	}


}
