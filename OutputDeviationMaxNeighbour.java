
public class OutputDeviationMaxNeighbour extends Output {

	protected InitialData initial;
	protected int i,j,k;
	
	
	public OutputDeviationMaxNeighbour(double outputTimeInterval, GridCartesian grid,
			Equations equations, InitialData initial) {
		super(outputTimeInterval, grid, equations);
		this.initial = initial;
		
		i = (int) Math.round(0.8*grid.nx());
		j = (int) Math.round(0.2*grid.nx());
		k = 0;
	}

	@Override
	public String getDetailedDescription() {
		return "prints difference between neighbouring values values of all quantities over time";
	}

	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		double diff = 0, difftmp;
		int qq = quantities[0][0][0].length;
		double[] init          = initial.getInitialValue(new GridCell(i  , j, k, grid), qq);
		double[] initNeighbour = initial.getInitialValue(new GridCell(i+1, j, k, grid), qq);
		boolean firsttime;
		
		print(time + " ");
		for (int q = 0; q < qq; q++){
			
			firsttime = true;
			
			for (int i = ((GridCartesian) grid).indexMinX(); i <= ((GridCartesian) grid).indexMaxX(); i++){
				for (int j = ((GridCartesian) grid).indexMinY(); j <= ((GridCartesian) grid).indexMaxY(); j++){
					
					difftmp = Math.abs( (quantities[i][j][k][q] 
							- init[q]
							) 
							- (
									quantities[i+1][j][k][q] 
							- initNeighbour[q]
							) );
				
					if ((diff < difftmp) || (firsttime)){
						diff = difftmp;
					}
					firsttime = false;
				}
			}
			
			print(diff + " ");
		}
		println("");
	}

	@Override
	protected void finalizeOutput() {
		// TODO Auto-generated method stub
		
	}

}
