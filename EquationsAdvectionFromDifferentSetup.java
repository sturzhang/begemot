
public abstract class EquationsAdvectionFromDifferentSetup extends EquationsAdvection {

	//protected double[][][][] conservedQuantities;
	protected Setup hydroSetup;
	
	public EquationsAdvectionFromDifferentSetup(Grid grid, int numberOfPassiveScalars, Setup hydroSetup) {
		super(grid, numberOfPassiveScalars);
		//this.conservedQuantities = conservedQuantities;
		this.hydroSetup = hydroSetup;
	}

	protected abstract double[] getVelocityFromConservedQuantities(double[] conservedQuantities);
	
	@Override
	public double[] velocityWithoutTransformation(int i, int j, int k) {
		//System.err.println(i + " " + j + " " + k + " " + conservedQuantities);
		
		//return getVelocityFromConservedQuantities(conservedQuantities[i][j][k]);
		//return getVelocityFromConservedQuantities(Iteration.setups.get(0).getConservedQuantities()[i][j][k]);
		
		
		
		// TODO does this actually need further transformations? how do trafos work across setups?
		// if trafo is not needed, override velcity() function, rather than this one
		return getVelocityFromConservedQuantities(hydroSetup.getConservedQuantities()[i][j][k]);
	}

	@Override
	public double flux(int i, int j, int k, double quantity, int direction) {
		return quantity * velocity(i, j, k)[direction];
	}

	@Override
	public double jacobian(int i, int j, int k, double quantity, int direction) {
		return velocity(i, j, k)[direction];
	}
	
	
}
