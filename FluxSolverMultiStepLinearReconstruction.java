import matrices.SquareMatrix;


public class FluxSolverMultiStepLinearReconstruction extends FluxSolverMultiStepCartesianSecondOrder {

	public FluxSolverMultiStepLinearReconstruction(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed flux(GridEdgeMapped gridEdge, 
			double[] leftleft, double[] left, double[] right, double[] rightright){

		int qq = left.length;
		
		double[] averageFlux1 = new double[qq];
		double[] averageFlux2 = new double[qq];
		double[] diff1 = new double[qq];
		double[] diff2 = new double[qq];
		double[] average = new double[qq];
		
		// TODO doe snot really work on non-square grids
		double dx = ((GridCartesian) grid).xSpacing();
				
		for (int q = 0; q < qq; q++){
			averageFlux1[q] = 0.125*(-leftleft[q] + 5.0*left[q] + 5.0*right[q] - rightright[q]);
			averageFlux2[q] = 0.125*( leftleft[q] +     left[q] -     right[q] - rightright[q])/dx;
			diff1[q]        = 0.125*( leftleft[q] - 3.0*left[q] + 3.0*right[q] - rightright[q]);
			diff2[q]        = 0.125*(-leftleft[q] +     left[q] +     right[q] - rightright[q])/dx;
			
			average[q]      = 0.5 * (right[q] + left[q]);
		}
		
		GridCell g = gridEdge.adjacent1();
		SquareMatrix[] mat = equations.diagonalization(g.i(), g.j(), g.k(), average, GridCartesian.X_DIR);
		
		SquareMatrix Jacobian = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		averageFlux1 = Jacobian.mult(averageFlux1); // TODO this can be replaced by the flux function
		averageFlux2 = Jacobian.mult(Jacobian.mult(averageFlux2));
		
		
		mat[1] = mat[1].absElementwise();
		double waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(mat[1].value(ii, ii), waveSpeed);
		}
		SquareMatrix absJacobian = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		
		double[] upwinding1 = absJacobian.mult(diff1);
		double[] upwinding2 = absJacobian.mult(Jacobian.mult(diff2));
		
		double[] flux1 = new double[qq];
		double[] flux2 = new double[qq];
		for (int q = 0; q < qq; q++){
			flux1[q] = averageFlux1[q] - upwinding1[q];
			flux2[q] = averageFlux2[q] - upwinding2[q];
		}
		
		return new FluxMultiStepAndWaveSpeed(flux1, flux2, waveSpeed);
	}

	@Override
	public int steps() {
		return 2;
	}

	@Override
	public String getDetailedDescription() {
		return "Fromm scheme implemented in a locally linearized manner";
	}

}
