
public class StencilTurnerOneD extends StencilTurner {

	
	public static final int NEIGHBOUR_LEFT  = 0;
	public static final int NEIGHBOUR_RIGHT = 1;
	
	
	public StencilTurnerOneD(Grid grid) {
		super(grid);
	}

	@Override
	public GridCell[] getStencil(GridCell g, GridEdgeMapped gridEdge) {
		return new GridCell[]{g, gridEdge.getOtherGridCell(g)};
	}

}
