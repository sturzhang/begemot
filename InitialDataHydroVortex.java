public abstract class InitialDataHydroVortex extends InitialDataHydro {

	protected double[] offsetSpeed;
	protected boolean addSoundWave;
	
	public InitialDataHydroVortex(Grid grid, EquationsHydro equations, double[] offsetSpeed, boolean addPressurePerturbation) {
		super(grid, equations);
		this.offsetSpeed = offsetSpeed;
		this.addSoundWave = addPressurePerturbation;
	}
	
	public InitialDataHydroVortex(Grid grid, EquationsHydro equations) {
		super(grid, equations);
		this.offsetSpeed = new double[] {0.0, 0.0, 0.0};
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double r, x, y;
		double rhoV2, pressure;
		double tmp, tmpV;
    	
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		r = Math.sqrt(x*x + y*y); 
		
		double checkerboardamplitude = 0.0;
		
		
		res[EquationsHydroIdeal.INDEX_RHO] = 
				rho(r) 
				; /*+ Math.pow(-1.0, g.i()+g.j())*checkerboardamplitude*0.1
				+ 0.2 * Math.exp(-((x-0.1)*(x-0.1))/0.02/0.02)
				+ 0.2 * Math.exp(-((x-0.3)*(x-0.3))/0.02/0.02); */
    				
		if (r == 0.0){
			res[EquationsHydroIdeal.INDEX_XMOM] = 0.0 + Math.pow(-1.0, g.i()+g.j())*checkerboardamplitude*0.1;
			res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		} else {
			res[EquationsHydroIdeal.INDEX_XMOM] = -rho(r) * (speed(r) * y / r - offsetSpeed[0]) + Math.pow(-1.0, g.i()+g.j())*checkerboardamplitude*0.1;
			res[EquationsHydroIdeal.INDEX_YMOM] =  rho(r) * (speed(r) * x / r + offsetSpeed[1]);
		}
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;

		pressure = pressure(r);
		
		if (addSoundWave){
			tmp = 300.0 * Math.exp(-(x + 0.8)*(x + 0.8) / Math.pow(0.02, 2));
			
			//pressure += 1000.0 * Math.exp(-((x + 0.8)*(x + 0.8) + (y + 0.8)*(y + 0.8))/ 0.04);
			pressure += tmp;
			
			tmpV = res[EquationsHydroIdeal.INDEX_XMOM]/res[EquationsHydroIdeal.INDEX_RHO];
			res[EquationsHydroIdeal.INDEX_RHO] += tmp/(1.4*pressure(0.5)/rho(0.5));
			res[EquationsHydroIdeal.INDEX_XMOM] = res[EquationsHydroIdeal.INDEX_RHO]*(tmpV + tmp/rho(0.5)/Math.sqrt(1.4*pressure(0.5)/rho(0.5)));
		}
		
		rhoV2 = (Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) + 
    						 Math.pow(res[EquationsHydroIdeal.INDEX_YMOM], 2) +
    						 Math.pow(res[EquationsHydroIdeal.INDEX_ZMOM], 2) ) / 
    						res[EquationsHydroIdeal.INDEX_RHO];
    				
		
		

		
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, rhoV2)  + Math.pow(-1.0, g.i()+g.j())*checkerboardamplitude*0.1;
    	
    	for (int ii = 5; ii < res.length; ii++){
    		res[ii] = (x < 0 ? 1.0 : 0.0);
    	}
    	
    	return res;	
	}

	protected abstract double speed(double r);
	protected abstract double pressure(double r);
	protected abstract double rho(double r);
	
	
}
