import java.util.LinkedList;


public class GeometricGridCell {

	protected LinkedList<GeometricGridEdge> myEdges;
	protected ListOfCornerPoints myPoints;
	
	protected GridCell myGridCell;
	
	protected double cellSize;
	protected double perimeter;
	protected double inscribedCircleRadius;
	protected double[] centerOfMass;
	
	protected boolean isValid;
	
	public GeometricGridCell(LinkedList<GeometricGridEdge> edges, ListOfCornerPoints points, boolean isValid) {
		this.myEdges = edges;
		this.myPoints = points;
		this.isValid = isValid;
		
		cellSize = 0;
		perimeter = 0;
		for (GeometricGridEdge e : myEdges){
			e.setCell(this);
			cellSize += e.unitNormalVector()[0] * e.edgeSize() * e.midpoint()[0]; // shoelace formula
			perimeter += e.edgeSize();
		}
		
		if (cellSize < 0){
			for (GeometricGridEdge e : myEdges){
				e.reverseNormalVector();
				// only now the edgelist has consistently outward pointing normal vectors
			}
			cellSize *= -1.0;
		}
		
		inscribedCircleRadius = 2.0*cellSize / perimeter; // the circle actually need not exist
		
		setCenterOfMass();
	}
	
	
	protected void setCenterOfMass(){	
		centerOfMass = new double[2];
		for (CornerPoint p : myPoints){
			centerOfMass[0] += p.x();
			centerOfMass[1] += p.y();
		}
		centerOfMass[0] /= myPoints.size();
		centerOfMass[1] /= myPoints.size();
	}
	
	
	public double                        cellSize(){              return cellSize; }
	public double[]                      center(){                return centerOfMass; }
	public double                        inscribedCircleRadius(){ return inscribedCircleRadius; }
	public LinkedList<GeometricGridEdge> edges(){                 return myEdges; }
	public ListOfCornerPoints            points(){                return myPoints; }
	public GridCell                      gridCell(){              return myGridCell; }
	public boolean                       isValid(){               return isValid; }
	
	public void setGridCell(GridCell gridCell){
		myGridCell = gridCell; // recall that contrary to GeometricGridCell, a GridCell only carries info about memory slots
	}
	
	
}
