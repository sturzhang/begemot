
public class OutputErrorNormAdvectionConstant extends OutputErrorNorm {

	protected Boundary boundary;
	
	public OutputErrorNormAdvectionConstant(double outputTimeInterval, GridCartesian grid,
			EquationsAdvectionConstant equations, InitialData initial, Boundary boundary, int exponent) {
		super(outputTimeInterval, grid, equations, initial, exponent);
		this.equations = equations;
		this.boundary = boundary;
	}

	@Override
	protected double[] getExactSolution(int i, int j, int k, int numberOfConservedQuantities, double time){
		double[] vel = ((EquationsAdvectionConstant) equations).velocity(i,j,k);
		
		int xIndex = ((GridCartesian) grid).getXIndex(grid.getX(i,j,k) - vel[0]*time);
		int yIndex = ((GridCartesian) grid).getYIndex(grid.getY(i,j,k) - vel[1]*time);
		int zIndex = ((GridCartesian) grid).getZIndex(grid.getZ(i,j,k) - vel[2]*time);
		
		while (xIndex < ((GridCartesian) grid).indexMinX()){ xIndex += ((GridCartesian) grid).nx(); }
		while (yIndex < ((GridCartesian) grid).indexMinY()){ yIndex += ((GridCartesian) grid).ny(); }
		while (zIndex < ((GridCartesian) grid).indexMinZ()){ zIndex += ((GridCartesian) grid).nz(); }
			
		while (xIndex > ((GridCartesian) grid).indexMaxX()){ xIndex -= ((GridCartesian) grid).nx(); }
		while (yIndex > ((GridCartesian) grid).indexMaxY()){ yIndex -= ((GridCartesian) grid).ny(); }
		while (zIndex > ((GridCartesian) grid).indexMaxZ()){ zIndex -= ((GridCartesian) grid).nz(); }
		
		return initial.getInitialValue(new GridCell(xIndex, yIndex, zIndex, grid), numberOfConservedQuantities);
	}
	
}
