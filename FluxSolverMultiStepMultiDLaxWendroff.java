import matrices.SquareMatrix;


public class FluxSolverMultiStepMultiDLaxWendroff extends FluxSolverMultiStepMultiDCartesianFirstOrder {

	public FluxSolverMultiStepMultiDLaxWendroff(GridCartesian grid, Equations equations) {
		super(grid, equations);
	}

	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LT, double[] L, double[] LB,
			double[] RT, double[] R, double[] RB, int timeIntegratorStep) {
		
		boolean verbose = false;
		/*boolean verbose = ((
					(gridEdge.adjacent1().i() == 38) && (gridEdge.adjacent1().j() == 26)
				) || (
						(gridEdge.adjacent2().i() == 38) && (gridEdge.adjacent2().j() == 26)
						));
		*/
		
		if (verbose){
			System.err.println("--------------");
			System.err.println("(" + gridEdge.adjacent1().i() + ", " + gridEdge.adjacent1().j() + ")---(" 
					+ gridEdge.adjacent2().i() + ", " + gridEdge.adjacent2().j() + ")");
		}
		
		GridCell g = gridEdge.adjacent1();
		int i = g.i(); int j = g.j(); int k = g.k();
		int direction = GridCartesian.X_DIR;
		int otherDirection = GridCartesian.Y_DIR;
		
		int quantities = equations.getNumberOfConservedQuantities();
		
		double[] diff    = new double[quantities];
		double[] perp    = new double[quantities];
		double[] central = new double[quantities];
		double[] upwindingTermX, upwindingTermY;
		
		
		double[] flux1d = new double[quantities];
		double[] flux2d = new double[quantities];
		
		
		double[] fluxLeft = equations.fluxFunction(i, j, k, L, direction);
		double[] fluxRight = equations.fluxFunction(i, j, k, R, direction);
		
		double[] fluxLeftTop = equations.fluxFunction(i, j, k, LT, direction);
		double[] fluxLeftBottom = equations.fluxFunction(i, j, k, LB, direction);
		double[] fluxRightTop = equations.fluxFunction(i, j, k, RT, direction);
		double[] fluxRightBottom = equations.fluxFunction(i, j, k, RB, direction);
		
		double[] fluxPerpLeftTop = equations.fluxFunction(i, j, k, LT, otherDirection);
		double[] fluxPerpLeftBottom = equations.fluxFunction(i, j, k, LB, otherDirection);
		double[] fluxPerpRightTop = equations.fluxFunction(i, j, k, RT, otherDirection);
		double[] fluxPerpRightBottom = equations.fluxFunction(i, j, k, RB, otherDirection);
		
		double verticalAverageFluxLeft, verticalAverageFluxRight;

		for (int q = 0; q < quantities; q++){ 
			verticalAverageFluxLeft = 0.25*(fluxLeftTop[q] + 2.0*fluxLeft[q] + fluxLeftBottom[q]);
			verticalAverageFluxRight = 0.25*(fluxRightTop[q] + 2.0*fluxRight[q] + fluxRightBottom[q]);
			diff[q] = verticalAverageFluxRight - verticalAverageFluxLeft; 
			
			//perp[q] = 0.25*(fluxRightTop[q] - fluxRightBottom[q] + fluxLeftTop[q] - fluxLeftBottom[q]);
			perp[q] = 0.25*(fluxPerpRightTop[q] - fluxPerpRightBottom[q] + fluxPerpLeftTop[q] - fluxPerpLeftBottom[q]);
			
			central[q] = 0.5*(verticalAverageFluxLeft + verticalAverageFluxRight);
		}
		
		if (verbose){
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": diff[q] = " + diff[q]); }
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": perp[q] = " + perp[q]); }
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": central[q] = " + central[q]); }
		}
		
		SquareMatrix[] mat = equations.diagonalization(i, j, k, mean(L, R), direction);
		SquareMatrix Jx = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
		
		if (verbose){ 
			System.err.println("Jx = ");
			System.err.println(Jx);
		}
		
		double waveSpeed = 0.0;
		for (int ii = 0; ii < mat[1].rows(); ii++){
			if (Double.isNaN(mat[1].value(0, 0))){
				System.err.println("Eigenvalue #" + ii + " NaN: ");
			}
			waveSpeed = Math.max(Math.abs(mat[1].value(ii, ii)), waveSpeed);
		}
		
		
		
		//mat = equations.diagonalization(i, j, k, mean(L, R), otherDirection);
		//SquareMatrix Jy = mat[0].mult(mat[1]).mult(mat[2]).toSquareMatrix();
				
		flux1d = central;
		
		upwindingTermX = Jx.mult(0.5).mult(diff);
		//upwindingTermY = Jy.mult(0.5).mult(perp);
		upwindingTermY = Jx.mult(0.5).mult(perp);
		
		if (verbose){
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": upwindingTermX[q] = " + upwindingTermX[q]); }
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": upwindingTermY[q] = " + upwindingTermY[q]); }
		}
		
		for (int q = 0; q < quantities; q++){
    		flux2d[q] = - upwindingTermX[q] - upwindingTermY[q];
    		if ((Double.isNaN(flux2d[q])) || (Double.isNaN(flux1d[q]))){
    	    	System.err.println("fluxes are NaN");
    	    }
    	}
		
		if (verbose){
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": flux1d[q] = " + flux1d[q]); }
			for (int q = 0; q < quantities; q++){ System.err.println("q = " + q + ": flux2d[q] = " + flux2d[q]); }
		}
		
		
		return new FluxMultiStepAndWaveSpeed(flux1d, flux2d, waveSpeed);
		
	}

	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep) {
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (dir == GridCartesian.X_DIR){ // we don't know which one to divide by dx, and which by dy
				return dt/((GridCartesian) grid).xSpacing();
			} else if (dir == GridCartesian.Y_DIR){
				return dt/((GridCartesian) grid).xSpacing();
			} else {
				System.err.println("ERROR: asked for non-x, non-y direction");
				return 0.0;
			}
		}
	}

	@Override
	public int steps() { return 2; }


	@Override
	public String getDetailedDescription() {
		return "Lax-Wendroff-type method with the Cauchy Kowalewskaya procedure in multi-d as a guiding principle";
	}

}
