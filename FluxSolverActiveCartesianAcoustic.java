
public class FluxSolverActiveCartesianAcoustic extends FluxSolverActiveCartesian {

	public FluxSolverActiveCartesianAcoustic(GridCartesian grid, EquationsAcousticSimple equations, Reconstruction reconstruction) {
		super(grid, equations, reconstruction);
	}
	
	public FluxSolverActiveCartesianAcoustic(GridCartesian grid, Equations equations, Reconstruction reconstruction, int fluxTimeIntegration) {
		super(grid, equations, reconstruction, fluxTimeIntegration);
	}

	@Override
	protected double[] forwardToEvolution(double pos, double timeInterval,
			Reconstruction recon, int direction, GridEdgeMapped edge) {
		return ((EquationsAcousticSimple) equations).evolve1d(
				pos, timeInterval, (ReconstructionParabolic) reconstruction, direction, edge);
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
