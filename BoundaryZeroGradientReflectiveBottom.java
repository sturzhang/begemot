
public class BoundaryZeroGradientReflectiveBottom extends Boundary {

		protected EquationsHydroIdeal equations;
		
		public BoundaryZeroGradientReflectiveBottom(GridCartesian grid, EquationsHydroIdeal equations) {
			super(grid);
			this.equations = equations;
		}

		@Override
		protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
			int i, j, k;
			int nextI, nextJ, nextK;
			boolean switchX, switchY, switchZ;
			
			i = g.i(); j = g.j(); k = g.k();
			
			switchX = false; switchY = false; switchZ = false;
			nextI = i; nextJ = j; nextK = k;
		
			if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); }
			if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
				
			if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }
			if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }

			if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); switchY = true; }							
			if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }

			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
			}
				
			if (switchY){
				conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] *= -1.0;
			}
		}
						
						

		@Override
		public String getDetailedDescription() {
	    	return "Reflective at the bottom, otherwise zero gradients";
		}


}
