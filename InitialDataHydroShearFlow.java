
public class InitialDataHydroShearFlow extends InitialDataHydro {

	public InitialDataHydroShearFlow(Grid grid, EquationsHydro equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}
	
	

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		
		res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
		
		res[EquationsHydroIdeal.INDEX_XMOM] = 0.1 * Math.sin(
				(g.getY() - grid.yMidpoint()) / (grid.ymax() - grid.ymin()) * 2.0*Math.PI) * res[EquationsHydroIdeal.INDEX_RHO];
		double rhoV2 = Math.pow(res[EquationsHydroIdeal.INDEX_XMOM], 2) / res[EquationsHydroIdeal.INDEX_RHO];
		
		double pressure = 1.0/1.4;
		
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
				((EquationsHydroIdeal) equations).getEnergy(
    								pressure, rhoV2);
 
		res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
		
				
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "shear flow";
	}

}
