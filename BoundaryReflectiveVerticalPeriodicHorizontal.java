
public class BoundaryReflectiveVerticalPeriodicHorizontal extends Boundary {

	protected EquationsHydroIdeal equations;
	
	public BoundaryReflectiveVerticalPeriodicHorizontal(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid);
		this.equations = equations;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k;
		int nextI, nextJ, nextK;
		boolean switchX, switchYpos, switchYneg, switchZ;
		
		i = g.i(); j = g.j(); k = g.k();
		
		switchX = false; switchYpos = false; switchYneg = false; switchZ = false;
		nextI = i; nextJ = j; nextK = k;
	
		if (i < ((GridCartesian) grid).indexMinX()){ nextI += ((GridCartesian) grid).nx(); }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK += ((GridCartesian) grid).nz(); }
			
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI -= ((GridCartesian) grid).nx(); }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK -= ((GridCartesian) grid).nz(); }

		if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); switchYneg = true; }							
		if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); switchYpos = true; }

		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
		}
			
		
		// TODO wrong implementation!
		if (switchYpos){
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] *= 1.0;
		} else if (switchYneg){
			conservedQuantities[i][j][k][EquationsHydroIdeal.INDEX_YMOM] *= 1.0;
		}
	}
					
					

	@Override
	public String getDetailedDescription() {
    	return "Reflective on top and bottom and periodic to the left and right";
	}

}
