public class GridCellCartesian extends GridCell {

	protected GridCartesian myGrid;
	
	public GridCellCartesian(int i, int j, int k, GridCartesian grid){
		super(i, j, k, grid);
		myGrid = grid;
	}
	
	public double getX(){ return myGrid.xmin() + (i - myGrid.ghostcellsX() + 0.5) / myGrid.nx() * (myGrid.xmax() - myGrid.xmin()); }
    public double getY(){ return myGrid.ymin() + (j - myGrid.ghostcellsY() + 0.5) / myGrid.ny() * (myGrid.ymax() - myGrid.ymin()); }
    public double getZ(){ return myGrid.zmin() + (k - myGrid.ghostcellsZ() + 0.5) / myGrid.nz() * (myGrid.zmax() - myGrid.zmin()); }
	

}
