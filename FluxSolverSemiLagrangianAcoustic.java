
public class FluxSolverSemiLagrangianAcoustic extends FluxSolverSemiLagrangian {

	protected boolean stationarityConsistent;
	
	public FluxSolverSemiLagrangianAcoustic(GridCartesian grid, EquationsAcousticSimple equations, boolean stationarityConsistent) {
		super(grid, equations);
		this.stationarityConsistent = stationarityConsistent;
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		
		
		int quantities = ijk.length;
		double[] fluxRes = new double[quantities];
		EquationsAcousticSimple eq = ((EquationsAcousticSimple) equations);
		
		
		double c = eq.soundspeed();
		double maxEVal = c + Math.abs(eq.vx()) +  Math.abs(eq.vy());
		//double maxEVal = Math.max(c, Math.max(Math.abs(eq.vx()), Math.abs(eq.vy())));
		// TODO maybe here |v| instead?
		
		if (timeIntegratorStep == 1){
			double uip1j   = ip1jk[EquationsAcoustic.INDEX_VX];
			double uij     = ijk[EquationsAcoustic.INDEX_VX];
			
			double pip1j   = ip1jk[EquationsAcoustic.INDEX_P];
			double pij     = ijk[EquationsAcoustic.INDEX_P];
			
			double diffP, diffU, diffV, sumP, sumU; 
			
			if (stationarityConsistent){
				double uip1jp1 = ip1jp1k[EquationsAcoustic.INDEX_VX];
				double uijp1   = ijp1k[EquationsAcoustic.INDEX_VX];
				double uip1jm1 = ip1jm1k[EquationsAcoustic.INDEX_VX];
				double uijm1   = ijm1k[EquationsAcoustic.INDEX_VX];
			
				double pip1jp1 = ip1jp1k[EquationsAcoustic.INDEX_P];
				double pijp1   = ijp1k[EquationsAcoustic.INDEX_P];
				double pip1jm1 = ip1jm1k[EquationsAcoustic.INDEX_P];
				double pijm1   = ijm1k[EquationsAcoustic.INDEX_P];
				
				double vip1jp1 = ip1jp1k[EquationsAcoustic.INDEX_VY];
				double vijp1   = ijp1k[EquationsAcoustic.INDEX_VY];
				double vip1jm1 = ip1jm1k[EquationsAcoustic.INDEX_VY];
				double vijm1   = ijm1k[EquationsAcoustic.INDEX_VY];
			
						
				diffP = 0.25*(pip1jp1 - pijp1 + 2.0*(pip1j - pij) + pip1jm1 - pijm1);
				diffU = 0.25*(uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1);
				
				diffV = 0.25*(vijp1 - vijm1 + vip1jp1 - vip1jm1);
				
				sumP  = 0.25*(pip1jp1 + pijp1 + 2.0*(pip1j + pij) + pip1jm1 + pijm1);
				sumU  = 0.25*(uip1jp1 + uijp1 + 2.0*(uip1j + uij) + uip1jm1 + uijm1);	
			} else {
				diffP = pip1j - pij;
				sumP = pip1j + pij;
				diffU = uip1j - uij;
				sumU = uip1j + uij;
				
				diffV = 0;
			}
			
			double uStar = 0.5*sumU - 0.5/c*diffP;
			double PiStar = 0.5*sumP - c/2*(diffU + diffV);
			
			if (eq.symmetric()){
				fluxRes[EquationsAcoustic.INDEX_VX] = c*PiStar;
				fluxRes[EquationsAcoustic.INDEX_VY] = 0;
				fluxRes[EquationsAcoustic.INDEX_VZ] = 0;
				fluxRes[EquationsAcoustic.INDEX_P] = c*uStar;
			} else {
				fluxRes[EquationsAcoustic.INDEX_VX] = PiStar;
				fluxRes[EquationsAcoustic.INDEX_VY] = 0;
				fluxRes[EquationsAcoustic.INDEX_VZ] = 0;
				fluxRes[EquationsAcoustic.INDEX_P] = c*c*uStar;				
			}
				
			if ((Double.isNaN(c)) || (Double.isNaN(uStar))){
				System.err.println("ERROR: NaN");
			}
		} else if (timeIntegratorStep == 2){
			double U = eq.vx();
			double absU = Math.abs(U);
			
			for (int q = 0; q < quantities; q++){
				fluxRes[q] = U*(ip1jk[q] + ijk[q])/2 - absU*(ip1jk[q] - ijk[q])/2;
			}		
			
			maxEVal = absU;
		} else {
			System.err.println("ERROR: No flux computation foreseen for timeIntegratorStep > 2!");
			maxEVal = 0;
		}
		return new FluxAndWaveSpeed(fluxRes, maxEVal);
		
		
	}

	@Override
	public String getDetailedDescription() {
		return "Semi-Lagrangian acoustic flux solver!";
	}

}
