
public class InitialDataIsentropicTaylorGreen extends InitialDataIsentropic {

	public InitialDataIsentropicTaylorGreen(Grid grid,
			EquationsIsentropicEuler equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double x = g.getX() - grid.xMidpoint();
    	double y = g.getY() - grid.yMidpoint();
    	double r = Math.sqrt(x*x + y*y);
    	double k = 20.0/grid.xMidpoint();
    	double v = 0.2;
    	
    	double vTangential;
    	if (r < grid.xMidpoint()*0.7){
    		vTangential = r/grid.xMidpoint() * (grid.xMidpoint()*0.7 - r)/grid.xMidpoint();
    	} else {
    		vTangential = 0;
    	}
    	
    	res[EquationsIsentropicEuler.INDEX_RHO] = 1.0;
    				
    	res[EquationsIsentropicEuler.INDEX_XMOM] =  res[EquationsIsentropicEuler.INDEX_RHO]*(
    			v*Math.sin(k*x)*Math.cos(k*y) - (r == 0 ? 0 : vTangential * y / r));
    	res[EquationsIsentropicEuler.INDEX_YMOM] =  res[EquationsIsentropicEuler.INDEX_RHO]*(
    			-v*Math.cos(k*x)*Math.sin(k*y) + (r == 0 ? 0 : vTangential * x / r));
    	res[EquationsIsentropicEuler.INDEX_ZMOM] = 0.0;
    			
    		
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Taylor-Green vortex, simplified for 2-d isentropic eqautions";
	}

}
