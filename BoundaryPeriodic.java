
public class BoundaryPeriodic extends Boundary {

	public BoundaryPeriodic(GridCartesian grid) {
		super(grid);
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		
		i = g.i(); j = g.j(); k = g.k();
		nextI = i; nextJ = j; nextK = k;
							
		if (i < ((GridCartesian) grid).indexMinX()){ nextI += ((GridCartesian) grid).nx(); }
		if (j < ((GridCartesian) grid).indexMinY()){ nextJ += ((GridCartesian) grid).ny(); }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK += ((GridCartesian) grid).nz(); }
							
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI -= ((GridCartesian) grid).nx(); }
		if (j > ((GridCartesian) grid).indexMaxY()){ nextJ -= ((GridCartesian) grid).ny(); }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK -= ((GridCartesian) grid).nz(); }

		for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
			conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Periodic";
	}

}
