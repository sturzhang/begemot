
public class InitialDataGreshoVorticity extends InitialDataScalarAdvection {

	public InitialDataGreshoVorticity(Grid grid, EquationsAdvectionGresho equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		double x = g.getX() - grid.xMidpoint();
		double y = g.getY() - grid.yMidpoint();
		double r = Math.sqrt(x*x+y*y);
		double width = ((EquationsAdvectionGresho) equations).width();
		
		if (r < width){
			res[0] = 1.0/width;
		} else {
			if (r < 2*width){ 
				res[0] = -1.0/width;
			} else {
				res[0] = 0.0;
			}
		}
		res[0] += 10.0*Math.exp(-((x-0.2)*(x-0.2) + y*y)/0.1/0.1);
		
		return res; 
	}

	@Override
	public String getDetailedDescription() {
		return "The advected quantity is set to the vorticity of the Gresho vortex";
	}

}
