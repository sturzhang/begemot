import java.util.LinkedList;


public class TestSeriesAcoustics extends TestSeries {

	// NOTE: one should pay attention to the iteration having an output after time has reached finaltime
	
	public TestSeriesAcoustics() {
		
		double finaltime = 0.05;
		double mach = 0.5;
		
		EquationsAcousticSimple equations;
		
		LinkedList<Reconstruction> reconstructions = new LinkedList<Reconstruction>();
    	reconstructions.add(new ReconstructionPiecewiseConstant());
    	//reconstructions.add(new ReconstructionWENO(1));  // WENO3
    	//reconstructions.add(new ReconstructionWENO(2));  // WENO5
    	
    	LinkedList<InitialData> initialData;
    	LinkedList<Boundary> boundaries;
		LinkedList<FluxSolver> fluxSolvers;
    	LinkedList<TimeIntegratorFiniteVolume> timeIntegrators;		
		
    	for (Reconstruction reconstruction: reconstructions){
    		LinkedList<GridCartesian> grids = new LinkedList<GridCartesian>();
    		/*grids.add(new Grid1D(0.0, 1.0, 51, 
    				reconstruction.numberOfGhostcells())); //*/
    		/*grids.add(new Grid1D(-1.0, 1.0, 101, 
    				reconstruction.numberOfGhostcells()));//*/
        	grids.add(new GridCartesian2D(-1.5, 1.0, 51, 0.3, 1.3, 51, 
    				reconstruction.numberOfGhostcells()));//*/
        	grids.add(new GridCartesian2D(-1.5, 1.0, 51, 0.3, 1.3, 51, 
    				reconstruction.numberOfGhostcells()));//*/
    	
        	for (GridCartesian grid : grids){
        		equations = new EquationsAcousticSimple(grid, 0, 1.0/mach, false, new double[] {0.0, 0.0, 0.0});
            	
        		initialData = new LinkedList<InitialData>();
    	    	//initialData.add(new InitialDataSphericalShockAcoustic(grid, equations));
    	    	//initialData.add(new InitialDataQaudraticAcoustic(grid, equations, 0.2, 1.0, 1.0/mach/mach));
    	    	initialData.add(new InitialDataAcousticGaussianGresho(grid, equations, 0.0, 0.2, 0.0, 1.0/mach/mach));
    	    	//initialData.add(new InitialDataDeltaPeak(grid, equations));
    	    	//initialData.add(new InitialDataRiemannProblem(grid, equations));
    	    	//initialData.add(new InitialDataAcousticPertubationEvolution(grid, equations));
    	    	//initialData.add(new InitialAtmosphereAcoustic(grid, equations, -1.0, true));
 
    	    	boundaries = new LinkedList<Boundary>();
    	    	boundaries.add(new BoundaryPeriodic(grid));
    	    	boundaries.add(new BoundaryZeroGradient(grid));
    	    	//boundaries.add(BoundaryFixedToInitial(grid, initialDatum));
    	    	
    	    	fluxSolvers = new LinkedList<FluxSolver>();
    	    	fluxSolvers.add(new FluxSolverRoe(grid, equations));
    	    	fluxSolvers.add(new FluxSolverRoePreconditionedAcoustic(grid, equations, "0201"));
    	    	fluxSolvers.add(new FluxSolverOneStepMultiDAcousticBichar(grid, equations));
    	    	if (grid instanceof GridCartesian2D){
    	    		fluxSolvers.add(new FluxSolverVertexAcousticBichar((GridCartesian2D) grid, equations));
    	    	}
 
    	    	timeIntegrators = new LinkedList<TimeIntegratorFiniteVolume>();
    	    	timeIntegrators.add(new TimeIntegratorExplicitConservative(grid, 0.2));
    	    	timeIntegrators.add(new TimeIntegratorRungeKutta2(grid, 0.3));
    	    	timeIntegrators.add(new TimeIntegratorRungeKutta3(grid, 0.3));
    	    	//timeIntegrators.add(new TimeIntegratorSemiImplicitAcoustic(grid, 0.5, equations));
    	
    	    	for (InitialData initialDatum : initialData){
    	    		for (Boundary boundary : boundaries){
    	    			for (FluxSolver fluxSolver : fluxSolvers){
    	    				for (TimeIntegratorFiniteVolume timeIntegrator : timeIntegrators){
    	    					add(new Test(
		    						reconstruction, grid, equations,
		    						new SourceZero(grid), new ObstacleNone(grid), 
		    						initialDatum, boundary, fluxSolver, timeIntegrator,
		    						new OutputErrorNorm(finaltime, grid, equations, initialDatum, 1),
		    						finaltime)
    	    					);
    	    				}
    	    			}
    	    		}
    	    	}
        	}
    	}
	}

}
