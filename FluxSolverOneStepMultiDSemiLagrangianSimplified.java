
public class FluxSolverOneStepMultiDSemiLagrangianSimplified extends FluxSolverOneStepMultiD {

	// this solver cannot really be used, as the perpendicular velocity cannot be computed!
	
	public FluxSolverOneStepMultiDSemiLagrangianSimplified(GridCartesian grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	protected FluxAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] ijp1k, double[] ijk, double[] ijm1k, double[] ip1jp1k,
			double[] ip1jk, double[] ip1jm1k, double[] ijp1kp1, double[] ijkp1,
			double[] ijm1kp1, double[] ip1jp1kp1, double[] ip1jkp1,
			double[] ip1jm1kp1, double[] ijp1km1, double[] ijkm1,
			double[] ijm1km1, double[] ip1jp1km1, double[] ip1jkm1,
			double[] ip1jm1km1, int timeIntegratorStep) {
		
		int quantities = ijk.length;
		double[] fluxRes = new double[quantities];
		double maxEVal;
		
		EquationsHydroIdeal eq = ((EquationsHydroIdeal) equations);
		
		double uStar, relaxationSpeed, PiStar;
		
		double uip1j   = ip1jk[EquationsHydroIdeal.INDEX_XMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double uij     = ijk[EquationsHydroIdeal.INDEX_XMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double uip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double uijp1   = ijp1k[EquationsHydroIdeal.INDEX_XMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double uip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_XMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double uijm1   = ijm1k[EquationsHydroIdeal.INDEX_XMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
		
		double vip1jp1 = ip1jp1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jp1k[EquationsHydroIdeal.INDEX_RHO];
		double vijp1   = ijp1k[EquationsHydroIdeal.INDEX_YMOM]/ijp1k[EquationsHydroIdeal.INDEX_RHO];
		double vij     = ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO];
		double vip1j   = ip1jk[EquationsHydroIdeal.INDEX_YMOM]/ip1jk[EquationsHydroIdeal.INDEX_RHO];
		double vip1jm1 = ip1jm1k[EquationsHydroIdeal.INDEX_YMOM]/ip1jm1k[EquationsHydroIdeal.INDEX_RHO];
		double vijm1   = ijm1k[EquationsHydroIdeal.INDEX_YMOM]/ijm1k[EquationsHydroIdeal.INDEX_RHO];
				
		double pip1j   = eq.getPressureFromConservative(ip1jk);
		double pij     = eq.getPressureFromConservative(ijk);
		double pip1jp1 = eq.getPressureFromConservative(ip1jp1k);
		double pijp1   = eq.getPressureFromConservative(ijp1k);
		double pip1jm1 = eq.getPressureFromConservative(ip1jm1k);
		double pijm1   = eq.getPressureFromConservative(ijm1k);
		
		double cR = eq.getSoundSpeedFromConservative(ip1jk);
		double cL = eq.getSoundSpeedFromConservative(ijk);
		
		relaxationSpeed = Math.max(cR*ip1jk[EquationsHydroIdeal.INDEX_RHO], 
				cL*ijk[EquationsHydroIdeal.INDEX_RHO]);
		
		double diffP = 0.25*(pip1jp1 - pijp1 + 2.0*(pip1j - pij) + pip1jm1 - pijm1);
		double diffU = 0.25*(uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1);
		
		double diffV = 0.25*(vijp1 - vijm1 + vip1jp1 - vip1jm1);
		
		double sumP  = 0.25*(pip1jp1 + pijp1 + 2.0*(pip1j + pij) + pip1jm1 + pijm1);
		double sumU  = 0.25*(uip1jp1 + uijp1 + 2.0*(uip1j + uij) + uip1jm1 + uijm1);
		
		
		uStar  = 0.5*sumU - 0.5/relaxationSpeed*diffP;
		PiStar = 0.5*sumP - relaxationSpeed/2*(diffU + diffV);	
		
			
		fluxRes[EquationsHydroIdeal.INDEX_RHO]    = 0; 
		fluxRes[EquationsHydroIdeal.INDEX_XMOM]   = PiStar;
		fluxRes[EquationsHydroIdeal.INDEX_YMOM]   = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ZMOM]   = 0;
		fluxRes[EquationsHydroIdeal.INDEX_ENERGY] = PiStar*uStar;
			
		maxEVal = relaxationSpeed/Math.min(ip1jk[EquationsHydroIdeal.INDEX_RHO], ijk[EquationsHydroIdeal.INDEX_RHO]) + Math.abs(uStar);
			
		
		boolean simpleAdvection              = false;
		
		
		
		if (simpleAdvection){
			double absUStar = Math.abs(uStar);
			double signUStar = (uStar == 0 ? 0 : uStar/absUStar);
			double V = 0;
			
			double uX, uY;
			double[] pos = gridEdge.midpoint();
			double x = pos[0] - grid.xMidpoint();
			double y = pos[1] - grid.yMidpoint();
			double r = Math.sqrt(x*x+y*y);
			if (r < 0.2){      uX = -5.0*y;           uY = 5.0*x; }
			else if (r < 0.4){ uX = -(2 - 5.0*r)*y/r; uY = (2 - 5.0*r)*x/r; }
			else {             uX = 0;                uY = 0; } //*/
		
			double uNormal = uX * gridEdge.normalVector()[0] + uY * gridEdge.normalVector()[1];
			//uStar = uNormal;
			double uPerpX = uX - uNormal * gridEdge.normalVector()[0];
			double uPerpY = uY - uNormal * gridEdge.normalVector()[1];
			//V = Math.sqrt(uPerpX*uPerpX + uPerpY*uPerpY);
			V = -uPerpX * gridEdge.normalVector()[1] + uPerpY * gridEdge.normalVector()[0];
			
			//V = Math.signum(ijk[EquationsHydroIdeal.INDEX_YMOM]/ijk[EquationsHydroIdeal.INDEX_RHO])*V;
			//V = 0; 
		
		
			for (int q = 0; q < quantities; q++){
				fluxRes[q] += uStar*(ip1jk[q] + ijk[q])/2 - absUStar*(ip1jk[q] - ijk[q])/2;
				/*fluxRes[q] += uStar*(ip1jp1k[q] + ijp1k[q] + 2.0*(ip1jk[q] + ijk[q]) + ip1jm1k[q] + ijm1k[q])/8 
						- absUStar*(ip1jp1k[q] - ijp1k[q] + 2.0*(ip1jk[q] - ijk[q]) + ip1jm1k[q] - ijm1k[q])/8;
				fluxRes[q] -= signUStar*V*(ip1jp1k[q] - ip1jm1k[q]  + ijp1k[q] - ijm1k[q] )/8; //*/
			}		
			
		} else {
			double average, diffX, diffY;
			
			double div = (uip1jp1 - uijp1 + 2.0*(uip1j - uij) + uip1jm1 - uijm1)/4 
					+ (vijp1 - vijm1 + vip1jp1 - vip1jm1)/4;
			double dtEstimate = 1.0 / 2 / maxEVal / 10;
			
			uStar = (uip1j + uij)/2;
			
			//double signU = Math.signum(uip1j + uij);
			double signU = Math.signum(uStar);
			
			double signV = Math.signum(vip1j + vij);
			
			
			for (int q = 0; q < quantities; q++){
				//average = (ijk[q]*uij + ip1jk[q]*uip1j)/2;
				average = uStar * (ijk[q] + ip1jk[q])/2;
				
				//diffX = ip1jk[q]*uip1j - ijk[q]*uij;
				diffX = uStar*(ip1jk[q] - ijk[q]);
				
				//diffY = (ip1jp1k[q]*uip1jp1 + ijp1k[q]*uijp1 - ip1jm1k[q]*uip1jm1 - ijm1k[q]*uijm1)/4;
				diffY = uStar * (ip1jp1k[q] + ijp1k[q] - ip1jm1k[q] - ijm1k[q])/4;
				
				fluxRes[q] += (average - signU/2*diffX - signV/2*diffY) / (1 + dtEstimate*div);
				
			}
			
			
		}
		
		
		return new FluxAndWaveSpeed(fluxRes, maxEVal);
		
	}

	@Override
	public String getDetailedDescription() {
		return "CGK-like, but additive operator split with the advection step working with the same quantities";
	}

}
