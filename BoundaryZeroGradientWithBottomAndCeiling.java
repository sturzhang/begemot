
public class BoundaryZeroGradientWithBottomAndCeiling extends Boundary {

	protected double[] quantitiesAtBottom;
	protected double[] quantitiesAtCeiling;
	
	public BoundaryZeroGradientWithBottomAndCeiling(GridCartesian grid, double[] quantitiesAtBottom, double[] quantitiesAtCeiling) {
		super(grid);
		this.quantitiesAtBottom = quantitiesAtBottom;
		this.quantitiesAtCeiling = quantitiesAtCeiling;
	}

    public String getDetailedDescription(){
    	String resBottom = "";
    	for (int i = 0; i < quantitiesAtBottom.length; i++){
    		resBottom += (i == 0 ? "" : ", ") + quantitiesAtBottom[i];
    	}
    	
    	String resCeiling = "";
    	for (int i = 0; i < quantitiesAtCeiling.length; i++){
    		resBottom += (i == 0 ? "" : ", ") + quantitiesAtCeiling[i];
    	}
    	
    	return "Fixed to " + resBottom + "at bottom, to " +resCeiling + "on top and zero-gradient (outflow) everywhere else";
    }

	
    @Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k;
		int nextI, nextJ, nextK;
		
		i = g.i(); j = g.j(); k = g.k();
		
		if (j < ((GridCartesian) grid).indexMinY()){
	
			for (int q = 0; q < Math.min(conservedQuantities[i][j][k].length, quantitiesAtBottom.length); q++){
				conservedQuantities[i][j][k][q] = quantitiesAtBottom[q];
			}
		} else {
			if (j > ((GridCartesian) grid).indexMaxY()){
				for (int q = 0; q < Math.min(conservedQuantities[i][j][k].length, quantitiesAtCeiling.length); q++){
					conservedQuantities[i][j][k][q] = quantitiesAtCeiling[q];
				}
			} else {
				nextI = i; nextJ = j; nextK = k;
			
				if (i < ((GridCartesian) grid).indexMinX()){ nextI = ((GridCartesian) grid).indexMinX(); }
				if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); }
				if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
				
				if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }
				if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }
				if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }
			
				for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
					conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
				}
			}
		}
		
	}

}
