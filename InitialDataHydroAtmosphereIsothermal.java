
public class InitialDataHydroAtmosphereIsothermal extends InitialDataHydroAtmosphere {

	protected double K, rho0;
	
	public InitialDataHydroAtmosphereIsothermal(GridCartesian grid, EquationsHydroIdeal equations,
			double rhoAtBottom, double pressureAtBottom, double gy, double xVelocity) {
		super(grid, equations, gy, xVelocity, false);
		K = pressureAtBottom/rhoAtBottom;
		rho0 = rhoAtBottom;
	}

    public String getDetailedDescription(){
    	return "Static atmosphere with a perfect gas with p = K rho, K =" + K + ", rho at bottom = " + rho0 + 
    			", pressure at bottom = " + (K*rho0) + ", gravity pointing along the y-axis and having the value g_y = " + gy;
    }

    public double getRho(double y){
    	return rho0 * Math.exp(gy * y / K); // + 0.05*Math.exp(-(y-0.3)*(y-0.3)/0.1/0.1);
    }
        
    public double getPressure(double y){
    	return K*getRho(y); // 1.0 + rho0 * gy * y;
    }

	@Override
	public double getRhoDeriv(double y) {
		return gy * K * getRho(y);
	}

	@Override
	public double getPressureDeriv(double y) {
		return K*getRhoDeriv(y);
	}

	
}
