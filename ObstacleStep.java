import java.util.LinkedList;


public class ObstacleStep extends ObstacleHydro {

	protected double position;
	protected double height;
	
	protected int iStart, jTop;
	
	public ObstacleStep(GridCartesian grid, EquationsHydroIdeal equations, double position, double height) {
		super(grid, equations);
		this.position = position;
		this.height = height;
		//System.err.println(height + " " + position);
		iStart = grid.getXIndex(position);
		jTop = grid.getYIndex(grid.ymin + height);
	}

	@Override
	protected void setObstacleBoundaries() {
		obstacleBoundary = new LinkedList<CellInterface>();
		
		for (int i = ((GridCartesian) grid).indexMinX(); i < iStart; i++){
			for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
				obstacleBoundary.add(new CellInterface(i, ((GridCartesian) grid).indexMinY(), k, ReconstructionCartesian.NORTH));
			}
		}
		
		
		for (int i = iStart; i <= ((GridCartesian) grid).indexMaxX(); i++){
			for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
				obstacleBoundary.add(new CellInterface(i, jTop, k, ReconstructionCartesian.NORTH));
			}
		}
		
		System.err.println("step corner: " + iStart + " " + jTop);
		
		
		
		//System.err.println(grid.indexMinZ() + " " + grid.indexMaxZ());
		//System.err.println(iStart + " " + jTop +  " " +grid.ymin + " " +  height + " " + position);
		for (int j = ((GridCartesian) grid).indexMinY(); j <= jTop; j++){
			for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
				obstacleBoundary.add(new CellInterface(iStart, j, k, ReconstructionCartesian.WEST));
			}
		}

	}

	@Override
	public String getDetailedDescription() {
		return "Step obstacle beginning at x = " + position + " and havng a height of " + height;
	}

	@Override
	protected void setObstacleInterior(boolean[][][] excludeFromTimeEvolution) {

		for (int i = iStart; i <= ((GridCartesian) grid).indexMaxX(); i++){
			for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
				excludeFromTimeEvolution[i][((GridCartesian) grid).indexMinY()][k] = true;
			}
		}

		for (int i = iStart; i <= ((GridCartesian) grid).indexMaxX(); i++){
			for (int j = ((GridCartesian) grid).indexMinY(); j <= jTop; j++){
				for (int k = ((GridCartesian) grid).indexMinZ(); k <= ((GridCartesian) grid).indexMaxZ(); k++){
					excludeFromTimeEvolution[i][j][k] = true;
				}
			}
		}
		
	}

}
