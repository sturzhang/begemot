
public class InitialDataLakeAtRest extends InitialDataIsentropic {

	protected SourceIsentropicEulerLikeInShallowWater source;
	protected double waterLevel;
	
	public InitialDataLakeAtRest(Grid grid, EquationsIsentropicEuler equations,
			SourceIsentropicEulerLikeInShallowWater source, double waterLevel) {
		super(grid, equations);
		this.source = source;
		this.waterLevel = waterLevel;
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double bottom = source.bottomTopography(g.getX());
		double[] res = new double[numberOfConservedQuantities];
		res[EquationsIsentropicEuler.INDEX_RHO] = waterLevel - bottom;
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Lake at rest";
	}

}
