
public abstract class InitialDataAdvectionStationary extends
		InitialDataScalarAdvection {

	public InitialDataAdvectionStationary(Grid grid, EquationsAdvectionConstant equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int numberOfConservedQuantities) {
		double[] res = new double[numberOfConservedQuantities];
		
		double x = g.getX() - grid.xMidpoint();
		double y = g.getY() - grid.yMidpoint();
		
		double[] U = ((EquationsAdvectionConstant) equations).velocity();
		
		// (U \partial_x + V \partial_y) f(V x - U y) = f'(...) (UV - VU) = 0
		
		res[0] = initialDataPerpendicularToVelocity(U[1]*x - U[0]*y);
		return res;
	}

	protected abstract double initialDataPerpendicularToVelocity(double perpCoordinate);
	


}
