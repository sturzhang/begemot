
public class InitialDataHydroSphericalShock extends InitialDataHydro {

	public InitialDataHydroSphericalShock(Grid grid, EquationsHydroIdeal equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double pressure, r;
    	
    	if (grid.dimensions() <= 2){
    		//r = Math.abs(g.getX() - grid.xMidpoint());
    		//r = Math.abs(g.getY() - grid.yMidpoint());
    		r = Math.sqrt(Math.pow(g.getX() - grid.xMidpoint(), 2) + Math.pow(g.getY() - grid.yMidpoint(), 2));
    	} else {
    		r = Math.sqrt(Math.pow(g.getX() - grid.xMidpoint(), 2) + 
    					  Math.pow(g.getY() - grid.yMidpoint(), 2) + 
    					  Math.pow(g.getZ() - grid.zMidpoint(), 2));
    	}
    	
    	
    	
    	if (r < 0.3){ // grid.xMidpoint()/3
    		res[EquationsHydroIdeal.INDEX_RHO] = 1.0;
    		pressure = 1.0;
    		
    		//res[EquationsHydroIdeal.INDEX_RHO] = 1.0*0.001 + 1.0;
    		//pressure = 1.0*0.001 + 1.0;
    		//pressure = 1.0;
    	} else {
    		res[EquationsHydroIdeal.INDEX_RHO] = 0.125;
    		pressure = 0.1;
    		
    		//res[EquationsHydroIdeal.INDEX_RHO] = 0.125*0.001 + 1.0;
    		//pressure = 0.1*0.001 + 1.0;
    		//pressure = 1.0;
    	}
    				
    	res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
    	res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    				
    	res[EquationsHydroIdeal.INDEX_ENERGY] = 
    						((EquationsHydroIdeal) equations).getEnergy(pressure, 0.0);
    	return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Spherical shock 'tube' test";
	}

}
