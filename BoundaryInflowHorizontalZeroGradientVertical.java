
public class BoundaryInflowHorizontalZeroGradientVertical extends Boundary {

	protected double[] conservedQuantitiesAtInflow;

	public BoundaryInflowHorizontalZeroGradientVertical(GridCartesian grid, double[] conservedQuantitiesAtInflow) {
		super(grid);
		this.conservedQuantitiesAtInflow = conservedQuantitiesAtInflow;
	}

	@Override
	protected void fillCell(GridCell g, double[][][][] conservedQuantities, double time){
		int i, j, k, nextI, nextJ, nextK;
		boolean inflow;
		
		i = g.i(); j = g.j(); k = g.k();
		
		nextI = i; nextJ = j; nextK = k;
		inflow = false;
			
		if (i < ((GridCartesian) grid).indexMinX()){ inflow = true; }
		if (j < ((GridCartesian) grid).indexMinY()){ nextJ = ((GridCartesian) grid).indexMinY(); }
		if (k < ((GridCartesian) grid).indexMinZ()){ nextK = ((GridCartesian) grid).indexMinZ(); }
			
		if (i > ((GridCartesian) grid).indexMaxX()){ nextI = ((GridCartesian) grid).indexMaxX(); }//inflow = true; }
		if (j > ((GridCartesian) grid).indexMaxY()){ nextJ = ((GridCartesian) grid).indexMaxY(); }
		if (k > ((GridCartesian) grid).indexMaxZ()){ nextK = ((GridCartesian) grid).indexMaxZ(); }

		if (inflow){
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantitiesAtInflow[q];
			}
		} else {
			for (int q = 0; q < conservedQuantities[i][j][k].length; q++){
				conservedQuantities[i][j][k][q] = conservedQuantities[nextI][nextJ][nextK][q];
			}
		}
	}

	@Override
	public String getDetailedDescription() {
		return "Zero gradient in the vertical direction, inflow and outflow (equal) in the horizontal one.";
	}

}
