
public class InitialDataHydroNoFlow extends InitialDataHydro {

	private double rho;
	private double pressure;
	
	public InitialDataHydroNoFlow(Grid grid, EquationsHydro equations, double rho, double pressure) {
		super(grid, equations);
		this.rho = rho;
		this.pressure = pressure;
	}

    public String getDetailedDescription(){
    	return "Ideal motionless gas of density = " + rho + "and pressure = " + pressure;
    }
	
	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];

		res[EquationsHydroIdeal.INDEX_RHO] = rho;
		res[EquationsHydroIdeal.INDEX_ENERGY] = 
				((EquationsHydroIdeal) equations).getEnergy(pressure, 0.0);
		
		res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;
    				
		return res;		
	}

}
