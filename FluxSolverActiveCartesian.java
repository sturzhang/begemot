
public abstract class FluxSolverActiveCartesian extends FluxSolverActive {

	public FluxSolverActiveCartesian(GridCartesian grid, Equations equations, Reconstruction reconstruction) {
		super(grid, equations, reconstruction);
	}
	
	public FluxSolverActiveCartesian(GridCartesian grid, Equations equations, Reconstruction reconstruction, int fluxTimeIntegration) {
		super(grid, equations, reconstruction, fluxTimeIntegration);
	}

	protected void performTimeEvolutionOfInterfaceValues(double timeInterval){
		GridEdgeMapped[] allEdges;
		double position = 0; // to make compiler happy
		int direction = -1; // to make compiler happy
		double[] normal;
		
		for (GridCell g : grid){
			// TODO here values are computed twice (keep track of already known ones and set both at the same time!)
					
			
			allEdges = grid.getAllEdges(g.i(), g.j(), g.k());
			for (int edgeIndex = 0; edgeIndex < allEdges.length; edgeIndex++){
				normal = allEdges[edgeIndex].normalVector();
				if (Math.abs(normal[0]) == 1.0){
					direction = GridCartesian.X_DIR;		    					
					position = 0.5*(allEdges[edgeIndex].adjacent1().getX() + allEdges[edgeIndex].adjacent2().getX()); 
				} else if (Math.abs(normal[1]) == 1.0){
					direction = GridCartesian.Y_DIR;
					position = 0.5*(allEdges[edgeIndex].adjacent1().getY() + allEdges[edgeIndex].adjacent2().getY());
				} else if (Math.abs(normal[2]) == 1.0){
					direction = GridCartesian.Z_DIR;
					position = 0.5*(allEdges[edgeIndex].adjacent1().getZ() + allEdges[edgeIndex].adjacent2().getZ());
				} else {
					System.err.println("ERROR: Problem at edge normal determination (flux solver)!");
				}
				
				
				grid.interfaceValues[g.i()][g.j()][g.k()][edgeIndex] = 
						forwardToEvolution(position, timeInterval, reconstruction, direction, allEdges[edgeIndex]);
			
			}
		}
	}

	// this is related purely to the fact that the syntax of the statement depends on the equations and the reconstruction:
	// they must fit. therefore in every inheriting class you basically have to provide a statement in which just the
	// class names change. in any case you call something like the evolve1d method (provided e.g. by EquationsAdvectionConstant)
	// and pass it some reconstruction that is allowed.
	protected abstract double[] forwardToEvolution(double pos, double timeInterval, Reconstruction recon, int direction, GridEdgeMapped edge);
	
	@Override
	public int steps() { return 1; }
	
	@Override
	public String getDetailedDescription() {
		return "Simplified solver: similar to active flux, but dimensionally split and using the updated value at the interface to compute the flux.";
	}

}
