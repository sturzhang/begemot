
public class OutputVorticityAndDivergenceHydro extends Output {

	public OutputVorticityAndDivergenceHydro(double outputTimeInterval, Grid grid, EquationsHydroIdeal equations) {
		super(outputTimeInterval, grid, equations);
	}

	@Override
	public String getDetailedDescription() {
		return "prints the central-averaged vorticity";
	}

	
	protected double getXDerivativeOfVelocity(double[][][][] quantities, int i, int j, int k, int qMomentum){
		return 0.25*(quantities[i+1][j+1][k][qMomentum]/quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
		      -     quantities[i-1][j+1][k][qMomentum]/quantities[i-1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
		    		  )/((GridCartesian) grid).xSpacing()/2 + 
		      0.5*(quantities[i+1][j][k][qMomentum]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
		      -    quantities[i-1][j][k][qMomentum]/quantities[i-1][j][k][EquationsHydroIdeal.INDEX_RHO]
		    		  )/((GridCartesian) grid).xSpacing()/2 + 
			  0.25*(quantities[i+1][j-1][k][qMomentum]/quantities[i+1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
			  -     quantities[i-1][j-1][k][qMomentum]/quantities[i-1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
					  )/((GridCartesian) grid).xSpacing()/2;
	}
	
	protected double getYDerivativeOfVelocity(double[][][][] quantities, int i, int j, int k, int qMomentum){
		return 0.25*(quantities[i+1][j+1][k][qMomentum]/quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
				-  quantities[i+1][j-1][k][qMomentum]/quantities[i+1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
				  )/((GridCartesian) grid).ySpacing()/2 + 
		  0.5*(quantities[i][j+1][k][qMomentum]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
		  -    quantities[i][j-1][k][qMomentum]/quantities[i][j-1][k][EquationsHydroIdeal.INDEX_RHO]
				  )/((GridCartesian) grid).ySpacing()/2 + 
		  0.25*(quantities[i-1][j+1][k][qMomentum]/quantities[i-1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
		  -     quantities[i-1][j-1][k][qMomentum]/quantities[i-1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
				  )/((GridCartesian) grid).ySpacing()/2;
	}
	
	@Override
	protected void printOutput(double time, double[][][][] quantities) {
		int i,j,k;
		double dxv, dyu, dxu, dyv;
		
		println("");
		println("");

		
		for (GridCell g : grid){
			i = g.i(); j = g.j(); k = g.k();
			
			dyu = getYDerivativeOfVelocity(quantities, i, j, k, EquationsHydroIdeal.INDEX_XMOM);
			dyv = getYDerivativeOfVelocity(quantities, i, j, k, EquationsHydroIdeal.INDEX_YMOM);
			
			/*dyu = 0.25*(quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
				  -     quantities[i+1][j-1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i+1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
						  )/((GridCartesian) grid).ySpacing()/2 + 
				  0.5*(quantities[i][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j+1][k][EquationsHydroIdeal.INDEX_RHO]
				  -    quantities[i][j-1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i][j-1][k][EquationsHydroIdeal.INDEX_RHO]
						  )/((GridCartesian) grid).ySpacing()/2 + 
				  0.25*(quantities[i-1][j+1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i-1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
				  -     quantities[i-1][j-1][k][EquationsHydroIdeal.INDEX_XMOM]/quantities[i-1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
						  )/((GridCartesian) grid).ySpacing()/2; */
			
			
			dxv = getXDerivativeOfVelocity(quantities, i, j, k, EquationsHydroIdeal.INDEX_YMOM);
			dxu = getXDerivativeOfVelocity(quantities, i, j, k, EquationsHydroIdeal.INDEX_XMOM);
			
			/*dxv = 0.25*(quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
			      -     quantities[i-1][j+1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i-1][j+1][k][EquationsHydroIdeal.INDEX_RHO]
			    		  )/((GridCartesian) grid).xSpacing()/2 + 
			      0.5*(quantities[i+1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j][k][EquationsHydroIdeal.INDEX_RHO]
			      -    quantities[i-1][j][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i-1][j][k][EquationsHydroIdeal.INDEX_RHO]
			    		  )/((GridCartesian) grid).xSpacing()/2 + 
				  0.25*(quantities[i+1][j-1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i+1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
				  -     quantities[i-1][j-1][k][EquationsHydroIdeal.INDEX_YMOM]/quantities[i-1][j-1][k][EquationsHydroIdeal.INDEX_RHO]
						  )/((GridCartesian) grid).xSpacing()/2; */
			
			println(g.getX() + " " + g.getY() + " " + (dxv - dyu) + " " + (dxu + dyv));
		}

	}

	@Override
	protected void finalizeOutput() {

	}
}
