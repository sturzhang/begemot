
public class InitialDataAcousticDivFree extends InitialDataAcoustic {

	private double pressure, dxu;

	
	public InitialDataAcousticDivFree(Grid grid, EquationsAcousticSimple equations,
			double pressure, double dxu) {
		super(grid, equations);
		this.pressure = pressure;
		this.dxu = dxu;
	}

	@Override
	public double[] getInitialValue(GridCell g, int q) {
		double[] res = new double[q];
		
		res[EquationsAcousticSimple.INDEX_P] = pressure;
		res[EquationsAcousticSimple.INDEX_VX] = dxu*(g.getX() - grid.xMidpoint()) + 5.0*(g.getX() - grid.xMidpoint())*(g.getX() - grid.xMidpoint());
				//+ 0.3*((g.getY() - grid.yMidpoint()));
		res[EquationsAcousticSimple.INDEX_VY] = -dxu*(g.getY() - grid.yMidpoint()) - 5.0*(g.getX() - grid.xMidpoint())*(g.getY() - grid.yMidpoint());
			//+ 0.3*((g.getX() - grid.xMidpoint()));
		res[EquationsAcousticSimple.INDEX_VZ] = 0.0;
		
		return res;	
	}

	@Override
	public String getDetailedDescription() {
		return "divergencefree velocity field (linear in x and y with x-slope of u = " + dxu + ") and constant pressure = " + pressure;
	}

}
