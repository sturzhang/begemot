
public abstract class InitialDataIsentropicVortex extends InitialDataIsentropic {

	protected double[] offsetSpeed;
	
	public InitialDataIsentropicVortex(Grid grid, EquationsIsentropicEuler equations, double[] offsetSpeed) {
		super(grid, equations);
		this.offsetSpeed = offsetSpeed;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
    	double r, x, y;
		
		x = g.getX() - grid.xMidpoint();
		y = g.getY() - grid.yMidpoint();
		r = Math.sqrt(x*x + y*y); 
		
		res[EquationsHydroIdeal.INDEX_RHO] = rho(r);
    				
		if (r == 0.0){
			res[EquationsHydroIdeal.INDEX_XMOM] = 0.0;
			res[EquationsHydroIdeal.INDEX_YMOM] = 0.0;
		} else {
			res[EquationsHydroIdeal.INDEX_XMOM] = -rho(r) * (speed(r) * y / r - offsetSpeed[0]);
			res[EquationsHydroIdeal.INDEX_YMOM] =  rho(r) * (speed(r) * x / r + offsetSpeed[1]);
		}
		res[EquationsHydroIdeal.INDEX_ZMOM] = 0.0;

    	return res;	
	}

	protected abstract double speed(double r);
	protected abstract double rho(double r);

}
