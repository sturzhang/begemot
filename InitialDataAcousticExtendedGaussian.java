
public class InitialDataAcousticExtendedGaussian extends
		InitialDataAcousticExtended {

	protected double width;
	
	public InitialDataAcousticExtendedGaussian(Grid grid, EquationsAcousticExtended equations, double width) {
		super(grid, equations);
		this.width = width;
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];
		double x = g.getX() - grid.xMidpoint();
		
		double value = Math.exp(-x*x/width/width); 
		
		res[EquationsAcousticExtended.INDEX_RHO] = value;
		res[EquationsAcousticExtended.INDEX_P]   = 0.0;    					
		res[EquationsAcousticExtended.INDEX_VX]  = 0.0;
		
		res[EquationsAcousticExtended.INDEX_VY]  = 0.0;
		res[EquationsAcousticExtended.INDEX_VZ]  = 0.0;

		return res;
	}

	@Override
	public String getDetailedDescription() {
		// TODO Auto-generated method stub
		return null;
	}

}
