
public class FluxSolverMultiStepMultiDExactAcousticWithAdvection extends
		FluxSolverMultiStepMultiDExactRiemann {

	public FluxSolverMultiStepMultiDExactAcousticWithAdvection(
			GridCartesian grid, EquationsAcoustic equations) {
		super(grid, equations);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public double prefactor(int dir, double speedX, double speedY, double dt, int fluxSolverStep){
		if (fluxSolverStep == 0){ 
			return 1.0;
		} else {
			if (((GridCartesian) grid).dimensions() == 2){
				if (dir == GridCartesian.X_DIR){
					return dt/((GridCartesian) grid).ySpacing();
				} else if (dir == GridCartesian.Y_DIR){
					return dt/((GridCartesian) grid).xSpacing();
				} else {
					System.err.println("ERROR: asked for non-x, non-y direction");
					return 0.0;
				}
			} else {
				return 0.0;
			}
		}
	}
		
	@Override
	protected FluxMultiStepAndWaveSpeed fluxmultid(GridEdgeMapped gridEdge,
			double[] LT, double[] L, double[] LB,
			double[] RT, double[] R, double[] RB, int timeIntegratorStep) {
		int quantities = L.length;
				
		double[] flux1 = new double[quantities];
		double[] flux2 = new double[quantities];
		
		double[] diffX = new double[quantities];
		double[] mixeddiffX = new double[quantities];
		double[] sumX = new double[quantities];
		
		double centralAverageLeft[] = new double[quantities];
		double centralAverageRight[] = new double[quantities];
		double centralDiffLeft[] = new double[quantities];
		double centralDiffRight[] = new double[quantities];
		double secondDiffLeft[] = new double[quantities];
		double secondDiffRight[] = new double[quantities];
		
		double c = ((EquationsAcousticSimple) equations).soundspeed();
		
		double U = ((EquationsAcousticSimple) equations).vx();
		double V = ((EquationsAcousticSimple) equations).vy();

		
		for (int q = 0; q < quantities; q++){ 
			diffX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] - (0.25*LT[q] + 0.5*L[q] + 0.25*LB[q]);
			//diffX[q] = R[q] - L[q];
			sumX[q] = 0.25*RT[q] + 0.5*R[q] + 0.25*RB[q] + 0.25*LT[q] + 0.5*L[q] + 0.25*LB[q];
			mixeddiffX[q] = (LT[q] + RT[q] - (LB[q] + RB[q]));
			
			centralAverageLeft[q] = (LT[q] + LB[q])/2;
			centralAverageRight[q] = (RT[q] + RB[q])/2;
			centralDiffLeft[q] = LT[q] - LB[q];
			centralDiffRight[q] = RT[q] - RB[q];
			secondDiffLeft[q] = LT[q] - 2.0*L[q] + LB[q];
			secondDiffRight[q] = RT[q] - 2.0*R[q] + RB[q];
		}
		
		double absU = Math.abs(U);
		double absV = Math.abs(V);
		
		// advective flux
		
		for (int q = 0; q < quantities; q++){
			if (q == EquationsAcoustic.INDEX_P){
				flux1[q] = -absU/2*(R[q]-L[q]) + U/2*(R[q] + L[q]);
			} else {
				flux1[q] = -absU/2*diffX[q] + U/2*sumX[q];
			}
			flux2[q] = 0;
		}
		
		/*flux2[EquationsAcoustic.INDEX_VX] = 
				- absU*absV/4*(secondDiffRight[EquationsAcoustic.INDEX_VX] - secondDiffLeft[EquationsAcoustic.INDEX_VX])
				+ V*absU/4*(centralDiffRight[EquationsAcoustic.INDEX_VX] - centralDiffLeft[EquationsAcoustic.INDEX_VX]) 
				- U*U*absV/8/V*(secondDiffRight[EquationsAcoustic.INDEX_VY] + secondDiffLeft[EquationsAcoustic.INDEX_VY])   
				+ U*U /8 *(centralDiffRight[EquationsAcoustic.INDEX_VY] + centralDiffLeft[EquationsAcoustic.INDEX_VY])  
				+ U* (    absV/4*(secondDiffRight[EquationsAcoustic.INDEX_VX] + secondDiffLeft[EquationsAcoustic.INDEX_VX])    
						- V/4 *(centralDiffRight[EquationsAcoustic.INDEX_VX] + centralDiffLeft[EquationsAcoustic.INDEX_VX])   
						- absU/8*(centralDiffRight[EquationsAcoustic.INDEX_VY] - centralDiffLeft[EquationsAcoustic.INDEX_VY])  
						+ absU*absV/8/V *(secondDiffRight[EquationsAcoustic.INDEX_VY] - secondDiffLeft[EquationsAcoustic.INDEX_VY])   ) ;
			//*/
		
		/*flux1[EquationsAcoustic.INDEX_VY] +=
							+absU*absV/8/U *(centralDiffRight[EquationsAcoustic.INDEX_VX] - centralDiffLeft[EquationsAcoustic.INDEX_VX]) 
							- absU*V/8/U *(secondDiffRight[EquationsAcoustic.INDEX_VX] - secondDiffLeft[EquationsAcoustic.INDEX_VX]) 
							+ V/8 *(secondDiffRight[EquationsAcoustic.INDEX_VX] + secondDiffLeft[EquationsAcoustic.INDEX_VX]) 
							- absV/8 *(centralDiffRight[EquationsAcoustic.INDEX_VX] + centralDiffLeft[EquationsAcoustic.INDEX_VX]); //*/ 
		

		
		// acoustic flux
		
		double sqrtU2V2 = Math.sqrt(U*U+V*V);

		
		//flux1[EquationsAcoustic.INDEX_P ] -= 0.5*c*diffX[EquationsAcoustic.INDEX_P];
		flux1[EquationsAcoustic.INDEX_P] -= c*(R[EquationsAcoustic.INDEX_P] - L[EquationsAcoustic.INDEX_P])/2;
		/*flux1[EquationsAcoustic.INDEX_P] -= c*(R[EquationsAcoustic.INDEX_P]*0.5+0.25*RT[EquationsAcoustic.INDEX_P]+0.25*RB[EquationsAcoustic.INDEX_P]
				- (L[EquationsAcoustic.INDEX_P]*0.5+0.25*RT[EquationsAcoustic.INDEX_P]+0.25*RB[EquationsAcoustic.INDEX_P]   ))/2;//*/
		flux1[EquationsAcoustic.INDEX_P] += c*sumX[EquationsAcoustic.INDEX_VX]/2;
		
		flux1[EquationsAcoustic.INDEX_VX] += c*(R[EquationsAcoustic.INDEX_P] + L[EquationsAcoustic.INDEX_P])/2;
		flux1[EquationsAcoustic.INDEX_VX] -= c*(diffX[EquationsAcoustic.INDEX_VX]/2 + mixeddiffX[EquationsAcoustic.INDEX_VY]/8);
		
		/*if ((absU > c) && (absV < c)){
			if (U > 0){
				flux1[EquationsAcoustic.INDEX_VY] -= (c - absV)*(centralDiffLeft[EquationsAcoustic.INDEX_VX])/4;
			} else {
				flux1[EquationsAcoustic.INDEX_VY] -= (c - absV)*(centralDiffRight[EquationsAcoustic.INDEX_VX])/4;
			}
		} else if ((absU < c) && (absV > c)){
			if (V > 0){
				flux1[EquationsAcoustic.INDEX_VX] -= (c - absU)*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RB[EquationsAcoustic.INDEX_VX]-LB[EquationsAcoustic.INDEX_VX])/4;
			} else {
				flux1[EquationsAcoustic.INDEX_VX] -= (c - absU)*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RT[EquationsAcoustic.INDEX_VX]-LT[EquationsAcoustic.INDEX_VX])/4;
			}
		} else {
			
			if (sqrtU2V2 > c){
				// not supersonic in any component, but supersonic in absolute value
				if (U > 0){
					flux1[EquationsAcoustic.INDEX_VY] -= (c - Math.max(absU,absV))*(centralDiffLeft[EquationsAcoustic.INDEX_VX])/4;
				} else {
					flux1[EquationsAcoustic.INDEX_VY] -= (c - Math.max(absU,absV))*(centralDiffRight[EquationsAcoustic.INDEX_VX])/4;
				}
				
				if (V > 0){
					flux1[EquationsAcoustic.INDEX_VX] -= (c - Math.max(absU,absV))*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RB[EquationsAcoustic.INDEX_VX]-LB[EquationsAcoustic.INDEX_VX])/4;
				} else {
					flux1[EquationsAcoustic.INDEX_VX] -= (c - Math.max(absU,absV))*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RT[EquationsAcoustic.INDEX_VX]-LT[EquationsAcoustic.INDEX_VX])/4;			
				}
			
				
				if (absU > absV){
					if (U > 0){
						flux1[EquationsAcoustic.INDEX_VY] -= (absU - absV)*(centralDiffLeft[EquationsAcoustic.INDEX_VX])/4;
					} else {
						flux1[EquationsAcoustic.INDEX_VY] -= (absU - absV)*(centralDiffRight[EquationsAcoustic.INDEX_VX])/4;
					}
				} else {
					if (V > 0){
						flux1[EquationsAcoustic.INDEX_VX] -= (absV - absU)*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RB[EquationsAcoustic.INDEX_VX]-LB[EquationsAcoustic.INDEX_VX])/4;
					} else {
						flux1[EquationsAcoustic.INDEX_VX] -= (absV - absU)*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RT[EquationsAcoustic.INDEX_VX]-LT[EquationsAcoustic.INDEX_VX])/4;
					}
				}
				
			} else {
				// fully subsonic
			
				flux1[EquationsAcoustic.INDEX_VX] -= diffX[EquationsAcoustic.INDEX_VX]/2*(c-sqrtU2V2);
				flux1[EquationsAcoustic.INDEX_VY] -= mixeddiffX[EquationsAcoustic.INDEX_VX]/8*(c-sqrtU2V2);
				
				
				if (U > 0){
					flux1[EquationsAcoustic.INDEX_VY] -= (sqrtU2V2 - Math.max(absU,absV))*(centralDiffLeft[EquationsAcoustic.INDEX_VX])/4;
				} else {
					flux1[EquationsAcoustic.INDEX_VY] -= (sqrtU2V2 - Math.max(absU,absV))*(centralDiffRight[EquationsAcoustic.INDEX_VX])/4;
				}
				
				if (V > 0){
					flux1[EquationsAcoustic.INDEX_VX] -= (sqrtU2V2 - Math.max(absU,absV))*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RB[EquationsAcoustic.INDEX_VX]-LB[EquationsAcoustic.INDEX_VX])/4;
				} else {
					flux1[EquationsAcoustic.INDEX_VX] -= (sqrtU2V2 - Math.max(absU,absV))*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RT[EquationsAcoustic.INDEX_VX]-LT[EquationsAcoustic.INDEX_VX])/4;			
				}
			
				
				if (absU > absV){
					if (U > 0){
						flux1[EquationsAcoustic.INDEX_VY] -= (absU - absV)*(centralDiffLeft[EquationsAcoustic.INDEX_VX])/4;
					} else {
						flux1[EquationsAcoustic.INDEX_VY] -= (absU - absV)*(centralDiffRight[EquationsAcoustic.INDEX_VX])/4;
					}
				} else {
					if (V > 0){
						flux1[EquationsAcoustic.INDEX_VX] -= (absV - absU)*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RB[EquationsAcoustic.INDEX_VX]-LB[EquationsAcoustic.INDEX_VX])/4;
					} else {
						flux1[EquationsAcoustic.INDEX_VX] -= (absV - absU)*(R[EquationsAcoustic.INDEX_VX]-L[EquationsAcoustic.INDEX_VX] + RT[EquationsAcoustic.INDEX_VX]-LT[EquationsAcoustic.INDEX_VX])/4;
					}
				}
			}
		
		}//*/
		
		return new FluxMultiStepAndWaveSpeed(flux1, flux2, c + Math.max(absU, absV));
	}

	@Override
	public String getDetailedDescription() {
		return "Exact evolution of the reconstruction that yields the consistent scheme, but now with advection included";
	}

}
