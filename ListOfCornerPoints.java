import java.util.LinkedList;

@SuppressWarnings("serial")
public class ListOfCornerPoints extends LinkedList<CornerPoint> {   
	
	public ListOfCornerPoints() {
		
	}
	
	public LinkedList<GeometricGridEdge> getEdges(){
		
		LinkedList<GeometricGridEdge> res = new LinkedList<GeometricGridEdge>();
		boolean firsttime = true;
		CornerPoint previousPoint = null; // to make compiler happy
		GeometricGridEdge tmpEdge = null, previousEdge = null; // to make compiler happy
		
		for (CornerPoint p : this){
			if (firsttime){
				firsttime = false;
				previousPoint = this.getLast();
			} 
			
			tmpEdge = new GeometricGridEdge(previousPoint, p);
			
			for (GeometricGridEdge e : p.adjacentEdges()){
				if (e.adjoins(previousPoint)){
					tmpEdge.setDuplicateGridEdge(e);
					e.setDuplicateGridEdge(tmpEdge);
					break;
				}
			}
			
			previousPoint.addEdge(tmpEdge);
			p.addEdge(tmpEdge);
			if (res.size() > 0){
				// whatever the orientation initially is, it is consistently either outward or inward
				tmpEdge.adjustNormal(previousEdge);
			}
			previousEdge = tmpEdge;
			res.add(tmpEdge);
			
			previousPoint = p;
		}
		return res;
	}
	
	public CornerPoint getCurrent(){
		return this.getFirst();
	}
	
	
	
	
}
