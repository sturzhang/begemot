
public class InitialDataAcousticExtendedDelta extends
		InitialDataAcousticExtended {

	public InitialDataAcousticExtendedDelta(GridCartesian grid,
			EquationsAcousticExtended equations) {
		super(grid, equations);
	}

	@Override
	public double[] getInitialValue(GridCell g, int qq) {
		double[] res = new double[qq];

		double peakheight = 1.0/g.size();
		
		if (g.i() == ((GridCartesian) grid).nx()/2){
			res[EquationsAcousticExtended.INDEX_RHO] = 0.0;
			res[EquationsAcousticExtended.INDEX_P]   = 0.0;    					
			res[EquationsAcousticExtended.INDEX_VX]  = peakheight;
		} else {
			res[EquationsAcousticExtended.INDEX_RHO] = 0.0;
			res[EquationsAcousticExtended.INDEX_P]   = 0.0;    					
			res[EquationsAcousticExtended.INDEX_VX]  = 0.0;			
		}
		res[EquationsAcousticExtended.INDEX_VY] = 0.0;
		res[EquationsAcousticExtended.INDEX_VZ] = 0.0;
	
	
		return res;
	}

	@Override
	public String getDetailedDescription() {
		return "Approximation of delta-distribution";
	}

}
