
public class OutputPressureless extends OutputAllQuantities {

	public OutputPressureless(double outputTimeInterval, GridCartesian grid, EquationsPressurelessEuler equations) {
		super(outputTimeInterval, grid, equations);
	}

	public OutputPressureless(double outputTimeInterval, double zproportion, GridCartesian3D grid, Equations equations) {
		super(outputTimeInterval, zproportion, grid, equations);
	}

	@Override
	protected void printOneCell(double[] quantities, int i, int j, int k) {
		double vx, vy, vz, v2;
		
		vx = quantities[EquationsPressurelessEuler.INDEX_XMOM]/quantities[EquationsPressurelessEuler.INDEX_RHO];
		vy = quantities[EquationsPressurelessEuler.INDEX_YMOM]/quantities[EquationsPressurelessEuler.INDEX_RHO];
		vz = quantities[EquationsPressurelessEuler.INDEX_ZMOM]/quantities[EquationsPressurelessEuler.INDEX_RHO];
		v2 = vx*vx+vy*vy+vz*vz;
		
		print(quantities[EquationsPressurelessEuler.INDEX_RHO] + " ");
		print(vx + " " + vy + " " + vz + " ");
		print(Math.sqrt(v2) + " ");
		
	}

	@Override
	protected void printForTestPurpose(double[][][][] quantities) {
		// nothing
	}

	@Override
	public String getDetailedDescription() {
		return "prints: rho, vx, vy, vz, |v|";
	}

}
